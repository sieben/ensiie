#############################################
# Etude du noyau Linux & Apprentissage de R #
#############################################

# Include 

library(FactoMineR) # ACP
library(vegan) # Clustering
library(quantreg) 
library(ks) # Kernel smoothing
library(PerformanceAnalytics) #skewness et kurtosis


# Input

data_commit_file <- read.table("commit_file.log",sep=" ",header=FALSE)
colnames(data_commit_file) <-  c("commit","name")

data_lines_file <- read.table("lines_file.log",sep=" ",header=FALSE)
colnames(data_lines_file) <- c("lines","name")

data_differentcommiter_file <- read.table("file_commiter.log",sep=" ",header=FALSE)
colnames(data_differentcommiter_file) <- c("name","diff_commiters")

data_extension <- read.table("file_extension.log",sep=" ",na.strings="NA",fill = TRUE ,header=FALSE)
colnames(data_extension) <- c("name","extension")

data_loglines <- data_lines_file
data_loglines[[1]] <- log(data_loglines[[1]])
colnames(data_loglines) <- c("log_line","name")

data_merged <- merge( data_commit_file, data_lines_file )
data_merged <- merge( data_merged, data_differentcommiter_file )
data_merged <- merge( data_merged, data_loglines )
data_merged <- merge( data_merged, data_extension, all.x = TRUE )

data_num <- data_merged[,c(-1,-6)] # Without file name & file type

attach(data_merged) 

# Data sorting

data_merged_sortby_commit <- data_merged[order(commit),]
data_merged_sortby_lines <- data_merged[order(lines),]
data_merged_sortby_diff_commiters <- data_merged[order(diff_commiters),]

capture.output( tail(data_merged_sortby_commit$name, n= 20 ), file = 'ranking_commit.txt' )

# Subset ( *.c / *.h )

data_merged_c <- data_merged[ which(data_merged$extension=='c'), ]
data_merged_h <- data_merged[ which(data_merged$extension=='h'), ]

capture.output( summary( data_merged_c ), file = 'summary_data_merged_c.txt' )
capture.output( summary( data_merged_h ), file = 'summary_data_merged_h.txt' )

# Primary analysis

capture.output( head( data_merged) , file = 'data_merged.txt')
capture.output( summary( data_merged ) , file='summary_data_merged.txt' )
capture.output( cor( data_num ) , file = 'cor_data_num.txt' ) # Correlation
capture.output( cov( data_num ) , file = 'cov_data_num.txt' ) # Covariance 

# Empirical Cumulative Distribution Function

data_merged_ecdf_lines <- ecdf(data_merged$lines)
data_merged_ecdf_commit <- ecdf(data_merged$commit)
data_merged_ecdf_diff_commiters <- ecdf(data_merged$diff_commiters)

jpeg( filename = 'img/ecdf_lines.jpeg' )
plot( data_merged_ecdf_lines )
dev.off()

jpeg( filename = 'img/ecdf_commit.jpeg' )
plot( data_merged_ecdf_commit )
dev.off()

jpeg( filename = 'img/ecdf_diff_commiters.jpeg' )
plot ( data_merged_ecdf_diff_commiters )
dev.off()

# ANOVA 

aov_lines_commit <- aov( data_merged$lines ~ data_merged$commit )
aov_lines_diff_commiters <- aov( data_merged$lines ~ data_merged$diff_commiters )
aov_commit_diff_commiters <- aov( data_merged$commit ~ data_merged$diff_commiters )

capture.output( aov_lines_commit, file = 'aov_lines_commit.txt' )
capture.output( aov_lines_diff_commiters, file = 'aov_lines_diff_commiters.txt' )
capture.output( aov_commit_diff_commiters, file = 'aov_commit_diff_commiters.txt' )

capture.output( summary(aov_lines_diff_commiters), file = 'summary_aov_lines_diff_commiters.txt' )
capture.output( summary(aov_commit_lines), file = 'summary_aov_commit_lines.txt' )
capture.output( summary(aov_commit_diff_commiters), file = 'summary_aov_commit_diff_commiters.txt' )


jpeg( filename = 'img/aov_lines_commit.jpeg' )
plot( aov_lines_commit )
dev.off()

jpeg( filename = 'img/aov_lines_diff_commiters.jpeg' )
plot ( aov_lines_diff_commiters )
dev.off()

jpeg( filename = 'img/aov_commit_diff_commiters.jpeg' )
plot( aov_commit_diff_commiters )
dev.off()

# Regression exp

f <- function( x, a, b ){ a * exp( b * x ) }
#fm <- nls( y ~ f( x, a, b ), data = data_merged_sortby_lines, start = c( a=1, b=1 )) 

# Quantile regression

rq_commit_line_0.1 <- rq( commit ~ lines , tau = 0.1 )
rq_commit_line_0.5 <- rq( commit ~ lines , tau = 0.5 )
rq_commit_line_0.9 <- rq( commit ~ lines , tau = 0.9 )

# K-medians (ROBUST) 

data_merged.clara.silcoeff <- numeric(10)

for( k in  2:10 ) 
{
	data_merged.clara.silcoeff [k] <- 
	summary( silhouette( clara( data_num[,c(-4)], k ) ) )$avg.width
}
rm(k)

jpeg( filename='img/clara_clustering.jpeg')
plot(data_merged.clara.silcoeff)
dev.off()

## Best choice k = 2

data_merged.clara <- clara( data_num[,c(-3,-4)], 2 )
data_merged.outliers <- data_merged[ clara( data_num[,c(-4)], 2 )$clustering == 2, ]
data_merged.outliers_sortby_lines <- data_merged.outliers[order( data_merged.outliers$lines, decreasing=TRUE ), ]

jpeg( filename='img/clara.jpeg' )
plot( data_merged.clara )
dev.off()

# Principal composant analysis 

# stat package

pca <- prcomp( data_num )
names( pca )

jpeg( filename='img/pca.jpeg' )
biplot( pca )
dev.off()

jpeg( filename='img/pca2.jpeg' )
pca2 <- princomp( data_num )
biplot( pca2 )
dev.off()


capture.output( summary(pca) , file='../summary_pca.txt' )
capture.output( pca, file='../pca.txt' )


data_num_c <- data_merged_c[,c(-1,-5,-6)]

jpeg( filename = 'img/pca_c.jpeg' )
pca_c <-  prcomp( data_num_c )
biplot( pca_c )
dev.off()

capture.output( summary(pca_c) , file='../summary_pca_c.txt' )
capture.output( pca_c, file='../pca_c.txt' )

# pca( H file )

data_num_h <- data_merged_h[,c(-1,-5,-6)]

jpeg( filename = 'img/pca_h.jpeg' )
pca_h <- prcomp( data_num_h )
biplot( pca_h )
dev.off()

capture.output( summary(pca_h), file = '../summary_pca_h.txt' )
capture.output( pca_h, file = '../pca_h.txt' )

# anova ( c file )

data_merged_c_aov_lines_commit <- aov( data_merged_c$lines ~ data_merged_c$commit )
data_merged_c_aov_lines_diff_commiters <- aov( data_merged_c$lines ~ data_merged_c$diff_commiters )
data_merged_c_aov_commit_diff_commiters <- aov( data_merged_c$commit ~ data_merged_c$diff_commiters )

capture.output( data_merged_c_aov_lines_commit, file = 'data_merged_c_aov_lines_commit.txt' )
capture.output( data_merged_c_aov_lines_diff_commiters, file = 'data_merged_c_aov_lines_diff_commiters.txt' )
capture.output( data_merged_c_aov_commit_diff_commiters, file = 'data_merged_c_aov_commit_diff_commiters.txt' )

capture.output( summary(data_merged_c_aov_lines_diff_commiters), file = 'data_merged_c_summary_aov_lines_diff_commiters.txt' )
capture.output( summary(data_merged_c_aov_lines_commit), file = 'data_merged_c_summary_aov_lines_commit.txt' )
capture.output( summary(data_merged_c_aov_commit_diff_commiters), file = 'data_merged_c_summary_aov_commit_diff_commiters.txt' )

# anova ( h file )

data_merged_h_aov_lines_commit <- aov( data_merged_h$lines ~ data_merged_h$commit )
data_merged_h_aov_lines_diff_commiters <- aov( data_merged_h$lines ~ data_merged_h$diff_commiters )
data_merged_h_aov_commit_diff_commiters <- aov( data_merged_h$commit ~ data_merged_h$diff_commiters )

capture.output( data_merged_h_aov_lines_commit, file = 'data_merged_h_aov_lines_commit.txt' )
capture.output( data_merged_h_aov_lines_diff_commiters, file = 'data_merged_h_aov_lines_diff_commiters.txt' )
capture.output( data_merged_h_aov_commit_diff_commiters, file = 'data_merged_h_aov_commit_diff_commiters.txt' )

capture.output( summary(data_merged_h_aov_lines_diff_commiters), file = 'data_merged_h_summary_aov_lines_diff_commiters.txt' )
capture.output( summary(data_merged_h_aov_lines_commit), file = 'data_merged_h_summary_aov_lines_commit.txt' )
capture.output( summary(data_merged_h_aov_commit_diff_commiters), file = 'data_merged_h_summary_aov_commit_diff_commiters.txt' )

########################## Graphes #########################

jpeg( filename='img/plot.jpeg' )
plot( data_num, main='Damier des plots' ) # Damier de graphes
dev.off()


## Histogrammes

jpeg( filename='img/histogramme.jpeg' )
hist(	data_merged[[5]],
	main='Histogramme log(lines)',
	xlab='log(lines)',
	ylab='Effectif'	
) # Histogramme du vecteur log_line (Normal saikks)
dev.off()

# Il faut en faire plein avec l'utilisation du log des 
# valeurs intéressantes


jpeg(filename='barplot.jpeg')
barplot(data_merged[[3]]) # Le graphe est chaotique
dev.off()

# QQ-plot 

jpeg( filename='img/qqplot_commit_lines.jpeg' )
qqplot( commit, lines ) 
dev.off()

jpeg( filename='img/qqplot_commit_diff_commiters.jpeg' )
qqplot( commit, diff_commiters )
dev.off()

jpeg( filename='img/qqplot_lines_diff_commiters.jpeg' )
qqplot( lines, diff_commiters )
dev.off()

# Graphes bivariés

jpeg( filename='img/pairs.jpeg' )
pairs( data_merged, main='Graphes bivariés' )
dev.off()

# Export to csv 

write.csv2( data_merged, file = 'export/merged.log', eol='\n', na='NA' )
