\def\^#1{\if#1i{\accent"5E\i}\else{\accent"5E #1}\fi}
\def\"#1{\if#1i{\accent"7F\i}\else{\accent"7F #1}\fi}
\documentclass[12pt,a4paper]{tgarticle}
\usepackage{french,epsfig,graphicx,amssymb,rotating}
\bibliographystyle{unsrt}
%
%\special{!userdict begin /bop-hook{gsave 110 20 translate
%56 rotate /Helvetica-BoldOblique findfont 170 scalefont setfont
%0 0 moveto 0.88 setgray (Brouillon) 0.7 0.84 0.85 setrgbcolor show grestore}def end}
\special{!userdict begin /bop-hook{gsave 110 20 translate
56 rotate /Helvetica-BoldOblique findfont 150 scalefont setfont
0 0 moveto 0.88 setgray (Preliminaire) 0.70 0.78 0.95 setrgbcolor show grestore}def end}
 
%\special{!userdict begin /bop-hook{gsave 110 20 translate
%6 /Helvetica findfont 17 scalefont setfont 80 750 moveto 0.88 setgray (DOCUMENT DE TRAVAIL)
%1 0 0 setrgbcolor show grestore}def end}
 
%

\sloppy
\begin{document}
\refstepcounter{page}
\refstepcounter{page}

\renewcommand\arraystretch{1.5}

\begin{center}
{\sc
Casgam, un Code de Calcul Statistique de la D\'esexcitation Radiative 
Nucl\'eaire.

--

T. Granier
}
\end{center}

\section{Introduction}
Dans de nombreux probl\`emes de physique nucl\'eaire, la capacit\'e de calculer la d\'esexcitation
radiative des noyaux s'av\`ere cruciale. C'est en particulier le cas pour tenter de pr\'edire 
des taux de production d'isom\`eres ou encore pour estimer le spectre en \'energie et la
multiplicit\'e du rayonnement 
gamma \'emis dans une r\'eaction.

Lorsque l'\'energie d'excitation correspond au domaine discret 
exp\'erimentalement connu, le calcul de la d\'esexcitation du noyau compos\'e est imm\'ediat 
et consiste \`a suivre le diagramme constitu\'e par 
les rapports d'embranchement des niveaux entre eux. 

En revanche, pour des \'energies d'excitation plus \'elev\'ees,
la largeur naturelle des niveaux devient sup\'erieure \`a leur espacement moyen. Le spectre
en \'energie des \'etats excit\'es du noyau est continu et aucun diagramme de d\'esexcitation
n'est directement accessible \`a l'exp\'erience.

Il est cependant possible de traiter le probl\`eme d'une mani\`ere statistique en ayant recours
\`a des 
mod\`eles de densit\'e de niveaux et de coefficients de transmission radiative.
Une telle approche est d\'ej\`a ancienne mais l'affinement constant,
ces derni\`eres ann\'ees, des mod\`eles de transmission gamma et de densit\'e 
de niveaux devrait permettre des pr\'edictions plus r\'ealistes que par le pass\'e.

Un code de calcul, utilisant un choix de ces mod\`eles, a \'et\'e d\'evelopp\'e 
dans le cadre des \'etudes sur les isom\`eres entreprises \`a la {\sc dam} :
le code {\sc casgam}.
Le but du pr\'esent rapport est de d\'ecrire les ingr\'edients physiques qui le composent et d'en pr\'esenter 
quelques applications possibles.

\section{Description g\'en\'erale}
\begin{figg}[hbt]
\vskip -1cm
\begin{center}
\hskip 1cm
\epsfig{file=schemaniv.eps,height=10cm}
\end{center}
\vskip -2cm
\caption{Description des \'etats excit\'es du noyau dans le code {\sc casgam}.
Les niveaux tabul\'es dans la litt\'erature sont pris en compte jusqu'\`a l'\'energie
$E_n$. Un mod\`ele de densit\'es de niveaux est utilis\'e \`a haute \'energie, depuis
une \'energie $E_c$ jusqu'\`a l'\'energie d'excitation initiale $E$.}
\label{fig1}
\end{figg}
La m\'ethode de calcul suivie est probabiliste. Partant d'un \'etat initial du noyau,
caract\'eris\'e par une \'energie d'excitation, un spin et une parit\'e, 
la cascade de d\'esexcitation est suivie \'etape par \'etape, chaque \'etape faisant l'objet d'un tirage
selon les probabilit\'es des diff\'erentes transitions possibles. Le calcul de la 
cascade est
r\'eit\'er\'e un grand nombre de fois afin d'obtenir une moyenne statistiquement 
viable.

\bigskip
Trois ingr\'edients de base entrent dans le mod\`ele :
\begin{itemize}
\item un mod\`ele de {\it densit\'es de niveaux} permettant de d\'ecrire,
en spin, parit\'e et \'energie, le domaine des \'etats excit\'es accessibles au noyau,

\item un mod\`ele permettant de d\'eterminer la {\it probabilit\'e 
moyenne de transition} d'un \'etat donn\'e \`a un autre,

\item des {\it sch\'emas 
de niveaux} du noyau, issus de donn\'ees exp\'erimentales ou de mod\`eles th\'eoriques.
\end{itemize}

La description des \'etats du noyau est sch\'ematis\'ee \`a la figure \ref{fig1}.

L'\'etat initial est caract\'eris\'e par une \'energie d'excitation $E$, un spin
$J$ et une parit\'e $\pi$. 

\`A haute \'energie 
d'excitation, les \'etats du noyau sont d\'ecrits par 
le mod\`ele de densit\'es de niveaux jusqu'\`a l'\'energie $E_c$. 

\`A basse \'energie, la description en termes de densit\'es de niveaux fait place 
aux sch\'emas de niveaux, \'etablis {\it a priori}, depuis le fondamental jusqu'\`a l'\'energie $E_n$.
Un recouvrement des deux descriptions --~c'est-\`a-dire $E_n > E_c$~-- 
permet une transition douce entre les 
deux mod\`eles et l'utilisation de la totalit\'e des donn\'ees exp\'erimentales disponibles tout
en paliant leur incompl\'etude \`a haute \'energie.

\`A chaque pas de la cascade de d\'esexcitation, le domaine compris entre $E$ et 
$E_c$ est discr\'etis\'e en \'energie. Les couplages possibles entre l'\'etat
initial $(E,J^\pi)$ de la transition et les densit\'es d'\'etats 
de spin-parit\'e pr\'esents \`a chaque niveau
discr\'etis\'e sont calcul\'es. Il en va de m\^eme des taux de transition vers
les \'etats discrets de basse \'energie.
Les taux de transitions ainsi calcul\'es sont normalis\'es \`a l'unit\'e
afin de disposer de probabilit\'es de transition.
Un tirage permet alors de d\'eterminer l'\'etat final de la transition,
 qui peut donc \^etre, soit un 
des niveaux discrets tabul\'es donn\'es en entr\'ee, soit un \'etat purement virtuel
situ\'e dans le domaine d\'ecrit par les densit\'es de niveaux.

La d\'esexcitation est ensuite poursuivie pas \`a pas jusqu'\`a l'atteinte du
fondamental ou d'un \'etat isom\'erique du noyau.

Dans le cas o\`u la cascade transite par un niveau discret dont les rapports
d'embranchement vers les \'etats d'\'energie inf\'erieure sont inconnus,
ces derniers sont \'egalement calcul\'es \`a l'aide du mod\`ele de transmission gamma.

\section{Arri\`ere-plan th\'eorique}

Une description statistique de la d\'esex\-ci\-ta\-tion \'elec\-tro\-ma\-gn\'e\-ti\-que 
du noyau se justifie lorsque la notion de densit\'e de niveaux
 devient plus pertinente que celle d'\'etats individuels.
Un tel continuum appara\^it g\'en\'eralement pour des \'energies de quelques MeV.

Dans un tel domaine il est possible de substituer \`a la notion 
de largeur de rayonnement $\gamma$ individuelle, celle 
de force de rayonnement $\gamma$ ou << force $\gamma$ >>, d\'efinie comme
la distribution en fonction de l'\'energie du rayonnement,
de la largeur r\'eduite moyenne  pour des transitions d'un type multipolaire
donn\'e. 

La force  $\gamma$ est donc une quantit\'e moyenne, au m\^eme 
titre que les densit\'es de niveaux. L'utilisation de tels concepts pour
mener \`a bien des predictions th\'eoriques n'est donc justifi\'e 
que pour des observables physiques moyennes. Dans le cas 
pr\'esent, il s'agit de taux isom\'eriques, spectres et multiplicit\'es $\gamma$.

 
\subsection{Transmission radiative}

\subsubsection{D\'efinitions}

Pour un type de multipole XL, X correspondant au mode \'electrique (E) ou 
magn\'etique (M) et L \`a la multipolarit\'e, la force $\gamma$ est g\'en\'eralement 
d\'efinie comme le produit de la largeur radiative moyenne r\'eduite, par unit\'e 
d'intervale d'\'energie :  

$$ 
f_{\rm \scriptscriptstyle XL\scriptstyle}={<\Gamma_{\rm \scriptscriptstyle XL
\scriptstyle} (E_\gamma)>\over E_\gamma^{(2L+1)}} \rho (E)\, ,
$$
o\`u $E_\gamma$ repr\'esente l'\'energie du quantum de rayonnement,
$<\Gamma_{\rm \scriptscriptstyle XL
\scriptstyle} (E_\gamma)>$, la largeur moyenne de rayonnement,
et $\rho (E)$, la densit\'e d'\'etats \`a l'\'energie initiale $E$.

Le coefficient de transmission radiative associ\'e
s'exprime  alors par la formule suivante :

$$ T_{\gamma\rm \scriptscriptstyle XL\scriptstyle}(E_\gamma)=2\pi 
f_{\rm \scriptscriptstyle XL\scriptstyle}(E_\gamma)E_\gamma^{(2L+1)}\, .$$

\subsubsection{Mod\`eles de force $\gamma$}


Une estimation simple de la force $\gamma$ a \'et\'e obtenue en 1952 par Weisskopf
dans le mod\`ele \`a une particule~\cite{blatt}. 
Cette estimation r\'esulte en une force de rayonnement ind\'ependante de 
l'\'energie.
D\`es 1959 il apparut que cette ind\'ependance en \'energie
\'etait inadapt\'ee pour d\'ecrire les 
donn\'ees exp\'erimentales sur les largeur totales de rayonnement des r\'esonances 
neutroniques~\cite{adv}.

\bigskip
Une autre approche, propos\'ee par Brink en 1955~\cite{brink}, relie 
la largeur moyenne de photod\'esexcitation \`a la section efficace
du processus inverse, la photoabsorption.
Or, on sait que
celle-ci est domin\'ee par des r\'esonances g\'eantes dont l'origine est attribu\'ee 
aux mouvements collectifs des nucl\'eons \`a l'int\'erieur du noyau \cite{goldhaber}.
En particulier, la r\'esonance dipolaire g\'eante, observ\'ee d\`es 1947 est expliqu\'ee
comme une vibration dipolaire des liquides de neutrons et de protons dans le noyau.

Il est concevable intuitivement que ces mouvements collectifs varient peu avec l'\'energie 
d'excitation intrins\`eque du noyau. Cette hypoth\`ese, souvent r\'ef\'er\'ee comme 
l'hypoth\`ese de Brink, aboutit \`a associer \`a chaque \'etat excit\'e,
des r\'esonances g\'eantes similaires \`a celles b\^aties sur le fondamental mais 
d\'ecal\'ees en \'energie de l'\'energie de l'\'etat excit\'e.

Si la r\'esonance g\'eante est d\'ecrite par une lorentzienne standard de param\`etres connus,
ajust\'es sur les donn\'ees exp\'erimentales, 
il est alors possible de calculer la force $\gamma$ pour le type de rayonnement consid\'er\'e.

Dans le cas du  rayonnement E1,
la force $\gamma$ est alors calcul\'ee \`a partir des param\`etres lorentziens
de la r\'esonance dipolaire g\'eante.
Des travaux ult\'erieurs ont montr\'e que les rayonnements M1 et E2 \'etaient
associ\'es respectivement aux r\'esonances g\'eantes de permutation de spin et 
isoscalaire~\cite{kopecky}.

Le mod\`ele de lorentzienne standard a \'et\'e progressivement affin\'e par diff\'erents 
auteurs.
Des arguments th\'eoriques appuy\'es par des mesures exp\'erimentales
ont montr\'e la n\'ecessit\'e de faire d\'ependre la largeur  de la r\'esonance
de l'\'energie d'excitation de l'\'etat~\cite{dover}~\cite{mccullagh}.
D'autres travaux encore ont montr\'e que cette largeur devait de surcro\^it d\'ependre
de la temp\'erature nucl\'eaire de l'\'etat~\cite{kamdenskij}~\cite{zareski}.

%En outre, dans le cas du rayonnement E1,
%les donn\'ees exp\'erimentales montrent qu'il est n\'ecessaire de prendre en compte,
%pour certains noyaux, 
%l'existence d'une seconde r\'esonance g\'eante situ\'ee
%\`a plus basse \'energie (environ 5,5 MeV) et d'amplitude beaucoup plus faible,
%baptis\'ee << r\'esonance pygm\'ee >>.

\bigskip
Dans le code {\sc casgam}, le calcul des forces $\gamma$ est conforme aux recommendations 
\'etablies par l'{\sc aiea}~\cite{ripl}.
La formule suivante, publi\'ee  en 1993 par Kopecky et Chrien \cite{kopecky}, est utilis\'ee 
dans le cas d'une transition de type E1 :

$$ 
\begin{array}{cll}
f_{\rm E1}(E_\gamma)=8,68\cdot 10^{-8} \sigma_0\Gamma_0 \Biggl \{ & \hskip -0.4cm E_\gamma
\displaystyle
{\Gamma_{E_n}(E_\gamma ,T)\over(E^2_\gamma -E_0^2)^2+E_\gamma^2\Gamma^2_{E_n}(E_\gamma ,T)}  & 
\\[0.7cm]
 & \hskip 1.3cm +0,7\displaystyle{\Gamma_{E_n}(E_\gamma=0,T)\over E^3_0}\Biggr \} 
\hskip 0.2cm {\rm [mb^{-1}MeV^{-2}]}  & 
\end{array}
$$
Dans cette formule,
$\sigma_0$, $\Gamma_0$ et $E_0$ sont les param\`etres de la lorentzienne standard 
(hauteur, largeur et \'energie) ajust\'es
sur la r\'esonance dipolaire g\'eante, et
$\Gamma^2_{E_n}(E_\gamma ,T)$ est un terme de d\'ependance en
\'energie et temp\'erature ($T$) de la largeur de la r\'esonance :

$$
\Gamma^2_{E_n}(E_\gamma ,T)=\Biggl\{ k_0+(1-k_0){(E_\gamma-\epsilon_0)\over
E_0-\epsilon_0}\Biggr\} \Gamma_k(E_\gamma,T)\, ,
$$
$$
\Gamma_k(E_\gamma,T)= {\Gamma_0\over E_0^2}(E_\gamma^2+4\pi^2T^2) \, 
$$
avec pour d\'efinition de la temp\'erature nucl\'eaire $T$, 
$$
T=\sqrt{{E-E_\gamma\over a}}\, ,
$$
o\`u $a$ est le param\`etre de
densit\'e de niveaux d\'efini au \S ~suivant.
$\epsilon_0$ est une \'energie de r\'ef\'erence prise \`a 4,5 MeV et 
$k_0$ est exprim\'e suivant la param\'etrisation suivante, \'etablie empiriquement afin 
de reproduire les donn\'ees exp\'erimentales de capture r\'esonante moyenne,
les sections efficaces de capture et les spectres de photoproduction 
dans le cas de noyaux tr\`es d\'eform\'es \cite{ripl} :

$$
\begin{array}{rlrl}
k_0(x)=& 1,0 & {\rm pour \ {\it A} < 148} &\\
       & 1,0 + 0,09(A-148)^2\exp\Bigl\{-0,180(A-148)\biggr\} & {\rm pour \ {\it A} \ge 148}&
\hskip -0.2cm .
\end{array}
$$

Les param\`etres $E_0$, $\Gamma_0$ et $\sigma_0$ des r\'esonances dipolaires g\'eantes 
utilis\'es dans {\sc casgam} proviennent de l'\'evaluation \cite{sugdr}.

Certains noyaux pr\'esentent une r\'esonance dipolaire g\'eante \`a deux composantes. 
Dans ce cas, la force 
$\gamma$ utilis\'ee est une somme incoh\'erente de deux termes analogues calcul\'es 
\`a l'aide de l'expression de $f_{\rm E1}(E_\gamma)$ pr\'ec\'edente pour 
chacune des deux lorentziennes.

Le calcul des forces des rayonnements M1 et E2 se fait suivant la 
formule de lorentzienne standard :

$$
f_{\rm XL}(E_\gamma)={26\cdot 10^{-8}\over 2L+1} \sigma_0 E_\gamma^{(3-2L)}
{\Gamma_0^2\over (E_\gamma^2-E_0^2)^2+E_\gamma^2\Gamma_0^2} \ \ \ {\rm[mb^{-1} MeV^{-2}]}
$$

Dans ce cas, les param\`etres lorentziens sont obtenus \`a partir des param\`etrisations
globales des r\'esonances g\'eantes recommend\'ees par l'{\sc aiea}.

Les multipoles d'ordre sup\'erieurs sont n\'eglig\'es.

\subsection{Densit\'es de niveaux}

Il est bien connu que le nombre de niveaux cumul\'es 
se comporte, \`a basse \'energie d'excitation et pour un noyau donn\'e, de mani\`ere exponentielle 
en fonction de l'\'energie \cite{ericson}.
Cette loi, dite de << temp\'erature constante >>,  
se v\'erifie en g\'en\'eral sur les quelques premiers MeV. Au-del\`a,
les d\'eviations constat\'ees sont principalement attribu\'ees  aux limites
exp\'erimentales de la d\'etection des niveaux.

Pour les hautes \'energies d'excitation, les informations exp\'erimentales sont 
plus rares et souvent limit\'ees aux r\'esonances neutroniques et protoniques.
Au-del\`a de quelques MeV, 
on s'attend \`a ce que les densit\'es de niveaux nucl\'eaires aient un comportement
proche de celui obtenu dans les hypoth\`eses de gaz de Fermi de neutrons et protons.

La formulation de Gilbert-Cameron associe les deux lois --- loi de temp\'erature constante et
loi de gaz de Fermi ---
dans un formalisme simple.

De nombreux raffinement ult\'erieurs ont \'et\'e r\'ealis\'es mais force est de
constater que pour des \'energies inf\'erieures \`a l'\'energie de liaison neutron, 
le mod\`ele de Gilbert-Cameron employ\'e avec un jeu de param\`etres ad\'equats 
demeure l'un des plus r\'ealistes. 

\bigskip
Dans le code {\sc casgam}, trois possibilit\'es sont propos\'ees pour le calcul des densit\'es de niveaux :
\begin{itemize}
\item la formulation de Gilbert et Cameron \cite{gilcam} param\'etr\'ee par Su et coll. \cite{su},
\item une formulation de type Ignatyuk \cite{ignatyuk} de gaz de Fermi d\'ecal\'e en \'energie param\'etr\'ee
selon une approche partiellement microscopique par Hilaire~\cite{hilaire}, 
\item l'utilisation de fichiers de densit\'es de niveaux \'etablis par tout autre moyen. 
\end{itemize}

\subsubsection{Formulation de type Gilbert et Cameron}
La formulation de Gilbert et Cameron est tr\`es largement utilis\'ee dans les calculs de
r\'eaction nucl\'eaire.

Elle est compos\'ee de deux lois : une loi dite de << temp\'erature constante >> \`a basse \'energie
d'excitation
et une loi de gaz de Fermi \`a haute \'energie d'excitation.
Le point de raccordement de ces deux lois est d\'etermin\'e de mani\`ere \`a pr\'eserver 
la continuit\'e de la fonction et de sa d\'eriv\'ee par rapport \`a l'\'energie d'excitation.

Pour chaque noyau $(A,Z)$, la formulation est exprim\'ee en fonction de quatre 
param\`etres, d\'ependant soit de la charge $Z$, soit du nombre 
de neutrons $N$, et correspondant aux \'energies d'appariement  et de corrections de couches
proton et neutron : $S(Z)$, $S(N)$, $P(Z)$, $P(N)$.
L'ensemble de ces param\`etres est d\'etermin\'e 
{\it a priori} par ajustement \`a des donn\'ees exp\'erimentales.

\bigskip
La formulation composite de la densit\'e totale de niveaux \`a une \'energie d'excitation $E$ 
s'\'ecrit :
\begin{itemize}
\item \`a basse \'energie d'excitation :
$$
\rho_1(E)={1\over T_0}\exp{E-E_0\over T_0}
$$
o\`u $T_0$ est la temp\'erature nucl\'eaire constante et $E_0$, un param\`etre homog\`ene \`a une \'energie,
\item \`a haute \'energie d'excitation : 
$$
\rho_2(E)={\sqrt{\pi}\over 12}{\exp(2\sqrt{aU})\over a^{1/4}U^{5/4}}
{1\over\sqrt{2\pi}\sigma}\,  
$$
o\`u $U$ est l'\'energie d'excitation corrig\'ee des \'energies d'appariement proton et neutron :
$U=E-P(Z)-P(N)$, et $a$ est param\'etr\'e empiriquement en fonction de $A$ et des corrections 
de couches :
$$
{a\over A}=\xi_1\bigl(S(Z)+S(N)\bigr)+\xi_2\, .
$$
\end{itemize}
La densit\'e de niveaux de spin $J$ \`a une \'energie donn\'ee est le produit de la densit\'e totale
\`a cette \'energie par un terme de d\'ependance de spin $F(J)$ qui n'est autre 
qu'une distribution gaussienne 
de largeur $\sigma$ :
$$
F(J)={(2J+1)\over 2\sigma^2}\exp{-(J+1/2)^2\over2\sigma^2})
$$
Le param\`etre de d\'ependance en spin $\sigma$ varie avec l'\'energie d'excitation suivant
 l'expression empirique :
$$
\sigma^2=0,0888\sqrt{aU}A^{2\over 3}
$$
Dans cette approche, les parit\'es sont \'equiprobables.

\bigskip
Le point d'ajustement $E_x$ entre les deux formules est donn\'e par une loi empirique, 
fonction de la masse
du noyau :
$$
U_x=E_x-P(Z)-P(N)=\eta_1+{\eta_2\over A}\, ,
$$
de sorte que les valeurs de $T_0$ et $E_0$ dans l'expression de $\rho_1$ 
sont d\'etermin\'ees \`a $E=E_x$ :
$$
\begin{array}{l}
T_0=\tau(U_x)\, , \\
E_0=E_x-T_0\log T_0\cdot\rho_2(U_x)\, ,
\end{array}
$$
o\`u $\tau(U)$, la temp\'erature nucl\'eaire, est d\'efinie comme la d\'eriv\'ee par rapport
\`a l'\'energie de la densit\'e de niveaux :
$$
\tau(U)={\partial \rho_2\over \partial U}=\sqrt{{a\over U}}-{3\over 2}U \, .
$$

\bigskip
Le code {\sc casgam} utilise le jeu de param\`etres d\'etermin\'es par Su Zongdi et collaborateurs
par ajustement aux donn\'ees exp\'erimentales 
sur l'espacement moyen et la largeur de capture radiative \`a $B_n$
et le nombre cumul\'e de niveaux \`a basse \'energie \cite{su}.

Avec ces param\`etres, les valeurs des coefficients empiriques $\xi_1$, $\xi_2$, $\eta_1$ et
$\eta_2$
dans les expressions du param\`etre $a$ et de l'\'energie de raccordement $U_x$
sont les suivantes \cite{su} :
$$
\begin{array}{cll}
\xi_1= & 0,00880 & \\[0.3cm]
\xi_2=   & 0,142 \;\; {\rm pour :}  &\bullet\;\;  29\le Z\le 62 \;\; ({\rm \/ et} \: N \le 89) \\
   &                        & \bullet\;\; 79\le Z\le 85 \\[0.3cm]
\xi_2=   & 0,120 \;\;  {\rm pour :} & \bullet\;\; Z<28 \\
   &                        & \bullet\;\; 62 \;\; ({\rm et }N>89) \le Z \le 78     \\
   &                        & \bullet\;\; Z\ge86     \\[0.3cm]
\eta_1=&1,4 & \\[0.3cm]
\eta_2=&263 & 
\end{array}
$$

\subsubsection{Formule de type Ignatyuk param\'etr\'ee par Hilaire}

Le mod\`ele ph\'enom\'enologique de densit\'es de niveaux d'Ignatyuk est 
\'egalement une formulation de gaz de Fermi. La densit\'e  totale \`a une 
\'energie d'excitation $E$ s'\'ecrit :
$$
\rho(U)={\sqrt{\pi}\over 12}{\exp{(2\sqrt{aU})}\over a^{1/4}U^{5/4}}
$$
o\`u $U$ est l'\'energie d'excitation corrig\'ee de l'\'energie d'appariement $\Delta$ dont 
l'expression est :
$$
\Delta=\chi{12\over\sqrt{A}}
$$
La principale diff\'erence avec la formule 
\`a haute \'energie de Gilbert-Cameron est l'expression du param\`etre $a$
qui acquiert une d\'ependance dans l'\'energie d'excitation :
$$
a(U)=\tilde{a}\biggl\{1+f(U){\delta W\over U}\biggr \}\, .
$$
Dans cette expression, $\tilde{a}$ repr\'esente la valeur asymptotique de $a$ \`a haute \'energie,
$\delta W$ est un terme d'effet de couches \'egal \`a la diff\'erence entre la masse
exp\'erimentale du noyau et sa masse calcul\'ee dans le mod\`ele de la goutte liquide ;
le terme $f(U)$ a pour expression :
$$
f(U)=1-\exp{(-\gamma U)}
$$
o\`u $\gamma$ est un param\`etre ajust\'e.
La valeur asymtotique $\tilde{a}$ du param\`etre de gaz de Fermi est param\'etr\'ee
empiriquement en fonction de $A$.

Dans l'approche d'Hilaire, disponible dans le code {\sc casgam}, la formule
d'Ignatyuk est utilis\'ee pour d\'ecrire les densit\'es de niveaux intrins\`eques,
c'est-\`a-dire hors effets collectifs. Celles-ci sont d\'etermin\'ees en 
d\'enombrant les \'etats d'excitation particules-trous b\^atis sur les niveaux
de quasi-particules calcul\'es dans une approche microscopique de type
Hartree-Fock-Bogolioubov avec la force effective de Gogny.

Les param\`etres de la formule d'Ignatyuk obtenus dans cette approche sont les
suivants :
$$
\gamma=2,113\cdot\tilde{a}A^{-4/3}\, ,
$$
$$
\tilde{a}=0,0482A+0,123A^{2/3}\, .
$$

Les \'etats collectifs du noyau -- vibrationnels et rotationnels -- sont eux pris en compte 
par des << coefficients d'accroissement >>, $K_{\rm vib}$ et $K_{\rm rot}$, 
mis en facteur des densit\'es intrins\`eques
et dont les expressions sont appuy\'ees partiellement par des consid\'erations microscopiques :
$$
K_{\rm vib}(U)=\exp\Biggl[\alpha A^{2/3}\biggl({U\over a}\biggr)^{2/3}\Biggr]
$$
$$
K_{\rm rot}(U)=9,65\cdot 10^{-3} r_0^2 A^{5/3} \sqrt{{U\over a}}\, .
$$
La disparition des effets collectif \`a tr\`es haute \'energie d'excitation
est prise en compte par la mise en facteur d'une forme analytique $q(U)$ d\'etermin\'ee
empiriquement :
$$
q(U)=1+0,43147\cdot\exp(0,39522\cdot\delta W)\, .
$$

La d\'ependance en spin est d\'ecrite par la m\^eme fonction $F(J)$
que dans le mod\`ele de Gilbert-Cameron avec un param\`etre $\sigma$
de la forme :
$$
\sigma^2=7,71\cdot 10^{-3}r_0^2A^{5/3}\sqrt{U\over A}\, .
$$

Dans ce mod\`ele, les parit\'es sont \'equiprobales pour un spin et
une \'energie donn\'es.

La densit\'e de niveau effective s'\'ecrit donc :
$$ 
\rho(U,J,\pi)={1\over 2} \times q \times K_{\rm rot}\times K_{\rm vib}\times \rho_{\rm int}\times F(J)\, .
$$

\section{Applications du code}

\section{Conclusion}

\newpage
\begin{thebibliography}{99}
\bigskip
 
\bibitem{blatt} J.M. Blatt et V.F. Weisskopf, Theoretical Nuclear Physics, Wiley (1952)

\bibitem{adv} G.A. Bartholomew, Advances in Nuclear Physics {\bf 6}, p 229 Plenum Press (1973)

\bibitem{brink} D.M. Brink, Th\`ese de Doctorat, universit\'e d'Oxford (1955)

\bibitem{goldhaber} M. Goldhaber et E. Teller, Phys Rev {\bf 74}, 1046 (1948) 

\bibitem{dover} C.B. Dover {\it et al.} Ann. Phys. {\bf 70}, 458 (1972)

\bibitem{mccullagh} C. McCullagh, M. Stelts et R. Chrien, Phys. Rev. C {\bf 23},1394 (1982)

\bibitem{kamdenskij} S.G. Kadmenskij, V.P. Markushev et V.I. Furman, Sov. J. Nucl. Phys.
{\bf 37}, 165 (1983)

\bibitem{zareski} V.K. Sirotkin, Sov. J. Nucl. Phys. {\bf 43} 362 (1986)

\bibitem{kopecky} J. Kopecky, M. Uhl, et R. E. Chrien, Phys. Rev. {\bf C47}, 312 (1993)

\bibitem{ripl} J. Kopecky (coordonnateur) Reference Input Parameter Library, Handbook for calculation
of nuclear reaction data p 97 (1998)

\bibitem{ericson} T. Ericson, Nucl. Phys. {\bf 11}, 481 (1959)

\bibitem{sugdr} Su GDR

\bibitem{gilcam} A. Gilbert, A.G.W. Cameron, Can. J. Phys. {\bf 43} 1446 (1965)

\bibitem{su} Su Zongdi, Wang Cuilan, Zhuang Youxiang, Zhou Chunmei,
rapport AIEA INDC(CPR)-2, mai 1985

\bibitem{ignatyuk} A.V. Ignatyuk, G.N. Smirenkin et A.S. Tishin, Sov. J. Nucl. Phys. {\bf 21}, 255
(1975)

\bibitem{hilaire} S. Hilaire, th\`ese de doctorat, {\sc inpg} Grenoble (1997)




\end{thebibliography}

\end{document}
