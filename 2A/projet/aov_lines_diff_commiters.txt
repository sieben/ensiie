Call:
   aov(formula = data_merged$lines ~ data_merged$diff_commiters)

Terms:
                data_merged$diff_commiters   Residuals
Sum of Squares                  4077014329 14982528443
Deg. of Freedom                          1       34294

Residual standard error: 660.9726 
Estimated effects may be unbalanced
