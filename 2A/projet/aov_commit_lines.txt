Call:
   aov(formula = commit ~ lines)

Terms:
                   lines Residuals
Sum of Squares   9278625  27052163
Deg. of Freedom        1     34294

Residual standard error: 28.08613 
Estimated effects may be unbalanced
