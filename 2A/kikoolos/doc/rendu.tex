\documentclass[a4paper,11pt]{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Packages
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{fontspec}
\usepackage{xunicode}
\usepackage{xltxtra}
\usepackage{polyglossia}

\setmainfont[Mapping=tex-text]{Gentium Basic}

\setdefaultlanguage{french}

\usepackage{hyperref}
\usepackage{fancyhdr}
\usepackage{geometry}
\usepackage{verbatim}
\usepackage{algorithm}
\usepackage{algorithmic}
\pagestyle{plain}

\hypersetup{
    backref=true, %permet d'ajouter des liens dans...
        hyperindex=true, %ajoute des liens dans les index.
        colorlinks=true, %colorise les liens
        breaklinks=true, %permet le retour à la ligne dans les liens trop longs
        urlcolor= blue, %couleur des hyperliens
        linkcolor=black, %couleur des liens internes
        bookmarks=true, %créé des signets pour Acrobat
        bookmarksopen=true, %si les signets Acrobat sont créés,
        %les afficher complètement.
            pdftitle={Kikoolos}, %informations apparaissant dans
            pdfauthor={Remy Leone, Marc van der Wal}, %dans les informations du document
            pdfsubject={kernel, os, ensiie} %sous Acrobat.
}

\makeatletter
\renewcommand{\ALG@name}{Algorithme}
\makeatother

\newcommand{\manNom}[2]{\paragraph{Nom} \texttt{#1} -- #2}
\newcommand{\manSynopsis}{\paragraph{Synopsis}}
\newcommand{\manDescription}{\paragraph{Description}}
\newcommand{\manRef}[2]{\textit{#1}(#2)}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% HEADER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{ISE 2011 - Projet minikernel}
\author{Rémy Léone / Marc van der Wal}
\date{\today}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DOCUMENT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\maketitle

\tableofcontents

\section*{Introduction}

Ce projet, initié dans le cadre de l'enseignement de Systèmes d'Exploitation, a
pour but de construire un noyau de système d'exploitation pouvant mettre en
évidence les caractéristiques basiques d'un noyau moderne, tels que la gestion
de la mémoire, l'ordonnanceur, et le lancement de processus.


\subsection*{Contexte général}
% Que fait l'outil

KikoolOS est un noyau minimaliste destiné à être implanté au sein de systèmes
d'exploita\-tion minimalistes conçues pour l'architecture Intel 32 bits
(\textit{i386}), sur du matériel compatible PC.  Il se lance entièrement en
mémoire, n'utilise pas de systèmes de fichiers et est capable de lancer
simultanément entre zéro et quatre programmes fournis par l'utilisateur. 

\subsection*{Cas d'utilisation}
% Quelle utilisation ?

Ce noyau est parfaitement adapté à l'utilisation au sein d'un système disposant
de peu de ressources matérielles; par exemple, un système embarqué ayant un
nombre de tâches limité à faire, disposant d'une mémoire réduite et de peu de
périphériques.  Par exemple, une utilisation en tant que système de contrôle
d'un robot simple pourrait être envisagée.

\subsection*{Avantages et inconvénients}

Ce noyau étant beaucoup plus petit que les noyaux libres les plus utilisés
(Linux, \ldots), il dispose de nombre de propriétés intéressantes. En effet, il
est beaucoup plus léger, plus maintenable, plus simple à comprendre.  Sa taille
réduite, tournant autour des 30 ko, en est le témoin.

Cependant, il est également plus rudimentaire, il dispose de drivers pour
presque aucun périphérique (hormis l'écran, le clavier et les ports série),
n'est pas optimisé pour la performance et n'a pas subi d'audits de sécurité
réguliers.

En somme, ce noyau peut être utilisé dans des systèmes très spécifiques où
la légèreté et la simplicité sont primodiaux.

\section{Aperçu des fonctionnalités principales}

La construction de ce noyau est organisé autour des différentes tâches d'un
noyau de système d'exploitation:

\begin{description}

\item[Gestion de la mémoire :]
Le noyau utilise exclusivement les mécanismes de pagination du processeur.
L'espace virtuel d'adressage de chaque processus est partagée en une zone
utilisée par le noyau et une zone allouée au processus utilisateur en question.

\item[Processus :] L'ordonnanceur est un ordonnanceur simple, qui accorde
	des ressources et un temps CPU égaux à chaque processus en utilisant un
	algorithme \textit{round-robin}.  Celui-ci se repose sur
	l'interruption du PIT (IRQ 0) comme base d'horloge; celui-ci étant
	initialisé de telle sorte qu'une telle interruption survienne toutes
	les millisecondes.

\item[Appels systèmes et bibliothèques :] Une batterie d'appels systèmes
	basiques, ainsi qu'un certain nombre de fonctions de la \textit{libc},
	sont implémentés afin de fournir aux processus utilisateurs un nombre
	suffisant de fonctions standard, comme afficher des chaînes de
	caractères à l'écran, ou lire le clavier.  L'ensemble des appels et
	fonctions disponibles sont fournies avec leur page de \textit{man}
	correspondante.

\item[Exécution de programmes en mode utilisateur :] Il est possible d'exécuter
	simultanément jusqu'à quatre programmes compilés au format standard ELF.
	Les exécutables doivent néanmoins être générés à l'aide d'un compilateur
	spécifique, \manRef{kgcc}{1},

En pratique, afin de garder une image monolithique, ces exécutables sont
placés à la fin de l'image finale du noyau.  Une table interne au noyau
permet de garder trace de l'endroit et de la taille des exécutables à lancer
simultanément.  

\item[Shell :] Un shell minimaliste est implémenté directement dans le noyau,
permettant à l'u\-ti\-li\-sa\-teur d'interagir avec le système, en utilisant les
commandes suivantes: 

\begin{description}
	\item[s \textit{numéro}] Démarre (\textit{start}) le processus
		identifié par le numéro \textit{numéro}, compris entre 1 et 4.
	\item[b \textit{numéro}] Stoppe (\textit{break}) le processus
		\textit{numéro}.
	\item[r \textit{numéro}] Redémarre (\textit{restart}) le processus
		\textit{numéro}, ce qui revient à enchaîner les commandes
		\texttt{b} et \texttt{s}.
\end{description}

\end{description}

\section{Manuel}

Dans cette section et les suivantes, les commandes, appels système, fonctions
ou autres dont il existe une page de manuel sont notées en \textit{italique},
avec un suffixe numérique entre parenthèses correspondant à la section de
\textit{man} dans laquelle elle serait rangée.  \textbf{Exemple :}
\manRef{printf}{3}.

% \subsection{Installation / Prérequis}


\subsection{Vue générale}
% Qu'est ce que l'on voit à l'écran

L'écran est découpé en plusieurs parties; celles-ci étant, de haut en bas:

\begin{itemize}
\item L'état du système (uptime, les processus en cours, \ldots)
\item Un prompt de shell
\item Quatre fenêtres (organisées en deux lignes de deux colonnes) contenant la
	sortie standard des processus lancés
\end{itemize}

Une seule fenêtre aura toujours le ``focus'', c'est-à-dire que les frappes au
clavier seront dirigées vers cette fenêtre.  Des combinaisons de
touches permettront de changer la fenêtre ayant le focus; les raccourcis
\textit{Alt-Échap}, pour le shell, ainsi que \textit{Alt-F1} à \textit{Alt-F4}
pour les fenêtres des processus, ont été réservées pour cet usage. 

\subsection{Génération d'un système}
%fournir un kikoolos-gcc (abrégé kgcc ~ Ca fait un peu kernel compiler XD )
% L'objectif est de créer des applications dans le format attendu par KikoolOS
% La problématique générale c'est qu'est ce que ça génère


% le \textit{make}(1) permet de mettre l'accent sur le fait que c'est une
% commande unix, dont le man est visible avec "man 1 make".
Le code source de Kikoolos dispose d'un Makefile qui permet de fabriquer un
fichier de lancement du kernel.  À l'aide des outils classiques comme
\textit{make}(1), il est possible de fabriquer l'image du système adapté à la
machine.

Un fichier de configuration permettra de contrôler quels sont les exécutables
qui seront inclus dans l'image du noyau.

Lors de la génération du système, l'utilisateur fournira des programmes C.  Ces
fichiers C seront alors compilés par un compilateur inclus dans la
distribution, \emph{kgcc}(1), qui produira des exécutables au format ELF, adaptés
au système.  Ce compilateur utilisera le compilateur \emph{gcc}(1).

\subsection{Lancement du système}
%- Disquette, GRUB, etc.

Il sera possible de générer plusieurs supports d'installation.  Fichiers
binaires bruts, images de disquette ou CD-ROM bootables figurent parmi les
possibilités. L'image créée est ensuite chargée sur une disquette (ou n'importe
quelle autre support permettant d'amorcer le système) et il sera possible de
démarrer dessus.

\subsection{Utilisation avancée de KikoolOS}

\subsubsection{Options avancées du compilateur}

\manRef{kgcc}{1} reconnaît certaines options de \manRef{gcc}{1}, en particulier
les options -c, -S, -E et -o, pour contrôler la sortie du compilateur.
Consulter la page de manuel de \manRef{kgcc}{1} pour plus de détails.

\subsubsection{Format de l'image}

\begin{table}[htp]
	\centering
\begin{tabular}{|c|c|c|c|c|c|}
\hline
Boot & Setup & Noyau & Table & P1 & \ldots \\
\hline
\end{tabular}
	\caption{Format de l'image du système}
	\label{table:imgformat}
\end{table}

Le format de l'image du système est donné dans la table \label{table:imgformat}.
Voici les caractéristiques des différents champs:
\begin{description}
	\item[Boot] (512 octets) secteur de boot
	\item[Setup] (2048 octets) setup: mise en place d'un état connu, des
		registres de segments, IDT, GDT, etc.
	\item[Noyau] (taille variable) Image du noyau proprement dit
	\item[Table] (32 octets) table contenant les adresses dans
		l'image, ainsi que la taille, des exécutables des programmes qui
		doivent être lancés.
	\item[P1] (taille variable) image du processus 1, suivi des images des
		processus 2 à 4, si applicable.
\end{description}

La taille maximale de l'image est de 1440 ko.

La table des processus sera simplement du format suivant:

\begin{center}
	offset\_P1 taille\_P1 offset\_P2 taille\_P2 \ldots
\end{center}

Chaque valeur (offset et taille) est donnée dans un entier codé sur 32 bits non
signé.  Si moins de quatre processus doivent être lancés, les enregistrements
``offset'' et ``taille'' des autres processus sont mis à zéro.


\subsubsection{Carte mémoire}


\paragraph{Mapping de la mémoire physique}

L'organisation prévue de la mémoire physique est donnée dans la table
\ref{table:orga_phys}.

\begin{table}[htp]
	\centering
	\begin{tabular}{r|c|}
		\hline \texttt{0x0} & ~ \\
		~ & Mémoire système \\
		\texttt{0x1fffff} & ~ \\
		\hline \texttt{0x200000} & ~ \\
		~ & Mémoire processus 1 \\
		\texttt{0x3fffff} & ~ \\
		\hline \texttt{0x400000} & ~ \\
		~ & Mémoire processus 2 \\
		\texttt{0x5fffff} & ~ \\
		\hline \texttt{0x600000} & ~ \\
		~ & Mémoire processus 3 \\
		\texttt{0x7fffff} & ~ \\
		\hline \texttt{0x800000} & ~ \\
		~ & Mémoire processus 4 \\
		\texttt{0x9fffff} & ~ \\
		\hline
	\end{tabular}
	\caption{Organisation de la mémoire physique}
	\label{table:orga_phys}
\end{table}

\paragraph{Mapping de la mémoire virtuelle de chaque processus}

L'organisation prévue de la mémoire virtuelle par processus est donnée dans la
table \ref{table:orga_virt}.

\begin{table}[htp]
	\centering
	\begin{tabular}{r|c|}
		\hline \texttt{0x0} & ~ \\
		~ & \textit{Réservé} \\
		\texttt{0x7fffff} & ~ \\
		\hline \texttt{0x800000} & ~ \\
		~ & Mémoire processus \\
		\texttt{0x9fffff} & ~ \\
		\hline \texttt{0xc0000000} & ~ \\
		~ & Mémoire noyau \\
		\texttt{0xc0200000} & ~ \\
		\hline
	\end{tabular}
	\caption{Organisation de l'espace d'adressage virtuel de chaque processus}
	\label{table:orga_virt}
\end{table}


\section{Implémentation}

\subsection{Gestion de l'espace virtuel et physique}

La gestion de l'espace virtuel et physique de chaque processus est très
simpliste.  En effet, chaque processus se voit attribuer exactement 2 Mo de
mémoire physique.  Lors de la création de celui-ci, le contenu du fichier
exécutable ELF est copié dans l'emplacement mémoire où celui-ci va être exécuté,
à l'aide d'un chargeur ELF.  Le pointeur de pile sera initialisé afin de pointer
sur la fin de l'espace physique alloué.

L'espace virtuel, quant à lui, est géré à l'aide du MMU du processeur, et de son
système de pagination.  Celui-ci est mis en place immédiatement après le
rechargement de la GDT, l'initialisation de l'IDT, et du TSS utilisé par le
noyau.

\subsection{Algorithme d'ordonnancement}

L'ordonnanceur garde une trace de chaque processus à l'aide d'une liste
doublement chaînée de structures particulières, décrivant l'état d'un processus.
Le mécanisme des listes est empruntée à celle du noyau Linux.  Les descripteurs
de processus, eux, sont une structure C qui permet au noyau de garder trace,
entre autres, de:
\begin{itemize}
	\item son numéro (PID);
	\item la valeur de tous les registres lors de la commutation, afin de
		pouvoir restaurer le con\-texte d'un processus lorsqu'on commute
		sur celui-ci;
	\item son état d'exécuation (démarré, en attente d'une entrée, quantum
		CPU expiré, etc.);
	\item son état d'ordonnancement (commutable, non commutable);
	\item le quantum de CPU restant avant la commutation;
	\item la fenêtre d'écran utilisée pour la sortie;
	\item sa capacité à recevoir l'entrée du clavier (i.e. son focus).
\end{itemize}

Ces informations sont susceptibles d'être modifiées non seulement par
l'ordonnanceur, mais également par d'autres fonctions du noyau; en particulier,
la gestion de l'entrée du clavier, l'appel système \manRef{read\_vt}{2}, ou les
fonctions de création et de destruction de processus.

L'ordonnanceur est implémenté sous la forme d'une fonction \texttt{schedule()},
qui est appelée à chaque tic d'horloge.  L'ordonnancement se fait avec un simple
algorithme \emph{round-robin}, qui est donné dans l'algorithme 1.

\begin{algorithm}
\begin{algorithmic}
\STATE \COMMENT{Le quantum $q$ est le même pour tous les processus.}
\IF{aucune tâche ne tourne}
	\STATE{sortir}
\ELSIF{le processus courant a encore un quantum CPU non nul}
	\STATE{décrémenter le quantum}
	\STATE{sortir}
\ELSE
	\STATE{$i \leftarrow \mbox{PID du processus}$}
	\STATE{sauvegarder tous les registres du processus $i$}
	\STATE{marquer le processus $i$ comme arrêté et non commutable}
	\IF{aucun processus n'est commutable}
		\STATE{marquer tous les processus comme commutables}
	\ENDIF
	\FORALL{processus $j$}
		\IF{$j$ est commutable}
			\STATE{restaurer les registres de $j$}
			\STATE{réinitialiser le quantum CPU alloué à $j$, à $q$}
			\STATE{marquer $j$ comme étant en cours d'exécution}
			\STATE{commuter vers $j$}
		\ENDIF
	\ENDFOR
\ENDIF
\end{algorithmic}
\caption{Algorithme d'ordonnancement du noyau}
\end{algorithm}



\subsection{Fonctionnement des appels système}

Les appels système sont implémentés avec un mécanisme similaire à celui de
Linux.  Con\-crè\-tement, pour effectuer un appel système depuis un processus
utilisateur, il faut:
\begin{itemize}
	\item charger le registre \verb+%eax+ avec le numéro d'index
		correspondant à l'appel système choisi;
	\item activer l'interruption 128 (\verb+int 0x80+).
\end{itemize}

En pratique, depuis un programme tournant en mode utilisateur, il faudra inclure
le fichier d'en-têtes \texttt{unistd.h} fourni, afin de pouvoir effectuer ces
appels à la C.

Du point de vue du noyau, l'IDT est initialisée de telle sorte que
l'interruption 128 pointe vers la fonction \verb+_asm_syscall+, récupérant la
valeur du registre \verb+%eax+, et appelle la fonction C
\verb+do_syscall()+.  C'est cette dernière fonction qui va exécuter, selon ce
numéro, l'appel système proprement dit, pour ensuite renvoyer la valeur de
retour de l'appel.


\section{Pages de manuel}

\subsection{Section 1 -- Commandes utilisateur}

\subsubsection{kgcc}

\manNom{kgcc}{Compilateur C pour KikoolOS}

\manSynopsis

\begin{verbatim}
kgcc [-v] [-c|-S|-E] [-Idir...] [-Ldir...] [-Dmacro[=defn]...]
     [-Umacro] [-o outfile] infile...
\end{verbatim}

\manDescription

\manRef{kgcc}{1} est un script servant de coquille par-dessus \manRef{gcc}{1}, et
permettant de générer des fichiers binaires exécutables au format ELF. 

Cette génération se fait en quatre étapes (pré-compilation, compilation,
assemblage et édition de liens), et il est néanmoins possible d'imposer
\manRef{kgcc}{1} de s'arrêter à la fin d'une étape de la compilation.

\paragraph{Options}

\begin{description}
	\item[-v] Mode verbeux: affiche les commandes exécutées pour chaque
		étape de la compilation.
	\item[-c] Pré-compiler, compiler et assembler, mais ne pas faire d'édition de liens
	\item[-S] Pré-compiler et compiler, mais ne pas assembler
	\item[-E] Pré-compiler uniquement
	\item[-I\textit{dir}] Ajoute le répertoire \textit{dir} à la liste des
		répertoires recherchés lors de l'inclusion d'un fichier avec
		\texttt{\#include}.
	\item[-L\textit{dir}] Ajoute le répertoire \textit{dir} à la liste des
		répertoires recherchés lors de l'édition de liens.
	\item[-D\textit{macro}\char91=\textit{defn}\char93] Définit la macro
		\textit{macro}, et lui donne la valeur \textit{defn} (1 si
		\textit{defn} n'est pas donné).
	\item[-U\textit{macro}] Annule toute définition précédente de la macro
		\textit{macro}.
\end{description}

\paragraph{Bugs}

KikoolOS ne fournit aucun mécanisme d'édition de liens dynamiques; c'est pourquoi
elle est systématiquement faite en statique par défaut.

\subsection{Section 2 -- Appels système}

\subsubsection{\_exit}

\manNom{\_exit}{Met fin à l'exécution du processus courant}

\manSynopsis
\begin{verbatim}
#include <unistd.h>

void _exit()
\end{verbatim}

\manDescription \manRef{\_exit}{2} met immédiatement fin à l'exécution du
processus courant.

\paragraph{Remarque} Cette fonction ne retourne jamais.

\subsubsection{write\_vt}

\manNom{write\_vt}{Primitive d'affichage d'une chaîne de caractères fixe}
\manSynopsis
\begin{verbatim}
#include <unistd.h>

size_t write_vt(char* sz)
\end{verbatim}

\manDescription

\manRef{write\_vt}{2} ordonne au noyau d'afficher la chaîne de caractères
débutant à l'em\-pla\-ce\-ment \textit{sz}, et terminé par l'octet zéro, sur la
sortie standard du programme appelant.

\paragraph{Valeur de retour}

En cas de réussite, le nombre d'octets imprimée sur la sortie standard est
renvoyée.  En cas d'erreur, la valeur $-1$ est renvoyée.

\paragraph{Voir aussi}

\manRef{read\_vt}{2},
\manRef{printf}{3}

\subsubsection{read\_vt}

\manNom{read\_vt}{Primitive de lecture depuis l'entrée standard}
\manSynopsis
\begin{verbatim}
#include <unistd.h>

size_t read_vt(char* buf, size_t nmemb)
\end{verbatim}

\manDescription

\manRef{read\_vt}{2} lit jusqu'à \textit{nmemb} octets depuis l'entrée
standard, et les place à l'em\-pla\-ce\-ment mémoire débutant à 
\textit{buf}.  La lecture est faite caractère par caractère dans un tampon
temporaire, jusqu'à avoir atteint \textit{nmemb} caractères, ou jusqu'au
prochain retour chariot.

Si \textit{nmemb} est supérieur à 256, seuls les 256 premiers octets seront
renvoyés.

\paragraph{Valeur de retour}

En cas de réussite, le nombre d'octets lus depuis l'entrée standard est
renvoyée.  En cas d'erreur, la valeur $-1$ est renvoyée.

\paragraph{Voir aussi}

\manRef{read\_vt}{2},
\manRef{scanf}{3}


\subsection{Section 3 -- Fonctions de bibliothèque}

\subsubsection{getchar, gets}

\manNom{getchar, gets}{Entrée de caractères et de chaînes de caractères}

\manSynopsis
\begin{verbatim}
#include <stdio.h>

int getchar(void);
char* gets(char* buf);
\end{verbatim}

\manDescription \manRef{getchar}{3} lit le caractère suivant de l'entrée
standard et la renvoie en tant que \textit{un\-signed char} casté vers un
\textit{int}, ou $-1$ en cas d'erreur.

\manRef{gets}{3} lit une ligne de l'entrée standard dans un tampon pointé
par \textit{buf}, jusqu'au prochain retour chariot.  Aucun test de débordement
de tampon n'est effectué (voir \textbf{Bugs}).

\paragraph{Bugs}

N'utilisez jamais \manRef{gets}{3}.  Puisqu'il est impossible de connaître en
amont le nombre de caractères lus par cette fonction, et qu'elle continue
d'écrire des caractères après la fin du tampon, il est extrêmement dangereux de
s'en servir.


\subsubsection{printf, sprintf, snprintf, vprintf, vsprintf, vsnprintf}

\manNom{printf, sprintf, snprintf, vprintf, vsprintf, vsnprintf}
	{Formatage des sorties}

\manSynopsis
\begin{verbatim}
#include <stdio.h>

int printf (const char *format, ...);
int sprintf (char *str, const char *format, ...);
int snprintf (char *str, size_t size, const char *format, ...);

#include <stdarg.h>

int vprintf (const char *format, va_list ap);
int vsprintf (char *str, const char *format, va_list ap);
int vsnprintf (char *str, size_t size, const char *format, va_list ap); 
\end{verbatim}

\manDescription

Les fonctions de la famille \textit{printf} produisent des sorties en accord
avec le \texttt{format} décrit plus bas.  \manRef{printf}{3} et \manRef{vprintf}{3}
écrivent sur la sortie standard.  \manRef{sprintf}{3}, \manRef{snprintf}{3},
\manRef{vsprintf}{3} et \manRef{vsnprintf}{3} écrivent leur résultat dans la chaîne
\texttt{str}.

\manRef{snprintf}{3} et \manRef{vsnprintf}{3} prennent en argument
supplémentaire \textit{size}, la longueur maximale du tampon dans laquelle ils
écrivent.  Il est vivement conseillé de les préférer à \manRef{sprintf}{3} et
\manRef{vsprintf}{3}, respectivement, afin d'éviter des dépassements de tampon.


\paragraph{Chaîne de format}

La chaîne de format est une chaîne de caractères contenant des
\emph{spécificatifs de converstion}, commençant avec le caractère \%.


Le caractère \% est suivi de zéro ou au moins un drapeau parmi les suivants:

\begin{description}
	\item[0] La valeur doit être remplie avec des zéros à gauche, jusqu'à atteindre
		la largeur du champ.
	\item[$-$] La valeur convertie est alignée à gauche au lieu de droite (le comportement
		par défaut).  Elle est remplie à droite avec des espaces, au lieu que ce
		soit fait à gauche.
	\item[+] Le signe ($+$ ou $-$) sera toujours placée avant la valeur, même si elle
		est positive.
\end{description}

Ensuite, elle est suivie d'une longueur de champ, optionnelle, indiquant une
longueur minimale.  Un dépassement de cette longueur n'entraîne en aucun cas
une troncature du champ; dans ce cas-là, elle sera étendue pour pouvoir
contenir la valeur.

Enfin, elle est terminée avec un et un seul \emph{spécificateur de conversion},
obligatoire, parmi les suivants:

\begin{description}
	\item[d, i] L'argument sera imprimé en tant qu'entier en base 10.
	\item[x, X] L'argument sera imprimé en tant qu'entier en base 16.  Les
		chiffres $a$, $b$, $c$, $d$, $e$ et $f$ minuscules sont
		utilisés pour représenter les chiffres valant 10 à 15.
	\item[c] L'argument sera imprimé en tant que caractère, dont le code (ASCII)
		est donné par la valeur de l'argument.
	\item[s] L'argument sera imprimé en tant que chaîne de caractère.  Les
		caractères de cette chaîne seront copiés jusqu'au premier
		caractère zéro rencontré (non inclus).
\end{description}

\paragraph{Exemple}

Pour imprimer la réponse à la question de la Vie, de l'Univers et du Reste :
\begin{verbatim}
#include <stdio.h>
printf("la réponse est %d", 42);
\end{verbatim}

\subsubsection{putchar, puts}

\manNom{putchar, puts}{Sortie de caractères et de chaînes}

\manSynopsis 
\begin{verbatim}
#include <stdio.h>

int putchar(int c);

int puts(const char *s);
\end{verbatim}

\manDescription

\textit{putchar()} écrit le caractère \textit{c}, casté vers un \textit{unsigned
char}, sur la sortie standard.

\textit{puts()} écrit la chaîne \textit{s} sur la sortie standard, suivie d'un
retour à la ligne.

\paragraph{Valeur de retour}

\textit{putchar()} renvoie le caractère écrit, casté vers un \textit{int}, ou
$-1$ en cas d'erreur.

\textit{puts()} renvoie un nombre positif ou nul en cas de succès, ou $-1$ en
cas d'erreur.

\paragraph{Voir aussi}

\manRef{write\_vt}{2}, \manRef{gets}{3}, \manRef{scanf}{3}

\subsubsection{scanf, sscanf, vscanf, vsscanf}

\manNom{scanf, sscanf, vscanf, vsscanf}{Formatage et conversion d'entrée}

\manSynopsis

\begin{verbatim}
#include <stdio.h>

int scanf(const char *format, ...);
int sscanf(const char *str, const char *format, ...);

#include <stdarg.h>

int vscanf(const char *format, va_list ap);
int vsscanf(const char *str, const char *format, va_list ap);
\end{verbatim}

\manDescription

Les fonctions de la famille \textit{scanf} parcourent l'entrée selon la chaîne
\textit{format}
décrit ci-dessous.  Ce format peut contenir des spécificatifs de conversion; les
résultats de ces conversions, s'il y en a, sont stockés dans les emplacements
pointés par les pointeurs donnés en argument.  Chaque pointeur argument doit
être d'un type approprié pour la valeur renvoyée par chaque spécificatif de
conversion.

Si le nombre de spécificatifs de conversion dans \textit{format} est supérieure
au nombre de pointeurs, les résultats sont imprévisibles.  Si le nombre de
pointeurs est supérieur au nombre de spécificatifs de conversion, alors les
arguments en excès ne seront pas utilisés.

La fonction \manRef{scanf}{3} lit l'entrée depuis l'entrée standard, et
\manRef{sscanf}{3} lit l'entrée depuis la chaîne de caractères pointée par
\textit{str}.

La chaîne de format consiste en une séquence de directives qui décrivent la
manière dont la séquence de caractères doit être traitée.  Si le traitement
d'une directive échoue, aucune autre entrée n'est lue, et \textit{scanf()} 
s'arrête.  Un ``échec'' peut être provoqué par un échec de lecture (i.e.\
impossible de lire plus de caractères depuis l'entrée), ou un échec de
traitement (i.e.\ l'entrée était incorrecte).

Une directive est l'une des choses suivantes~:
\begin{itemize}
	\item Une séquence de blancs (espaces, tabulations, retours chariots,
		etc.).  Cette directive impose un nombre quelconque de blancs,
		qui peut être nul, dans l'entrée.

	\item Un caractère ordinaire, différent d'un blanc ou du caractère \%
		(pourcent).  Ce caractère doit être exactement identique au
		caractère suivant de l'entrée.

	\item Un spécificatif de conversion, commençant par un \% (pourcent).
		Une séquence de caractères est convertie selon cette
		spécification, et le résultat est placé dans l'argument pointeur
		correspondant.  Si l'élément suivant de l'entrée ne vérifie pas
		le spécificatif de conversion, la conversion échoue.
\end{itemize}

Chaque spécificatif de conversion commence avec le caractère \%, suivi par:
\begin{itemize}
	\item Un nombre entier en base 10, optionnel, spécifiant la longueur
		maximale du champ.  La lecture des caractère s'arrête lorsqu'un
		caractère non conforme est rencontré, ou lorsque ce maximum est
		atteint.  La plupart des conversions ne tiendront pas compte des
		carac\-tères blancs initiaux, et ces caractères ignorés ne
		comptent pas dans la longueur maximale du champ.

	\item Un spécificateur de conversion, sous la forme d'une seule lettre,
		spécifiant le type de la conversion de l'entrée à effectuer.
\end{itemize}

Les spécificateurs de conversion disponibles sont les suivants:

\begin{description}
	\item[\%] Caractère pourcent littéral.  Aucune conversion n'est faite,
		et aucun emplacement mémoire n'est modifié.

	\item[d] Nombre entier décimal, éventuellement signé.  Le pointeur
		suivant doit pointer vers un \textit{int}.

	\item[u] Nombre entier décimal non signé.  Le pointeur suivant doit
		pointer vers un \textit{unsigned int}.

	\item[x, X] Nombre hexadécimal non signé.  Le pointeur suivant doit pointer
		vers un \textit{unsigned int}.

	\item[s] Séquence de caractères non-blancs consécutifs.  Le pointeur
		suivant doit pointer vers une chaîne de caractères suffisamment
		longues pour contenir la séquence d'entrée, ainsi que le
		caractère zéro terminant la chaîne, qui est automatiquement
		ajoutée.  L'entrée s'arrête au premier blanc, ou lorsque la
		largeur maximale du champ est atteinte.

	\item[c] Séquence de caractères arbitraires dont la longueur est donnée
		par la largeur maximale du champ (par défaut, 1).  Le pointeur
		suivant doit être un pointeur vers \textit{char}, et il faudra
		suffisamment de place.  Aucun caractère zéro n'est rajouté.
\end{description}

\paragraph{Valeur de retour}

Ces fonctions renvoient le nombre d'entrées conformes et valuées, ce qui peut
être inférieur au nombre de champs donné dans \textit{format}, voire zéro, lors
d'une quelconque erreur de traitement.

La valeur $-1$ est renvoyée lors d'une erreur de lecture survenant avant la
première conversion.

\paragraph{Voir aussi}

\manRef{getc}{3}, \manRef{printf}{3}

\subsubsection{stdarg, va\_start, va\_arg, va\_end, va\_copy}

\manNom{stdarg, va\_start, va\_arg, va\_end, va\_copy}{Liste d'arguments
variables}

\manSynopsis

\begin{verbatim}
#include <stdarg.h>

void va_start(va_list ap, last);
type va_arg(va_list ap, type);
void va_end(va_list ap);
void va_copy(va_list dest, va_list src);
\end{verbatim}

\manDescription

Une fonction peut être appelée avec un nombre variable d'arguments, de types
variables.  Le fichier d'include \texttt{<stdarg.h>} déclare un type
\textit{va\_list} et définit trois macros afin de parcourir une liste
d'arguments dont le nombre et le type ne sont pas connus de la fonction
appelante.

La fonction appelée doit déclarer un objet de type \textit{va\_list}, qui est
utlisé par les macros \textit{va\_start()}, \textit{va\_arg()} et
\textit{va\_end()}.

\begin{description}
	\item[va\_start()]  La macro \textit{va\_start()} initialise
		la liste \textit{ap} pour une utilisation ultérieure par
		\textit{va\_arg()} et \textit{va\_end()}, et doit être appelée
		en premier.

		L'argument \textit{last} est le nom du dernier argument avant la
		liste variable d'arguments, i.e.\ le dernier argument dont le
		type est connu de la fonction appelante.

	\item[va\_arg()] La macro \textit{va\_arg()} est définie comme une
		expression qui a le type et la valeur de l'argument suivant de
		l'appel.  L'argument \textit{ap} est la liste de type
		\textit{va\_list} initialisée par \textit{va\_start()}.  Chaque
		appel de \textit{va\_arg()} modifie \textit{ap} pour que l'appel
		suivant renvoie l'argu\-ment suivant.  L'argument
		\textit{type} est un nom de type, donné de tele manière que le
		type d'un pointeur vers un tel objet puisse être obtenu en
		ajoutant une * à \textit{type}.

		La première utilisation de la macro \textit{va\_arg()} après
		celle de \textit{va\_start()} renvoie l'argument après
		\textit{last}.  Des invocations successives renvoient les
		valeurs des arguments restants.

		S'il n'y a pas d'argument suivant, ou si \textit{type} n'est pas
		compatible avec le type de l'argu\-ment suivant, des erreurs
		aléatoires peuvent survenir.

		Si \textit{ap} est donnée en argument à une fonction qui
		utilise \texttt{va\_arg(ap, type)}, alors la valeur de
		\textit{ap} est imprévisible après le retour de cette fonction.

	\item[va\_end()]  Chaque invocation de \textit{va\_start()} doit être
		accompagnée d'un appel correspondant à \textit{va\_end()} dans
		la même fonction.  Après l'appel de \texttt{va\_end(ap)}, la
		variable \textit{ap} est indéfinie.  Des parcours multiples de
		la liste, chacun entourés par des appels à
		\textit{va\_start()} et \textit{va\_end()}, sont possibles.
		\textit{va\_end()} peut être une macro ou une fonction.		

	\item[va\_copy()]  Copie une liste d'arguments variable de
		\textit{src} vers \textit{dest}.  Chaque appel de
		\textit{va\_copy()} doit être suivie d'un appel à
		\textit{va\_end()} dans la même fonction, une fois le traitement
		de \textit{dest} terminé.
\end{description}

\paragraph{Exemple}

La fonction \textit{foo} prend une chaîne de caractères de format, et imprime
l'argument associé à chaque format de caractère, selon son type.

\begin{verbatim}
#include <stdio.h>
#include <stdarg.h>

void
foo(char *fmt, ...)
{
    va_list ap;
    int d;
    char c, *s;

    va_start(ap, fmt);
    while (*fmt)
        switch (*fmt++) {
            case 's':          /* chaine */
                s = va_arg(ap, char *);
                printf("string %s\n", s);
                break;
            case 'd':          /* int */
                d = va_arg(ap, int);
                printf("int %d\n", d);
                break;
            case 'c':          /* char */
                /* cast nécessaire ici car va_arg n'accepte
                   seulement les types entièrement promus */
                c = (char) va_arg(ap, int);
                printf("char %c\n", c);
                break;
        }
    va_end(ap);
}
\end{verbatim}

\subsection{Section 5 -- Formats de fichier}

\subsubsection{tasklistrc}

\manNom{tasklistrc}{Informations statiques concernant les processus à lancer}

\manDescription

Le fichier \manRef{tasklistrc}{5} contient une liste des chemins vers les
exécutables ELF à inclure dans l'image du noyau KikoolOS lors de la génération
du système.

Chacun des exécutables est donné sur une ligne.  Les lignes commençant par le
caractère \# sont traités comme des commentaires et ignorés.  Enfin, comme il
n'est possible d'inclure que quatre processus maximum dans l'image, seuls les
quatre premiers exécutables donnés dans le fichier seront pris en compte lors de
la génération de l'image.


\section{Recette de tests}

\subsection{Utilisation standard}

Afin de tester le bon fonctionnement du système, il serait envisageable de
tester la génération d'un système avec quatre programmes, permettant eux-mêmes
de tester les fonctions et appels systèmes implémentés.

On pourra ensuite lancer un autre jeu de test avec quatre fois le même
programme, affichant un compteur à l'écran qui est régulièrement incrémenté,
afin de s'assurer du bon fonctionnement de l'ordonnanceur.

\subsection{Cas limites -- Protection de la mémoire et génération d'exceptions}

Un cas limite est la tentative d'accès à une zone mémoire qui n'appartient pas
au processus.  Dans ce cas, ce processus verra son accès à la mémoire refusée,
et sera tué par le noyau.

Un autre cas est la génération d'exceptions matérielles: par exemple, la
tentative de division par zéro, une faute de page, une erreur de protection
générale, ou un opcode non valable, rencontrés lors de l'exécution d'un
programme.

Enfin, les appels système et les fonctions de bibliothèque peuvent également
échouer pour diverses raisons.  Il faudra donc tester la bonne détection des cas
d'erreur.

\subsection{Traitement des erreurs}

Le système affichera les erreurs:
\begin{itemize}
	\item dans la fenêtre du shell, lorsqu'il s'agit d'erreurs système;
	\item dans la fenêtre du processus correspondant, lorsqu'il s'agit
		d'une erreur provenant d'un processus particulier.
\end{itemize}

Lorsque l'utilisateur tape une commande non valable sur le shell, le système
devra lui en informer avec un message d'erreur adéquat (\texttt{Invalid
command}, par exemple).

Le système devra également imprimer un message adéquat dans la fenêtre d'un
processus, lorsque celui-ci tente une action interdite par le noyau (dans le
cadre de la protection mémoire, en particulier), ou lorsqu'une exception
matérielle est soulevée par le processeur (une division par zéro ou une
instruction illégale, en particulier).

Lors d'une exception matérielle survenant en mode noyau, le noyau devra
pouvoir détecter ces cas, et s'arrêter de manière similaire à un \textit{kernel
panic}.

\subsection{Robustesse}

De manière générale, le plantage d'un processus ne doit pas affecter le bon
fonctionnement des autres processus, ni du noyau.  Il faudra impérativement
vérifier cette propriété de robustesse lors des tests des cas limites.  

\end{document}
