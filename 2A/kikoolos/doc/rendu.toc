\select@language {french}
\contentsline {section}{\numberline {1}Aperçu des fonctionnalités principales}{2}{section.1}
\contentsline {section}{\numberline {2}Manuel}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Vue générale}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Génération d'un système}{3}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Lancement du système}{3}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Utilisation avancée de KikoolOS}{3}{subsection.2.4}
\contentsline {subsubsection}{\numberline {2.4.1}Options avancées du compilateur}{3}{subsubsection.2.4.1}
\contentsline {subsubsection}{\numberline {2.4.2}Format de l'image}{3}{subsubsection.2.4.2}
\contentsline {subsubsection}{\numberline {2.4.3}Carte mémoire}{4}{subsubsection.2.4.3}
\contentsline {paragraph}{Mapping de la mémoire physique}{4}{section*.6}
\contentsline {paragraph}{Mapping de la mémoire virtuelle de chaque processus}{5}{section*.7}
\contentsline {section}{\numberline {3}Implémentation}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}Gestion de l'espace virtuel et physique}{5}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Algorithme d'ordonnancement}{5}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Fonctionnement des appels système}{6}{subsection.3.3}
\contentsline {section}{\numberline {4}Pages de manuel}{7}{section.4}
\contentsline {subsection}{\numberline {4.1}Section 1 -- Commandes utilisateur}{7}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}kgcc}{7}{subsubsection.4.1.1}
\contentsline {paragraph}{Nom}{7}{section*.8}
\contentsline {paragraph}{Synopsis}{7}{section*.9}
\contentsline {paragraph}{Description}{7}{section*.10}
\contentsline {paragraph}{Options}{7}{section*.11}
\contentsline {paragraph}{Bugs}{7}{section*.12}
\contentsline {subsection}{\numberline {4.2}Section 2 -- Appels système}{8}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}\_exit}{8}{subsubsection.4.2.1}
\contentsline {paragraph}{Nom}{8}{section*.13}
\contentsline {paragraph}{Synopsis}{8}{section*.14}
\contentsline {paragraph}{Description}{8}{section*.15}
\contentsline {paragraph}{Remarque}{8}{section*.16}
\contentsline {subsubsection}{\numberline {4.2.2}write\_vt}{8}{subsubsection.4.2.2}
\contentsline {paragraph}{Nom}{8}{section*.17}
\contentsline {paragraph}{Synopsis}{8}{section*.18}
\contentsline {paragraph}{Description}{8}{section*.19}
\contentsline {paragraph}{Valeur de retour}{8}{section*.20}
\contentsline {paragraph}{Voir aussi}{8}{section*.21}
\contentsline {subsubsection}{\numberline {4.2.3}read\_vt}{8}{subsubsection.4.2.3}
\contentsline {paragraph}{Nom}{8}{section*.22}
\contentsline {paragraph}{Synopsis}{8}{section*.23}
\contentsline {paragraph}{Description}{9}{section*.24}
\contentsline {paragraph}{Valeur de retour}{9}{section*.25}
\contentsline {paragraph}{Voir aussi}{9}{section*.26}
\contentsline {subsection}{\numberline {4.3}Section 3 -- Fonctions de bibliothèque}{9}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}getchar, gets}{9}{subsubsection.4.3.1}
\contentsline {paragraph}{Nom}{9}{section*.27}
\contentsline {paragraph}{Synopsis}{9}{section*.28}
\contentsline {paragraph}{Description}{9}{section*.29}
\contentsline {paragraph}{Bugs}{9}{section*.30}
\contentsline {subsubsection}{\numberline {4.3.2}printf, sprintf, snprintf, vprintf, vsprintf, vsnprintf}{9}{subsubsection.4.3.2}
\contentsline {paragraph}{Nom}{9}{section*.31}
\contentsline {paragraph}{Synopsis}{9}{section*.32}
\contentsline {paragraph}{Description}{10}{section*.33}
\contentsline {paragraph}{Chaîne de format}{10}{section*.34}
\contentsline {paragraph}{Exemple}{10}{section*.35}
\contentsline {subsubsection}{\numberline {4.3.3}putchar, puts}{11}{subsubsection.4.3.3}
\contentsline {paragraph}{Nom}{11}{section*.36}
\contentsline {paragraph}{Synopsis}{11}{section*.37}
\contentsline {paragraph}{Description}{11}{section*.38}
\contentsline {paragraph}{Valeur de retour}{11}{section*.39}
\contentsline {paragraph}{Voir aussi}{11}{section*.40}
\contentsline {subsubsection}{\numberline {4.3.4}scanf, sscanf, vscanf, vsscanf}{11}{subsubsection.4.3.4}
\contentsline {paragraph}{Nom}{11}{section*.41}
\contentsline {paragraph}{Synopsis}{11}{section*.42}
\contentsline {paragraph}{Description}{11}{section*.43}
\contentsline {paragraph}{Valeur de retour}{13}{section*.44}
\contentsline {paragraph}{Voir aussi}{13}{section*.45}
\contentsline {subsubsection}{\numberline {4.3.5}stdarg, va\_start, va\_arg, va\_end, va\_copy}{13}{subsubsection.4.3.5}
\contentsline {paragraph}{Nom}{13}{section*.46}
\contentsline {paragraph}{Synopsis}{13}{section*.47}
\contentsline {paragraph}{Description}{13}{section*.48}
\contentsline {paragraph}{Exemple}{14}{section*.49}
\contentsline {subsection}{\numberline {4.4}Section 5 -- Formats de fichier}{15}{subsection.4.4}
\contentsline {subsubsection}{\numberline {4.4.1}tasklistrc}{15}{subsubsection.4.4.1}
\contentsline {paragraph}{Nom}{15}{section*.50}
\contentsline {paragraph}{Description}{15}{section*.51}
\contentsline {section}{\numberline {5}Recette de tests}{15}{section.5}
\contentsline {subsection}{\numberline {5.1}Utilisation standard}{15}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Cas limites -- Protection de la mémoire et génération d'exceptions}{15}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Traitement des erreurs}{15}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Robustesse}{16}{subsection.5.4}
