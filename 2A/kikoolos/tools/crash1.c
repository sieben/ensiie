#include <unistd.h>

int main()
{
	int a;

	write_vt("Test syscall non valable... ");

	asm("movl $42, %%eax\n"
		"int $0x80\n"
		::: "eax");

	write_vt("OK\n");

	write_vt("Test adresse hors de ma port\202e... ");

	a = *(int*)(0xcafebabe);

	write_vt("OK\n");
}
