#include <util.h>
#include "kernel.h"
#include "sched.h"
#include "mm.h"
#include "kbd.h"

/* Tronche de la pile à l'intérieur de do_syscalls :
 *
 * esp -> |     .      |
 *        |     .      |
 *        |     .      |
 *        |     .      |
 *        |     .      |
 * ebp -> |    ebp     |
 * ebp+4  | addr. ret. |
 * ebp+8  |    eax     | (arg 1 do_syscalls)
 * ebp+12 |    gs      |
 * ebp+16 |    fs      |
 * ebp+20 |    es      |
 * ebp+24 |    ds      |
 * ebp+28 |    edi     | (arg 5 appel système)
 * ebp+32 |    esi     | (arg 4 appel système)
 * ebp+36 |    ebp     |
 * ebp+40 |    esp     |
 * ebp+44 |    ebx     | (arg 1 appel système)
 * ebp+48 |    edx     | (arg 3 appel système)
 * ebp+52 |    ecx     | (arg 2 appel système)
 * ebp+56 |    eax     | (entrée: numéro syscall, sortie: valeur de retour)
 */

int do_syscalls(int nr)
{
	int ret = 0;

	switch(nr)
	{
		case 0: /* exit */
			{
				sys_exit();
			}
			break;


		case 1: /* write_vt */
			{
				char* u_str;

				asm("mov 44(%%ebp), %%eax\n"
					"mov %%eax, %0" 
					: "=m"(u_str) :
					: "eax");

				kprints(current->sc, u_str);
				return strlen(u_str);
			}
			break;
		
		case 2: /* read_vt */
			{
				char* buf;
				size_t nmemb;

				asm("mov 44(%%ebp), %%eax\n"
					"mov %%eax, %0\n"
					"mov 52(%%ebp), %%eax\n"
					"mov %%eax, %1\n"
					: "=m"(buf), "=m"(nmemb)
					: :
					"eax");

				return read_vt(buf, nmemb);
			}

		default:
			kprintf(current->sc, "\ninvalid syscall %d\n", nr);
			return -1;
	}
}

void syscalls_init()
{
	/* init syscall vector entry 0x80 */
	{
		extern unsigned long idt_table[];
		extern void _asm_syscalls();
		long addr = (long)_asm_syscalls;
		unsigned short* pidt = (unsigned short*)(idt_table+(0x80<<1));
		pidt[0] = addr;
		pidt[2] = 0xef00; /* set DPL at 3, interrupt gate */
		pidt[3] = (((long)addr)>>16)&0xffff;
	}
}
