/*
 *  mini kernel initialisation
 */


/*
 * High loaded stuff by Hans Lermen & Werner Almesberger, Feb. 1996
 */

#include <config.h>
.text
.globl _start, startup_32
.globl idt_table

#define MULTIBOOT_HEADER_MAGIC 0x1BADB002
#define MULTIBOOT_HEADER_FLAGS 0x00000001
#define CHECKSUM -(MULTIBOOT_HEADER_MAGIC + MULTIBOOT_HEADER_FLAGS)


.macro SAVE_REGS
	cld
	pusha
	pushl %ds
	pushl %es
	pushl %fs
	pushl %gs
	pushl %ebx
	movl $(__KERNEL_DS), %ebx
	movl %ebx, %ds
	movl %ebx, %es
	popl %ebx
.endm

.macro RESTORE_REGS
	popl %gs
	popl %fs
	popl %es
	popl %ds
	popa
.endm



.org	0
_start:
	jmp startup_32

.align 4
multiboot_header:
	.long MULTIBOOT_HEADER_MAGIC
	.long MULTIBOOT_HEADER_FLAGS
	.long CHECKSUM

.org 20

startup_32:
	cld
	cli

	movl $(__KERNEL_DS),%eax
	movl %eax,%ss
	movl %eax,%ds
	movl %eax,%es
	movl %eax,%fs
	movl %eax,%gs

	movl $stack_end,%esp

	xorl %eax,%eax
1:	incl %eax		# check that A20 really IS enabled
	movl %eax,0x000000	# loop forever if it isn't
	cmpl %eax,0x100000
	je 1b
/*
 * Initialize eflags.  Some BIOS's leave bits like NT set.  This would
 * confuse the debugger if this code is traced.
 * XXX - best to initialize before switching to protected mode.
 */
	pushl $0
	popfl
/*
 * Clear BSS
 */
	xorl %eax,%eax
	movl $ SYMBOL_NAME(_edata),%edi
	movl $ SYMBOL_NAME(_end),%ecx
	subl %edi,%ecx
	cld
	rep
	stosb
/*
 * Launch the mini kernel
 */
	call gdt_init
	call interrupt_init

/*
 * Pour que le PIT fasse un tic toutes les millisecondes, on programme
 * son diviseur de fréquences de manière adéquate.
 * On a: 1193,182 / 1193 = 1,000 115 kHz
 */
	movb $0b00110100, %al	/* canal 0, LSB/MSB, rate generator */
	outb %al, $0x43
	movw $0x4a9, %ax	/* 1193 = 0x4a9 en hexa */
	outb %al, $0x40		/* envoi LSB */
	movb %ah, %al
	outb %al, $0x40		/* envoi MSB */

	movb $0xff,%al
	outb %al,$0xa1		/* masquer toutes les IRQ pour le moment */
	movb $0xff,%al
	outb %al,$0x21		/* masquer toutes les IRQ pour le moment */
	sti			/* au moins on aura les traps */

	call vga_init
	call sched_init
	call init_mm
	call kbd_init
	call syscalls_init
	call tss_init
	call main_init
	cli
	movb $0xff,%al
	outb %al,$0xa1		/* enable IRQ: -8,-9,-10,-11,-12,-13,-14,-15 */
	movb $0xf8,%al
	outb %al,$0x21		/* enable IRQ: +00,+01,+02,-03,-04,-05,-06,-07  */
	sti
	call mini_kernel
1:	
	jmp 1b

/******** memory for stack ********/
/* this is equivalent to offset 0x2000 in the final image. */
.org 0x1000
stack:
.org 0x2000
stack_end:

/******** interrupt table ********/
#include "idt.h"
idt_descr:
	.word 256*8-1		/* idt_table contains 256 entries */
	.long idt_table

/******** irq entries     ********/
.globl minikernel_irq0
minikernel_irq0:
	SAVE_REGS
	call do_minikernel_irq0
	movb $0x20,%al
	outb %al,$0x20
	RESTORE_REGS
	iret

.globl minikernel_irq1
minikernel_irq1:
	cld
	SAVE_REGS
	call do_minikernel_irq1
	movb $0x20,%al
	outb %al,$0x20
	RESTORE_REGS
	iret
	
