#ifndef __SCHED_H
#define __SCHED_H

#include <stdint.h>
#include "kernel.h"
#include "kbd.h"
#include "mm.h"

#define ACTIVE 1
#define EXPIRED 0

#define KERNEL_MODE 0
#define USER_MODE 3

#define MAX_PROCESSES 4

#define QUANTUM 16

enum task_state {
	TASK_RUNNING,
	TASK_STOPPED,
};

struct task_struct {
    int pid;
    struct {
        uint32_t eax, ecx, edx, ebx;
        uint32_t esp, ebp, esi, edi;
        uint32_t eip, eflags;
        uint32_t cs:16, ss:16, ds:16, es:16, fs:16, gs:16;
        uint32_t cr3;
    } regs __attribute__ ((packed));
    struct {
        uint32_t esp0;
        uint32_t ss0:16;
    } kstack __attribute__ ((packed));
    struct {
        char buf[KBD_BUFSIZE];
        char* read_ptr;
        char* write_ptr;
    } kbd;
    enum task_state state;
    int active;		/* Commutable ? */
    int time_left;	/* Quantum CPU restant */
    int needs_resched;	/* ... */
    subscreen* sc;	/* Subscreen for printf() and stuff */
    int has_focus;	/* Has keyboard focus? */
    int waiting_for_key;/* Attente d'une touche ou pas? */
};

#ifndef __SCHED__
extern struct task_struct task_list[MAX_PROCESSES];
extern struct task_struct* current;
#endif



void sched_init();

int make_new_pid();

int sched_add_task();

void sched_remove_task(struct task_struct* ts);

void schedule();

int nprocs();

struct task_struct* sched_get_task_by_pid(int pid);

void update_top();

#endif

/* vi:ts=4:sw=4:et
 */
