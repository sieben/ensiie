/*
 * basic print to vga screen for debuging
 */

#include <stdarg.h>
#include <stdint.h>
#include "kernel.h"
#include "vga.h"
#include "screen.h"
#include <util.h>

/*
 * setup has initialiszed the handler and put all
 * information at 0x90000 adressese
 */

#define SCREEN_INFO ( *(struct screen_info *) 0x90000 )

struct screen_info {
	unsigned char  orig_x;			/* 0x00 */
	unsigned char  orig_y;			/* 0x01 */
	unsigned short dontuse1;		/* 0x02 -- EXT_MEM_K sits here */
	unsigned short orig_video_page;		/* 0x04 */
	unsigned char  orig_video_mode;		/* 0x06 */
	unsigned char  orig_video_cols;		/* 0x07 */
	unsigned short unused2;			/* 0x08 */
	unsigned short orig_video_ega_bx;	/* 0x0a */
	unsigned short unused3;			/* 0x0c */
	unsigned char  orig_video_lines;	/* 0x0e */
	unsigned char  orig_video_isVGA;	/* 0x0f */
	unsigned short orig_video_points;	/* 0x10 */

	/* VESA graphic mode -- linear frame buffer */

	unsigned short lfb_width;		/* 0x12 */
	unsigned short lfb_height;		/* 0x14 */
	unsigned short lfb_depth;		/* 0x16 */
	unsigned long  lfb_base;		/* 0x18 */
	unsigned long  lfb_size;		/* 0x1c */
	unsigned short dontuse2, dontuse3;	/* 0x20 -- CL_MAGIC and CL_OFFSET here */
	unsigned short lfb_linelength;		/* 0x24 */
	unsigned char  red_size;		/* 0x26 */
	unsigned char  red_pos;			/* 0x27 */
	unsigned char  green_size;		/* 0x28 */
	unsigned char  green_pos;		/* 0x29 */
	unsigned char  blue_size;		/* 0x2a */
	unsigned char  blue_pos;		/* 0x2b */
	unsigned char  rsvd_size;		/* 0x2c */
	unsigned char  rsvd_pos;		/* 0x2d */
	unsigned short vesapm_seg;		/* 0x2e */
	unsigned short vesapm_off;		/* 0x30 */
	unsigned short pages;			/* 0x32 */
	/* 0x34 -- 0x3f reserved for future expansion */
};

/********************************************/
/**** real screan                        ****/
/****    - kernel is alive               ****/
/****    - ttyS0 infos                   ****/
/****    - ttyS1 infos                   ****/
/****    - 4 scroll lines                ****/
/****    - 3 scroll lines (shell)        ****/
/****    - user screen (4 processes)     ****/

static int nblines, nbcols;
static char *vidmem;
static int vidport;

subscreen sc_banner;
subscreen sc_top;
subscreen sc_shell;
subscreen sc_user[4];

void vga_init()
{
	int nbl;
	int i;

	if (SCREEN_INFO.orig_video_mode == 7) {
		vidmem = (char *) 0xb0000;
		vidport = 0x3b4;
	} else {
		vidmem = (char *) 0xb8000;
		vidport = 0x3d4;
	}
	nblines = SCREEN_INFO.orig_video_lines;
	nbcols  = SCREEN_INFO.orig_video_cols;
	/* clear screen */
	for (i=0 ; i< (nblines*nbcols*2) ; i += 2) {
		vidmem[i]   = ' ';
		vidmem[i+1] = 0x07;
	}

	nbl= nblines;

	/* initialisation de la banniere */

	sc_banner.vidmem = vidmem;
	sc_banner.nblines = 4;
	sc_banner.nbcols = nbcols;
	sc_banner.cline = 0;
	sc_banner.ccol = 0;
    sc_banner.currentAttribute = 0x07;
	nbl -= sc_banner.nblines;

	/* initialisation du top (processus actif, uptime, ...) */

	sc_top.vidmem = sc_banner.vidmem + sc_banner.nblines * nbcols * 2;
	sc_top.nblines = 1;
	sc_top.nbcols = nbcols;
	sc_top.cline = 0;
	sc_top.ccol = 0;
    sc_top.currentAttribute = 0x07;
	nbl -= sc_top.nblines;

	/* initialisation du shell  */

	sc_shell.vidmem= sc_top.vidmem + sc_top.nblines * nbcols * 2;
	sc_shell.nblines= 3;
	sc_shell.nbcols= nbcols;
	sc_shell.cline=0;
	sc_shell.ccol=0;
    sc_shell.currentAttribute = 0x07;
	nbl -= sc_shell.nblines;

	/* init 4 process screens */
	{
		void* vidmem_user = sc_shell.vidmem + sc_shell.nblines * nbcols * 2;
		for (i = 0; i < 2; i++)
		{

			sc_user[2*i].vidmem     = vidmem_user;
			sc_user[2*i].nblines    = nbl / 2;
			sc_user[2*i].nbcols     = nbcols / 2;
			sc_user[2*i].cline      = 0;
			sc_user[2*i].ccol       = 0;
            sc_user[2*i].currentAttribute = 0x07;

			sc_user[2*i+1].vidmem     
				= sc_user[2*i].vidmem + (nbcols - sc_user[2*i].nbcols) * 2;
			sc_user[2*i+1].nblines    = sc_user[2*i].nblines;
			sc_user[2*i+1].nbcols     = nbcols - sc_user[2*i].nbcols;
			sc_user[2*i+1].cline      = 0;
			sc_user[2*i+1].ccol       = 0;
            sc_user[2*i+1].currentAttribute = 0x07;

			vidmem_user += (nbl / 2) * nbcols * 2;
		}
	}
}


static void vkprintf(subscreen* psc, const char* fmt, va_list args);
static void scroll(subscreen *psc);

void kprintc(subscreen* psc, char c)
{
	int x, y;

	x = psc->ccol;
	y = psc->cline;

	if ( c == '\n' ) {
		x = 0;
		if ( ++y >= psc->nblines ) {
			scroll(psc);
			y--;
		}
	} else if ( c == '\r' ) {
		x = 0;
	} else {
		/* Écrire dans la mémoire vidéo au bon endroit, 
		 * puis met le bon attribut (currentAttribute) */
		psc->vidmem [ ( x + nbcols * y ) * 2 ] = c; 
		psc->vidmem [ ( x + nbcols * y ) * 2 + 1] = psc->currentAttribute;
		if ( ++x >= psc->nbcols ) {
			x = 0;
			if ( ++y >= psc->nblines ) {
				scroll(psc);
				y--;
			}	
		}	
	}

	psc->ccol  = x;
	psc->cline = y;

#if 0
	pos = (x + cols * y) * 2;	/* Update cursor position */
	outb_p(14, vidport);
	outb_p(0xff & (pos >> 9), vidport+1);
	outb_p(15, vidport);
	outb_p(0xff & (pos >> 1), vidport+1);
#endif
}

void kprints(subscreen* psc, const char *p)
{
	while (*p)
		kprintc(psc,*p++);
}

void kprintf(subscreen* psc, const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	vkprintf(psc,fmt,args);
	va_end(args);
}

void vgaprintc(char c)
{
	kprintc(&sc_shell,c);
}

void vgaprints(const char* s)
{
	kprints(&sc_shell,s);
}

void vgaprintf(const char* fmt,...)
{ 
	va_list args;
	va_start(args, fmt);
	vkprintf(&sc_shell,fmt,args);
	va_end(args);
}

static void scroll(subscreen *psc)
{
	int i;

	for ( i = 1; i < psc->nblines; i++ )
	{
		memcpy ( psc->vidmem + ((i - 1) * nbcols) * 2,
				psc->vidmem + (i * nbcols) * 2,
				psc->nbcols * 2);
	}
	for ( i = ( psc->nblines - 1 ) * nbcols * 2;
			i < ((psc->nblines - 1) * nbcols + psc->nbcols) * 2; i += 2 )
		psc->vidmem[i] = ' ';
}

static void vkprintf_str(subscreen* psc, const char* str, 
		int len, char fillwith, int placeleft, int signe)
{
	int i;
	if (len==0) {
		if (signe) kprintc(psc,signe);
		kprints(psc,str);
	} else {
		int slen=strlen(str) + (signe ? 1 : 0);
		if (slen >= len) {
			if (signe) kprintc(psc,signe);
			kprints(psc,str);
		} else if (placeleft) {
			if (signe) kprintc(psc,signe);
			kprints(psc,str);
			for (i=0; i<(len-slen) ; i++) {
				kprintc(psc,fillwith);
			}
		} else {
			if (signe&&(fillwith!=' ')) kprintc(psc,signe);
			for (i=0; i<(len-slen) ; i++)
				kprintc(psc,fillwith);
			if ( signe&&(fillwith==' ')) kprintc(psc,signe);
			kprints(psc,str);
		}
	}
}

static void vkprintf(subscreen* psc, const char* fmt, va_list args)
{
	int len=0;
	char fillwith=' ';
	int	 placeleft=0;
	int	 signe=0;
	char buf[100];

	while (*fmt) {
		switch (*fmt) {
			case '%' :
				fmt++;
				while (*fmt) {
					switch (*fmt) {
						case '%' : fmt++; kprintc(psc,'%'); goto leave_printarg;
						case 's' :
							   vkprintf_str(psc,va_arg(args, char*), len, fillwith, placeleft,0);
							   fmt++;
							   goto leave_printarg;
						case 'c' :
							   kprintc(psc,va_arg(args, int)&0xff);
							   fmt++;
							   goto leave_printarg;
						case 'x' :
						case 'X' : {
								   char* p=buf;
								   unsigned int x= va_arg(args, int);
								   int i;
								   for (i=0 ; i<7 ; i++, x <<= 4) {
									   if ( (x&0xf0000000)!=0 )
										   break;
								   }
								   for ( ; i<8 ; i++, x <<= 4) {
									   char c= (x>>28)&0xf;
									   if ( c>=10 )
										   *p++= c-10+'a';
									   else
										   *p++= c+'0';
								   }
								   *p=0;
								   vkprintf_str(psc,buf, len, fillwith, placeleft,0);
								   fmt++;
								   goto leave_printarg;
							   }
						case 'd' :
						case 'i' : {
								   char* p=buf;
								   int y=1000000000;
								   int x= va_arg(args, int);
								   if (x<0) {
									   signe = '-';
									   x = -x;
								   } else if (signe)
									   signe = '+';

								   for (    ; y>=10 ; y /= 10) {
									   if ( (x/y)>0 )
										   break;
								   }
								   for (    ; y>=10 ; y /= 10) {
									   *p++ = (x / y) + '0';
									   x = x % y;
								   }
								   *p++ = x + '0';
								   *p=0;
								   if (placeleft)
									   fillwith=' ';
								   vkprintf_str(psc, buf, len, fillwith, placeleft, signe);
								   fmt++;
								   goto leave_printarg;
							   }
						case '0' : fmt++; fillwith='0'; continue;
						case '+' : fmt++; signe= 1; continue;
						case '-' : fmt++; placeleft= 1; continue;
						default:
							   if ( (*fmt<'1') || ('9'<=*fmt) ) {
								   /* ignore char */
								   continue;
							   }
							   /* get number */
							   while (*fmt && !((*fmt<'0') || ('9'<*fmt)) ) {
								   len = len*10 + *fmt-'0';
								   fmt++;
							   }
							   continue;
					}
				}
leave_printarg:
				len=0;
				fillwith=' ';
				placeleft=0;
				signe=0;
				continue;
			default:
				kprintc(psc,*fmt++);
				continue;
		}
	}
}


uint8_t getcolor(const char color, const char background, int blinking, int hl)
{
	uint8_t arg = 0;

	if( ( hl != 0 && hl !=1 ) || ( blinking != 0 && blinking !=1 ) ){
		return 0x07; // Données incorrectes
	}

	arg |= (blinking) << 7;
	arg |= (hl) << 3;

	switch(background){
		case('n'): //noir
			arg |= 0 << 4;
			break;
		case('b'): //bleu
			arg |= 1 << 4;
			break;
		case 'v': //vert
			arg |= 2 << 4;
			break;
		case 'c': //cyan
			arg |= 3 << 4;
			break;
		case 'r': //rouge
			arg |= 4 << 4;
			break;
		case 'm': //magenta
			arg |= 5 << 4;
			break;
		case 'j': //jaune
			arg |= 6 << 4;
			break;
		case 'w': //blanc
			arg |= 7 << 4;
			break;
	}

	switch(color){
		case('n'): //noir
			arg |= 0 << 0;
			break;
		case('b'): //bleu
			arg |= 1 << 0;
			break;
		case 'v': //vert
			arg |= 2 << 0;
			break;
		case 'c': //cyan
			arg |= 3 << 0;
			break;
		case 'r': //rouge
			arg |= 4 << 0;
			break;
		case 'm': //magenta
			arg |= 5 << 0;
			break;
		case 'j': //jaune
			arg |= 6 << 0;
			break;
		case 'w': //blanc
			arg |= 7 << 0;
			break;
	}


	return arg;


}

void color(subscreen* psc, const char color, const char background, 
		int blinking, int hl)
{

	int i, j;
	uint8_t arg = getcolor(color,background,blinking,hl);

	for( i = 0; i < psc->nblines; i++)
		for ( j = 0; j < psc->nbcols; j++)
            if (psc->vidmem[2 * (nbcols * i + j) + 1] == psc->currentAttribute)
                psc->vidmem[2 * (nbcols * i + j) + 1] = arg;

    set_current_attribute(psc, arg);
}



/* vi:ts=4:et:sw=4:
 */
