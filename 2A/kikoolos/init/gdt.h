#ifndef __GDT_H
#define __GDT_H

#include <stdint.h>

#define GDTBASE    0x90000	/* addr. physique ou doit resider la gdt */
#define GDTSIZE    0xFF		/* nombre max. de descripteurs dans la table */


/* Descripteur de segment */
struct gdtdesc {
    uint16_t lim0_15;    
    uint16_t base0_15;
    uint8_t base16_23;
    uint8_t access;
    uint8_t lim16_19 : 4;
    uint8_t other : 4;
    uint8_t base24_31;
} __attribute__ ((packed));

/* Registre GDTR */
struct gdtr {
    uint16_t limit;
    uint32_t base;
} __attribute__ ((packed));


void gdt_init();

void init_gdt_desc(uint32_t base, uint32_t limit, uint8_t access, uint8_t other,
                   struct gdtdesc *desc);

#endif
