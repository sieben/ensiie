#include <util.h>
#include "gdt.h"

/* GDT */
struct gdtdesc gdt[256];
struct gdtr gdtr;

void gdt_init()
{
	int i;

	/* Entrée nulle, sinon ça va chier */
	init_gdt_desc(0x0, 0x0, 0x0, 0x0, &gdt[0]);

	/* Entrées correspondant à la GDT précédemment chargée
	 * (sinon ça va chier aussi) */
	init_gdt_desc(0x0, 0x0, 0x0, 0x0, &gdt[1]);	/* non utilisée */
	init_gdt_desc(0x0, 0xfffff, 0x9a, 0xc, &gdt[2]); /* code kernel */
	init_gdt_desc(0x0, 0xfffff, 0x92, 0xc, &gdt[3]); /* data kernel */
	init_gdt_desc(0x0, 0xfffff, 0xfa, 0xc, &gdt[4]); /* code user */
	init_gdt_desc(0x0, 0xfffff, 0xf2, 0xc, &gdt[5]); /* data user */
	init_gdt_desc(0xcafebabe, 0x67, 0xe9, 0x0, &gdt[6]); /* TSS (plus tard) */

	/* Entrées pipo pour le reste */
	for(i = 7; i < 255; i++)
	{
		init_gdt_desc(0xdeadc0de, 0x00, 0x10, 0x4, &gdt[i]);
	}

	/* initialisation de la structure pour GDTR */
	gdtr.base = (uint32_t)&gdt;
	gdtr.limit = (uint16_t)GDTSIZE * 8;
	
	/* recopie de la GDT a son adresse */
	memcpy((void*)gdtr.base, (void*)gdt, gdtr.limit);
	
	/* chargement du registre GDTR */
	asm("lgdtl (gdtr)");
}


void init_gdt_desc(uint32_t base, uint32_t limit, uint8_t access, uint8_t other,
                   struct gdtdesc *desc)
{
        desc->lim0_15 = (limit & 0xffff);
        desc->base0_15 = (base & 0xffff);
        desc->base16_23 = (base & 0xff0000) >> 16;
        desc->access = access;
        desc->lim16_19 = (limit & 0xf0000) >> 16;
        desc->other = (other & 0xf);
        desc->base24_31 = (base & 0xff000000) >> 24;
        return;
}
