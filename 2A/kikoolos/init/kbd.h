#ifndef __KBD_H
#define __KBD_H

#define KBD_BUFSIZE 256

#include "kernel.h"
#include "util.h"
#include "sched.h"


#define focused (task_list[0].has_focus ? &task_list[0] : \
		 task_list[1].has_focus ? &task_list[1] : \
		 task_list[2].has_focus ? &task_list[2] : \
		 task_list[3].has_focus ? &task_list[3] : (void*)0)

#endif
