/*
 * basic tools
 */

#include <stdarg.h>
#include "kernel.h"
#include <util.h>

#define NULL ((void *)0);

unsigned char  inb  (unsigned short port)
{
	unsigned char  _v;
	__asm__ __volatile__ ("in" "b" " %"  "w"  "1,%"   ""   "0"  : "=a" (_v) : "Nd" (port)   ); return _v;
}


unsigned char  inb_p (unsigned short port)
{
	unsigned char  _v;
	__asm__ __volatile__ ("in" "b" " %"  "w"  "1,%"   ""   "0"  "\noutb %%al,$0x80"   : "=a" (_v) : "Nd" (port)   );
	return _v;
}


unsigned short  inw  (unsigned short port)
{
	unsigned short  _v;
	__asm__ __volatile__ ("in" "w" " %"  "w"  "1,%"   ""   "0"  : "=a" (_v) : "Nd" (port)   );
	return _v;
}

unsigned short  inw_p (unsigned short port)
{
	unsigned short  _v;
	__asm__ __volatile__ ("in" "w" " %"  "w"  "1,%"   ""   "0"  "\noutb %%al,$0x80"   : "=a" (_v) : "Nd" (port)   );
	return _v; }

void outb  (unsigned   char   value, unsigned short port)
{
	__asm__ __volatile__ ("out" "b" " %"   "b"   "0,%"  "w"  "1"  : : "a" (value), "Nd" (port));
}

void outb_p(unsigned   char   value, unsigned short port)
{
	__asm__ __volatile__ ("out" "b" " %"   "b"   "0,%"  "w"  "1"  "\noutb %%al,$0x80"   : : "a" (value), "Nd" (port));
}

void outw (unsigned short value, unsigned short port)
{
	__asm__ __volatile__ ("out" "w" " %"   "w"   "0,%"  "w"  "1"  : : "a" (value), "Nd" (port));
}

void outw_p (unsigned   short   value, unsigned short port)
{
	__asm__ __volatile__ ("out" "w" " %"   "w"   "0,%"  "w"  "1"  "\noutb %%al,$0x80"   : : "a" (value), "Nd" (port));
}

void* memcpy(void* dest, const void* src, size_t n )
{
    int i;
    char *d = (char *) dest, *s = (char *)src;

    for( i=0; i<n; i++ ) d[i] = s[i];

    return dest;
}

int strlen(const char*p)
{
    int len=0;
    while (*p++) len += 1;
    return len;
}

char* strcpy(char* dest, const char* src)
{
    char* p=dest;
    while ( (*dest++=*src++)!=0 );
    return p;
}

int strcmp(const char* s1, const char* s2)
{
    while (*s1 && *s2) {
        if (*s1==*s2){
            s1++; s2++;
        }
        else
            return *s1-*s2;
    }
    if ( *s1==0 && *s2==0 )
        return 0;
    else
    {
        if (*s1==0)
            return -1;
        else
            return 1;
    }
}

