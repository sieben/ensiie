#ifndef __VGA_H
#define __VGA_H

/* fonctions to print on vga screen */

void vgainit();
void vgaprintc(char c);			/* print a char */
void vgaprints(const char* s);		/* print a string */
void vgaprintf(const char* fmt,...);	/* basic printf */

#endif
