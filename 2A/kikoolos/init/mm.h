#ifndef __MM_H
#define __MM_H

#define	PAGESIZE 	4096
#define	RAM_MAXPAGE	0x10000

#define	VADDR_PD_OFFSET(addr)	((addr) & 0xFFC00000) >> 22
#define	VADDR_PT_OFFSET(addr)	((addr) & 0x003FF000) >> 12
#define	VADDR_PG_OFFSET(addr)	(addr) & 0x00000FFF
#define PAGE(addr)		(addr) >> 12

#define PAGING_FLAG 0x80000000	/* CR0 - bit 31 */
/* #define USER_OFFSET 0x40000000 */
#define USER_OFFSET 0x00800000
/* #define USER_STACK  0xE0000000 */
#define USER_STACK  0x00a00000

#ifdef __MM__
unsigned int *pd0;			/* kernel page directory */
unsigned int *pt0;			/* kernel page table */
unsigned char mem_bitmap[RAM_MAXPAGE / 8];	/* bitmap allocation de pages (1 Go) */
#else
extern unsigned char mem_bitmap[];
#endif

struct pd_entry {
	unsigned int present:1;
	unsigned int writable:1;
	unsigned int user:1;
	unsigned int pwt:1;
	unsigned int pcd:1;
	unsigned int accessed:1;
	unsigned int _unused:1;
	unsigned int page_size:1;
	unsigned int global:1;
	unsigned int avail:3;

	unsigned int page_table_base:20;
} __attribute__ ((packed));

struct pt_entry {
	unsigned int present:1;
	unsigned int writable:1;
	unsigned int user:1;
	unsigned int pwt:1;
	unsigned int pcd:1;
	unsigned int accessed:1;
	unsigned int dirty:1;
	unsigned int pat:1;
	unsigned int global:1;
	unsigned int avail:3;

	unsigned int page_base:20;
} __attribute__ ((packed));


/* Marque une page comme utilisee / libre dans le bitmap */
#define set_page_frame_used(page)	mem_bitmap[((unsigned int) page)/8] |= (1 << (((unsigned int) page)%8))
#define release_page_frame(p_addr)	mem_bitmap[((unsigned int) p_addr/PAGESIZE)/8] &= ~(1 << (((unsigned int) p_addr/PAGESIZE)%8))

/* Selectionne une page libre dans le bitmap */
int * get_page_frame(void);

/* Initialise les structures de donnees de gestion de la memoire */
void init_mm(void);

/* Cree un repertoire de page pour une tache */
unsigned int* pd_create(unsigned int*, unsigned int);

#endif
