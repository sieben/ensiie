#include <config.h>
#include "kernel.h"
#include "vga.h"
#include "sched.h"

void do_pates_bolo(int ex)
{
	uint16_t cs;
	uint32_t eip;
	uint32_t cr2 = 0x0;


	/* Récupération du pointeur d'instruction où l'erreur est survenue */


	/*
	 * En cas de faute de page, on sait (en regardant bien la pile noyau)
	 * où sont sauvegardés le CS:EIP de l'endroit où l'exception a été
	 * provoquée.  Par contre, lors d'une faute de page, un code d'erreur
	 * supplémentaire est empilé sur la pile noyau; il faut donc en
	 * tenir compte.
	 *
	 * En cas de faute de page, on récupère également la valeur de cr2, qui
	 * contient l'adresse à laquelle on a tenté de lire.
	 */
	if (ex != 0x0e)
	{
		asm("movl 32(%%ebp), %0\n" : "=r" (eip) : );
		asm("movw 36(%%ebp), %0\n" : "=r" (cs) : );
	}
	else
	{
		asm("movl %%cr2, %0\n" : "=r" (cr2) );
		asm("movl 36(%%ebp), %0\n" : "=r" (eip) : );
		asm("movw 40(%%ebp), %0\n" : "=r" (cs) : );
	}

	if (cs == USER_CS)
	{
		/* 
		 * Un process user a fait de la merde.
		 * Killons-le !
		 */
		set_current_attribute(current->sc, getcolor('r', 'n', 0, 0));
		kprintf(current->sc, "\n%s\nYou dun goofed!",
			(ex == 0x00) ? "Division par z\202ro" :
			(ex == 0x01) ? "Debug" :
			(ex == 0x02) ? "Interruption non masquable" :
			(ex == 0x03) ? "Breakpoint" :
			(ex == 0x04) ? "Overflow" :
			(ex == 0x05) ? "Bounds check fault" :
			(ex == 0x06) ? "Opcode non valable" :
			(ex == 0x07) ? "Coprocesseur indisponible" :
			(ex == 0x08) ? "D-D-D-DOUBLE FAULT" :
			(ex == 0x09) ? "Coprocessor segment overrun" :
			(ex == 0x0a) ? "TSS non valable" :
			(ex == 0x0b) ? "Segment non pr\202sent" :
			(ex == 0x0c) ? "Faute de pile" :
			(ex == 0x0d) ? "Faute de protection g\202n\202rale" :
			(ex == 0x0e) ? "Faute de page" :
			(ex == 0x0f) ? "R\202serv\202" :
			(ex == 0x10) ? "Erreur de coprocesseur" :
			(ex == 0x11) ? "Faute d'alignement" :
			(ex == 0x12) ? "Machine check exception" :
			(ex == 0x13) ? "Exception SIMD" :
			(ex == 0x1e) ? "Exception de s\202curit\202" :
			"Exception inconnue"
		);

		set_current_attribute(current->sc, getcolor('w', 'n', 0, 0));
		sys_exit(); /* Ne retourne pas */
	}




	vgaprintf("\nPAF! P\203tes bolognaises! ");
	vgaprintf("%s, ",
		(ex == 0x00) ? "Division par z\202ro" :
		(ex == 0x01) ? "Debug" :
		(ex == 0x02) ? "Interruption non masquable" :
		(ex == 0x03) ? "Breakpoint" :
		(ex == 0x04) ? "Overflow" :
		(ex == 0x05) ? "Bounds check fault" :
		(ex == 0x06) ? "Opcode non valable" :
		(ex == 0x07) ? "Coprocesseur indisponible" :
		(ex == 0x08) ? "D-D-D-DOUBLE FAULT" :
		(ex == 0x09) ? "Coprocessor segment overrun" :
		(ex == 0x0a) ? "TSS non valable" :
		(ex == 0x0b) ? "Segment non pr\202sent" :
		(ex == 0x0c) ? "Faute de pile" :
		(ex == 0x0d) ? "Faute de protection g\202n\202rale" :
		(ex == 0x0e) ? "Faute de page" :
		(ex == 0x0f) ? "R\202serv\202" :
		(ex == 0x10) ? "Erreur de coprocesseur" :
		(ex == 0x11) ? "Faute d'alignement" :
		(ex == 0x12) ? "Machine check exception" :
		(ex == 0x13) ? "Exception SIMD" :
		(ex == 0x1e) ? "Exception de s\202curit\202" :
		"Exception inconnue"
	);

	vgaprintf("pid=%d, eip=%04x:%08x", current->pid, cs, eip);
	
	if (ex == 0x0e)
		vgaprintf(", cr2=%08x", cr2);

	while(1);
}
