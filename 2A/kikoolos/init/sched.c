#define __SCHED__

#include <stdint.h>
#include <config.h>
#include "kernel.h"
#include "sched.h"
#include "screen.h"
#include "process.h"

#define NULL ((void*)0)

#define valid_process(i) (task_list[(i)].pid != -1)

struct task_struct task_list[MAX_PROCESSES];
struct task_struct* current;


void sched_init()
{
	int i;
	current = NULL;
	for (i = 0; i < MAX_PROCESSES; i++)
	{
		task_list[i].pid = -1;
	}
}


int make_new_pid()
{
	int i;
	int res = 0;

	for (i = 0; i < MAX_PROCESSES; i++)
		res = (task_list[i].pid > res) ? task_list[i].pid : res;

	return res + 1;
}

/* Renvoie le nombre de processus qui tournent (i.e. enregistrés auprès du
 * scheduler) */
int nprocs()
{
	int i, res = 0;
	for (i = 0; i < MAX_PROCESSES; i++)
		if (task_list[i].pid != -1)
			res++;
	return res;
}

static int next_active()
{
	int i;
	for (i = 0; i < MAX_PROCESSES; i++)
		if (valid_process(i) && task_list[i].active == ACTIVE)
			return i;

	return -1;
}


/* _eip : adresse virtuelle du 1er instruction à exécuter au sein du process
 * _esp : adresse virtuelle du pointeur de pile du nouveau processus
 * _cr3 : pointe vers le répertoire de pages du nouveau processus
 *
 * Renvoie le pid de la nouvelle tâche, ou -1 si ça a foiré
 */
int sched_add_task(uint32_t _eip, uint32_t _esp, uint32_t _cr3, 
		uint32_t _kstack_base)
{
	int i;
	uint32_t _eflags;

	/* Trouver une entrée task_struct vierge dans le tableau */
	for(i = 0; i < MAX_PROCESSES && task_list[i].pid != -1; i++);

	/* Plus de place? Dommage. */
	if (i == MAX_PROCESSES)
		return -1;

	/* Récupérer les eflags */
	asm("pushfl\n"		/* Empilement des flags dans la pile */
			"popl %%eax\n"		/* Dépilement dans un registre */
			"movl %%eax, %0\n"	/* Stockage dans _eflags */
			: "=m"(_eflags) :: "eax");

	_eflags |= 0x200;		/* Réactivation des interruptions */
	_eflags &= 0xffffbfff;		/* Désactivation du bit "nested task" */

	task_list[i].pid = make_new_pid();

	task_list[i].regs.eax = 0xabadbabe;
	task_list[i].regs.ecx = 0xcbadbabe;
	task_list[i].regs.edx = 0xdbadbabe;
	task_list[i].regs.ebx = 0xbbadbabe;
	task_list[i].regs.esp = _esp;
	task_list[i].regs.ebp = 0xebadbabe;
	task_list[i].regs.esi = 0x5badbabe;
	task_list[i].regs.edi = 0x4badbabe;

	task_list[i].regs.eip = _eip;
	task_list[i].regs.eflags = _eflags;

	task_list[i].regs.cs  = USER_CS;
	task_list[i].regs.ss  = USER_DS;
	task_list[i].regs.ds  = USER_DS;
	task_list[i].regs.es  = USER_DS;
	task_list[i].regs.fs  = USER_DS;
	task_list[i].regs.gs  = USER_DS;

	task_list[i].regs.cr3 = _cr3;

	task_list[i].kstack.ss0  = KERNEL_DS;
	task_list[i].kstack.esp0 = _kstack_base + PAGESIZE;

	task_list[i].kbd.read_ptr  = task_list[i].kbd.buf;
	task_list[i].kbd.write_ptr = task_list[i].kbd.buf;

	task_list[i].state 	= TASK_RUNNING;
	task_list[i].active 	= ACTIVE;
	task_list[i].time_left	= QUANTUM;
	task_list[i].needs_resched = 0;
	task_list[i].sc		= &sc_user[task_list[i].pid - 1];
	task_list[i].has_focus	= 0;

	if (nprocs() == 1)
		current = &task_list[i];

	return task_list[i].pid;
}

/*
 * mode = USER_MODE ou KERNEL_MODE selon si le scheduler a été appelé en
 * mode user ou un mode noyau
 */
static void prepare_switch(int mode)
{
	uint32_t kesp, eflags;
	uint16_t kss, ss, cs;

	default_tss.ss0 	= current->kstack.ss0;
	default_tss.esp0	= current->kstack.esp0;

	/*
	 * Empiler les registres nécessaires à une commutation logicielle
	 * par iret.  Ceux-ci ne sont pas les mêmes selon si le process
	 * était en mode noyau ou en mode user...
	 */
	ss	= current->regs.ss;
	cs	= current->regs.cs;
	eflags	= current->regs.eflags;
	eflags	|= 0x200;	/* Activation des interruptions */
	eflags	&= 0xffffbfff;	/* Désact. du bit "nested task */

	if (mode == USER_MODE)
	{
		kss	= current->kstack.ss0;
		kesp	= current->kstack.esp0;
	}
	else
	{
		kss	= current->regs.ss;
		kesp	= current->regs.esp;
	}

	/*
	 * Si la commutation fait qu'on reste en mode kernel, on n'empile
	 * ni ss, ni esp, car on ne change pas de DPL.
	 */
	asm("   mov %0, %%ss \n"
			"   mov %1, %%esp \n"
			"   cmp %[KMODE], %[mode] \n"
			"   je next \n"
			"   push %2 \n"
			"   push %3 \n"
			"next: \n"
			"   push %4 \n"
			"   push %5 \n"
			"   push %6 \n"
			"   push %7 \n"
			"   ljmp $0x10, $do_switch"
			:: \
			"m"(kss), \
			"m"(kesp), \
			"m"(ss), \
			"m"(current->regs.esp), \
			"m"(eflags), \
			"m"(cs), \
			"m"(current->regs.eip), \
			"m"(current), \
			[KMODE] "i"(KERNEL_MODE), \
			[mode] "g"(mode)
			);
}

/* Tronche de la pile à l'intérieur de schedule :
 *
 * esp -> |     .      |
 *        |     .      |
 *        |     .      |
 *        |     .      |
 * ebp -> |    ebp'    | ebp pour minikernel_irq0
 *        | addr. ret. | vers do_minikernel_irq0
 *        |     .      |
 *        |     .      |
 *        |     .      |
 * ebp'-> |    ebp     |
 * ebp'+0 | addr. ret. | vers minikernel_irq0
 * ebp'+4 |    ebx     |
 * ebp'+8 |    gs      |
 * ebp'+12|    fs      |
 * ebp'+16|    es      |
 * ebp'+20|    ds      |
 * ebp'+24|    edi     | 
 * ebp'+28|    esi     | 
 * ebp'+32|    ebp     |
 * ebp'+36|    esp     |
 * ebp'+40|    ebx     | 
 * ebp'+44|    edx     | 
 * ebp'+48|    ecx     | 
 * ebp'+52|    eax     | 
 * ebp'+56|    eip     |
 * ebp'+60|    cs      |
 * ebp'+64|   eflags   |
 * ebp'+68|    esp     |
 * ebp'+72|    ss      |
 */
void schedule()
{
	int n = nprocs();
	int i;

	cli();

	if (n <= 0)
		return;
	else if (current && current->pid != -1 && current->time_left > 0)
	{
		current->time_left--;
		return;
	}
	else
	{
		void* stack_frame;
		uint32_t _cr3;

		if (current && current->pid != -1)
		{
			/* Récupération du pointeur de pile */
			asm("mov (%%ebp), %%eax\n"
					"mov %%eax, %0"
					: "=m"(stack_frame) : );
			/* Sauvegarder tous les registres */
			asm("movl %%cr3, %%eax\n"
					"movl %%eax, %0\n"
					: "=m"(_cr3) :: "eax");

			/* stack_frame = (uint32_t*)(0x3000 - 76); */

			current->regs.eflags	= *(uint32_t*)(stack_frame + 64);
			current->regs.cs	= *(uint32_t*)(stack_frame + 60);
			current->regs.eip	= *(uint32_t*)(stack_frame + 56);
			current->regs.eax	= *(uint32_t*)(stack_frame + 52);
			current->regs.ecx	= *(uint32_t*)(stack_frame + 48);
			current->regs.edx	= *(uint32_t*)(stack_frame + 44);
			current->regs.ebx	= *(uint32_t*)(stack_frame + 40);
			current->regs.ebp	= *(uint32_t*)(stack_frame + 32);
			current->regs.esi	= *(uint32_t*)(stack_frame + 28);
			current->regs.edi	= *(uint32_t*)(stack_frame + 24);
			current->regs.ds	= *(uint32_t*)(stack_frame + 20);
			current->regs.es	= *(uint32_t*)(stack_frame + 16);
			current->regs.fs	= *(uint32_t*)(stack_frame + 12);
			current->regs.gs	= *(uint32_t*)(stack_frame + 8);
			current->regs.cr3	= _cr3;

			if (current->regs.cs == KERNEL_CS)
			{
				/*
				 * L'ancien pointeur de pile est simplement
				 * celui vers lequel il pointait avant qu'on
				 * sauvegardait tous ces registres
				 */
				current->regs.ss  = default_tss.ss0;
				current->regs.esp = (stack_frame + 68);
			}
			else
			{
				current->regs.ss  = *(uint32_t*)(stack_frame + 72);
				current->regs.esp = *(uint32_t*)(stack_frame + 68);
			}

			/* Marquer le processus comme arrêté et non commutable */
			current->state = TASK_STOPPED;
			current->active = EXPIRED;
		}

		/* Si aucun processus n'est commutable */
		i = next_active();
		while (i == -1)
		{
			/* Marquer tous les processus comme commutables */
			for(i = 0; i < MAX_PROCESSES; i++)
			{
				if (valid_process(i))
					task_list[i].active = ACTIVE;
			}
			i = next_active();
		}

		/* Prendre le processus commutable suivant */

		/* Basculer vers lui */
		current = &task_list[i];

		/* Réinitialiser le quantum */
		current->time_left = QUANTUM;

		/* Marquer comme étant en cours d'exécution */
		current->state = TASK_RUNNING;

		update_top();

		/* Restaurer son contexte et commuter */
		if (current->regs.cs == KERNEL_CS)
			prepare_switch(KERNEL_MODE);
		else
			prepare_switch(USER_MODE);

		/* Cet endroit ne sera jamais atteint */
		return;
	}
}

void sched_remove_task(struct task_struct* ts){
	/* Invalide l'entrée correspondante dans le task_struct */
	ts->pid = -1;
	ts->time_left = 0;
	ts->state = TASK_STOPPED;
	return;
}


struct task_struct* sched_get_task_by_pid(int pid)
{
	int i;

	for (i = 0; i < MAX_PROCESSES; i++)
		if (valid_process(i) && task_list[i].pid == pid)
			return &(task_list[i]);

	return NULL;
}

void update_top(){

	int i, disp_comma;
	kprintf(&sc_top,"\n");

	kprintf(&sc_top,"Processes: %d total",nprocs());

	if( nprocs > 0 ) 
		kprintf(&sc_top," (pids ");

	for( i = 0; i < MAX_PROCESSES; i ++ ){
		if( valid_process(i) ){
			kprintf(&sc_top, "%s%d", disp_comma == 1 ? ", " : "", task_list[i].pid);
			disp_comma = 1;
		}
	}
	kprintf(&sc_top,")");
	return;
}
