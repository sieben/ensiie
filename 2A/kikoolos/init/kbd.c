#define __KBD

#include "kbd.h"
#include "kernel.h"
#include <util.h>
#include "screen.h"
#include "sched.h"

/* Certaines touches génèrent 2 codes à la suite */
static int extended_key;

/* Gardons trace des modificateurs (shift, ctrl...) */
static int modifiers;

//static int cmd_pending;

#define MOD_SHIFT 1
#define MOD_CTRL 2
#define MOD_ALT 4
#define MOD_SUPER 8


enum _kbd_event {
	KBD_PRESS = 0,
	KBD_RELEASE = 1
};

unsigned char kbdmap[] = {
	0x1B, 0x1B, 0x1B, 0x1B,	/*      esc     (0x01)  */
	'1', '!', '1', '1',
	'2', '@', '2', '2',
	'3', '#', '3', '3',
	'4', '$', '4', '4',
	'5', '%', '5', '5',
	'6', '^', '6', '6',
	'7', '&', '7', '7',
	'8', '*', '8', '8',
	'9', '(', '9', '9',
	'0', ')', '0', '0',
	'-', '_', '-', '-',
	'=', '+', '=', '=',
	0x08, 0x08, 0x7F, 0x08,	/*      backspace       */
	0x09, 0x09, 0x09, 0x09,	/*      tab     */
	'q', 'Q', 'q', 'q',
	'w', 'W', 'w', 'w',
	'e', 'E', 'e', 'e',
	'r', 'R', 'r', 'r',
	't', 'T', 't', 't',
	'y', 'Y', 'y', 'y',
	'u', 'U', 'u', 'u',
	'i', 'I', 'i', 'i',
	'o', 'O', 'o', 'o',
	'p', 'P', 'p', 'p',
	'[', '{', '[', '[',
	']', '}', ']', ']',
	0x0A, 0x0A, 0x0A, 0x0A,	/*      enter   */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      ctrl    */
	'a', 'A', 'a', 'a',
	's', 'S', 's', 's',
	'd', 'D', 'd', 'd',
	'f', 'F', 'f', 'f',
	'g', 'G', 'g', 'g',
	'h', 'H', 'h', 'h',
	'j', 'J', 'j', 'j',
	'k', 'K', 'k', 'k',
	'l', 'L', 'l', 'l',
	';', ':', ';', ';',
	0x27, 0x22, 0x27, 0x27,	/*      '"      */
	'`', '~', '`', '`',	/*      `~      */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      Lshift  (0x2a)  */
	'\\', '|', '\\', '\\',
	'z', 'Z', 'z', 'z',
	'x', 'X', 'x', 'x',
	'c', 'C', 'c', 'c',
	'v', 'V', 'v', 'v',
	'b', 'B', 'b', 'b',
	'n', 'N', 'n', 'n',
	'm', 'M', 'm', 'm',
	0x2C, 0x3C, 0x2C, 0x2C,	/*      ,<      */
	0x2E, 0x3E, 0x2E, 0x2E,	/*      .>      */
	0x2F, 0x3F, 0x2F, 0x2F,	/*      /?      */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      Rshift  (0x36)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x37)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x38)  */
	' ', ' ', ' ', ' ',	/*      space   */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x3a)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x3b)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x3c)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x3d)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x3e)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x3f)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x40)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x41)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x42)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x43)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x44)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x45)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x46)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x47)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x48)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x49)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x4a)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x4b)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x4c)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x4d)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x4e)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x4f)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x50)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x51)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x52)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x53)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x54)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x55)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x56)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x57)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x58)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x59)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x5a)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x5b)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x5c)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x5d)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x5e)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x5f)  */
	0xFF, 0xFF, 0xFF, 0xFF,	/*      (0x60)  */
	0xFF, 0xFF, 0xFF, 0xFF	/*      (0x61)  */
};

static char keycode_to_char(unsigned short keycode)
{
	if (keycode > 0x61) return 0xff;

	return kbdmap[4 * (keycode - 1) + (modifiers & MOD_SHIFT ? 1 : 0)];
}

void kbd_init()
{
	extended_key = 0;
}

void do_minikernel_irq1()
{
	unsigned char c = inb(0x60);
	unsigned short key;



	switch(c)
	{
		case 0xe0: 
			extended_key = 1;
			return;
		case 0xfa: /* Acknowledge */
			return;
	}

	enum _kbd_event ev = (c & 0x80) >> 7;
	c &= 0x7f;

	key = c;
	key |= (extended_key ? 0xe000 : 0x0);
	extended_key = 0;

	switch(key)
	{
		case 0x1d: case 0xe01d:
			modifiers &= ~MOD_CTRL;
			modifiers |= (ev ? 0 : MOD_CTRL);
			break;
		case 0x2a: case 0x36:
			modifiers &= ~MOD_SHIFT;
			modifiers |= (ev ? 0 : MOD_SHIFT);
			break;
		case 0x38: case 0xe038:
			modifiers &= ~MOD_ALT;
			modifiers |= (ev ? 0 : MOD_ALT);
			break;

			/* Switch focus */

#if 0
		case 0x1:
			if(modifiers == MOD_ALT && ev == KBD_PRESS)  
				switch_focus(0);
			break;

#endif
		case 0x3b:
			if(modifiers == MOD_ALT && ev == KBD_PRESS)
				switch_focus(1);
			break;

		case 0x3c:
			if(modifiers == MOD_ALT && ev == KBD_PRESS)
				switch_focus(2);
			break;

		case 0x3d:
			if(modifiers == MOD_ALT && ev == KBD_PRESS)
				switch_focus(3);
			break;

		case 0x3e:
			if(modifiers == MOD_ALT && ev == KBD_PRESS)
				switch_focus(4);
			break;


		default:
			if (ev == KBD_PRESS)
			{
				*(focused->kbd.write_ptr) = keycode_to_char(key);
				focused->kbd.write_ptr++;
				focused->kbd.write_ptr = focused->kbd.buf + 
					((focused->kbd.write_ptr - focused->kbd.buf) % 256);
			}
			break;
	}
}


size_t read_vt(char * buf, size_t nmemb){

	size_t cpt = 0;

	while( cpt < nmemb ){

		while( current != focused || focused->kbd.read_ptr == focused->kbd.write_ptr );
				

		switch ( * focused->kbd.read_ptr ) {

			case 0xe: // Backspace
				 // Il faut vérifier que l'on a bien quelque
				 // chose à effacer avant de continuer 
				 if( cpt == 0 )
					 break;

				 cpt--;
				 buf--;

			case 0x0a: // Entrée
				 *buf = *focused->kbd.read_ptr;
				 cpt++;
				 buf++;
				 focused->kbd.read_ptr++;
				 goto end_read;
			

			default:
				 *buf = *focused->kbd.read_ptr;
				 cpt++;
				 buf++;


		}

		focused->kbd.read_ptr = focused->kbd.buf + ((focused->kbd.read_ptr - focused->kbd.buf) % 256);
		focused->kbd.read_ptr++;
	}

end_read:
	*buf = '\0';
	return cpt;
}
