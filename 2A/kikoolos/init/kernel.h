#ifndef __KERNEL_H
#define __KERNEL_H

#include <stdint.h>

#define cli() asm("cli")
#define sti() asm("sti")

/**********************************************************************/
/**** sub screen, and print function                               ****/
/**********************************************************************/

typedef struct _subscreen {
	char* vidmem;
	int nblines, nbcols;
	int cline, ccol;
 	uint8_t currentAttribute;
} subscreen;

extern subscreen sc_banner, sc_top, sc_shell, sc_ttyS0, sc_ttyS1, sc_user[4];

void kprintc(subscreen*, char c);
void kprints(subscreen*, const char* s);
void kprintf(subscreen*, const char* fmt, ...);

uint8_t getcolor(const char color, const char background, int blinking, int hl);
void color(subscreen* psc, const char color, const char background, int blinking, int hl);

/**********************************************************************/
/****  functions for initializing                                  ****/
/**********************************************************************/

void vga_init();
void ttyS_init();
void interrupt_init();
void main_init();

/**********************************************************************/
/**** other functions                                              ****/
/**********************************************************************/

void ttyS_set_uart_name(char *name);
int  ttyS_detect(unsigned int base);

void minikernel_irq0();
void do_minikernel_irq0();
void minikernel_irq1();
void do_minikernel_irq1();

#endif
