#include "kernel.h"
#include "process.h"
#include "screen.h"

void main_init()
{
	/* init irq0 entry 0x20 (timer) */
	{
		extern unsigned long idt_table[];
		long addr = (long) minikernel_irq0;
		unsigned short* pidt = ( unsigned short* ) ( idt_table+( 0x20<<1 ) );
		pidt[0]= addr;
		pidt[3]= (((long)addr)>>16)&0xffff;
	}
	/* init irq1 entry 0x21 (keyboard) */
	{
		extern unsigned long idt_table[];
		long addr=(long)minikernel_irq1;
		unsigned short* pidt=(unsigned short*)(idt_table+(0x21<<1));
		pidt[0]= addr;
		pidt[3]= (((long)addr)>>16)&0xffff;
	}
}

void do_minikernel_irq0()
{
	static int count = -1;
	static int time = 0;

	cli();
	count++;
	if ((count%1000) == 0) {
		kprintf(&sc_banner,
				"\rUptime: %3dd %2d:%02d:%02d",
				time / 86400,
				(time / 3600) % 24,
				(time / 60) % 60,
				time % 60
		       );
		/*
		set_current_attribute(&sc_banner, getcolor('r','w',0,0));
		kprintf(&sc_banner,"Rouge");
		set_current_attribute(&sc_banner, getcolor('b','w',0,0));
		kprintf(&sc_banner,"bleu");
		*/
		count = 0;
		time++;
	}
	schedule();
}


/* vi:ts=4:sw=4:
 */
