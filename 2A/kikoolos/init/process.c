#define __PROCESS__
#include <util.h>
#include <config.h>
#include "mm.h"
#include "gdt.h"
#include "kernel.h"
#include "process.h"
#include "sched.h"
#include "vga.h"

#define LOOP_THROTTLE 2000000

struct tss default_tss;

extern struct gdtdesc gdt[];

void test_task1();

void tss_init()
{
	/* initialisation TSS */
	default_tss.debug_flag 	= 0x0;
	default_tss.io_map	= 0x0;
	default_tss.esp0	= 0x3000;
	default_tss.ss0		= KERNEL_DS;

	/* initialisation descripteur TSS */
	init_gdt_desc((uint32_t)&default_tss, 0x67, 0xE9, 0x00, &gdt[6]);

	/* chargement du TR */
	/* on remarque que 0x30 = 0x06 (entrée dans GDT) << 3 */
	__asm__("movw $0x30, %ax\n"
			"ltr %ax");

	/* màj de ss:esp dans le TSS */
	__asm__("movw %%ss, %0\n"
			"movl %%esp, %1" 
			: "=m" (default_tss.ss0), "=m" (default_tss.esp0) : );
}



void test_task1()
{
	int i;

	char* plop = (char*)0x801000;
	plop[0] = '\n';
	plop[1] = 'b';
	plop[2] = 'o';
	plop[3] = 'n';
	plop[4] = 'j';
	plop[5] = 'o';
	plop[6] = 'u';
	plop[7] = 'r';
	plop[8] = '!';
	plop[9] = ' ';
	plop[10]= '0';
	plop[11] = '\0';

	while(1)
	{
		asm("movl %0, %%ebx\n"
				"movl $1, %%eax\n"
				"int $0x80" :: "m"(plop) : "eax", "ebx");
		for(i = 0; i < LOOP_THROTTLE; i++);
		plop[10]++;
		plop[10] = ((plop[10] - '0') % 10) + '0';
	}
	return;
}

void test_task2()
{
	int i, j;

	char* plop = (char*)0x801100;
	char* aurevoir = &plop[14];

	plop[0] = '\n';
	plop[1] = 'a';
	plop[2] = 'u';
	plop[3] = ' ';
	plop[4] = 'r';
	plop[5] = 'e';
	plop[6] = 'v';
	plop[7] = 'o';
	plop[8] = 'i';
	plop[9] = 'r';
	plop[10] = '!';
	plop[11] = ' ';
	plop[12] = '0';
	plop[13] = '\0';
	plop[14] = '\n';
	plop[15] = 'S';
	plop[16] = 'i';
	plop[17] = ' ';
	plop[18] = 'c';
	plop[19] = '\'';
	plop[20] = 'e';
	plop[21] = 's';
	plop[22] = 't';
	plop[23] = ' ';
	plop[24] = 'c';
	plop[25] = 'o';
	plop[26] = 'm';
	plop[27] = 'm';
	plop[28] = 'e';
	plop[29] = ' ';
	plop[30] = '\207';
	plop[31] = 'a';
	plop[32] = ',';
	plop[33] = ' ';
	plop[34] = 'j';
	plop[35] = 'e';
	plop[36] = ' ';
	plop[37] = 'm';
	plop[38] = 'e';
	plop[39] = ' ';
	plop[40] = 'c';
	plop[41] = 'a';
	plop[42] = 's';
	plop[43] = 's';
	plop[44] = 'e';
	plop[45] = ' ';
	plop[46] = '!';
	plop[47] = '\n';
	plop[48] = '\0';
	

	for(i = 0; i < 5; i++)
	{
		asm("movl %0, %%ebx\n"
				"movl $1, %%eax\n"
				"int $0x80" :: "m"(plop) : "eax", "ebx");
		for(j = 0; j < LOOP_THROTTLE; j++);
		plop[12]++;
		plop[12] = ((plop[12] - '0') % 10) + '0';
	}

	asm("movl %0, %%ebx\n"
		"movl $1, %%eax\n"
		"int $0x80\n" 		/* Affichage du message "je me casse" */
		"movl $0, %%eax\n"
		"int $0x80\n"		/* exit() */
		:: "m"(aurevoir) : "eax", "ebx");
}

int create_process( void * start, void * end, int size ){

	int i;
	unsigned int pages, start_page, kstack_start;
	unsigned int * page_directory;

	/* On copie le code à l'adresse specifiée */
	memcpy(start, end, size);

	/* Mise à jour du bitmap */

	start_page = (unsigned int) PAGE(start);

	if( size % PAGESIZE )
		pages = size / PAGESIZE + 1;
	else
		pages = size / PAGESIZE;

	for( i = 0; i < pages; i++ )
		set_page_frame_used(start_page + i);

	page_directory = pd_create(start,size);
	kstack_start = (unsigned int) get_page_frame();

	if ( kstack_start > 0x400000 ){
		vgaprintf("FAIL => Pas assez de mémoire => tu l'as dans l'os >_<\n");
		return(-1);
	}

	return kstack_start;

}

void sys_exit()
{
	cli();
	/* libérer la mémoire */
	pd_release_process_memory(current->pid);
	/* libérer la task_struct */
	sched_remove_task(current);
	/* commuter */
	schedule();
}

#if 0
int kill_process( int pid ){

	int i;
	int end_page, start_page;
	/* Appel au scheduler pour le supprimer de la liste des processus actifs */

	sched_remove_task(sched_get_task_by_pid(pid));

	/* Suppression des pages */

	for( i = 0; i < end_page; i++ ){
		release_page_frame(start_page + i);
	}

	return 0;
}
#endif
