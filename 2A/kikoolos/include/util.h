#ifndef __UTIL_H
#define __UTIL_H

#include <stdint.h>

/* basic types                      */
typedef unsigned int size_t;

/* standard tools                   */
void* memcpy(void* __dest, __const void* __src, size_t __n);
int   strlen(const char*p);
char* strcpy(char* dest, const char*src);
int   strcmp(const char* s1, const char* s2);

unsigned char inb(uint16_t port);
void outb(uint8_t value, uint16_t port);
uint8_t inb_p(uint16_t port);
uint16_t inw(uint16_t port);
uint16_t inw_p(uint16_t port);
void outb_p(uint8_t value, uint16_t port);
void outw (uint16_t value, uint16_t port);
void outw_p (uint16_t value, uint16_t port);
 
#endif
