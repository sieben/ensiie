#ifndef _STDIO_H_
#define _STDIO_H_

#include <stddef.h>
#include <stdarg.h>

extern int putchar(int c);

extern int puts(const char* s);

extern int printf(const char *format, ...);

extern int vprintf(const char *format, va_list ap);

extern int vsprintf(char* str, const char *format, va_list ap);

extern int vsnprintf(char* str, size_t size, const char *format, va_list ap);

extern int getchar(void);

#endif
