#ifndef _UNISTD_H_
#define _UNISTD_H_

#include <stddef.h>

void exit();

size_t write_vt(char* sz);

size_t read_vt(char* sz, size_t count);

#endif
