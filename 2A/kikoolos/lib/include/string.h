#ifndef _STRING_H_
#define _STRING_H_

#include <stddef.h>

extern int strcmp(const char* s1, const char* s2);

extern size_t strlen(const char *s);

extern char* strcpy(char* dest, const char* src);

extern char* strncpy(char* dest, const char* src, size_t n);

#endif
