#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>

int vprintf(const char* format, va_list ap)
{
	char buf[512];
	buf[0] = 'f';
	buf[1] = 'a';
	buf[2] = 'i';
	buf[3] = 'l';
	buf[4] = '\0';

	size_t ret = vsnprintf(buf, 512, format, ap);
	write_vt(buf);
	return ret;
}

