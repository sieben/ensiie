#include <stdio.h>
#include <string.h>
#include <stdarg.h>

int sprintf(char* str, const char* fmt, ...)
{
	va_list ap;
	size_t ret;
	va_start(ap, fmt);
	ret = vsnprintf(str, (size_t)-1, fmt, ap);
	va_end(args);
	return ret;
}
