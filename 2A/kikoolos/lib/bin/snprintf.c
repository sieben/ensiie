#include <stdio.h>
#include <string.h>
#include <stdarg.h>

int snprintf(char* str, size_t size, const char* fmt, ...)
{
	va_list ap;
	size_t ret;
	va_start(ap, fmt);
	ret = vsnprintf(str, size, fmt, ap);
	va_end(args);
	return ret;
}
