#include <stdio.h>
#include <string.h>
#include <stdarg.h>

int printf(const char* fmt, ...)
{
	va_list ap;
	size_t ret;
	va_start(ap, fmt);
	ret = vprintf(fmt, ap);
	va_end(args);
	return ret;
}
