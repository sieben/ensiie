#include <unistd.h>
#include <stdio.h>


int puts(const char* s)
{
	int ret;

	ret = write_vt((char*)s);
	write_vt("\n");

	return ((ret > 0) ? 1 : -1);
}
