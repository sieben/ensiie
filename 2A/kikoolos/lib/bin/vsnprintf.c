#include <stdio.h>
#include <string.h>
#include <stdarg.h>



static void vsnprintf_str(char** str, size_t* size, const char* src, int len,
		char fillwith, int placeleft, int signe)
{
	int i;

	if (len == 0)
	{
		if (signe)
		{
			*(*str)++ = signe;
			(*size)--;
		}
		strncpy(*str, src, *size);
		*str += (strlen(src) > *size) ? *size : strlen(src);
		*size -= (strlen(src) > *size) ? *size : strlen(src);
	}
	else
	{
		int slen = strlen(src) + (signe ? 1 : 0);
		len = (*size < len) ? *size : len;
		
		if (slen >= len) 
		{
			if (signe)
			{
				*(*str)++ = signe;
				(*size)--;
			}
			strncpy(*str, src, len - (signe ? 1 : 0));
			*str += (strlen(src) > len) ? len : strlen(src);
			*size -= (strlen(src) > len) ? len : strlen(src);
		}
		else if (placeleft)
		{
			if (signe)
			{
				*(*str)++ = signe;
				(*size)--;
			}
			strncpy(*str, src, len - (signe ? 1 : 0));
			*str += (strlen(src) > len) ? len : strlen(src);
			*size -= (strlen(src) > len) ? len : strlen(src);

			for (i = 0; i < (len - slen) && *size > 0; i++, (*size)--)
				*(*str)++ = fillwith;
		}
		else
		{
			if (signe && (fillwith != ' '))
			{
				*(*str)++ = signe;
				(*size)--;
			}
			for (i = 0; i < (len - slen) && *size > 0; i++, (*size)--)
				*(*str)++ = fillwith;
			if (signe && fillwith == ' ')
			{
				*(*str)++ = signe;
				(*size)--;
			}
			strncpy(*str, src, len - (signe ? 1 : 0));
			*str += (strlen(src) > len) ? len : strlen(src);
			*size -= (strlen(src) > len) ? len : strlen(src);
		}
	}
}


int vsnprintf(char *str, size_t size, const char* format, va_list ap)
{
	int len = 0;
	char* beg_str = str;
	char fillwith = ' ';
	int placeleft = 0;
	int signe = 0;
	char buf[100];

	while (*format && size != 0)
	{
		switch (*format)
		{
			case '%':
				format++;

				while (*format)
				{
					switch (*format)
					{
						case '%':
							format++;
							*str++ = '%';
							size--;
							goto leave_printarg;
						case 's':
							vsnprintf_str(&str, &size, va_arg(ap, char*), 
									len, fillwith, placeleft, 0);
							format++;
							goto leave_printarg;
						case 'c':
							*str++ = (char)(va_arg(ap, int) & 0xff);
							format++;
							size--;
							goto leave_printarg;
						case 'x':
						case 'X': {
							char* p = buf;
							unsigned int x = va_arg(ap, int);
							int i;
							for (i = 0; i < 7; i++, x <<= 4) 
								if ( (x & 0xf0000000) != 0 )
									break;
							for ( ; i < 8; i++, x <<= 4)
							{
								char c = (x >> 28) & 0xf;
								if (c >= 10)
									*p++ = c - 10 + 'a';
								else
									*p++ = c + '0';
							}
							*p = 0;
							vsnprintf_str(&str, &size, buf, len, fillwith, 
									placeleft, 0);
							format++;
							goto leave_printarg;
						    }
						case 'd':
						case 'i': {
							char* p = buf;
							int y = 1000000000;
							int x = va_arg(ap, int);
							if (x < 0)
							{
								signe = '-';
								x = -x;
							}
							else if (signe)
								signe = '+';

							for ( ; y >= 10; y /= 10)
								if ( (x/y) > 0 )
									break;

							for ( ; y >= 10; y /= 10)
							{
								*p++ = (x / y) + '0';
								x %= y;
							}

							*p++ = x + '0';
							*p = '\0';

							if (placeleft)
								fillwith = ' ';

							vsnprintf_str(&str, &size, buf, len, fillwith, 
									placeleft, signe);
							format++;
							goto leave_printarg;
						}
						case '0':
								  format++; fillwith='0'; continue;
						case '+':
								  format++; signe = 1; continue;
						case '-':
								  format++; placeleft = 1; continue;

						default:
								  if ( (*format < '1') || ('9' <= * format) )
									  continue;

								  while (*format && !((*format < '0') || 
											  ('9' < *format )))
								  {
									  len = len * 10 + *format - '0';
									  format++;
								  }
								  continue;
					}
				}
leave_printarg:
				len = 0;
				fillwith = ' ';
				placeleft = 0;
				signe = 0;
				continue;
			default:
				*str++ = *format++;
				size--;
				continue;
		}
	}

	*str++ = '\0';
	return (size_t)(str - beg_str);
}








/* vi:ts=4:sw=4:
 */
