#include <string.h>

size_t strlen(const char* s)
{
	int len;
	for (len = 0; *s++; len++);
	return len;
}
