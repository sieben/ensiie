#include <unistd.h>
#include <stdio.h>

int putchar(int c)
{
	char str[2];
	int ret;

	str[0] = (char)c;
	str[1] = '\0';

	ret = write_vt(str);

	return ((ret == 1) ? c : -1);
}

