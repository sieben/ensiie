#include <string.h>

char* strcpy(char* dest, const char* src)
{
	char* p = dest;
	while ((*dest++ = *src++) != 0);
	return p;
}

