/*
 * entry to minikernel
 * 
 * initialisation are:
 *   - vga driver
 */

#include <util.h>
#include "../init/kernel.h"
#include "../init/process.h"
#include "../init/sched.h"
#include "../init/vga.h"
#include "../init/mm.h"

extern struct tss default_tss;

extern void test_task1();
extern void test_task2();

void mini_kernel()
{
	uint32_t* pd[4];
    extern uint32_t* __task_table[8];
    int i;

	color(&sc_banner, 	'm', 'n', 0, 1);
	color(&sc_top,		'c', 'n', 0, 1);

	kprintf(&sc_banner,"\n\n\n\n"
			"###############################################\n"
			"Kikoolos kernel - powered by x0r and Sieben FTW\n"
			"###############################################\n");


	vgaprints("enter mini kernel\n");
	kprintf(&sc_shell, "KikoolOS Process Control shell\n%% ");

    asm("cli\n"); /* Évitons de se faire pourrir */

    for(i = 0; i < 4; i++)
    {
        uint32_t addr = 0x200000 + 0x200000 * i;
        uint32_t kstack_start;

        if (__task_table[2*i] != 0x0)
        {
            uint32_t* prog_hdr = __task_table[2*i];
            uint32_t* j;

            /* Création d'un page directory */
            pd[i] = pd_create((unsigned int*)addr, 0x200000);

            /* Création d'une pile noyau */
            kstack_start = (uint32_t) get_page_frame();
            
            /* Enregistrement de la tâche auprès du scheduler */
            sched_add_task(0x800000, 0xa00000, pd[i], kstack_start);

            /* Copie du text */
            memcpy((void*)addr + (prog_hdr[0] - 0x800000),
                    (void*)prog_hdr[1],
                    (unsigned int)prog_hdr[2]);

            /* Copie du data */
            memcpy((char*)addr + (prog_hdr[3] - 0x800000),
                    (void*)prog_hdr[4],
                    (unsigned int)prog_hdr[5]);

            /* Initialisation du BSS */
            for (j = (uint32_t*)prog_hdr[6]; 
                    j < (uint32_t*)(prog_hdr[6] + prog_hdr[7]); j++)
                *j = 0;
        }

    }

    /* Donner au shell le focus (ou pas, en fait) */
    switch_focus(1);

	asm("cli \n"		        /* Évitons de se faire pourrir */
		"movl $0x3000, %0 \n"	/* On sauvegarde un pointeur de pile kernel */
		"movw $0x2B, %%ax \n"	/* ainsi que le data segment qui va bien */
		"movw %%ax, %%ds \n"	/* dans le TSS */
		"movl %1, %%eax \n"	    /* On charge CR3 avec l'adresse du nouveau */
		"movl %%eax, %%cr3 \n"  /* page directory */
		"push $0x2B \n"	        /* Data segment user */
		"push $0xa00000 \n"	    /* Pointeur de pile user */
		"pushfl \n"		        /* Modification des flags CPU */
		"popl %%eax \n"	        /* On pushe, on récupère dans %eax */
		"orl $0x200, %%eax \n"  /* Réactivation des interruptions dès qu'on 
                                 * commute */
		"and $0xffffbfff, %%eax \n" /* Désactivation du bit "nested task"
		   		                 * (sinon ça va chier) */
		"push %%eax \n"	        /* On remet les flags modifiés dans la pile */
		"push $0x23 \n"	        /* Code segment user */
		"push $0x800000 \n"	    /* Adresse virtuelle user */
		"iret" 		            /* Et hop, on commute ! */
		: "=m" (default_tss.esp0) : "m"(pd[0]));
}

// vi:et:sw=4:ts=4:
