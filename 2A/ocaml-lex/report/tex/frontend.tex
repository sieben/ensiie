\chapter{Front-end : implémentation d'un analyseur syntaxique}

\section{Introduction}
On souhaite implémenter un analyseur syntaxique basique qui
 transforme un fichier d'entrée au format lex en un 
arbre de syntaxe abstraite, structure de données qui 
exprime la façon dont les règles de syntaxe sont utilisées dans le fichier d'entrée.
Cette partie est la première étape de la compilation du fichier "lex" en code OCaml.

L'implémentation de cet analyseur syntaxique est réalisée 
au moyen de deux outils d'Objective Caml : \texttt{ocamllex} 
et \texttt{ocamlyacc} qui sont respectivement un 
générateur de \textsl{"lexers"}, ou analyseurs lexicaux, 
et un générateur de \textsl{"parsers"}, ou analyseurs syntaxiques.

On va donc tout d'abord expliciter la structure de l'analyseur
syntaxique réalisée et ensuite discuter des choix d'implémentation qui ont été faits.

\section{Structure de l'analyseur syntaxique}

\subsection{Présentation}
La structure choisie pour cet analyseur syntaxique est traditionnelle. La méthode utilisée est la suivante :
\begin{enumerate}
\item Définition du type des expressions régulières à reconnaître
\item Définition de la structure de données de l'arbre de syntaxe abstraite à construire
\item Décomposition du fichier d'entrée en unités lexicales
\item Analyse syntaxique du flux d'unités lexicales et remplissage de l'arbre de syntaxe abstraite
\item Affichage de l'arbre de syntaxe abstraite
\end{enumerate}

D'après ce plan, on peut en déduire qu'il est possible de créer trois modules distincts utilisés par une fonction principale.

On a choisi de créer un fichier OCaml de définitions 
appelé \texttt{expression.ml}. Il implémente 
les points 1., 2. et 5. Ce fichier est compilé en module.

Ensuite on doit générer l'analyseur lexical qui va 
implémenter le point 3. au moyen d'un fichier \texttt{lexer.mll} 
compilé d'abord avec \texttt{ocamllex} puis compilé en module.

Puis on génère l'analyseur syntaxique qui implémente le point 4. avec 
un fichier \texttt{parser.mly} compilé d'abord avec \texttt{ocamlyacc} puis compilé en module.

Enfin on utilise ces trois modules dans un 
fichier principal \texttt{frontend.ml} qui implémente la méthode décrite ci-dessus.

\subsection{Définitions}
Le fichier \texttt{expression.ml} contient les définitions et l'initialisation de l'arbre de syntaxe abstraite.
Les définitions nécessaires à l'analyseur syntaxique que l'on souhaite réaliser sont les suivantes :
\begin{itemize}
\item Le type des expressions régulières. Ce type doit 
correspondre à la définition de la grammaire formelle du langage 
à analyser syntaxiquement. Concrètement, ce type doit 
être en accord avec les règles du fichier \texttt{parser.mly}.
\item Le type de l'arbre de syntaxe abstraite.
\item La fonction qui a un une expression régulière associe sa représentation sous forme d'arbre.
\end{itemize}

\subsection{Analyseur lexical}
Le fichier \texttt{lexer.mll} a une syntaxe particulière qui ressemble 
au fichier d'entrée de format lex. En effet les fichiers au format \textit{.mll} 
sont au programme \texttt{ocamllex} ce que les fichiers au format \textit{.lex} sont à \texttt{lex}.
Il comprend une en-tête contenant du code OCaml indiquant que ce module utilse les 
modules \texttt{Expresssion} et \texttt{Parser}. De plus on initialise des 
buffers qui vont servir à enregistrer les parties en-tête, trailer, 
et les couples d'expressions régulières et d'actions dans l'arbre de syntaxe abstraite.
On trouve ensuite une section de définitions pour les alias des caractères normaux et spéciaux.

Ensuite on trouve la section des règles qui est divisée en deux groupes :
\begin{itemize}
\item Les règles qui remplissent l'arbre de syntaxe abstraite avec 
des chaînes de caractères correspondant à des morceaux du fichier d'entrée (l'entête, le trailer, etc\ldots).
\item La règle d'analyse syntaxique des expressions régulières 
destinée à être appliquée à la partie règles de l'arbre de syntaxe abstraite.
\end{itemize}

On voit ici qu'on a choisi d'abord de "parser" le fichier d'entrée pour 
le diviser en trois parties (l'en-tête, les règles et le trailer) 
puis ensuite d'analyser syntaxiquement les règles. Pour ce faire, il 
faut analyser lexicalement cette même partie et produire des "tokens" 
ou unités syntaxiques qui serviront à l'analyseur syntaxique.

\subsection{Analyseur syntaxique}
Le fichier \texttt{parser.mly}, de manière analogue au fichier \texttt{lexer.mll} 
a une syntaxe particulière qui ressemble aussi au format lex. Cette 
syntaxe (ainsi que celle des fichiers \texttt{.mll}) est décrite 
dans la documentation officielle trouvable à cette \href{http://caml.inria.fr/pub/docs/manual-ocaml/manual026.html}{adresse}.

L'analyseur syntaxique est donc décrit sous une forme simple :
\begin{itemize}
\item une en-tête qui indique ici d'utiliser le module expression pour avoir accès au type 
correspondant aux expressions régulières définies plus haut.
\item la définition des unités syntaxiques valides
\item la définition des règles d'associativité des unités syntaxiques.
\item la définition de la grammaire de départ "main".
\item le type d'argument passé à "main".
\item les grammaires du langage formel reconnu :
	\begin{itemize}
	\item la grammaire de "main"
	\item la grammaire des expressions régulières qui définit l'agencement 
des règles de syntaxe du langage des expressions régulières que l'on souhaite reconnaître.
	\end{itemize}
\end{itemize}

L'analyse syntaxique sert donc bien à "parser" la partie règles du fichier d'entrée au format lex.

\subsection{Fonction principale}
La fonction principale qui réalise l'analyse syntaxique comporte deux phases.

Tout d'abord on analyse lexicalement le fichier d'entrée ce qui remplira la 
structure de données \texttt{arbre\_syntaxe} définie dans \texttt{expression.ml} 
avec les chaînes de caractères de l'en-tête et du trailer et aussi avec 
la liste de couples de chaînes d'expressions régulières et d'actions.

Ensuite on analyse syntaxiquement les règles stockées dans la structure 
de données. Pour cela on utilise une fonction qui "parse" une 
chaîne de caractères, ici les chaînes de caractères des expressions régulières stockées.

En même temps que l'analyse syntaxique on peut afficher 
à l'écran l'arbre de syntaxe abstraite à l'aide de 
quelques fonctions auxilliaires. De même, on peut transformer 
l'arbre de syntaxe abstraite en automate fini en utilisant les expressions du Middle-End. 

\section{Choix d'implémentation}

\subsection{Structures de données}
On a choisi un type produit avec enregistrements modifiables qui comporte trois champs :
\begin{itemize}
\item \texttt{entete} une chaîne de caractères qui contient l'en-tête
\item \texttt{regles} une liste de couple de chaînes 
de caractères qui correspond à la liste des couples (expression régulière, action)
\item \texttt{trailer} une chaîne de caractères qui contient le trailer du fichier
\end{itemize}

\subsection{Choix de l'alphabet reconnu et des jetons générés par le lexer}
On choisit de reconnaître l'alphabet décrit dans le sujet 
en produisant des jetons appropriés à l'analyse syntaxique. Les caractères 
spéciaux sont au nombre de 12. Tous les autres caractères sont 
considérés comme normaux du point de vue du lexer.

\subsection{Grammaire ambigüe des expressions régulières}
Pour respecter la priorité des opérations devant celle de la disjonction 
"a ou b" et de la priorité de la disjonction devant la concaténation "ab" 
on utilise une grammaire qui reconnaît notre langage.

\subsection{Visualisation de l'arbre de syntaxe abstraite}
Les fonctions auxilliaires permettent d'imprimer des expressions sur la 
sortie standard mais il est aussi possible de générer des fichiers 
de visualisation tels que des fichiers DOT.

\section{Conclusion}

On a tout d'abord créé un fichier de définitions, puis 
généré un analyseur syntaxique et ensuite un 
analyseur lexical.

Enfin, avec ces trois éléments utilisés par la 
fonction principale, on peut générer un exécutable.
Au final on a réalisé une implémentation de l'analyse syntaxique 
du fichier \textit{.ml} et on en a déduit un arbre de syntaxe abstraite 
que l'on peut aisément transformer en automate avec les fonctions du Middle-End.

Les outils \texttt{ocamllex} et \texttt{ocamlyacc} ont été utiles pour 
la réalisation de cet analyseur syntaxique. On a de plus la possibilité de 
vérifier la correction de notre programme par ses retours sur le terminal.

