\chapter{Middle-end : compilation d'expression régulières vers des automates}

\section{Introduction}

L'arbre de syntaxe abstraite a été obtenu dans la partie
précédente, qui faisait la traduction du fichier \textit{.lex} en une
structure de données, ici un arbre. Cette partie s'attarde
à optimiser cette structure de données en tirant parti de
la représentation des données sous forme d'automate fini.

Toutefois, la traduction doit se faire d'étape à étape et, comme
le sujet l'indique, on passera d'abord de l'arbre de syntaxe
abstraite à un automate non-déterministe avec $\epsilon$-transitions,
puis à un automate non-déterministe sans $\epsilon$-transitions, et enfin à
un automate fini déterministe. Sous cette forme, on a la
possibilité d'optimiser la représentation par minimisation de l'automate
fini déterministe. On arrivera à un automate fini déterministe minimal
qui sera la forme finale de notre représentation.

\section{Expressions régulières et automates non-détérministes}

Dans la partie Front-end on a choisi de représenter le langage reconnu par
le parser de la façon suivante. On s'occupe ici de la reconnaissance
des expressions régulières ce qui correspond au type \texttt{regexp} décrit
dans le fichier \texttt{expression.ml}

\section{Automates non-déterministes et expressions rationnelles}

\subsection{Automates non-déterministes}

Nous représentons un \emph{état} d'un \emph{automate fini
non-déterministe} (NFA) par un enregistrement à quatre champs du
type suivant :

\begin{itemize}

\item Le champ \texttt{terminal} est un booléen indiquant si l'état est
terminal ou non.

\item Le champs \texttt{trans} contient la liste
 des transitions dont l'état en question est
l'origine.

\item Le champ \texttt{index} sert à identifier les états de
manière unique : deux états différents portent des index différents.

\item Le champ \texttt{eps\_trans} contient la liste des $\epsilon$-transitions
dont l'état en question est l'origine.

\end{itemize}


On définit tout d'abord une compteur entier « global », dont le
contenu initial est $0$. À chaque appel de la
fonction \texttt{nfa\_create}, ce compteur
est incrémenté d'une unité et un nouvel état est créé.

Pour ajouter une transition, il suffit de modifier en place une des
listes \texttt{trans} ou \texttt{eps\_trans} de l'état origine.

\subsection{Construction de Thompson}

L'algorithme de transformation d'une expression rationnelle en automate
que nous allons employer est connu sous le nom de «~construction de
Thompson~».  Les automates qu'il produit ont la particularité d'avoir
un seul état terminal (appelé par symétrie \emph{état final}).  De
plus aucune transition ne sort de l'état final.

La construction de Thompson procède par récurrence sur la structure de
l'expression rationnelle.

Étant donnés deux automates de Thompson $A$ et $B$ reconnaissant
respectivement les langages $L_a$ et $L_b$, on peut construire un
automate de Thompson reconnaissant le langage $L_a \cup L_b$ de la
manière suivante :

\begin{figure}[h!]
\centering
\begin{VCPicture}{(0,-1)(2,1)}
% states
\State[a1]{(1,1)}{a1}
\State[s1]{(0,0)}{s1} \State[s2]{(2,0)}{s2} \State[a2]{(1,-1)}{a2}
% initial--final
\Initial{s1} \Final{s2}
% transitions 
\ArcL{s1}{a1}{\epsilon} \ArcL{a1}{s2}{\epsilon} \ArcR{s1}{a2}{\epsilon}
\ArcR{a2}{s2}{\epsilon}
%
\end{VCPicture}
\end{figure}

De même, on peut construire un automate de Thompson reconnaissant le
langage $L_1 L_2$ comme suit :

\begin{figure}[h!]
\centering
\begin{VCPicture}{(-0.5,-0.5)(1.5,0.5)}
% states
\State[s3]{(0,0)}{s3} \State[s4]{(1.5,0)}{s4}
% initial--final
\Initial{s3} \Final{s4}
% transitions 
\EdgeL{s3}{s4}{\epsilon}
%
\end{VCPicture}
\end{figure}

Enfin, étant donné un automate de Thompson $a$ reconnaissant le
langage $L$, l'automate suivant reconnaît le langage $L^*$ :

\begin{figure}[h!]
\centering
\begin{VCPicture}{(-0.5,-0.5)(1.5,1)}
% states
\State[s5]{(0,0)}{s5} \State[s6]{(1.5,0)}{s6}
% initial--final
\Initial{s5} \Final{s6}
% transitions 
\EdgeL{s5}{s6}{\epsilon} \LoopN{s5}{\epsilon}
%
\end{VCPicture}
\end{figure}

La fonction \texttt{thompson\_aux} construit récursivement
l'automate en suivant la structure de l'expression régulière.

Pour obtenir la fonction \texttt{thompson}, il suffit d'appeler
la fonction précédente et de marquer l'état final de l'automate
obtenu comme terminal.

Il est a noter que toutes les constructions comme $L^+$ ou bien
des automates reconnaissant des ensembles de mots reposent
sur des versions dérivées de ces constructions. Le code source
permet souvent de déduire les opérations utilisées.

\section{Déterminisation}

Un automate qui reconnaît les mêmes chaînes que
l'expression rationnelle de départ est obtenu via l'algorithme précédent.
Il est nécessaire de programmer une
fonction qui teste si une chaîne est reconnue ou non par l'automate.
La réalisation de ce test sur un automate non déterministe est
susceptible d'être peu efficace. En effet, puisque plusieurs transitions
portant le même caractère peuvent sortir du même état, il faut
quelquefois essayer de nombreux chemins qui épellent la chaîne à
reconnaître.  C'est la raison pour laquelle il est généralement
préférable d'effectuer la reconnaissance sur un automate
\emph{déterministe}.  L'objet de cette partie est donc de produire un
tel automate à partir de celui obtenu grâce à la construction de
Thompson.

\subsection{Automates déterministes}

Nous représentons un état d'un automate déterministe (DFA) par un
enregistrement du type suivant :

\begin{itemize}

\item Le champ \texttt{dterminal} est un booléen indiquant si l'état est
terminal ou non.

\item Un tableau \texttt{dtrans} à 256 cases (une par
caractère) décrit les transitions dont l'état est l'origine : étant
donnés un état $s$ et un entier $i$ compris entre $0$ et $255$,
$s.\mathit{dtrans}.(i)$ est l'extrémité de la transition d'origine $s$
et étiquetée par $c_i$.  Par souci de simplicité, nous supposons que
chaque état est origine d'exactement une transition étiquetée par
chaque caractère.

\end{itemize}

Dans notre représentation des automates, chaque état contient
directement la liste de ses successeurs.  Ainsi, pour désigner un
automate, il suffit de donner son état initial.

Pour tester si un automate déterministe reconnaît une chaîne, il
suffit de lire cette dernière caractère par caractère en effectuant
simultanément les transitions sur l'automate.  La chaîne est
reconnue si le dernier état obtenu est terminal.

\subsection{Construction des sous-ensembles}

L'algorithme que nous allons mettre en \oe{}uvre pour déterminiser un
automate est connu sous le nom de «~construction des sous-ensembles~».
Les états de l'automate
déterministe correspondent à des ensembles d'états de l'automate de
départ : tous les états qu'on peut atteindre à partir de l'état
initial en suivant une certaine chaîne de caractères.

L'état initial de l'automate déterministe est l'ensemble des états
qu'on peut atteindre en suivant la chaîne vide, c'est-à-dire l'état
initial de l'automate de départ, plus tous les états qu'on peut
atteindre à partir de l'état initial en suivant uniquement des
$\epsilon$-transitions.  L'état correspondant à l'ensemble d'états
${s_1, \ldots, s_n}$ est terminal si et seulement si un des états
$s_1, \ldots, s_n$ est terminal.

Pour voir où mène la transition sur un caractère $c$ issue de
l'ensemble d'états $\{s_1, \ldots, s_m\}$, on regarde toutes les
transitions sur $c$ issues des états $s_1$ à $s_m$ dans l'automate
initial.  Soient $t_1, \ldots, t_n$ les états auxquelles elles mènent.
Soit $\{t_1, \ldots, t_n, t'_1, \ldots, t'_{n'}\}$ la
$\epsilon$-fermeture de cet ensemble d'états (c'est-à-dire les états
accessibles à partir de $t_1, \ldots, t_n$ en suivant uniquement des
$\epsilon$-transitions).  On ajoute alors, dans l'automate
déterministe produit, une transition sur $c$ depuis l'état $\{s_1,
\ldots, s_m\}$ vers l'état $\{t_1, \ldots, t_n, t'_1, \ldots,
t'_{n'}\}$.  On répète ce procédé jusqu'à ce qu'il soit impossible
d'ajouter de nouvelles transitions.

Nous représenterons les ensembles d'états (d'automates
non-déterministes) simplement par des listes de type
\texttt{nfa\_state list} (nous supposerons qu'un même état
apparaît au plus une fois dans une telle liste).

On remarque que la représentation d'un ensemble d'état n'est pas unique : à un même
ensemble correspond plusieurs listes ordonnées différemment.  C'est
pourquoi, pour pouvoir identifier de manière unique un ensemble
d'état, nous définissons son \emph{empreinte} comme la liste
\textbf{triée} des numéros de ses états.

\subsubsection{$\epsilon$ fermeture}

Étant donné un ensemble $S$ d'états d'un automate non-déterministe, on
définit son $\epsilon$-fermeture comme l'ensemble des états
accessibles à partir d'un état de $S$ en traversant zéro, une ou
plusieurs $\epsilon$-transitions.

Le corps de notre fonction \texttt{epsilon\_closure} est une
fonction récursive, \texttt{aux}, qui prend deux arguments : (1)
la liste \texttt{accu} des états qui font parti de la fermeture
et qui ont déjà été traités et (2) la liste des états à traiter.
Afin de s'assurer que la fonction termine même en présence de cycles
d'$\epsilon$-transitions, on teste si un état est dans
l'accumulateur (\texttt{accu}) avant de le traiter.

\subsubsection{Table des transitions}

Soit $S$ un ensemble d'états (d'un automate non-déterministe).  On
appelle \emph{table des transitions} de $S$ le tableau $t$ à 256 cases
tel que $t.(i)$ contienne la liste des états accessibles en effectuant
une transition étiquetée par le caractère $c_i$ depuis un état de $S$.

Le corps de notre fonction de déterminisation est une fonction
récursive, \texttt{transl} qui prend pour argument une liste
d'états de l'automate non-déterministe et retourne l'état de
l'automate déterministe les représentant.

Chaque état de l'automate déterministe est stocké lors de sa
création dans la (référence de) liste \texttt{store}, associé à
l'empreinte de l'ensemble qu'il représente.  Ainsi, lors de chaque
appel à la fonction \texttt{transl}, deux cas peuvent se produire
:

\begin{itemize}

\item L'ensemble d'états passé en argument (\texttt{list}) a déjà
été rencontré.  L'état associé dans la liste \texttt{store} est
alors directement retourné.

\item L'ensemble d'états est considéré pour la première fois.  Dans
ce cas, un nouvel état est créé et stocké dans la liste
d'association.  Ses transitions sont ensuite calculées (ce qui
nécessite des appels récursifs à \texttt{transl} de manière à
construire les extrémités des transitions).  Enfin, l'état est
marqué terminal si et seulement si un des états de l'ensemble
qu'il représente est lui-même terminal.

\end{itemize}

\section{Limites de l'algorithme utilisé}

L'automate obtenu après la déterminisation peut comporter un grand
nombre d'états.  Il peut donc être souhaitable de chercher à réduire
sa taille de manière à obtenir un automate équivalent (c'est-à-dire
reconnaissant le même langage) mais comportant un nombre d'état
minimal.  Ce procédé est appelé \emph{minimisation}.  Il consiste à
identifier des états \emph{équivalents} : deux états sont équivalents
si les suffixes du langage reconnus par l'automate à partir de ces
états sont égaux.

