type token =
  | INT of (int)
  | REG of (string)
  | INS of (string)
  | LPAR
  | RPAR
  | EOL
  | LW
  | SW
  | SYSCALL
  | COMMA
  | EOF

val s :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> Mips.mips list
