open Coloring

let e_c = empty 

open Graph

module Labels = struct
  type t = Inter | Prefer
  let compare = compare
  let default = Inter
end

module Reg = struct
  type t = Register.reg
  let equal = (=)
  let hash = Hashtbl.hash
  let compare = compare
end

module MyGraph = Persistent.Graph.ConcreteLabeled(Reg)(Labels)

type graph = MyGraph.t

let degree g r = 
  MyGraph.fold_succ_e 
    (fun e n -> match MyGraph.E.label e with
	Labels.Inter -> n + 1 
      | Labels.Prefer -> n
    )
    g r 0 


  
let prefer_out g r = 
  MyGraph.fold_succ_e
    (fun e r -> MyGraph.E.label e = Labels.Prefer || r) 
    g r false 
    
let find_register p g = 
  MyGraph.fold_vertex (fun v r -> if 
      not (Register.is_real v) && p v then Some v else r) g None


module N = Oper.Neighbourhood(MyGraph)

let neighbour g a b = 
 try MyGraph.fold_succ_e 
    (fun e r ->
      match MyGraph.E.label e with
	  Labels.Prefer -> r
	| Labels.Inter -> b = MyGraph.E.dst e || r) g a false 
   with _ -> false
 
let neighbours g v = 
  let l = N.list_from_vertex g v in
  List.fold_left (fun r w -> 
    match MyGraph.E.label (MyGraph.find_edge g v w) with
	Labels.Inter -> w::r
      | Labels.Prefer -> r)
    [] l

let join_nodes g a b = 
  let ab = Register.join a b in
  let g = MyGraph.add_vertex g ab in
  let g = MyGraph.fold_succ_e
    (fun e g -> 
      if MyGraph.E.dst e = b then g else
	let f = MyGraph.E.create ab (MyGraph.E.label e) (MyGraph.E.dst e) in
	MyGraph.add_edge_e g f) 
    g a g in
  let g = MyGraph.fold_succ_e
    (fun e g -> 
      if MyGraph.E.dst e = a then g else
	let f = MyGraph.E.create ab (MyGraph.E.label e) (MyGraph.E.dst e) in
	MyGraph.add_edge_e g f) 
    g b g in
  MyGraph.remove_vertex (MyGraph.remove_vertex g a) b, ab
      
let find_preference criterion g =
  MyGraph.fold_edges_e 
    (fun e r -> match MyGraph.E.label e with
	Labels.Prefer -> 
	  let a, b = MyGraph.E.src e, MyGraph.E.dst e in
	  if (not (Register.is_real a && Register.is_real b)) && 
	    criterion a b then Some(a,b)
	  else r
      | Labels.Inter -> r)
    g None 

let remove_preferences g r = 
  MyGraph.fold_succ_e
    (fun e g -> match MyGraph.E.label e with
	Labels.Inter -> g
      | Labels.Prefer -> MyGraph.remove_edge_e g e)
    g r g

module C = Oper.Choose(MyGraph)

let choose g = C.choose_vertex g

let empty = MyGraph.empty

let is_empty = MyGraph.is_empty

let add_interference v w g = 
  if v <> w then
    MyGraph.add_edge (try MyGraph.remove_edge g v w with _ -> g) v w
  else g

let add_preference v w g = 
  if v = w || (Register.is_real v && Register.is_real w) ||neighbour g v w
  then g 
  else
    let e = MyGraph.E.create v Labels.Prefer w in 
    MyGraph.add_edge_e g e

let add_reg g s= MyGraph.add_vertex g s
let remove_reg g s = MyGraph.remove_vertex g s

module ToDot = struct
  include MyGraph

  let graph_attributes g = []
  let vertex_attributes v = []
  let edge_attributes e = match E.label e with
      Labels.Inter -> [ `Dir `None ]
    | Labels.Prefer -> [ `Dir `None; `Style `Dashed ]
  let default_vertex_attributes v = []
  let default_edge_attributes e = []
  let vertex_name v = "\"" ^ Register.to_string v ^ "\""
  let get_subgraph v = None
end


let print_graph_coloring graph coloring =
  let module TD = 
      struct 
	include ToDot
	let vertex_attributes v =
	  try
	    match find v coloring with
		Color w -> [ `Fillcolor (Register.to_color w); 
			     `Style `Filled ]
	      | Spill j -> [ `Fillcolor (j*0x404040); 
			     `Style `Filled;
			     `Fontcolor (if j <= 2 then 0xFFFFFF else 0) ]
	  with Not_found -> []
      end
  in
  let module L = Graphviz.Dot(TD) in
  let tmp = Filename.temp_file "inter_graph" ".svg" in
  let to_dot = Unix.open_process_out ("dot -Tsvg -o" ^ tmp) in
  L.output_graph to_dot graph;
  flush to_dot;
  match Unix.close_process_out to_dot with
      Unix.WSIGNALED (-8) | Unix.WEXITED 0 -> 
	let ff_process = Unix.create_process "firefox"
	  [|"firefox";"file://" ^ tmp ^ ""|] 
	  Unix.stdin Unix.stdout Unix.stderr in
	ignore (Unix.waitpid [] ff_process);
	Unix.unlink tmp
    | Unix.WEXITED x -> Printf.fprintf stderr "Error drawing the graph (dot exited with code %i\n" x
    | Unix.WSTOPPED x -> Printf.fprintf stderr "Error drawing the graph (dot stoped with signal %i\n" x
    | Unix.WSIGNALED x -> Printf.fprintf stderr "Error drawing the graph (dot killed with signal %i\n" x


let print_graph g = print_graph_coloring g e_c
