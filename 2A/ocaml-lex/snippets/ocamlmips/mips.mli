(** MIPS instructions *)

open Register


(** Type for a MIPS instruction *)
type mips = 
    I of string * reg * int (** instruction with one immediate value as input (example: [li]) *)
  | R of string * reg * reg (** instruction with one register as input (example: [neg])*)
  | RI of string * reg * reg * int (** instruction with one register and one immediate value as input (example: [addi]) *)
  | RR of string * reg * reg * reg (** instruction with two registers as input (example: [add]) *)
  | LW of reg * reg * int (** [LW(r,s,i)] corresponds to [lw r, i(s)] *)
  | SW of reg * reg * int (** [SW(r,s,i)] corresponds to [sw r, i(s)] *)
  | Syscall (** [syscall] instruction *)

(** printing function for a MIPS instruction *)
val print_mips : out_channel -> mips -> unit
  
