open Register

type mips = 
    I of string * reg * int
  | R of string * reg * reg
  | RI of string * reg * reg * int
  | RR of string * reg * reg * reg
  | LW of reg * reg * int
  | SW of reg * reg * int
  | Syscall

let print_mips out = function 
  | I(n, r, i) -> Printf.fprintf out "\t%s %a, %i\n" n print_reg r i
  | R(n, r, s) -> Printf.fprintf out "\t%s %a, %a\n" n print_reg r print_reg s
  | RI(n, r, s, i) -> Printf.fprintf out "\t%s %a, %a, %i\n" n print_reg r print_reg s i
  | RR(n, r, s, t) -> Printf.fprintf out "\t%s %a, %a, %a\n" n print_reg r print_reg s print_reg t
  | LW(r, s, i) -> Printf.fprintf out "\tlw %a, %i(%a)\n" print_reg r i print_reg s
  | SW(r, s, i) -> Printf.fprintf out "\tsw %a, %i(%a)\n" print_reg r i print_reg s
  | Syscall -> Printf.fprintf out "\tsyscall\n"
