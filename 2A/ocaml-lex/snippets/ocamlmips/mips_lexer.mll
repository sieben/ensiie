{ open Mips_parser 
}
let alpha = ['a'-'z''A'-'Z']
let num = ['0'-'9']
let alphanum = alpha|num
rule scan = parse
    [' ''\t'] { scan lexbuf } (* permet de passer les espaces *)
  | ['$''%']alphanum+ as n  { REG(n) } 
  | "lw" { LW }
  | "sw" { SW }
  | "syscall" { SYSCALL }
  | alpha alphanum+ as s { INS(s) }
  | '-'?num+ as s { INT(int_of_string s) }
  | '.'[^'\n']*'\n' { Lexing.new_line lexbuf; scan lexbuf }
  | alphanum*':'[^'\n']*'\n' { Lexing.new_line lexbuf; scan lexbuf }
  | '(' { LPAR }
  | ')' { RPAR }
  | '\n' { Lexing.new_line lexbuf; EOL }
  | ',' { COMMA }
  | eof { EOF }
