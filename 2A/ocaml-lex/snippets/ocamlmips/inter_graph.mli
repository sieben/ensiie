(** Interference and preference graphs *)

open Register
type graph
(** type of interference and preference graphs *)


(** {6 Construction of graphs } *)

val empty : graph
(** @return the empty graph *)
val add_reg : graph -> reg -> graph
(** add a register as a node of the graph *)

val remove_reg : graph -> reg -> graph
(** remove a register of the graph, removing all edges comming out this register *)

val add_interference : reg -> reg -> graph -> graph
(** add an interference edge between two register *)

val add_preference : reg -> reg -> graph -> graph
(** add a preference edge between two register *)

val remove_preferences : graph -> reg -> graph
(** remove all preference edges comming out a register *)

val join_nodes : graph -> reg -> reg -> graph * reg
(** [join_nodes g a b] joins the two nodes [a] and [b] into a new node [a|b] 
    @return the resulting graph and the joined register
*)

(** {6 Tests on graphs } *)

val is_empty : graph -> bool
(** @return [true] iff the graph is empty *)
val degree : graph -> reg -> int
(* [degree g r] returns the degree of [r] in [g], i.e., its number of neighbours for the interference *)

val neighbour : graph -> reg -> reg -> bool
(** [neighbour g a b] returns [true] iff there is a interference edge between [a] and [b] in [g] *)

val prefer_out : graph -> reg -> bool
(** [prefer_out g r] returns [true] iff there is at least one preference edge comming out [r] in [g] *)


val find_register : (reg -> bool) -> graph -> reg option
(** [find_register p g] returns a pseudo-register  of [g] satisfying a predicate [p]
@return [Some r] if such a {b pseudo}-register [r] exists, 
 [None] else (even if a real register satisfies [p]) *)
val find_preference : (reg -> reg -> bool) -> graph -> (reg*reg) option
(** [find_preference p g] returns a preference edge of [g] satisfying a predicate [p], at least one of the nodes is assumed to be a pseudo-register, not a real register
@return [Some (a,b)] if there exists a preference edge between [a] and [b] satisfying [p a b] with at least [a] or [b] a {b pseudo}-register,
 [None] else *)

val neighbours : graph -> reg -> reg list
(** [neighbours g a] returns the list of nodes of [g] that interfers with [a] *)

val choose : graph -> reg
(** [choose g] returns some node of [g] (pseudo or real register)
@raise Invalid_argument if the graph is empty *)

(** {6 Printing functions within firefox } *)

val print_graph : graph -> unit
(** opens a Firefox window with a graphical representation of the graph *)

val print_graph_coloring : graph -> Coloring.coloring -> unit
(** same as {!print_graph}, but with a (partial) coloring *)
