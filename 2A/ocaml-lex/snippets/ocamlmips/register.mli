(** Pseudo- and real registers *)

(** abstract type for registers and pseudo-registers *)
type reg

val sp : reg
(** MIPS register [$sp] *)
val temp0 : reg
(** reserved register to handle spilled pseudo-registers *)
val temp1 : reg
(** reserved register to handle spilled pseudo-registers *)

val nb_regs : unit -> int
(** number of available real registers *)

val set_nb_regs : int -> unit
(** set the number of real registers that can be allocated *)

(** [is_real r] return [true] iff [r] is a real register *)
val is_real : reg -> bool


val join : reg -> reg -> reg
(** [join r s] takes two pseudo-registers [r] and [s] and produces a pseudo-register joining them *)

(** [real r] return the real register contained in the join 
    @raise Not_found if it does not exist
*)
val real : reg -> reg


(** printing function for registers *)
val print_reg : out_channel -> reg -> unit

(** {6 Register sets} *)

type reg_set
(** type for sets of registers *)

val empty : reg_set
(** The empty set. *)

val add : reg -> reg_set -> reg_set
(**  [add r s] returns a set containing all elements of [s], plus [r]. If [r] was already in [s], [s] is returned unchanged. *)

val remove : reg -> reg_set -> reg_set
(** [remove r s] returns a set containing all elements of [s],
       except [r]. If [r] was not in [s], [s] is returned unchanged. *)

val is_empty : reg_set -> bool
(** Test whether a set is empty or not. *)

val choose : reg_set -> reg
 (** Return one element of the given set, or raise [Not_found] if
       the set is empty. Which element is chosen is unspecified,
       but equal elements will be chosen for equal sets. *)

val fold : (reg -> 'a -> 'a) -> reg_set -> 'a -> 'a
   (** [fold f s a] computes [(f xN ... (f x2 (f x1 a))...)],
       where [x1 ... xN] are the elements of [s]. The order of the elements of [s] is unspecified. *)


val iter : (reg -> unit) -> reg_set -> unit
    (** [iter f s] applies [f] in turn to all elements of [s]. The order of the elements of [s] is unspecified. *)
  
(** {6 Conversion functions} *)

val from_int : int -> reg
val from_string : string -> reg
val to_string : reg -> string
val to_color : reg -> Graph.Graphviz.color

