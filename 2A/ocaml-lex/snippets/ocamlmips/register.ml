type reg = int list

let ref_count = ref 32 
let new_reg _ = ref_count := !ref_count + 1; [!ref_count]

let sp = [29]

let temp0 = [24]
let temp1 = [25]

let nb_regs_ref = ref 2

let nb_regs () = !nb_regs_ref

let set_nb_regs i = nb_regs_ref := i


let rec to_string r = match r with
  | [31] -> "$ra"
  | [29] -> "$sp"
  | [24] -> "$t8"
  | [25] -> "$t9"
  | [2] -> "$v0"
  | [3] -> "$v1"
  | [4] -> "$a0"
  | [5] -> "$a1"
  | [6] -> "$a2"
  | [7] -> "$a3"
  | [r] ->  if r < 32 then Printf.sprintf "$%i" r else
      Printf.sprintf "%%%i" (r - 32)
  | x::q -> to_string [x] ^ "|" ^ to_string q
  | [] -> failwith "empty register"

let print_reg out r = Printf.fprintf out "%s" (to_string r) 

let assigned_sing v = v < 32

let is_real = List.exists assigned_sing

module Reg = struct
  type t = reg
let compare = compare
let equal = (=)
end

module RegSet = Set.Make(Reg)

type reg_set = RegSet.t

include RegSet

let from_int k =  [k + 1]

let from_string s = match s with
  | "$zero" -> [0]
  | "$at" -> [1]
  | "$v0" -> [2]
  | "$v1" -> [3]
  | "$a0" -> [4]
  | "$a1" -> [5]
  | "$a2" -> [6]
  | "$a3" -> [7]
  | "$t0" -> [8]
  | "$t1" -> [9]
  | "$t2" -> [10]
  | "$t3" -> [11]
  | "$t4" -> [12]
  | "$t5" -> [13]
  | "$t6" -> [14]
  | "$t7" -> [15]
  | "$s0" -> [16]
  | "$s1" -> [17]
  | "$s2" -> [18]
  | "$s3" -> [19]
  | "$s4" -> [20]
  | "$s5" -> [21]
  | "$s6" -> [22]
  | "$s7" -> [23]
  | "$t8" -> [24]
  | "$t9" -> [25]
  | "$k0" -> [26]
  | "$k1" -> [27]
  | "$gp" -> [28]
  | "$sp" -> [29]
  | "$fp" -> [30]
  | "$ra" -> [31]
  | _ -> if s.[0] = '$' then 
      [int_of_string (String.sub s 1 (String.length s - 1))]
    else if s.[0] = '%' then
      [int_of_string (String.sub s 1 (String.length s - 1))+32]
    else failwith "Not a register"


let join = (@)

let to_color c= 
  let aux = function
  | 2 -> 0xff0000
  | 3 -> 0x00ff00
  | 4 -> 0x0000ff
  | 5 -> 0x00ffff
  | 6 -> 0xff00ff
  | 7 -> 0xffff00
  | 8 -> 0x800000
  | 9 -> 0x008000
  | 10 -> 0x000080
  | 11 -> 0x008080
  | 12 -> 0x800080
  | 13 -> 0x808000
  | n -> (n - 13) * 0x002000 
  in match c with
      [r] -> aux r
    | _ -> failwith "to_color : not a normal register"

let rec real = function 
  | [] -> raise Not_found
  | x::q when is_real [x] -> [x]
  | x::q -> real q
