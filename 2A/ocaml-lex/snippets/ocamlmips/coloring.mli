(** Coloring of pseudo-registers *)

open Register
type decision = 
Spill of int (** the pseudo-register is spilled, the integer gives its position in the stack *)
| Color of reg (** the pseudo-register is affected to a real register *)
(** decision for one pseudo-register *)

type coloring
(** abstract type for (partial) coloring *)

val empty : coloring
(** empty coloring *)

val add_color : reg -> reg -> coloring -> coloring
(** [add_color r s c] affects the pseudo-register [r] to the real register [s] in [c] *)

val add_spill : reg -> coloring -> coloring
(** [add_spill r c] decides to spill the pseudo-register [r] in [c] *)

val find : reg -> coloring -> decision
(** [find r c] finds the decision associated to [r] in the coloring [c]
@raise Not_found if no decision has been made yet
*)

val copy_color :  reg -> reg -> coloring -> coloring
(** [copy_color r s c] copies the decision made for [r] in [c] to [s]
@raise Not_found if no decision has been made for [r] yet
 *)

(** {6 Printing functions } *)

val print_dec : out_channel -> decision -> unit
val print_coloring : out_channel -> coloring -> unit
