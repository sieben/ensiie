open Register 
module Reg = struct
  type t = reg
  let compare = compare
  let equal = (=)
end

module RegMap = Map.Make(Reg)

type decision = Spill of int | Color of reg

type coloring = decision RegMap.t

let spill_count = ref 0
let find = RegMap.find
let empty = RegMap.empty
let add_color r s c = RegMap.add r (Color s) c
let add_spill r c =
  let s = Spill !spill_count in
  spill_count := !spill_count + 1;
  RegMap.add r s c
let copy_color r s c = RegMap.add s (RegMap.find r c) c

let print_dec out dec = match dec with
    Color s -> print_reg out s
  | Spill j -> Printf.fprintf out "Spill %i" j

let print_coloring out coloring = 
  RegMap.iter (fun r s -> Printf.fprintf out "%a : %a\n" print_reg r print_dec s)
    coloring
