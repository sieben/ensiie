%{
  open Mips
  open Register
%}
%token <int> INT
%token <string> REG INS
%token LPAR RPAR EOL LW SW SYSCALL COMMA EOF
%start s
%type <Mips.mips list> s
%%
s: 
   e EOL s { $1::$3 }
|  EOL s { $2 }
|  EOF { [] }

e:
  LW REG COMMA INT LPAR REG RPAR { LW(from_string $2, from_string $6, $4) }
| SW REG COMMA INT LPAR REG RPAR { SW(from_string $2, from_string $6, $4) }
| INS REG COMMA INT { I($1, from_string $2, $4) }
| INS REG COMMA REG { R($1, from_string $2, from_string $4) }
| INS REG COMMA REG COMMA INT { RI($1, from_string $2, from_string $4, $6) }
| INS REG COMMA REG COMMA REG { RR($1, from_string $2, from_string $4, from_string $6) }
| SYSCALL { Syscall }

