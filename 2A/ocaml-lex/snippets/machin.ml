(*#load "graphics.cma";;*)

open Graphics;;

open_graph "";;

set_font "-*-helvetica-medium-r-*-*-24-*-*-*-*-*-*-*";;

draw_string "coucou";;

moveto 0 50;;

let l,_ = text_size "coucou" in 
  draw_string "coucou"; 
  rlineto (-l) 0;
  rmoveto l 0;;

let l,_ = text_size "coucou" in 
  draw_string "coucou"; 
  rlineto (-l) 0;
  rmoveto 0 (-2);
  rlineto l 0;
  rmoveto 0 2;;

let l,_ = text_size "coucou" in 
  draw_string "coucou"; 
  rlineto (-l) 0;
  rmoveto 0 (-2);
  rlineto l 0;
  rmoveto 0 (-2);
  rlineto (-l) 0;
  rmoveto l 4;;
