open Expr;;

type état =
  { mutable transitions : (char * état) list;
  mutable epsilon_transitions : état list;
  mutable terminal : bool;
  numéro : int };;

val expr_vers_automate : expr -> état;;
