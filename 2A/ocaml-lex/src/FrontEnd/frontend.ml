(* Fonction parsant une chaîne de caractères et produisant le sous-arbre de
 * syntaxe abstraite qui représente cette expression
 * type : string -> Expression.regexp
 * args : regexp_string, en l'occurence, chaque premier élément des couples
 * (regexp, action) dans le champ ".regles" de la structure de données
 * arbre_syntaxe
 * precondition : aucune
 * postcondition : renvoie le sous-arbre de syntaxe abstraite qui représente
 * l'expression régulière sous forme de chaîne de caractères passée en entrée,
 * renvoie l'expression régulière vide (arbre de syntaxe abstraite vide si un
 * problème de parsage survient) 
 * *)
let parse_regexp regexp_string =
    try (
        let lexbuf = Lexing.from_string regexp_string in
            Parser.main Lexer.regexp lexbuf
        )
    with failure -> Expression.Empty
;;

(* Fonction qui permet l'affichage dans le terminal d'une règle de l'arbre de syntaxe
 * abstraite
 * type : string * string -> unit
 * args : regexp la chaîne de caractères correspondant à l'expression régulière,
 * action la chaîne de caractères correspondant à l'action
 * precondition : aucune
 * postcondition : affiche à l'écran une règle de l'arbre de syntaxe abstraite
 * *)
let affichage_texte (regexp, action) =
        print_string "Règle ( " ;
        Expression.affiche_regexp (parse_regexp regexp) ;
        print_string " , " ;
        print_string action ;
        print_string " )" ;
        print_newline () ;
;;

let affichage_dot (regexp, action) = ()
  (* ébauche de l'affichage des règles en fichier .dot
   let corps_fichier_dot =
            String.concat "" (
                "Règles -> En-tête ; Fichier -> Règles ; Fichier -> Trailer;" :: [] )
                    in
*)
;;

(* Fonction qui applique une fonction d'affichage à la liste des couples
 * (regexp, action) de chaînes de caractères.
 * type : string * string -> unit -> (string * string) list
 * args : fun_affichage fonction d'affichage et regles liste de règles
 * correspondant au champ ".regles" de la structure de données
 * précondition : aucune
 * postcondition : applique une fonction d'affichage à l'ensemble des règles de
 * l'arbre de syntaxe abstraite
 * *)
let print_regles fun_affichage regles =
    List.iter fun_affichage (List.rev regles)
;;

(* Fonction qui affiche l'arbre de syntaxe abstraite selon la chaîne de
 * caractères "affichage" (argument de la fonction)
 * *)
let print_arbre_syntaxe affichage arbre =
        match affichage with
        | "texte" -> print_string "Fichier (" ;
                     print_newline () ;
                     print_string "En-tête ( " ;
                     print_string arbre.Expression.entete ;
                     print_string " ) ," ;
                     print_newline () ;
                     print_string "Règles ( " ;
                     print_newline () ;
                     print_regles affichage_texte arbre.Expression.regles ;
                     print_newline () ;
                     print_string " ) ," ;
                     print_newline () ;
                     print_string "Trailer ( " ;
                     print_string arbre.Expression.trailer ;
                     print_string " )" ;
                     print_newline () ;
                     print_string ")" ; 
                     print_newline () ;

(**** ébauche de l'affichage en fichier .dot ********

        | "dot" -> let corps_fichier_dot =
                    String.concat "" (
                        "digraph arbre_syntaxe {" ::
                        "Fichier -> En-tête ; Fichier -> Règles ; Fichier ->
                                Trailer;" :: [] )
                    in
                    let file = "arbre_syntaxe.dot" in
                    let oc = open_out file in
                    Printf.fprintf oc "%s\n" corps_fichier_dot ;
                    close_out oc ;
                    
                    print_regles affichage_dot arbre.Expression.regles ;
                    
                    String.concat "" (
                        "En-tête [label=" :: arbre.Expression.entete :: "]" ::
                        "Trailer [label=" :: arbre.Expression.trailer :: "]" :: [] )
                    in
                    let file = "arbre_syntaxe.dot" in
                    let oc = open_out file in
                    Printf.fprintf oc "%s\n" corps_fichier_dot ;
                    close_out oc ;
*)
        | _ -> ()
;;

(* Fonction principale :
        * prend en argument le fichier indiqué en ligne de commande ou à défaut
        * l'entrée standard.
        *
        * analyse lexicalement le fichier pour remplir la structure de données
        * puis
        * affiche textuellement sur le terminal l'arbre de syntaxe abstraite
        * correspondant à l'analyse syntaxique de la structure de données
 * *)
let _ =
    let cin =
        if Array.length Sys.argv > 1 
        then open_in Sys.argv.(1)
        else stdin
    in
    try (let lexbuf = Lexing.from_channel cin in Lexer.token lexbuf)
    with failure -> print_arbre_syntaxe "texte" (* Sys.argv.(2) *) Expression.arbre_syntaxe
;;
