(* Fichier lexer.mll, fichier d'entrée d'OCamllex *)

(* Partie en-tête *)

{
open Expression
open Parser

(* Création des buffers spécialisés pour les trois parties du fichier :
        * l'en-tête, la liste de (regexp, action) et le trailer *)
let entete_buffer = Buffer.create 500
let trailer_buffer = Buffer.create 500
let regexp_buffer = Buffer.create 10
let action_buffer = Buffer.create 20

exception End_of_File
}

(* Partie définitions (optionnelle) *)

(* définit l'alias special qui représente l'ensemble des caractères spéciaux
 * réservés du langage que l'on souhaite analyser *)
let special = ['\\' '[' ']' '^' '-' '?' '.' '*' '+' '|' '(' ')' '{' '}']

(* Partie règles *)

(* Règle principale servant à analyser lexicalement le fichier ".lex" *)
rule token = parse
| [' ' '\t' '\n'] { token lexbuf }
| "%{" { entete lexbuf }
| "%%" { regles lexbuf }
| eof { raise End_of_File }
| _ as c { Printf.printf "Format invalide : pas d'en-tête %c" c }

(* Règle secondaire dérivée de "token" qui permet de remplir le champ ".entete"
 * de l'arbre de syntaxe abstraite *)
and entete = parse
| "%}" { arbre_syntaxe.entete <- Buffer.contents entete_buffer;
         Buffer.clear entete_buffer;
         token lexbuf }
| eof { invalid_arg "format non valide" }
| _ as c { Buffer.add_char entete_buffer c;
           entete lexbuf }

(* Règle secondaire dérivée de "token" qui permet de remplir le premier élément
 * du couple (regexp, action) de manière à pouvoir remplir le champ ".regles" de
 * l'arbre de syntaxe abstraite *)
and regles = parse
| "%%" { trailer lexbuf }
| '{' { action lexbuf }
| eof {   invalid_arg "format non valide" }
| _ as c { Buffer.add_char regexp_buffer c; regles lexbuf }

(* Règle secondaire dérivée de "regles" qui permet de remplir le deuxième
 * élément du couple (regexp, action) et de remplir le champ ".regles" de
 * l'arbre de syntaxe abstraite *)
and action = parse
| '}' { let action = Buffer.contents action_buffer
        and regexp = Buffer.contents regexp_buffer
        in arbre_syntaxe.regles <- (regexp, action)::arbre_syntaxe.regles;
        Buffer.clear action_buffer; Buffer.clear regexp_buffer;
        regles lexbuf  
      }
| eof {   invalid_arg "format non valide" }
| _ as c { Buffer.add_char action_buffer c; action lexbuf }

(* Règle secondaire dérivée de "regles" qui permet de remplir le champ
 * ".trailer" de l'arbre de syntaxe abstraite *)
and trailer = parse
| eof { arbre_syntaxe.trailer <- Buffer.contents trailer_buffer;
        Buffer.clear trailer_buffer;
        raise End_of_File }
| _ as c { Buffer.add_char trailer_buffer c;
           trailer lexbuf }

(* Règle principale servant à analyser lexicalement les expression régulières
 * (regexp) pour permettre leur analyse syntaxique par le parser défini dans
 * parser.mly *)
and regexp = parse
| [' ' '\t' '\n'] { regexp lexbuf }
| ([^ '\\' '[' ']' '^' '-' '?' '.' '*' '+' '|' '(' ')' '{' '}'] as c) { CARACTERE (c) }
| '\\'([^ '\\' '[' ']' '^' '-' '?' '.' '*' '+' '|' '(' ')' '{' '}'] as c) { CARACTERE (c) }
| '\\'(special as s) { CARACTERE (s) }
| '[' { CROCHET_G }
| ']' { CROCHET_D }
| '^' { COMPLEMENTAIRE }
| '?' { OPTION }
| '-' { TIRET }
| '*' { ETOILE }
| '+' { PLUS }
| '|' { OU }
| '(' { PARENTHESE_G }
| ')' { PARENTHESE_D }
| '.' { CARACTERE_JOKER }
| eof { EOF }
