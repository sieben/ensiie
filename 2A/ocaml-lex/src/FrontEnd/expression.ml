(* Définition du type "regexp" : expression régulière *)
type regexp =
        | Empty
        | Caractere of char
        | Ensemble of regexp
        | Complementaire of regexp
        | Intervalle of char * char
        | Option of regexp
        | Etoile of regexp
        | Plus of regexp
        | Ou of regexp * regexp
        | Concatenation of regexp * regexp
        | Caractere_joker
;;

(* Définition du type "arbre" : arbre de syntaxe abstraite *)
type arbre = { mutable entete: string ; mutable regles: (string * string) list ;
mutable trailer: string }
;;

(* Initialisation de l'arbre de syntaxe abstraite qui sera rempli par l'analyse
 * lexicale *)
let arbre_syntaxe = { entete = "" ; regles = [] ; trailer = "" }
;;

(* Fonction d'affichage dans le terminal des expressions régulières
 * type : regexp -> unit
 * arguments : regexp 
 * précondition : aucune
 * postcondition : affiche sur la sortie standard la chaîne de caractères
 * correspondant à l'expression régulière
 * *)
let rec affiche_regexp = function
        | Caractere (c) -> Printf.printf "'%c'" c
        | Ensemble (e) -> print_string "Ensemble ( " ; affiche_regexp e ; print_string " )"
        | Complementaire (e) -> print_string "Complémentaire ( " ; affiche_regexp e ; print_string " )"
        | Intervalle (a,b) -> Printf.printf "Intervalle ( %c , %c )" a b
        | Option (e) -> print_string "( " ; affiche_regexp e ; print_string " )?"
        | Etoile (e) -> print_string "( " ; affiche_regexp e ; print_string " )*"
        | Plus (e) -> print_string "( " ; affiche_regexp e ; print_string " )+"
        | Ou(a,b) -> print_string "Ou ( " ; affiche_regexp a; print_string " , " ; affiche_regexp b ; print_string " )"
        | Concatenation(a,b) -> print_string "Concaténation ( " ; affiche_regexp a ; print_string " , " ; affiche_regexp b ; print_string " )"
        | Caractere_joker -> print_string "'.'"
        | Empty -> ()
;;
