%{
open Expression
%}

%token <char> CARACTERE
%token TIRET COMPLEMENTAIRE OPTION ETOILE PLUS OU CARACTERE_JOKER EOF
%token CROCHET_G CROCHET_D PARENTHESE_G PARENTHESE_D

%left OU

%start main
%type <Expression.regexp> main
%%

main:
     regexp EOF { $1 }
;

regexp:
    | CARACTERE { Caractere($1) }
    | CARACTERE TIRET CARACTERE { Intervalle($1,$3) }
    | CROCHET_G regexp CROCHET_D { Ensemble($2) }
    | COMPLEMENTAIRE regexp { Complementaire($2) }
    | regexp OPTION { Option($1) }
    | regexp ETOILE { Etoile($1) }
    | regexp PLUS { Plus($1) }
    | PARENTHESE_G regexp PARENTHESE_D { $2 }
    | regexp regexp %prec OU { Concatenation($1,$2) }
    | regexp OU regexp { Ou($1,$3) }
    | CARACTERE_JOKER { Caractere_joker }
;
