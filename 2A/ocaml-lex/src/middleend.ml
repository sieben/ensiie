(* MiddleEnd.ml *)

(* On indexe les etats de l'automate à partir de 0 *)

let counter = ref 0
;;

type regexp =
        Empty
  | Caractere of char
  | Ensemble of regexp
  | Complementaire of regexp
  | Intervalle of char * char
  | Option of regexp
  | Etoile of regexp
  | Plus of regexp
  | Ou of regexp * regexp
  | Concatenation of regexp * regexp
;;

(* Etat d'un automate fini non deterministe *)

type nfa_state =
        { mutable trans: (char * nfa_state) list; (* Array des transitions *)
        mutable eps_trans: nfa_state list; (* Liste des états joignables par une
        epsilon transition *)
        mutable terminal: bool; (* vaut true si l'etat est terminal *)
        index: int (* identifiant de l'état, on utilise counter pour
        avoir un identifiant unique *)
        }
;;

(* Etat d'un automate de Thompson *)

type thompson =
        { initial: nfa_state;
        final: nfa_state
        }
;;

(* Etat d'un automate fini deterministe *)

type dfa_state =
        { mutable dtrans: dfa_state array;
        mutable dterminal: bool
        }
;;

(* Constructeur d'automate fini non deterministe *)

let nfa_create () =
        counter := !counter + 1;
        { trans = [];
        eps_trans = [];
        terminal = false;
        index = !counter
        }
;;

(* Ajout d'une transition pour le char c entre les deux
 * etats s1 et s2 pour un automate quelconque *)

let add_trans s1 c s2 =
        s1.trans <- (c, s2) :: s1.trans
;;

(* Ajout d'une epsilon-transition entre l'etat s1 et s2 *)

let add_eps_trans s1 s2 =
        s1.eps_trans <- s2 :: s1.eps_trans
;;

(* Fonctions de construction de l'algorithme de Thompson *)

let thompson_epsilon () =
        let s1 = nfa_create ()
        and s2 = nfa_create () in
        add_eps_trans s1 s2;
        { initial = s1; final = s2 }
;;


(* Comme action *)

let thompson_Caractere c =
        let s1 = nfa_create ()
        and s2 = nfa_create () in
        add_trans s1 c s2;
        { initial = s1; final = s2 }
;;

let thompson_Ensemble list =
        let s1 = nfa_create ()
        and s2 = nfa_create () in
        List.iter (fun x -> add_eps_trans s1 x.initial) list;
        List.iter (fun x -> add_eps_trans x.final s2) list;
        { initial = s1; final = s2 }
;;

let thompson_Ou a1 a2 =
        thompson_Ensemble [a1;a2]
;;


let thompson_Concat a1 a2 =
        add_eps_trans a1.final a2.initial;
        { initial = a1.initial; final = a2.final }
;;

let thompson_Etoile a =
        let s1 = nfa_create ()
        and s2 = nfa_create () in
        add_eps_trans a.final a.initial;
        add_eps_trans s1 a.initial;
        add_eps_trans a.final s2;
        add_eps_trans s1 s2;
        { initial = s1; final = s2 }
;;

(* Intervalle de char sous la forme de char list *)

let rec range c1 c2 =
        if c2 >= c1
        then
                []
        else
                c1 :: (range (Char.chr((Char.code c1) + 1)) c2)
;;

let rec subtract_elem list element =
        match list with
        [] -> []
    | a::q -> 
                    if a == element
                    then subtract_elem q element
                    else a :: subtract_elem q element
;;

let rec subtract_list l1 l2 =
        match l2 with
        [] -> []
    |elem::q -> subtract_list (subtract_elem l1 elem) q
;;


let rec analyse_concat c =
        match c with
        Empty -> []
    |Caractere(c) -> [c]
    |Concatenation(a, b) -> analyse_concat(a) @ analyse_concat(b)
    |Intervalle(c1, c2) -> range c1 c2
    |_ -> []
;;

let thompson_Complementaire re =
        let special = ['\\';'[';']';'^';'-';'?';'.';'*';'+';'|';'(';')';'{';'}']
        and letters = range 'a' 'z'
        and cap_letters = range 'A' 'Z'
        and numbers = range '0' '9' in
        let sigma = ['_'] @ special @ letters @ cap_letters @ numbers in
        match re with
        Empty -> thompson_Ensemble (List.map thompson_Caractere sigma)
    | Intervalle (c1, c2) ->
                    let l = subtract_list sigma (range c1 c2) in
                    thompson_Ensemble (List.map thompson_Caractere l)
    |Caractere (c) ->
                    let l = subtract_list sigma [c] in
                    thompson_Ensemble (List.map thompson_Caractere l)
    |Concatenation (c1, c2) ->
                    let l = subtract_list sigma (analyse_concat(c1) @ analyse_concat(c2)) in
                    thompson_Ensemble (List.map thompson_Caractere l)
    |_ -> thompson_epsilon() (* A améliorer *)
;;

(* Fonction auxiliere utile dans la transformation de regexp vers un automate de
 * Thompson *)

let rec thompson_aux = function
        Empty -> thompson_epsilon ()
    | Caractere c ->
                    thompson_Caractere c
    | Ou (re1, re2) ->
                    thompson_Ou (thompson_aux re1) (thompson_aux re2)
    | Concatenation (re1, re2) ->
                    thompson_Concat (thompson_aux re1) (thompson_aux re2)
    | Etoile re ->
                    thompson_Etoile (thompson_aux re)
                    (* x? = x|epsilon *)
    | Option re ->
                    thompson_Ou (thompson_epsilon ()) (thompson_aux re)
                    (* x+ = xx* *)
    | Plus re ->
                    thompson_Concat (thompson_aux re) (thompson_Etoile (thompson_aux re))
                    (* x1..xn = x1|..|xn *)
    | Intervalle (c1, c2) ->
                    thompson_Ensemble (List.map thompson_Caractere (range c1 c2))
    | Complementaire re ->
                    thompson_Complementaire re
    |_ -> thompson_epsilon() (* A ameliorer *)
;;

(* Bootstrap pour la transformation d'une expression reguliere
 * vers le graphe de Thompson associé*)

let thompson re =
        let a = thompson_aux re in
        a.final.terminal <- true;
        a.initial
;;

(* Creation de l'automate deterministe *)

let dfa_create () =
        let s =
                { dtrans = [||];
                dterminal = false;
        }
        in
        s.dtrans <- Array.make 256 s;
        s
;;


(* Fonction testant si une string est reconnue ou 
 * non par un automate *)

let accept initial string =
        let n = String.length string in
        let rec aux state i =
                if i = n then state.dterminal
                else aux state.dtrans.(int_of_char string.[i]) (i + 1)
        in
        aux initial 0
;;

(* Fonction testant si un etat d'un automate non-deterministe
 * apparait dans une liste d'etats. Un etat est identifié par
 * son index *)

let rec mem_state s = function
        [] -> false
    | s' :: list ->
                    s.index = s'.index || mem_state s list
;;

(* Mise d'une liste d'etat sous forme triee *)

let trace list =
        List.sort compare (List.map (function s -> s.index) list)
;;

(* Calcul de la epsilon-fermeture d'un automate *)

let epsilon_closure list =
        let rec aux accu = function
                [] -> accu
    | s :: q ->
                    if mem_state s accu
                    then aux accu q
                    else aux (s :: accu) (s.eps_trans @ q)
        in
        aux [] list
;;

(* Calcul de la table des transitions d'un ensemble d'etat *)

let trans_table list =
        let t = Array.create 256 [] in
        List.iter (function s ->
                List.iter (function c, s' ->
                        let i = int_of_char c in
                        if not (mem_state s' t.(i)) then t.(i) <- s' :: t.(i)
                        ) s.trans
                ) list;
                t
;;

(* Determinisation d'un automate *)

let determ initial_state =
        let store = ref [] in

        let rec transl list =
                let tr = trace list in
                if List.mem_assoc tr !store
                then List.assoc tr !store
                else begin
                        let s = dfa_create () in
                        store := (tr, s) :: !store;
                        let table = trans_table list in
                        for i = 0 to 255 do
                                s.dtrans.(i) <-
                                        transl (epsilon_closure table.(i));
                                        s.dterminal <-
                                                List.exists (fun s' -> s'.terminal) list
                        done;
                        s
                end
                        in

                        transl (epsilon_closure [initial_state])
;;

(* Bootstrap pour determiniser une expression reguliere *)

let dfa_of_regexp re =
        determ (thompson re)
;;

(* Separation des etats terminaux des non-terminaux d'un automate *)
(* Il s'agit de la premiere partition dans l'algorithme de Hopcroft *)

let isole_terminal list =
        let rec aux terminal non_terminal to_sort =
                match to_sort with
                [] -> terminal, non_terminal
    |s::q -> if s.dterminal
                then
                        aux (s::terminal) non_terminal q
                else
                        aux terminal (s::non_terminal) q
        in aux [] [] list
;;
