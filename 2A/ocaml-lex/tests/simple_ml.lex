%{
  (* Header *)
%}
%%
\^\-\\ {Printf.printf "SPECIAL\n" }
int|let { Printf.printf "KEYWORD\n" }
[0-9]+ { Printf.printf "INT\n" }
[a-zA-Z][a-zA-Z_]* { Printf.printf "VAR\n" }
%%
  (* this is a trailer *)
let _ = 
  let lexbuf = { content = "integer38^-\\let\n"; pos = 0 } in
  try
    let rec l lexbuf = 
      let lexbuf, _ = lexer lexbuf in
      l lexbuf
    in
    l lexbuf
  with
      Lexing_error | Invalid_argument _ -> ()
   
