%{
(* une calculatrice en notation polonaise inversée (cf. man dc) *)

 exception Empty_stack

 let precision = ref 0

 let print f = 
   Printf.printf "%.*f " !precision f
   
 let div x y = 
   ceil (x /. y *. (10. ** (float_of_int (!precision))))
	 /. (10. ** (float_of_int !precision))

 let rem x y =
   x -. (div x y *. y)

 let push, pop, print_stack, clear = 
   let stack = ref [] in
   let push x = stack := x::!stack in
   let pop () = match !stack with
     | x :: q -> stack := q; x
     | [] -> raise Empty_stack in
   let print_stack () =
     List.iter print !stack;
     print_newline () in
   let clear () = stack := [] in
   push, pop, print_stack, clear
      
  let old_pos = ref 0

%}
%%
\  { () }
[0-9]+(\.[0-9]*)? { let s = String.sub buf.content !old_pos 
	   (buf.pos - !old_pos) in
	 push (float_of_string s) }
\+ { push (pop () +. pop ()) }
\- { push (pop () -. pop ()) }
\* { push (pop () *. pop ()) }
/ { push (div (pop ()) (pop ())) }
% { push (rem (pop ()) (pop ())) }
~ { let x = pop () in let y = pop () in
		      push (div x y);
		      push (rem x y) }
\^ { push (pop () ** pop ()) }
v { push (sqrt (pop ())) }
p { let v = pop () in print v; print_newline (); push v }
n { let v = pop () in print v }
f { print_stack () }
c { clear () }
d { let v = pop () in push v; push v }
r { let x = pop () in let y = pop () in push x; push y }
k { precision := (int_of_float (pop ())) }
K { push (float_of_int (!precision)) }
%%
let rec fix lexbuf = 
  let lexbuf, _ = 
    try 
      lexer lexbuf 
    with Empty_stack -> prerr_endline "dc : stack empty"; 
      { lexbuf with pos = lexbuf.pos + 1 }, ()
  in
  old_pos := lexbuf.pos;
  fix lexbuf

let _ =
try
  while true do
    let lexbuf = { content = read_line () ^ "\n"; pos = 0 } in
    old_pos := 0;
    try 
      fix lexbuf
    with Lexing_error -> ()
  done	
with End_of_file -> exit 0
