// clientu.c (client UDP)

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>

char* id=0;
short port=0;

int sock=0;	/* socket de communication */

int main(int argc, char** argv)
{
    struct  sockaddr_in moi;
    struct  sockaddr_in autre;
    int     ret = 0;
    socklen_t len = sizeof(moi), autre_len = sizeof(autre);
    char    buf_read[256], buf_write[256];

    if (argc!=4) {
        fprintf(stderr,"usage: %s id host port\n",argv[0]);
        exit(1);
    }
    
    id= argv[1];
    port= atoi(argv[3]);

    if ((sock = socket (AF_INET, SOCK_DGRAM, 0)) == -1) {
        fprintf(stderr,"\n%s: socket %s\n",argv[0],strerror(errno));
        exit(1);
    }

    getsockname(sock,(struct  sockaddr  *)&moi, &len);

    autre.sin_family = AF_INET;
    autre.sin_port = htons(port);
    inet_aton(argv[2],&autre.sin_addr);

    while (buf_write[0] != 'q')
    {
        fgets(buf_write, 255, stdin);
        buf_write[255]=0;
        printf("\nEnvoi de message (de %s a %s:%d) : %s\n", id,
                inet_ntoa(autre.sin_addr), ntohs(autre.sin_port),
                buf_write);

        ret=sendto(sock, buf_write, sizeof(buf_write), 0,
                (struct sockaddr*) &autre, autre_len);
        if (ret<=0) {
            printf("\n%s: erreur dans sendto (num=%d, mess=%s)\n",
                    argv[0],ret,strerror(errno));
            continue;
        }

        getsockname(sock, (struct sockaddr*)&moi, &len);
        ret=recvfrom(sock, buf_read, sizeof(buf_read), 0,
                (struct sockaddr*) &autre, &autre_len);
        printf("\nMessage recu (de %s:%d a %s) : %s\n",
                inet_ntoa(autre.sin_addr),ntohs(autre.sin_port), id,
                buf_read);
        if (ret<=0) {
            printf("\n %s:  erreur dans recvfrom (num=%d, mess=%s)\n",
                    argv[0],ret,strerror(errno));
            continue;
        }
    }	
    printf("Au revoir, et a bientot sur le saimoun's chat !\n");
    return 0;
}
