// clientu.c (client UDP)

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>

char* id=0;
char *truc;
short sport=0;

int sock=0;	/* socket de communication */

int main(int argc, char** argv)
{
    struct  sockaddr_in moi; /* SAP du client */
    struct  sockaddr_in serveur; /* SAP du serveur */
    int  nb_question=0;
    int ret,len;
    int     serveur_len= sizeof(serveur);
    char    buf_read[1<<8], buf_write[1<<8];

    if (argc!=4) {
        fprintf(stderr,"usage: %s id host sport\n",argv[0]);
        exit(1);
    }
    id= argv[1];
    sport= atoi(argv[3]);

    if ((sock = socket (AF_INET, SOCK_DGRAM, 0)) == -1) {
        fprintf(stderr,"\n%s: socket %s\n",argv[0],strerror(errno));
        exit(1);
    }

    len = sizeof(moi);
    getsockname(sock,(struct  sockaddr  *)&moi, (socklen_t*) &len);

    serveur.sin_family = AF_INET;
    serveur.sin_port = htons(sport);
    inet_aton(argv[2],&serveur.sin_addr);

    while (nb_question<6) {

        sprintf(buf_write,"#%2s=%03d",id,nb_question++);
        printf("\n client %2s: (%s:%4d) envoie a ", id,
                inet_ntoa(moi.sin_addr),ntohs(moi.sin_port));
        printf("(%s:%4d): %s\n", 
                inet_ntoa(serveur.sin_addr),ntohs(serveur.sin_port),
                buf_write);

        ret=sendto(sock, buf_write, sizeof(buf_write), 0,
                (struct sockaddr*) &serveur, serveur_len);
        if (ret<=0) {
            printf("\n%s: erreur dans sendto (num=%d, mess=%s)\n",
                    argv[0],ret,strerror(errno));
            continue;
        }
        len=sizeof(moi); 
        getsockname(sock, (struct sockaddr*)&moi, (socklen_t*) &len);
        printf("\n client %2s: (%s:%4d) recoit de ",id,
                inet_ntoa(moi.sin_addr),ntohs(moi.sin_port));
        ret=recvfrom(sock, buf_read, sizeof(buf_read), 0,
                (struct sockaddr*) &serveur, (socklen_t*) &serveur_len);
        if (ret<=0) {
            printf("\n %s:  erreur dans recvfrom (num=%d, mess=%s)\n",
                    argv[0],ret,strerror(errno));
            continue;
        }
        printf("(%s:%4d) : %s\n",inet_ntoa(serveur.sin_addr),
                ntohs(serveur.sin_port),buf_read);
    }	
    printf("j ai fini bye\n");
    return 0;
}
