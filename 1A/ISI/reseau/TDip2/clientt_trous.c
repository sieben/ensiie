// clientt.c (client TCP)

#include <stdio.h>
#include <errno.h>
#include <netinet/in.h>
#include <string.h>

#define NBECHANGE 3

char* id=0;
short sport=0;

int sock=0;	/* socket de communication */

int main(int argc, char** argv)
{
struct  sockaddr_in moi; /* SAP du client */
struct  sockaddr_in serveur; /* SAP du serveur */
int nb_question=0;
int ret,len;

	if (argc!=4) {
		fprintf(stderr,"usage: %s id serveur port\n",argv[0]);
		exit(1);
	}
	id= argv[1];
	sport= atoi(argv[3]);

	if ((sock = socket (AF_INET, SOCK_DGRAM, 0)) == -1) {
		fprintf(stderr,"%s: socket %s\n",argv[0],
			strerror(errno));
		exit(1);
	}
	serveur.sin_family = AF_INET;
	serveur.sin_port = htons(sport);
	inet_aton(argv[2],&serveur.sin_addr);
	
	if (connect( sock,( struct sock_addr *) &server, sizeof(server) )<0) {
		fprintf(stderr,"%s: connect %s\n",argv[0],strerror(errno));
		perror("bind");
		exit(1);
	}
	len=sizeof(moi); 
	getsockname(sock,(struct  sockaddr  *)&moi,&len);

	for (nb_question=0 ; nb_question<NBECHANGE ; nb_question++) {
		char    buf_read[1<<8], buf_write[1<<8];

		sprintf(buf_write,"#%2s=%03d",id,nb_question);
		printf(" client %2s: (%s:%4d) envoie a  ", id,
			inet_ntoa(moi.sin_addr),ntohs(moi.sin_port));
		printf("(%s:%4d) : %s\n",
			inet_ntoa(serveur.sin_addr),ntohs(serveur.sin_port),
			buf_write);
		ret=write(/*..................................*/);
		if (ret<=strlen(buf_write)) {
			printf("\n%s: erreur dans write (num=%d, mess=%s)\n",
				argv[0],ret,strerror(errno));
			continue;
		}
		printf(" client %2s: (%s:%4d) recoit de ",id, 
				inet_ntoa(moi.sin_addr),ntohs(moi.sin_port));
		ret=read(/*...........................*/);
		if (ret<=0) {
			printf("\n%s:  erreur dans read (num=%d, mess=%s)\n",
				argv[0],ret,strerror(errno));
			continue;
		}
		printf("(%s:%4d) : %s\n",inet_ntoa(serveur.sin_addr),
			ntohs(serveur.sin_port),buf_read);
	}
	close(sock);
	printf(" j ai fini, bye\n");
	return 0;
}
