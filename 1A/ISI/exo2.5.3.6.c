#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>

void fils(){
    printf("Je suis un fils et voici mon pid : %d\n",getpid());
    if( read(tube[1]) == 0){
        printf("Le tube est vide et ferme, je peux donc me fermer\n");
        sleep(3);
        exit(0);
    }
}

int main(){
    int pid;
    int tube[2];
    if ( ( pid = fork() ) == 0 )
        fils();
    else{
        int input;
        while(input != 0){
            printf("Tapez 0\n");
            scanf("%d",&input);
        };
        close(tube[2]);
        close(tube[1]);
        close(tube[0]);

        /*Fermeture de tous les tubes*/
        exit(0);

    }
    return 0;
}
/*
   Pere
   Sur lecture de 0
   close(tube1[1]);
   close(tube2[1]);

   Pour les 2 fils avant de commencer
   close(tube1[1]);
   close(tube2[1]);
   Les fils heritent des tubes en lecture et en ecriture
 */
