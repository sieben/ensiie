/* Solution de l'ED */

#include <stdio.h>
#include <fcntl.h>
#include <dlfcn.h>

#include "compress.h"

#define MAX_SIZE 4096

void (*operation)(UBYTE *, ULONG, UBYTE *, ULONG *) = NULL;
UBYTE buffer_in[MAX_SIZE + 256];
UBYTE buffer_out[MAX_SIZE + 256];

void treatment(char *input, char *output)
{
  FILE *in = NULL, *out = NULL;
  ULONG length_in = 0;
  ULONG length_out = 0;

  if((in = fopen(input, "r")) == NULL) {
	perror("Erreur ouverture fichier d'entree");
	exit(1);
  }

  if((out = fopen(output, "w")) == NULL) {
	perror("Erreur ouverture fichier de sortie");
	exit(1);
  }

  length_in = fread(buffer_in, sizeof(UBYTE), MAX_SIZE, in);

  operation(buffer_in, length_in, buffer_out, &length_out);

  fwrite(buffer_out, sizeof(UBYTE), length_out, out);
   
  fclose(in);
  fclose(out);
}

/*
 Dynamic load of a function 'function'() stored in the file 'objet'.o;
 'loader' returns the memory address where the function is loaded.
*/
void (* loader(char *objet, char *function))(UBYTE*, ULONG, UBYTE*,
ULONG*)
{
	char commande[256];
	char partageable[256];
	void *so_handle;

	sprintf(commande,"cc -shared -o %s.so %s.c\n",objet,objet);
	printf("%s", commande);
	system(commande);

	sprintf(partageable, "%s.so", objet);
	if ((so_handle = dlopen(partageable, RTLD_LAZY)) == NULL)
	{
		fprintf(stderr, "Impossible to open %s: %s\n", partageable,
		    dlerror());
		return NULL;
	}

	return (void (*) (UBYTE *, ULONG, UBYTE *, ULONG *)) 
					    dlsym(so_handle, function);
}

char *argv0;

void usage(void);

main(int argc, char **argv)
{

	argv0 = argv[0];
	if(argc < 4) usage();

	operation = loader(argv[1], argv[1]);
	printf("Called function address : %x\n", operation);
	treatment(argv[2], argv[3]);
}

void usage(void)
{
	fprintf(stderr, "Usage: %s operation f1 f2\n", argv0);
	exit(1);
}

