#include <stdio.h>
#include <fcntl.h>
#include <dlfcn.h>
#include <unistd.h>

#include "compress.h"

#define MAX_SIZE 4096

void (*operation)(UBYTE *, ULONG, UBYTE *, ULONG *) = NULL;
/* operation contiendra l'adresse de compress() ou uncompress()*/
UBYTE buffer_in[MAX_SIZE + 256]; /*zone de travail pour treatment*/
UBYTE buffer_out[MAX_SIZE + 256]; /*zone de travai pour treatement*/

/* la fonction treatment() effectue le travail requis : compresser ou
decompresser le fichier dont le nom lui est passe en premier parametre.
Le resultat de l'operation est place dans le fichier dont le nom lui
est passe en second  parametre*/
void treatment(char *input, char *output)
{
  FILE *in = NULL, *out = NULL;
  ULONG length_in = 0;
  ULONG length_out = 0;

  /* Ouverture du fichier d'entree en lecture (fichier a transformer */
  if((in = fopen(...)) == NULL) {
	perror("Erreur ouverture fichier d'entree");
	exit(1);
  }

  /* Creation du fichier de sortie (resultat de la transformation)*/ 
  if((out = fopen(...)) == NULL) {
	perror("Erreur ouverture fichier de sortie");
	exit(1);
  }

  /* lecture dans le fichier d'entree du bloc de donnees a transformer */ 
  length_in = fread(...);

  /* Modification du buffer par appel de l'operation (compress ou uncompress*/
  operation(...);

  /* Ecriture du buffer transforme dans le fichier de sortie */
  fwrite(...);
  
  /* fermeture des fichiers d'entr�e et de sortie */ 
  fclose(in);
  fclose(out);
}

/* La fonction loader() procede au chargement dynamique du contenu d'un
fichier objet designe par "objet".o, puis retourne l'adresse virtuelle ou
la "function"() a ete implantee.
Il faut au prealable rendre le fichier partageable*/

void (* loader(char *objet, char *function))(UBYTE*, ULONG, UBYTE*, ULONG*)
{
	char commande[256]; /*zone pour composer la commmande a executer pour 
			    creer l'objet  partageable */
	char partageable[256]; /* nom de l'objet partageable*/
	void *so_handle;

        /* composition de la commande qui creera le fichier partageable  */
    	/*decommenter pour la seconde question
	sprintf(command, ...);*/

	/*cette operation est inutile si le fichier .so existe deja,
	resultant d'une creation a la main ou d'une execution  precedente*/
	
	/* execution de la commande pour creer l'objet  partageable */ 
	/*decommenter pour la seconde question 
	system (command);*/

	/* creation du nom de l'objet partageable*/
	sprintf(partageable, .......);
	
        /* ouverture de l'objet partageable */
	if ((so_handle = dlopen(...)) == NULL)
	{
		fprintf(stderr, "Impossible to open %s: %s\n", partageable,
		    dlerror());
		return NULL;
	}

        /* retour de l'adresse de la fonction chargee */
	return (void (*) (UBYTE *, ULONG, UBYTE *, ULONG *)) 
					    dlsym(...);
}

char *argv0;

void usage(void);

main(int argc, char **argv)
{

	argv0 = argv[0];
	if(argc < 4) usage();

        /* Chargement de la fonction */
	operation = loader(...);
	printf("Called function address : %x\n", operation);

        /* Traitement */
	treatment(...);
}

void usage(void)
{
	fprintf(stderr, "Usage: %s operation f1 f2\n", argv0);
	exit(1);
}

