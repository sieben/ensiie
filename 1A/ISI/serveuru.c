// Serveuru.c  (Serveur UDP)

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>


char* id=0;
short port=0;
int sock=0;	/* socket de communication */
int  nb_reponse=0;

int main(int argc, char** argv)
{
int ret;
struct  sockaddr_in serveur; /* SAP du serveur */

	if (argc!=3) {
		fprintf(stderr,"usage: %s id port\n",argv[0]);
		_exit(1);
	}
	id= argv[1];
	port= atoi(argv[2]);

	if ((sock = socket (AF_INET, SOCK_DGRAM, 0)) == -1) {
		fprintf(stderr,"%s: socket %s\n",argv[0],strerror(errno));
		_exit(1);
	}
	serveur.sin_family = AF_INET;
	serveur.sin_port = htons(port);
	serveur.sin_addr.s_addr = INADDR_ANY;
	if (bind (sock, (struct sockaddr*)&serveur, sizeof(serveur))<0) {
		fprintf(stderr,"%s: bind %s\n",argv[0],strerror(errno));
		_exit(1);
	}
	struct  sockaddr_in client; /* SAP du client */
	unsigned int client_len=sizeof(client);

	while (1) {
		char    buf_read[1<<8], buf_write[1<<8];

		ret=recvfrom(sock,buf_read,1<<8, 0, (struct sockaddr*)&client,
			 &client_len);
		if (ret<=0) {
			printf("%s: recvfrom=%d:%s\n",argv[0],ret,strerror(errno));
			continue;
		}
		printf("serveur %s  (%s:%d) recu le message %s\n",id,
			 inet_ntoa(serveur.sin_addr),ntohs(serveur.sin_port),
			 buf_read);
		sprintf(buf_write,"#%2s reponse%03d#",id,nb_reponse++);
		ret=sendto(sock,buf_write,strlen(buf_write)+1,
			0, (struct sockaddr*)&client, client_len);
		if (ret<=0) {
			printf("%s: sendto=%d: %s\n",argv[0],ret,strerror(errno));
			continue;
		}
		sleep(6);
	}
	return 0;
}


