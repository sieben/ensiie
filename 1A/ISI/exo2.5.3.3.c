#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>

void fils(){
    printf("Je vais mourir dans 5 secondes \n");
    sleep(5);
    getpid();
    exit(0);
}


int main(){


    int n = 1;
    int pid;
    if ( ( pid = fork() ) == 0 ) fils();
    else { 
        while( n != 0 ){
            scanf("%d",&n); 
            fils();
        };
        kill(0,SIGTERM);
        exit(0);
    }
    return 0;
}

/*Correction :
fils 1 & 2
sleep(5)*/
