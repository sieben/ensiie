#include <signal.h>
#include <unistd.h>
#include <stdio.h>
/* ajouter les fonctions de traitement associ�es aux signaux */

void fonc3(){
 /* � compl�ter pour prendre en compte SIGUSR1 */
}

void fonc2(){
 /* cr�er un fils qui attend 3 secondes puis se termine */
 /* � compl�ter pour prendre en compte SIGCHLD */
 fonc3();
}

void fonc1(){
 /* � compl�ter pour prendre en compte SIGINT */
 fonc2();
}

int main(){
 fonc1();
 printf("fin du main\n");
 return(0);
}
