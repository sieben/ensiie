/* ISI - TP Not� du 070203
   Exercice 1
*/

#include <unistd.h>	/* lseek */
#include <stdio.h>	/* printf */
#include <fcntl.h>	/* open */

int main(void) {
    
    int hFichier;
    int uOffset;
    char szString[11];
    int uSize;
    
    /* on r�cup�re le descripteur du fichier et on ouvre en lecture/�criture */
    if ((hFichier = open("fich", O_RDWR)) == -1) {
	printf("Erreur : impossible d'ouvrir \"fich\".\n");
	return 1;
    }

    /* on se place 2000 bytes avant la fin du fichier */
    uOffset = lseek(hFichier, -2000, SEEK_END);
    printf("Offset (2000 bytes avant la fin du fichier) : %d\n", uOffset);
    
    /* on lit une cha�ne de 10 caract�res */
    uSize = read(hFichier, szString, 10);
    
    /* et on l'affiche */
    write(1, szString, uSize);
    write(1, "\n", 1);
    
    return 0;
}
