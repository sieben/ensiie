#include <stdio.h> /* printf */
#include <unistd.h> /* pause */
#include <signal.h> /* signal */

int *sommet;
void fonc1();
void fonc2();
void fonc3();

void pile()
{int i;

 printf("sommet de pile\n");
 printf("adresse sommet = %d\n",sommet);
 for (i=0;i<6;i++)
     printf("addresse=%d, contenu=%d\n",sommet+i,*(sommet+i));
}

int main (){
int a=1;

sommet = &a;
signal(SIGINT, pile);

pause();
fonc1();
}

void fonc1 (){
int b=2;

sommet = &b;
signal(SIGINT, pile);

pause();
fonc2();
}

void fonc2 (){
int c=3;

sommet = &c;
signal(SIGINT, pile);

pause();
fonc3();
}

void fonc3 (){
int d=4;

sommet = &d;
signal(SIGINT, pile);

pause();
}
