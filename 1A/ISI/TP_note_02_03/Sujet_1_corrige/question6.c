/* TP Not� d'ISI - le 04/02/2003
 * Exercice 3
 */

#include <signal.h> /* signal() */
#include <unistd.h> /* read(), write(), dup(), execlp() */
#include <stdio.h> /* printf() */


void trait1()
{
	printf("R�ception de SIGINT\n");
}

void trait2()
{
	printf("R�ception de SIGCHLD\n");
}

void trait3()
{
	printf("R�ception de SIGUSR1\n");
}

void fonc3()
{
	printf("d�but de fonc3\n");
	signal(SIGUSR1, trait3);
	pause();
	printf("fin de fonc3\n");
}

void fonc2()
{
	printf("d�but de fonc2\n");
	if (!fork()) {
		sleep(3);
		_exit(0);
	}
	signal(SIGCHLD, trait2);
	pause();
	signal(SIGCHLD, SIG_DFL);
	printf("fin de fonc2\n");
	fonc3();
}

void fonc1()
{
	printf("d�but de fonc1\n");
	signal(SIGINT, trait1);
	pause();
	signal(SIGINT, SIG_DFL);
	printf("fin de fonc1\n");
	fonc2();
}

int main()
{
	fonc1();
	printf("fin du main\n");
	return 0;
}
