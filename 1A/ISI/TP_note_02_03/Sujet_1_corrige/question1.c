/* TP Not� d'ISI - le 04/02/2003
 * Exercice 1 - Question 1
 */

#include <fcntl.h> /* open(), close() */
#include <unistd.h> /* write(), getlogin() */
#include <stdio.h> /* printf() */
#include <ctype.h> /* tolower() */
#include <string.h> /* strlen() */


int main(int argc, char **argv)
{
	int hFichier;
	char *szLogin;
	
	if (argc != 2) {
		printf("Usage : %s <fichier>\n", argv[0]);
		return 1;
	}
	else {
		/* on ouvre le fichier et on r�cup�re son handle
		 * possibilit� de cr�ation du fichier avec les bonnes permissions */
		if ((hFichier = open(argv[1], O_WRONLY | O_CREAT,0600)) == -1) {
			printf("Erreur : impossible d'ouvrir %s\n", argv[1]);
			return 1;
		}
		
		/* on r�cup�re le login dans szLogin */
		szLogin = getlogin();
		
		/* on se positionne au bon endroit dans le fichier */
		lseek(hFichier, 1000*(tolower(*szLogin)-'a'+1), SEEK_SET);

		/* on �crit le login � l'offset en question */
		write(hFichier, szLogin, strlen(szLogin));
		
		/* on ferme le fichier */
		close(hFichier);
		return 0;
	}
}

