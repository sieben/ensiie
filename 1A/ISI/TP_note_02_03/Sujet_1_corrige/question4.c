/* TP Not� d'ISI - le 04/02/2003
 * Exercice 2 - Question 2
 */

#include <unistd.h> /* read(), write(), dup(), execlp() */
#include <stdio.h> /* printf() */
#include <stdlib.h> /* atoi() */

int main(int argc, char **argv)
{
	int entree, sortie;
	
	if (argc == 3) {		
		entree = atoi(argv[1]);
		sortie = atoi(argv[2]);
		
		if (entree == sortie || entree < 3 || sortie < 3) {
			printf("Erreur : entr�e et sortie invalides !\n");
			return 1;
		}
		/* on copie stdout sur sortie */
		printf("Sortie %d\n", dup2(1, sortie));
		/* on copie stdin sur entree */
		printf("Entr�e %d\n", dup2(0, entree));
		/* on ex�cute lire avec les param�tres d'entr�e/sortie */
		execlp("./lire", "lire", argv[1], argv[2], 0);

		return 0;
		
	}
	else  {
		printf("Usage : %s [<entr�e> <sortie>]\n", argv[0]); 
		return 1;
	}
}

