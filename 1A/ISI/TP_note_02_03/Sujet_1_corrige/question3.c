/* TP Not� d'ISI - le 04/02/2003
 * Exercice 2 - Question 1
 */

#include <unistd.h> /* read(), write(), dup(), execlp() */
#include <stdio.h> /* printf() */

int main(void)
{
	/* on copie stdout sur 3 */
	printf("Sortie %d\n", dup2(1, 3));
	/* on copie stdin sur 5 */
	printf("Entr�e %d\n", dup2(0, 5));
	/* on ex�cute lire53 avec 5 = stdin et 3 = stdout */
	execlp("./lire53", "lire53", 0);
	
	return 0;
}

