/* TP Not� d'ISI - le 04/02/2003
 * Exercice 3
 */

#include <signal.h> /* signal() */
#include <unistd.h> /* read(), write(), dup(), execlp() */
#include <stdio.h> /* printf() */

int tube[2];

void fils(int i)
{
	close(tube[0]);
	signal(SIGPIPE, _exit);
	while(1) {
		sleep(i);
		switch(i) {
			case 1 : write(tube[1], "1", 1);
					 break;
			case 2 : write(tube[1], "3", 1);
					 break;
			case 3 : write(tube[1], "2", 1);
					 break;
		}
	}
}


int main()
{
	char c;

	pipe(tube);
	if (fork())
		if (fork())
			if (fork()) {
				/* processus p�re */
				close(tube[1]);
				signal(SIGALRM, _exit);
				alarm(20);
				while (1) {
					read(tube[0], &c, 1);
					printf("%c\n", c);
				}
			} else fils(3);
		else fils(2);
	else fils(1);

	return 0;
}
