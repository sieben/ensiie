/* TP Not� d'ISI - le 04/02/2003
 * Exercice 1 - Question 2
 */

#include <fcntl.h> /* open(), close() */
#include <unistd.h> /* read(), write() */
#include <stdio.h> /* printf() */
#include <ctype.h> /* tolower() */
#include <string.h> /* strlen() */
#include <stdlib.h> /* malloc() */

int main(int argc, char **argv)
{
	int hFichier;
	char *szLogin;
	char *szResultat;
	
	if (argc != 2) {
		printf("Usage : %s <fichier>\n", argv[0]);
		return 1;
	}
	else {
		/* on ouvre le fichier et on r�cup�re son handle
		 * possibilit� de cr�ation du fichier avec les bonnes permissions */
		if ((hFichier = open(argv[1], O_RDONLY)) == -1) {
			printf("Erreur : impossible d'ouvrir %s\n", argv[1]);
			return 1;
		}
		
		/* on r�cup�re le login dans szLogin */
		szLogin = getlogin();
		
		/* on se positionne au bon endroit dans le fichier */
		lseek(hFichier, 1000*(tolower(*szLogin)-'a'+1), SEEK_SET);

		szResultat = (char *) malloc(strlen(szLogin+1));
		/* on lit le login � l'offset en question */
		read(hFichier, szResultat, strlen(szLogin));
		
		/* on ferme le fichier */
		close(hFichier);

		write(1, szResultat, strlen(szResultat));
		write(1, "\n", 1);
		return 0;
	}
}

