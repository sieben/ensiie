#include <stdio.h>
#include <stdlib.h>


void termine_string();


void recop(int lg, char *t1, char *t2)
{
	int j;
	for(j = 0; j < lg; j++)
		t2[j] = t1[j];

	termine_string();
}

int main(int argc, char** argv)
{
	char tabc[8] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
	int lg = 8;
	char *ptr = (char *) malloc(lg);
	recop(lg, tabc, ptr);
	printf("%s\n", ptr);
	
	return 0;
}


/*
 * Quelques explications sur termine_string()
 *
 * La fonction attendue par le prof sur votre copie 
 * est celle donnée dans les slides, càd :
 * """
 * void termine_string() {
 *		int base;
 *		((char*) *(&base + 8))[7] = '\0';
 * }
 * """
 * Cette fonction, compilée avec des options que je n'ai pas 
 * trouvé, permet effectivement de répondre à la question.
 *
 * Ce que je vous propose ici, c'est une fonction qui marche
 * sur mon pc, avec mon compilateur, mon os, ... Donc ne vous
 * étonnez pas si lorsque vous compilez et testez, ça ne marche
 * pas (tant mieux si ça marche d'ailleurs :D).
 *
 * Mais je vous propose aussi un moyen de le faire marcher chez 
 * vous. La boucle for ci-dessous permet l'affichage de la pile
 * sur 30 entrées.
 * Vous pouvez ajouter un printf dans votre main pour trouver 
 * quelle adresse a été renvoyée par malloc, mais vous pouvez
 * aussi essayer de la trouver avec cette dite boucle et la pile
 * telle qu'elle est affichée. Bonne chance :)
 *
 */

void termine_string()
{
	/*
	 * Le code attendu :
	 * """
	 * int base;
	 * ((char*) *(&base + 8))[7] = '\0';
	 * """
	 */

	/*
	 * Le code pour analyser votre pile :
	 */
	int *base;

	// Regardez votre pile, et cherchez où est empilée l'adresse retournée par malloc
	for(base = (int*) &base; base < ((int*)&base) + 30; base++)
		printf("%p : %8x\n", base, *base);

	((char*) *((int*)&base + 15))[7] = '\0';
}
