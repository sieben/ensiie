/* solution complete du programme pour lire une disquette msdos 
   avec utilisation d'un seul grand buffer contenant toute la disquette*/
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define DISK_SIZE 2880*512

/* info du volume */
int SectorSize;		/* taille du secteur en octets */
int ClusterSectSize;	/* taille du cluster en secteurs */
int ClusterByteSize;	/* taille du cluster en octets */
int FirstRootDirSect;	/* premier secteur du repertoire racine */
int NbRootEntries;	/* nombre d'entree dans le repertoire racine */
int FirstDataSector;	/* premier secteur de la zone data*/
int ReservedSectors;

char buf[DISK_SIZE];

void print_vol_info(const unsigned char* buf);

void print_dir_entry(const unsigned char* pentry);

unsigned char* read_data_cluster(int FirstFileCluster);

unsigned short get_next_cluster(unsigned short current_cluster);


int main()
{
	int i;
	if (read(0,buf,DISK_SIZE)== -1) {perror("pb lecture fichier : ");_exit(0);}
	print_vol_info(buf);
	fprintf(stderr,"\n**************Contenu du repertoire racine ************\n\n");
	//for (i=0 ; i<NbRootEntries ; i++)
	for (i=0 ; i<10 ; i++)
		print_dir_entry(buf+(FirstRootDirSect*SectorSize)+(i*32));
}

void print_vol_info(const unsigned char* buf)

{
	int FatNumber;
	int SectorNumber;
	int FatSectSize;
	int DiskByteSize;
	
	fprintf(stderr,"\n*****************informations sur le volume : ******************\n\n");
	SectorSize = *(unsigned short*) (buf+11);
	fprintf(stderr,"taille secteur= %d octets\n",SectorSize);
	ClusterSectSize = *(unsigned char*) (buf+13);
	fprintf(stderr,"nb secteur par cluster= %d secteurs\n",ClusterSectSize);
	ReservedSectors = *(unsigned short *)(buf+14);
	fprintf(stderr,"nb secteurs reserves= %d\n",ReservedSectors);
	FatNumber = *(unsigned char *)(buf+16);
	fprintf(stderr,"nb fats= %d\n",FatNumber);
	NbRootEntries = *(unsigned short *) (buf+17);
	fprintf(stderr,"nb entrees rootdir= %d\n",NbRootEntries);
	SectorNumber = *(unsigned short *)(buf+19);
	fprintf(stderr,"nb secteurs sur disque= %d\n",SectorNumber);
	FatSectSize = *(unsigned short *)(buf+22);
	fprintf(stderr,"longueur fat en secteurs= %d\n", FatSectSize);
	ClusterByteSize = SectorSize * ClusterSectSize;
	fprintf(stderr,"ClusterByteSize= %d\n",ClusterByteSize);
	DiskByteSize = SectorNumber * SectorSize;
	fprintf(stderr,"DiskByteSize = %d, soit %3.1f MO\n",
			DiskByteSize,(float)DiskByteSize /(1<<20));
	FirstRootDirSect = ReservedSectors + FatNumber * FatSectSize;
	fprintf(stderr,"FirstRootDirSect= %d\n",FirstRootDirSect);
	FirstDataSector = FirstRootDirSect+ NbRootEntries * 32/ SectorSize;
	fprintf(stderr,"FirstDataSector= %d\n",FirstDataSector);
}


void print_dir_entry(const unsigned char* pentry)
{
#define Name	pentry 		/* nom du fichier (8 premiers octets) */
#define EMPTY_ENTRY 0x00 /* marks file as deleted when in name[0] */
#define DELETED_FILE 0xe5 /* marks file as deleted when in name[0] */

#define Ext		pentry+8	/* extension (3 octes suivants)*/
#define Attr	pentry+11	/* attributs (octets suivant) */

#define ATTR_RO      1  /* read-only */
#define ATTR_HIDDEN  2  /* hidden */
#define ATTR_SYS     4  /* system */
#define ATTR_VOLUME  8  /* volume label */
#define ATTR_DIR     16 /* directory */
#define ATTR_ARCH    32 /* archived*/
#define BIG_MASK (ATTR_RO | ATTR_HIDDEN | ATTR_SYS | ATTR_VOLUME)

#define ATTR_OTH	(ATTR_HIDDEN | ATTR_SYS | ATTR_VOLUME | ATTR_ARCH)
#define ATTR_NON_NORMAL (ATTR_HIDDEN | ATTR_SYS | ATTR_VOLUME | ATTR_DIR)

#define First_cluster	*(unsigned short *)(pentry+26)
#define Size	*(int *)(pentry+28) /* taille du fichier */

int i,align;
int normal;
	fprintf(stderr,"\n************* contenu d'une entr�e : ***************\n\n");
	fprintf(stderr,"adresse= %x : ",pentry);
	/* entree valide ? (donnee par le premier octet) */
	if ( (*Name == EMPTY_ENTRY) ||
		 (*Name== DELETED_FILE) ){
			fprintf(stderr,"entree vide ou fichier detruit\n");
			return;
		}
	/* entr�e pour un grand nom */
	if ((*(Attr) & BIG_MASK) == BIG_MASK){
			fprintf(stderr,"entr�e pour un grand nom\n");
			return;
	}
	/* nom du fichier  (8 premiers octets + 3 pour l'extension) */
    fprintf(stderr," nom du fichier :");
	for (i=0 ; i <8 ; i++) {
		if (*(Name+i)!=' ' ) 
			fprintf(stderr,"%c",0x20<=*(Name+i) && *(Name+i)<0x7f ? *(Name+i) : '?');
		else
			break;
	}
	align = 8-i+1;
	for (i=0 ; i <3 ; i++) {
		if (*(Ext+i)!=' ' ) {
			if (i==0)  {
				fprintf(stderr,"%c",'.');
				align--;
			}
			fprintf(stderr,"%c",0x20<=*(Ext+i) && *(Ext+i)<0x7f ? *(Ext+i) : '?');
		} else
			break;
	}
	align +=3-i;
	for (i=0 ; i<align ; i++) fprintf(stderr," ");

	/* attribut (12ieme octet) */ 
	fprintf(stderr," RO=%c HI=%c SY=%c VO=%c DI=%c AR=%c",
		*(Attr) & ATTR_RO ? 'T' : ' ',
		*(Attr) & ATTR_HIDDEN ? 'T' : ' ',
		*(Attr) & ATTR_SYS ? 'T' : ' ',
		*(Attr) & ATTR_VOLUME ? 'T' : ' ',
		*(Attr) & ATTR_DIR ? 'T' : ' ',
		*(Attr) & ATTR_ARCH ? 'T' : ' '
	);
	normal= (*(Attr) & ATTR_NON_NORMAL)==0 ;
	
	/* taille (28-29-30-31) */
	fprintf(stderr," SZ=%04d",Size);

	/* premier cluster (26-27) */
	fprintf(stderr," FC=%04d",First_cluster);
	fprintf(stderr,"\n");

	if (normal) {
		unsigned short next;
		unsigned char* p= read_data_cluster(First_cluster);
		fprintf(stderr,"\n***** contenu premier cluster (%d) du fichier: ****\n\n",First_cluster);
		for (i=0 ; i<512 && i<Size ; i++) {
			fprintf(stderr,"%c",*p);
			p++;
		}
		next =First_cluster;
		while ((next=get_next_cluster(next)) != 0xfff){
			fprintf(stderr,"\n\n********* contenu du cluster suivant (%d)******** \n\n",next);
			sleep(2);
			for (i=0 ; i<512 && i<Size ; i++) {
				fprintf(stderr,"%c",*p);
				p++;
			}
		}
		fprintf(stderr,"\n************************************************************ \n");
		fprintf(stderr,"                     fin du fichier \n");
		fprintf(stderr,"*********************************************************** \n\n");
	}	
	else fprintf(stderr,"\n******************** fichier anormal ****************\n");
}

unsigned char* read_data_cluster(int FirstFileCluster)
{
	return buf+(FirstDataSector+((FirstFileCluster-2)*ClusterSectSize))*SectorSize;
}
unsigned short get_next_cluster(unsigned short current_cluster){
	if (current_cluster == 0xfff) {
		fprintf(stderr," �a va pas la tete : fin de chainage !\n");
		return current_cluster;
	} else {
		/* compliqu� � cause du petit indien : 
		version d�taill�e : 
		unsigned short nosuivant; 
		unsigned char * fat =(buf + ReservedSectors * SectorSize);
		unsigned short  nobit = (current_cluster+1)*12;
		unsigned short nooctet = nobit /8;
		if (nobit % 8) 
			nosuivant = ((fat[nooctet] & 0xf)<<8) + fat[nooctet-1];
		else 
			nosuivant = (fat[nooctet-1]<<4) + (fat[nooctet-2]>>4);
		return nosuivant;*/
		/*version courte : */
		unsigned short nosuivant = * (unsigned short *)(buf+
				ReservedSectors*SectorSize+current_cluster*3/2);
		return (current_cluster%2) ? nosuivant>>4 : nosuivant & 0xfff;
	}
}

