#!/bin/sh

if [ $# = 0 ]
    then echo usage = $0 fichier [fichier]
    exit
fi
for i in $*
do
    if [ -f $i]
    then echo "Voulez-vous supprimer $i ?"
	continue = true
	while [ $continue = true]
	read rep
	case $rep in
	    o|O|y|Y) echo "Le fichier a ete supprime";
		$continue = false;;
	    n|N) echo "Le fichier n'a pas ete supprime";
		$continue = false;;
	    *) echo "Veuillez rentrer une reponse correcte";;
	esac
	done
    fi
done