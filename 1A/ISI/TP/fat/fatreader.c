/* solution complete du programme pour lire une disquette msdos 
   avec utilisation d'un seul grand buffer contenant toute la disquette*/
#include <stdio.h>
#include <string.h>
#define DISK_SIZE 2880*512

// info du volume 
int SectorSize;			// taille du secteur en octets 
int ClusterSectSize;	// taille du cluster en secteurs 
int ClusterByteSize;	// taille du cluster en octets 
int FirstRootDirSect;	// premier secteur du repertoire racine 
int NbRootEntries;		// nombre d'entree dans le repertoire racine 
int FirstDataSector;	// premier secteur de la zone data


// Pour simplifier la programmation et �viter la programmation des lectures
// successives de secteurs, on peut lire la totalit� de la disquette en une
// seule fois. Comme cette op�ration est toutefois assez longue, il est
// conseill� de faire une image de la disquette sur un fichier du disque dur
// local par la commande "cp /dev/fd0 image" puis de travailler ensuite �
// partir du fichier image.
// Afin d'avoir un maximum de souplesse on pourra consid�rer que le contenu
// de la disquette est accessible sur l'entr�e standard, quite � rediriger
// cette derni�re. On ex�cutera par exemple la commande:
// monprog </dev/fd0
// ou si on a recopi� le contenu de la disquette sur un fichier image:
// monprog <image
//
 
char buf[DISK_SIZE]; 	//pour recevoir le contenu de la disquette

void print_vol_info(const unsigned char* buf);

void print_dir_entry(const unsigned char* pentry);

unsigned char* read_data_cluster(int FirstFileCluster);

main()
{
	int i;
	read(0,buf,DISK_SIZE);
	print_vol_info(buf);
	fprintf(stderr,"\nContenu du repertoire racine \n\n");
	for (i=0 ; i<NbRootEntries ; i++){
		print_dir_entry(buf+ FirstDirRootSect*SectSize + 32 * i);
   }
}

//
// Fonction d'affichage des caract�ristiques  g�n�rales d'une disquette
// (contenu du secteur de d�marrage)
//
void print_vol_info(const unsigned char* buf)

{
	int ReservedSectors;	//nb de secteurs r�serv�s
	int FatNumber;			//nb de FATs
	int SectorNumber;		//nb de secteurs
	int FatSectSize;		//taille d'une FAT en nombre de secteurs
	int DiskByteSize;		//taille d'un disque en octets
	
	fprintf(stderr,"\ninformations sur le volume : \n\n");
	SectorSize = *(unsigned short*) (buf+11);
	fprintf(stderr,"taille secteur= %d octets\n",SectorSize);
	ClusterSectSize = *(unsigned char*) (buf+13);
	fprintf(stderr,"nb secteur par cluster= %d secteurs\n",ClusterSectSize);
	ReservedSectors = *(unsigned short*) (buf+14);
	fprintf(stderr,"nb secteurs reserves= %d\n",ReservedSectors);
	FatNumber = *(unsigned char*) (buf+16);
	fprintf(stderr,"nb fats= %d\n",FatNumber);
	NbRootEntries = *(unsigned short*) (buf+17);
	fprintf(stderr,"nb entrees rootdir= %d\n",NbRootEntries);
	SectorNumber = *(unsigned short*) (buf+19);
	fprintf(stderr,"nb secteurs sur disque= %d\n",SectorNumber);
	FatSectSize = *(unsigned short*) (buf+22);
	fprintf(stderr,"longueur fat en secteurs= %d\n", FatSectSize);
	ClusterByteSize = SectorSize * ClusterSectSize;
	fprintf(stderr,"ClusterByteSize= %d\n",ClusterByteSize);
	DiskByteSize = SectorNumber * SectorSize;
	fprintf(stderr,"DiskByteSize = %d, soit %3.1f MO\n",
			DiskByteSize,(float)DiskByteSize /(1<<20));
	FirstRootDirSect = ReservedSectors + FatNumber * FatSectSize;
	fprintf(stderr,"FirstRootDirSect= %d\n",FirstRootDirSect);
	FirstDataSector = (FirstRootDirSect+NbRootEntries*32)/SectorSize;
	fprintf(stderr,"FirstDataSector= %d\n",FirstDataSector);
}

//
// fonction d'affichage des caract�rstiques d'un fichier
// (contenu d'une entr�e du rootdir)
//
void print_dir_entry(const unsigned char* pentry)
{
#define Name	pentry 		// nom du fichier (8 premiers octets) 
#define EMPTY_ENTRY 0x00 	// marks file as deleted when in name[0] 
#define DELETED_FILE 0xe5 	// marks file as deleted when in name[0] 

#define Ext pentry+8	// extension (3 octes suivants)
#define Attr pentry+11	// attributs (octets suivant) 

#define ATTR_RO      1  	// read-only 
#define ATTR_HIDDEN  2  	// hidden 
#define ATTR_SYS     4  	// system 
#define ATTR_VOLUME  8  	// volume label 
#define ATTR_DIR     16 	// directory 
#define ATTR_ARCH    32 	// archived

#define COMPL_MASK (ATTR_RO | ATTR_HIDDEN | ATTR_SYS | ATTR_VOLUME)

#define ATTR_OTH		(ATTR_HIDDEN | ATTR_SYS | ATTR_VOLUME | ATTR_ARCH)
#define ATTR_NON_NORMAL (ATTR_HIDDEN | ATTR_SYS | ATTR_VOLUME | ATTR_DIR)

#define First_cluster	*(short *)(pentry+26)
#define Size	*(int *)(pentry+28) 		// taille du fichier

int i,align;
int normal;

	// entree valide ? (donnee par le premier octet) 
	if ( (*Name == EMPTY_ENTRY) ||		// entree vide
		 (*Name== DELETED_FILE) ){		//fichier supprime
			fprintf(stderr,"entree vide ou fichier detruit\n");
			return;
		}
	// complement 
	/*if (*(Attr) = mask .............................){	
			fprintf(stderr,"complement de nom\n");
			return;
			}*/
	// nom du fichier  (8 premiers octets + 3 pour l'extension) 
	//	........................	// impression du nom du fichier

	//	.......................		// affichage du point

	//	......................		// affichage extension

	// attribut (12ieme octet) 

	//	.....................		// affichage des 6 attributs
	
	normal= (*(Attr) & ATTR_NON_NORMAL)==0 ;
	
	// taille (octets 28-29-30-31) 
	//	....................		//affichage de la taille
	// premier cluster (octets 26-27) 
	//	....................		//affichage du num�ro du premier cluster
	//
	 // si ce n'est pas un fichier normal, on n'affichera pas son contenu
	 //
	normal= (*(Attr) & ATTR_NON_NORMAL)==0 ;	
	if (normal) {
		unsigned char* p= read_data_cluster(First_cluster);
		fprintf(stderr,"contenu du fichier :\n    ");
		for (i=0 ; i<100 && i<Size ; i++) {
		
	//	...........................	// affichage du contenu du fichier
	//	...........................   // 100 octets seulement au max
		// indication : les caract�res affichables c sont tels que
		// 0x20 <= c <t 0x7F. remplacer les autres par '?'
		}
		
		fprintf(stderr,"\n\n");
	}	
	else fprintf(stderr,"fichier anormal\n");
}

/*
* renvoie l'adresse du premier cluster du fichier dans buf a partir de son
* numero
*/
unsigned char* read_data_cluster(int FirstFileCluster)
{
	return buf + ( FirstDataSector + ( ( FirstFileCluster - 2 ) * ClusterSectSize ) ) * SectorSize;
}
