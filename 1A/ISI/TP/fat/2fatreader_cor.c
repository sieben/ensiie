/* solution complete du programme pour lire une disquette msdos 
   avec utilisation d'un seul grand buffer contenant toute la disquette*/
/* utilisation de la d�finition d'une structure de donn�e fat_boot_sector
 * trouv�e dans msdos_fs.h. Apparemment cela ne pose plus de pb 
 * d'alignement comme j'en avais rencontr� autrefois. On pourrait
 * sans doute utiliser aussi la structure de donn�es dir_entry.
 * Bizarrerie : MSDOS_EOF12 est d�finie comme 0XFF8 alors que j'utilise
 * 0XFFF avec succes dans ce m�me programme !!!
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define DISK_SIZE 2880*512
//#include <linux/fs.h> //$$
//#include <linux/stat.h> //$$
//#include <linux/fd.h> //$$
//#include <asm/byteorder.h> //$$

struct fat_boot_sector {
	char    ignored[3];     /* Boot strap short or near jump */
	char    system_id[8];   /* Name - can be used to special case
	                              partition manager volumes */
	unsigned char    sector_size[2]; /* bytes per logical sector */
	unsigned char    cluster_size;   /* sectors/cluster */
	unsigned short   reserved;       /* reserved sectors */
	unsigned char    fats;           /* number of FATs */
	unsigned char    dir_entries[2]; /* root directory entries */
	unsigned char    sectors[2];     /* number of sectors */
	unsigned char    media;          /* media code (unused) */
	unsigned short   fat_length;     /* sectors/FAT */
	unsigned short   secs_track;     /* sectors per track */
	unsigned short   heads;          /* number of heads */
	unsigned int   hidden;         /* hidden sectors (unused) */
	unsigned int   total_sect;     /* number of sectors (if sectors == 0) */
};
/* info du volume */
int SectorSize;		/* taille du secteur en octets */
int ClusterSectSize;	/* taille du cluster en secteurs */
int ClusterByteSize;	/* taille du cluster en octets */
int FirstRootDirSect;	/* premier secteur du repertoire racine */
int NbRootEntries;	/* nombre d'entree dans le repertoire racine */
int FirstDataSector;	/* premier secteur de la zone data*/
int ReservedSectors;

char buf[DISK_SIZE];

void print_vol_info(const unsigned char* buf);

void print_dir_entry(const unsigned char* pentry);

unsigned char* read_data_cluster(int FirstFileCluster);

unsigned short get_next_cluster(unsigned short current_cluster);


int main()
{
	int i;
	struct fat_boot_sector *fbs;//$$
	if (read(0,buf,DISK_SIZE)== -1) {perror("pb lecture fichier : ");_exit(0);}
	print_vol_info(buf);
	fbs = (struct fat_boot_sector *)buf; //$$
	printf("\nsector_size : %d\n",*(unsigned short*)fbs->sector_size);//$$
	printf("cluster_size : %d\n",fbs->cluster_size);//$$
	printf("reserved : %d\n",fbs->reserved);//$$
	printf("fats : %d\n",fbs->fats);//$$
	printf("dir_entries : %d\n",*(unsigned short *)fbs->dir_entries);//$$
	printf("sectors : %d\n",*(unsigned short *)fbs->sectors);//$$
	printf("fat_length : %d\n",fbs->fat_length);//$$
	printf("secs_track : %d\n",fbs->secs_track);//$$
	printf("heads : %d\n",fbs->heads);//$$
	printf("total_sect : %d\n",fbs->total_sect);//$$
	//_exit(0);//$$
	fprintf(stderr,"\n**************Contenu du repertoire racine ************\n\n");
	//for (i=0 ; i<NbRootEntries ; i++)
	for (i=0 ; i<20 ; i++)
		print_dir_entry(buf+(FirstRootDirSect*SectorSize)+(i*32));
    return 0;
}

void print_vol_info(const unsigned char* buf)

{
	int FatNumber;
	int SectorNumber;
	int FatSectSize;
	int DiskByteSize;
	
	fprintf(stderr,"\n*****************informations sur le volume : ******************\n\n");
	SectorSize = *(unsigned short*) (buf+11);
	fprintf(stderr,"taille secteur= %d octets\n",SectorSize);
	ClusterSectSize = *(unsigned char*) (buf+13);
	fprintf(stderr,"nb secteur par cluster= %d secteurs\n",ClusterSectSize);
	ReservedSectors = *(unsigned short *)(buf+14);
	fprintf(stderr,"nb secteurs reserves= %d\n",ReservedSectors);
	FatNumber = *(unsigned char *)(buf+16);
	fprintf(stderr,"nb fats= %d\n",FatNumber);
	NbRootEntries = *(unsigned short *) (buf+17);
	fprintf(stderr,"nb entrees rootdir= %d\n",NbRootEntries);
	SectorNumber = *(unsigned short *)(buf+19);
	fprintf(stderr,"nb secteurs sur disque= %d\n",SectorNumber);
	FatSectSize = *(unsigned short *)(buf+22);
	fprintf(stderr,"longueur fat en secteurs= %d\n", FatSectSize);
	ClusterByteSize = SectorSize * ClusterSectSize;
	fprintf(stderr,"ClusterByteSize= %d\n",ClusterByteSize);
	DiskByteSize = SectorNumber * SectorSize;
	fprintf(stderr,"DiskByteSize = %d, soit %3.1f MO\n",
			DiskByteSize,(float)DiskByteSize /(1<<20));
	FirstRootDirSect = ReservedSectors + FatNumber * FatSectSize;
	fprintf(stderr,"FirstRootDirSect= %d\n",FirstRootDirSect);
	FirstDataSector = FirstRootDirSect+ NbRootEntries * 32/ SectorSize;
	fprintf(stderr,"FirstDataSector= %d\n",FirstDataSector);
}


void print_dir_entry(const unsigned char* pentry)
{
#define Name	pentry 		/* nom du fichier (8 premiers octets) */
#define EMPTY_ENTRY 0x00 /* marks file as deleted when in name[0] */
#define DELETED_FILE 0xe5 /* marks file as deleted when in name[0] */

#define Ext		pentry+8	/* extension (3 octes suivants)*/
#define Attr	pentry+11	/* attributs (octets suivant) */

#define ATTR_RO      1  /* read-only */
#define ATTR_HIDDEN  2  /* hidden */
#define ATTR_SYS     4  /* system */
#define ATTR_VOLUME  8  /* volume label */
#define ATTR_DIR     16 /* directory */
#define ATTR_ARCH    32 /* archived*/
#define BIG_MASK (ATTR_RO | ATTR_HIDDEN | ATTR_SYS | ATTR_VOLUME)

#define ATTR_OTH	(ATTR_HIDDEN | ATTR_SYS | ATTR_VOLUME | ATTR_ARCH)
#define ATTR_NON_NORMAL (ATTR_HIDDEN | ATTR_SYS | ATTR_VOLUME | ATTR_DIR)

#define First_cluster	*(unsigned short *)(pentry+26)
#define Size	*(int *)(pentry+28) /* taille du fichier */

int i,align;
int normal;
static int EntryIndex=0;
	fprintf(stderr,"\n\n****** contenu de l'entr�e : %d *************\n",
    EntryIndex++);
	fprintf(stderr,"adresse= %x : ",pentry);
	/* entree valide ? (donnee par le premier octet) */
	if ( (*Name == EMPTY_ENTRY) ||
		 (*Name== DELETED_FILE) ){
			fprintf(stderr,"entree vide ou fichier detruit\n");
			return;
		}
	/* entr�e pour un grand nom */
	if ((*(Attr) & BIG_MASK) == BIG_MASK){
			fprintf(stderr,"entr�e pour un grand nom\n");
	        fprintf(stderr,"\ncomplement de nom: ");
            for (i=0 ; i <8 ; i++) 
		        if (*(Name+i)!=' ' ) 
			        fprintf(stderr,"%c",0x20<=*(Name+i) && *(Name+i)<0x7f ? *(Name+i) : '?');
			return;
	}
	/* nom du fichier  (8 premiers octets + 3 pour l'extension) */
	fprintf(stderr,"\nnom du fichier: ");
    for (i=0 ; i <8 ; i++) {
		if (*(Name+i)!=' ' ) 
			fprintf(stderr,"%c",0x20<=*(Name+i) && *(Name+i)<0x7f ? *(Name+i) : '?');
		else
			break;
	}
	align = 8-i+1;
	for (i=0 ; i <3 ; i++) {
		if (*(Ext+i)!=' ' ) {
			if (i==0)  {
				fprintf(stderr,"%c",'.');
				align--;
			}
			fprintf(stderr,"%c",0x20<=*(Ext+i) && *(Ext+i)<0x7f ? *(Ext+i) : '?');
		} else
			break;
	}
	align +=3-i;
	for (i=0 ; i<align ; i++) fprintf(stderr," ");

	/* attribut (12ieme octet) */ 
	fprintf(stderr," RO=%c HI=%c SY=%c VO=%c DI=%c AR=%c",
		*(Attr) & ATTR_RO ? 'T' : 'F',
		*(Attr) & ATTR_HIDDEN ? 'T' : 'F',
		*(Attr) & ATTR_SYS ? 'T' : 'F',
		*(Attr) & ATTR_VOLUME ? 'T' : 'F',
		*(Attr) & ATTR_DIR ? 'T' : 'F',
		*(Attr) & ATTR_ARCH ? 'T' : 'F'
	);
	normal= (*(Attr) & ATTR_NON_NORMAL)==0 ;
	
	/* taille (28-29-30-31) */
	fprintf(stderr," SZ=%04d",Size);

	/* premier cluster (26-27) */
	fprintf(stderr," FC=%04d",First_cluster);
	fprintf(stderr,"\n");

	if (normal) {
		unsigned short next;
		unsigned char* p= read_data_cluster(First_cluster);
		fprintf(stderr,"\n***** contenu premier cluster (%d) du fichier: ****\n\n",First_cluster);
		for (i=0 ; i<512 && i<Size ; i++) {
			fprintf(stderr,"%c",*p);
			p++;
		}
		next =First_cluster;
		while ((next=get_next_cluster(next)) != 0xfff){
			fprintf(stderr,"\n\n********* contenu du cluster suivant (%d)******** \n\n",next);
			sleep(2);
			for (i=0 ; i<512 && i<Size ; i++) {
				fprintf(stderr,"%c",*p);
				p++;
			}
		}
		fprintf(stderr,"\n************************************************************ \n");
		fprintf(stderr,"                     fin du fichier \n");
		fprintf(stderr,"*********************************************************** \n\n");
	}	
	else fprintf(stderr,"\n******************** fichier anormal ****************\n");
}

unsigned char* read_data_cluster(int FirstFileCluster)
{
	return buf+(FirstDataSector+((FirstFileCluster-2)*ClusterSectSize))*SectorSize;
}
unsigned short get_next_cluster(unsigned short current_cluster){
	if (current_cluster == 0xfff) {
		fprintf(stderr," �a va pas la tete : fin de chainage !\n");
		return current_cluster;
	} else {
		/* compliqu� � cause du petit indien : 
		version d�taill�e : 
		unsigned short nosuivant; 
		unsigned char * fat =(buf + ReservedSectors * SectorSize);
		unsigned short  nobit = (current_cluster+1)*12;
		unsigned short nooctet = nobit /8;
		if (nobit % 8) 
			nosuivant = ((fat[nooctet] & 0xf)<<8) + fat[nooctet-1];
		else 
			nosuivant = (fat[nooctet-1]<<4) + (fat[nooctet-2]>>4);
		return nosuivant;*/
		/*version courte : */
		unsigned short nosuivant = * (unsigned short *)(buf+
				ReservedSectors*SectorSize+current_cluster*3/2);
		return (current_cluster%2) ? nosuivant>>4 : nosuivant & 0xfff;
	}
}

