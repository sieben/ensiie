// renvoie le nom, les aliases et les adresses notation point�e
// d'une machine.

#include <stdio.h>
#include <errno.h>
#include <netinet/in.h>
#include <string.h>
#include <netdb.h>

int main(int argc, char** argv)
{
struct  hostent * hote;
int i=0;
	if (argc!=2) {
		fprintf(stderr,"usage: %s host\n",argv[0]);
		exit(1);
	};
	
	hote = gethostbyname(argv[1]);
	
	printf("   nom officiel : %s\n",hote->h_name);
	while (hote->h_aliases[i]!=NULL){
		printf("   alias: %s\n",hote->h_aliases[i++]);
		if (i>5) break;
		}
	i=0;
	while (hote->h_addr_list[i]!=NULL) 
	{
		printf("   adresse ip : %s\n", 
		inet_ntoa(*(struct in_addr*)hote->h_addr_list[i++]));
	}
   }


