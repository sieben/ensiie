#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>

void fils(){
printf("Je suis un fils et voici mon numero de PID : %d \n",getpid());
}

int main(){
    int pid;
    if( ( pid = fork() ) == 0 ) 
        fils();
    else{
        int input;
        while(input != 0){
            scanf("%d",&input);
        }
        kill(0,SIGUSR1);
    }
    return 0;
}
/*
Pere
Sur lecture de 0
kill(pid1,SIGUSR1)
kill(pid2,SIGUSR2)
*/
