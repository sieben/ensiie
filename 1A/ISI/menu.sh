#!/bin/sh

#Écrire un script qui tant que l'utilisateur n'a pas tapé 9 :
#* Affiche un menu
#* Demande à l'utilisateur de saisir une option du menu
#* Affiche à l'utilisateur le résultat de sa demande

clear

echo "************************ Main Menu ************************"
echo "<1> Print date "
echo "<2> Print users logged on the same machine"
echo "<3> Print current process list"
echo "<4> Print size of current folder"
echo "<5> Size and disk occupation"
echo "<9> Quitter "
echo "***********************************************************"
read choix
case $choix in
    1 ) date; read;  source menu.sh;;
    2 ) users; read; source menu.sh;;
    3 ) ps; read; source menu.sh;;
    4 ) du; read; source menu.sh;;
    5 ) quota; read; source menu.sh;;
    9 ) exit;;
esac
