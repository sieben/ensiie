/**
 * @file fichiers_obj.c
 * @brief Implémentation du module de traitement des fichiers *.obj
 * @author Lasnier Simon
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "fichiers_obj.h"
#include "affichagencurses.h"

#define     TAILLE_BUF      1024

/**
 * @addtogroup fichier_obj
 */

/*@{*/

/**
 * @brief Ouvrir un fichier avec gestion des erreurs
 * @param adr adresse du fichier à ouvrir
 * @param mode mode d'ouverture du fichier (r, rw, w, a, etc.)
 * @pre le fichier existe et est ouvrable avec le mode fourni
 * @post si l'ouverture s'est bien passée, un FILE* est renvoyé, sinon
 * écrit une erreur sur la sortie d'erreur standard
 * @return un pointeur sur une structure de type FILE qui décrit le fichier
 * @test ouvrir un fichier qui n'existe pas
 * @test ouvrir un fichier inaccessible en lecture
 * @test ouvrir un fichier en écriture alors que l'on a pas le droit
 */

FILE* ouvrir (const char *adr, const char *mode)
{
    FILE *f = fopen(adr, mode);
    if (f == NULL)
    {
        fprintf(stderr, "ouvrir : impossible d'ouvrir '%s' en mode '%s'.\n",
            adr, mode);
        exit(EXIT_FAILURE);
    }
    
    return f;
}

/**
 * @brief Ecrit un tiers des indices d'une face dans un fichier
 * @param pfich un pointeur sur une structure FILE qui décrit le fichier dans
 * lequel on veut enregistrer
 * @param f la face dont on veut écrire les indices
 * @param sommets l'ensemble des sommets trouvés dans les maillages
 * @param normales l'ensemble des normales trouvées dans les maillages
 * @param textures l'ensemble des textures trouvées dans les maillages
 * @param numero le numero du tiers des indices à écrire (0, 1 ou 2)
 * @pre pfich est un pointeur sur un fichier ouvert, f est une face valide,
 * sommets, normales et textures sont des tableau valides, et numero est entre
 * 0 et 2
 * @post les données correspondant à la face sont enregistrées. En vérité la
 * fonction écrit la chaîne " X/X/X" dans le fichier pfich, ou " X//X" ou " X",
 * selon le contenu de la face f.
 */

void ecrire_comp_face (FILE *pfich, face f, tableau sommets, tableau textures,
                       tableau normales, int numero)
{
    int indice_texture = -1;
    int indice_sommet = -1;
    int indice_normale = 1;
    sommet s = NULL;
    vecteur v = NULL;
    texture t = NULL;

    s = extraire_sommet(f, numero);
    indice_sommet = recherche_tableau(sommets, s);
    if (indice_sommet == -1)
    {
        fprintf(stderr, "ecrire_comp_face : une face a un sommet inconnu.\n");
        exit(EXIT_FAILURE);
    }

    v = extraire_normale(s);
    if (est_nul(v))
        fprintf(pfich, " %d", indice_sommet+1);
    else
    {
        indice_normale = recherche_tableau(normales, v);
        if (indice_normale == -1)
        {
            fprintf(stderr, "ecrire_comp_face : normale inconnue.\n");
            exit(EXIT_FAILURE);
        }

        t = extraire_texture(s);

        if (t == NULL || (indice_texture = recherche_tableau(textures, t)) == -1)
            fprintf(pfich, " %d//%d", indice_sommet+1, indice_normale+1);
        else
            fprintf(pfich, " %d/%d/%d", indice_sommet+1,
                        indice_texture+1,
                        indice_normale+1);
    }
}

void lire_sommet (const char *buf, tableau sommets, maillage m)
{
    float c1 = 0.0f, c2 = 0.0f, c3 = 0.0f;
    sommet s = NULL;
    point p = NULL;
    sscanf(buf, "%f %f %f", &c1, &c2, &c3);
    p = new_point(c1, c2, c3);
    s = new_sommet(p, vecteur_nul, texture_default());
    ajouter_element(sommets, s);
    ajouter_sommet(m, s);
}

void lire_faces (const char *buf, const tableau sommets, maillage m)
{
    char little_buf[100] = {0};
    char *tmp = NULL;
    tableau indices = new_tableau();
    int i1 = 0, i2 = 0, i3 = 0, i = 0;
    sommet s0 = NULL, s1 = NULL, s2 = NULL;

    while (sscanf(buf, "%s", little_buf) != EOF)
    {
        if (strstr(buf, "//"))
        {
            if (sscanf(little_buf, "%d//%d", &i1, &i3) == 0)
            {
                supprimer_tableau(indices);
                return;
            }
        }
        else if (sscanf(little_buf, "%d/%d", &i1, &i3) == 0)
        {
            if (sscanf(little_buf, "%d/%d/%d", &i1, &i2, &i3) == 0)
            {
                if (sscanf(little_buf, "%d", &i1) == 0)
                {
                    supprimer_tableau(indices);
                    return;
                }
            }
        }
        
        if (i1 <= taille_tableau(sommets) && i1 > 0)
            ajouter_element(indices, i1);
        
        i = 0;
        while (buf[i] == ' ')
            i++;
        buf=buf+i;
        tmp = strchr(buf, ' ');
        if (tmp != NULL)
            buf = tmp;
        else
            break;
    }

    if (taille_tableau(indices) <= 2)
    {
        supprimer_tableau(indices);
        return;
    }

    s0 = acceder_element(sommets, acceder_element(indices,0,int)-1, sommet);
    for (i = 2 ; i < taille_tableau(indices) ; i++)
    {
        s1 = acceder_element(sommets, acceder_element(indices,i-1,int)-1, sommet);
        s2 = acceder_element(sommets, acceder_element(indices,i,int)-1, sommet);

	    ajouter_face(m, new_face(s0, s1, s2, vecteur_nul, couleur_default()));
    }

    supprimer_tableau(indices);
}

maillage charger_maillage (const char *fichier)
{
    FILE *pfich = ouvrir(fichier, "r");
    char buf[TAILLE_BUF] = {0};

    char *search = strrchr(fichier, '/');
    if (search == NULL)
	strcpy(buf, fichier);
    else
	strcpy(buf, search+1);

    if (strlen(buf) > 4)
	buf[strlen(buf)-4] = '\0';
    
    maillage retour = new_maillage(buf);

    tableau sommets = new_tableau();

    while (fscanf(pfich, "%s", buf) != EOF)
    {
        if (buf[0] == 'v')
        {
            if (buf[1] == '\0')
            {
                fgets(buf, TAILLE_BUF, pfich);
                lire_sommet(buf, sommets, retour);
            }
        }
        else
            fgets(buf, TAILLE_BUF, pfich);
    }

    rewind(pfich);

    while (fscanf(pfich, "%s", buf) != EOF)
    {
        if (buf[0] == 'f')
        {
            if (buf[1] == '\0')
            {
                fgets(buf, TAILLE_BUF, pfich);
                lire_faces(buf, sommets, retour);
            }
        }
        else
            fgets(buf, TAILLE_BUF, pfich);
    }
    
    supprimer_tableau(sommets);

    fclose(pfich);

    return retour;
}


void enregistrer_maillage (const char *fichier, const maillage m)
{
    FILE *pfich = ouvrir(fichier, "w");
    int i = 0;
    const int nb_sommets = nombre_de_sommets(m);
    const int nb_faces = nombre_de_faces(m);
    tableau sommets = new_tableau();
    tableau normales = new_tableau();
    tableau textures = new_tableau();
    sommet s = NULL;
    point p = NULL;
    vecteur v = NULL;
    face f = NULL;
    texture t = NULL;

    fputs(" #\n # Written and fixed by FML (Fast Mesh Lab)\n # (C) FML "
          "2010\n #\n\n", pfich);
    if (nom_maillage(m) != NULL)
        fprintf(pfich, "g %s\n\n# vertices\n", nom_maillage(m));
    else
        fprintf(pfich, "# vertices\n");

    for (i = 0 ; i < nb_sommets ; i++)
    {
        s = obtenir_sommet(m, i);
        p = extraire_point(s);
        fprintf(pfich, "v %f %f %f\n", extraire_coordonnee_point(p, 0),
                extraire_coordonnee_point(p, 1),
                extraire_coordonnee_point(p, 2));
        ajouter_element(sommets, s);
    }

    fputs("\n# vertex normals\n", pfich);
    for (i = 0 ; i < nb_sommets ; i++)
    {
        s = obtenir_sommet(m, i);
        v = extraire_normale(s);
        if (!est_nul(v))
            fprintf(pfich, "vn %f %f %f\n", extraire_composante(v, 0),
                    extraire_composante(v, 1),
                    extraire_composante(v, 2));
        ajouter_element(normales, v);
    }

    fputs("\n# texture vertices\n", pfich);
    for (i = 0 ; i < nb_sommets ; i++)
    {
        s = obtenir_sommet(m, i);
        t = extraire_texture(s);
        if (t != NULL)
            fprintf(pfich, "vt %f %f\n", t->x, t->y);
        ajouter_element(textures, t);
    }

    fputs("\n# faces\n", pfich);
    for (i = 0 ; i < nb_faces ; i++)
    {
        putc((int)'f', pfich);
        f = obtenir_face(m, i);
        ecrire_comp_face(pfich, f, sommets, textures, normales, 0);
        ecrire_comp_face(pfich, f, sommets, textures, normales, 1);
        ecrire_comp_face(pfich, f, sommets, textures, normales, 2);
        putc((int)'\n', pfich);
    }

    supprimer_tableau(sommets);
    supprimer_tableau(textures);
    supprimer_tableau(normales);

    fclose(pfich);

    printf_ncurses ( "Enregistrement du maillage dans le fichier %s.\n" ,
                     fichier );
}

/*@}*/
