#include <stdio.h>
#include <stdlib.h>
#include "../../face.h"


int main()
{
    printf("** Cr�ation de la premi�re face **\n");
    
    // Creation du premier sommet de la face 
    point p0=new_point(1,2,3);
    vecteur n0=vecteur_nouveau(1,3,4);
    texture t1=new_texture(1,6);
    sommet s0=new_sommet(p0,n0,t1);
    // Creation du deuxieme sommet de la face 
    point p1=new_point(4,5,6);
    vecteur n1=vecteur_nouveau(1,2,3);
    sommet s1=new_sommet(p1,n1,t1);
    // Creation du troisieme sommet de la face 
    point p2=new_point(7,8,9);
    vecteur n2=vecteur_nouveau(7,6,4);
    sommet s2=new_sommet(p2,n2,t1);
    // Creation de la face 
    vecteur n00=vecteur_nouveau(1,2,0);
    face f0=new_face(s0,s1,s2,n00,couleur_default());
    afficher_face(f0);
    printf("\n");

    printf("** Cr�ation de la seconde face **\n");

    point p3=new_point(3,2,1);
    vecteur n3=vecteur_nouveau(9,9,9);
    texture t2=new_texture(3,0);
    sommet s3=new_sommet(p3,n3,t2);
    // Creation du deuxieme sommet de la face 
    point p4=new_point(5,9,0);
    vecteur n4=vecteur_nouveau(1,4,1);
    sommet s4=new_sommet(p4,n4,t2);
    // Creation du troisieme sommet de la face 
    point p5=new_point(9,8,7);
    vecteur n5=vecteur_nouveau(0,0,1);
    sommet s5=new_sommet(p5,n5,t2);
    // Creation de la face 
    vecteur n01=vecteur_nouveau(7,4,3);
    face f1=new_face(s3,s4,s5,n01,couleur_default());
    afficher_face(f1);

    printf("** Cr�ation de la troisi�me face **\n");
    face f2=new_face(s1,s0,s2,n00,couleur_default());
    afficher_face(f1);

    printf("** Test de bonne formation de la face **\n");
    printf("la face est bien formée : %d\n",bienforme_face(f0));

    printf("** Calcul du barycentre de la face **\n");
    printf("barycentre(f0) : ");
    afficher_point(barycentre_face(f0));

    printf("** Extraction du deuxi�me sommet de la face **\n");
    printf("extraire_sommet(f0,1) : ");
    afficher_sommet(extraire_sommet(f0,1));

    printf("** Extraction la normale de la face**\n");
    printf("extraire_normale(f0) : ");
    afficher_vecteur(extraire_normale_f(f0));

    printf("** Extraction la couleur de la face  **\n");
    printf("extraire_couleur(f0) : ");
    afficher_couleur(extraire_couleur(f0));

    printf("** Modification du deuxi�me sommet de la face  **\n");
    point p7=new_point(7,4,0);
    vecteur v7=vecteur_nouveau(8,6,9);
    sommet s7=new_sommet(p7,v7,t2);
    printf("nouveau deuxi�me sommet : ");
    afficher_sommet(s7);
    face
f7=modif_face_sommets(f0,extraire_sommet(f0,0),s7,extraire_sommet(f0,2));
    printf("modifier_face_sommets(f0,extraire_sommet(f0,0),s1,extraire_sommet(f0,2)) : ");
    afficher_face(f7);
    
    printf("** Modification de la normale de la face  **\n");
    vecteur v2=vecteur_nouveau(4,5,6);
    printf("nouvelle normale : ");
    afficher_vecteur(v2);
    face f8=modif_face_normale(f0,v2);
    printf("extraire_normale_f(modif_face_normale(f0,v2)) : ");
    afficher_vecteur(extraire_normale_f(f8));
    
    printf("** Calcul de la normale � une face  **\n");
    vecteur v6 = calcul_norm_face(f0);
    printf("calcul_norm_face(f0) : ");
    afficher_vecteur(v6);

    printf("** Comparaison de deux faces selon les points **\n");
    printf("comp_face_points(f0,f0) = %d\n",comp_face_points(f0,f0));
    printf("comp_face_points(f0,f2) = %d\n",comp_face_points(f0,f2));
    printf("comp_face_points(f0,f1) = %d\n",comp_face_points(f0,f1));
    printf("\n");

    printf("** Comparaison de deux faces selon les normales **\n");
    printf("comp_face_sommets(f0,f0) = %d\n",comp_face_sommets(f0,f0));
    printf("comp_face_sommets(f0,f2) = %d\n",comp_face_sommets(f0,f2));
    printf("comp_face_sommets(f0,f1) = %d\n",comp_face_sommets(f0,f1));
    printf("\n");
    
    printf("** Comparaison de deux faces  **\n");
    printf("comp_face(f0,f0) = %d\n",comp_face(f0,f0));
    printf("comp_face(f0,f1) = %d\n",comp_face(f0,f1));
    printf("\n");

    printf("** Calcul de la courbure entre deux faces  **\n");
    printf("courbure_deux_faces(f0,f0) = %f\n",courbure_deux_faces(f0,f0));
    printf("courbure_deux_faces(f0,f1) = %f\n",courbure_deux_faces(f0,f1));
    printf("\n");

    return 0;
}
