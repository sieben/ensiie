
#include <stdio.h>

#include "../../detection_correction.h"
#include "../../point.h"
#include "../../maillages.h"
#include "../../fichiers_obj.h"
#include "../../qualite.h"

void affichage_face_deg()
{
	tableau tabmaillageVenus_degfaces = charger_maillages("obj/MeshErrors/venus2_degfaces_20.obj");
	maillage m = acceder_element(tabmaillageVenus_degfaces,0,maillage);
	const int nbf=nombre_de_faces(m);
	int k;
	tableau indices_faces_normalestab=new_tableau();


	for (k=0;k<nbf;++k) //Parcours des faces
	{
		if( facteur_quali2(obtenir_face(m,k))<1 ) //La face est normale
		{
			ajouter_element(indices_faces_normalestab,k);
		}
	}

	suppression ( m, indices_faces_normalestab, 'f' );

	enregistrer_maillages ("affichage_face_deg.obj", tabmaillageVenus_degfaces);


}

void afficher_sommetH(sommet s)
{
    point ps=extraire_point(s);
    printf("%f %f %f\n",extraire_coordonnee_point(ps,0),
    extraire_coordonnee_point(ps,1),extraire_coordonnee_point(ps,2));
}

void afficher_faceH(maillage m,face f4)
{

    point pf41=extraire_point(extraire_sommet(f4,0));
    printf("point 0, d'indice %d :%f %f %f\n",indice_sommet(m,extraire_sommet(f4,0)),extraire_coordonnee_point(pf41,0),
    extraire_coordonnee_point(pf41,1),extraire_coordonnee_point(pf41,2));
    point pf42=extraire_point(extraire_sommet(f4,1));
    printf("point 1, d'indice %d :%f %f %f\n",indice_sommet(m,extraire_sommet(f4,1)),extraire_coordonnee_point(pf42,0),
    extraire_coordonnee_point(pf42,1),extraire_coordonnee_point(pf42,2));
    point pf43=extraire_point(extraire_sommet(f4,2));
    printf("point 2, d'indice %d :%f %f %f\n",indice_sommet(m,extraire_sommet(f4,2)),extraire_coordonnee_point(pf43,0),
    extraire_coordonnee_point(pf43,1),extraire_coordonnee_point(pf43,2));
    printf("\n\n");

}



void afficher_maillage(maillage m)
{
 int nbs = nombre_de_sommets(m);
 int nbf = nombre_de_faces(m);
 int k = 0;


 for(k = 0 ; k<nbs; k++)
 {
  printf("sommet %d : \n\n",k);
  afficher_sommetH(obtenir_sommet(m,k));
 }

  for(k = 0 ; k<nbf; k++)
 {
  printf("face %d : \n\n",k);
  afficher_faceH(m,obtenir_face(m,k));
 }

}


int main ( int argc, char * argv[] )
{
// sommets_dupliques

    printf("\n\nSommets_dupliques:\n\n");
    tableau tabmaillageVenus_dupvertices = charger_maillages("obj/MeshErrors/venus2_dupvertices_20.obj");
    maillage maillageVenus_dupvertices = acceder_element(tabmaillageVenus_dupvertices,0,maillage);

    printf("MAILLAGE AVANT CORRECTION :\n");

    point som_dup_a=new_point(0,0,0);
    point som_dup_e=new_point(2,3,4); // point utilisé dans aucune face
    point som_dup_b=new_point(0,1,0);
    point som_dup_c=new_point(1,1,0);
    point som_dup_d=new_point(1,0,0);

    sommet som_dup_sa=new_sommet(som_dup_a,NULL,NULL);
    sommet som_dup_sb=new_sommet(som_dup_b,NULL,NULL);
    sommet som_dup_sb2=new_sommet(som_dup_b,NULL,NULL);
    sommet som_dup_sc=new_sommet(som_dup_c,NULL,NULL);
    sommet som_dup_sd=new_sommet(som_dup_d,NULL,NULL);
    sommet som_dup_sd2=new_sommet(som_dup_d,NULL,NULL);
    sommet som_dup_se=new_sommet(som_dup_e,NULL,NULL);
    sommet som_dup_se2=new_sommet(som_dup_e,NULL,NULL);

    face som_dup_abd=new_face(som_dup_sa, som_dup_sb, som_dup_sd,NULL,NULL);
    face som_dup_bcd=new_face(som_dup_sb, som_dup_sc, som_dup_sd2,NULL,NULL);

    maillage som_dup_m=new_maillage ( "som_dup" );
    ajouter_sommet ( som_dup_m , som_dup_sa );
    ajouter_sommet ( som_dup_m , som_dup_sb );
    ajouter_sommet ( som_dup_m , som_dup_sc );
    ajouter_sommet ( som_dup_m , som_dup_sd );
    ajouter_sommet ( som_dup_m , som_dup_sd2 );
    ajouter_sommet ( som_dup_m , som_dup_se );
    ajouter_sommet ( som_dup_m , som_dup_se2 );
    ajouter_sommet ( som_dup_m , som_dup_sb2 );
    ajouter_face   ( som_dup_m , som_dup_abd );
    ajouter_face   ( som_dup_m , som_dup_bcd );

    tableau som_dup_maill=new_tableau();
    ajouter_element(som_dup_maill, som_dup_m);
    afficher_maillage(som_dup_m);
    printf("MAILLAGE APRES CORRECTION :\n");
    corr_somm_dups(som_dup_m);
    afficher_maillage(som_dup_m);
    enregistrer_maillages ("test_som_dup.obj", som_dup_maill);

    printf("MAILLAGE VENUS AVANT CORRECTION :\n");
    enregistrer_maillages ("obj/detection_correction/test_som_dupvenusavant.obj", tabmaillageVenus_dupvertices);
    afficher_maillage(maillageVenus_dupvertices);
    printf("MAILLAGE VENUS APRES CORRECTION :\n");
    corr_somm_dups(maillageVenus_dupvertices);
    printf("Correction appliquée\n");
    afficher_maillage(maillageVenus_dupvertices);
    enregistrer_maillages ("obj/detection_correction/test_som_dupvenusapres.obj", tabmaillageVenus_dupvertices);

    free_maillage(som_dup_m);
    supprimer_tableau(som_dup_maill);

    free_maillage(maillageVenus_dupvertices);
    supprimer_tableau(tabmaillageVenus_dupvertices);



//sommets_non_references

   printf("\n\nSommets_non_references:\n\n");

    point som_ref_a=new_point(0,0,0);
    point som_ref_b=new_point(0,1,0);
    point som_ref_c=new_point(1,1,0);
    point som_ref_d=new_point(1,0,0);
    point som_ref_e=new_point(12,17,32);

    sommet som_ref_sa=new_sommet(som_ref_a,NULL,NULL);
    sommet som_ref_sb=new_sommet(som_ref_b,NULL,NULL);
    sommet som_ref_sc=new_sommet(som_ref_c,NULL,NULL);
    sommet som_ref_sd=new_sommet(som_ref_d,NULL,NULL);
    sommet som_ref_se=new_sommet(som_ref_e,NULL,NULL); //sommet non reference

    face som_ref_abd=new_face(som_ref_sa, som_ref_sb, som_ref_sd,NULL,NULL);
    face som_ref_bcd=new_face(som_ref_sb, som_ref_sc, som_ref_sd,NULL,NULL);

    maillage som_ref_m=new_maillage("som_ref_test");

    ajouter_sommet(som_ref_m, som_ref_sa);
    ajouter_sommet(som_ref_m, som_ref_sb);
    ajouter_sommet(som_ref_m, som_ref_sc);
    ajouter_sommet(som_ref_m, som_ref_sd);
    ajouter_sommet(som_ref_m, som_ref_se);

    ajouter_face(som_ref_m, som_ref_abd);
    ajouter_face(som_ref_m, som_ref_bcd);

    corr_somm_nonref(som_ref_m);

    tableau som_ref_maill=new_tableau();
    ajouter_element(som_ref_maill, som_ref_m);
    enregistrer_maillages ("test_som_ref.obj", som_ref_maill);

    free_maillage(som_ref_m);
    supprimer_tableau(som_ref_maill);



// Faces dupliquees

    printf("\n\nFaces dupliquées\n\n");

    tableau tabmaillageVenus_dupfaces = charger_maillages("obj/MeshErrors/venus2_dupfaces_20.obj");
    maillage maillageVenus_dupfaces = acceder_element(tabmaillageVenus_dupfaces,0,maillage);


    printf("MAILLAGE VENUS AVANT CORRECTION :\n");
    enregistrer_maillages ("obj/detection_correction/test_faces_dupvenusavant.obj", tabmaillageVenus_dupfaces);
    afficher_maillage(maillageVenus_dupfaces);
    printf("MAILLAGE VENUS APRES CORRECTION :\n");
    corr_faces_dups(maillageVenus_dupfaces);
    printf("Correction appliquée\n");
    afficher_maillage(maillageVenus_dupfaces);
    enregistrer_maillages ("obj/detection_correction/test_faces_dupvenusapres.obj", tabmaillageVenus_dupfaces);

    free_maillage(maillageVenus_dupfaces);
    supprimer_tableau(tabmaillageVenus_dupfaces);



// Faces invalides

    printf("\n\nFaces invalides\n\n");

    point faces_inv_a=new_point(0,0,0);
    point faces_inv_e=new_point(2,3,4);
    point faces_inv_b=new_point(0,1,0);
    point faces_inv_c=new_point(1,1,0);
    point faces_inv_d=new_point(1,0,0);

    sommet faces_inv_sa=new_sommet(faces_inv_a,NULL,NULL);
    sommet faces_inv_sb=new_sommet(faces_inv_b,NULL,NULL);
    sommet faces_inv_sb2=new_sommet(faces_inv_b,NULL,NULL);
    sommet faces_inv_sc=new_sommet(faces_inv_c,NULL,NULL);
    sommet faces_inv_sd=new_sommet(faces_inv_d,NULL,NULL);
    sommet faces_inv_sd2=new_sommet(faces_inv_d,NULL,NULL);
    sommet faces_inv_se=new_sommet(faces_inv_e,NULL,NULL);
    sommet faces_inv_se2=new_sommet(faces_inv_e,NULL,NULL);

    face faces_inv_abd=new_face(faces_inv_sa, faces_inv_sb, faces_inv_sd,NULL,NULL);
    face faces_inv_bcd=new_face(faces_inv_sb, faces_inv_sc, faces_inv_sd2,NULL,NULL);
    face faces_inv_abd3=new_face(faces_inv_sa, faces_inv_sb, faces_inv_sd,NULL,NULL);
    face faces_inv_abd4=new_face(faces_inv_sa, faces_inv_sb, faces_inv_sd,NULL,NULL);
    face faces_inv_abd5=new_face(faces_inv_sa, faces_inv_sb, faces_inv_sd,NULL,NULL);

    maillage faces_inv_m=new_maillage ( "faces_inv" );
    ajouter_sommet ( faces_inv_m , faces_inv_sa );
    ajouter_sommet ( faces_inv_m , faces_inv_sb );
    ajouter_sommet ( faces_inv_m , faces_inv_sc );
    ajouter_sommet ( faces_inv_m , faces_inv_sd );
    //ajouter_sommet ( faces_inv_m , faces_inv_sd2 );
    ajouter_sommet ( faces_inv_m , faces_inv_se );
    ajouter_sommet ( faces_inv_m , faces_inv_se2 );
    ajouter_sommet ( faces_inv_m , faces_inv_sb2 );
    ajouter_face   ( faces_inv_m , faces_inv_abd );
    ajouter_face   ( faces_inv_m , faces_inv_bcd );
    ajouter_face   ( faces_inv_m , faces_inv_abd3 );
    ajouter_face   ( faces_inv_m , faces_inv_abd4 );
    ajouter_face   ( faces_inv_m , faces_inv_abd5 );

    tableau faces_inv_maill=new_tableau();
    ajouter_element(faces_inv_maill, faces_inv_m);
    afficher_maillage(faces_inv_m);

    printf("MAILLAGE APRES CORRECTION :\n");
    corr_faces_invalides(faces_inv_m);
    afficher_maillage(faces_inv_m);
    enregistrer_maillages ("test_faces_inv.obj", faces_inv_maill);


// Sommets non manifold

    printf("\n\nSommets non manifold\n\n");
    int nbnonman = 0;

    tableau tabmaillageVenus_nonman = charger_maillages("obj/MeshErrors/venus2_NonManifoldVertex_1.obj");
    maillage maillageVenus_nonman = acceder_element(tabmaillageVenus_nonman,0,maillage);

  //printf("MAILLAGE VENUS AVANT CORRECTION :\n");
  //enregistrer_maillages ("obj/detection_correction/test_sommnonmanvenusavant.obj", tabmaillageVenus_nonman);
  //afficher_maillage(maillageVenus_nonman);
    printf("MAILLAGE VENUS APRES CORRECTION :\n");
    printf("%d",recherche_somm_nonmanifold(maillageVenus_nonman));
    printf("Correction appliquée\n");
    afficher_maillage(maillageVenus_nonman);
    enregistrer_maillages ("obj/detection_correction/test_sommnonmanvenusapres.obj", tabmaillageVenus_nonman);

    free_maillage(maillageVenus_nonman);
    supprimer_tableau(tabmaillageVenus_nonman);

    tableau tabmaillageVenus_invfaces = charger_maillages("obj/MeshErrors/venus2_dupfaces_20.obj");
    maillage maillageVenus_invfaces = acceder_element(tabmaillageVenus_invfaces,0,maillage);

    printf("MAILLAGE VENUS AVANT CORRECTION :\n");
    enregistrer_maillages ("obj/detection_correction/test_faces_invvenusavant.obj", tabmaillageVenus_invfaces);
    afficher_maillage(maillageVenus_invfaces);
    printf("MAILLAGE VENUS APRES CORRECTION :\n");
    corr_faces_invalides(maillageVenus_invfaces);
    printf("Correction appliquée\n");
    afficher_maillage(maillageVenus_invfaces);
    enregistrer_maillages ("obj/detection_correction/test_faces_invvenusapres.obj", tabmaillageVenus_invfaces);

    free_maillage(maillageVenus_invfaces);
    supprimer_tableau(tabmaillageVenus_invfaces); 


//faces degenerees

    printf("\n\nFaces degenerees:\n\n");

    //affichage_face_deg();
	tableau tabmaillageVenus_degfaces = charger_maillages("obj/MeshErrors/venus2_degfaces_20.obj");
	maillage maillageVenus_degfaces = acceder_element(tabmaillageVenus_degfaces,0,maillage);

	corr_faces_degenerees( maillageVenus_degfaces );
	enregistrer_maillages ("affichage_face_deg.obj", tabmaillageVenus_degfaces);

	
	const int nbf=nombre_de_faces(maillageVenus_degfaces);
	int k;
	tableau indices_faces_normalestab=new_tableau();
	

	for (k=0;k<nbf;++k) //Parcours des faces
	{
		if( facteur_quali2(obtenir_face(maillageVenus_degfaces,k))<=0.9 ) //La face est normale
		{
			ajouter_element(indices_faces_normalestab,k);
		}

	}

	suppression ( maillageVenus_degfaces, indices_faces_normalestab, 'f' );

	enregistrer_maillages ("affichage_face_mauvaises.obj", tabmaillageVenus_degfaces);




/* Il faut reserver une collection de points dont les combinaisons forment
	   tous les cas possibles.
    Nous allons choisir des points dont les coordonnées seront des elements
	   de l'ensemble {-1.0,0.0,1.0} afin de faciliter la representation dans l'espace.
 */


    point c1,c2,c3,c4,c5,c6,c7,c8; // Les points qui sont des coins
	point o; // Point origine {0,0,0}
	point m1,m2,m3,m4,m5,m6; // Les points "milieu", centre des faces
	point a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12;

	// A REMPLIR AVEC UN RUBIKS CUBE 

	o = new_point(0.0,0.0,0.0);

	c1 = new_point(1.0,1.0,1.0);
    c2 = new_point(-1.0,-1.0,-1.0);
	c3 = new_point(1.0,-1.0,-1.0);
	c4 = new_point(1.0,-1.0,1.0);
	c5 = new_point(1.0,1.0,-1.0);
	c6 = new_point(-1.0,1.0,1.0);
	c7 = new_point(-1.0,1.0,-1.0);
	c8 = new_point(-1.0,-1.0,1.0);

	m1 = new_point (0.0,0.0, 1.0 );
  m2 = new_point (0.0,0.0, -1.0 );
  m3 = new_point (0.0, 1.0 ,0.0);
  m4 = new_point (0.0, -1.0 ,0.0);
  m5 = new_point ( 1.0 ,0.0,0.0);
  m6 = new_point ( -1.0 ,0.0,0.0);
//
  a1 = new_point ( 1.0 , 1.0 ,0.0);
  a2 = new_point ( -1.0 , -1.0 ,0.0);
  a3 = new_point ( 1.0 , -1.0 ,0.0);
  a4 = new_point ( -1.0 , 1.0 ,0.0);
//
  a5 = new_point ( 1.0,0.0,1.0);
  a6 = new_point (0.0, 1.0 , 1.0);
  a7 = new_point (-1.0,0.0, 1.0 );
  a8 = new_point (0.0, -1.0 , 1.0);
//
  a9 = new_point  (1.0,0.0,-1.0);
  a10 = new_point (0.0,1.0,-1.0);
  a11 = new_point (-1.0,0.0,-1.0);
  a12 = new_point (0.0,-1.0,-1.0);
//
  int res;
  maillage m;
//
  // Tests des fonctions de detections et de corrections d'erreur 
//
  // Sommets non manifolds (2D) :
//
  sommet sm1,sc1,sc6,sc4,sc8;
  face f1, f2;
//
  sm1 = new_sommet(m1,NULL,NULL);
  sc1 = new_sommet(c1,NULL,NULL);
  sc6 = new_sommet(c6,NULL,NULL);
  sc4 = new_sommet(c4,NULL,NULL);
  sc8 = new_sommet(c8,NULL,NULL);
//
  f1 = new_face(sm1,sc1,sc6, NULL , couleur_default() );
  f2 = new_face(sm1,sc4,sc8, NULL , couleur_default() );
//
  m = new_maillage("test");
//
  ajouter_face(m,f1);
  ajouter_face(m,f2);
//
  recherche_somm_nonmanifold(m);
//
  // Sommets non manifolds (3D) :
//
  sommet sc7,sc5;
  face f3;
  sc7 = new_sommet(c7,NULL,NULL);
  sc5 = new_sommet(c5,NULL,NULL);
//
  f3 = new_face( sm1 , sc7 , sc5 , NULL , couleur_default() );
//
  m= new_maillage("test");
//
  ajouter_face(m,f1);
  ajouter_face(m,f3);
//
  recherche_somm_nonmanifold(m);
//
  // Tests des fonctions d'intersection de triangles 
//
//    printf("\n\nIntersection de triangles\n\n");
//
  // Cas nominaux 
//
//
  // Triangles 3D 
//
  // Triangles qui se coupent 
//
  //tri_tri_overlap_test_3D(c6,a12,c1,c8,a10,c4);
  res =	intersection(c6,a12,c1,c8,a10,c4);
  printf("Triangles qui se coupent (3D). Résultat attendu : 1, Résultat obtenu : %d \n",res);
//
  // Triangles disjoints 
//
  //check_min_max(c6,c4,c1,c7,c3,c2);
  res = intersection(c6,c4,c1,c7,c3,c2);
  printf("Triangles disjoints (3D). Résultat attendu : 0, Résultat obtenu : %d \n",res);
//
  // Triangles 2D 
//
  // Triangles qui se coupent 
//
  res =  intersection(a1,m4,m6,a2,m3,m5);
  printf("Triangles qui se coupent (2D). Résultat attendu : 1, Résultat obtenu : %d \n",res);
//
  // Triangles disjoints 
//
  res =    intersection(a1,m5,m3,m6,m4,a2);
  printf("Triangles disjoints (2D). Résultat attendu : 0, Résultat obtenu : %d \n",res);
//
  // Tests aux limites 
//
  // Triangles en 3D 
//
  // Triangles egaux 
//
  res =    intersection(c6,c4,m2,c6,c4,m2);
  printf("Triangles égaux (3D). Résultat attendu : 1, Résultat obtenu : %d\n",res);
  //intersection(c6,c4,m2,c6,c4,m2);
//
  // Triangles qui ont un sommet commun 
//
  res =    intersection(c1,c8,c4,c2,c5,c4);
  printf("Triangles qui ont un sommet commun (3D). Résultat attendu : 1, Résultat obtenu : %d \n",res);
  //tri_tri_overlap_test_3D(c1,c8,c4,c2,c5,c4);
//
  // Triangles qui ont une arete commune 
//
  res =    intersection(c2,c6,c4,c5,c6,c4);
  printf("Triangles qui ont une arête commune (3D). Résultat attendu : 1, Résultat obtenu : %d \n",res);
  //tri_tri_overlap_test_3D(c2,c6,c4,c5,c6,c4);
//
  // Triangles en 2D 
//
  // ATTENTION LES POINTS SONT DANS LE PLAN (X,Y) ! 
//
  // Triangles egaux 
//
  res =    intersection(m3,a2,a3,m3,a2,a3);
  printf("Triangles égaux (2D). Résultat attendu : 1, Résultat obtenu : %d \n",res);
  //ccw_tri_tri_intersection_2D(m3,a2,a3,m3,a2,a3);
//
  // Triangles qui ont un sommet commun 
//
  res =    intersection(o,m5,m3,o,m4,m6);
  printf("Triangles qui ont un sommet commun (2D). Résultat attendu : 1, Résultat obtenu : %d \n",res);
  //ccw_tri_tri_intersection_2D(o,m5,m3,o,m4,m6);
//
  // Triangles qui ont une arete commune 
//
  //ccw_tri_tri_intersection_2D(a4,a2,a3,a1,a3,a4);
  res =    intersection(a4,a2,a3,a1,a3,a4);
  printf("Triangles qui ont une arête commune (2D). Résultat attendu : 1, Résultat obtenu : %d \n",res);


/*    int i = 0;
    maillage m = NULL;
    tableau maills = charger_maillages("obj/MeshErrors/venus2_NonManifoldVertex_1.obj");

    for (i = 0 ; i < taille_tableau(maills) ; i++)
    {
        m = acceder_element(maills, i, maillage);
        printf("Maillage %s : %d sommet(s) non manifold(s).\n",
                nom_maillage(m), recherche_somm_nonmanifold(m));
        free_maillage(m);
    }

    supprimer_tableau(maills);*/

	return 0;
}
