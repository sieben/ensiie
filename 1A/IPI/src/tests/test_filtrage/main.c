
#include <stdio.h>
#include <stdlib.h>

#include "../../maillages.h"
#include "../../fichiers_obj.h"
#include "../../filtrage.h"

int main ( void )
{
    maillage m = NULL;
    maillage nm = NULL;

    // Filtre uniforme
    m = charger_maillage ( "obj/face/1.obj" );
    nm = filtre_uniforme ( m );
    enregistrer_maillage ( "uniforme_1.obj" , nm );
    free_maillage ( m );
    free_maillage ( nm );

    // Filtre uniforme du maillage bol
    m = charger_maillage ( "obj/filtrage/bol.obj" );
    nm = filtre_uniforme ( m );
    enregistrer_maillage ( "uniforme_bol.obj" , nm );
    free_maillage ( m );
    free_maillage ( nm );

    // Filtre uniforme du maillage paroi_fine
    m = charger_maillage ( "obj/filtrage/paroi_fine.obj" );
    nm = filtre_uniforme ( m );
    enregistrer_maillage ( "uniforme_paroi_fine.obj" , nm );
    free_maillage ( m );
    free_maillage ( nm );

    // Filtre normal du maillage bol
    m = charger_maillage ( "obj/filtrage/bol.obj" );
    nm = filtre_normal ( m );
    enregistrer_maillage ( "normal_bol.obj" , nm );
    free_maillage ( m );
    free_maillage ( nm );

    // Filtre normal du maillage paroi_fine
    m = charger_maillage ( "obj/filtrage/paroi_fine.obj" );
    nm = filtre_normal ( m );
    enregistrer_maillage ( "normal_paroi_fine.obj" , nm );
    free_maillage ( m );
    free_maillage ( nm );

    return 0;
}
