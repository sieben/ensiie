#include <stdio.h>
#include <stdlib.h>

#include "../../maillages.h"
#include "../../point.h"
#include "../../sommet.h"
#include "../../face.h"


int main ( void )
{

    int k=0;
    printf("****test de ajouter_sommet****\n");
    //ajouter_sommet
    maillage m = new_maillage ( "test" );
    //sommet valide
    printf("ajout d'un sommet valide\n");
    point p = new_point(2,0.4,3.25);
    sommet s = new_sommet(p,NULL,NULL);
    ajouter_sommet(m,s);
    //sommet vide
    printf("ajout d'un sommet vide\n");
    ajouter_sommet(m,NULL);
    printf("taille tableau sommets : %d\n",nombre_de_sommets(m));

    printf("\n****test de supprimer_sommet****\n");
    //supprimer_sommet
    //sommet valide
    printf("suppression d'un sommet\n");
    supprimer_sommet(m,0);
    printf("taille tableau sommets : %d\n",nombre_de_sommets(m));

    printf("\n****test de ajouter_face****\n");
    //ajouter_face
    point p1=new_point(2,0.4,3.25);
    point p2=new_point(22,0.24,25);
    point p3=new_point(0.5,1,3.12);
    sommet s1=new_sommet(p1,NULL,NULL);
    sommet s2=new_sommet(p2,NULL,NULL);
    sommet s3=new_sommet(p3,NULL,NULL);
    face f=new_face(s1,s2,s3,NULL,NULL);
    //face valide
    printf("ajout d'une face valide\n");
    ajouter_face(m,f);
    //face vide
    printf("ajout d'une face vide\n");
    ajouter_face(m,NULL);
    printf("taille tableau faces : %d\n",nombre_de_faces(m));
    //supprimer_face
    printf("suppression d'une face\n");
    supprimer_face ( m , indice_face (m,f) );
    printf("taille tableau faces : %d\n",nombre_de_faces(m));

     printf("\n****test de nombre_de_sommets****\n");
    //nombre_de_sommets
    ajouter_sommet(m,s1);
    ajouter_sommet(m,s2);
    ajouter_sommet(m,s3);
    int ns=nombre_de_sommets(m);
    printf("m contient %d sommet(s), (3 normalement).\n",ns);

     printf("\n****test de obtenir_sommet****\n");
    //obtenir_sommet
    sommet s4=obtenir_sommet(m,1);
    point p4=extraire_point(s4);
    printf("%f %f %f\n",extraire_coordonnee_point(p4,0),
    extraire_coordonnee_point(p4,1),extraire_coordonnee_point(p4,2));
    printf("les coordonnees doivent etre 22 0.24 25\n");
    //doit afficher 22 0.24 25

     printf("\n****test de modifier_sommet****\n");
    //modifier_sommet
    point p5=new_point(0.2,1,4.11);
    sommet s5=new_sommet(p5,NULL,NULL);
    printf("remplacement du deuxième sommet par (0.2,1,4.11)\n");
    modifier_sommet(m,s5,1); // remplace s2 par s5
    s4=obtenir_sommet(m,1);
    p4=extraire_point(s4);
    printf("%f %f %f ((0.2,1,4.11) normalement)\n",extraire_coordonnee_point(p4,0),
    extraire_coordonnee_point(p4,1),extraire_coordonnee_point(p4,2));
    //doit afficher 0.2 1 4.11

     printf("\n****test de nombre_de_faces****\n");
    //nombre_de_faces
    //f=new_face(s1,s2,s3,NULL,NULL,NULL);
    ajouter_face(m,f);
    int nf=nombre_de_faces(m);
    printf("m contient %d face(s)\n",nf);
    printf("1 face normalement\n");

    printf("\n****test de obtenir_face****\n");
    //obtenir_face
    face f4=obtenir_face(m,0);
    point pf41=extraire_point(extraire_sommet(f4,0));
    printf("point 0 :%f %f %f\n",extraire_coordonnee_point(pf41,0),
    extraire_coordonnee_point(pf41,1),extraire_coordonnee_point(pf41,2));
    point pf42=extraire_point(extraire_sommet(f4,1));
    printf("point 1 :%f %f %f\n",extraire_coordonnee_point(pf42,0),
    extraire_coordonnee_point(pf42,1),extraire_coordonnee_point(pf42,2));
    point pf43=extraire_point(extraire_sommet(f4,2));
    printf("point 2 :%f %f %f\n",extraire_coordonnee_point(pf43,0),
    extraire_coordonnee_point(pf43,1),extraire_coordonnee_point(pf43,2));
    //doit afficher les coordonnees de p1, p2, p3

// boucles qui font planter (double free)
    ajouter_face(m,f);
    ajouter_face(m,f);
    ajouter_face(m,f);
    ajouter_face(m,f);

    printf("%d\n",nombre_de_faces(m));

    for(k=nombre_de_faces(m)-1;k>=0;k--)
      {
	supprimer_face(m,k);
      }


    printf("%d\n",nombre_de_faces(m));

  printf("%d\n",nombre_de_sommets(m));

    for(k=nombre_de_sommets(m)-1;k>=0;k--)
      {
	supprimer_sommet(m,k);
      }
    ajouter_face(m,f);


    printf("\n****test de modifier_face****\n");
    p1=new_point(2,0.4,3.25);
    p2=new_point(22,0.24,25);
    p3=new_point(0.5,1,3.12);

    sommet s6=new_sommet(p1,NULL,NULL);
    sommet s7=new_sommet(p2,NULL,NULL);
    sommet s8=new_sommet(p3,NULL,NULL);

    //modifier_face
    face f5=new_face(s6,s7,s8,NULL,NULL);
    modifier_face(m,f5,0); // remplace la face 0 (f4) par f5
    point pf51=extraire_point(extraire_sommet(f5,0));
    printf("point 1 :%f %f %f\n",extraire_coordonnee_point(pf51,0),
    extraire_coordonnee_point(pf51,1),extraire_coordonnee_point(pf51,2));
    point pf52=extraire_point(extraire_sommet(f5,1));
    printf("point 2 :%f %f %f\n",extraire_coordonnee_point(pf52,0),
    extraire_coordonnee_point(pf52,1),extraire_coordonnee_point(pf52,2));
    point pf53=extraire_point(extraire_sommet(f5,2));
    printf("point 3 :%f %f %f\n",extraire_coordonnee_point(pf53,0),
    extraire_coordonnee_point(pf53,1),extraire_coordonnee_point(pf53,2));
    //doit afficher les coordonnees de p3, p2, p1

    for(k=nombre_de_faces(m)-1;k>=0;k--)
      {
	supprimer_face(m,k);
      }
    for(k=nombre_de_sommets(m)-1;k>=0;k--)
      {
	supprimer_sommet(m,k);
      }

    //supprimer_face(m,0);
    //printf("%d sommets\n",nombre_de_sommets(m));


    maillage m1=new_maillage("testfonctionsavancees");

    p1=new_point(0,0,0);
    p2=new_point(1,0,0);
    p3=new_point(0,-1,0);
    p4=new_point(-1,1,0);
    p5=new_point(0,1,0);
    point p6=new_point(1,1,0);

    //liberer_mem(s6);
    s6=new_sommet(p1,NULL,NULL);
    s7=new_sommet(p2,NULL,NULL);
    s8=new_sommet(p3,NULL,NULL);
    sommet s9=new_sommet(p4,NULL,NULL);
    sommet s10=new_sommet(p5,NULL,NULL);
    sommet s11=new_sommet(p6,NULL,NULL);

    ajouter_sommet(m1,s6);
    ajouter_sommet(m1,s7);
    ajouter_sommet(m1,s8);
    ajouter_sommet(m1,s9);
    ajouter_sommet(m1,s10);
    ajouter_sommet(m1,s11);

    f5=new_face(s6,s7,s8,NULL,NULL);
    face f6=new_face(s6,s7,s10,NULL,NULL);
    face f7=new_face(s7,s10,s11,NULL,NULL);
    face f8=new_face(s6,s9,s10,NULL,NULL);

    ajouter_face(m1,f5);
    ajouter_face(m1,f6);
    ajouter_face(m1,f7);
    ajouter_face(m1,f8);

    //faces_voisines

    printf("sommets : %d ",nombre_de_sommets(m1));
    printf("faces : %d\n",nombre_de_faces(m1));

    printf("\n***test de faces_voisines***\n");
    tableau facesvoisines=faces_voisines(m1,1); // voisines de f6
    k=0;
    printf("faces voisines de :\n");
    afficher_face(obtenir_face(m1,1));
    printf("\n");
    for(k=0;k<taille_tableau(facesvoisines);k++)
      {
	afficher_face(acceder_element(facesvoisines,k,face));
      }

  //faces_incidentes

    printf("***test de faces_incidentes***\n");
    tableau facesincidentes=faces_incidentes(m1,0);
     for(k=0;k<taille_tableau(facesincidentes);k++)
      {
	afficher_face(acceder_element(facesincidentes,k,face));
      }

    //sommets_voisins
    printf("***test de sommets_voisin***\n");
    printf("voisins de : ");
    afficher_sommet(obtenir_sommet(m1,0));
    tableau *voisins=sommets_voisins(m1,0,1);
    for(k=0;k<taille_tableau(voisins[1]);k++)
    {
        afficher_sommet(obtenir_sommet(m1,(int)acceder_element(voisins[1],k,long)));
    }

    //calculer_normale_sommet
    calculer_normale_sommet(m1,0);
    //free_maillage

    free_maillage(m1);

    return 0;
}
