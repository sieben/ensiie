#include <stdio.h>
#include <stdlib.h>
#include "../../qualite.h"
#include "../../fichiers_obj.h"

int main ()
{   
	//Cr�ation des points p01 p02 p03 (points reperes)
	printf("** Cr�ation du point repere p01 **\n");
    point p01=new_point(8,-4,1);
	afficher_point(p01);
	printf("\n");
	printf("** Cr�ation du point repere p02 **\n");
    point p02=new_point(1,2,7);
    afficher_point(p02);
	printf("\n");

	//Cr�ation des vecteurs v01 v02
	printf("** Cr�ation du vecteur repere v01 **\n");
    vecteur v01=vecteur_nouveau(7,-6,-2);
	afficher_vecteur(v01);
	printf("\n");
	printf("** Cr�ation du vecteur repere v02 **\n");
    vecteur v02=vecteur_nouveau(2,0,4);
	afficher_vecteur(v01);
	printf("\n");
  printf("** Cr�ation de la premi�re face **\n");
    
    // Creation du premier sommet de la face 
    point p0=new_point(1,2,3);
    vecteur n0=vecteur_nouveau(1,3,4);
    texture t1=new_texture(1,6);
    sommet s0=new_sommet(p0,n0,t1);
    // Creation du deuxieme sommet de la face 
    point p1=new_point(4,5,6);
    vecteur n1=vecteur_nouveau(1,2,3);
    sommet s1=new_sommet(p1,n1,t1);
    // Creation du troisieme sommet de la face 
    point p2=new_point(7,8,9);
    vecteur n2=vecteur_nouveau(7,6,4);
    sommet s2=new_sommet(p2,n2,t1);
    // Creation de la face 
    vecteur n00=vecteur_nouveau(1,2,0);
    face f0=new_face(s0,s1,s2,n00,couleur_default());
    afficher_face(f0);
    printf("\n");

    printf("** Cr�ation de la seconde face **\n");

    point p3=new_point(3,2,1);
    vecteur n3=vecteur_nouveau(9,9,9);
    texture t2=new_texture(3,0);
    sommet s3=new_sommet(p3,n3,t2);
    // Creation du deuxieme sommet de la face 
    point p4=new_point(5,9,0);
    vecteur n4=vecteur_nouveau(1,4,1);
    sommet s4=new_sommet(p4,n4,t2);
    // Creation du troisieme sommet de la face 
    point p5=new_point(9,8,7);
    vecteur n5=vecteur_nouveau(0,0,1);
    sommet s5=new_sommet(p5,n5,t2);
    // Creation de la face 
    vecteur n01=vecteur_nouveau(7,4,3);
    face f1=new_face(s3,s4,s5,n01,couleur_default());
    afficher_face(f1);

    
	// Coloration en fonction de la distance � un point
    printf("** calcul de la qualit� d'une face par rapport � un point **\n");
	printf("   ** pour la face f0 et le point p01**\n");
	printf("distance_point_quali(f0,p01) = %f\n",
		distance_point_quali(f0,p01));
    printf("   ** pour la face f1 et le point p02**\n");
    printf("distance_point_quali(f1,p02) = %f\n",
		distance_point_quali(f1,p02));
	printf("   ** pour la face f1 et le point p01**\n");
    printf("distance_point_quali(f1,p01) = %f\n",
        distance_point_quali(f1,p01));
	printf("   ** pour la face f0 et le point p02**\n");
    printf("distance_point_quali(f0,p02) = %f\n",
        distance_point_quali(f0,p02));
	printf("\n");
	printf("\n");

    // Coloration en fonction de la distance � un axe
	printf("** calcul de la qualit� d'une face par rapport � un axe **\n");
	printf("   ** pour la face f0 et le l'axe defini par p02 et v02 **\n");
    printf("distance_axe_quali(f0, p02, v02) = %f\n",
      distance_axe_quali(f0,p02,v02)); 
	printf("   ** pour la face f0 et le l'axe defini par p01 et v01 **\n");
    printf("distance_axe_quali(f0, p01, v01) = %f\n",
	  distance_axe_quali(f0,p01,v01));
	printf("   ** pour la face f1 et le l'axe defini par p01 et v01 **\n");
    printf("distance_axe_quali(f1, p01, v01) = %f\n",
      distance_axe_quali(f1,p01,v01));
	printf("\n");
	printf("\n");


    // Coloration en fonction de la distance � un plan
	printf("** calcul de la qualit� d'une face par rapport � un plan **\n");
	printf("   ** pour la face f0 et le le plan defini par p01 v01 et v02 **\n");
    printf("distance_plan_quali(f0,p01,v01,v02) = %f\n",
	distance_plan_quali(f0,p01,v01,v02));
	printf("   ** pour la face f1 et le le plan defini par p01 v01 et v02 **\n");
    printf("distance_plan_quali(f1,p01,v01,v02) = %f\n",
	distance_plan_quali(f1,p01,v01,v02));
	printf("\n");
	printf("\n");

    // Coloration en fonction du facteur de qualit�
	printf("** calcul de la qualit� (expression math�matique) d'une face  **\n");
	printf("   ** pour la face f0 **\n");
    printf("facteur_quali(f0) = %f\n",facteur_quali(f1));
	printf("\n");
	printf("\n");

 
 
	/*   char * other = "";
    tableau maills = charger_maillages("test.obj", &other );


    maillage m = acceder_element(maills, 0, maillage);
    
    // Coloration en fonction de la courbure locale
	printf("** calcul de la qualit� d'une face d'un certain indice dans un maillage  **\n");
	printf("   ** pour la face num�ro 6 et le maillage m **\n");
    printf("courbure_quali(6,m) = %f\n",
	courbure_quali(6, m));
	printf("\n");
    free_maillage(m);
    supprimer_tableau(maills);*/

    return 0;
}

