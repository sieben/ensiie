#include <stdio.h>
#include <stdlib.h>
#include "../../sommet.h"

int main()
{
    printf("** Création de sommets **\n");

    point p1=new_point(1,2,3);
    vecteur n1=vecteur_nouveau(1,3,6);
    texture t1=new_texture(1,2);
    sommet s1=new_sommet(p1,n1,t1);
    afficher_sommet(s1);

    sommet s2=new_sommet(p1,n1,t1);
    afficher_sommet(s2);

    vecteur n3=vecteur_nouveau(9,8,5);
    sommet s3=new_sommet(p1,n3,t1);
    afficher_sommet(s3);

    point p3=new_point(4,5,6);
    sommet s4=new_sommet(p3,n1,t1);
    afficher_sommet(s4);

    point p2=new_point(6,2,9);
    vecteur n2=vecteur_nouveau(3,3,1);
    sommet s5=new_sommet(p2,n2,t1);
    afficher_sommet(s5);
    printf("\n");
 
    printf("** Modification du point d'un sommet **\n");
    sommet s6=modif_point_sommet(s1,p2);
    printf("modif_point_sommet(s1,p2) : ");
    afficher_point(extraire_point(s6));
    printf("\n");

    printf("** Modification de la normale d'un sommet **\n");
    sommet s7=modif_norm_sommet(s1,n2);
    printf("modif_norm_sommet(s1,n2) : ");
    afficher_vecteur(extraire_normale(s7));
    printf("\n");

    printf("** Modification de la texture d'un sommet **\n");
    sommet s8=modif_texture_sommet(s7,t1);
    printf("modif_texture_sommet(s7,t1) : ");
    afficher_texture(extraire_texture(s8));
    printf("\n");

    printf("** Extraction de la deuxi�me coordonn�e de la texture d'un sommet **\n");
    printf("extraire_coordonnee_texture(t1,1) : %d\n",
        extraire_coordonnee_texture(t1,1));
    printf("\n");

    printf("** Comparaison de deux sommets **\n");
    printf ( "deux sommets confondus : comp_sommet(s1,s2) = %d\n",
            comp_sommet(s1,s2));
    printf ( "deux sommets de points confondus : comp_sommet(s1,s3) = %d\n",
            comp_sommet(s1,s3));
    printf ( "deux sommets de normales confondues : comp_sommet(s1,s4) = %d\n",
            comp_sommet(s1,s4));
    printf ( "deux sommets quelconque distincts : comp_sommet(s1,s5) = %d\n",
            comp_sommet(s1,s5));
    printf("\n");

    printf("** Calcul de la m�diane d'un segment compris entre deux sommets **\n");
    printf("sommets confondus : moitie_cote(s1,s2) : \n");
    afficher_sommet(moitie_cote(s1,s2));
    printf("sommets distincts : moitie_cote(s1,s3) = \n");
    afficher_sommet(moitie_cote(s1,s4));

    printf("** Copie d'un sommet **\n");
    printf("copie_sommet(s1) : ");
    afficher_sommet(copie_sommet(s1)); 

    printf("** Extraction du point du sommet **\n");
    printf ( "extraire_point(s1) : ");
    afficher_point(extraire_point(s1));
    printf("\n");

    printf("** Extraction de la normale du sommet **\n");
    printf ( "extraire_normale(s1) : ");
    afficher_vecteur(extraire_normale(s1));
    printf("\n");

    printf("** Extraction de la texture du sommet **\n");
    printf ( "extraire_texture(s1) : ");
    afficher_texture(extraire_texture(s1));
    printf("\n");

    return 0;
}
