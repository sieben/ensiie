Pour faire vos tests proprement, j'ai arrangé les dossiers : il y a un dossier
test_XXXX par module, avec XXXX le nom du module. Il faut juste que vous éditiez
le fichier main.c qui se trouve dans le dossiers de votre module.
Une fois fait, vous retourner dans le dossier principal (svn-21C), et vous
tappez : "make test_XXXX", en remplacant "XXXX" par le nom de votre module.
Pour compiler tous les tests, vous pouvez aussi tapper "make tests".

Saimoun
