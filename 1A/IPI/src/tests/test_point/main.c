#include <stdio.h>
#include <stdlib.h>
#include "../../point.h"
#include "../../vecteur.h"

int main ( void )
{
    printf("** Cr�ation de points **\n");

    point p1 = new_point (1,2,3);
    printf ( "p1 = new_point (1,2,3) : ");
    afficher_point(p1);

    point p2 = new_point (-1,-2,-3);
    printf ( "p2 = new_point (-1,-2,-3) : ");
    afficher_point(p2);

    point p3 = new_point (1,2,3);
    printf ( "p3 = new_point (1,2,3) : ");
    afficher_point(p3);

    point p4 = new_point (7,8,9);
    printf ( "p4 = new_point (7,8,9) : ");
    afficher_point(p4);
    printf("\n");

    printf("** Test de bonne formation **\n");

    printf ( "bienforme_point(p4) : %d\n" , bienforme_point(p4) );
    printf ( "bienforme_point(p1) : %d\n" , bienforme_point(p4) );
    printf("\n");

    printf("** Conversion d'un point en vecteur **\n");

    vecteur v1 =  convertir_en_vecteur (p1);
    printf ( "v1 = convertir_en_vecteur (p1) : ");
    afficher_vecteur(v1);
    printf("\n");

    printf("** Extraction de la premi�re coordonn�e du point **\n");

    printf ( "extraire_coordonnee_point(p1,1) : %f\n",
    extraire_coordonnee_point(p1,1));
    printf("\n");

    printf("** Modification de la seconde coordonn�e du point **\n");

    p2=modif_point(p2,5,-2,4);
    printf("avec seconde coordonn�e identique : modif_point(p2,5,-2,4) = ");
    afficher_point(p2);

    p4=modif_point(p4,5,-2,4);
    printf("avec seconde coordonn�e diff�rente : modif_point(p4,5,-2,4) = ");
    afficher_point(p4);
    printf("\n");

    printf("** Comparaison de deux points **\n");
    printf ( "comparaison de deux points confondus : comp_point(p1,p3) = %d\n",
            comp_point(p1,p3) );
    printf ( "comparaison de deux points distincts : comp_point(p1,p2) = %d\n",
            comp_point(p1,p2) );
    printf("\n");

    printf("** Calcul de la distance entre deux points **\n");
    printf ( "calcul de la distance entre deux points confondus : distance_point(p1,p3) = %f\n",
            distance_point(p1,p3) );
    printf ( "calcul de la distance entre deux points confondus : distance_point(p1,p2) = %f\n",
            distance_point(p1,p2) );
    printf("\n");
    return 0;
}

