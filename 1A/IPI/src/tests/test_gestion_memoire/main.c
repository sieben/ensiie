/**
 * Fichier de test du module gestion_memoire.
 * Pour pouvoir tester sans erreurs, il faut commenter toutes
 * les lignes où il y a le commentaire "va échouer"
 */

#include <stdio.h>
#include <stdlib.h>
#include "../../gestion_memoire.h"

int main (void)
{
    int i = 0;

    /*** Tests sur la gestion basique de la mémoire ***/

    char *str = NULL;
    str = allouer_mem(256*sizeof(char));

    int *sert_a_rien = NULL;
    //sert_a_rien = allouer_mem(0); // va échouer

    float *tab = NULL;
    tab = reallouer_mem(tab, 33);
    tab = reallouer_mem(tab, 30);
    //tab = reallouer_mem(tab, 0); // va échouer

    double *tab2 = NULL, *tab3 = NULL;
    //tab2 = allouer_et_init_mem(0,15); // va échouer
    //tab3 = allouer_et_init_mem(15,0); // va échouer

    liberer_mem(NULL);
    liberer_mem(str);
    liberer_mem(tab);

    /*** Tests sur les tableaux ***/

    tableau t = new_tableau();
    for (i = 0 ; i < 10 ; i++)
	ajouter_element(t, new_tableau());

    tableau t_10 = acceder_element(t, 9, tableau);
    for (i = 40 ; i < 45000 ; i++)
	ajouter_element(t_10, i*2);

    printf("Indice avant la suppression : %d\n", recherche_tableau(t, t_10));
    supprimer_element(t, 2);
    printf("Indice après la suppression : %d\n", recherche_tableau(t, t_10));

    ajouter_element(t_10, 42);
    remplacer_element(t_10, 2, 2355);
    //remplacer_element(t_10, 103000, 42); // va échouer
    //supprimer_element(t, -1); // va échouer

    for (i = 0 ; i < 9 ; i++)
	supprimer_tableau(acceder_element(t, i, tableau));
    supprimer_tableau(t);
    t = NULL;

    /*ajouter_element(t, 4); // va échouer
    remplacer_element(t, 2, 24); // va échouer
    taille_tableau(t); // va échouer
    supprimer_element(t,1); // va échouer
    vider_tableau(t); // va échouer
    supprimer_tableau(t); // va échouer
    recherche_tableau(t, t_10); // va échouer*/

    //tri tableau entiers
    printf("Tri \n");

    tableau tab_tri = new_tableau();

    ajouter_element(tab_tri,1);
    ajouter_element(tab_tri,0);
    ajouter_element(tab_tri,72);
    ajouter_element(tab_tri,8);
    ajouter_element(tab_tri,32);
    ajouter_element(tab_tri,4);
    ajouter_element(tab_tri,36);

    printf("Tableau avant le tri :\n");
    for(i = 0 ; i < taille_tableau(tab_tri) ; i++ )
	printf("%d\n",acceder_element(tab_tri,i,int));

    printf("\nTableau après le tri :\n");
    trier_tableau_entiers(tab_tri);
    for(i = 0 ; i < taille_tableau(tab_tri) ; i++ )
	printf("%d\n",acceder_element(tab_tri,i,int));

    supprimer_tableau(tab_tri);

    return 0;
}

