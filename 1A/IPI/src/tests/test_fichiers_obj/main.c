/**
 * Fichier de test du module fichier_obj.
 * Pour pouvoir tester sans erreurs, il faut commenter toutes
 * les lignes où il y a le commentaire "va échouer"
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../../fichiers_obj.h"

FILE* ouvrir (const char *adr, const char *mode);
void ecrire_comp_face (FILE *pfich, face f, tableau sommets, tableau textures,
                       tableau normales, int numero);


int main (int argc, char *argv[])
{

    /**** Tests des fonctions d'I/O ****/

    //FILE *f = NULL;
    FILE *f2 = NULL;

    //f = ouvrir("inexistant.txt", "r"); // va échouer
    //f2 = ouvrir("read-only.txt", "w+"); // va échouer

    /*f2 = ouvrir("tests/test_fichiers_obj/read-only.txt", "r");
    printf("contenu du fichier read-only.txt : \n");
    int c = fgetc(f2);
    while (c != EOF)
    {
        putchar((char)c);
        c = fgetc(f2);
    }
    fclose(f2);*/


    /**** Tests des fonctions auxiliaires de lecture d'un fichier Obj ****/


    /**** Tests des fonctions auxiliaires d'écritures d'un fichier Obj ****/

    tableau sommets = new_tableau();
    point p1 = new_point(1.0, 0.0, 0.0);
    point p2 = new_point(1.0, 1.0, -1.0);
    point p3 = new_point(0.0, -1.0, 1.0);
    vecteur v = vecteur_nouveau(0.0, 1.0, 0.0);
    texture t = allouer_mem(sizeof(struct t_texture));
    ajouter_element(sommets, new_sommet(p1, vecteur_nul, texture_default()));
    ajouter_element(sommets, new_sommet(p2, vecteur_nul, texture_default()));
    ajouter_element(sommets, new_sommet(p3, v, t));
    tableau normales = new_tableau();
    tableau textures = new_tableau();
    ajouter_element(textures, t);
    ajouter_element(normales, v);

    face fa = new_face( acceder_element(sommets, 0, sommet),
            acceder_element(sommets, 1, sommet),
            acceder_element(sommets, 2, sommet),
            vecteur_nul, couleur_default());

    printf("Composantes de la face : (devrait être 1 2 3/1/1)\n\t-->");
    ecrire_comp_face(stdout, fa, sommets, textures, normales, 0);
    ecrire_comp_face(stdout, fa, sommets, textures, normales, 1);
    ecrire_comp_face(stdout, fa, sommets, textures, normales, 2);

    putchar('\n');

    liberer_mem_face(fa);
    supprimer_tableau(normales);
    supprimer_tableau(textures);
    supprimer_tableau(sommets);


    /**** Tests des fonctions de chargement et d'enregistrement des
      fichiers Obj ****/

    if (argc < 2)
    {
        fprintf(stderr, "usage: %s fichier.obj\n", argv[0]);
        exit(1);
    }

    maillage m = NULL;

    printf("\nCopie de %s...\n", argv[1]);
    m = charger_maillage (argv[1]);
    char tmp[200];
    sprintf(tmp, "copie_de_%s", argv[1]);
    enregistrer_maillage(tmp, m);
    free_maillage(m);
    printf("\tFait.\n\n");

    return 0;
}

