
#include <stdio.h>

#include "../../detection_correction.h"
#include "../../point.h"
#include "../../maillages.h"
#include "../../fichiers_obj.h"
#include "../../qualite.h"
#include "../../intersection.h"


int main ( int argc, char * argv[] )
{

/* Il faut reserver une collection de points dont les combinaisons forment
	   tous les cas possibles.
    Nous allons choisir des points dont les coordonnées seront des elements
	   de l'ensemble {-1.0,0.0,1.0} afin de faciliter la representation dans l'espace.
 */


    point c1,c2,c3,c4,c5,c6,c7,c8; // Les points qui sont des coins
	point o; // Point origine {0,0,0}
	point m1,m2,m3,m4,m5,m6; // Les points "milieu", centre des faces
	point a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12;

	// A REMPLIR AVEC UN RUBIKS CUBE 

	o = new_point(0.0,0.0,0.0);

	c1 = new_point(1.0,1.0,1.0);
    c2 = new_point(-1.0,-1.0,-1.0);
	c3 = new_point(1.0,-1.0,-1.0);
	c4 = new_point(1.0,-1.0,1.0);
	c5 = new_point(1.0,1.0,-1.0);
	c6 = new_point(-1.0,1.0,1.0);
	c7 = new_point(-1.0,1.0,-1.0);
	c8 = new_point(-1.0,-1.0,1.0);

	m1 = new_point (0.0,0.0, 1.0 );
  m2 = new_point (0.0,0.0, -1.0 );
  m3 = new_point (0.0, 1.0 ,0.0);
  m4 = new_point (0.0, -1.0 ,0.0);
  m5 = new_point ( 1.0 ,0.0,0.0);
  m6 = new_point ( -1.0 ,0.0,0.0);
//
  a1 = new_point ( 1.0 , 1.0 ,0.0);
  a2 = new_point ( -1.0 , -1.0 ,0.0);
  a3 = new_point ( 1.0 , -1.0 ,0.0);
  a4 = new_point ( -1.0 , 1.0 ,0.0);
//
  a5 = new_point ( 1.0,0.0,1.0);
  a6 = new_point (0.0, 1.0 , 1.0);
  a7 = new_point (-1.0,0.0, 1.0 );
  a8 = new_point (0.0, -1.0 , 1.0);
//
  a9 = new_point  (1.0,0.0,-1.0);
  a10 = new_point (0.0,1.0,-1.0);
  a11 = new_point (-1.0,0.0,-1.0);
  a12 = new_point (0.0,-1.0,-1.0);
//
  int res;
  maillage m;
//
  // Tests des fonctions de detections et de corrections d'erreur 
//
  // Sommets non manifolds (2D) :
//
  sommet sm1,sc1,sc6,sc4,sc8;
  face f1, f2;
//
  sm1 = new_sommet(m1,NULL,NULL);
  sc1 = new_sommet(c1,NULL,NULL);
  sc6 = new_sommet(c6,NULL,NULL);
  sc4 = new_sommet(c4,NULL,NULL);
  sc8 = new_sommet(c8,NULL,NULL);
//
  f1 = new_face(sm1,sc1,sc6, NULL , couleur_default() );
  f2 = new_face(sm1,sc4,sc8, NULL , couleur_default() );
//
  m = new_maillage("test");
//
  ajouter_face(m,f1);
  ajouter_face(m,f2);
//
  recherche_somm_nonmanifold(m);
//
  // Sommets non manifolds (3D) :
//
  sommet sc7,sc5;
  face f3;
  sc7 = new_sommet(c7,NULL,NULL);
  sc5 = new_sommet(c5,NULL,NULL);
//
  f3 = new_face( sm1 , sc7 , sc5 , NULL , couleur_default() );
//
  m= new_maillage("test");
//
  ajouter_face(m,f1);
  ajouter_face(m,f3);
//
  recherche_somm_nonmanifold(m);
//
  // Tests des fonctions d'intersection de triangles 
//
//    printf("\n\nIntersection de triangles\n\n");
//
  // Cas nominaux 
//
//
  // Triangles 3D 
//
  // Triangles qui se coupent 
//
  //tri_tri_overlap_test_3D(c6,a12,c1,c8,a10,c4);
  res =	intersection(c6,a12,c1,c8,a10,c4);
  printf("Triangles qui se coupent (3D). Résultat attendu : 1, Résultat obtenu : %d \n",res);
//
  // Triangles disjoints 
//
  //check_min_max(c6,c4,c1,c7,c3,c2);
  res = intersection(c6,c4,c1,c7,c3,c2);
  printf("Triangles disjoints (3D). Résultat attendu : 0, Résultat obtenu : %d \n",res);
//
  // Triangles 2D 
//
  // Triangles qui se coupent 
//
  res =  intersection(a1,m4,m6,a2,m3,m5);
  printf("Triangles qui se coupent (2D). Résultat attendu : 1, Résultat obtenu : %d \n",res);
//
  // Triangles disjoints 
//
  res =    intersection(a1,m5,m3,m6,m4,a2);
  printf("Triangles disjoints (2D). Résultat attendu : 0, Résultat obtenu : %d \n",res);
//
  // Tests aux limites 
//
  // Triangles en 3D 
//
  // Triangles egaux 
//
  res =    intersection(c6,c4,m2,c6,c4,m2);
  printf("Triangles égaux (3D). Résultat attendu : 1, Résultat obtenu : %d\n",res);
  //intersection(c6,c4,m2,c6,c4,m2);
//
  // Triangles qui ont un sommet commun 
//
  res =    intersection(c1,c8,c4,c2,c5,c4);
  printf("Triangles qui ont un sommet commun (3D). Résultat attendu : 1, Résultat obtenu : %d \n",res);
  //tri_tri_overlap_test_3D(c1,c8,c4,c2,c5,c4);
//
  // Triangles qui ont une arete commune 
//
  res =    intersection(c2,c6,c4,c5,c6,c4);
  printf("Triangles qui ont une arête commune (3D). Résultat attendu : 1, Résultat obtenu : %d \n",res);
  //tri_tri_overlap_test_3D(c2,c6,c4,c5,c6,c4);
//
  // Triangles en 2D 
//
  // ATTENTION LES POINTS SONT DANS LE PLAN (X,Y) ! 
//
  // Triangles egaux 
//
  res =    intersection(m3,a2,a3,m3,a2,a3);
  printf("Triangles égaux (2D). Résultat attendu : 1, Résultat obtenu : %d \n",res);
  //ccw_tri_tri_intersection_2D(m3,a2,a3,m3,a2,a3);
//
  // Triangles qui ont un sommet commun 
//
  res =    intersection(o,m5,m3,o,m4,m6);
  printf("Triangles qui ont un sommet commun (2D). Résultat attendu : 1, Résultat obtenu : %d \n",res);
  //ccw_tri_tri_intersection_2D(o,m5,m3,o,m4,m6);
//
  // Triangles qui ont une arete commune 
//
  //ccw_tri_tri_intersection_2D(a4,a2,a3,a1,a3,a4);
  res =    intersection(a4,a2,a3,a1,a3,a4);
  printf("Triangles qui ont une arête commune (2D). Résultat attendu : 1, Résultat obtenu : %d \n",res);


/*    int i = 0;
    maillage m = NULL;
    tableau maills = charger_maillage("obj/MeshErrors/venus2_NonManifoldVertex_1.obj");

    for (i = 0 ; i < taille_tableau(maills) ; i++)
    {
        m = acceder_element(maills, i, maillage);
        printf("Maillage %s : %d sommet(s) non manifold(s).\n",
                nom_maillage(m), recherche_somm_nonmanifold(m));
        free_maillage(m);
    }

    supprimer_tableau(maills);*/

	return 0;
}
