# -------------------------------------------------------------------------- #
#                                                                            #
#         Makefile pour la boite a outils de gestion des maillages 3D        #
#         Par Simon Lasnier                                                  #
#                                                                            #
# -------------------------------------------------------------------------- #

# ----- Debug -----
# on pour activer le debug (make DEBUG=on)
# off pour le desactiver (make DEBUG=off)
# pour tout recompiler avec du debug, faire un make mrproper && make DEBUG=on
DEBUG = on

# ----- Nom du module -----
MODULE = subdivision

# ---- Nom de l'executable ----
EXEC = test_$(MODULE)

# -------------------------------------------------------------------------- #
#                           Variables internes                               #
# -------------------------------------------------------------------------- #

# Programmes
CC = gcc
LD = gcc
CFLAGS = -W -Wall
LDFLAGS = -lm -lncurses

# Option de Debug (regle par la variable DEBUG, ne pas toucher)
OPT_DEBUG =	

ifeq ($(DEBUG),on)
OPT_DEBUG = -g
endif


# ----------------------------------------------------------------------
# Cibles possibles :
#	- make ou make all : compile le programme de test
#	- make clean : nettoie le dossier (apres compilation)
#	- make mrproper : nettoie, supprime l'executable et la doc
# ----------------------------------------------------------------------


all: $(EXEC)

$(EXEC): main.o ../../$(MODULE).o ../../fichiers_obj.o ../../maillages.o \
    ../../point.o ../../sommet.o ../../face.o ../../gestion_memoire.o \
    ../../vecteur.o ../../affichagencurses.o ../../detection_correction.o
	@echo "Tests du module : $(MODULE)"
	@echo -e "\tEdition de lien et production de l'executable $(EXEC)"
	$(LD) $(LDFLAGS) $(OPT_DEBUG) -o $(EXEC) $^

.PHONY : mrproper clean doc

clean:
	@echo "Tests du module : $(MODULE)"
	@echo -e "\tSuppression des fichiers *.o et des fichiers temporaires"
	rm -rf *.o *~ *.gch

mrproper: clean
	@echo "Tests du module : $(MODULE)"
	@echo -e "\tSuppression de l'executable $(EXEC)"
	rm -rf $(EXEC)

main.o: main.c
	@echo "Tests du module : $(MODULE)"
	@echo -e "\tCompilation de $<"
	$(CC) $(OPT_DEBUG) $(CFLAGS) -c -o $@ $<
