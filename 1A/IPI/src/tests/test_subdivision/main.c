/**
 * Fichier de test du module gestion_memoire.
 * Pour pouvoir tester sans erreurs, il faut commenter toutes
 * les lignes où il y a le commentaire "va échouer"
 */

#include <stdio.h>
#include <stdlib.h>

#include "../../subdivision.h"
#include "../../fichiers_obj.h"

 
int main ( void )
{
    // Tests sur la fonction subdivision_plane
    maillage m = charger_maillage ( "triangle.obj" );
    subdivision_plane ( m );
    enregistrer_maillage ( "test1.obj" , m );

    //2eme test
    m = charger_maillage ( "../../obj/detection_correction/face_degenere.obj" );
    subdivision_plane ( m ); //va echouer
    enregistrer_maillage ( "test2.obj" , m );

    //3eme test
    m = charger_maillage ( "../../obj/subdivision/cube.obj" );
    subdivision_plane ( m );
    enregistrer_maillage ( "test3.obj" , m );
    
    //4eme test
    m = charger_maillage ( "../../obj/subdivision/d_sphere.obj" );
    subdivision_plane ( m );
    enregistrer_maillage ( "test4.obj" , m );

    // Tests sur la fonction subdivision_normale
    m = charger_maillage ("../../obj/detection_correction/avec_trous.obj" );
    subdivision_normale ( m ); //va échouer
    enregistrer_maillage ( "test5.obj" , m );
  
    //2em test
    m = charger_maillage ("../../obj/subdivision/cube.obj" );
    subdivision_normale ( m );
    enregistrer_maillage ( "test6.obj" , m );

    //3em test
    m = charger_maillage ("../../obj/subdivision/d_sphere.obj" );
    subdivision_normale ( m );
    enregistrer_maillage ( "test7.obj" , m );

    //4eme test
    m = charger_maillage ("../../obj/detection_correction/sommet_duplique.obj");
    subdivision_normale ( m ); //va échouer
  
    //5eme test
    m = charger_maillage("../../obj/detection_correction/sommet_non_manifold.obj");
    subdivision_normale ( m );//va échouer

    //6eme test
    m = charger_maillage("../../obj/detection_correction/paroi_fine.obj" );
    subdivision_normale ( m );
    
  
    //7eme test
    m = charger_maillage("../../obj/detection_correction/bol_epaisseur.obj" );
    subdivision_normale ( m );

    return 0;
}
