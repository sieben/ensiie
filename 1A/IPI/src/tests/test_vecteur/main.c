#include <stdio.h>
#include "../../vecteur.h"

int main ( void )
{
    // Création de vecteurs
    printf ( "vecteur_nul = %p\n" , vecteur_nul );

    vecteur v1 = vecteur_nouveau (1,2,3);
    printf ( "v1 = vecteur_nouveau (1,2,3) : ");
    afficher_vecteur(v1);

    vecteur v2 = vecteur_nouveau (-1,-2,-3);
    printf ( "v2 = vecteur_nouveau (-1,-2,-3) : ");
    afficher_vecteur(v2);

    vecteur v3 = vecteur_copie ( v2 );
    printf ( "v3 = vecteur_copie ( v2 ) : ");
    afficher_vecteur(v3);

    // Test de nullité
    printf ( "est_nul (vecteur_nul) : %d\n" , est_nul ( vecteur_nul ) );

    // Addition de deux vecteurs
    vecteur_add ( v1 , v2 );
    printf ( "v1 + v2 = ");
    afficher_vecteur(v2);

    vecteur_add (vecteur_nul , v3 );
    printf ( "v3 + vecteur_nul = ");
    afficher_vecteur(v3);

    // Soustraction de deux vecteurs
    vecteur_diff ( vecteur_nul , v1 );
    printf ( "v1 - v0 = ");
    afficher_vecteur(v1);

    vecteur_diff (v2, v2);
    printf ( "est_nul(v2 - v2) : %d\n" , est_nul(v2));

    return 0;
}
