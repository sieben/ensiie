#include "point.h"
#include "face.h"
#include "maillages.h"
#include "affichagencurses.h"

/* coplanar returns whether the triangles are coplanar
 *  source and target are the endpoints of the segment of
 *  intersection if it exists)
 */

int tri_tri_overlap_test_2d( float p1[2], float q1[2], float r1[2],
			     float p2[2], float q2[2], float r2[2] );

int coplanar_tri_tri3d( float p1[3], float q1[3], float r1[3],
			float p2[3], float q2[3], float r2[3],
			float normal_1[3], float normal_2[3] );

/* some 3D macros */

#define CROSS(dest,v1,v2) dest[0]=v1[1]*v2[2]-v1[2]*v2[1]; \
                          dest[1]=v1[2]*v2[0]-v1[0]*v2[2]; \
                          dest[2]=v1[0]*v2[1]-v1[1]*v2[0];

#define DOT(v1,v2) (v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2])

#define SUB(dest,v1,v2) dest[0]=v1[0]-v2[0]; \
                        dest[1]=v1[1]-v2[1]; \
                        dest[2]=v1[2]-v2[2];

#define SCALAR(dest,alpha,v) dest[0] = alpha * v[0]; \
                             dest[1] = alpha * v[1]; \
                             dest[2] = alpha * v[2];

#define CHECK_MIN_MAX(p1,q1,r1,p2,q2,r2) {\
    SUB(v1,p2,q1)\
    SUB(v2,p1,q1)\
    CROSS(N1,v1,v2)\
    SUB(v1,q2,q1)\
    if (DOT(v1,N1) > 0.0f) return 0;\
    SUB(v1,p2,p1)\
    SUB(v2,r1,p1)\
    CROSS(N1,v1,v2)\
    SUB(v1,r2,p1) \
    if (DOT(v1,N1) > 0.0f) return 0;\
    else return 1; }



/* Permutation in a canonical form of T2's vertices */

#define TRI_TRI_3D(p1,q1,r1,p2,q2,r2,dp2,dq2,dr2) { \
    if (dp2 > 0.0f) { \
        if (dq2 > 0.0f) CHECK_MIN_MAX(p1,r1,q1,r2,p2,q2) \
        else if (dr2 > 0.0f) CHECK_MIN_MAX(p1,r1,q1,q2,r2,p2)\
        else CHECK_MIN_MAX(p1,q1,r1,p2,q2,r2) }\
    else if (dp2 < 0.0f) { \
        if (dq2 < 0.0f) CHECK_MIN_MAX(p1,q1,r1,r2,p2,q2)\
        else if (dr2 < 0.0f) CHECK_MIN_MAX(p1,q1,r1,q2,r2,p2)\
        else CHECK_MIN_MAX(p1,r1,q1,p2,q2,r2)\
    } else { \
        if (dq2 < 0.0f) { \
            if (dr2 >= 0.0f)  CHECK_MIN_MAX(p1,r1,q1,q2,r2,p2)\
            else CHECK_MIN_MAX(p1,q1,r1,p2,q2,r2)\
        } \
        else if (dq2 > 0.0f) { \
            if (dr2 > 0.0f) CHECK_MIN_MAX(p1,r1,q1,p2,q2,r2)\
            else  CHECK_MIN_MAX(p1,q1,r1,q2,r2,p2)\
        } \
        else  { \
            if (dr2 > 0.0f) CHECK_MIN_MAX(p1,q1,r1,r2,p2,q2)\
            else if (dr2 < 0.0f) CHECK_MIN_MAX(p1,r1,q1,r2,p2,q2)\
            else return coplanar_tri_tri3d(p1,q1,r1,p2,q2,r2,N1,N2);\
        }}}



/*
 *
 *  Three-dimensional Triangle-Triangle Overlap Test
 *
 */


int tri_tri_overlap_test_3d ( float p1[3], float q1[3], float r1[3],
                              float p2[3], float q2[3], float r2[3] )
{
    // On cherche si les triangles ont un point en commun
    if ( p1[0] == p2[0] && p1[1] == p2[1] && p1[2] == p2[2] ) return 0;
    if ( p1[0] == q2[0] && p1[1] == q2[1] && p1[2] == q2[2] ) return 0;
    if ( p1[0] == r2[0] && p1[1] == r2[1] && p1[2] == r2[2] ) return 0;
    if ( q1[0] == p2[0] && q1[1] == p2[1] && q1[2] == p2[2] ) return 0;
    if ( q1[0] == q2[0] && q1[1] == q2[1] && q1[2] == q2[2] ) return 0;
    if ( q1[0] == r2[0] && q1[1] == r2[1] && q1[2] == r2[2] ) return 0;
    if ( r1[0] == p2[0] && r1[1] == p2[1] && r1[2] == p2[2] ) return 0;
    if ( r1[0] == q2[0] && r1[1] == q2[1] && r1[2] == q2[2] ) return 0;
    if ( r1[0] == r2[0] && r1[1] == r2[1] && r1[2] == r2[2] ) return 0;

    float dp1, dq1, dr1, dp2, dq2, dr2;
    float v1[3], v2[3];
    float N1[3], N2[3];

    /* Compute distance signs  of p1, q1 and r1 to the plane of
       triangle(p2,q2,r2) */

    SUB(v1,p2,r2)
    SUB(v2,q2,r2)
    CROSS(N2,v1,v2)

    SUB(v1,p1,r2)
    dp1 = DOT(v1,N2);
    SUB(v1,q1,r2)
    dq1 = DOT(v1,N2);
    SUB(v1,r1,r2)
    dr1 = DOT(v1,N2);

    if (((dp1 * dq1) > 0.0f) && ((dp1 * dr1) > 0.0f))  return 0;

    /* Compute distance signs  of p2, q2 and r2 to the plane of
       triangle(p1,q1,r1) */


    SUB(v1,q1,p1)
    SUB(v2,r1,p1)
    CROSS(N1,v1,v2)

    SUB(v1,p2,r1)
    dp2 = DOT(v1,N1);
    SUB(v1,q2,r1)
    dq2 = DOT(v1,N1);
    SUB(v1,r2,r1)
    dr2 = DOT(v1,N1);

    if (((dp2 * dq2) > 0.0f) && ((dp2 * dr2) > 0.0f)) return 0;

    /* Permutation in a canonical form of T1's vertices */


    if (dp1 > 0.0f)
    {
        if (dq1 > 0.0f) TRI_TRI_3D(r1,p1,q1,p2,r2,q2,dp2,dr2,dq2)
        else if (dr1 > 0.0f) TRI_TRI_3D(q1,r1,p1,p2,r2,q2,dp2,dr2,dq2)
        else TRI_TRI_3D(p1,q1,r1,p2,q2,r2,dp2,dq2,dr2)
    }
    else if (dp1 < 0.0f)
    {
        if (dq1 < 0.0f) TRI_TRI_3D(r1,p1,q1,p2,q2,r2,dp2,dq2,dr2)
        else if (dr1 < 0.0f) TRI_TRI_3D(q1,r1,p1,p2,q2,r2,dp2,dq2,dr2)
        else TRI_TRI_3D(p1,q1,r1,p2,r2,q2,dp2,dr2,dq2)
    }
    else
    {
        if (dq1 < 0.0f)
        {
            if (dr1 >= 0.0f) TRI_TRI_3D(q1,r1,p1,p2,r2,q2,dp2,dr2,dq2)
            else TRI_TRI_3D(p1,q1,r1,p2,q2,r2,dp2,dq2,dr2)
        }
        else if (dq1 > 0.0f)
        {
            if (dr1 > 0.0f) TRI_TRI_3D(p1,q1,r1,p2,r2,q2,dp2,dr2,dq2)
            else TRI_TRI_3D(q1,r1,p1,p2,q2,r2,dp2,dq2,dr2)
        }
        else
        {
            if (dr1 > 0.0f) TRI_TRI_3D(r1,p1,q1,p2,q2,r2,dp2,dq2,dr2)
            else if (dr1 < 0.0f) TRI_TRI_3D(r1,p1,q1,p2,r2,q2,dp2,dr2,dq2)
            else return coplanar_tri_tri3d(p1,q1,r1,p2,q2,r2,N1,N2);
        }
    }
}


int coplanar_tri_tri3d ( float p1[3], float q1[3], float r1[3],
                         float p2[3], float q2[3], float r2[3],
                         float normal_1[3], float normal_2[3] )
{
    float P1[2], Q1[2], R1[2];
    float P2[2], Q2[2], R2[2];

    float n_x, n_y, n_z;

    n_x = ((normal_1[0]<0)?-normal_1[0]:normal_1[0]);
    n_y = ((normal_1[1]<0)?-normal_1[1]:normal_1[1]);
    n_z = ((normal_1[2]<0)?-normal_1[2]:normal_1[2]);


    /* Projection of the triangles in 3D onto 2D such that the area of
       the projection is maximized. */


    if (( n_x > n_z ) && ( n_x >= n_y ))
    {
        // Project onto plane YZ

        P1[0] = q1[2]; P1[1] = q1[1];
        Q1[0] = p1[2]; Q1[1] = p1[1];
        R1[0] = r1[2]; R1[1] = r1[1];

        P2[0] = q2[2]; P2[1] = q2[1];
        Q2[0] = p2[2]; Q2[1] = p2[1];
        R2[0] = r2[2]; R2[1] = r2[1];
    }
    else if (( n_y > n_z ) && ( n_y >= n_x ))
    {
        // Project onto plane XZ

        P1[0] = q1[0]; P1[1] = q1[2];
        Q1[0] = p1[0]; Q1[1] = p1[2];
        R1[0] = r1[0]; R1[1] = r1[2];

        P2[0] = q2[0]; P2[1] = q2[2];
        Q2[0] = p2[0]; Q2[1] = p2[2];
        R2[0] = r2[0]; R2[1] = r2[2];
    }
    else
    {
        // Project onto plane XY

        P1[0] = p1[0]; P1[1] = p1[1];
        Q1[0] = q1[0]; Q1[1] = q1[1];
        R1[0] = r1[0]; R1[1] = r1[1];

        P2[0] = p2[0]; P2[1] = p2[1];
        Q2[0] = q2[0]; Q2[1] = q2[1];
        R2[0] = r2[0]; R2[1] = r2[1];
    }

    return tri_tri_overlap_test_2d(P1,Q1,R1,P2,Q2,R2);
}


/*
 *
 *  Three-dimensional Triangle-Triangle Intersection
 *
 */

/*
   This macro is called when the triangles surely intersect
   It constructs the segment of intersection of the two triangles
   if they are not coplanar.
 */

#define CONSTRUCT_INTERSECTION(p1,q1,r1,p2,q2,r2) { \
    SUB(v1,q1,p1) \
    SUB(v2,r2,p1) \
    CROSS(N,v1,v2) \
    SUB(v,p2,p1) \
    if (DOT(v,N) > 0.0f) {\
        SUB(v1,r1,p1) \
        CROSS(N,v1,v2) \
        if (DOT(v,N) <= 0.0f) { \
            SUB(v2,q2,p1) \
            CROSS(N,v1,v2) \
            if (DOT(v,N) > 0.0f) { \
                SUB(v1,p1,p2) \
                SUB(v2,p1,r1) \
                alpha = DOT(v1,N2) / DOT(v2,N2); \
                SCALAR(v1,alpha,v2) \
                SUB(source,p1,v1) \
                SUB(v1,p2,p1) \
                SUB(v2,p2,r2) \
                alpha = DOT(v1,N1) / DOT(v2,N1); \
                SCALAR(v1,alpha,v2) \
                SUB(target,p2,v1) \
                return 1; \
            } else { \
                SUB(v1,p2,p1) \
                SUB(v2,p2,q2) \
                alpha = DOT(v1,N1) / DOT(v2,N1); \
                SCALAR(v1,alpha,v2) \
                SUB(source,p2,v1) \
                SUB(v1,p2,p1) \
                SUB(v2,p2,r2) \
                alpha = DOT(v1,N1) / DOT(v2,N1); \
                SCALAR(v1,alpha,v2) \
                SUB(target,p2,v1) \
                return 1; \
            } \
        } else { \
            return 0; \
        } \
    } else { \
        SUB(v2,q2,p1) \
        CROSS(N,v1,v2) \
        if (DOT(v,N) < 0.0f) { \
            return 0; \
        } else { \
            SUB(v1,r1,p1) \
            CROSS(N,v1,v2) \
            if (DOT(v,N) >= 0.0f) { \
                SUB(v1,p1,p2) \
                SUB(v2,p1,r1) \
                alpha = DOT(v1,N2) / DOT(v2,N2); \
                SCALAR(v1,alpha,v2) \
                SUB(source,p1,v1) \
                SUB(v1,p1,p2) \
                SUB(v2,p1,q1) \
                alpha = DOT(v1,N2) / DOT(v2,N2); \
                SCALAR(v1,alpha,v2) \
                SUB(target,p1,v1) \
                return 1; \
            } else { \
                SUB(v1,p2,p1) \
                SUB(v2,p2,q2) \
                alpha = DOT(v1,N1) / DOT(v2,N1); \
                SCALAR(v1,alpha,v2) \
                SUB(source,p2,v1) \
                SUB(v1,p1,p2) \
                SUB(v2,p1,q1) \
                alpha = DOT(v1,N2) / DOT(v2,N2); \
                SCALAR(v1,alpha,v2) \
                SUB(target,p1,v1) \
                return 1; \
            }}}}


#define TRI_TRI_INTER_3D(p1,q1,r1,p2,q2,r2,dp2,dq2,dr2) { \
    if (dp2 > 0.0f) { \
        if (dq2 > 0.0f) CONSTRUCT_INTERSECTION(p1,r1,q1,r2,p2,q2) \
        else if (dr2 > 0.0f) CONSTRUCT_INTERSECTION(p1,r1,q1,q2,r2,p2)\
        else CONSTRUCT_INTERSECTION(p1,q1,r1,p2,q2,r2) }\
    else if (dp2 < 0.0f) { \
        if (dq2 < 0.0f) CONSTRUCT_INTERSECTION(p1,q1,r1,r2,p2,q2)\
        else if (dr2 < 0.0f) CONSTRUCT_INTERSECTION(p1,q1,r1,q2,r2,p2)\
        else CONSTRUCT_INTERSECTION(p1,r1,q1,p2,q2,r2)\
    } else { \
        if (dq2 < 0.0f) { \
            if (dr2 >= 0.0f)  CONSTRUCT_INTERSECTION(p1,r1,q1,q2,r2,p2)\
            else CONSTRUCT_INTERSECTION(p1,q1,r1,p2,q2,r2)\
        } \
        else if (dq2 > 0.0f) { \
            if (dr2 > 0.0f) CONSTRUCT_INTERSECTION(p1,r1,q1,p2,q2,r2)\
            else  CONSTRUCT_INTERSECTION(p1,q1,r1,q2,r2,p2)\
        } \
        else  { \
            if (dr2 > 0.0f) CONSTRUCT_INTERSECTION(p1,q1,r1,r2,p2,q2)\
            else if (dr2 < 0.0f) CONSTRUCT_INTERSECTION(p1,r1,q1,r2,p2,q2)\
            else { \
                *coplanar = 1; \
                return coplanar_tri_tri3d(p1,q1,r1,p2,q2,r2,N1,N2);\
            } \
        }}}



/*
   The following version computes the segment of intersection of the
   two triangles if it exists.
   coplanar returns whether the triangles are coplanar
   source and target are the endpoints of the line segment of intersection
 */


int tri_tri_intersection_test_3d(float p1[3], float q1[3], float r1[3],
        float p2[3], float q2[3], float r2[3],
        int * coplanar,
        float source[3], float target[3] )
{
    float dp1, dq1, dr1, dp2, dq2, dr2;
    float v1[3], v2[3], v[3];
    float N1[3], N2[3], N[3];
    float alpha;

    // Compute distance signs  of p1, q1 and r1
    // to the plane of triangle(p2,q2,r2)


    SUB(v1,p2,r2)
    SUB(v2,q2,r2)
    CROSS(N2,v1,v2)

    SUB(v1,p1,r2)
    dp1 = DOT(v1,N2);
    SUB(v1,q1,r2)
    dq1 = DOT(v1,N2);
    SUB(v1,r1,r2)
    dr1 = DOT(v1,N2);

    if (((dp1 * dq1) > 0.0f) && ((dp1 * dr1) > 0.0f))  return 0;

    // Compute distance signs  of p2, q2 and r2
    // to the plane of triangle(p1,q1,r1)


    SUB(v1,q1,p1)
    SUB(v2,r1,p1)
    CROSS(N1,v1,v2)

    SUB(v1,p2,r1)
    dp2 = DOT(v1,N1);
    SUB(v1,q2,r1)
    dq2 = DOT(v1,N1);
    SUB(v1,r2,r1)
    dr2 = DOT(v1,N1);

    if (((dp2 * dq2) > 0.0f) && ((dp2 * dr2) > 0.0f)) return 0;

    // Permutation in a canonical form of T1's vertices


    if (dp1 > 0.0f)
    {
        if (dq1 > 0.0f) TRI_TRI_INTER_3D(r1,p1,q1,p2,r2,q2,dp2,dr2,dq2)
        else if (dr1 > 0.0f) TRI_TRI_INTER_3D(q1,r1,p1,p2,r2,q2,dp2,dr2,dq2)

        else TRI_TRI_INTER_3D(p1,q1,r1,p2,q2,r2,dp2,dq2,dr2)
    } else if (dp1 < 0.0f) {
        if (dq1 < 0.0f) TRI_TRI_INTER_3D(r1,p1,q1,p2,q2,r2,dp2,dq2,dr2)
        else if (dr1 < 0.0f) TRI_TRI_INTER_3D(q1,r1,p1,p2,q2,r2,dp2,dq2,dr2)
        else TRI_TRI_INTER_3D(p1,q1,r1,p2,r2,q2,dp2,dr2,dq2)
    } else {
        if (dq1 < 0.0f) {
            if (dr1 >= 0.0f) TRI_TRI_INTER_3D(q1,r1,p1,p2,r2,q2,dp2,dr2,dq2)
            else TRI_TRI_INTER_3D(p1,q1,r1,p2,q2,r2,dp2,dq2,dr2)
        }
        else if (dq1 > 0.0f) {
            if (dr1 > 0.0f) TRI_TRI_INTER_3D(p1,q1,r1,p2,r2,q2,dp2,dr2,dq2)
            else TRI_TRI_INTER_3D(q1,r1,p1,p2,q2,r2,dp2,dq2,dr2)
        }
        else  {
            if (dr1 > 0.0f) TRI_TRI_INTER_3D(r1,p1,q1,p2,q2,r2,dp2,dq2,dr2)
            else if (dr1 < 0.0f) TRI_TRI_INTER_3D(r1,p1,q1,p2,r2,q2,dp2,dr2,dq2)
            else {
                // triangles are co-planar

                *coplanar = 1;
                return coplanar_tri_tri3d(p1,q1,r1,p2,q2,r2,N1,N2);
            }
        }
    }
}



/* some 2D macros */

#define ORIENT_2D(a, b, c)  ((a[0]-c[0])*(b[1]-c[1])-(a[1]-c[1])*(b[0]-c[0]))


#define INTERSECTION_TEST_VERTEX(P1, Q1, R1, P2, Q2, R2) {\
    if (ORIENT_2D(R2,P2,Q1) >= 0.0f)\
    if (ORIENT_2D(R2,Q2,Q1) <= 0.0f)\
    if (ORIENT_2D(P1,P2,Q1) > 0.0f) {\
        if (ORIENT_2D(P1,Q2,Q1) <= 0.0f) return 1; \
        else return 0;} else {\
            if (ORIENT_2D(P1,P2,R1) >= 0.0f)\
            if (ORIENT_2D(Q1,R1,P2) >= 0.0f) return 1; \
            else return 0;\
            else return 0;}\
    else \
    if (ORIENT_2D(P1,Q2,Q1) <= 0.0f)\
    if (ORIENT_2D(R2,Q2,R1) <= 0.0f)\
    if (ORIENT_2D(Q1,R1,Q2) >= 0.0f) return 1; \
    else return 0;\
    else return 0;\
    else return 0;\
    else\
    if (ORIENT_2D(R2,P2,R1) >= 0.0f) \
    if (ORIENT_2D(Q1,R1,R2) >= 0.0f)\
    if (ORIENT_2D(P1,P2,R1) >= 0.0f) return 1;\
    else return 0;\
    else \
    if (ORIENT_2D(Q1,R1,Q2) >= 0.0f) {\
        if (ORIENT_2D(R2,R1,Q2) >= 0.0f) return 1; \
        else return 0; }\
    else return 0; \
    else  return 0; \
}


#define INTERSECTION_TEST_EDGE(P1, Q1, R1, P2, Q2, R2) { \
    if (ORIENT_2D(R2,P2,Q1) >= 0.0f) {\
        if (ORIENT_2D(P1,P2,Q1) >= 0.0f) { \
            if (ORIENT_2D(P1,Q1,R2) >= 0.0f) return 1; \
            else return 0;} else { \
                if (ORIENT_2D(Q1,R1,P2) >= 0.0f){ \
                    if (ORIENT_2D(R1,P1,P2) >= 0.0f) return 1; else return 0;} \
                else return 0; } \
    } else {\
        if (ORIENT_2D(R2,P2,R1) >= 0.0f) {\
            if (ORIENT_2D(P1,P2,R1) >= 0.0f) {\
                if (ORIENT_2D(P1,R1,R2) >= 0.0f) return 1;  \
                else {\
                    if (ORIENT_2D(Q1,R1,R2) >= 0.0f) return 1; else return 0;}}\
            else  return 0; }\
        else return 0; }}


int ccw_tri_tri_intersection_2d(float p1[2], float q1[2], float r1[2],
        float p2[2], float q2[2], float r2[2])
{
    if ( ORIENT_2D(p2,q2,p1) >= 0.0f ) {
        if ( ORIENT_2D(q2,r2,p1) >= 0.0f ) {
            if ( ORIENT_2D(r2,p2,p1) >= 0.0f ) return 1;
            else INTERSECTION_TEST_EDGE(p1,q1,r1,p2,q2,r2)
        } else {
            if ( ORIENT_2D(r2,p2,p1) >= 0.0f )
                INTERSECTION_TEST_EDGE(p1,q1,r1,r2,p2,q2)
            else INTERSECTION_TEST_VERTEX(p1,q1,r1,p2,q2,r2)}}
    else {
        if ( ORIENT_2D(q2,r2,p1) >= 0.0f ) {
            if ( ORIENT_2D(r2,p2,p1) >= 0.0f )
                INTERSECTION_TEST_EDGE(p1,q1,r1,q2,r2,p2)
            else  INTERSECTION_TEST_VERTEX(p1,q1,r1,q2,r2,p2)}
        else INTERSECTION_TEST_VERTEX(p1,q1,r1,r2,p2,q2)}
}

int tri_tri_overlap_test_2d( float p1[2], float q1[2], float r1[2],
        float p2[2], float q2[2], float r2[2])
{
    if ( ORIENT_2D(p1,q1,r1) < 0.0f )
        if ( ORIENT_2D(p2,q2,r2) < 0.0f )
            return ccw_tri_tri_intersection_2d(p1,r1,q1,p2,r2,q2);
        else
            return ccw_tri_tri_intersection_2d(p1,r1,q1,p2,q2,r2);
    else
        if ( ORIENT_2D(p2,q2,r2) < 0.0f )
            return ccw_tri_tri_intersection_2d(p1,q1,r1,p2,r2,q2);
        else
            return ccw_tri_tri_intersection_2d(p1,q1,r1,p2,q2,r2);

}

/* Fonction chargée de convertir une donnée sous la forme de point
 * en un simple tableau de trois float 
 */

void convertir_point_en_tab (point p, float tab[3])
{
    tab[0]=p->x;
    tab[1]=p->y;
    tab[2]=p->z;
}


int intersection ( point p1 , point q1 , point r1 ,
                   point p2 , point q2 , point r2 )
{
    float tp1[3] , tq1[3] , tr1[3] , tp2[3] , tq2[3] , tr2[3];

    convertir_point_en_tab (p1,tp1);
    convertir_point_en_tab (q1,tq1);
    convertir_point_en_tab (r1,tr1);
    convertir_point_en_tab (p2,tp2);
    convertir_point_en_tab (q2,tq2);
    convertir_point_en_tab (r2,tr2);

/* Il faut modifier l'excellente fonction de base pour faire 
   en sorte que les points communs et les arêtes communes 
   soient ignorées */

    return tri_tri_overlap_test_3d (tp1,tq1,tr1,tp2,tq2,tr2);
}

void affiche_intersection ( maillage m )
{
    int i; // Indice de parcours global des faces
    int j; // Indice de parcours local des faces

    const int N = nombre_de_faces ( m );

    for ( i = 0 ; i < N ; ++i )
    {
        face f = obtenir_face ( m , i );
        couleur c = extraire_couleur ( f );

        if ( c == NULL )
        {
            c = couleur_default();
            modif_face_couleur ( f , c );
        }

	c->r = 255;
        c->v = 255;
        c->b = 255;
    }

    for ( i = 0 ; i < N ; ++i )
    {
        face f = obtenir_face ( m , i );
        couleur cf = extraire_couleur ( f );

        for ( j = i + 1 ; j < N ; ++j )
        {
            face g = obtenir_face ( m , j );
            couleur cg = extraire_couleur ( g );

            if ( intersection ( extraire_point ( extraire_sommet ( f , 0 ) ),
                                extraire_point ( extraire_sommet ( f , 1 ) ),
                                extraire_point ( extraire_sommet ( f , 2 ) ),
                                extraire_point ( extraire_sommet ( g , 0 ) ),
                                extraire_point ( extraire_sommet ( g , 1 ) ),
                                extraire_point ( extraire_sommet ( g , 2 ) ) )
                 == 1 )
            {
	        // Couleur rouge pour les deux faces
	        cf->r = 255;
                cf->v = 0;
                cf->b = 0;

	        cg->r = 255;
                cg->v = 0;
                cg->b = 0;

                printf_ncurses ( "Les faces %d et %d se coupent.\n" , i , j );
            }
        }
    }
}
