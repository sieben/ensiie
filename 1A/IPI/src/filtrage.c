/**
 * @file   filtrage.c
 * @brief  Définition du module de filtrage d'un maillage.
 * @author Leone Rémy
 * @author Michel Teddy
 */

#include <stdio.h>
#include <time.h>

#include "filtrage.h"
#include "sommet.h"
#include "gestion_memoire.h"
#include "affichagencurses.h"


maillage filtre_uniforme ( maillage m ) 
{
    float f[3] = { 1 / 3.0f };

    clock_t tps = clock();
    maillage nm = filtre ( m , f , 3 );
    printf_ncurses ( "Application du filtre uniforme (%0.3f secondes).\n" ,
                     ( (float) clock() - tps ) / (float) CLOCKS_PER_SEC );

    return nm;
}


maillage filtre_normal ( maillage m ) 
{
    float f[3] = { 0.39894f , 0.353927f , 0.247133f };

    clock_t tps = clock();
    maillage nm = filtre ( m , f , 3 );
    printf_ncurses ( "Application du filtre gaussien (%0.3f secondes).\n" ,
                     ( (float) clock() - tps ) / (float) CLOCKS_PER_SEC );

    return nm;
}


maillage filtre ( maillage m , float filtre[] , size_t taille )
{
    // Création du nouveau maillage
    maillage res = new_maillage ( nom_maillage ( m ) );

    int i; // Indice pour parcourir la liste des sommets
    const int card = nombre_de_sommets ( m );

    // Pour chaque sommet
    for ( i = 0 ; i < card ; ++i )
    {
        int j; // Indice pour parcourir la liste des coefficients du filtre

        vecteur new_point = vecteur_nouveau ( 0.0f , 0.0f , 0.0f );
        vecteur new_normale = vecteur_nouveau ( 0.0f , 0.0f , 0.0f );

        // On récupère le tableau des voisins de niveau taille du sommet i
        tableau * voisins = sommets_voisins ( m , i , taille - 1 );

        // Pour chaque coefficient du filtre
        for ( j = 0 ; (size_t) j < taille ; ++j )
        {
            int k;

            // Si le coefficient du filtre est très petit, on ne fait rien
            if ( filtre[j] < 0.01f && filtre[j] > -0.01f )
            {
                continue;
            }

            // On récupère le tableau des voisins de niveau j
            int nbr_voisins = taille_tableau ( voisins[j] );

            // S'il n'y a pas de voisins, on passe à l'itération suivante
            if ( nbr_voisins == 0 )
            {
                continue;
            }

            // Vecteurs temporaires pour le point et la normale
            vecteur tmp_point = vecteur_nouveau ( 0.0f , 0.0f , 0.0f );
            vecteur tmp_normale = vecteur_nouveau ( 0.0f , 0.0f , 0.0f );

            // Calcul de la somme des voisins de niveau j
            for ( k = 0 ; k < nbr_voisins ; ++k )
            {
                sommet s = obtenir_sommet ( m , (int)
                            acceder_element ( voisins[j] , k , long ) );

                vecteur_add ( convertir_en_vecteur ( extraire_point ( s ) ) ,
                              tmp_point );

                vecteur_add ( extraire_normale ( s ) , tmp_normale );
            }

            // On multiplie les vecteurs temporaires par un coefficient
            vecteur_mult ( tmp_point , filtre[j] / nbr_voisins );
            vecteur_mult ( tmp_normale , filtre[j] / nbr_voisins );

            // On ajoute les vecteurs temporaires au nouveau point
            vecteur_add ( tmp_point , new_point );
            vecteur_add ( tmp_normale , new_normale );

            supprimer_vecteur ( tmp_point );
            supprimer_vecteur ( tmp_normale );
        }

        vecteur_normalise ( new_normale );

        // Suppression des tableaux de sommets voisins
        for ( j = 0 ; (size_t) j < taille ; ++j )
        {
            supprimer_tableau ( voisins[j] );
        }

        liberer_mem ( voisins );

        // Copie des coordonnées de texture du sommet
        texture t = extraire_texture ( obtenir_sommet ( m , i ) );
        texture nt = allouer_mem ( sizeof(struct t_texture) );

        if ( t )
        {
            nt->x = t->x;
            nt->y = t->y;
        }
        else
        {
            nt->x = 0.0f;
            nt->y = 0.0f;
        }

        // Création du nouveau sommet
        sommet r = new_sommet ( (point) new_point , new_normale , nt );

        // On ajoute le sommet au nouveau maillage
        ajouter_sommet ( res , r );
    }

    const int nbr_faces = nombre_de_faces ( m );

    // Copie des faces du maillage
    for ( i = 0 ; i < nbr_faces ; ++i )
    {
        face f = obtenir_face ( m , i );

        int i1 = indice_sommet ( m , extraire_sommet ( f , 0 ) );
        int i2 = indice_sommet ( m , extraire_sommet ( f , 1 ) );
        int i3 = indice_sommet ( m , extraire_sommet ( f , 2 ) );

        // Copie de la couleur de la face
        couleur c = couleur_default();
        couleur color = extraire_couleur ( f );

        if ( c && color )
        {
            c->r = color->r;
            c->v = color->v;
            c->b = color->b;
        }

        // Création de la nouvelle face
        face newface = new_face ( obtenir_sommet ( res , i1 ) ,
                                  obtenir_sommet ( res , i2 ) ,
                                  obtenir_sommet ( res , i3 ) ,
                                  NULL , c );

        ajouter_face ( res , newface );
    }

    return res;
}
