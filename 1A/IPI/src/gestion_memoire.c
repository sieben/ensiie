/**
 * @file gestion_memoire.c
 * @brief Implémentation du module de gestion de la mémoire et des tableaux
 * @author Lasnier Simon
 */

#include <stdio.h>
#include "gestion_memoire.h"

/*@{*/

/**
 * @addtogroup gestion_memoire
 */

/**
 * @struct t_tableau
 * Structure pour gérer les tableaux de manière plus propre, champs de la
 * structure :
 * 		- tab : pointeur sur le premier élement d'un tableau de void*
 *		- taille : entier contenant la taille du tableau
 */

struct t_tableau
{
    void** tab;
    int taille;
};

/*@}*/


#define	    BUFFER_MEM	    10


void* allouer_mem (size_t taille)
{
    void* retour = NULL;

    if (taille == 0)
    {
        fprintf(stderr, "allouer_mem : tentative avec 0 octets.\n");
        exit(EXIT_FAILURE);
    }

    retour = malloc(taille);
    if (retour == NULL)
    {
        fprintf(stderr, "allouer_mem : allocation impossible.\n");
        exit(EXIT_FAILURE);
    }

    return retour;
}


void* reallouer_mem (void *pointeur, size_t taille)
{
    if (taille == 0)
    {
        fprintf(stderr, "reallouer_mem : tentative avec 0 octets.\n");
        exit(EXIT_FAILURE);
    }

    void* retour = realloc(pointeur, taille);

    if (retour == NULL)
    {
        fprintf(stderr, "reallouer_mem : reallocation impossible.\n");
        exit(EXIT_FAILURE);
    }

    return retour;
}


void* allouer_et_init_mem (size_t nb_elts, size_t taille_elt)
{
    void* retour = NULL;

    if (nb_elts == 0 || taille_elt == 0)
    {
        fprintf(stderr, "allouer_et_init_mem : tentative avec 0 octets.\n");
        exit(EXIT_FAILURE);
    }

    retour = calloc(nb_elts, taille_elt);
    if (retour == NULL)
    {
        fprintf(stderr, "allouer_et_init_mem : allocation impossible.\n");
        exit(EXIT_FAILURE);
    }

    return retour;
}


tableau new_tableau ()
{
    tableau t = allouer_mem(sizeof(struct t_tableau));
    t->tab = NULL;
    t->taille = 0;
    return t;
}

int ajouter_element_aux (tableau t, void* element)
{
    if (t == NULL)
    {
        fprintf(stderr, "ajouter_element_aux : tableau est NULL.\n");
        exit(EXIT_FAILURE);
    }

    if (t->taille == 0)
        t->tab = allouer_mem(sizeof(void*) * ((size_t) BUFFER_MEM) );
    else if (t->taille % BUFFER_MEM == 0)
        t->tab = reallouer_mem(t->tab,
                sizeof(void*) * ((size_t)t->taille + BUFFER_MEM));

    t->tab[t->taille] = element;

    return ((t->taille)++);
}

void remplacer_element_aux (tableau t,int indice,void* nouveau)
{
    if (t == NULL)
    {
        fprintf(stderr, "remplacer_element_aux : tableau est NULL.\n");
        exit(EXIT_FAILURE);
    }

    if (indice < 0 || indice >= t->taille)
    {
        fprintf(stderr, "remplacer_element_aux : l'indice %d est incorrect.\n",
                indice);
        exit(EXIT_FAILURE);
    }

    t->tab[indice] = nouveau;
}

void supprimer_element (tableau t, int indice)
{
    int i = 0;

    if (t == NULL)
    {
        fprintf(stderr, "supprimer_element : tableau est NULL.\n");
        exit(EXIT_FAILURE);
    }

    if (indice < 0 || indice >= t->taille)
    {
        fprintf(stderr, "supprimer_element : l'indice %d est incorrect.\n",
                indice);
        exit(EXIT_FAILURE);
    }

    for (i = indice ; i < t->taille-1 ; i++)
        t->tab[i] = t->tab[i+1];

    (t->taille)--;

    if (t->taille == 0)
        liberer_mem(t->tab);
    else if (t->taille % BUFFER_MEM == 0)
        t->tab = reallouer_mem(t->tab, sizeof(void*) * ((size_t)t->taille));
}


void* acceder_element_aux (const tableau t, int indice)
{
    if (t == NULL)
    {
        fprintf(stderr, "acceder_element_aux : tableau est NULL.\n");
        exit(EXIT_FAILURE);
    }

    if (indice < 0 || indice >= t->taille)
    {
        fprintf(stderr, "acceder_element_aux : l'indice %d est ", indice);
        fprintf(stderr, "negatif ou supérieur à la taille du tableau.\n");
        exit(EXIT_FAILURE);
    }

    return t->tab[indice];
}

tableau supprime_elements ( tableau t1 , const tableau t2 )
{
    if ( t1 == NULL ) return NULL;

    tableau nouveau_t = new_tableau();
    int i;

    // Pour chaque case du tableau t1
    for ( i = 0 ; i < t1->taille ; ++i )
    {
        // Si l'élément n'est pas présent dans t2, on le garde
        if (t2 == NULL || recherche_tableau ( t2 , t1->tab[i] ) == -1)
            ajouter_element ( nouveau_t , t1->tab[i] );
    }

    supprimer_tableau ( t1 );
    return nouveau_t;
}

void fusionne_tableaux ( tableau t1 , const tableau t2 )
{
    if ( t1 == NULL || t2 == NULL )
        return;

    int i;

    // Pour chaque élément de t2
    for ( i = 0 ; i < t2->taille ; ++i )
    {
        ajouter_element ( t1 , t2->tab[i] );
    }

    return;
}

tableau supprime_doublons ( tableau t )
{
    tableau nouveau_t = new_tableau();

    if ( t == NULL || t->taille == 0 )
    {
        liberer_mem ( t );
        return nouveau_t;
    }

    int i;

    // Pour chaque case du tableau
    for ( i = 0 ; i < t->taille ; ++i )
    {
        // On recherche l'élément dans le nouveau tableau
        if ( recherche_tableau_aux ( nouveau_t , t->tab[i] ) == -1 )
        {
            ajouter_element ( nouveau_t , t->tab[i] );
        }
    }

    liberer_mem ( t );
    return nouveau_t;
}


int taille_tableau (const tableau t)
{
    if (t == NULL)
    {
        fprintf(stderr, "taille_tableau : tableau est NULL.\n");
        exit(EXIT_FAILURE);
    }

    return t->taille;
}

int recherche_tableau_aux (const tableau t, void* element)
{
    int i = 0;

    if (t == NULL)
    {
        fprintf(stderr, "recherche_tableau_aux : tableau est NULL.\n");
        exit(EXIT_FAILURE);
    }

    for (i = 0 ; i < t->taille ; i++)
        if (t->tab[i] == element)
            return i;

    return -1;
}

void vider_tableau (tableau t)
{
    if (t == NULL)
    {
        fprintf(stderr, "vider_tableau : tableau est NULL.\n");
        exit(EXIT_FAILURE);
    }

    if (t->tab != NULL)
    {
        liberer_mem(t->tab);
        t->tab = NULL;
    }

    t->taille = 0;
}

void supprimer_tableau (tableau t)
{
    if (t != NULL)
    {
        vider_tableau(t);
        liberer_mem(t);
    }
}

/*@{*/

/**
 * @brief fonction auxilliaire de trier_tableau_entiers (pour l'appel de qsort)
 * @param m pointeur vers un entier
 * @param n pointeur vers un entier
 * @pre aucune
 * @return un entier dont le signe est celui de *m-*n
 */

int compare_trier_tableau_entiers(const void *m,const void *n)
{
    return(*((int *)m)-*((int *)n));
}

/*@}*/

void trier_tableau_entiers(tableau t)
{
    qsort(&(t->tab[0]),t->taille,sizeof(void *),compare_trier_tableau_entiers);
}

