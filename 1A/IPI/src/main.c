/**
 * @file   main.c
 * @brief  Fichier principal, qui utilise tous les modules.
 * @author Anonyme
 */

#include <stdio.h>
#include <stdlib.h>
#include "affichagencurses.h"
#include <string.h>
#include <unistd.h>

int main ( int argc , char **argv )
{
    int error;

    if( argc > 2 )
    {
        if( strcmp( argv[2] , "-X" ) == 0 )
        {
            affiche_init(argc, argv);
            affiche_boucle();
        }
    }
    else if( argc == 2 && strcmp( argv[1] , "-X" ) == 0 )
    {
        char* new_argv[1];
        new_argv[0]=argv[0];
        affiche_init(1, new_argv);
        affiche_boucle();
    }
    else
    {
        error=main_ncurses(argc, argv);
    }

    return error;
}

/*
int main(int argc, char *argv[])
       {
           int teddy, opt;
           
           nsecs = 0;
           tfnd = 0;
           flags = 0;
 
           while ((opt = getopt(argc, argv, "X:")) != -1) {
               switch (opt) {
               
               case 'X':
            
            teddy = 1;
               affiche_init(argc, argv);
            affiche_boucle();
            affiche_exit();
            
                   break;
            
               default: 
               
               error=main_ncurses(argc, argv); 
               }
           }

           printf("flags=%d; tfnd=%d; optind=%d\n", flags, tfnd, optind);

}
*/
