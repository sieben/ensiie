/**
 * @file   subdivision.c
 * @brief  Implantation du module subdivision.
 * @author Lasnier Simon
 * @author Zhong Chaoran
 */

#include <time.h>
#include <stdbool.h>

#include "subdivision.h"
#include "affichagencurses.h"
#include "maillages.h"
#include "face.h"
#include "sommet.h"
#include "vecteur.h"
#include "detection_correction.h"


sommet recherche_sommet ( maillage m , sommet s )
{
    int i;
    int nbr_sommets = nombre_de_sommets ( m );

    for ( i = 0 ; i < nbr_sommets ; ++i )
    {
        sommet ns = obtenir_sommet ( m , i );
        if ( comp_sommet ( ns , s ) ) return ns;
    }

    return NULL;
}


maillage subdivision_plane ( maillage m )
{
    clock_t tps = clock();

    maillage mm = new_maillage ( nom_maillage ( m ) );
    int i;
    const int nbr_faces = nombre_de_faces ( m );

    // On parcourt le tableau des faces
    for ( i = 0 ; i < nbr_faces ; ++i )
    {
        face f = obtenir_face ( m , i );

        // Copie des sommets du maillage
        sommet s1 = copie_sommet ( extraire_sommet ( f , 0 ) );
        sommet s2 = copie_sommet ( extraire_sommet ( f , 1 ) );
        sommet s3 = copie_sommet ( extraire_sommet ( f , 2 ) );

	sommet s = NULL;

        // Ajout des anciens sommets au nouveau maillage
        if ( ( s = recherche_sommet ( mm , s1 ) ) )
	{
	    liberer_mem_sommet ( s1 );
	    s1 = s;
	}
	else ajouter_sommet ( mm , s1 );

	if ( ( s = recherche_sommet ( mm , s2 ) ) )
	{
	    liberer_mem_sommet ( s2 );
	    s2 = s;
	}
	else ajouter_sommet ( mm , s2 );

	if ( ( s = recherche_sommet ( mm , s3 ) ) )
	{
	    liberer_mem_sommet ( s3 );
	    s3 = s;
	}
	else ajouter_sommet ( mm , s3 );

        // Recherche des milieux des arêtes
        sommet s12 = moitie_cote ( s1 , s2 );
        sommet s23 = moitie_cote ( s3 , s2 );
        sommet s13 = moitie_cote ( s1 , s3 );

        // Ajout des nouveaux sommets au nouveau maillage
        if ( ( s = recherche_sommet ( mm , s12 ) ) )
	{
	    liberer_mem_sommet ( s12 );
	    s12 = s;
	}
	else ajouter_sommet ( mm , s12 );

        if ( ( s = recherche_sommet ( mm , s23 ) ) )
	{
	    liberer_mem_sommet ( s23 );
	    s23 = s;
	}
	else ajouter_sommet ( mm , s23 );

        if ( ( s = recherche_sommet ( mm , s13 ) ) )
	{
	    liberer_mem_sommet ( s13 );
	    s13 = s;
	}
	else ajouter_sommet ( mm , s13 );

        // Copie de la couleur de la face
        couleur c1 = couleur_default();
        couleur c2 = couleur_default();
        couleur c3 = couleur_default();
        couleur c4 = couleur_default();

        couleur color = extraire_couleur ( f );

        if ( color )
        {
            c1->r = color->r;
            c1->v = color->v;
            c1->b = color->b;

            c2->r = color->r;
            c2->v = color->v;
            c2->b = color->b;

            c3->r = color->r;
            c3->v = color->v;
            c3->b = color->b;

            c4->r = color->r;
            c4->v = color->v;
            c4->b = color->b;
        }
        
        // Ajout des faces au nouveau maillage
        ajouter_face ( mm , new_face ( s1, s12, s13, NULL , c1 ) );
        ajouter_face ( mm , new_face ( s12, s2, s23, NULL , c2 ) );
        ajouter_face ( mm , new_face ( s12, s23, s13, NULL , c3 ) );
        ajouter_face ( mm , new_face ( s13, s23, s3, NULL , c4 ) );
    }

    printf_ncurses ( "Subdivision plane (%0.3f secondes).\n" ,
                     ( (float) clock() - tps ) / (float) CLOCKS_PER_SEC );

    return mm;
}


maillage subdivision_normale ( maillage m )
{
    // Nouveau maillage vide
    maillage mm = subdivision_plane ( m );

    // Parcourir les sommets du maillage m
    int i;
    const int nbr_sommets_m = nombre_de_sommets ( m );
    const int nbr_sommets_mm = nombre_de_sommets ( mm );

    // On parcourt le tableau des sommets
    for ( i = 0 ; i < nbr_sommets_mm ; ++i )
    {
        tableau * t = sommets_voisins ( mm , i , 1 );
	int j;
	vecteur v_somme = vecteur_nouveau ( 0.0f , 0.0f , 0.0f );
	int taille = taille_tableau ( t[1] );
	  
	for ( j = 0 ; j < taille ; ++j )
	{
	    vecteur v = convertir_en_vecteur ( extraire_point (
                            obtenir_sommet ( mm , (unsigned int)
                            acceder_element ( t[1] , j , long ) ) ) );

            vecteur_diff ( convertir_en_vecteur ( extraire_point (
                           obtenir_sommet ( mm , i ) ) ) , v );

            vecteur_add ( v , v_somme );
            supprimer_vecteur ( v );
        }

	vecteur_mult ( v_somme , 0.68 / (taille-1.7) );
	  
	vecteur v_sommet_point_new = vecteur_copie ( convertir_en_vecteur (
                              extraire_point ( obtenir_sommet ( mm , i ) ) ) );
        vecteur_add ( v_somme , v_sommet_point_new );

	supprimer_vecteur ( v_somme );

	point ps = extraire_point(obtenir_sommet(mm,i));

	modif_point(ps,extraire_composante(v_sommet_point_new,0),
		    extraire_composante(v_sommet_point_new,1),
		    extraire_composante(v_sommet_point_new,2));

        supprimer_tableau ( t[0] );
        supprimer_tableau ( t[1] );
        liberer_mem ( t );
	
    }

    return mm;
}


maillage subdivision_normale_bis ( maillage m )
{
    clock_t tps = clock();

    // Nouveau maillage vide
    maillage mm = new_maillage ( nom_maillage ( m ) );
    int i;
    const int nbr_faces = nombre_de_faces ( m );
    const int nbr_sommets = nombre_de_sommets ( m );

    for ( i = 0 ; i<nbr_sommets; ++i )
    {
	calculer_normale_sommet (m,i);
    }

    // On parcourt le tableau des faces
    for ( i = 0 ; i < nbr_faces ; ++i )
    {
        face f = obtenir_face ( m , i );

        // Copie des sommets du maillage
        sommet s1 = copie_sommet ( extraire_sommet ( f , 0 ) );
        sommet s2 = copie_sommet ( extraire_sommet ( f , 1 ) );
        sommet s3 = copie_sommet ( extraire_sommet ( f , 2 ) );

	sommet s = NULL;

        // Ajout des anciens sommets au nouveau maillage
        if ( ( s = recherche_sommet ( mm , s1 ) ) )
	{
	    liberer_mem_sommet ( s1 );
	    s1 = s;
	}
	else ajouter_sommet ( mm , s1 );

	if ( ( s = recherche_sommet ( mm , s2 ) ) )
	{
	    liberer_mem_sommet ( s2 );
	    s2 = s;
	}
	else ajouter_sommet ( mm , s2 );

	if ( ( s = recherche_sommet ( mm , s3 ) ) )
	{
	    liberer_mem_sommet ( s3 );
	    s3 = s;
	}
	else ajouter_sommet ( mm , s3 );

        // Recherche des milieux des arêtes
        sommet s12 = moitie_cote ( s1 , s2 );
        sommet s23 = moitie_cote ( s3 , s2 );
        sommet s13 = moitie_cote ( s1 , s3 );

        // Ajout des nouveaux sommets au nouveau maillage
        if ( ( s = recherche_sommet ( mm , s12 ) ) )
	{
	    liberer_mem_sommet ( s12 );
	    s12 = s;
	}
	else ajouter_sommet ( mm , s12 );

        if ( ( s = recherche_sommet ( mm , s23 ) ) )
	{
	    liberer_mem_sommet ( s23 );
	    s23 = s;
	}
	else ajouter_sommet ( mm , s23 );

        if ( ( s = recherche_sommet ( mm , s13 ) ) )
	{
	    liberer_mem_sommet ( s13 );
	    s13 = s;
	}
	else ajouter_sommet ( mm , s13 );


        // Vecteurs normaux des trois sommets
        vecteur n1 = extraire_normale ( s1 );
        vecteur n2 = extraire_normale ( s2 );
        vecteur n3 = extraire_normale ( s3 );

        // Modification des coordonnées du sommet s12
        {
            point p = extraire_point ( s12 );

            vecteur tmp = vecteur_copie ( n1 );
            vecteur_add ( n2 , tmp );
            vecteur_mult ( tmp , distance_point ( extraire_point(s1) ,
                                 extraire_point(s2) ) * 0.05f );

            vecteur tmp2 = convertir_en_vecteur ( p );
            vecteur_add ( tmp2 , tmp );
            supprimer_vecteur ( tmp2 );

            modif_point ( p , extraire_composante ( tmp , 0 ) ,
                              extraire_composante ( tmp , 1 ) ,
                              extraire_composante ( tmp , 2 ) );

            supprimer_vecteur ( tmp );
        }

        // Modification des coordonnées du sommet s23
        {
            point p = extraire_point ( s23 );

            vecteur tmp = vecteur_copie ( n2 );
            vecteur_add ( n3 , tmp );
            vecteur_mult ( tmp , distance_point ( extraire_point(s2) ,
                                 extraire_point(s3) ) * 0.05f );

            vecteur tmp2 = convertir_en_vecteur ( p );
            vecteur_add ( tmp2 , tmp );
            supprimer_vecteur ( tmp2 );

            modif_point ( p , extraire_composante ( tmp , 0 ) ,
                              extraire_composante ( tmp , 1 ) ,
                              extraire_composante ( tmp , 2 ) );

            supprimer_vecteur ( tmp );
        }


        // Modification des coordonnées du sommet s13
        {
            point p = extraire_point ( s13 );

            vecteur tmp = vecteur_copie ( n1 );
            vecteur_add ( n3 , tmp );
            vecteur_mult ( tmp , distance_point ( extraire_point(s1) ,
                                 extraire_point(s3) ) * 0.05f );

            vecteur tmp2 = convertir_en_vecteur ( p );
            vecteur_add ( tmp2 , tmp );
            supprimer_vecteur ( tmp2 );

            modif_point ( p , extraire_composante ( tmp , 0 ) ,
                              extraire_composante ( tmp , 1 ) ,
                              extraire_composante ( tmp , 2 ) );

            supprimer_vecteur ( tmp );
        }


        // Copie de la couleur de la face
        couleur c1 = couleur_default();
        couleur c2 = couleur_default();
        couleur c3 = couleur_default();
        couleur c4 = couleur_default();

        couleur color = extraire_couleur ( f );

        if ( color )
        {
            c1->r = color->r;
            c1->v = color->v;
            c1->b = color->b;

            c2->r = color->r;
            c2->v = color->v;
            c2->b = color->b;

            c3->r = color->r;
            c3->v = color->v;
            c3->b = color->b;

            c4->r = color->r;
            c4->v = color->v;
            c4->b = color->b;
        }

        // Ajout des faces au nouveau maillage
        ajouter_face ( mm , new_face ( s1, s12, s13, NULL , c1 ) );
        ajouter_face ( mm , new_face ( s12, s2, s23, NULL , c2 ) );
        ajouter_face ( mm , new_face ( s12, s23, s13, NULL , c3 ) );
        ajouter_face ( mm , new_face ( s13, s23, s3, NULL , c4 ) );
    }

    printf_ncurses ( "Subdivision normale (%0.3f secondes).\n" ,
                     ( (float) clock() - tps ) / (float) CLOCKS_PER_SEC );

    return mm;
}
