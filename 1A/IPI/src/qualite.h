/**
 * @file qualite.h
 * @brief Module contenant les traitements liés aux calculs de la qualité
 * la manipulation des faces.
 * @author Iraqi Sarah
 * @author Crémèse Jean
 */

#ifndef QUALITE_H
#define QUALITE_H

#include "maillages.h"

/* ------------------------------------------------------------------------ */
/* Définition des traitements sur ces données                               */
/* ------------------------------------------------------------------------ */


/**
 * Calcul de la qualité d'une face en fonction de la distance par 
 * rapport à un point. On calcule la plus petite distance entre 
 * la face et le point.
 *
 * @param f la face dont on souhaite calculer la qualité
 * @param p le point de référence
 * @pre la face est bien formée 
 * @pre le point est bien formé
 * @return une valeur positive
 * @test calcul de la qualité d'une face bien formée et d'un point
 *      contenu dans la face
 * @test calcul de la qualité d'une et d'un point 
 *      quelconque
 */

float distance_point_quali(face f, point p);


/**
 * Calcul de la qualité d'une face en fonction de la distance par 
 * rapport à un axe. On calcule la plus petite distance entre la 
 * face et l'axe.
 *
 * @param f la face dont on souhaite calculer la qualité
 * @param p le point de référence de l'axe
 * @param v le vecteur définissant l'axe
 * @pre la face est bien formée
 * @pre le vecteur est bien formé
 * @pre le point est bien formé
 * @return une valeur positive
 * @test calcul de la qualité d'une face par rapport à un axe qui intersecte la
 *       face en un point
 * @test calcul de la qualité d'une face par rapport à un axe confondu
 * @test calcul de la qualité d'une face bien formée par rapport à un
 *      axe bien formé quelconque
 */

float distance_axe_quali(face f, point p, vecteur v);

/**
 * Calcul de la qualité d'une face en fonction de la distance par 
 * rapport à un plan définis par un point et deux vecteurs.
 * On calcule la plus petite distance entre la face et le plan.
 *
 * @param f la face dont on souhaite calculer la qualité
 * @param p le point de référence du plan
 * @param v1 le premier vecteur définissant le plan
 * @param v2 le second vecteur définissant le plan
 * @pre la face est bien formée
 * @pre le point est bien formé
 * @pre les deux vecteurs sont bien formés
 * @return une valeur positive
 * @test calcul la qualité d'une face par rapport à un
 *      plan qui contient la face
 * @test calcul la qualité d'une face par rapport à un
 *      plan quelconque */

float distance_plan_quali(face f, point p, vecteur v1,vecteur v2);


/**
 * Calcul de la qualité d'une face à par le facteur de qualité.
 *
 * @param f la face dont on souhaite calculer la qualité
 * @pre la face est bien formée
 * @return une valeur positive
 * @test calcul la qualité d'une face.
 */

float facteur_quali(face f);


/**
 * Calcul de la qualité d'une face en fonction de la courbure locale du 
 * maillage.
 *
 * On calcule la courbure à l'aide des normales des faces 
 * adjacentes : on détermine l'angle entre la normale de la face 
 * passée en argument et celles de ses faces voisines. On calcule
 * un facteur de qualité à par les trois rayons de courbures obtenus.
 *
 * @param indice l'indice de la face dont on souhaite calculer la qualité
 * @param m le maillage qui contient la face 
 * @pre le maillage est bien formé
 * @pre l'indice est cohérent avec le maillage
 * @return une valeur positive
 * @test calcul la qualité d'une face d'un maillage.
 */

float courbure_quali(int indice, maillage m);


/**
 * Colore un maillage en blanct.
 *
 * @param m Maillage à colorier.
 */

void coloration_normale ( maillage m );


/**
 * Colore un maillage en utilisant la distance par rapport à un point.
 *
 * @param m Maillage à colorier.
 * @param p Point de référence.
 */

void coloration_distance_point ( maillage m , point p );


/**
 * Colore un maillage en utilisant la distance par rapport à un axe.
 *
 * @param m Maillage à colorier.
 * @param p Point de référence.
 * @param v Vecteur définissant l'axe.
 */

void coloration_distance_axe ( maillage m , point p , vecteur v );


/**
 * Colore un maillage en utilisant la distance par rapport à un plan.
 *
 * @param m Maillage à colorier.
 * @param p Point de référence.
 * @param v1 Premier vecteur définissant le plan.
 * @param v2 Second vecteur définissant le plan.
 */

void coloration_distance_plan ( maillage m , point p , vecteur v1 , vecteur v2 );


/**
 * Colore un maillage en utilisant le facteur de qualité.
 *
 * @param m Maillage à colorier.
 */

void coloration_facteur_qualite ( maillage m );


/**
 * Colore un maillage en utilisant la courbure locale.
 *
 * @param m Maillage à colorier.
 */

void coloration_courbure ( maillage m );

/*@}*/

#endif /* fin de ifndef QUALITE_H */
