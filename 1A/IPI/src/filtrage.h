/**
 * @file filtrage.h
 * @brief Implantation du module de filtrage d'un maillage.
 * @author Rémy Leone
 * @author Teddy Michel
 */

#ifndef FICHIER_FILTRAGE_H
#define FICHIER_FILTRAGE_H

#include "maillages.h"

/**
 * @defgroup filtrage Filtrage
 * @brief    Filtrage d'un maillage.
 * @author   Rémy Leone
 * @author   Teddy Michel
 * @date     23 mars 2010
 */

/*@{*/

/**
 * Filtre un maillage avec un filtre donné.
 *
 * @param  m Maillage à filtrer.
 * @param  filtre Tableau de coefficients du filtre.
 * @param  taille Taille du tableau.
 * @return Nouveau maillage filtré.
 *
 * @pre    Le maillage m est défini.
 * @post   Donne un maillage filtré.
 *
 * @test   Tester avec un maillage corrigé.
 * @test   Tester avec un maillage contenant des trous.
 * @test   Tester avec un maillage avec une paroi fine.
 */

maillage filtre ( maillage m , float filtre[] , size_t taille );

/**
 * Filtre un maillage avec un filtre uniforme.
 *
 * @param  m maillage à filtrer.
 * @return Nouveau maillage filtré uniformement.
 *
 * @pre    Le maillage m est défini.
 * @post   Donne un maillage filtré de manière uniforme.
 * 
 * @test   Voir les tests de la fonction filtre.
 */

maillage filtre_uniforme ( maillage m );

/**
 * Filtre un maillage avec un filtre normal.
 *
 * @param  m maillage à filtrer.
 * @return Nouveau maillage filtré normalement.
 *
 * @pre    Le maillage m est défini.
 * @post   Donne un maillage filtré de manière normale.
 * 
 * @test   Voir les tests de la fonction filtre.
 */

maillage filtre_normal ( maillage m );

/*@}*/

#endif
