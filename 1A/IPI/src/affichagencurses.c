/**
* @file affichagencurses.c
* @brief Aspect graphique du programme en mode console
* @author Vatin Charles
*/

#define DATA_DIR "obj/smooth/"

#include "affichagencurses.h"

/**
 * @addtogroup affichagencurses
 */

bool interface_glut = true;
bool interface_ncurses = false;
pthread_t th = 0;

void printf_ncurses( const char * texte , ... )
{
    if ( interface_glut == false )
    {
        va_list args;
        va_start ( args , texte );

        char buffer[512];
        va_list params;
        va_start ( params , texte );
        vsprintf ( buffer , texte , params );
        va_end ( params );

        wprintw ( msgbarre, buffer );
        wrefresh ( msgbarre );
    }
    else if ( !interface_ncurses )
    {
        va_list args;
        va_start ( args , texte );

        char buffer[512];
        va_list params;
        va_start ( params , texte );
        vsprintf ( buffer , texte , params );
        va_end ( params );

	printf ( "%s" , buffer );
    }

    return;
}

void init_prog()
{
    initscr();
    start_color();
    init_pair(1,COLOR_GREEN,COLOR_BLACK);
    init_pair(2,COLOR_RED,COLOR_WHITE);
    init_pair(3,COLOR_WHITE,COLOR_BLUE);
    init_pair(4,COLOR_CYAN,COLOR_WHITE);
    curs_set(0);
    keypad(stdscr,TRUE);
    cbreak();
    bkgd(COLOR_PAIR(3));
    noecho();
}

void draw_menubarre(WINDOW* menubarre)
{
    wbkgd(menubarre,COLOR_PAIR(4));

    wmove(menubarre, 0, 1);
    wprintw(menubarre, "Fichier");
    wattron(menubarre, COLOR_PAIR(2));
    wprintw(menubarre, "(F2)");
    wattroff(menubarre, COLOR_PAIR(2));

    wmove(menubarre, 0, 14);
    wprintw(menubarre, "Corrections");
    wattron(menubarre, COLOR_PAIR(2));
    wprintw(menubarre, "(F3)");
    wattroff(menubarre, COLOR_PAIR(2));

    wmove(menubarre, 0, 31);
    wprintw(menubarre, "Filtrages");
    wattron(menubarre, COLOR_PAIR(2));
    wprintw(menubarre, "(F4)");
    wattroff(menubarre, COLOR_PAIR(2));

    wmove(menubarre, 0, 46);
    wprintw(menubarre, "Subdivisions");
    wattron(menubarre, COLOR_PAIR(2));
    wprintw(menubarre, "(F5)");
    wattroff(menubarre, COLOR_PAIR(2));

    //wmove(menubarre, 0, 64);
    //wprintw(menubarre, "Qualite");
    //wattron(menubarre, COLOR_PAIR(2));
    //wprintw(menubarre, "(F6)");
    //wattroff(menubarre, COLOR_PAIR(2));

    wrefresh(menubarre);
}

void draw_fichiermenu(WINDOW* fichiermenu, int selection)
{
    touchwin(stdscr);
    refresh();
    fichiermenu=newwin(5, 13, 1, 0);
    wbkgd(fichiermenu,COLOR_PAIR(4));
    wmove(fichiermenu, 1, 1);
    if (selection==1) wattron(fichiermenu, A_REVERSE);
    wprintw(fichiermenu,"Charger\n");
    wattroff(fichiermenu, A_REVERSE);
    wmove(fichiermenu, 2, 1);
    if (selection==2) wattron(fichiermenu, A_REVERSE);
    wprintw(fichiermenu,"Enregistrer\n");
    wattroff(fichiermenu, A_REVERSE);
    wmove(fichiermenu, 3, 1);
    if (selection==3) wattron(fichiermenu, A_REVERSE);
    wprintw(fichiermenu,"Afficher\n");

    wattroff(fichiermenu, A_REVERSE);
    wmove(fichiermenu, 4, 1);
    wrefresh(fichiermenu);
}

void draw_correctionsmenu(WINDOW* correctionsmenu, int selection)
{
    touchwin(stdscr);
    refresh();
    correctionsmenu=newwin(12, 24, 1, 13);
    wbkgd(correctionsmenu,COLOR_PAIR(4));
    wmove(correctionsmenu, 1, 1); 
    if (selection==1) wattron(correctionsmenu, A_REVERSE);
    wprintw(correctionsmenu,"Sommets dupliques\n");
    wattroff(correctionsmenu, A_REVERSE);
    wmove(correctionsmenu, 2, 1); 
    if (selection==2) wattron(correctionsmenu, A_REVERSE);
    wprintw(correctionsmenu,"Sommets non ref\n");
    wattroff(correctionsmenu, A_REVERSE);
    wmove(correctionsmenu, 3, 1); 
    if (selection==3) wattron(correctionsmenu, A_REVERSE);
    wprintw(correctionsmenu,"Faces dupliquees\n");
    wattroff(correctionsmenu, A_REVERSE);
    wmove(correctionsmenu, 4, 1); 
    if (selection==4) wattron(correctionsmenu, A_REVERSE);
    wprintw(correctionsmenu,"Faces invalides\n");
    wattroff(correctionsmenu, A_REVERSE);
    wmove(correctionsmenu, 5, 1); 
    if (selection==5) wattron(correctionsmenu, A_REVERSE);
    wprintw(correctionsmenu,"Sommets nonmanifold\n");
    wattroff(correctionsmenu, A_REVERSE);
    wmove(correctionsmenu, 6, 1); 
    if (selection==6) wattron(correctionsmenu, A_REVERSE);
    wprintw(correctionsmenu,"Faces mal orientees\n");
    wattroff(correctionsmenu, A_REVERSE);
    wmove(correctionsmenu, 7, 1); 
    if (selection==7) wattron(correctionsmenu, A_REVERSE);
    wprintw(correctionsmenu,"Faces degenerees\n");
    wattroff(correctionsmenu, A_REVERSE);
    wmove(correctionsmenu, 8, 1); 
    if (selection==8) wattron(correctionsmenu, A_REVERSE);
    wprintw(correctionsmenu,"Faces intersectes\n");
    wattroff(correctionsmenu, A_REVERSE);
    wmove(correctionsmenu, 9, 1); 
    if (selection==9) wattron(correctionsmenu, A_REVERSE);
    wprintw(correctionsmenu,"Faces manquantes\n");
    wattroff(correctionsmenu, A_REVERSE);
    wmove(correctionsmenu, 10, 1);
    if (selection==10) wattron(correctionsmenu, A_REVERSE);
    wprintw(correctionsmenu,"Toutes les corrections\n");
    wattroff(correctionsmenu, A_REVERSE);
    wrefresh(correctionsmenu);
}

void draw_filtragesmenu(WINDOW* filtragesmenu, int selection)
{
    touchwin(stdscr);
    refresh();
    filtragesmenu=newwin(4, 17, 1, 30);
    wbkgd(filtragesmenu,COLOR_PAIR(4));
    wmove(filtragesmenu, 1, 1); 
    if (selection==1) wattron(filtragesmenu, A_REVERSE);
    wprintw(filtragesmenu,"Filtre uniforme\n");
    wattroff(filtragesmenu, A_REVERSE);
    wmove(filtragesmenu, 2, 1); 
    if (selection==2) wattron(filtragesmenu, A_REVERSE);
    wprintw(filtragesmenu,"Filtre normale\n");
    wattroff(filtragesmenu, A_REVERSE);
    wrefresh(filtragesmenu);
}

void draw_subdivisionsmenu(WINDOW* subdivisionsmenu, int selection)
{
    touchwin(stdscr);
    refresh();
    subdivisionsmenu=newwin(4, 21, 1, 45);
    wbkgd(subdivisionsmenu,COLOR_PAIR(4));
    wmove(subdivisionsmenu, 1, 1); 
    if (selection==1) wattron(subdivisionsmenu, A_REVERSE);
    wprintw(subdivisionsmenu,"Subdivision plane\n");
    wattroff(subdivisionsmenu, A_REVERSE);
    wmove(subdivisionsmenu, 2, 1); 
    if (selection==2) wattron(subdivisionsmenu, A_REVERSE);
    wprintw(subdivisionsmenu,"Subdivision normale\n");
    wattroff(subdivisionsmenu, A_REVERSE);
    wrefresh(subdivisionsmenu);
}

void draw_qualitemenu(WINDOW* qualitemenu, int selection)
{
    touchwin(stdscr);
    refresh();
    qualitemenu=newwin(4, 17, 1, 63);
    wbkgd(qualitemenu,COLOR_PAIR(4));
    wmove(qualitemenu, 1, 1); 
    if (selection==1) wattron(qualitemenu, A_REVERSE);
    wprintw(qualitemenu,"Coloriage\n");
    wattroff(qualitemenu, A_REVERSE);
    wmove(qualitemenu, 2, 1); 
    if (selection==2) wattron(qualitemenu, A_REVERSE);
    wprintw(qualitemenu,"etc...\n");
    wattroff(qualitemenu, A_REVERSE);
    wrefresh(qualitemenu);
}

int action_menu(int type)
{
    WINDOW* typemenu=NULL;
    int key=0;
    int selection=1;

    while(key!=27)
    {
        if (type==2)
        {
            draw_fichiermenu(typemenu,selection);
            key=getch();
            switch(key)
            {
                case KEY_RIGHT:
                case KEY_F(3):
                selection=1;
                type=3;
                break;
                case KEY_F(4):
                selection=1;
                type=4;
                break;
                case KEY_F(5):
                selection=1;
                type=5;
                break;
                //case KEY_F(6):
                //selection=1;
                //type=6;
                //break;
                case KEY_DOWN:
                if (++selection==4) selection=1;
                break;
                case KEY_UP:
                if (--selection==0) selection=3;
                    break;
                case 10:
		    delwin(typemenu);
		    touchwin(stdscr);
		    wrefresh(stdscr);
                return (10+selection);
                break;
                case KEY_F(2):
		    delwin(typemenu);
		    touchwin(stdscr);
		    wrefresh(stdscr);
                return 0;
                break;
                case 27:
		    delwin(typemenu);
		    touchwin(stdscr);
		    wrefresh(stdscr);
		    return 0;
		    break;
            }
        }
        else if (type==3)
        {
            draw_correctionsmenu(typemenu,selection);
            key=getch();
            switch(key)
            {
                case KEY_LEFT:
                case KEY_F(2):
                    selection=1;
                    type=2;
                    break;
                case KEY_RIGHT:
                case KEY_F(4):
                    selection=1;
                    type=4;
                    break;
                case KEY_F(5):
                    selection=1;
                    type=5;
                    break;
                //case KEY_F(6):
                //    selection=1;
                //    type=6;
                //    break;
                case KEY_DOWN:
                    if (++selection==11) selection=1;
                    break;
                case KEY_UP:
                    if (--selection==0) selection=10;
                    break;
                case 10:
                    delwin(typemenu);
                    touchwin(stdscr);
                    wrefresh(stdscr);
                    return (20+selection);
                    break;
                case KEY_F(3):
                    delwin(typemenu);
                    touchwin(stdscr);
                    wrefresh(stdscr);
                    return 0;
                    break;
                case 27:
		    delwin(typemenu);
		    touchwin(stdscr);
		    wrefresh(stdscr);
		    return 0;
		    break;
            }
        }
        else if (type==4)
        {
            draw_filtragesmenu(typemenu,selection);
            key=getch();
            switch(key)
            {
                case KEY_F(2):
                    selection=1;
                    type=2;
                    break;
                case KEY_LEFT:
                case KEY_F(3):
                    selection=1;
                    type=3;
                    break;
                case KEY_RIGHT:
                case KEY_F(5):
                    selection=1;
                    type=5;
                    break;
                //case KEY_F(6):
                //    selection=1;
                //    type=6;
                //    break;
                case KEY_DOWN:
                    if (++selection==3) selection=1;
                    break;
                case KEY_UP:
                    if (--selection==0) selection=2;
                    break;
                case 10:
                    delwin(typemenu);
                    touchwin(stdscr);
                    wrefresh(stdscr);
                    return (30+selection);
                    break;
                case KEY_F(4):
                    delwin(typemenu);
                    touchwin(stdscr);
                    wrefresh(stdscr);
                    return 0;
                    break;
                case 27:
		    delwin(typemenu);
		    touchwin(stdscr);
		    wrefresh(stdscr);
		    return 0;
		    break;
                }
        }
        else if (type==5)
        {
            draw_subdivisionsmenu(typemenu,selection);
            key=getch();
            switch(key)
            {
                case KEY_F(2):
                    selection=1;
                    type=2;
                break;
                case KEY_F(3):
                    selection=1;
                    type=3;
                    break;
                case KEY_LEFT:
                case KEY_F(4):
                    selection=1;
                    type=4;
                    break;
                //case KEY_RIGHT:
                //case KEY_F(6):
                //    selection=1;
                //    type=6;
                //    break;
                case KEY_DOWN:
                    if (++selection==3) selection=1;
                    break;
                case KEY_UP:
                    if (--selection==0) selection=2;
                    break;
                case 10:
                    delwin(typemenu);
                    touchwin(stdscr);
                    wrefresh(stdscr);
                    return (40+selection);
                    break;
                case KEY_F(5):
                    delwin(typemenu);
                    touchwin(stdscr);
                    wrefresh(stdscr);
                    return 0;
                    break;
                case 27:
		    delwin(typemenu);
		    touchwin(stdscr);
		    wrefresh(stdscr);
		    return 0;
		    break;
            }
        }
        /*else if (type==6)
        {
            draw_qualitemenu(typemenu,selection);
            key=getch();
            switch(key)
            {
                case KEY_F(2):
                    selection=1;
                    type=2;
                    break;
                case KEY_F(3):
                    selection=1;
                    type=3;
                    break;
                case KEY_F(4):
                    selection=1;
                    type=4;
                    break;
                case KEY_LEFT:
                case KEY_F(5):
                    selection=1;
                    type=5;
                    break;
                case KEY_DOWN:
                    if (++selection==3) selection=1;
                    break;
                case KEY_UP:
                    if (--selection==0) selection=2;
                    break;
                case 10:
                    delwin(typemenu);
                    touchwin(stdscr);
                    wrefresh(stdscr);
                    return (50+selection);
                    break;
                case KEY_F(6):
                    delwin(typemenu);
                    touchwin(stdscr);
                    wrefresh(stdscr);
                    return 0;
                    break;
                case 27:
		    delwin(typemenu);
		    touchwin(stdscr);
		    wrefresh(stdscr);
		    return 0;
		    break;
            }
        }*/
    }
    return 0;
}
/*
typedef struct
{
    maillage m;
    int argc;
    char **argv;
} Params;

void* start_interface_glut (void* args)
{
    Params *p = (Params*)args;
    interface_glut = true;
    affiche_init(p->argc, p->argv);
    affiche_maillage(p->m);
    affiche_boucle();

    return NULL;
}
*/

typedef struct t_nom_et_emplacement
{
    char * name;   ///< Nom du fichier
    char * location;   ///< Emplacement du fichier	

}*nom_et_emplacement;

void aiguilleur(int argc, char **argv, int action, maillage *m)
{
    int pid;
    char* location;
    switch(action)
    {
        case 11:
        {
        wclear(msgbarre);
            //DIR * dirp = opendir ( DATA_DIR );
            //struct dirent * direntp;
            tableau liste_obj=new_tableau();
            int nb_obj=0;
            int i;
            int compteur;
            int continuer;
            int choix=1;
            int key;
            int maillage_charge=0;
            //fichier_obj=malloc(200*sizeof(char));
		

	    dossier arbre_liste_obj = liste_dossier ( "obj/" );

            if ( liste_obj )
            {
        	dossier sdir = arbre_liste_obj->dirs;
        	//int submenus[20] = {0};
        	//int i = 0;

        	// Pour chaque sous-dossier
        	while ( sdir )
        	{
            		//submenus[++i] = glutCreateMenu ( menu );

            		fichier file = sdir->files;

            		if ( file )
            		{
                		while ( file )
                		{
                    			//++entries;
                    			//glutAddMenuEntry ( file->name , -entries );
					nom_et_emplacement fichier_obj2=malloc(sizeof(struct t_nom_et_emplacement));
					fichier_obj2->name=file->name;
		                        // Nom du fichier : "obj/???/???"
                        	  	location = allouer_mem ( strlen ( "obj/" ) +
                                             strlen ( sdir->name ) +
                                             strlen ( file->name ) + 2 );
                        		strcpy ( location , "obj/" );
                        		strcat ( location , sdir->name );
                        		strcat ( location , "/" );
                        		strcat ( location , file->name );
					fichier_obj2->location=location;
					ajouter_element(liste_obj, fichier_obj2);
					//ajouter_element(liste_obj, file->name);
                    			file = file->next;
                		}
            		}
            		//else
            		//{
                	//	glutAddMenuEntry ( "Aucun fichier" , 0 );
            		//}

            		sdir = sdir->next;
      	     }

             //sdir = arbre_liste_obj->dirs;
             //i = 0;
        
             //menu_modeles = glutCreateMenu ( menu );
        
             // Pour chaque sous-menu
             //while ( sdir )
             //{
            	//glutAddSubMenu ( sdir->name , submenus[++i] );
            	//sdir = sdir->next;
             //}
         }

             









/*
            if ( !dirp )
            {
                fprintf ( stderr , "Impossible d'accéder au répertoire %s.\n", DATA_DIR );
            }
            else
            {
                while ( ( direntp = readdir ( dirp ) ) )
                {
                    if ( strstr ( direntp->d_name, ".obj" ) )
                    {
                        ajouter_element(liste_obj, direntp->d_name);
                    }
                }               
            }*/

            nb_obj=taille_tableau(liste_obj);
            if (nb_obj==0)                    /* Signaler que l'utilisateur n'a pas d'element */
            {
    	        mvwprintw(msgbarre, 5, 5, "Le repertoire ne contient pas de fichiers obj");
	            refresh();
	            //while(getch()!=27) {}         /* Attendre que l'utilisateur presse la touche ECHAP */
	            return ;
            }

            compteur=0;

            while(maillage_charge==0)
            {
                i=0;                            /* Initialisation du compteur a 0 */
                continuer=1;
                wclear(msgbarre);
                mvwprintw(msgbarre, 5, 20, "%d fichiers obj", nb_obj);
                while(continuer && i<10 && compteur<nb_obj) /* Afficher les elements par paquet de 10 */
                {                                            /* ou moins s'il en reste moins de 10 */
	                mvwprintw(msgbarre, 7+i, 5, "%s", ((nom_et_emplacement)acceder_element(liste_obj, compteur, nom_et_emplacement))->name);
                    compteur++;
	                i++;
                }

                 mvwchgat(msgbarre, 7+((choix-1) % 10), 5, strlen(((nom_et_emplacement)acceder_element(liste_obj, choix-1, nom_et_emplacement))->name), A_REVERSE, 0, NULL);
                 wrefresh(msgbarre);

                 continuer=1;
                 while(continuer)
                 {
	                switch(key=getch())
                    {
	                    case KEY_DOWN:
	                    if (choix!=nb_obj)        /* Si on n'est pas arrive au dernier element */
	                    {
                            if(choix%10 != 0)
                            {
                                 compteur=compteur-i;
                            }
 		                    choix++;              /* On selectionne l'element suivant */
	                    }
                        else compteur=compteur-i;	      
	                    continuer=0;              /* On a appuye sur une bonne touche alors on sort de la boucle */
	                    break;
	                    case KEY_UP:
	                    if (choix!=1)             /* Si on n'est pas arrive au premier element */
	                    {
                            if(choix%10 == 1)
                            {
                                compteur=compteur-10;
                            }
		                    choix--;              /* On selectionne l'element precedent */    
	                    }  
                        compteur=compteur-i;
	                    continuer=0;              /* On a appuye sur une bonne touche alors on sort de la boucle */
	                    break;
                        case 10:
                            //*fichier_obj='\0';
                            //strcat(fichier_obj ,DATA_DIR);
                            //strcat(fichier_obj ,acceder_element(liste_obj, choix-1, char*));
                            free_maillage(*m);
                            *m=charger_maillage(((nom_et_emplacement)acceder_element(liste_obj, choix-1, nom_et_emplacement))->location);
                            continuer=0;
                            maillage_charge=1;
                            wclear(stdscr);
                            wrefresh(stdscr);
                            wclear(msgbarre);
                            wrefresh(msgbarre);
                            printf_ncurses("Maillage charge.\n");

                        break;
                        case 27:
                            continuer=0;
                            maillage_charge=1;
                            wclear(msgbarre);
                            wrefresh(msgbarre);
                        break;
                    }
                }
            }

            //closedir ( dirp );
	    supprimer_liste_dossier ( arbre_liste_obj );
            break;
        }
        case 12:
        {
        wclear(msgbarre);
            if(*m==NULL)
            {
                printf_ncurses("Aucun maillage n'est charge.\n");
                wrefresh(msgbarre);
            }
            else
            {
                location=malloc(200*sizeof(char));
                //move(y, x); /* On efface de l'ecran ce qui ete deja ecrit la ou on veut ecrire */
                clrtoeol();
                echo();
                curs_set(1);
                mvwprintw(msgbarre, 9, 10,"Veuillez donner un nom de fichier SVP.\n");
                wrefresh(msgbarre);
                mvwgetstr(msgbarre, 10, 10, location); /* on attend que l'utilisateur tape ce qu'il veut rentrer */
                noecho(); /* on desactive l'affichage des touches tapees par l'utilisateur */
                curs_set(0); /* cache le curseur de saisie */
                enregistrer_maillage (location, *m);
                printf_ncurses("Maillage enregistre.\n");
            }
            break;
        }
        case 13:
        {
        wclear(msgbarre);
/*
            Params args;
            args.m = *m;
            args.argc = argc;
            args.argv = argv;
            pthread_create(&th, NULL, start_interface_glut, &args);
*/
          
            if((pid=fork())==0)
            {
                interface_glut = true;
                affiche_init(argc, argv);
                affiche_maillage(*m);
                affiche_boucle();
            }
            break;
        }
        case 21:
        wclear(msgbarre);
            if(*m==NULL)
            {
                printf_ncurses("Aucun maillage n'est charge.\n");
                wrefresh(msgbarre);
            }
            else
	    {
            corr_somm_dups(*m);
	    }
            break;
        case 22:
        wclear(msgbarre);
            if(*m==NULL)
            {
                printf_ncurses("Aucun maillage n'est charge.\n");
                wrefresh(msgbarre);
            }
            else
	    {
            corr_somm_nonref(*m);
            }
	    break;
        case 23:
        wclear(msgbarre);
            if(*m==NULL)
            {
                printf_ncurses("Aucun maillage n'est charge.\n");
                wrefresh(msgbarre);
            }
            else
	    {
            corr_faces_dups(*m);
            }
	    break;
        case 24:
        wclear(msgbarre);
            if(*m==NULL)
            {
                printf_ncurses("Aucun maillage n'est charge.\n");
                wrefresh(msgbarre);
            }
            else
	    {
            corr_faces_invalides(*m);
            }
	    break;
        case 25:
        wclear(msgbarre);
            if(*m==NULL)
            {
                printf_ncurses("Aucun maillage n'est charge.\n");
                wrefresh(msgbarre);
            }
            else
	    {
            recherche_somm_nonmanifold(*m);
            }
	    break;
        case 26:
        wclear(msgbarre);
            if(*m==NULL)
            {
                printf_ncurses("Aucun maillage n'est charge.\n");
                wrefresh(msgbarre);
            }
            else
	    {
            corr_faces_malorientees(*m);
            }
	    break;
        case 27:
        wclear(msgbarre);
            if(*m==NULL)
            {
                printf_ncurses("Aucun maillage n'est charge.\n");
                wrefresh(msgbarre);
            }
            else
	    {
            corr_faces_degenerees(*m);
            }
	    break;
        case 28:
        wclear(msgbarre);
            if(*m==NULL)
            {
                printf_ncurses("Aucun maillage n'est charge.\n");
                wrefresh(msgbarre);
            }
            else
	    {
            affiche_intersection(*m);
            }
	    break;
        case 29:
        wclear(msgbarre);
            if(*m==NULL)
            {
                printf_ncurses("Aucun maillage n'est charge.\n");
                wrefresh(msgbarre);
            }
            else
	    {
            corr_faces_manquantes(*m);
            }
	    break;
        case 30:
        wclear(msgbarre);
            if(*m==NULL)
            {
                printf_ncurses("Aucun maillage n'est charge.\n");
                wrefresh(msgbarre);
            }
            else
	    {
            //corr_maillage(*m);
            }
	    break;
        case 31:
        wclear(msgbarre);
            if(*m==NULL)
            {
                printf_ncurses("Aucun maillage n'est charge.\n");
                wrefresh(msgbarre);
            }
            else
	    {
            *m=filtre_uniforme(*m);
            }
	    break;
        case 32:
        wclear(msgbarre);
            if(*m==NULL)
            {
                printf_ncurses("Aucun maillage n'est charge.\n");
                wrefresh(msgbarre);
            }
            else
	    {
            *m=filtre_normal(*m);
	    }
            break;
        case 41:
        wclear(msgbarre);
            if(*m==NULL)
            {
                printf_ncurses("Aucun maillage n'est charge.\n");
                wrefresh(msgbarre);
            }
            else
	    {
            *m=subdivision_plane(*m);
            }
	    break;
        case 42:
        wclear(msgbarre);
            if(*m==NULL)
            {
                printf_ncurses("Aucun maillage n'est charge.\n");
                wrefresh(msgbarre);
            }
            else
	    {
            *m=subdivision_normale(*m);
            }
	    break;
    }
}


int main_ncurses(int argc, char **argv)
{
    interface_ncurses = true;
    WINDOW* menubarre;
    int key=0;
    maillage* m=malloc(sizeof(maillage));
    *m=NULL;
    int colssave=COLS;
    int linessave=LINES;
    interface_glut = false;

    init_prog();
    msgbarre=subwin(stdscr,LINES-3,COLS,3,0);

    if(COLS<80)
    {
        wprintw(msgbarre,"Veuillez agrandir le terminal (au moins 80x20)\nESC pour quitter");
        while(key!=27 && COLS<80)
        {
            key=getch();
        }
        clear();
    }

    menubarre=subwin(stdscr,1,66,0,0);

mvprintw(LINES/2-4, COLS/2-39, "    _________   ___________   __ ______________ __  __   __    ___    ____ \n");
mvprintw(LINES/2-3, COLS/2-39, "   / ____/   | / ___/_  __/  /  |/  / ____/ ___/ / / /  / /   /   |  / __ )\n");
mvprintw(LINES/2-2, COLS/2-39, "  / /_  / /| | \\__ \\ / /    / /|_/ / __/ \\__ \\/ /_/ /  / /   / /| | / __  |\n");
mvprintw(LINES/2-1, COLS/2-39, " / __/ / ___ |___/ // /    / /  / / /___ ___// __  /  / /___/ ___ |/ /_/ / \n");
mvprintw(LINES/2, COLS/2-39, "/_/   /_/  |_/____//_/    /_/  /_/_____//___/_/ /_/  /_____/_/  |_/_____/  \n");

    if(argc>1)
    {
        *m=charger_maillage(argv[1]);
    }

    if(*m!=NULL)
    {
        mvprintw(LINES-6, COLS/2-8, "Bienvenue sur FML.\n");
    }
    else
    {
        mvprintw(LINES-6, COLS/2-40, "Bienvenue sur FML, Chargez un maillage pour effectuer des modifications dessus.\n");
    }

    mvprintw(LINES-2, 0, "ESC pour quitter.\n");
    refresh();

    while(key!=27)
    {

        if(COLS!=colssave || LINES!=linessave )
        {
            colssave=COLS;
            linessave=LINES;
            wrefresh(msgbarre);
            delwin(msgbarre);
            msgbarre=subwin(stdscr,LINES-3,COLS,3,0);
        }

        draw_menubarre(menubarre);
        if(*m==NULL)
        {
            mvwprintw(stdscr, 2, COLS-26, "Aucun maillage charge");
        }
        else
        {
            mvwprintw(stdscr, 2, COLS-strlen(nom_maillage(*m))-30, "%s (%d sommets, %d faces)\n", nom_maillage(*m), nombre_de_sommets(*m), nombre_de_faces(*m));
        }


        refresh();
        key=getch();
        
        wmove(msgbarre, 0, 0);
        if (key==KEY_F(2))
        {
            aiguilleur(argc, argv, action_menu(2), m);
        }
        if (key==KEY_F(3))
        {
            aiguilleur(argc, argv, action_menu(3), m);
        }
        if (key==KEY_F(4))
        {
            aiguilleur(argc, argv, action_menu(4), m);
        }
        if (key==KEY_F(5))
        {
            aiguilleur(argc, argv, action_menu(5), m);
        }
        //if (key==KEY_F(6))
        //{
        //    aiguilleur(argc, argv, action_menu(6), m);
        //}
    }
    
    //if(th!=0) pthread_cancel(th);
    delwin(menubarre);
    delwin(msgbarre);
    endwin();
    return 0;
}
