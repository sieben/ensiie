/**
 * @file face.h
 * Module contenant les définition des données et des traitements liées  la
 * manipulation des faces.
 * @author Sara Iraqi
 * @author Jean Crémèse
 */

#ifndef FACE_H
#define FACE_H

#include "sommet.h"


/**
 * @addtogroup face
 */

/*@{*/

/* ------------------------------------------------------------------------ */
/* Définition des données                                                   */
/* ------------------------------------------------------------------------ */

/**
 * Définition du type face
 * Composé de :
 *  - d'un premier sommet
 *  - d'un second sommet
 *  - d'un troisième sommet
 *  - d'une normale (éventuellement NULL)
 *  - d'une coloration
 */
typedef struct t_face * face;

/**
 * Définition du type couleur (selon le codage RVB)
 */
typedef struct t_couleur* couleur;

struct t_couleur
{
    unsigned char r; ///< Composante rouge.
    unsigned char v; ///< Composante verte.
    unsigned char b; ///< Composante bleue.
};


/* ------------------------------------------------------------------------ */
/* Définition des traitements sur ces données                               */
/* ------------------------------------------------------------------------ */

//Fonction d'affichage d'une face qu'on utilisera dans les
// tests des autres modules 
void afficher_face(face f);


// Fonction d'affichage de la couleur d'une face qu'on utilisera dans les
// tests des autres modules 
void afficher_couleur(couleur c);

/**
 * Teste si la face est bien formée, c'est-à -dire si elle est
 * composée de trois sommets tous non NULL et non identiques deux à  deux.
 * @param f la face à  tester
 * @return 1 si la face est bien formée, 0 sinon
 * @test le  resultat avec une face mal formée
 * @test le resultat avec une face bien formée
 */

int bienforme_face(face f);

/**
 * Calcule le barycentre de la face.
 * @param f la face dont il faut calculer le barycentre
 * @return le barycentre de la face
 * @test a partir d'une face mal formée
 * @test a partir d'une face bien formée
 */
point barycentre_face(face f);

/**
 * Extrait le n-ième sommet de la face.
 *
 * @param f la face dont on veut extraire le sommet
 * @param numero un entier qui correspond à  la place du sommet dans la face
 * @pre le numéro est compris entre 0 et 2
 * @return le sommet bien formé
 * @test extraire à  partir d'une face bien formée

 */
sommet extraire_sommet(face f, int numero);

/**
 * Extrait la normale de la face.
 *
 * @param f la face dont on veut extraire la normale
 * @return la normale bien formée correspondante
 * @test extraire à  partir d'une face bien formée dont la normale est NULL
 * @test extraire à  partir d'une face bien formée dont la normale est quelconque
 */
vecteur extraire_normale_f(face f);

/**
 * Extrait la couleur de la face.
 *
 * @param f la face dont on veut extraire la couleur
 * @return la couleur bien formée correspondante
 * @test extraire à partir d'une face bien formée
 */
couleur extraire_couleur(face f);

/**
 * Extrait la n-ième composante de la couleur
 * @param c la couleur dont on veut extraire la coordonnée
 * @param numero l'index de la coordonnée voulue 
 * @pre le numero est compris entre 0 et 2
 * @return la composante bien formée
 * @test modifier la coordonnée t2 d'une couleur en la remplacant par elle-màªme.
 * @test modifier la coordonnée t2 d'une couleur
 */
char extraire_coordonnee_couleur(couleur c, int numero);

/**
 * Création d'une face en précisant éventuellement sa normale et sa couleur.
 *
 * Si on ne souhaite pas définir pour le moment la normale, on lui donne le
 * vecteur nul en passant en parametre la fonction vecteur_nul().
 * Si on ne souhaite pas définir pour le moment la couleur, on lui donne la
 * couleur blanche en passant en parametre la fonction couleur_default().
 * Si on ne souhaite pas définir pour le moment la texture, on passe en
 * parametre la fonction texture_default().
 *
 * @param s1 le premier sommet
 * @param s2 le deuxieme sommet
 * @param s3 le troisieme sommet
 * @param normale le vecteur qui correspond à  la normale
 * @param c la coloration de la face
 * @return la face bien formée
 * @test création d'une face à  partir d'un sommet s1 mal formé et du reste des
 *       paramètres bien formés
 * @test création d'une face à  partir de deux sommets s1 et s2 identiques et du
 *       reste des paramétres bien formés
 * @test création d'une face à  partir d'une normale mal formée et du reste des
 *       paramètres bien formés
  * @test création d'une face à  partir de tout les paramètres bien formés
*/
face new_face(sommet s1, sommet s2, sommet s3, vecteur normale, couleur c);

/**
 * Suppression d'une face.
 *
 * @param f la face à  supprimer
 * @post le pointeur pointe désormais vers NULL
 */
void liberer_mem_face(face f);


/**
 * Modification des sommets d'une face.
 *
 * @param f la face à  modifier
 * @param s1 le premier nouveau sommet
 * @param s2 le deuxieme nouveau sommet
 * @param s3 le troisieme nouveau sommet
 * @return la face bien formée
 * @test modification de la face avec un des sommets mal formé
 * @test modification de la face avec un tous les sommets bien formés
 */
face modif_face_sommets(face f,sommet s1, sommet s2, sommet s3);

/**
 * Modification de la normale à  une face.
 *
 * @param f la face à  modifier
 * @param normale le vecteur qui correspond à  la nouvelle normale
 * @return la face bien formée
 * @test modification de la face bien formée avec une normale bien formé
 */
face modif_face_normale(face f, vecteur normale);

/**
 * Génération de la couleur blanche à  appliquer par défault.
 *
 * @return la couleur blanche bien formée
 */
couleur couleur_default();

/**
 * Génération d'une couleur 
 * 
 * @param c1 le char correspondant à la première composante
 * @param c2 le char correspondant à la première composante
 * @param c3 le char correspondant à la première composante
 * @pre les composantes sont bien formées
 * @return la couleur bien formée
 * @test génération d'une couleur
 */
couleur new_couleur(char c1, char c2, char c3);

/**
 * Modification de la couleur d'une face.
 *
 * @param f la face à  modifier
 * @param c la nouvelle coloration de la face
 * @pre la face est bien formée
 * @pre le nouvelle couleur est bien définie
 * @return la face bien formée
 * @test modification de la couleur d'une face mal formée
 */
face modif_face_couleur(face f,couleur c);

/**
 * Calcul de la normale à  une face.
 *
 * @param f la face dont on veut calculer la normale
 * @return le vecteur bien formé
 * @test calcul de la normale d'une face mal formée
 * @test calcul de la normale d'une face bien formée
 */
vecteur calcul_norm_face(face f);

/**
 * Comparaison de deux faces selon les points,
 * Indépendant de l'ordre des points (par ex (1,3,2) et (3,1,2) sont identiques)
 *
 * @param f1 la première face à  comparer
 * @param f2 la deuxième face à  comparer
 * @return 1 si les deux faces sont identiques, 0 sinon
 * @test comparaison de deux face identiques 
 * @test comparaison de deux face dont les points sont identiques 
 * @test comparaison de deux faces distinctes
 **/

int comp_face_points(face f1,face f2);


/**
 * Comparaison de deux faces selon les sommets,
 * On compare les sommets dans l'ordre définis dans la face.
 *
 * @param f1 la première face à  comparer
 * @param f2 la deuxième face à  comparer
 * @return 1 si les deux faces sont identiques, 0 sinon
 * @test comparaison de deux face identiques 
 * @test comparaison de deux face dont les sommets sont identiques 
 * @test comparaison de deux faces distinctes
 **/

int comp_face_sommets(face f1,face f2);


/**
 * Comparaison de deux faces.
 * On compare les sommets dans l'ordre définis dans la face et les normales.
 *
 * @param f1 la première face à  comparer
 * @param f2 la deuxième face à  comparer
 * @return 1 si les deux faces sont identiques, 0 sinon
 * @test comparaison de deux faces, dont l'un est mal formé
 * @test comparaison de deux faces égaux bien formés
 * @test comparaison de deux faces inégaux bien formés
 */
int comp_face(face f1,face f2);

/**
 * Calcul de la courbure de la courbure entre deux faces.
 *
 * @param f1 la première face servant au calcul
 * @param f2 la seconde face servant au calcul
 * @pre les 2 faces sont correctement formées
 * @return une valeur positive
 * @test calcul de la courbure entre 2 faces identiques
 * @test calcul de la courbure entre 2 faces non identiques et bien formées
 * @test calcul de la courbure entre une des faces mal formé
*/
float courbure_deux_faces(face f1, face f2);

/*@}*/

#endif /* fin de ifndef FACE_H */
