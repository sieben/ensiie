/**
* @file point.h
* @brief Module contenant les d�finition des donn�es et des traitements li�s
*        � la manipulation des points.
* @author Iraqi Sara
* @author Cr�m�se Jean
*/

#ifndef POINT
#define POINT

#include "gestion_memoire.h"

/**
* @defgroup point Point
* @brief Module de gestion des points.
* @author Iraqi Sara
* @author Cr�m�se Jean
*/


/*@{*/

/* ------------------------------------------------------------------------ */
/* D�finition des donn�es                                                   */
/* ------------------------------------------------------------------------ */

typedef struct t_point* point;
/**
 * D�finition du type point
 */

struct t_point
{
    float x; ///< Premi�re coordonn�e.
    float y; ///< Deuxi�me coordonn�e.
    float z; ///< Troisi�me coordonn�e.
};

/* ------------------------------------------------------------------------ */
/* D�finition des traitements sur ces donn�es                               */
/* ------------------------------------------------------------------------ */

// Fonction d'affichage des points qu'on utilisera dans les tests des
// autres modules 
void afficher_point(const point p);

/**
 * Teste si le point est bien form�, c'est-�-dire s'il est
 * compos� de trois coordonn�es toutes non NULL.
 *
 * @param p le point � tester
 * @return 1 si le point est bien form�, 0 sinon
 * @test test d'un point mal form�
 * @test test d'un point
 */

int bienforme_point(point p);

/**
 * Conversion d'un point en un vecteur.
 *
 * @param p le point qu'on veut convertir
 * @pre p est bien form�
 * @return le vecteur cr�� poss�de les coordonn�es du point
 * @test convertir un point en vecteur
 */

#define convertir_en_vecteur(p) vecteur_nouveau( (p)->x, (p)->y, (p)->z )

/**
 * Extraction de la n-i�me coordonn�e du point.
 *
 * @param p le point dont on veut extraire la coordonn�e
 * @param numero un entier qui correspond � la place de la
 *      coordonn�e dans le point
 * @pre le num�ro est compris entre 0 et 2
 * @pre p est bien form�
 * @return la coordonn�e correspondante
 * @test extraire la coordonn�e 1 d'un point
 */

float extraire_coordonnee_point(point p, int numero);

/**
 * Cr�ation d'un point.
 *
 * @param  x le float qui est la premiere coordonn�e du futur point
 * @param  y le float qui est la deuxieme coordonn�e du futur point
 * @param  z le float qui est la troisieme coordonn�e du futur point
 * @return le point correctement form�
 * @test cr�er un point � partir de trois coordonn�es
 */

point new_point(float x, float y, float z);

/**
 * Modification d'une coordonn�e.
 * Si on ne souhaite pas changer une coordonn�e, on passe celle du point �
 * modifier en argument.
 *
 * @param p le point
 * @param  x1 le flottant qui remplace la premiere coordonn�e du point p.
 * @param  y1 le flottant qui remplace la deuxieme coordonn�e du futur point p.
 * @param  z1 le flottant qui remplace la troisieme coordonn�e du futur point p.
 * @pre p est bien form�
 * @return Point correctement form� diff�rent du triplet d'origine.
 * @test modifier la coordonn�e y1 d'un point en la remplacant par elle-m�me.
 * @test modifier la coordonn�e y1 d'un point.
 */

point modif_point(point p,float x1,float y1,float z1);

/**
 * Comparaison de deux points.
 *
 * @param p1 le premier des deux points � comparer.
 * @param p2 le deuxieme des deux points � comparer.
 * @pre p1 est bien form�
 * @pre p2 est bien form�
 * @return Renvoie 1 si les deux points sont identiques, 0 sinon.
 * @test comparer deux points �gaux
 * @test comparer deux points in�gaux
 */

int comp_point(point p1,point p2);

/**
 * Calcul de la distance entre deux points.
 *
 * @param p1 Premier des deux points pour lesquels il faut calculer la distance.
 * @param p2 Second des deux points pour lesquels il faut calculer la distance.
 * @pre p1 est bien form�
 * @pre p2 est bien form�
 * @return Renvoie un flottant positif entre les deux points.
 * @test calculer la distance entre deux points �gaux.
 * @test calculer la distance entre deux points diff�rents.
 */

float distance_point(point p1,point p2);

/**
 * Suppression d'un point en m�moire.
 *
 * @param p le point qu'on veut supprimer
 * @return le pointeur pointe d�sormais vers NULL
 */

#define liberer_mem_point(p)  liberer_mem(p)

/*@}*/

#endif /* fin de ifndef POINT_H */
