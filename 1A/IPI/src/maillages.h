/**
 * @file maillages.h
 * @brief Module de gestion des maillages.
 * @author Lasnier Simon
 */

#ifndef MAILLAGES
#define MAILLAGES

#include "sommet.h"
#include "face.h"
#include "gestion_memoire.h"

/**
 * @defgroup maillage Maillage
 * @brief Module de gestion des maillages.
 * @author Lasnier Simon
 * @author Olivi Hermann
 */

/*@{*/

typedef struct t_maillage * maillage;


/**
 * Crée un nouveau maillage vide.
 *
 * @param  nom Nom du maillage (spécifié par "g" dans le fichier Obj).
 * @return Pointeur sur le maillage crée.
 *
 * @pre    Aucune condition.
 * @post   Le maillage renvoyé est vide, mais initialisé. Si nom est NULL, alors
 *         un nom par défaut sera donné au maillage.
 */

maillage new_maillage ( const char * nom );


/**
 * Donne le nom d'un maillage.
 *
 * @param  m Maillage.
 * @return Nom du maillage.
 *
 * @pre    Le maillage m doit être valide.
 * @post   Donne une chaine de caractère terminée par le caractère nul.
 */

char * nom_maillage ( maillage m );


/**
 * Ajoute le sommet dans le maillage.
 *
 * @param m Maillage.
 * @param s Sommet que l'on veut ajouter.
 * @return Indice du sommet sur le maillage, -1 si le sommet est vide.
 *
 * @pre m est un maillage valide, s est un sommet valide.
 * @post m est le maillage avec le sommet supplémentaire, et l'indice du sommet
 *       sur le maillage est renvoyé.
 * @test Ajouter un sommet vide (NULL).
 */

int ajouter_sommet ( maillage m , sommet s );


/**
 * Supprime un sommet d'un maillage depuis une copie du sommet.
 *
 * @param m Maillage.
 * @param indice_s indice du sommet que l'on veut supprimer.
 *
 * @pre m est un maillage valide, indice_s correspond à un sommet sur le
 * maillage
 * @post le sommet a été supprimé
 * @test supprimer un sommet d'un maillage quelconque : les vérifications sont
 *       faites dans le module gestion_mémoire, fonction supprimer_element.
 */

void supprimer_sommet ( maillage m , unsigned int indice_s );


/**
 * Ajoute une face au maillage m.
 *
 * @param m Maillage.
 * @param f Face que l'on veut ajouter.
 * @pre m est un maillage valide, f une face valide
 * @post m est le maillage avec la face supplémentaire
 * @return l'indice qui correspond à la face sur le maillage, (-1) si la face
 *         est vide.
 * @test ajouter une face vide (NULL)
 */

int ajouter_face ( maillage m , face f );


/**
 * Supprime la face f du maillage m.
 *
 * @param m Maillage.
 * @param indice_f indice de la face que l'on veut supprimer
 * @pre m est un maillage valide, indice_f un indice valide
 * @post la face a été supprimée
 * @test supprimer une face d'un maillage quelconque : les vérifications sont
 *       faites dans le module gestion_mémoire, fonction supprimer_element.
 * @test supprimer une face d'un maillage quelconque : les vérifications sont
 *       faites dans le module gestion_mémoire, fonction supprimer_element.
 */

void supprimer_face ( maillage m , unsigned int indice_f );


/**
 * Obtenir le nombre de sommets du maillage m.
 *
 * @param m Maillage.
 * @pre m est un maillage valide
 * @post renvoi le nombre de sommets
 * @return un entier correspondant au nombre de sommets
 * @test obtenir le nombre de sommets à partir d'un maillage quelconque : les
 *       vérifications sont faites dans le module gestion_mémoire, fonction
 *       taille_tableau.
 */

int nombre_de_sommets ( maillage m );


/**
 * Obtenir le sommet d'un maillage à partir de son indice.
 *
 * @param m Maillage.
 * @param indice Indice du sommet.
 * @pre m est un maillage valide, indice du sommet est bon
 * @post le sommet correspondant à l'indice est renvoyé
 * @return le sommet correspondant à l'indice
 * @test obtenir un sommet d'un maillage quelconque : les vérifications sont
 *       faites dans le module gestion_mémoire, fonction acceder_element_aux.
 */

sommet obtenir_sommet ( maillage m , unsigned int indice );


/**
 * Donne l'indice d'un sommet dans un maillage.
 *
 * @param m Maillage.
 * @param s Sommet dont on veut connaitre l'indice.
 * @return Indice du sommet ou -1 si le sommet n'est pas dans le maillage.
 *
 * @pre Le maillage m et le sommet s sont valides.
 * @post L'indice correspondant au sommet est renvoyé.
 * @test Tester avec un sommet innexistant.
 */

int indice_sommet ( maillage m , sommet s );


/**
 * Modifier un sommet d'un maillage.
 *
 * @param m Maillage.
 * @param nouveau Nouvelles valeurs du sommet.
 * @param indice Indice du sommet.
 * @pre m est un maillage valide, nouveau est un sommet valide
 * un indice valide
 * @post le sommet est modifié
 * @test modifier le sommet d'un maillage quelconque : les vérifications sont
 *       faites dans le module gestion_mémoire, fonction remplacer_element_aux.
 */

void modifier_sommet ( maillage m , sommet nouveau , unsigned int indice );


/**
 * Obtenir le nombre de faces d'un maillage.
 *
 * @param m Maillage.
 * @pre m est un maillage valide
 * @post renvoi le nombre de faces
 * @return un entier correspondant au nombre de faces
 * @test obtenir le nombre de faces à partir d'un maillage vide : les
 *       vérifications sont faites dans le module gestion_mémoire, fonction
 *       taille_tableau.
 */

int nombre_de_faces ( maillage m );


/**
 * Obtenir la face d'un maillage à partir de son indice.
 *
 * @param m Maillage.
 * @param indice Indice de la face.
 * @test obtenir la face à partir d'un maillage vide
 * @pre m est un maillage valide, l'indice de la face est bon
 * @post la face correspondant à l'indice est renvoyé
 * @return la face correspondant à l'indice
 * @test obtenir une face d'un indice quelconque : les vérifications sont faites
 *       dans le module gestion_mémoire, fonction acceder_element_aux.
 */

face obtenir_face ( maillage m , unsigned int indice );


/**
 * Donne l'indice d'une face dans un maillage.
 *
 * @param m Maillage.
 * @param f Face dont on veut connaitre l'indice.
 * @return Indice de la face ou -1 si la face n'est pas dans le maillage.
 *
 * @pre Le maillage m et la face f sont valides.
 * @post L'indice correspondant à la face est renvoyé.
 * @test Tester avec une face innexistante.
 */

int indice_face ( maillage m , face f );


/**
 * Modifie une face d'un maillage.
 *
 * @param m Maillage.
 * @param nouvelle Nouvelles face.
 * @param indice Indice de la face à modifier.
 *
 * @pre  m est un maillage valide, nouvelle est une face valide, indice valide
 * @post La face est modifiée.
 * @test Modifier une face d'un maillage quelconque : les vérifications sont
 *       faites dans le module gestion_mémoire, fonction remplacer_element_aux.
 */

void modifier_face ( maillage m , face nouvelle , unsigned int indice );


/**
 * Cherche les voisins d'un sommet pour un niveau donné.
 *
 * @param m Maillage.
 * @param indice Indice du sommet dont on veut connaitre les voisins.
 * @param niveau Niveau de profondeur des voisins (le niveau 0 correspond au
 *               sommet lui-même, le niveau 1 à ses voisins, le niveau 2 aux
 *               voisins de ses voisins, etc.).
 *
 * @return Tableau de tableaux d'indices de sommets pour chaque niveau
 *         inférieur à celui demandé.
 *
 * @pre  Le maillage m est valide et indice correspond à un sommet existant.
 * @post Renvoie un tableau de tableaux contenant les sommets voisins.
 *
 * @test Utiliser un maillage invalide ou contenant des faces manifolds.
 * @test Chercher les voisins d'un sommet inexistant sur le maillage.
 * @test Chercher les voisins de niveau 4 d'un sommet.
 *
 * Cette fonction retourne un tableau de (niveau+1) tableaux d'indices.
 * Chaque tableau correpond à un niveau de voisinnage : 0 pour le sommet
 * lui-même, 1 pour ses voisins, 2 pour les voisins de ses voisins, etc.
 * Si un sommet est présent parmi les voisins de niveau i, il ne peut pas
 * apparaitre parmi les voisins de niveau supérieur à i.
 */

tableau * sommets_voisins ( maillage m , unsigned int indice ,
                            unsigned int niveau );
/**
 * Cherche si un sommet appartient à une face du maillage (test sur les
 * coordonnées des points uniquement)
 *
 * @param m Maillage.
 * @param s sommet 
 * @param ind_f indice de la face dans le maillage
 *
 * @return booléeen.
 */

int estdansface ( maillage m , sommet s , unsigned int ind_f );

/**
 * Récupère les faces voisines d'une face.
 *
 * @param m Maillage.
 * @param indice_f indice de la face dont on veut les faces voisines.
 * @return une structure de type tableau qui contient les faces
 *
 * @pre  m est un maillage valide, f est une face valide, taille est un pointeur
 *       sur un entier qui contiendra la taille du tableau récupéré
 * @post renvoi un tableau contenant les faces voisines.
 * @test obtenir les faces voisines d'une face sur un maillage quelconque
 */

tableau faces_voisines ( maillage m , unsigned int indice_f );


/**
 * Récupère les faces incidentes à un point.
 *
 * @param m Maillage.
 * @param indice_s indice du sommet dont on veut les faces incidentes
 * @return une structure de type tableau contenant les faces incidentes
 *
 * @pre  m est un maillage valide, indice_s est un indice valide
 * @post renvoi un tableau contenant les faces incidentes.
 * @test obtenir les faces incidentes à un sommet sur un maillage quelconque
 */

tableau faces_incidentes ( maillage m , unsigned int indice_s );


/**
 * Teste si deux sommets sont voisins sur le maillage.
 *
 * @param m Maillage
 * @param s1 le premier sommet à tester
 * @param s2 le seconde sommet à tester
 * @pre le maillage et les sommets sont valides
 * @post un booléen est retourné
 * @return 1 si les sommets sont valides, présents sur le maillage et
 * voisins du le maillage, 0 sinon
 */

int sont_voisins ( maillage m , sommet s1 , sommet s2 ) ;

/**
 * Libère la mémoire allouée pour un maillage.
 *
 * @param m Maillage.
 * @pre m est un maillage valide
 * @post la mémoire du maillage a été libérée
 * @test libérer la mémoire d'un maillage quelconque
 */

void free_maillage ( maillage m );


/**
 * Calcule la normale d'un sommet dans un maillage.
 *
 * @param m Maillage.
 * @param indice_s indice du sommet dont on veut calculer la normale
 * @pre m est un maillage valide, indice_s est un indice sur le sommet
 * @post la normale du sommet correspondant à indice_s est calculée
 * @test calculer la normale à partir d'un sommet sur un maillage vide (NULL)
 */

void calculer_normale_sommet ( maillage m , unsigned int indice_s );


/**
 * Modifie les dimensions d'un maillage.
 *
 * @param m Maillage.
 * @param scale Facteur d'aggrandissement.
 * @pre   Le maillage m est valide, scale est strictement positif.
 * @post  Le maillage a été redimensionné.
 */

void scale_maillage ( maillage m , float scale );


/**
 * Modifie les dimensions d'un tableau de maillages pour qu'ils tiennent dans
 * un cube de côté 1.
 *
 * @param m Maillage.
 * @pre   Le maillage est valide.
 * @post  Le maillage ont été redimensionnés.
 */

float maillages_unitize ( maillage m );

/*@}*/

#endif
