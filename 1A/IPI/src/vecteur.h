/**
 * @file   vecteur.h
 * @brief  Définition du module vecteur, contenant des outils vectoriels.
 * @author Rémy Leone
 * @author Teddy Michel
 */

#ifndef FICHIER_VECTEUR_H
#define FICHIER_VECTEUR_H

#include <stdbool.h>

/**
 * @defgroup vecteur Vecteur
 * @brief    Outils vectoriels.
 * @author   Rémy Leone
 * @author   Teddy Michel
 * @date     23 mars 2010 : Création
 */

/*@{*/

typedef struct t_vecteur * vecteur;

// Fonction d'affichage des vecteurs qu'on utilisera dans les tests des
// autres modules 
void afficher_vecteur(const vecteur v);

/**
 * Création d'un vecteur à partir de ses trois coordonnées.
 *
 * @param  x Abscisse.
 * @param  y Ordonnée.
 * @param  z Côte.
 * @return Pointeur sur le vecteur crée.
 *
 * @pre    Aucune.
 * @post   Donne un pointeur sur le vecteur crée.
 */

vecteur vecteur_nouveau ( float x , float y , float z );

/**
 * Definit le vecteur nul.
 */

#define     vecteur_nul        NULL

/**
 * Teste si un vecteur est nul.
 *
 * @return Booléen.
 *
 * @pre    Aucune.
 * @post   Valeur de verité.
 */

bool est_nul ( const vecteur v );

/**
 * Copie un vecteur.
 *
 * @param  v Vecteur à copier.
 * @return Pointeur sur un nouveau vecteur égal à v.
 *
 * @pre    Le vecteur v doit etre défini.
 * @post   Donne un pointeur sur le vecteur crée.
 */

vecteur vecteur_copie ( const vecteur v );

/**
 * Addition de deux vecteurs.
 *
 * @param  v1 Pointeur sur le premier vecteur.
 * @param  v2 Pointeur sur le second vecteur.
 *
 * @pre    Les vecteurs v1 et v2 doivent être définis.
 * @post   Après l'appel, on a v2 = v1 + v2
 *
 * @test   Addition d'un vecteur quelconque avec le vecteur nul.
 * @test   Addition de deux vecteurs opposés (leur somme est nulle).
 */

void vecteur_add ( const vecteur v1 , vecteur v2 );

/**
 * Soustraction de deux vecteurs.
 *
 * @param  v1 Pointeur sur le premier vecteur.
 * @param  v2 Pointeur sur le second vecteur.
 *
 * @pre    Les vecteurs v1 et v2 doivent être définis.
 * @post   Après l'appel, on a v2 = v2 - v1
 *
 * @test   Soustraction d'un vecteur quelconque avec le vecteur nul.
 * @test   Soustraction de deux vecteurs identiques.
 */

void vecteur_diff ( const vecteur v1 , vecteur v2 );


/**
 * Soustraction de deux vecteurs, avec creation du vecteur resultat.
 *
 * @param  v1 Pointeur sur le premier vecteur.
 * @param  v2 Pointeur sur le second vecteur.
 *
 * @pre    Les vecteurs v1 et v2 doivent être définis.
 * @post   renvoi un nouveau vecteur qui contient v2 - v1
 * @return v2 - v1
 *
 * @test   Soustraction d'un vecteur quelconque avec le vecteur nul.
 * @test   Soustraction de deux vecteurs identiques.
 */

vecteur vecteur_diffN ( const vecteur v1 , const vecteur v2 );


/**
 * Multiplication d'un vecteur par un nombre.
 *
 * @param  v Pointeur sur le vecteur à multiplier.
 * @param  k Nombre.
 *
 * @pre    Le vecteur v doit être défini.
 * @post   Le vecteur v a été multiplié par k.
 *
 * @test   Multiplier le vecteur nul par un nombre quelconque.
 * @test   Multiplier un vecteur quelconque par zéro.
 */

void vecteur_mult ( vecteur v , float k );

/**
 * Multiplication d'un vecteur par un reel avec creation 
 * du vecteur resultat.
 *
 * @param  v Pointeur sur le vecteur à multiplier.
 * @param  k Nombre.
 *
 * @pre    Le vecteur v doit être défini.
 * @post   Le vecteur v a été multiplié par k.
 *
 * @test   Multiplier le vecteur nul par un nombre quelconque.
 * @test   Multiplier un vecteur quelconque par zéro.
 */

vecteur vecteur_multN ( const vecteur v1 , const float k );

/**
 * Calcule le produit vectoriel de deux vecteurs.
 *
 * @param  v1 Pointeur sur le premier vecteur.
 * @param  v2 Pointeur sur le second vecteur.
 * @return Résultat de l'opération.
 *
 * @pre    Les vecteurs v1 et v2 doivent être définis.
 * @post   Donne le produit vectoriel de v1 et v2.
 *
 * @test   Tester avec deux vecteurs quelconques.
 * @test   Tester avec un vecteur quelconque et le vecteur nul.
 * @test   Tester avec deux vecteurs égaux.
 */

vecteur vecteur_prod_vect ( const vecteur v1 , const vecteur v2 );

/**
 * Calcule le produit scalaire de deux vecteurs.
 *
 * @param  v1 Pointeur sur le premier vecteur.
 * @param  v2 Pointeur sur le second vecteur.
 * @return Résultat de l'opération.
 *
 * @pre    Les vecteurs v1 et v2 doivent être définis.
 * @post   Donne le produit scalaire de v1 et v2.
 *
 * @test   Tester avec deux vecteurs quelconques.
 * @test   Tester avec deux vecteurs orthogonaux.
 * @test   Tester avec un vecteur quelconque et le vecteur nul.
 * @test   Tester avec deux vecteurs égaux.
 */

float vecteur_prod_scal ( const vecteur v1 , const vecteur v2 );

/**
 * Indique si deux vecteurs sont égaux.
 *
 * @param  v1 Pointeur sur le premier vecteur.
 * @param  v2 Pointeur sur le second vecteur.
 * @return Booléen valant true si les deux vecteurs sont égaux.
 *
 * @pre    Les vecteurs v1 et v2 doivent être définis.
 * @post   Indique si v1 et v2 sont égaux.
 *
 * @test   Tester avec deux vecteurs quelconques.
 * @test   Tester avec deux vecteurs égaux.
 */

bool vecteur_egaux ( const vecteur v1 , const vecteur v2 );

/**
 * Calcule la norme d'un vecteur.
 *
 * @param  v Pointeur sur le vecteur dont on veut calculer la norme.
 * @return Norme du vecteur.
 *
 * @pre    Le vecteur v doit être défini.
 * @post   Donne la norme du vecteur v.
 *
 * @test   Tester avec un vecteur quelconque.
 * @test   Tester avec le vecteur nul.
 */

float vecteur_norme ( const vecteur v );

/**
 * Normalise un vecteur.
 *
 * @param  v Pointeur sur le vecteur à normaliser.
 *
 * @pre    Le vecteur v doit être défini.
 * @post   Après l'appel, le vecteur est de norme égale à un.
 *
 * @test   Tester avec un vecteur quelconque.
 * @test   Tester avec le vecteur nul.
 * @test   Tester avec un vecteur normalisé.
 */

void vecteur_normalise ( vecteur v );

/**
 * Calcule l'angle en radians entre deux vecteurs.
 *
 * @param  v1 Pointeur sur le premier vecteur.
 * @param  v2 Pointeur sur le second vecteur.
 * @return Angle entre les deux vecteurs ou 0 si l'un des deux est nul.
 *
 * @pre    Les vecteurs v1 et v2 doivent être définis.
 * @post   Donne l'angle en radians entre v1 et v2.
 *
 * @test   Tester avec deux vecteurs égaux quelconques.
 * @test   Tester avec le vecteur nul et un vecteur quelconque.
 * @test   Tester avec deux vecteurs nuls.
 */

float vecteur_angle ( const vecteur v1 , const vecteur v2 );

/**
 * Calcule la distance entre deux points.
 * 
 * @param  v1 Pointeur sur le premier vecteur.
 * @param  v2 Pointeur sur le second vecteur.
 * @return Distance entre les deux points.
 *
 * @pre    Les vecteurs v1 et v2 doivent être définis.
 * @post   Donne la distance entre les extrémités des deux vecteurs.
 *
 * @test   Tester avec deux vecteurs égaux.
 * @test   Tester avec le vecteur nul.
 */

float vecteur_distance ( const vecteur v1 , const vecteur v2 );

/**
 * Extrait les composantes d'un vecteur.
 *
 * @param  v Pointeur sur le vecteur.
 * @param  c Indice de la composante souhaitée (x = 0, y = 1, z = 2).
 * @return Composante selectionnée.
 *
 * @pre    Le vecteur v doit être défini.
 * @post   Valeur de la composante selectionnée.
 *
 * @test   Tester avec un vecteur quelconque.
 * @test   Tester avec le vecteur nul.
 */

float extraire_composante ( const vecteur v , int c );

/**
 * Modifie les composantes d'un vecteur.
 *
 * @param  v Pointeur sur le vecteur.
 * @param  x Nouvelle abscisse.
 * @param  y Nouvelle ordonnée.
 * @param  z Nouvelle cote.
 *
 * @pre    Le vecteur v doit être défini.
 * @post   Les composantes du vecteur sont modifiées.
 */

void vecteur_modifie ( vecteur v , float x , float y , float z );

/**
 * Calcule le déterminant d'une matrice formée par deux vecteurs.
 *
 * @param  v1 Premier vecteur.
 * @param  v2 Second vecteur.
 * @return Déterminant de la matrice.
 *
 * @pre    Les vecteurs v1 et v2 doivent être définies.
 * @post   Donne le déterminant de la matrice des vecteurs v1 et v2.
 *
 * @test   Tester avec des vecteurs quelconques.
 * @test   Tester avec le vecteur nul.
 * @test   Tester avec des vecteurs égaux.
 */

float determinant_2D ( const vecteur v1 , const vecteur v2 );

/**
 * Calcule le déterminant d'une matrice formée par trois vecteurs.
 *
 * @param  v1 Premier vecteur.
 * @param  v2 Deuxième vecteur.
 * @param  v3 Troisième vecteur.
 * @return Déterminant de la matrice.
 *
 * @pre    Les vecteurs v1, v2 et v3 doivent être définies.
 * @post   Donne le déterminant de la matrice des vecteurs v1, v2 et v3.
 *
 * @test   Tester avec des vecteurs quelconques.
 * @test   Tester avec le vecteur nul.
 * @test   Tester avec des vecteurs égaux.
 */

float determinant_3D ( const vecteur v1 , const vecteur v2 , const vecteur v3 );


/**
 * Supprime un vecteur (libère la mémoire).
 *
 * @param  v Vecteur à supprimer
 *
 * @pre    v est un vecteur valide.
 * @post   La mémoire utilisée par le vecteur v a été libérée
 */

#define     supprimer_vecteur(v)    liberer_mem(v)


/*@}*/

#endif
