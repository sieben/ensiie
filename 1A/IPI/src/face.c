/**
 * @file face.c
 * Implémentation du module des traitements liés à la manipulation des faces.
 * @author Sara Iraqi
 * @author Jean Crémèse
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "face.h"

/**
 * @addtogroup face
 */

/*@{*/

/**
 * Définition du type face.
 */

struct t_face
{
    sommet s1; ///< Premier sommet
    sommet s2; ///< Deuxième sommet
    sommet s3; ///< Troisième sommet
    vecteur normale; ///< Normale (éventuellement NULL)
    couleur c; ///< Couleur de la face.
} ;

/*@}*/

void afficher_face(face f)
{
    afficher_sommet(extraire_sommet(f,0));
    afficher_sommet(extraire_sommet(f,1));
    afficher_sommet(extraire_sommet(f,2));
    printf("normal de la face : ");
    afficher_vecteur(extraire_normale_f(f));
    afficher_couleur(extraire_couleur(f));
    printf("\n");
}

void afficher_couleur(couleur c)
{
    printf( "couleur de la face (%c, %c, %c)",
            extraire_coordonnee_couleur(c,0),
            extraire_coordonnee_couleur(c,1),
            extraire_coordonnee_couleur(c,2));
    printf("\n");
    
}


int bienforme_face(face f)
{
    if (f == NULL)
        return 0;

    // Si au moins l'un des sommets est mal formé, on renvoie 0
    if ( !bienforme_sommet(f->s1) ||
         !bienforme_sommet(f->s2) ||
         !bienforme_sommet(f->s3) ) return 0;
    else return 1;
}


point barycentre_face ( face f)
{
    if(!bienforme_face(f))
    {
        // Si la face est mal formée, on renvoie un message d'erreur
        fprintf(stderr, "barycentre_face : La face est mal formée.\n");
        exit(EXIT_FAILURE);
    }
    else
    {
        // On extrait les 3 sommets de la face qu'on transforme en point
        point p0=extraire_point (extraire_sommet(f,0));
        point p1=extraire_point (extraire_sommet(f,1));
        point p2=extraire_point (extraire_sommet(f,2));

        // On additionne les premières, deuxièmes et troisièmes coordonnées
        // entre elles et on divise la somme par 3
        float xg=(extraire_coordonnee_point(p0,0)+
                  extraire_coordonnee_point(p1,0)+
                  extraire_coordonnee_point(p2,0)) / 3.0f;
        float yg=(extraire_coordonnee_point(p0,1)+
                  extraire_coordonnee_point(p1,1)+
                  extraire_coordonnee_point(p2,1)) / 3.0f;
        float zg=(extraire_coordonnee_point(p0,2)+
                  extraire_coordonnee_point(p1,2)+
                  extraire_coordonnee_point(p2,2)) / 3.0f;

        // On retourne le point créé à partir des 3 nouveaux points
        return new_point (xg,yg,zg);
    }
}


sommet extraire_sommet(face f, int numero)
{
    if(numero<0 || numero>2)
    {
        // Si l'index du sommet n'est pas compris entre 0 et 2, on renvoie
        // un message d'erreur
        fprintf ( stderr , "extraire_sommet :  L'index du sommet n'est "
                           "pas compris entre 0 et 2.\n" );
        exit(EXIT_FAILURE);
    }
    else
    {
        // On renvoie le sommet correspondant à l'index passé en argument
        if(numero==0) return f->s1;
        else if(numero==1) return f->s2;
        else return f->s3;
    }
}


vecteur extraire_normale_f ( face f )
{
    // Si la normale n'est pas définie, on la calcule
    if ( f->normale == NULL )
    {
        f->normale = calcul_norm_face ( f );
    }

    return f->normale;
}


couleur extraire_couleur(face f)
{
    return f->c;
}

char extraire_coordonnee_couleur(couleur c, int numero)
{
    if ( c != NULL )
    {
        // On renvoie la coordonnée correspondant à l'index passé en argument
        if ( numero == 0 ) return c->r;
        if ( numero == 1 ) return c->v;
        if ( numero == 2 ) return c->b;

        fprintf ( stderr , "extraire_coordonnee_couleur : L'index de la "
                           "coordonnée n'est pas compris entre 0 et 2.\n" );
        exit ( EXIT_FAILURE );
    }

    fprintf ( stderr , "extraire_coordonnee_couleur : couleur invalide.\n" );
    exit ( EXIT_FAILURE );
}

couleur new_couleur(char c1, char c2, char c3){
    // On alloue de la mémoire à une structure dont on définit les composantes
    couleur c = allouer_mem(sizeof(struct t_couleur));
    c->r = c1;
    c->v = c2;
    c->b = c3;
    return c;
}


face new_face ( sommet s1, sommet s2, sommet s3,
                vecteur normale, couleur c )
{
    if (!bienforme_sommet(s1) ||!bienforme_sommet(s2)||!bienforme_sommet(s3))
    {
        // Si au moins un des sommets passé en argument est mal formé, on
        // renvoie un message d'erreur
        fprintf ( stderr , "new_face : Au moins un des sommets passé "
                           "en argument est mal formé.\n" );
        exit(EXIT_FAILURE);
    }

  
    // On alloue de la mémoire à une face puis on définit ses composantes
    face f=allouer_mem(sizeof(struct t_face));

    f->s1=s1;
    f->s2=s2;
    f->s3=s3;
    f->normale=normale;
    f->c=c;

    return f;
}

void liberer_mem_face(face f)
{
    if (f->normale != NULL)
        supprimer_vecteur(f->normale);

    if (f->c != NULL)
        liberer_mem(f->c);

    liberer_mem(f);
}

face modif_face_sommets(face f,sommet s1, sommet s2, sommet s3)
{
    if (!bienforme_sommet(s1) ||
        !bienforme_sommet(s2) ||
        !bienforme_sommet(s3))
    {
        // Si au moins un des sommets passé en argument est mal formé,
        // on renvoie un message d'erreur
        fprintf ( stderr ,  "modif_face_sommets : Au moins un des sommets "
                            "passé en argument est mal formé.\n" );
        exit(EXIT_FAILURE);
    }
    /*else if ( comp_point(extraire_point(s1),extraire_point(s2)) ||
              comp_point(extraire_point(s1),extraire_point(s3)) ||
              comp_point(extraire_point(s2),extraire_point(s3)) ) {
        // Si au moins 2 des sommets passé en argument ont leur point egaux,
        // on renvoie un message d'erreur
        fprintf ( stderr , "modif_face_sommets : Au moins 2 des sommets "
                           "passés en argument ont leur point égaux.\n" );
        exit(EXIT_FAILURE);
    } */ //////////////////////////////////////////////////////////////////ATTENTION
    else
    {
        // On remplace les sommets de la face
        f->s1=s1;
        f->s2=s2;
        f->s3=s3;
        return f;
    }
}

face modif_face_normale(face f, vecteur normale)
{
    // On remplace la normale de la face
    f->normale=normale;
    return f;
}


couleur couleur_default()
{
    // On définit une couleur "vierge" dont on définit les composantes RVB
    // à blanc
    couleur c = allouer_mem ( sizeof(struct t_couleur) );

    c->r=0xff;
    c->v=0xff;
    c->b=0xff;

    return c;
}


face modif_face_couleur(face f,couleur c)
{
    if(!bienforme_face(f))
    {
        // Si la face est mal formée, on renvoie un message d'erreur
        fprintf(stderr, "modif_face_couleur : La face est mal formée.\n");
        exit(EXIT_FAILURE);
    }
    else
    {
        // On modifie la couleur de la face
        f->c=c;
        return f;
    }
}


vecteur calcul_norm_face(face f)
{
    if(!bienforme_face(f))
    {
        // Si la face est mal formée, on renvoie un message d'erreur
        fprintf ( stderr , "calcul_norm_face : La face est mal formée.\n");
        exit(EXIT_FAILURE);
    }
    else
    {
        vecteur v0 = convertir_en_vecteur ( extraire_point ( extraire_sommet ( f , 0 ) ) );
        vecteur v1 = convertir_en_vecteur ( extraire_point ( extraire_sommet ( f , 1 ) ) );
        vecteur v2 = convertir_en_vecteur ( extraire_point ( extraire_sommet ( f , 2 ) ) );

        // Calcul de v1 - v0 et de v2 - v0
        vecteur_diff ( v0 , v1 );
        vecteur_diff ( v0 , v2 );

        vecteur v = vecteur_prod_vect ( v1 , v2 );

        supprimer_vecteur ( v0 );
        supprimer_vecteur ( v1 );
        supprimer_vecteur ( v2 );
/*
        // On extrait les normales des 3 sommets de la face
        vecteur n1 = extraire_normale(extraire_sommet(f,0));
        vecteur n2 = extraire_normale(extraire_sommet(f,1));
        vecteur n3 = extraire_normale(extraire_sommet(f,2));

        // On fait la somme de trois vecteurs
        vecteur v = vecteur_copie ( n1 );
        vecteur_add ( n2 , v );
        vecteur_add ( n3 , v );
*/
        vecteur_normalise ( v );

        return v;
    }
}


int comp_face_sommets(face f1,face f2)
{
    if (!bienforme_face(f1) || !bienforme_face(f2))
    {
        // Si au moins une des deux faces est mal formée, on renvoie un
        // message d'erreur
        fprintf(stderr, "au moins une des deux faces est mal formée");
        exit(EXIT_FAILURE);
    }
    else
    {
        // On compare les sommets de même indice des 2 faces
        //on renvoie 0 si au moins un des sommets n'est pas identique ou si les
        // normales sont non identiques.
        if(!comp_sommet(extraire_sommet(f1,0) , extraire_sommet(f2,0)) ||
           !comp_sommet(extraire_sommet(f1,1) , extraire_sommet(f2,1)) ||
           !comp_sommet(extraire_sommet(f1,2) , extraire_sommet(f2,2))
        )
            return 0;
        else return 1;
    }
}

int comp_face_points(face f1,face f2)
{
    int k = 0, i = 0;
    int nb_points_identiques = 0;

    for(k=0;k<=2;k++)
    {
        for(i=0;i<=2;i++)
        {
            if ( comp_point ( extraire_point(extraire_sommet(f1,k)),
                              extraire_point(extraire_sommet(f2,i)) ) )
                ++nb_points_identiques;
        }
    }

    return ( nb_points_identiques == 3 );
}


int comp_face(face f1,face f2)
{
    if (!bienforme_face(f1) || !bienforme_face(f2))
    {
        // Si au moins une des deux faces est mal formée, on renvoie un
        // message d'erreur
        fprintf ( stderr , "comp_face : Au moins une des deux faces "
                           "est mal formée.\n" );
        exit(EXIT_FAILURE);
    }
    else
    {
        // On compare les sommets de même indice des 2 faces ainsi que les
        // normales des faces, on renvoie 0
        // si au moins un des sommets n'est pas identique ou si les
        // normales sont non identiques.
        if(!comp_sommet(extraire_sommet(f1,0) , extraire_sommet(f2,0)) ||
           !comp_sommet(extraire_sommet(f1,1) , extraire_sommet(f2,1)) ||
           !comp_sommet(extraire_sommet(f1,2) , extraire_sommet(f2,2)) ||
           !vecteur_egaux(extraire_normale_f(f1),extraire_normale_f(f2)))
            return 0;
        else return 1;
    }
}


float courbure_deux_faces(face f1, face f2)
{
    if(!bienforme_face(f1)||!bienforme_face(f2))
    {
        fprintf ( stderr , "courbure_deux_faces : Au moins une des "
                           "faces est mal formée.\n" );
        exit(EXIT_FAILURE);
    }

    return fabsf(vecteur_angle(extraire_normale_f(f1),extraire_normale_f(f2)));
}
