/**
 * @file   maillages.c
 * @brief  Module de gestion des maillages (surfaces 3D).
 * @author Olivi Hermann
 * @author Michel Teddy
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "maillages.h"

/**
 * @addtogroup maillage
 */

/*@{*/

/* Prototype d'une fonction auxilliaire utilisée dans faces_voisines et
   faces_incidentes */

int estdansface ( maillage m , sommet s , unsigned int ind_f );

/* Il y a des s à la fin des noms pour les différencier des structures
   originelles */

/**
 * @struct t_maillage
 * Structure qui contiendra les maillages, sous la forme d'un tableau de
 * sommets et d'un tableau de faces.
 * Composantes :
 *      - un tableau (de type tableau) contenant les faces
 *      - un tableau (de type tableau) contenant les sommets
 */

struct t_maillage
{
    char * nom;      ///< Nom du maillage.
    tableau faces;   ///< Tableau des faces du maillage.
    tableau sommets; ///< Tableau des sommets du maillage.
};

maillage new_maillage ( const char * nom )
{
    maillage res;
    res = allouer_mem ( sizeof ( struct t_maillage ) );

    if (nom == NULL)
        res->nom = NULL;
    else
    {
        res->nom = allouer_mem ( sizeof(char) *  ( strlen ( nom ) + 1 ) );
        strcpy ( res->nom , nom );
    }

    res->faces = new_tableau();
    res->sommets = new_tableau();

    return res;
}

char * nom_maillage ( maillage m )
{
    return m->nom;
}

int ajouter_sommet ( maillage m , sommet s )
{
    if ( s == NULL )
    {
        fprintf ( stderr , "ajouter_sommet : Sommet vide.\n" );
        return -1;
    }

    if (recherche_tableau(m->sommets, s) == -1)
        return ajouter_element ( m->sommets , s );
    else
        return -1;
}

void supprimer_sommet ( maillage m , unsigned int indice_s )
{
    supprimer_element ( m->sommets , indice_s );
}

int ajouter_face ( maillage m , face f )
{
    if ( f == NULL)
    {
        fprintf ( stderr , "ajouter_face : face vide.\n" );
        return -1;
    }

    return ajouter_element ( m->faces , f );
}

void supprimer_face ( maillage m , unsigned int indice_f )
{
    supprimer_element ( m->faces , indice_f );
}

int nombre_de_sommets ( maillage m )
{
    return taille_tableau ( m->sommets );
}

sommet obtenir_sommet ( maillage m , unsigned int indice )
{
    return acceder_element ( m->sommets , indice , sommet );
}

int indice_sommet ( maillage m , sommet s )
{
    int indice;
    const int taille = nombre_de_sommets ( m );

    // Parcourt du tableau des sommets
    for ( indice = 0 ; indice < taille ; ++indice )
    {
        if ( s == acceder_element(m->sommets,indice,sommet) )
        {
            return indice;
        }
    }

    return -1;
}

void modifier_sommet ( maillage m , sommet nouveau , unsigned int indice )
{
    remplacer_element ( m->sommets , indice , nouveau );
}

int nombre_de_faces ( maillage m )
{
    return taille_tableau ( m->faces );
}

face obtenir_face ( maillage m , unsigned int indice )
{
    return acceder_element ( m->faces , indice , face );
}

int indice_face ( maillage m , face f )
{
    int indice;
    const int taille = nombre_de_faces ( m );

    // Parcourt du tableau des sommets
    for ( indice = 0 ; indice < taille ; ++indice )
    {
        if ( f == acceder_element( m->faces, indice, face ) )
        {
            return indice;
        }
    }

    return -1;
}


void modifier_face ( maillage m , face nouvelle , unsigned int indice )
{
    remplacer_element ( m->faces , indice , nouvelle );
}


tableau * sommets_voisins ( maillage m , unsigned int indice ,
                            unsigned int niveau )
{
    // La valeur de niveau est incorrecte
    if ( niveau > 5 )
    {
        fprintf ( stderr, "sommets_voisins : La valeur de niveau "
                          "est incorrecte : %d\n" , niveau );
        return NULL;
    }

    if ( niveau == 0 )
    {
        tableau * tab = malloc ( sizeof(tableau) );
        tab[0] = new_tableau();

        // Voisin de niveau 0 (le sommet lui-même)
        ajouter_element ( tab[0] , (long) indice );

        return tab;
    }

    if ( niveau == 1 )
    {
        tableau * tab = allouer_mem ( 2 * sizeof(tableau) );
        tab[0] = new_tableau();
        tab[1] = new_tableau();

        // Voisin de niveau 0 (le sommet lui-même)
        ajouter_element ( tab[0] , (long) indice ); 

        int nbr_faces = taille_tableau ( m->faces );
        int i = 0;

        // Tableau des arêtes dont les extrémités sont les voisins de niveau 1
        tableau aretes = new_tableau();

        // Pour chaque face du maillage
        for ( i = 0 ; i < nbr_faces ; ++i )
        {
            sommet s = acceder_element ( m->sommets , indice , sommet );
            face f = acceder_element ( m->faces , i , face );

            sommet s0 = extraire_sommet ( f , 0 );
            sommet s1 = extraire_sommet ( f , 1 );
            sommet s2 = extraire_sommet ( f , 2 );

            // On cherche si la face contient le sommet demandé
            if ( s0 == s )
            {
                long indice1 = indice_sommet ( m , s1 );
                long indice2 = indice_sommet ( m , s2 );

                // On ajoute les deux autres sommets de la face au tableau
                ajouter_element ( aretes , indice1 );
                ajouter_element ( aretes , indice2 );
            }
            else if ( s1 == s )
            {
                long indice2 = indice_sommet ( m , s2 );
                long indice0 = indice_sommet ( m , s0 );

                // On ajoute les deux autres sommets de la face au tableau
                ajouter_element ( aretes , indice2 );
                ajouter_element ( aretes , indice0 );
            }
            else if ( s2 == s )
            {
                long indice0 = indice_sommet ( m , s0 );
                long indice1 = indice_sommet ( m , s1 );

                // On ajoute les deux autres sommets de la face au tableau
                ajouter_element ( aretes , indice0 );
                ajouter_element ( aretes , indice1 );
            }
        }

        int nbr_aretes = taille_tableau ( aretes ) / 2;

        // Tri du tableau des arêtes pour que les sommets se suivent
        if ( nbr_aretes > 2 )
        {
            for ( i = 0 ; i < nbr_aretes - 1 ; ++i )
            {
                // Somme de l'extrémité de l'arête
                long s = acceder_element ( aretes , 2 * i , long );
                int j = i + 1;

                for ( ; j < nbr_aretes ; ++j )
                {
                    // Arête qui part du sommet s
                    if ( acceder_element ( aretes , 2 * j , long ) == s &&
                         i + 1 != j )
                    {
                        // On échange les éléments i+1 et j du tableau
                        long s_tmp = acceder_element ( aretes ,
                                                        2 * j + 1 , long );

                        remplacer_element ( aretes , 2 * j ,
                            acceder_element ( aretes , 2 * (i+1) , long ) );

                        remplacer_element ( aretes , 2 * j + 1 ,
                            acceder_element ( aretes , 2 * (i+1) + 1 , long ) );

                        remplacer_element ( aretes , 2 * (i+1) , s );
                        remplacer_element ( aretes , 2 * (i+1) + 1 , s_tmp );
                    }
                }
            }
        }

        // Suppression des doublons du tableau des voisins de niveau 1
        tableau voisins = supprime_doublons ( aretes );

        fusionne_tableaux ( tab[1] , voisins );
        liberer_mem ( voisins );

        return tab;
    }

    // On cherche les voisins de niveau n - 1
    tableau * tab = sommets_voisins ( m , indice , niveau - 1 );
    tableau voisins = new_tableau();

    int i;
    int taille = taille_tableau ( tab[niveau - 1] );

    // Pour chaque voisin de niveau n - 1
    for ( i = 0 ; i < taille ; ++i )
    {
        // Recherche des voisins de niveau 1
        tableau * t = sommets_voisins ( m ,
                acceder_element ( tab[niveau - 1] , i , long ) , 1 );

        // Ajout au tableau des voisins
        fusionne_tableaux ( voisins , t[1] );

        // Suppression du tableau temporaire
        liberer_mem ( t );

        // Suppression des doublons
        tableau voisins_tmp = supprime_doublons ( voisins );
        voisins = voisins_tmp;
    }

    // Suppression des sommets présents dans les tableaux de voisins de
    // niveaux inférieurs à n
    for ( i = 0 ; i < (int) niveau ; ++i )
    {
        tableau voisins_tmp = supprime_elements ( voisins , tab[i] );
        voisins = voisins_tmp;
    }

    tab = reallouer_mem ( tab , ( niveau + 1 ) * sizeof(tableau) );

    // Ajoute des voisins de niveau n au tableau de tableaux de voisins
    tab[niveau] = voisins;

    return tab;
}

int sont_voisins ( maillage m , sommet s1 , sommet s2 )
{
    if (m == NULL || s1 == NULL || s2 == NULL)
        return 0;

    int indice_s1 = recherche_tableau(m->sommets, s1);
    int indice_s2 = recherche_tableau(m->sommets, s2);
    if (indice_s1 == -1 || indice_s2 == -1)
        return 0;

    tableau *tmp = sommets_voisins(m, indice_s1, 1);

    int retour = (recherche_tableau(tmp[1], indice_s2) != -1);

    supprimer_tableau(tmp[0]);
    supprimer_tableau(tmp[1]);
    liberer_mem(tmp);

    return retour;
}

void free_maillage ( maillage m )
{
    if (m == NULL)
        return;

    int i = 0;

    if (m->faces != NULL)
    {
        for (i = 0 ; i < taille_tableau(m->faces) ; i++)
            liberer_mem_face(acceder_element(m->faces, i, face));

        supprimer_tableau ( m->faces );
    }

    if (m->sommets != NULL)
    {
        for (i = 0 ; i < taille_tableau(m->sommets) ; i++)
            liberer_mem_sommet(acceder_element(m->sommets, i, sommet));

        supprimer_tableau ( m->sommets );
    }

    if (m->nom != NULL)
        liberer_mem ( m->nom );

    liberer_mem ( m );
}

// Teste si le sommet s appartient à la face d'indice ind_f (test sur le point
// du sommet uniquement)
// Fonction auxilliaire utilisée dans faces_voisines et faces_incidentes

int estdansface ( maillage m , sommet s , unsigned int ind_f )
{
    int l;
    face f = obtenir_face ( m , ind_f ); // f: face d'indice ind_f
    point p0 = extraire_point ( s );

    for ( l = 0 ; l <= 2 ; l++ ) //parcours sommets de la face f
    {
        point pf = extraire_point ( extraire_sommet ( f , l ) );

        if ( pf==p0 ) return 1;
    }

    return 0;
}


tableau faces_voisines ( maillage m , unsigned int indice_f )
{
    tableau voisines = new_tableau();

    const int nbf = nombre_de_faces ( m );
    int k = 0;
    face f = obtenir_face ( m , indice_f );

    sommet s0 = extraire_sommet ( f , 0 );
    sommet s1 = extraire_sommet ( f , 1 );
    sommet s2 = extraire_sommet ( f , 2 );

    // On parcourt le tableau de faces du maillage
    for ( k = 0; k < nbf ; ++k )
    {
        face fk = obtenir_face ( m , k );

        // Si la face k a une arête commune avec f
        // et qu'elle est différente de f, c'est une voisine de f
        if ( ( ( estdansface ( m , s0 , k ) && estdansface ( m , s1 , k ) )
            || ( estdansface ( m , s1 , k ) && estdansface ( m , s2 , k ) )
            || ( estdansface ( m , s0 , k ) && estdansface ( m , s2 , k ) ) )
            && !comp_face_points ( f , fk ) ) ajouter_element ( voisines , fk );
    }

    return voisines;
}

tableau faces_incidentes ( maillage m , unsigned int indice_s )
{
    int nbf = nombre_de_faces ( m );
    int k;
    sommet s = obtenir_sommet ( m , indice_s );
    tableau incidentes = new_tableau();

    // On parcourt le tableau de faces du maillage
    for ( k = 0 ; k < nbf ; ++k )
    {
        // Si le sommet s est dans la face k, c'est une face incidente à s
        if ( estdansface ( m , s , k ) )
            ajouter_element ( incidentes , obtenir_face ( m , k ) );
    }

    return incidentes;
}

void calculer_normale_sommet ( maillage m , unsigned int indice_s )
{
    tableau faces_inc = faces_incidentes ( m , indice_s );
    sommet s = obtenir_sommet ( m , indice_s );

    int i, n = taille_tableau ( faces_inc );

    tableau normales_faces = new_tableau();

    // Pour chaque face voisine, on ajoute la normale au tableau sans doublons
    for ( i = 0 ; i < n ; ++i )
    {
        int j;
        bool existe = false;
        vecteur norm = extraire_normale_f (
                           acceder_element ( faces_inc , i , face ) );

        // On cherche la normale de la face dans le tableau des normales
        for ( j = 0 ; j < taille_tableau ( normales_faces ) ; ++j )
        {
            if ( vecteur_egaux ( acceder_element ( normales_faces, j, vecteur ),
                 norm ) ) existe = true;
        }

        if ( existe == false )
        {
            ajouter_element ( normales_faces , norm );
        }
    }

    supprimer_tableau ( faces_inc );

    n = taille_tableau ( normales_faces );

    vecteur normale = extraire_normale ( s );
    if ( normale ) supprimer_vecteur ( normale );
    normale = vecteur_nouveau ( 0.0f , 0.0f , 0.0f );

    // Pour chaque normale des faces voisines
    for ( i = 0 ; i < n ; ++i )
    {
        vecteur_add ( acceder_element ( normales_faces, i, vecteur ), normale );
    }

    supprimer_tableau ( normales_faces );

    vecteur_normalise ( normale );
    modif_norm_sommet ( s , normale );

    return;
}

void scale_maillage ( maillage m , float scale )
{
    // Facteur d'aggrandissement incorrect
    if ( scale <= 0.0f ) return;

    int i;
    const int nbr_sommets = nombre_de_sommets ( m );

    // On parcourt la liste des sommets
    for ( i = 0 ; i < nbr_sommets ; ++i )
    {
        point p = extraire_point ( obtenir_sommet ( m , i ) );

        float p1 = extraire_coordonnee_point ( p , 0 );
        float p2 = extraire_coordonnee_point ( p , 1 );
        float p3 = extraire_coordonnee_point ( p , 2 );

        modif_point ( p , p1 * scale , p2 * scale , p3 * scale );
    }

    return;
}

/// Valeur absolue d'un nombre
#define _abs(A)    ( ( (A) < 0 ) ? -(A) : (A) )

/// Maximum de deux nombres
#define _max(A,B)  ( ( (A) > (B) ) ? (A) : (B) )


float maillages_unitize (  maillage m )
{
    float maxx, minx, maxy, miny, maxz, minz;
    bool first = true;
    int j;
    int nbr_sommets = nombre_de_sommets ( m );

    // On parcourt la liste des sommets
    for ( j = 0 ; j < nbr_sommets ; ++j )
    {
            point p = extraire_point ( obtenir_sommet ( m , j ) );

            float p1 = extraire_coordonnee_point ( p , 0 );
            float p2 = extraire_coordonnee_point ( p , 1 );
            float p3 = extraire_coordonnee_point ( p , 2 );

            if ( first )
            {
                minx = maxx = p1;
                miny = maxy = p2;
                minz = maxz = p3;

                first = false;
            }
            else
            {
                if ( minx > p1 ) minx = p1;
                if ( miny > p2 ) miny = p2;
                if ( minz > p3 ) minz = p3;

                if ( maxx < p1 ) maxx = p1;
                if ( maxy < p2 ) maxy = p2;
                if ( maxz < p3 ) maxz = p3;
            }
        }

    // Calcul des dimensions des maillages
    float w = _abs ( maxx ) + _abs ( minx );
    float h = _abs ( maxy ) + _abs ( miny );
    float d = _abs ( maxz ) + _abs ( minz );

    // Calcul du centre des maillages
    float cx = ( maxx + minx ) / 2.0f;
    float cy = ( maxy + miny ) / 2.0f;
    float cz = ( maxz + minz ) / 2.0f;

    // Facteur d'aggrandissement
    float scale = 2.0f / _max ( _max ( w , h ) , d );

        // On parcourt la liste des sommets
        for ( j = 0 ; j < nbr_sommets ; ++j )
        {
            point p = extraire_point ( obtenir_sommet ( m , j ) );

            float p1 = extraire_coordonnee_point ( p , 0 );
            float p2 = extraire_coordonnee_point ( p , 1 );
            float p3 = extraire_coordonnee_point ( p , 2 );

            modif_point ( p , ( p1 - cx ) * scale ,
                              ( p2 - cy ) * scale ,
                              ( p3 - cz ) * scale );
        }

    return scale;
}

/*@}*/
