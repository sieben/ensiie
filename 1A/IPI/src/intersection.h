/**
 * @file intersection.h
 * @brief Module de détection des intersections de face.
 * @author Leone Remy
 */

#ifndef INTERSECTION_H
#define INTERSECTION_H

#include "maillages.h"
#include "affichagencurses.h"

/**
 * @defgroup intersection
 * @brief Détection des intersections de triangles.
 * @author Leone Remy
 */

/*@{*/

/**
 * Teste si deux triangles se coupent dans l'espace.
 *
 * @param p1 1er point du premier triangle
 * @param q1 2nd point du premier triangle
 * @param r1 3ème point du premier triangle
 * @param p2 1er point du second triangle
 * @param q2 2nd point du second triangle
 * @param r2 3ème point du second triangle
 * @pre Les trois points sont bien formés
 * @post Est ce que les triangles se coupent ou non.
 * @return Booléen.
 * @test Les triangles ne sont pas coplanaires et se coupent.
 * @test Les triangles ne sont pas coplanaires et ne se coupent pas.
 * @test Les triangles ont une arête commune
 * @test Les triangles ont un point commun
 */

int tri_tri_overlap_test_3d( float p1[3], float q1[3], float r1[3],
                             float p2[3], float q2[3], float r2[3] );


/**
 * Fonction appellée quand deux triangles sont coplanaires
 *
 * @param p1 Premier point du premier triangle
 * @param q1 Second point du premier triangle
 * @param r1 Troisième point du premier triangle
 * @param p2 Premier point du second triangle
 * @param q2 Second point du second triangle
 * @param r2 Troisième point du second triangle
 * @param N1
 * @param N2
 * @pre Les points doivent être bien formés
 * @post Un float du même signe que l'orientation est retourné
 * @return Renvoie un nombre qui a le même signe que l'orientation du triangle
 * @test Triangle dans le sens trigonométrique
 * @test Triangle dans le sens anti-trigonométrique
 * @test Triangle mal formé (Réduit à un point).
 * @test Triangle mal formé (Réduit à une ligne).
 */

int coplanar_tri_tri3d( float  p1[3], float  q1[3], float  r1[3],
                        float  p2[3], float  q2[3], float  r2[3],
                        float  N1[3], float  N2[3]);

/**
 *
 *
 * @param p1 1er point du premier triangle
 * @param q1 2nd point du premier triangle
 * @param r1 3ème point du premier triangle
 * @param p2 1er point du second triangle
 * @param q2 2nd point du second triangle
 * @param r2 3ème point du second triangle
 * @pre
 * @post
 * @return
 * @test
 */

int ccw_tri_tri_intersection_2d(float p1[2], float q1[2], float r1[2],
                float p2[2], float q2[2], float r2[2]);

/**
 * Teste si deux triangles se chevauchent.
 *
 * @param p1 1er point du premier triangle
 * @param q1 2nd point du premier triangle
 * @param r1 3ème point du premier triangle
 * @param p2 1er point du second triangle
 * @param q2 2nd point du second triangle
 * @param r2 3ème point du second triangle
 * @pre  Les deux triangles sont composés de points à deux coordonnées et sont
 *       bien formées.
 * @post On sait si les triangles se chevauchent ou non.
 * @return Booléen.
 * @test Les triangles se chevauchent.
 * @test Les triangles sont disjoints.
 * @test Les triangles ont une arête commune.
 * @test Les triangles ont un sommet commun.
 * @test Les triangles sont égaux.
 */

int tri_tri_overlap_test_2d(float p1[2], float q1[2], float r1[2],
                            float p2[2], float q2[2], float r2[2]);

/**
 * @param p1 1er point du premier triangle
 * @param q1 2nd point du premier triangle
 * @param r1 3ème point du premier triangle
 * @param p2 1er point du second triangle
 * @param q2 2nd point du second triangle
 * @param r2 3ème point du second triangle
 * @param coplanar
 * @param source
 * @param target
 * @return
 *
 * @pre  Les deux triangles sont composés de points à deux coordonnées et sont
 *       bien formées.
 * @post On sait si les triangles se chevauchent ou non.
 * @test Les triangles se chevauchent.
 * @test Les triangles sont disjoints.
 * @test Les triangles ont une arête commune.
 * @test Les triangles ont un sommet commun.
 * @test Les triangles sont égaux.
 *
*/


int tri_tri_intersection_test_3d( float p1[3], float q1[3], float r1[3],
                                  float p2[3], float q2[3], float r2[3],
                                  int * coplanar,
                                  float source[3],float target[3]);


/**
 * @param p1 1er point du premier triangle
 * @param q1 2nd point du premier triangle
 * @param r1 3ème point du premier triangle
 * @param p2 1er point du second triangle
 * @param q2 2nd point du second triangle
 * @param r2 3ème point du second triangle
 * @return 1 si il y a intersection 0 sinon
 *
 * @pre  Les points sont bien formés
 * @post On sait si les triangles se chevauchent ou non.
 * @test Les triangles se chevauchent.
 * @test Les triangles sont disjoints.
 * @test Les triangles ont une arête commune.
 * @test Les triangles ont un sommet commun.
 * @test Les triangles sont égaux.
 *
 */

int intersection( point p1 , point q1 , point r1 ,
          point p2 , point q2 , point r2 );

/**
 * @param m maillage 
 * 
 * @pre Le maillage est bien formé
 * @post Les faces qui se coupent sont coloriées en rouge
 * 
 * @test Des faces qui se coupent sont dans le maillage
 * @test Le maillage ne comporte pas de faces qui se coupent 
 * */

void affiche_intersection ( maillage m );

/*@}*/

#endif
