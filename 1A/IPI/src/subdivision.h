/**
 * @file subdivision.h
 * @brief Fonctions de subdivision des maillages.
 * @author Lasnier Simon
 * @author Zhong Chaoran
 */

#ifndef SUBDIVISION
#define SUBDIVISION

#include "maillages.h"

/**
 * @defgroup subdivision Subdivision
 * @brief    Fonctions de subdivision des maillages.
 * @author Lasnier Simon
 * @author Zhong Chaoran
 */

/*@{*/

/**
 * Subdivise de manière plane l'ensemble des faces d'un maillage.
 *
 * @param m Maillage
 * @pre m est un maillage valide
 * @post toutes les faces du maillage sont subdivisées
 * @test tester la subdivision sur un maillage courbé (par exemple une
 *       demi-sphère)
 * @test tester la subdivision sur un maillage plat (un plan)
 */

maillage subdivision_plane ( maillage m );


/**
 * Subdivise avec utilisation des normales l'ensemble des faces d'un maillage.
 *
 * @param m Maillage
 * @pre m est un maillage valide
 * @post toutes les faces du maillage sont subdivisées
 * @test tester la subdivision sur un maillage courbé (par exemple une
 *       demi-sphère)
 * @test tester la subdivision sur un maillage normal (un plan)
 */

maillage subdivision_normale ( maillage m );


/**
 * Subdivise avec utilisation des normales l'ensemble des faces d'un maillage.
 *
 * @param m Maillage
 * @pre m est un maillage valide
 * @post toutes les faces du maillage sont subdivisées
 * @test tester la subdivision sur un maillage courbé (par exemple une
 *       demi-sphère)
 * @test tester la subdivision sur un maillage normal (un plan)
 */

maillage subdivision_normale_bis ( maillage m );

/*@}*/

#endif
