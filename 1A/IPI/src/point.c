/**
 * @file point.c
 * @brief Module contenant l'implantation des données et des traitements liés
 *        à la manipulation des points
 * @author Iraqi Sara
 * @author Crémèse Jean
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "point.h"

void afficher_point(const point p)
{
    printf("(%.2f, %.2f, %.2f)\n",
        extraire_coordonnee_point(p,0),
        extraire_coordonnee_point(p,1),
        extraire_coordonnee_point(p,2));
    printf("\n");
}

float extraire_coordonnee_point (point p, int numero)
{
    if ( p != NULL )
    {
        // On renvoie la coordonnée correspondant à l'index passé en argument
        if ( numero == 0 ) return p->x;
        if ( numero == 1 ) return p->y;
        if ( numero == 2 ) return p->z;

        fprintf ( stderr , "extraire_coordonnee_point : L'index de la "
                           "coordonnée n'est pas compris entre 0 et 2.\n" );
        exit ( EXIT_FAILURE );
    }

    fprintf ( stderr , "extraire_coordonnee_point : point invalide.\n" );
    exit ( EXIT_FAILURE );
}

int bienforme_point ( point p )
{
    return (p != NULL);
}

point new_point(float x, float y, float z)
{
    // On alloue de la mémoire à un point dont on définit les composantes
    point p = allouer_mem(sizeof(struct t_point));
    p->x = x;
    p->y = y;
    p->z = z;
    return p;
}

point modif_point(point p,float x1,float y1,float z1)
{
    if(!bienforme_point(p))
    {
        // Si le point est mal formé, on renvoit une erreur
        fprintf ( stderr, "modif_point : Le point est mal formé.\n");
        exit(EXIT_FAILURE);
    }

    // On modifie les composantes du point
    p->x = x1;
    p->y = y1;
    p->z = z1;

    return p;
}


int comp_point ( point p1 , point p2 )
{
    if ( !bienforme_point(p1) || !bienforme_point(p2) )
    {
        // Si au moins un des deux point est mal formé, on renvoit une erreur
        fprintf ( stderr , "comp_point : Au moins un des deux point "
                           "est mal formé.\n" );
        exit(EXIT_FAILURE);
    }

    return (((p1->x)==(p2->x))&&((p1->y)==(p2->y))&&((p1->z)==(p2->z)));

}


float distance_point ( point p1 , point p2 )
{
    if(!bienforme_point(p1) || !bienforme_point(p2))
    {
        // Si au moins un des deux point est mal formé, on renvoit une erreur
        fprintf ( stderr , "distance_point : Au moins un des deux "
                           "points est mal formé.\n" );
        exit(EXIT_FAILURE);
    }
    else
    {
        // On calcule la différence entre les coordonnées de même index des
        // 2 points
        float x,y,z;
        x=extraire_coordonnee_point(p2,0)-extraire_coordonnee_point(p1,0);
        y=extraire_coordonnee_point(p2,1)-extraire_coordonnee_point(p1,1);
        z=extraire_coordonnee_point(p2,2)-extraire_coordonnee_point(p1,2);

        // On retourne la racine carrée de la somme des carrés des 3
        // coordonnées créées
        return(sqrt(x*x+y*y+z*z));
    }
}
