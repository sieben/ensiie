\beamer@endinputifotherversion {3.07pt}
\select@language {french}
\beamer@sectionintoc {1}{Structures de donn\IeC {\'e}es \& Vecteur}{3}{1}{1}
\beamer@sectionintoc {2}{Gestion des entr\IeC {\'e}es / Sorties}{5}{1}{2}
\beamer@sectionintoc {3}{Maillages}{7}{1}{3}
\beamer@sectionintoc {4}{Correction des erreurs}{9}{1}{4}
\beamer@subsectionintoc {4}{1}{Faces mal orient\IeC {\'e}es}{11}{1}{4}
\beamer@subsectionintoc {4}{2}{Intersection}{12}{1}{4}
\beamer@subsectionintoc {4}{3}{Recheche des sommets non-manifold}{13}{1}{4}
\beamer@subsectionintoc {4}{4}{Faces mal orient\IeC {\'e}es}{14}{1}{4}
\beamer@sectionintoc {5}{Filtrage}{15}{1}{5}
\beamer@sectionintoc {6}{Subdivisions}{17}{1}{6}
\beamer@sectionintoc {7}{Qualit\IeC {\'e}}{19}{1}{7}
\beamer@subsectionintoc {7}{1}{Coloration qualit\IeC {\'e}}{20}{1}{7}
\beamer@sectionintoc {8}{Interfaces graphiques}{22}{1}{8}
\beamer@subsectionintoc {8}{1}{Interface console (Ncurses)}{23}{1}{8}
\beamer@subsectionintoc {8}{2}{Interface graphique (Glut)}{24}{1}{8}
