/**
 * @file fichiers_obj.h
 * @brief Module de traitement des fichiers *.obj
 * @author Simon Lasnier
 */

#ifndef FICHIERS_OBJ
#define FICHIERS_OBJ

#include "maillages.h"

/**
 * @defgroup fichier_obj Fichiers Obj
 * @brief Module de traitement des fichier .obj
 * @author Simon Lasnier
 */

/*@{*/

/**
 * @brief Charger des maillages depuis un fichier *.obj
 * @param fichier adresse du fichier *.obj
 * @pre le fichier *.obj doit exister et être un véritable fichier *.obj
 * @post le maillage chargé est renvoyé par la fonction
 * @return le maillage chargé
 * @test charger les maillages à partir d'un fichier inexistant
 * @test charger les maillages à partr d'un fichier inaccessible en lecture
 * @test charger les maillages à partir d'un fichier vide
 * @test charger les maillages à partir d'un fichier erroné (mal écrit)
 */

maillage charger_maillage (const char *fichier) ;

/**
 * @brief Enregistrer un maillage dans un fichier .obj
 * @param fichier adresse du fichier *.obj
 * @param m un maillage chargé par la fonction charger_maillage ou créé
 * à la main.
 * @pre un tableau de maillages valide et un nom correct pour le fichier .obj
 * @post le maillages founi en paramètre est enregistré dans le fichier, et si
 * ce dernier existe déjà il sera écrasé.
 * @test enregistrer un maillage dans un fichier déja existant
 * @test enregistrer un maillage dans un dossier inaccessible en écriture
 */

void enregistrer_maillage (const char *fichier, const maillage m) ;

/*@}*/

#endif
