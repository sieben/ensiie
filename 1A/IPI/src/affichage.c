/**
 * @file   affichage.c
 * @brief  Module d'affichage des maillages avec OpenGL.
 * @author Michel Teddy
 */


#include <stdio.h>
#include <string.h>
#include <math.h>
#include <dirent.h>
#include <GL/glut.h>

#include "affichage.h"
#include "fichiers_obj.h"
#include "detection_correction.h"
#include "filtrage.h"
#include "subdivision.h"
#include "qualite.h"
#include "intersection.h"


/**
 * @addtogroup affichage
 */


/****************************************
 *          Variables globales          *
 ****************************************/

unsigned int gltb_lasttime = 0;
float gltb_lastposition[3];
float gltb_angle = 0.0f;
float gltb_axis[3];
float gltb_transform[4][4];
unsigned int gltb_width = 0;
unsigned int gltb_height = 0;
int gltb_button = -1;
bool tracking = false;
double pan_x = 0.0f;
double pan_y = 0.0f;
double pan_z = 0.0f;
int mouse_state = 0;
int mouse_button = 0;
char * model_file = NULL;
unsigned int model_list = 0;
maillage m_current = NULL;
float scale = 1.0f;
bool facetnormal = false;
bool drawnormales = false;
bool drawrepere = false;
int entries = 0;


/****************************************
 *              Prototypes              *
 ****************************************/

void draw_maillages ( void );
void _gltbPointToVector ( int x , int y , int width , int height , float v[3] );
void _gltbAnimate ( void );
void _gltbStartMotion ( int x , int y , int time );
void _gltbStopMotion ( unsigned time );
void gltbInit ( unsigned int button );
void gltbMatrix ( void );
void glut_reshape ( int width , int height );
void glut_mouse ( int button , int state , int x , int y );
void glut_motion ( int x , int y );
void glut_keyboard ( unsigned char key , int x , int y );
void glut_display ( void );
void menu ( int item );


bool est_un_dossier ( const char * path )
{
    DIR * rep = opendir ( path );
    bool isdir = false;

    if ( rep )
    {
        struct dirent * mydir = NULL;

        if ( ( mydir = readdir ( rep ) ) )
        {
            isdir = true;
        }

        closedir ( rep );
    }

    return isdir;
}

dossier liste_dossier ( const char * path )
{
    dossier dir = NULL;
    DIR * rep = opendir ( path );

    if ( rep )
    {
        struct dirent * mydir = NULL;
        char srcfile[256];
        fichier old_fichier = NULL;
        dossier old_dossier = NULL;

        dir = allouer_mem ( sizeof(struct t_dossier) );

        dir->name = allouer_mem ( strlen ( path ) + 1 );
        strcpy ( dir->name , path );
        dir->files = NULL;
        dir->dirs = NULL;
        dir->next = NULL;

        // Pour chaque fichier du dossier
        while ( ( mydir = readdir ( rep ) ) )
        {
            // On élimine les fichiers cachés
            if ( mydir->d_name[0] != '.' )
            {
                sprintf ( srcfile , "%s/%s" , path , mydir->d_name );

                // Dossier
                if ( est_un_dossier ( srcfile ) )
                {
                    dossier sdossier = liste_dossier ( srcfile );

                    sdossier->name = reallouer_mem ( sdossier->name ,
                                                 strlen ( mydir->d_name ) + 1 );
                    strcpy ( sdossier->name , mydir->d_name );

                    // On modifie le dossier précédent
                    if ( old_dossier )
                    {
                        old_dossier->next = sdossier;
                    }
                    // On modifie le premier dossier du dossier
                    else
                    {
                        dir->dirs = sdossier;
                    }

                    old_dossier = sdossier;
                }
                // Fichier
                else
                {
                    // Si le fichier ne porte pas l'extension OBJ, on continue
                    if ( strstr ( mydir->d_name , ".obj" ) == NULL )
                    {
                        continue;
                    }

                    fichier sfichier = allouer_mem ( sizeof(struct t_fichier) );

                    sfichier->name = allouer_mem ( strlen (mydir->d_name) + 1 );
                    strcpy ( sfichier->name , mydir->d_name );
                    sfichier->next = NULL;

                    // On modifie le fichier précédent
                    if ( old_fichier )
                    {
                        old_fichier->next = sfichier;
                    }
                    // On modifie le premier fichier du dossier
                    else
                    {
                        dir->files = sfichier;
                    }

                    old_fichier = sfichier;
                }
            }
        }

        closedir ( rep );
    }

    return dir;
}

void supprimer_liste_dossier ( dossier racine )
{
    if ( racine )
    {
        // Suppression du nom du dossier
        if ( racine->name )
        {
            liberer_mem ( racine->name );
        }

        fichier file = racine->files;

        // Suppression des fichiers
        while ( file )
        {
            fichier tmp = file->next;
            liberer_mem ( file->name );
            liberer_mem ( file );
            file = tmp;
        }

        // Suppression des sous-dossiers
        supprimer_liste_dossier ( racine->dirs );

        // Suppression des dossiers voisins
        if ( racine->next )
        {
            supprimer_liste_dossier ( racine->next );
        }

        liberer_mem ( racine );
    }

    return;
}

void draw_maillages ( void )
{
    if ( model_list )
    {
        glDeleteLists ( model_list , 1 );
    }

    model_list = glGenLists ( 1 );
    glNewList ( model_list , GL_COMPILE );

    // Couleur blanche par défaut
    glColor3ub ( 0 , 0 , 0 );

    glBegin ( GL_TRIANGLES );

    int i;
    const int nbr_triangles = nombre_de_faces ( m_current );

    // Pour chaque triangle
    for ( i = 0 ; i < nbr_triangles ; ++i )
    {
        face f = obtenir_face ( m_current , i );

        // Couleur du triangle
        couleur c = extraire_couleur ( f );
        if ( c ) glColor3ub ( c->r , c->v , c->b );
        else glColor3ub ( 0 , 0 , 0 );

        sommet s1 = extraire_sommet ( f , 0 );
        sommet s2 = extraire_sommet ( f , 1 );
        sommet s3 = extraire_sommet ( f , 2 );

        // Utilisation des normales des faces
        if ( facetnormal )
        {
            vecteur n = extraire_normale_f ( f );
            if ( n ) glNormal3fv ( (GLfloat *) n );

            glVertex3fv ( (GLfloat *) extraire_point ( s1 ) );
            glVertex3fv ( (GLfloat *) extraire_point ( s2 ) );
            glVertex3fv ( (GLfloat *) extraire_point ( s3 ) );
        }
        // Utilisation des normales des sommets
        else
        {
            vecteur n1 = extraire_normale ( s1 );
            vecteur n2 = extraire_normale ( s2 );
            vecteur n3 = extraire_normale ( s3 );

            if ( n1 == NULL )
            {
                calculer_normale_sommet ( m_current ,
                                          indice_sommet ( m_current , s1 ) );
                n1 = extraire_normale ( s1 );
            }

            if ( n1 ) glNormal3fv ( (GLfloat *) n1 );
            glVertex3fv ( (GLfloat *) extraire_point ( s1 ) );

            if ( n2 == NULL )
            {
                calculer_normale_sommet ( m_current ,
                                          indice_sommet ( m_current , s2 ) );
                n2 = extraire_normale ( s2 );
            }

            if ( n2 ) glNormal3fv ( (GLfloat *) n2 );
            glVertex3fv( (GLfloat *) extraire_point ( s2 ) );

            if ( n3 == NULL )
            {
                calculer_normale_sommet ( m_current ,
                                          indice_sommet ( m_current , s3 ) );
                n3 = extraire_normale ( s3 );
            }

            if ( n3 ) glNormal3fv ( (GLfloat *) n3 );
            glVertex3fv( (GLfloat *) extraire_point ( s3 ) );
        }
    }

    glEnd();


    // Affichage du repère
    if ( drawrepere )
    {
        glColor3ub ( 0 , 255 , 0 );
        glBegin ( GL_LINES );
        glVertex3f ( 0,0,0 );
        glVertex3f ( 0,0,2 );
        glEnd();

        glColor3ub ( 255 , 0 , 0 );
        glBegin ( GL_LINES );
        glVertex3f ( 0,0,0 );
        glVertex3f ( 0,2,0 );
        glEnd();

        glColor3ub ( 0 , 0 , 255 );
        glBegin ( GL_LINES );
        glVertex3f ( 0,0,0 );
        glVertex3f ( 2,0,0 );
        glEnd();
    }


    // Affichage des vecteurs normaux
    if ( drawnormales )
    {
        // Couleur grise
        glColor3ub ( 128 , 128 , 128 );

        // Utilisation des normales des faces
        if ( facetnormal )
        {
            // Pour chaque triangle
            for ( i = 0 ; i < nbr_triangles ; ++i )
            {
                face f = obtenir_face ( m_current , i );

                point tmp_point = barycentre_face ( f );
                vecteur tmp_vect = convertir_en_vecteur ( tmp_point );
                liberer_mem_point ( tmp_point );

                glBegin ( GL_LINES );
                glVertex3fv ( (GLfloat *) tmp_vect );

                vecteur tmp = vecteur_copie ( extraire_normale_f ( f ) );
                vecteur_mult ( tmp , 0.1f );
                vecteur_add ( tmp , tmp_vect );

                glVertex3fv ( (GLfloat *) tmp_vect );
                glEnd();

                supprimer_vecteur ( tmp );
                supprimer_vecteur ( tmp_vect );
            }
        }
        // Utilisation des normales des sommets
        else
        {
            const int nbr_sommets = nombre_de_sommets ( m_current );

            // Pour chaque sommet
            for ( i = 0 ; i < nbr_sommets ; ++i )
            {
                sommet s = obtenir_sommet ( m_current , i );

                point tmp_point = extraire_point ( s );
                vecteur tmp_vect = convertir_en_vecteur ( tmp_point );

                glBegin ( GL_LINES );
                glVertex3fv ( (GLfloat *) tmp_vect );

                vecteur tmp = vecteur_copie ( extraire_normale ( s ) );
                vecteur_mult ( tmp , 0.1f );
                vecteur_add ( tmp , tmp_vect );

                glVertex3fv ( (GLfloat *) tmp_vect );
                glEnd();

                supprimer_vecteur ( tmp );
                supprimer_vecteur ( tmp_vect );
            }
        }
    }

    glEndList();

    return;
}


void _gltbPointToVector ( int x , int y , int width , int height , float v[3] )
{
    float d, a;

    // Project x, y onto a hemi-sphere centered within width, height.
    v[0] = ( 2.0f * x - width) / width;
    v[1] = (height - 2.0 * y) / height;
    d = sqrt(v[0] * v[0] + v[1] * v[1]);
    v[2] = cos( ( 3.14159265f / 2.0f ) * ((d < 1.0) ? d : 1.0));
    a = 1.0 / sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
    v[0] *= a;
    v[1] *= a;
    v[2] *= a;

    return;
}


void _gltbAnimate ( void )
{
    glutPostRedisplay();
    return;
}


void _gltbStartMotion ( int x , int y , int time )
{
    tracking = true;
    gltb_lasttime = time;
    _gltbPointToVector ( x, y, gltb_width, gltb_height, gltb_lastposition);
    return;
}


void _gltbStopMotion ( unsigned time )
{
    tracking = false;

    if ( time - gltb_lasttime < 20 )
    {
        glutIdleFunc ( _gltbAnimate );
    }
    else
    {
        gltb_angle = 0;
        glutIdleFunc ( 0 );
    }

    return;
}


void gltbInit ( GLuint button )
{
    gltb_button = button;
    gltb_angle = 0.0f;

    // Put the identity in the trackball transform
    glPushMatrix();
    glLoadIdentity();
    glGetFloatv ( GL_MODELVIEW_MATRIX , (GLfloat *) gltb_transform );
    glPopMatrix();

    return;
}


void gltbMatrix(void)
{
    glPushMatrix();
    glLoadIdentity();
    glRotatef ( gltb_angle, gltb_axis[0], gltb_axis[1], gltb_axis[2]);
    glMultMatrixf ((GLfloat*)gltb_transform);
    glGetFloatv ( GL_MODELVIEW_MATRIX, (GLfloat*)gltb_transform);
    glPopMatrix();

    glMultMatrixf((GLfloat*)gltb_transform);
    return;
}


/**
 * Fonction appellée lors du redimensionnement de la fenêtre.
 *
 * @param width Nouvelle largeur de la fenêtre.
 * @param height Nouvelle hauteur de la fenêtre.
 ******************************/

void glut_reshape ( int width , int height )
{
    gltb_width = width;
    gltb_height = height;

    glViewport ( 0 , 0 , width , height );

    double ratio = (double)width / (double)height;

    glMatrixMode ( GL_PROJECTION );
    glLoadIdentity();
    gluPerspective ( 60.0f , ratio , 1.0f , 128.0f );
    glMatrixMode ( GL_MODELVIEW );
    glLoadIdentity();
    glTranslatef ( 0.0f , 0.0f , -3.0f );

    return;
}


void glut_mouse ( int button , int state , int x , int y )
{
    double model[4*4];
    double proj[4*4];
    int view[4];

    // fix for two-button mice -- left mouse + shift = middle mouse
    if ( button == GLUT_LEFT_BUTTON && glutGetModifiers() & GLUT_ACTIVE_SHIFT )
        button = GLUT_MIDDLE_BUTTON;

    mouse_state = state;
    mouse_button = button;

    if ( button == gltb_button )
    {
        if ( state == GLUT_DOWN )
            _gltbStartMotion ( x , y , glutGet ( GLUT_ELAPSED_TIME ) );
        else if ( state == GLUT_UP )
            _gltbStopMotion ( glutGet ( GLUT_ELAPSED_TIME ) );
    }

    if ( state == GLUT_DOWN && button == GLUT_MIDDLE_BUTTON )
    {
        glGetDoublev ( GL_MODELVIEW_MATRIX , model );
        glGetDoublev ( GL_PROJECTION_MATRIX , proj );
        glGetIntegerv ( GL_VIEWPORT , view );
        gluProject ( (GLdouble)x , (GLdouble)y , 0.0f ,
                      model , proj , view ,
                      &pan_x , &pan_y , &pan_z );
        gluUnProject ( (GLdouble)x , (GLdouble)y , pan_z ,
                       model , proj , view ,
                       &pan_x , &pan_y , &pan_z );
        pan_y = -pan_y;
    }

    glutPostRedisplay();
    return;
}


/**
 * Fonction appellée lors du déplacement du curseur de la souris.
 *
 * @param x Coordonnée horizontale du curseur de la souris.
 * @param y Coordonnée verticale du curseur de la souris.
 ******************************/

void glut_motion ( int x , int y )
{
    GLdouble model[4*4];
    GLdouble proj[4*4];
    GLint view[4];

    float current_position[3], dx, dy, dz;

    if ( tracking == false ) return;

    _gltbPointToVector(x, y, gltb_width, gltb_height, current_position);

    // Calculate the angle to rotate by
    dx = current_position[0] - gltb_lastposition[0];
    dy = current_position[1] - gltb_lastposition[1];
    dz = current_position[2] - gltb_lastposition[2];
    gltb_angle = 90.0f * sqrt ( dx * dx + dy * dy + dz * dz );

    // Calculate the axis of rotation (cross product)
    gltb_axis[0] = gltb_lastposition[1] * current_position[2] -
                   gltb_lastposition[2] * current_position[1];
    gltb_axis[1] = gltb_lastposition[2] * current_position[0] -
                   gltb_lastposition[0] * current_position[2];
    gltb_axis[2] = gltb_lastposition[0] * current_position[1] -
                   gltb_lastposition[1] * current_position[0];

    // XXX - constrain motion
    gltb_axis[2] = 0;

    // Reset for next time
    gltb_lasttime = glutGet ( GLUT_ELAPSED_TIME );
    gltb_lastposition[0] = current_position[0];
    gltb_lastposition[1] = current_position[1];
    gltb_lastposition[2] = current_position[2];

    // Remember to draw new position
    glutPostRedisplay();

    if ( mouse_state == GLUT_DOWN && mouse_button == GLUT_MIDDLE_BUTTON )
    {
        glGetDoublev(GL_MODELVIEW_MATRIX, model);
        glGetDoublev(GL_PROJECTION_MATRIX, proj);
        glGetIntegerv(GL_VIEWPORT, view);
        gluProject((GLdouble)x, (GLdouble)y, 0.0f,
                    model, proj, view,
                    &pan_x, &pan_y, &pan_z);
        gluUnProject((GLdouble)x, (GLdouble)y, pan_z,
            model, proj, view,
            &pan_x, &pan_y, &pan_z);
        pan_y = -pan_y;
    }

    glutPostRedisplay();
}


/**
 * Appelle la commande correspondant à l'entier donné.
 *
 * @param item Identifiant de l'item du menu.
 */

void menu ( int item )
{
    if ( item > 0 )
    {
        glut_keyboard ( (unsigned char) item , 0 , 0 );
    }
    else
    {
        dossier liste_obj = liste_dossier ( "obj/" );
        char * name = NULL;

        if ( liste_obj )
        {
            dossier sdir = liste_obj->dirs;
            int i = 0;

            // Pour chaque sous-dossier
            while ( sdir )
            {
                fichier file = sdir->files;

                while ( file )
                {
                    if ( ++i == -item )
                    {
                        // Nom du fichier : "obj/???/???"
                        name = allouer_mem ( strlen ( "obj/" ) +
                                             strlen ( sdir->name ) +
                                             strlen ( file->name ) + 2 );
                        strcpy ( name , "obj/" );
                        strcat ( name , sdir->name );
                        strcat ( name , "/" );
                        strcat ( name , file->name );
                        break;
                    }

                    file = file->next;
                }

                sdir = sdir->next;
            }
        }

        supprimer_liste_dossier ( liste_obj );

        free_maillage ( m_current );
        m_current = charger_maillage ( name );
        scale = maillages_unitize ( m_current );

        draw_maillages();
        liberer_mem ( name );

        glutPostRedisplay();
    }
}


/**
 * Fonction appellée lors de l'appui d'une touche du clavier.
 *
 * @param key Code de la touche.
 * @param x Coordonnée horizontale du curseur de la souris.
 * @param y Coordonnée verticale du curseur de la souris.
 */

void glut_keyboard ( unsigned char key , int x , int y )
{
    (void) x;
    (void) y;

    switch ( key )
    {
        // Appliquer toutes les corrections
        case '0':
            //corr_maillage ( m_current );
            draw_maillages();
            break;

        // Sommets proches
        case '1':
            //supprime_sommetsproches ( m_current , 1 );
            draw_maillages();
            break;

        // Sommets dupliqués
        case '2':
            corr_somm_dups ( m_current );
            draw_maillages();
            break;

        // Sommets non referencés
        case '3':
            corr_somm_nonref ( m_current );
            draw_maillages();
            break;

        // Sommets non manifolds
        case '4':
        {
            /*int n = */recherche_somm_nonmanifold ( m_current );
            draw_maillages();
            break;
        }

        // Faces mal orientées
        case '5':
            corr_faces_malorientees ( m_current );
            draw_maillages();
            break;

        // Faces dupliquées
        case '6':
            corr_faces_dups ( m_current );
            draw_maillages();
            break;

        // Faces invalides
        case '7':
            corr_faces_invalides ( m_current );
            draw_maillages();
            break;

        // Faces dégénérées
        case '8':
            corr_faces_degenerees ( m_current );
            draw_maillages();
            break;

        // Faces s'intersectant mutuellement
        case '9':
            affiche_intersection ( m_current );
            draw_maillages();
            break;

        // Affichage des normales
        case 'a':
            drawnormales = ( drawnormales == true ? false : true );
            draw_maillages();
            break;

        // Subdivision normale (bis)
        case 'b':
        {
            maillage tmp = subdivision_normale_bis ( m_current );
            free_maillage ( m_current );
            m_current = tmp;
	    scale = maillages_unitize ( m_current );
            draw_maillages();
            break;
        }

        // Activer/Désactiver le culling
        case 'c':
            if ( glIsEnabled ( GL_CULL_FACE ) ) glDisable ( GL_CULL_FACE );
            else glEnable ( GL_CULL_FACE );
            break;

        // Subdivision normale
        case 'd':
        {
            maillage tmp = subdivision_normale ( m_current );
            free_maillage ( m_current );
            m_current = tmp;
	    scale = maillages_unitize ( m_current );
            draw_maillages();
            break;
        }

        // Coloration avec le facteur de qualité
        case 'e':
            coloration_facteur_qualite ( m_current );
            draw_maillages();
            break;

        // Filtrage normal
        case 'f':
        {
            maillage tmp = filtre_normal ( m_current );
            free_maillage ( m_current );
            m_current = tmp;
            scale = maillages_unitize ( m_current );
            draw_maillages();
            break;
        }

        // Coloration avec la courbure locale
        case 'g':
            coloration_courbure ( m_current );
            draw_maillages();
            break;

        // Liste des commandes
        case 'h':
            printf ( "Liste des commandes\n\n" );
            printf ( "\ta : Afficher les normales\n" );
            printf ( "\tb : Subdivision normale (bis)\n" );
            printf ( "\tc : Activer/Désactiver le culling\n" );
            printf ( "\td : Subdivision normale\n" );
            printf ( "\te : Coloration avec le facteur de qualite\n" );
            printf ( "\tf : Filtrage normal\n" );
            printf ( "\tg : Coloration avec la courbure locale" );
            printf ( "\th : Liste des commandes\n" );
	    printf ( "\ti : Coloration par rapport a l'origine\n" );
	    printf ( "\tj : Coloration par rapport a l'axe Z\n" );
	    printf ( "\tk : Coloration par rapport au plan OXY\n" );
            printf ( "\tl : Coloration normale\n" );
	    printf ( "\tm : Faces manquantes\n" );
            printf ( "\tn : Activer/Désactiver les normales aux faces\n" );
            printf ( "\tp : Subdivision plane\n" );
            printf ( "\tq : Quitter l'application\n" );
            printf ( "\tr : Afficher/Masquer le repere\n" );
            printf ( "\ts : Diminuer la taille du maillage\n" );
            printf ( "\tS : Augmenter la taille du maillage\n" );
            printf ( "\tu : Filtrage uniforme\n" );
            printf ( "\tw : Mode fil-de-fer / Mode normal\n" );
            printf ( "\tW : Enregistrer le maillage (out.obj)\n" );
            printf ( "\t0 : Appliquer toutes les corrections\n" );
            printf ( "\t1 : Sommets proches\n" );
            printf ( "\t2 : Sommets dupliques\n" );
            printf ( "\t3 : Sommets non references\n" );
            printf ( "\t4 : Sommets non manifolds\n" );
            printf ( "\t5 : Faces mal orientees\n" );
            printf ( "\t6 : Faces dupliquees\n" );
            printf ( "\t7 : Faces invalides\n" );
            printf ( "\t8 : Faces degenerees\n" );
            printf ( "\t9 : Faces s'intersectant mutuellement\n" );
            printf ( "\n" );
            break;

        // Coloration par rapport à l'origine
        case 'i':
	{
	    point pt = new_point ( 0.0f , 0.0f , 0.0f );
	    coloration_distance_point ( m_current , pt );
	    liberer_mem_point ( pt );
            draw_maillages();
            break;
	}

        // Coloration par rapport à l'axe Z
        case 'j':
	{
	    point pt = new_point ( 0.0f , 0.0f , 0.0f );
	    vecteur v = vecteur_nouveau ( 0.0f , 0.0f , 1.0f );
	    coloration_distance_axe ( m_current , pt , v );
	    liberer_mem_point ( pt );
	    supprimer_vecteur ( v );
            draw_maillages();
            break;
	}

        // Coloration par rapport au plan OXY
        case 'k':
	{
	    point pt = new_point ( 0.0f , 0.0f , 0.0f );
	    vecteur v1 = vecteur_nouveau ( 0.0f , 1.0f , 0.0f );
	    vecteur v2 = vecteur_nouveau ( 1.0f , 0.0f , 0.0f );
	    coloration_distance_plan ( m_current , pt , v1 , v2 );
	    liberer_mem_point ( pt );
	    supprimer_vecteur ( v1 );
	    supprimer_vecteur ( v2 );
            draw_maillages();
            break;
	}

        // Coloration normale
        case 'l':
            coloration_normale ( m_current );
            draw_maillages();
            break;

        case 'm':
	    corr_faces_manquantes ( m_current );
	    draw_maillages();
            break;

        // Activer/Désactiver les normales aux faces
        case 'n':
            facetnormal = ( facetnormal == true ? false : true );
            draw_maillages();
            break;

        // Subdivision plane
        case 'p':
        {
            maillage tmp = subdivision_plane ( m_current );
            free_maillage ( m_current );
            m_current = tmp;
	    scale = maillages_unitize ( m_current );
            draw_maillages();
            break;
        }

        // Afficher/Masquer le repère
        case 'r':
            drawrepere = ( drawrepere == true ? false : true );
            draw_maillages();
            break;

        // Diminuer la taille du maillage
        case 's':
        case '-':
            scale_maillage ( m_current , 0.8f );
            scale *= 0.8f;
            draw_maillages();
            break;

        // Augmenter la taille du maillage
        case 'S':
        case '+':
            scale_maillage ( m_current , 1.25f );
            scale *= 1.25f;
            draw_maillages();
            break;

        // Filtrage uniforme
        case 'u':
        {
            maillage tmp = filtre_uniforme ( m_current );
            free_maillage ( m_current );
            m_current = tmp;
            scale = maillages_unitize ( m_current );
            draw_maillages();
            break;
        }

        // Mode fil-de-fer / Mode normal
        case 'w':
        {
            GLint params[2];
            glGetIntegerv ( GL_POLYGON_MODE , params );

            if ( params[0] == GL_FILL )
            {
                glPolygonMode ( GL_FRONT_AND_BACK , GL_LINE );
            }
            else
            {
                glPolygonMode ( GL_FRONT_AND_BACK , GL_FILL );
            }

            break;
        }

        // Enregistrer le maillage (out.obj)
        case 'W':
            scale_maillage ( m_current , 1.0f / scale );
            enregistrer_maillage ( "out.obj" , m_current );
            break;

        // Quitter
        case 'q':
        case 27:
            glutDestroyWindow ( glutGetWindow() ); // Destruction de la fenetre
            exit ( 0 ); // Fermeture du programme
            break;
    }

    glutPostRedisplay();
    return;
}


/**
 * Fonction appellée pour l'affichage de la scène.
 ******************************/

void glut_display ( void )
{
    glClearColor ( 1.0f , 1.0f , 1.0f , 1.0f );
    glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    glPushMatrix();

    glTranslatef ( pan_x , pan_y , 0.0f );
    gltbMatrix();
    glCallList(model_list);

    glPopMatrix();

    glutSwapBuffers();

    return;
}


void affiche_init ( int argc , char **argv )
{
    if ( argc <= 1 )
    {
        model_file = "obj/subdivision/cube_parfait.obj";
    }
    else
    {
        model_file = argv[1];
    }

    // Création de la fenêtre
    glutInitWindowSize ( 512 , 512 );
    glutInit ( &argc , argv );

    glutInitDisplayMode ( GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE );
    glutCreateWindow ( "Maillages 3D" );

    glutReshapeFunc ( glut_reshape );
    glutDisplayFunc ( glut_display );
    glutKeyboardFunc ( glut_keyboard );
    glutMouseFunc ( glut_mouse );
    glutMotionFunc ( glut_motion );


    int menu_modeles = 0;

    dossier liste_obj = liste_dossier ( "obj/" );

    if ( liste_obj )
    {
        dossier sdir = liste_obj->dirs;
        int submenus[20] = {0};
        int i = 0;

        // Pour chaque sous-dossier
        while ( sdir )
        {
            submenus[++i] = glutCreateMenu ( menu );

            fichier file = sdir->files;

            if ( file )
            {
                while ( file )
                {
                    ++entries;
                    glutAddMenuEntry ( file->name , -entries );
                    file = file->next;
                }
            }
            else
            {
                glutAddMenuEntry ( "Aucun fichier" , 0 );
            }

            sdir = sdir->next;
        }

        sdir = liste_obj->dirs;
        i = 0;
        
        menu_modeles = glutCreateMenu ( menu );
        
        // Pour chaque sous-menu
        while ( sdir )
        {
            glutAddSubMenu ( sdir->name , submenus[++i] );
            sdir = sdir->next;
        }
    }

    supprimer_liste_dossier ( liste_obj );

    int menu_correction = glutCreateMenu ( menu );

    glutAddMenuEntry ( "[1] Sommets proches" , '1' );
    glutAddMenuEntry ( "[2] Sommets dupliques" , '2' );
    glutAddMenuEntry ( "[3] Sommets non references" , '3' );
    glutAddMenuEntry ( "[4] Sommets non manifolds" , '4' );
    glutAddMenuEntry ( "[5] Faces mal orientees" , '5' );
    glutAddMenuEntry ( "[6] Faces dupliquees" , '6' );
    glutAddMenuEntry ( "[7] Faces invalides" , '7' );
    glutAddMenuEntry ( "[8] Faces degenerees" , '8' );
    glutAddMenuEntry ( "[9] Faces s'intersectant mutuellement" , '9' );
    glutAddMenuEntry ( "[m] Faces manquantes" , 'm' );
    glutAddMenuEntry ( "[0] Appliquer toutes les corrections" , '0' );

    int menu_filtrage = glutCreateMenu ( menu );

    glutAddMenuEntry ( "[u] Filtrage uniforme" , 'u' );
    glutAddMenuEntry ( "[f] Filtrage normal" , 'f' );

    int menu_subdivision = glutCreateMenu ( menu );

    glutAddMenuEntry ( "[p] Subdivision plane" , 'p' );
    glutAddMenuEntry ( "[d] Subdivision normale" , 'd' );
    glutAddMenuEntry ( "[b] Subdivision normale (bis)" , 'b' );

    int menu_coloration = glutCreateMenu ( menu );

    glutAddMenuEntry ( "[l] Coloration normale" , 'l' );
    glutAddMenuEntry ( "[e] Coloration avec le facteur de qualite" , 'e' );
    glutAddMenuEntry ( "[g] Coloration avec la courbure locale" , 'g' );
    glutAddMenuEntry ( "[i] Coloration par rapport a l'origine" , 'i' );
    glutAddMenuEntry ( "[j] Coloration par rapport a l'axe Z" , 'j' );
    glutAddMenuEntry ( "[k] Coloration par rapport au plan OXY" , 'k' );

    // Création du menu
    glutCreateMenu ( menu );

    glutAddSubMenu ( "Liste des modeles" , menu_modeles );
    glutAddSubMenu ( "Correction des erreurs" , menu_correction );
    glutAddSubMenu ( "Filtrage" , menu_filtrage );
    glutAddSubMenu ( "Subdivision" , menu_subdivision );
    glutAddSubMenu ( "Coloration" , menu_coloration );
    glutAddMenuEntry ( "" , 0 );

    glutAddMenuEntry ( "[w] Mode fil-de-fer / Mode normal" , 'w' );
    glutAddMenuEntry ( "[c] Activer/Desactiver le culling" , 'c' );
    glutAddMenuEntry ( "[n] Activer/Desactiver les normales aux faces" , 'n' );
    glutAddMenuEntry ( "[s] Diminuer la taille du maillage" , 's' );
    glutAddMenuEntry ( "[S] Augmenter la taille du maillage" , 'S' );
    glutAddMenuEntry ( "[a] Afficher/Masquer les normales" , 'a' );
    glutAddMenuEntry ( "[r] Afficher/Masquer le repere" , 'r' );
    glutAddMenuEntry ( "[W] Enregistrer le maillage (out.obj)" , 'W' );
    glutAddMenuEntry ( "[h] Liste des commandes" , 'h' );
    glutAddMenuEntry ( "[q] Quitter" , 'q' );

    glutAttachMenu ( GLUT_RIGHT_BUTTON );


    gltbInit ( GLUT_LEFT_BUTTON );

    // Lecture du fichier
    m_current = charger_maillage ( model_file );
    scale = maillages_unitize ( m_current );

    // Création de la display list
    draw_maillages();

    glEnable ( GL_LIGHTING );
    glEnable ( GL_LIGHT0 );
    glLightModeli ( GL_LIGHT_MODEL_TWO_SIDE , GL_TRUE );

    float ambient[] = { -0.2f , -0.2f , -0.2f , 1.0f };
    glLightfv ( GL_LIGHT0 , GL_AMBIENT , ambient );

    glEnable ( GL_DEPTH_TEST );
    glEnable ( GL_CULL_FACE );
    glEnable ( GL_COLOR_MATERIAL );

    return;
}

void affiche_maillage ( maillage m )
{
    free_maillage ( m_current );
    m_current = m;

    scale = maillages_unitize ( m_current );
    draw_maillages();

    return;
}

void affiche_boucle ( void )
{
    glutMainLoop();
    return;
}
