/**
* @file affichagencurses.h
* @brief Aspect graphique du programme en mode console
* @author Vatin Charles
*/

#ifndef AFFICHAGE_NCURSES_H
#define AFFICHAGE_NCURSES_H

#include <ncurses.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <pthread.h>

#include "affichagencurses.h"
#include "fichiers_obj.h"
#include "maillages.h"
#include "detection_correction.h"
#include "qualite.h"
#include "affichage.h"
#include "filtrage.h"
#include "subdivision.h"

WINDOW* msgbarre;


/**
 * @defgroup affichagencurses
 * @brief Détection et correction des erreurs sur les maillages.
 * @author Vatin Charles
 * @date     18 mai 2010
 */

/*@{*/

void printf_ncurses( const char * texte , ... );

/**
 * Initialise les paramètres utiles à ncurses.
 *
 * @pre  Aucune.
 * @post Création du contexte ncurses.
 */

void init_prog(void);

/**
 * Dessine la barre des menus (F2, F3, etc...)
 *
 * @param WINDOW* menubarre Adresse de l'espace qui a été allouée pour la barre des menus
 *
 * @pre  Les paramètres ncurses sont correctement initialisé. L'espace pour la barre des menus a bien été allouée.
 * @post Le buffer graphique contient la barre des menus à afficher.
 */

void draw_menubarre(WINDOW *menubarre);

/**
 * Alloue de la place et dessine le menu 'fichier'.
 *
 * @param WINDOW* fichiermenu Permet de récupérer l'endroit où l'espace va être allouée.
 * @param int selection Elément du menu 'fichier' où l'utilisateur est positionné.
 *
 * @pre  Les paramètres ncurses sont correctement initialisés.
 * @post Le menu 'fichier' est dessiné.
 */

void draw_fichiermenu(WINDOW* fichiermenu, int selection);

/**
 * Alloue de la place et dessine le menu 'corrections'.
 *
 * @param WINDOW* fichiermenu Permet de récupérer l'endroit où l'espace va être allouée.
 * @param int selection Elément du menu 'corrections' où l'utilisateur est positionné.
 *
 * @pre  Les paramètres ncurses sont correctement initialisés.
 * @post Le menu 'corrections' est dessiné.
 */

void draw_correctionsmenu(WINDOW* correctionsmenu, int selection);

/**
 * Alloue de la place et dessine le menu 'filtrages'.
 *
 * @param WINDOW* fichiermenu Permet de récupérer l'endroit où l'espace va être allouée.
 * @param int selection Elément du menu 'filtrages' où l'utilisateur est positionné.
 *
 * @pre  Les paramètres ncurses sont correctement initialisés.
 * @post Le menu 'filtrages' est dessiné.
 */

void draw_filtragesmenu(WINDOW* filtragesmenu, int selection);

/**
 * Alloue de la place et dessine le menu 'subdivisions'.
 *
 * @param WINDOW* fichiermenu Permet de récupérer l'endroit où l'espace va être allouée.
 * @param int selection Elément du menu 'subdivisions' où l'utilisateur est positionné.
 *
 * @pre  Les paramètres ncurses sont correctement initialisés.
 * @post Le menu 'subdivisions' est dessiné.
 */

void draw_subdivisionsmenu(WINDOW* subdivisionsmenu, int selection);

/**
 * Alloue de la place et dessine le menu 'qualite'.
 *
 * @param WINDOW* fichiermenu Permet de récupérer l'endroit où l'espace va être allouée.
 * @param int selection Elément du menu 'qualite' où l'utilisateur est positionné.
 *
 * @pre  Les paramètres ncurses sont correctement initialisés.
 * @post Le menu 'qualite' est dessiné.
 */

void draw_qualitemenu(WINDOW* qualitemenu, int selection);

/**
 * Ecoute les actions de l'utilisateur, et retourne un entier correspondant à ce qu'il a sélectionné.
 *
 * @param int type Entier qui permet de savoir par quel menu l'utilisateur est rentré.
 *
 * @return int Entier qui correspond a un élément parmis tous les éléments des menus disponibles. 
 *
 * @pre  L'entier 'type' correspond bien à un menu d'entrée.
 * @post L'entier correspond bien à un élément parmis les différent menus.
 */

int action_menu(int type);

/**
 * Fonction maîtresse de l'interface utilisateur, coordonne les actions de l'utilisateur avec ce qui est affiché sur l'écran.
 *
 * @param argc : Nombre d'arguments envoyés au programme.
 * @param argv : Tableau des arguments.
 *
 * @return int Entier qui indique s'il y a eu des erreurs ou non.
 * 
 * @pre  Des arguments valides.
 * @post L'utilisateur est satisfait du programme.
 */

int main_ncurses(int argc, char* argv[]);


/*@}*/

#endif
