/**
 * @file sommet.h
 * @brief D�finition du module de gestion des sommets.
 * @author Iraqi Sara
 * @author Cr�m�se Jean
 */

#ifndef SOMMET_H
#define SOMMET_H

#include "point.h"
#include "vecteur.h"
#include "gestion_memoire.h"

/**
 * @defgroup sommet Sommet
 * Module contenant les d�finition des donn�es et des traitements li�s � la
 * manipulation des sommets.
 * @author Iraqi Sara
 * @author Cr�m�se Jean
 */

/*@{*/

/* ------------------------------------------------------------------ */
/* D�finition des donn�es                                             */
/* ------------------------------------------------------------------ */


/**
 * @struct t_sommet
 * Structure qui contiendra la structure de sommet
 * (un point et un vecteur qui representera la normale).
 */

typedef struct t_sommet* sommet;


/**
 * @struct t_texture
 * D�finition du type texture
 * Compos� de :
 * - d'une premi�re coordonn�e
 * - d'une seconde coordonn�e
 */
typedef struct t_texture* texture;
struct t_texture
{
    float x;
    float y;
};

/* ------------------------------------------------------------------ */
/* D�finition des traitements sur ces donn�es                         */
/* ------------------------------------------------------------------ */

// Fonction d'affichage des sommets qu'on utilisera dans les tests des
// autres modules
void afficher_sommet(sommet s);

// Fonction d'affichage des sommets qu'on utilisera dans les tests des
// autres modules
void afficher_texture(texture t);


/**
 * Extrait le point du sommet.
 *
 * @param s le sommet dont on veut extraire le point
 * @pre s est bien form�
 * @return le point bien form� correspondant
 * @test extraire � partir d'un sommet bien form�
 */

point extraire_point(sommet s);


/**
 * Teste si un sommet est bien form�, c'est-�-dire que le point et le vecteur
 * (la normale) associ� sont bien form�s.
 *
 * @param s le sommet � tester
 * @return 1 si le sommet est bien form�, 0 sinon
 * @test si le resultat est 1 a partir d'un sommet bien forme
 * @test si le resultat est 0 a partir d'un sommet mal forme
 */

int bienforme_sommet (sommet s);


/**
 * Renvoi la texture par d�faut.
 * @return la texture par d�faut, NULL.
 */

texture texture_default ( void );


/**
 * Cr�� une texture.
 * 
 * @param t1 le float qui est la premi�re coordonn�e de la future texture
 * @param t2 le float qui est la deuxi�me coordonn�e de la future texture
 * @return la texture correctement form�e
 * @test cr�er une texture � partir de trois coordonn�es
 */

texture new_texture(float t1, float t2);


/**
 * Extrait la normale du sommet.
 *
 * @param s le sommet dont on veut extraire le point
 * @return la normale bien form�e correspondante
 * @pre s est bien form�
 * @test extraire � partir d'un sommet bien form�
 */

vecteur extraire_normale(sommet s);


/**
 * Modifie la texture du sommet.
 * 
 * @param s le sommet dont on veut modifier la texture
 * @param t la nouvelle texture
 * @pre s est bien form�
 * @pre t est bien form�e
 * @test remplacer la texture d'une face par une texture quelconque
 */

sommet modifier_texture_sommet(sommet s, texture t);


/**
 * Extrait la texture du sommet.
 *
 * @param s le sommet dont on veut extraire la texture
 * @return la texture bien form�e correspondante
 * @test extraire � partir d'un sommet bien form�
 */

texture extraire_texture(sommet s);


/**
 * Extrait la n-i�me composante de la texture.
 *
 * @param t la texture dont on veut extraire la coordonn�e
 * @param numero l'index de la coordonn�e voulue
 * @pre le num�ro est compris entre 0 et 2
 * @return la composante bien form�e
 * @test modifier la coordonn�e t2 d'une texture en la rempla�ant par elle-m�me.
 * @test modifier la coordonn�e t2 d'une texture
 */

int extraire_coordonnee_texture(texture t, int numero);


/**
 * Renvoie la m�diane d'un c�t� form� de deux sommets.
 *
 * @param s1 le premier sommet du c�t�
 * @param s2 le deuxi�me sommet du c�t�
 * @pre s1 est bien form�
 * @pre s2 est bien form�
 * @return le sommet bien form�
 * @test la m�diane entre deux sommets identiques
 * @test la m�diane entre deux sommets non identiques et bien form�
 */

sommet moitie_cote(sommet s1,sommet s2);


/**
 * Cr�ation d'un sommet � partir d'un point et de sa normale.
 * La normale peut �tre NULL si on ne souhaite pas l'imposer, sinon on
 * fournira un vecteur.
 *
 * @param p le point
 * @param normale le vecteur
 * @param t la texture
 * @pre p est bien form�
 * @pre normale est bien form�e
 * @return le sommet correctement form�
 * @test cr�er un sommet � partir d'un point bien form� et d'une normale bien form�e
 */

sommet new_sommet(point p, vecteur normale, texture t);


/**
 * Copie un sommet � partir d'un autre sommet.
 *
 * @param s Sommet � copier.
 * @return Nouveau sommet.
 */

sommet copie_sommet ( sommet s );


/**
 * Supprime le sommet en m�moire.
 *
 * @param s le sommet qu'on veut supprimer.
 * @post le pointeur pointe d�sormais vers NULL.
 */

void liberer_mem_sommet(sommet s);


/**
 * Modification du point d'un sommet.
 *
 * @param s le sommet � modifier.
 * @param p le nouveau point.
 * @pre p est bien form�
 * @return le sommet correctement form�
 * @test modification d'un sommet bien form� avec un point bien form�
 */

sommet modif_point_sommet (sommet s, point p);


/**
 * Modification de la normale d'un sommet.
 *
 * @param s le sommet � modifier
 * @param normale un vecteur qui correspond � la normale du futur sommet
 * @pre normale est bien form�e
 * @return le sommet correctement form�
 * @test modification d'un sommet bien form� avec une normale bien form�e
 */

sommet modif_norm_sommet (sommet s, vecteur normale);


/**
 * Modification de la texture d'un sommet.
 *
 * @param s le sommet dont on veut modifier la texture
 * @param t la nouvelle texture de la face
 * @pre le sommet est bien form�e
 * @pre la nouvelle texture est bien d�finie
 * @return le sommet bien form�
 * @test modification de la texture d'un sommet mal form�
 */

sommet modif_texture_sommet(sommet s, texture t);


/**
 * Comparaison des deux points des sommets et des deux normales.
 *
 * @param s1 le premier sommet des deux � comparer
 * @param s2 le deuxi�me sommet
 * @pre s1 est bien form�
 * @pre s2 est bien form�
 * @return 1 si les sommets sont identiques, 0 sinon
 * @test comparaison de deux sommets, dont l'un est mal form�
 * @test comparaison de deux sommets de deux sommets �gaux bien form�s
 * @test comparaison de deux sommets de deux sommets de normales diff�rentes bien form�s
 * @test comparaison de deux sommets de deux sommets de point diff�rentes bien form�s
 * @test comparaison de deux sommets de deux sommets de point et de normales diff�rentes bien form�s
 */

int comp_sommet (sommet s1, sommet s2);


/*@}*/

#endif /* fin de ifndef SOMMET_H */
