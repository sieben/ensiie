/**
 * @file sommet.c
 * @brief Implantation du module de gestion des sommets.
 * @author Iraqi Sara
 * @author Cr�m�se Jean
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sommet.h"


/**
 * @addtogroup sommet
 */

/*@{*/

/**
 * D�finition du type sommet
 * Compos� :
 *   - du point (trois coordonn�es x , y et z)
 *   - du vecteur (la normale)
 */

struct t_sommet
{
    point p;         ///< Point associ� au sommet.
    vecteur normale; ///< Vecteur normale au sommet.
    texture t;       ///< Texture du sommet
};


//Fonction d'affichage de la texture
void afficher_texture(texture t)
{
    printf("(%d, %d)\n",
        extraire_coordonnee_texture(t, 0),
        extraire_coordonnee_texture(t, 1));
    printf("\n");
}

// Fonction d'affichage des sommets
void afficher_sommet(sommet s)
{
    printf("point du sommet : ");
        afficher_point(extraire_point(s));
    printf("normale du sommet : ");
        afficher_vecteur(extraire_normale(s));
    printf("texture du sommet : ");
    afficher_texture(extraire_texture(s));
}

/*@}*/

int bienforme_sommet (sommet s)
{
    return ( (s != NULL) && bienforme_point(s->p) );
}


sommet moitie_cote( sommet s1 , sommet s2 )
{
    if ( !bienforme_sommet(s1) ||!bienforme_sommet(s2))
    {
        // Si au moins un des deux sommets est mal form�, on renvoie une erreur
        fprintf ( stderr , "moitie_cote : Au moins un des deux "
                "sommets est mal form�.\n" );
        exit ( EXIT_FAILURE );
    }
    else
    {
        point p1 = extraire_point ( s1 );
        point p2 = extraire_point ( s2 );

        //Pour chaque sommet, on extrait le point : on divise ensuite par deux
        //la diff�rence entre les coordonn�es de m�me index
        float n_x = (p2->x + p1->x) / 2.0f;
        float n_y = (p2->y + p1->y) / 2.0f;
        float n_z = (p2->z + p1->z) / 2.0f;

        vecteur n1 = extraire_normale ( s1 );
        vecteur n2 = extraire_normale ( s2 );

        // Calcul de la normale
        vecteur normale = vecteur_nouveau ( extraire_composante ( n1 , 0 ) +
                extraire_composante ( n2 , 0 ) ,
                extraire_composante ( n1 , 1 ) +
                extraire_composante ( n2 , 1 ) ,
                extraire_composante ( n1 , 2 ) +
                extraire_composante ( n2 , 2 ) );

        vecteur_normalise ( normale );

        // Calcul de la texture
        float tx=(extraire_coordonnee_texture(extraire_texture(s1),0) +
                  extraire_coordonnee_texture(extraire_texture(s2),0))/ 2.0f;
        float ty=(extraire_coordonnee_texture(extraire_texture(s1),1) +
                  extraire_coordonnee_texture(extraire_texture(s2),1))/ 2.0f;
        texture tnew=new_texture(tx,ty);
        // On retourne le sommet constitu� du point cr�� � partir des
        // coordonn�es cr��es et du vecteur normal
        return new_sommet(new_point(n_x,n_y,n_z),normale,tnew);
    }
}


void liberer_mem_sommet(sommet s)
{
    if (s->p != NULL)
        liberer_mem_point(s->p);
    if (s->normale != NULL)
        supprimer_vecteur(s->normale);
    if (s->t != NULL)
        liberer_mem(s->t);
    liberer_mem(s);
}


point extraire_point(sommet s)
{
    // On extrait le point si le sommet est bien form�
    if (bienforme_sommet(s))
        return (s->p);

    // Si le point est mal form�, on renvoie une erreur
    fprintf ( stderr , "extraire_point : Le sommet est mal form�.\n" );
    exit(EXIT_FAILURE);
}


vecteur extraire_normale(sommet s)
{
    if (bienforme_sommet(s))
        return s->normale;

    fprintf ( stderr , "extraire_normale : Le sommet est mal form�.\n" );
    exit(EXIT_FAILURE);
}


texture extraire_texture(sommet s)
{
    if (bienforme_sommet(s))
        return s->t;

    fprintf ( stderr , "extraire_texture : Le sommet est mal form�.\n" );
    exit(EXIT_FAILURE);
}

sommet modifier_texture_sommet(sommet s, texture t)
{
    s->t=t;
    return s;
}

texture new_texture(float t1, float t2)
{
    // On alloue de la m�moire � une structure dont on d�finit les composantes
    texture t = allouer_mem(sizeof(struct t_texture));
    t->x = t1;
    t->y = t2;
    return t;
}

texture texture_default ( void )
{
    return NULL;
}

sommet new_sommet(point p, vecteur normale, texture t)
{
    // On alloue de la m�moire � un sommet dont on d�finit le point et la
    // normale
    sommet s= allouer_mem(sizeof(struct t_sommet));
    s->p=p;
    s->normale= normale;
    s->t=t;
    return s;
}


sommet copie_sommet ( sommet s )
{
    if ( s == NULL ) return new_sommet ( NULL , NULL , NULL );

    point p = extraire_point ( s );
    point np = new_point ( extraire_coordonnee_point ( p , 0 ) ,
                           extraire_coordonnee_point ( p , 1 ) ,
                           extraire_coordonnee_point ( p , 2 ) );

    vecteur nn = vecteur_copie ( extraire_normale ( s ) );

    texture t = extraire_texture ( s );
    texture nt = allouer_mem ( sizeof(struct t_texture) );

    if ( t )
    {
        nt->x = t->x;
        nt->y = t->y;
    }

    sommet ns = new_sommet ( np , nn , nt );
    return ns;
}


sommet modif_point_sommet (sommet s, point p)
{
    s->p=p;
    return s;
}


sommet modif_norm_sommet (sommet s, vecteur normale)
{
    s->normale=normale;
    return s;
}

int extraire_coordonnee_texture(texture t, int numero)
{
    if ( t != NULL )
    {
        // On renvoie la coordonn�e correspondant � l'index pass� en argument
        if ( numero == 0 ) return t->x;
        if ( numero == 1 ) return t->y;

        fprintf ( stderr , "extraire_coordonnee_texture : L'index de la "
                           "coordonn�e n'est pas compris entre 0 et 1.\n" );
        exit ( EXIT_FAILURE );
    }

    fprintf ( stderr , "texture NULL.\n" );
    exit ( EXIT_FAILURE );
}

sommet modif_texture_sommet (sommet s, texture t)
{
    s->t=t;
    return s;
}

int comp_sommet (sommet s1, sommet s2)
{
    if (!bienforme_sommet(s1) || !bienforme_sommet(s2))
    {
        fprintf(stderr , "comp_sommet : les sommets sont mal form�s.\n");
        exit(EXIT_FAILURE);
    }
    else
        return ( vecteur_egaux(extraire_normale(s1),extraire_normale(s2))
                && comp_point(extraire_point(s1),extraire_point(s2)) );
}
