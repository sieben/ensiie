/**
 * @file detection_correction.c
 * @author Vatin Charles
 * @author Leone Remy
 * @author Lasnier Simon
 *
 * Implantation du module de détection et de correction des erreurs sur les
 * maillages.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "detection_correction.h"
#include "qualite.h"
#include "affichagencurses.h"

/**
 * @addtogroup detect_correct
 */


void suppression ( maillage m , tableau T , char type )
{
    int k = 0;
    const int taille = taille_tableau ( T );
    trier_tableau_entiers(T);

    int i;

    for ( k=taille-1; k >=0 ; k-- )
    {
	if ( type == 's' )
	{
	    supprimer_sommet ( m , acceder_element ( T , k , int ) );
	}
	else if ( type == 'f' )
	{
	    supprimer_face ( m , acceder_element ( T , k , int ) );
	}
    }

    supprimer_tableau ( T );
    return;
}


tableau recherche_doublons_sommet ( maillage m)
{
    int k;
    tableau indices_sommets_a_supprimer=new_tableau();
    const int nbs = nombre_de_sommets ( m );
    int i = 0;

    for ( k = 0 ; k < nbs-1 ; k++ )
    {
	if (recherche_tableau(indices_sommets_a_supprimer,k)==-1) // Si k n'est pas le doublon d'un autre sommet
	{

	    for ( i = k+1 ; i < nbs ; i++ ) // Parcours des sommets d'indice > k
	    {
		if ( comp_point(extraire_point(obtenir_sommet(m, i)), extraire_point(obtenir_sommet(m, k))) == 1 )
		    // Si i est un doublon de k
		{
		    reorg_faces(m,i,k);
		    ajouter_element (indices_sommets_a_supprimer , i );
		}
	    }
	}
    }

    return indices_sommets_a_supprimer;
}

void reorg_faces(maillage m, int ancien, int nouveau)
{

    sommet sold = obtenir_sommet(m,ancien);
    sommet snew = obtenir_sommet(m,nouveau);
    point  pold = extraire_point(sold);
    const int nbf = nombre_de_faces (m);
    int k = 0;
    int l = 0;
    int point_a_changer = -1;

    for(k = 0; k < nbf; k++)
    {
	face f = obtenir_face(m,k);
	point_a_changer = -1;

	for (l =0; l <= 2; l++)
	{
	    if (comp_point(pold,extraire_point(extraire_sommet(f,l))) == 1)
	    {
		point_a_changer = l;
	    }
	}

	switch( point_a_changer)
	{
	    case 0 :
		modifier_face(m,modif_face_sommets(f,snew,extraire_sommet(f,1),extraire_sommet(f,2)),k);
		break;
	    case 1 :
		modifier_face(m,modif_face_sommets(f,extraire_sommet(f,0),snew,extraire_sommet(f,2)),k);
		break;
	    case 2 :
		modifier_face(m,modif_face_sommets(f,extraire_sommet(f,0),extraire_sommet(f,1),snew),k);
		break;
	}
    }
}

void corr_somm_dups ( maillage m )
{
    //printf_ncurses("Nombre de sommets du maillage avant: %d\n",nombre_de_sommets(m));
    tableau indices_sommets_a_supprimer = recherche_doublons_sommet(m);
    int k = 0;

    trier_tableau_entiers(indices_sommets_a_supprimer);
    for ( k = taille_tableau(indices_sommets_a_supprimer)-1 ; k >= 0; k-- )
    {
	//printf_ncurses("a supprimer : %d\n",acceder_element(indices_sommets_a_supprimer,k,int));
	supprimer_sommet(m,acceder_element(indices_sommets_a_supprimer,k,int)); // supprime le sommet doublon du maillage
    }

    int res2 = taille_tableau(indices_sommets_a_supprimer);
    printf_ncurses("Nombre de sommets supprimés : %d\n", res2);
    //printf_ncurses("Nombre de sommets du maillage apres: %d\n",nombre_de_sommets(m));

    supprimer_tableau(indices_sommets_a_supprimer);
}

/*
void supprime_sommetsproches(maillage m,int n,int p)
{
  int k=0;
  int i=0;
  int j=0;
  int nbvoisins;
  int nbfacesadj;
  tableau voisins = new_tableau();
  tableau facesadj = new_tableau();
  float moyenneL=0;
  float moyenneG=0;
  tableau tabmoyenneL=new_tableau();
  float ecarttype=0;


  int borneinf=0;

  int indice_sommet_peripherique=0;
  int indice_sommet_central=0;

  for (k=0;k<n;k++) // Parcours de la liste des sommets du maillage
  {

  voisins=acceder_element(sommets_voisins(m,k,1),1,tableau); // voisins directs de sommets[k]
  nbvoisins=taille_tableau(voisins);
// Récupère les voisins du sommet k
for (i=0;i<nbvoisins;i++) // Parcours des voisins du sommet k
{
moyenneL+=distance_point(extraire_point(obtenir_sommet(m,k)),extraire_point());
}

moyenneL/=nbvoisins;
tabmoyenneL[k]=moyenneL;
moyenneG+=moyenneL;
moyenneL=0;

}

moyenneG/=n;
// Ecart type des moyennes locales par rapport à la moyenne globale
for (k=0;k<n;k++)
{
ecarttype+=(tabmoyenneL[k]-moyenneG)*(tabmoyenneL[k]-moyenneG);
}
ecarttype/=n;
ecarttype=sqrt(ecarttype);

borneinf=moyenneG-(p*ecarttype);

for (k=0;k<n;++k)
{
voisins=sommets_voisins(m,obtenir_sommet(m,k),&nbvoisins);
// Récupère les voisins du sommet k
for (i=0;i<nbvoisins;++i) // Parcours des voisins du sommet k
{
int d=distance_point(extraire_point(obtenir_sommet(m,k)),extraire_point(voisins[i]));
if(d<=borneinf) // Le sommet voisins[i] est proche du sommet k
{
// Récupère les faces incidentes à voisins[i]
facesadj=faces_incidentes(m,extraire_point(voisins[i]),&nbfacesadj);
for(j=0;j<nbfacesadj;j++)
{
indice_sommet_central=0;
indice_sommet_peripherique=0;

for (l=1;l<=3;l++)
{
// Récupération des indices des sommets central et périphérique dans la face
if(extraire_sommet(obtenir_face(m,j),l)==obtenir_sommet(m,k))
{
indice_sommet_central=l;
}
if(extraire_sommet(obtenir_face(m,j),l)==voisins[i])
{
indice_sommet_peripherique=l;
}
}

if (indice_sommet_central!=0) // Si la face contient le sommet central
{
    supprimer_face(facesadj[j]); // On la supprime
}
else
{
    face nf=new_face(obtenir_sommet(m,k),extraire_sommet(obtenir_face(m,j),
		(indice_sommet_peripherique+1)%3),extraire_sommet(obtenir_face(m,j),
		    (indice_sommet_peripherique+2)%3));
    modifier_face(m,nf,j);//On remplace le sommet peripherique par le sommet central

}
}

supprimer_sommet(m,voisins[i]);
nbvoisins--;
}
}
}
}
*/

tableau recherche_somm_nonref ( maillage m )
{
    const int nbs = nombre_de_sommets ( m );
    const int nbf = nombre_de_faces ( m );
    int k = 0;
    int j = 0;
    int l = 0;
    int ref = 0;
    int stop = 0;
    face f = NULL;
    sommet s = NULL;
    sommet s2 = NULL;
    tableau indices_somm_nonreftab = new_tableau();

    for ( k = 0 ; k < nbs ; k++ ) //parcours sommets
    {
	s = obtenir_sommet ( m , k );


	for ( j = 0 ; j < nbf ; j++ ) //parcours faces
	{
	    f = obtenir_face ( m , j );


	    for ( l = 0 ; l <= 2 ; l++ ) //parcours sommets de la face
	    {
		s2 = extraire_sommet ( f , l );

		if ( s == s2 )
		{
		    ref = 1; // Le sommet k est référencé (utilisé dans la face j)
		    stop = 1;
		    break;
		}
	    }

	    if ( stop == 1) break;
	}

	if ( ref == 0 ) // Le sommet est non référencé
	{
	    ajouter_element ( indices_somm_nonreftab , k );
	}

	ref = 0;
	stop = 0;
    }

    return indices_somm_nonreftab;
}

void corr_somm_nonref ( maillage m )
{
    tableau somm_nonreftab = recherche_somm_nonref ( m );
    int res = taille_tableau(somm_nonreftab);
    printf_ncurses("Nombre de sommets non référencés : %d\n", res);
    suppression ( m , somm_nonreftab , 's' );
    return;
}

tableau recherche_doublons_face ( maillage m )
{
    int k;
    int i;
    int nbfacesavecdoublons = 0;
    tableau indices_faces_doublestab = new_tableau();
    const int nbf = nombre_de_faces ( m );

    for ( k = 0 ; k < nbf-1 ; k++ )
    {
	if (recherche_tableau(indices_faces_doublestab,k)==-1)// Si la face k n'est pas un doublon d'une autre face
	{
	    for ( i = k+1 ; i < nbf ; i++ )
	    {
		if ( comp_face_points ( obtenir_face(m, i), obtenir_face ( m, k ) ) == 1 )
		{
		    ajouter_element ( indices_faces_doublestab , i );
		    nbfacesavecdoublons++;
		}
	    }
	}
    }

    printf_ncurses("Nombre de faces ayant des doublons : %d\n", nbfacesavecdoublons);
    return indices_faces_doublestab;
}


void corr_faces_dups ( maillage m )
{
    tableau faces_doublestab=recherche_doublons_face(m);
    suppression (m,faces_doublestab,'f');
    return;
}


tableau recherche_faces_invalides ( maillage m )
{
    int k=0;
    int l=0;
    int inv=0;
    tableau indices_faces_invalidestab=new_tableau();
    const int nbf=nombre_de_faces(m);
    const int nbs=nombre_de_sommets(m);
    tableau sommets_maillage=new_tableau();
    sommet s2;
    //Tableau des sommets (pointeurs vers) du maillage
    for ( k = 0 ;k <nbs ; ++k )
    {
	ajouter_element(sommets_maillage,obtenir_sommet(m,k));
    }

    for ( k = 0 ; k<nbf ; ++k ) // parcours des faces
    {

	face f = obtenir_face (m,k); // f: face d'indice k

	if (!bienforme_face(f)) ajouter_element ( indices_faces_invalidestab , k );

	else // Si la face est bien formée on regarde si ses sommets sont référencés
	{
	    for ( l=0;l<=2;++l)//parcours sommets de la face f
	    {
		s2 = extraire_sommet(f,l);

		// Si la face utilise un sommet d'index inexistant
		if ( recherche_tableau ( sommets_maillage , s2 ) == -1 )
		{
		    inv = 1;
		    break;
		}
	    }
	}

	if ( inv == 1 ) {
	    ajouter_element ( indices_faces_invalidestab , k );
	}
	inv = 0;
    }
    return indices_faces_invalidestab;
}

void corr_faces_invalides(maillage m)
{
    tableau faces_invalidestab=recherche_faces_invalides(m);
    int res = taille_tableau(faces_invalidestab);
    printf_ncurses("Nombre de faces invalides : %d\n", res);
    suppression(m,faces_invalidestab,'f');
}


/*
   int formeuneface(maillage m,sommet s,sommet s1, tableau voisinsdes)
   {
   int k=0;
   for (k=0;k<taille_tableau(voisinsdes);k++)
   {



   }

   }*/

/*
 * Fonction auxiliaire pour la détection des sommets non manifold.
 * La fonction teste juste si les deux faces fournies en paramètre ont
 * exactement deux sommets communs (trois sommets communs reviendrait à dire
 * qu'elles sont égales )
 */
int deux_sommets_communs (face f1, face f2)
{
    int nb_sommets_communs = 0;
    sommet s1 = NULL, s2 = NULL;
    int i = 0, j = 0;

    for (i = 0 ; i < 3 ; i++) // pour chaque sommet s1 de f1
    {
	s1 = extraire_sommet(f1, i);
	for (j = 0 ; j < 3 ; j++)   // pour chaque sommet s2 de f2
	{
	    s2 = extraire_sommet(f2, j);
	    if (s1 == s2)
		nb_sommets_communs++;
	}
    }

    return (nb_sommets_communs == 2);
}
//Compare deux tableaux d'entiers supposés triés.
int compare_tableaux(tableau t1,tableau t2)
{
    int k=0;
    const int ttab1 = taille_tableau(t1);
    if(ttab1 != taille_tableau(t2))
	return 0;

    for(k=0;k<ttab1;k++)
	if (acceder_element(t1,k,int) != acceder_element(t2,k,int))
	    return 0;

    return 1;
}


int recherche_somm_nonmanifold ( maillage m )
{
    int nb_somm_nonmanifold = 0;
    int nbs = nombre_de_sommets(m);
    tableau sommets_parcourus = new_tableau();
    tableau faces_parcourues = new_tableau();
    tableau voisins = NULL;
    tableau incidentes = NULL;
    int i = 0, j = 0, k = 0;
    sommet s = NULL;
    int nbvoisins = 0;
    int nbincidentes = 0;
    face f = NULL;
    sommet scentral = NULL;
    int estmanifold = 0, parcourspossible = 1;

    for (i = 0 ; i < nbs ; i++)
    {
	scentral = obtenir_sommet(m,i);
	tableau *vois = sommets_voisins(m,i,1);
	voisins = vois[1];  //voisins directs de scentral
	incidentes = faces_incidentes(m,i);
	nbvoisins = taille_tableau(voisins);
	s = obtenir_sommet(m,acceder_element(voisins,0,int));	//Le sommet a partir duquel on parcourt les voisins.
	ajouter_element(sommets_parcourus,indice_sommet(m,s));
	nbincidentes=taille_tableau(incidentes);

	while(parcourspossible!=0) //ne rentre jamais dans la boucle
	{

	    parcourspossible=0;
	    for (j = 0 ; j < nbincidentes ; j++) 
	    {
		f = acceder_element(incidentes,j,face);
		if ( (estdansface(m,s,indice_face(m,f))==1)&&((recherche_tableau(faces_parcourues,f))==(-1)))		//S appartient a une face incidente non déjà parcourue
		{
		    parcourspossible=1;
		    ajouter_element(faces_parcourues,f);
		    for( k=0 ; k<=2 ; k++)      //On cherche le sommet de la face different de scentral et s
		    {
			if ((extraire_sommet(f,k)!=s)&&(extraire_sommet(f,k)!=scentral))
			{
			    s=extraire_sommet(f,k);
			    ajouter_element(sommets_parcourus,indice_sommet(m,s));
			    break;
			}
		    }

		    break ;
		}
		// Le sommet n'appartient a aucune face incidente a scentral non parcourue.
	    }



	}
	parcourspossible=1;
	trier_tableau_entiers(sommets_parcourus);
	trier_tableau_entiers(voisins);
	if(compare_tableaux(sommets_parcourus,voisins))
	{
	    nb_somm_nonmanifold++;
	    for (j = 0 ; j < nbincidentes ; j++) 
	    {
		f = acceder_element(incidentes,j,face);
		couleur c = extraire_couleur ( f );

		c = couleur_default();
		modif_face_couleur ( f , c );

		c->r = 255;
		c->v = 0;
		c->b = 0;
	    }
	}

	//if(taille_tableau(faces_parcourues)!=0) printf("nb_voisin: %d, nb sommets percourus: %d, nb faces parcourues: %d\n", taille_tableau(voisins), taille_tableau(sommets_parcourus), taille_tableau(faces_parcourues));
	//if(taille_tableau(voisins)!=(taille_tableau(sommets_parcourus)-1)) nb_somm_nonmanifold++;
	//if(taille_tableau(incidentes)!=taille_tableau(voisins)) nb_somm_nonmanifold++;
	vider_tableau(sommets_parcourus);
	vider_tableau(faces_parcourues);

    }
    printf_ncurses("nombre de sommets non manifold : %d\n",nb_somm_nonmanifold);
    return nb_somm_nonmanifold;
}


/*
 * Fonction auxiliaire pour corr_faces_manquantes, la fonction ajoute (ou pas)
 * une face en faisant les tests nécéssaires.
 */
void _corrfm_ajouter_face (maillage m, sommet centre, sommet s1, sommet s2,
	int *nb_corrections, tableau incidentes)
{
    // Ici il faut vérifier que l'angle entre les deux arrêtes est < 145°
    // et si oui il faut ajouter la face dans le bon sens

    const int nb_incidentes = taille_tableau(incidentes);
    face f = NULL;
    sommet s = NULL;

    int i = 0, continuer = 1, j = 0;
    face face_ok = NULL;

    for (i = 0 ; i < nb_incidentes && continuer ; i++)
    {
	face_ok = NULL;
	f = acceder_element(incidentes, i, face);
	for (j = 0 ; j < 3 ; j++) // Pour chaque sommet de la face f
	{
	    s = extraire_sommet(f, j);
	    if (s != centre)
	    {
		if (s == s1)
		{
		    if (face_ok == NULL)
			face_ok = f;
		    else
		    {
			continuer = 0;
			break;
		    }
		}
		else if (s != s2 && sont_voisins(m, s, s1))
		{
		    if (face_ok == NULL)
			face_ok = f;
		    else
		    {
			continuer = 0;
			break;
		    }
		}
	    }
	}
    }

    if (face_ok == NULL)
	return;

    sommet first = NULL;
    sommet second = NULL;
    if (centre == extraire_sommet(face_ok, 0))
    {
	if (s1 == extraire_sommet(face_ok, 1))
	{
	    first = s2;
	    second = s1;
	}
	else
	{
	    first = s1;
	    second = s2;
	}
    }
    else if (centre == extraire_sommet(face_ok, 1))
    {
	if (s1 == extraire_sommet(face_ok, 2))
	{
	    first = s2;
	    second = s1;
	}
	else
	{
	    first = s1;
	    second = s2;
	}
    }
    else
    {
	if (s1 == extraire_sommet(face_ok, 0))
	{
	    first = s2;
	    second = s1;
	}
	else
	{
	    first = s1;
	    second = s2;
	}
    }

    vecteur cen = convertir_en_vecteur(extraire_point(centre));
    vecteur v1 = convertir_en_vecteur(extraire_point(first));
    vecteur v2 = convertir_en_vecteur(extraire_point(second));
    vecteur_diff(cen, v1); // v1 = vecteur de cen au premier sommet
    vecteur_diff(cen, v2); // v2 = vecteur de cen à l'autre sommet
    if (vecteur_angle(v1, v2) < 2.5307f) // 2.5307 = 145 degrés en radians
    {
	f = new_face(centre, first, second, vecteur_nul, couleur_default());
	ajouter_face(m, f);
	(*nb_corrections)++;
    }
    else
	fprintf(stderr, "vecteur trop grand : %f\n", vecteur_angle(v1, v2));

    supprimer_vecteur(v1);
    supprimer_vecteur(v2);
    supprimer_vecteur(cen);
}

void corr_faces_manquantes (maillage m)
{
    const int nb_soms = nombre_de_sommets (m);
    int i = 0, j = 0, k = 0, nb_incidentes = 0;
    tableau *voisins = NULL;
    tableau incidentes = NULL, sommets_ok = NULL, sommets_tous = NULL;
    face f = NULL;
    sommet s = NULL;
    sommet s2 = NULL;
    sommet centre = NULL;
    int nb_corrections = 0;

    for (i = 0 ; i < nb_soms ; i++) // Pour chaque sommet
    {
	voisins = sommets_voisins(m, i, 1);
	centre = obtenir_sommet(m, i);
	incidentes = faces_incidentes(m, i);
	nb_incidentes = taille_tableau(incidentes);

	// Si les tailles sont différentes, c'est que problème il y a
	if (taille_tableau(voisins[1]) != nb_incidentes)
	{
	    sommets_tous = new_tableau();
	    sommets_ok = new_tableau();

	    for (j = 0 ; j < nb_incidentes ; j++) // Pour chaque face incidente
	    {
		f = acceder_element(incidentes, j, face);
		for (k = 0 ; k < 3 ; k++) // Pour chaque sommet de la face f
		{
		    s = extraire_sommet(f, k);
		    if (s != centre) // Si s n'est pas le sommet central
		    {
			if (recherche_tableau(sommets_tous, s) == -1)
			    ajouter_element(sommets_tous, s);
			else // S'il y a un doublon c'est que le sommet est ok
			    ajouter_element(sommets_ok, s);
		    }
		}
	    }

	    // sommets_tous contiendra les sommets à problème
	    sommets_tous = supprime_elements(sommets_tous, sommets_ok);

	    if (taille_tableau(sommets_tous) % 2 != 0)
	    {
		supprimer_tableau(sommets_ok);
		supprimer_tableau(sommets_tous);
		continue;
	    }
	    /*{
	      fprintf(stderr, "Erreur : il y a %d sommets \"isoles\" !\n",
	      taille_tableau(sommets_tous));
	      exit(EXIT_FAILURE);
	      }*/

	    if (taille_tableau(sommets_tous) == 2)
	    {
		_corrfm_ajouter_face(m, centre,
			acceder_element(sommets_tous, 0, sommet),
			acceder_element(sommets_tous, 1, sommet), &nb_corrections,
			incidentes);
	    }
	    else
	    {
		int nb_faces_a_creer = taille_tableau(sommets_tous)/2;
		int faces_creees = 0;
		for (j = 0 ; j < nb_faces_a_creer*2 ; j++)
		{
		    s = acceder_element(sommets_tous, j, sommet);
		    for (k = 0 ; k < nb_faces_a_creer ; k++)
		    {
			s2 = acceder_element(sommets_tous, k, sommet);
			if (sont_voisins(m, s, s2))
			{
			    _corrfm_ajouter_face(m, centre, s, s2,
				    &nb_corrections, voisins[1]);
			    faces_creees++;
			}
		    }

		    if (faces_creees == nb_faces_a_creer)
			break;
		}
	    }

	    supprimer_tableau(sommets_ok);
	    supprimer_tableau(sommets_tous);
	}

	supprimer_tableau(voisins[0]);
	supprimer_tableau(voisins[1]);
	liberer_mem(voisins);
	supprimer_tableau(incidentes);
    }

    if (nb_corrections == 0)
	printf_ncurses("Aucun \"trou\" repere dans le maillage.\n");
    else
	printf_ncurses("Ajout de %d faces manquantes.\n", nb_corrections);
}



int minimum_3_float(float a, float b, float c)
{
    if( a < b )
    {
	return ( a < c ? 0 : 2 );
    }
    else
    {
	return ( b < c ? 1 : 2 );
    }
}

float distance_projete(point a, point droite1, point droite2)
{
    float distance;

    float x_droite1_a = extraire_coordonnee_point(a, 0) - extraire_coordonnee_point(droite1, 0);
    float y_droite1_a = extraire_coordonnee_point(a, 1) - extraire_coordonnee_point(droite1, 1);
    float z_droite1_a = extraire_coordonnee_point(a, 2) - extraire_coordonnee_point(droite1, 2);
    float x_droite2_droite1 = extraire_coordonnee_point(droite1, 0) - extraire_coordonnee_point(droite2, 0);
    float y_droite2_droite1 = extraire_coordonnee_point(droite1, 1) - extraire_coordonnee_point(droite2, 1);
    float z_droite2_droite1 = extraire_coordonnee_point(droite1, 2) - extraire_coordonnee_point(droite2, 2);

    vecteur droite1_a = vecteur_nouveau( x_droite1_a, y_droite1_a, z_droite1_a);
    vecteur droite2_droite1 = vecteur_nouveau( x_droite2_droite1, y_droite2_droite1, z_droite2_droite1);

    vecteur droite1_a_Vect_droite2_droite1 = vecteur_prod_vect (droite1_a, droite2_droite1);

    distance = vecteur_norme(droite1_a_Vect_droite2_droite1) / vecteur_norme(droite2_droite1);

    supprimer_vecteur(droite1_a);
    supprimer_vecteur(droite2_droite1);
    supprimer_vecteur(droite1_a_Vect_droite2_droite1);

    return distance;
}
void afficher_sommet_deg(sommet s)
{
    point ps=extraire_point(s);
    printf("%f %f %f\n",extraire_coordonnee_point(ps,0),
	    extraire_coordonnee_point(ps,1),extraire_coordonnee_point(ps,2));
}


void afficher_face_deg(maillage m,face f4)
{

    point pf41=extraire_point(extraire_sommet(f4,0));
    printf("point 0, d'indice %d :%f %f %f\n",indice_sommet(m,extraire_sommet(f4,0)),extraire_coordonnee_point(pf41,0),
	    extraire_coordonnee_point(pf41,1),extraire_coordonnee_point(pf41,2));
    point pf42=extraire_point(extraire_sommet(f4,1));
    printf("point 1, d'indice %d :%f %f %f\n",indice_sommet(m,extraire_sommet(f4,1)),extraire_coordonnee_point(pf42,0),
	    extraire_coordonnee_point(pf42,1),extraire_coordonnee_point(pf42,2));
    point pf43=extraire_point(extraire_sommet(f4,2));
    printf("point 2, d'indice %d :%f %f %f\n",indice_sommet(m,extraire_sommet(f4,2)),extraire_coordonnee_point(pf43,0),
	    extraire_coordonnee_point(pf43,1),extraire_coordonnee_point(pf43,2));
    printf("\n\n");

}

void corr_faces_degenerees ( maillage m )
{
    int k=0;
    int nbf=nombre_de_faces(m);
    face f;
    float facteur;
    point pf0;
    point pf1;
    point pf2;
    float distance_pf0_projete;
    float distance_pf1_projete;
    float distance_pf2_projete;
    int min;
    point plus_proche;
    int indice_plus_proche;
    float x_plus_proche_projete;
    float y_plus_proche_projete;
    float z_plus_proche_projete;
    point plus_proche_projete;
    sommet sommet_plus_proche_projete;
    float x_vecteur_projete;
    float y_vecteur_projete;
    float z_vecteur_projete;
    vecteur vecteur_projete;
    float distance_point_projete;
    float distance_autre_point_projete;
    point origine_vecteur_projete;
    point troisieme_point;
    tableau faces_inc_f;
    int nb_faces_inc_f;
    int i;
    int l;
    int j;
    face face_inc;
    face face_deux_autres_points;
    face face_coupee_en_deux;


    for (k=0;k<nbf;++k) //Parcours des faces
    {


	if( (facteur=facteur_quali((f=obtenir_face(m,k))))==1) //La face est dégénérée
	{

	    supprimer_face(m , k);
	    corr_faces_degenerees( m );
	    break;
	}

	else if( facteur>0.9 ) //La face est mauvaise
	{
	    afficher_face_deg(m,f);
	    pf0 = extraire_point(extraire_sommet(f,0));
	    pf1 = extraire_point(extraire_sommet(f,1));
	    pf2 = extraire_point(extraire_sommet(f,2));

	    distance_pf0_projete = distance_projete(pf0, pf1, pf2);
	    distance_pf1_projete = distance_projete(pf1, pf2, pf0);
	    distance_pf2_projete = distance_projete(pf2, pf0, pf1);

	    min = minimum_3_float(distance_pf0_projete, distance_pf1_projete, distance_pf2_projete);

	    if(min == 0)
	    {
		plus_proche = pf0;
		indice_plus_proche = indice_sommet(m, extraire_sommet(f,0));

		x_vecteur_projete = extraire_coordonnee_point(pf2, 0) - extraire_coordonnee_point(pf1, 0);
		y_vecteur_projete = extraire_coordonnee_point(pf2, 1) - extraire_coordonnee_point(pf1, 1);
		z_vecteur_projete = extraire_coordonnee_point(pf2, 2) - extraire_coordonnee_point(pf1, 2);

		distance_point_projete = distance_pf0_projete;

		origine_vecteur_projete = pf1;
		troisieme_point = pf2;
	    }
	    if(min == 1)
	    {
		plus_proche = pf1;
		indice_plus_proche = indice_sommet(m, extraire_sommet(f,1));

		x_vecteur_projete = extraire_coordonnee_point(pf0, 0) - extraire_coordonnee_point(pf2, 0);
		y_vecteur_projete = extraire_coordonnee_point(pf0, 1) - extraire_coordonnee_point(pf2, 1);
		z_vecteur_projete = extraire_coordonnee_point(pf0, 2) - extraire_coordonnee_point(pf2, 2);

		distance_point_projete = distance_pf1_projete;

		origine_vecteur_projete = pf2;
		troisieme_point = pf0;
	    }
	    if(min == 2)
	    {
		plus_proche = pf2;
		indice_plus_proche = indice_sommet(m, extraire_sommet(f,2));

		x_vecteur_projete = extraire_coordonnee_point(pf1, 0) - extraire_coordonnee_point(pf0, 0);
		y_vecteur_projete = extraire_coordonnee_point(pf1, 1) - extraire_coordonnee_point(pf0, 1);
		z_vecteur_projete = extraire_coordonnee_point(pf1, 2) - extraire_coordonnee_point(pf0, 2);

		distance_point_projete = distance_pf2_projete;

		origine_vecteur_projete = pf0;
		troisieme_point = pf1;
	    }

	    vecteur_projete = vecteur_nouveau( x_vecteur_projete, y_vecteur_projete, z_vecteur_projete);

	    vecteur_normalise(vecteur_projete );


	    distance_autre_point_projete = sqrt( abs(distance_point(plus_proche, origine_vecteur_projete)*distance_point(plus_proche, origine_vecteur_projete) - distance_point_projete*distance_point_projete) ); //Théorème de Pythagore
	    printf("distance autre point projete: %f\n", distance_point(plus_proche, origine_vecteur_projete)*distance_point(plus_proche, origine_vecteur_projete) - distance_point_projete*distance_point_projete);
	    printf("distance autre point projete: %f\n", distance_autre_point_projete);

	    vecteur_mult(vecteur_projete, distance_autre_point_projete);


	    x_plus_proche_projete = extraire_coordonnee_point(origine_vecteur_projete, 0) + extraire_composante(vecteur_projete, 0);
	    y_plus_proche_projete = extraire_coordonnee_point(origine_vecteur_projete, 1) + extraire_composante(vecteur_projete, 1);
	    z_plus_proche_projete = extraire_coordonnee_point(origine_vecteur_projete, 2) + extraire_composante(vecteur_projete, 2);
	    plus_proche_projete = new_point(x_plus_proche_projete, y_plus_proche_projete, z_plus_proche_projete); //Création du point projeté
	    sommet_plus_proche_projete = new_sommet(plus_proche_projete, NULL,NULL); //Transformation en sommet

	    ajouter_sommet(m, sommet_plus_proche_projete); //Ajout du projeté dans le maillage

	    printf("sommet_plus_proche_projete: \n");
	    afficher_sommet_deg(sommet_plus_proche_projete);
	    printf("indice_plus_proche_projete: %d\n", indice_sommet(m, sommet_plus_proche_projete));

	    printf("indice_plus_proche: %d \n", indice_plus_proche);
	    faces_inc_f = faces_incidentes(m, indice_plus_proche); //Correction
	    nb_faces_inc_f = taille_tableau(faces_inc_f);

	    for(i=0; i<nb_faces_inc_f; i++)
	    {
		// Si les deux faces sont identiques on ne fait rien
		if( f!=acceder_element(faces_inc_f, i, face) )
		{
		    face_inc=acceder_element(faces_inc_f, i, face);
		    for(l=0; l<3; l++)
		    {
			if(plus_proche==extraire_point(extraire_sommet(face_inc, l))) //On remplace dans les faces incidentes le sommet que l'on va supprimer par son projeté orthogonal
			{
			    modif_face_sommets(face_inc, sommet_plus_proche_projete, extraire_sommet(face_inc, (l+1)%3), extraire_sommet(face_inc, (l+2)%3));
			    break;
			}

		    }
		}
	    }

	    //recherche de la face qui contient les 2autres points que celui projeté
	    for (i=0;i<nbf;++i) //Parcours des faces
	    {
		face_deux_autres_points=obtenir_face(m,i);
		if(face_deux_autres_points!=f )
		{
		    for(l=0; l<3; l++)
		    {
			if(comp_point(origine_vecteur_projete, extraire_point(extraire_sommet(face_deux_autres_points, l)))==1)
			{
			    for(j=0; j<3; j++)
			    {
				if(comp_point(troisieme_point, extraire_point(extraire_sommet(face_deux_autres_points, j)))==1)
				{
				    face_coupee_en_deux = new_face(sommet_plus_proche_projete ,extraire_sommet(face_deux_autres_points, (l+1)%3), extraire_sommet(face_deux_autres_points, (l+2)%3), NULL,NULL);
				    ajouter_face(m, face_coupee_en_deux); //Ajout de la face dans le maillage
				    printf("\n\nFace_coupee_en_deux:\n");
				    afficher_face_deg(m,face_coupee_en_deux);
				    modif_face_sommets(face_deux_autres_points, sommet_plus_proche_projete, extraire_sommet(face_inc, (j+1)%3), extraire_sommet(face_inc, (j+2)%3));
				    //on arrete les boucles
				    //l=3;
				    //j=3;
				}
			    }
			}
		    }
		}
	    }

	    supprimer_vecteur(vecteur_projete);
	    //supprimer_tableau(faces_inc_f); Supprime également les faces du maillage ?
	    //Supprimer la face f du maillage et du point
	    supprimer_face(m , k); //J'espère que les points de la face ne sont pas supprimé aussi
	    supprimer_sommet(m, indice_plus_proche);

	    corr_faces_degenerees( m );
	    break; //Le tableau des faces du maillage va completement changer, il est préférable de stopper la boucle et de rappeler la fonction sur tout le maillage et donc de traiter les faces dégénérées une par une
	}
    }
}

/*
   void corr_faces_degenerees ( maillage m )
   {
   tableau a_corriger = recherche_faces_degenerees ( m );
   suppression ( m , a_corriger , 'f' );
   return;
   }


   void correction_faces_degenerees ( maillage m )
   {
   tableau t = recherche_faces_degenerees ( m );
   int k;
   const int taille = taille_tableau ( t );

   for ( k = 0 ; k <taille ; ++k )
   {
   face f = obtenir_face ( m , acceder_element ( t , k , int ) );
   sommet s0 = extraire_sommet ( f , 0 );
   sommet s1 = extraire_sommet ( f , 1 );
   sommet s2 = extraire_sommet ( f , 2 );
   }

   return;
   }
 */

/*
   tableau recherche_faces_malorientees ( maillage m )
   {
   int k=0;
   int i=0;
   tableau faces_malorienteestab = new_tableau();
   const int nbf = nombre_de_faces ( m );

   for ( k = 0 ; k < nbf ; ++k )
   {

   {
   ajouter_element (faces_malorienteestab,obtenir_face(m,i));
   }
   }
   return faces_malorienteestab;
   }

   int mal_orientee ( face f )
   {
   tableau voisines = faces_voisines ( f );
//...
}
 */


void flip_face ( maillage m , int i ){

    face f;
    f = obtenir_face ( m , i );

    sommet s1, s2, s3;

    s1 = extraire_sommet ( f, 0 );
    s2 = extraire_sommet ( f, 1 );
    s3 = extraire_sommet ( f, 2 );


    modif_face_sommets(f, s3, s2, s1);

    return;
}

int test_orientation ( maillage m , int i ){

    face f;
    f = obtenir_face ( m , i );
    vecteur vect_ref;
    vect_ref = extraire_normale_f ( f );
    int j;

    tableau tab = new_tableau ();
    tab = faces_voisines ( m , i );
    const int N = taille_tableau ( tab );
    face face_temp;
    vecteur vect_temp;


    for ( j = 0 ; j < N ; j++ ){

	face_temp = acceder_element ( tab , j , face );

	vect_temp = extraire_normale_f ( face_temp );

	/* 2,35619449 rad = 135 deg */

	if ( vecteur_angle ( vect_temp , vect_ref )  > 2.35619449 ){
	    supprimer_tableau ( tab );
	    return indice_face ( m , face_temp);

	}
    }

    supprimer_tableau ( tab );
    return (-1);

}

void corr_faces_malorientees(maillage m){

    int i, j;
    const int N = nombre_de_faces ( m );


    for ( i = 0 ; i < N ; i++ ){

	if ( (j = test_orientation ( m , i )) != -1 ){

	    flip_face ( m , j );

	};
    }

    return;
}
