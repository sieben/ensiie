/**
 * @file   affichage.h
 * @author Michel Teddy
 *
 * Affichage des maillages avec OpenGL et Glut.
 */

#ifndef FICHIER_AFFICHAGE_H
#define FICHIER_AFFICHAGE_H

#include <stdbool.h>
#include "maillages.h"

/**
 * @defgroup affichage Affichage
 * @author   Michel Teddy
 * @date     04 mai 2010 : Création.
 * @date     11 mai 2010 : Utilisation de Glut à la place de la SDL.
 * @date     18 mai 2010 : Gestion des déplacements au clavier et à la souris.
 *
 * Affichage des maillages avec OpenGL et Glut.
 */

/*@{*/


typedef struct t_dossier * dossier;
typedef struct t_fichier * fichier;

/// Structure représentant un dossier.
struct t_dossier
{
    char * name;   ///< Nom du dossier
    fichier files; ///< Premier fichier du dossier.
    dossier dirs;  ///< Premier sous-dossier du dossier.
    dossier next;  ///< Dossier suivant.
};

/// Structure représentant un fichier.
struct t_fichier
{
    char * name;   ///< Nom du fichier.
    fichier next;  ///< Fichier suivant.
};


/**
 * Indique si un fichier est un dossier.
 *
 * @param path Adresse du fichier.
 * @return Booléen.
 */

bool est_un_dossier ( const char * path );


/**
 * Liste le contenu d'un dossier.
 *
 * @param path Adresse du dossier.
 * @return Arbre représentant l'arborescence des fichiers et dossiers.
 */

dossier liste_dossier ( const char * path );


/**
 * Libère la mémoire allouée pour liste le contenu d'un dossier.
 *
 * @param racine Racine de l'arborescence listée.
 */

void supprimer_liste_dossier ( dossier racine );


/**
 * Crée la fenetre et initialise l'affichage avec Glut et OpenGL.
 *
 * @param argc : Nombre d'arguments envoyés au programme.
 * @param argv : Tableau des arguments.
 *
 * @pre  Aucune.
 * @post Affichage de la fenetre et création du contexte OpenGL.
 */

void affiche_init ( int argc , char * argv[] );


/**
 * Change le maillage à afficher.
 *
 * @param m Maillage à afficher.
 */

void affiche_maillage ( maillage m );


/**
 * Boucle d'affichage.
 *
 * @pre  L'affichage est correctement initialisé.
 * @post Aucune.
 */

void affiche_boucle ( void );

/*@}*/

#endif
