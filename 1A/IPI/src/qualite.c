/**
 * @file qualite.c
 * @brief Module contenant les traitements liés aux calculs de la qualité
 * d'une face.
 * @author Iraqi Sarah
 * @author Crémèse Jean
 * @date 13 avril 2010
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "face.h"
#include "maillages.h"

/*
 * Calcul de la qualité d'une face en fonction de la distance par rapport à un
 * point.
 * On calcule la plus petite distance entre la face et le point. Pour cela, on
 * prend le barycentre de la face.
 *
 * @param f la face dont on souhaite calculer la qualité
 * @param p le point de référence
 * @pre la face et le point sont correctement formés
 * @return une valeur positive
 */

float distance_point_quali(face f, point p)
{
    if(!bienforme_face(f))
    {
        fprintf(stderr, "distance_point_quali : La face est mal formée.\n");
        exit(EXIT_FAILURE);
    }

    if(!bienforme_point(p))
    {
        fprintf(stderr, "distance_point_quali : Le point est mal formé.\n");
        exit(EXIT_FAILURE);
    }

    return distance_point(barycentre_face(f),p);
}

/*
 * Calcul de la qualité d'une face en fonction de la distance par rapport à un
 * axe.
 * On calcule la plus petite distance entre la face et l'axe. Pour cela, on
 * prend le barycentre de la face.
 *
 * @param f la face dont on souhaite calculer la qualité
 * @param p le point de référence de l'axe
 * @param v le vecteur définissant l'axe
 * @pre la face, le point et le vecteur sont correctement formés
 * @return une valeur positive
 */

float distance_axe_quali(face f, point p, vecteur v)
{
    if(!bienforme_face(f))
    {
        fprintf(stderr, "distance_axe_quali : La face est mal formée.\n");
        exit(EXIT_FAILURE);
    }

    if(!bienforme_point(p))
    {
        fprintf(stderr, "distance_axe_quali : Le point est mal formé");
        exit(EXIT_FAILURE);
    }

    /* On extrait les coordonnées du barycentre G de la face */
    float xg=extraire_coordonnee_point(barycentre_face(f),0);
    float yg=extraire_coordonnee_point(barycentre_face(f),1);
    float zg=extraire_coordonnee_point(barycentre_face(f),2);

    /* On extrait les coordonnées du point P */
    float xp=extraire_coordonnee_point(p,0);
    float yp=extraire_coordonnee_point(p,1);
    float zp=extraire_coordonnee_point(p,2);

    vecteur w = vecteur_nouveau(xg-xp,yg-yp,zg-zp); // On créé le vecteur BG
    return vecteur_norme(vecteur_prod_vect(w,v))/(vecteur_norme(v));
}

/*
 * Calcul de la qualité d'une face en fonction de la distance par rapport à un
 * plan définis par un point et deux vecteurs.
 * On calcule la plus petite distance entre la face et le plan.Pour cela,
 * on prend le barycentre de la face.
 *
 * @param f la face dont on souhaite calculer la qualité
 * @param p le point de référence du plan
 * @param v1 le premier vecteur définissant le plan
 * @param v2 le second vecteur définissant le plan
 * @pre la face, le point et les deux vecteurs sont correctement formés
 * @return une valeur positive 
 */

float distance_plan_quali(face f, point p, vecteur v1,vecteur v2)
{
    if(!bienforme_face(f))
    {
        fprintf(stderr, "distance_plan_quali : la face est mal formée.\n");
        exit(EXIT_FAILURE);
    }

    if(!bienforme_point(p))
    {
        fprintf(stderr, "distance_plan_quali : le point est mal formé.\n");
        exit(EXIT_FAILURE);
    }

    /* On extrait les coordonnées du barycentre G de la face */
    float xg = extraire_coordonnee_point(barycentre_face(f),0);
    float yg = extraire_coordonnee_point(barycentre_face(f),1);
    float zg = extraire_coordonnee_point(barycentre_face(f),2);
    vecteur g=vecteur_nouveau(xg,yg,zg); /* On créé le vecteur OG pour pouvoir
                                          * réaliser un produit scalaire */

    vecteur n=vecteur_prod_vect (v1,v2); /* Calcul de la normale au plan */
    float xm=extraire_coordonnee_point(p,0);
    float ym=extraire_coordonnee_point(p,1);
    float zm=extraire_coordonnee_point(p,2);
    vecteur m=vecteur_nouveau(xm,ym,zm); /* On créé le vecteur OM pour pouvoir
                                          * réaliser un produit scalaire */
    float d=-(vecteur_prod_scal(m,n)); /* On détermine la quatrième composante
                             de l'équation du plan de référence dans l'espace */
    return (fabsf(vecteur_prod_scal(g,n)+d))/(sqrt(vecteur_prod_scal(n,n)));
}


/*
 * Calcul de la qualité d'une face en fonction de la courbure locale du maillage
 * On calcule la courbure à l'aide des normales des faces adjacentes : on
 * détermine l'angle entre la normale de la face passée en argument et celles
 * de ses faces voisines. On calcule un facteur de qualité à partir des trois
 * rayons de courbures obtenus.
 *
 * @param indice l'indice de la face dont on souhaite calculer la qualité
 * @param m le maillage qui contient la face 
 * @pre la face est correctement formée
 * @return une valeur positive comprise entre 0 et Pi
 */

float courbure_quali(int indice, maillage m)
{
    int i;
    float s = 0;
    tableau voisins = faces_voisines (m,indice);
    const int taille = taille_tableau (voisins);

    for ( i = 0 ; i < taille ; i++ )
    {
        face f = acceder_element (voisins, i, face);
        s += courbure_deux_faces(f,obtenir_face(m,indice));
    }

    s /= taille;
    return s;
}    

/*
 * Calcul de la qualité d'une face à l'aide du facteur de qualité.
 *
 * @param f la face dont on souhaite calculer la qualité
 * @pre la face est correctement formée
 * @return une valeur positive
 */

float facteur_quali(face f)
{
    if(!bienforme_face(f))
    {
        fprintf(stderr, "facteur_quali : La face est mal formée.\n");
        exit(EXIT_FAILURE);
    }

    sommet s1 = extraire_sommet ( f , 0 );
    sommet s2 = extraire_sommet ( f , 1 );
    sommet s3 = extraire_sommet ( f , 2 );

    /* On extrait les points de la face afin de construire les vecteurs
     * correspondant aux aretes */
    point p1 = extraire_point(s1);
    point p2 = extraire_point(s2);
    point p3 = extraire_point(s3);

    /* On calcule la longueurs des arêtes */
    float h1 = distance_point ( p1 , p2 );
    float h2 = distance_point ( p2 , p3 );
    float h3 = distance_point ( p1 , p3 );

    if(h1==0 || h2==0 || h3==0) return 1; //triangle plat


    vecteur v12 = vecteur_nouveau ( extraire_coordonnee_point(p2,0)-
                                    extraire_coordonnee_point(p1,0),
                                    extraire_coordonnee_point(p2,1)-
                                    extraire_coordonnee_point(p1,1),
                                    extraire_coordonnee_point(p2,2)-
                                    extraire_coordonnee_point(p1,2));

    vecteur v13 = vecteur_nouveau ( extraire_coordonnee_point(p3,0)-
                                    extraire_coordonnee_point(p1,0),
                                    extraire_coordonnee_point(p3,1)-
                                    extraire_coordonnee_point(p1,1),
                                    extraire_coordonnee_point(p3,2)-
                                    extraire_coordonnee_point(p1,2));

    float hauteur_triangle_initial = h3*sin(vecteur_angle(v13,v12));

    float surface_triangle_initial = abs( h1*hauteur_triangle_initial/2);

    float rayon_cercle_circonscrit = h2 / (sin(vecteur_angle(v13,v12)));

    float cote_triangle_equilateral = abs(2*rayon_cercle_circonscrit*sin(3.1415926535/3)); // Angle d'un triangle équilatéral: 60° ou pi/3 radian

    float surface_triangle_equilateral = cote_triangle_equilateral*cote_triangle_equilateral*sqrt(3)/4; // hauteur=coté*sqrt(3)/2 car triangle equilateral

    return (surface_triangle_equilateral - surface_triangle_initial) / surface_triangle_equilateral;
}


void coloration_normale ( maillage m )
{
    int i;
    const int nbr_faces = nombre_de_faces ( m );

    // On parcourt le tableau des faces
    for ( i = 0 ; i < nbr_faces ; ++i )
    {
        face f = obtenir_face ( m , i );
        couleur c = extraire_couleur ( f );

        if ( c == NULL )
        {
            c = couleur_default();
            modif_face_couleur ( f , c );
        }

        c->r = 255;
        c->v = 255;
        c->b = 255;
    }

    return;
}

void coloration_distance_point ( maillage m , point p )
{
    int i;
    const int nbr_faces = nombre_de_faces ( m );
    float * distances = allouer_mem ( nbr_faces * sizeof(float) );
    float max = 0.0f; // Distance maximale

    // On parcourt le tableau des faces
    for ( i = 0 ; i < nbr_faces ; ++i )
    {
        face f = obtenir_face ( m , i );
        distances[i] = distance_point_quali ( f , p );

        if ( distances[i] > max ) max = distances[i];
    }

    // On parcourt le tableau des faces
    for ( i = 0 ; i < nbr_faces ; ++i )
    {
        face f = obtenir_face ( m , i );
        couleur c = extraire_couleur ( f );

        if ( c == NULL )
        {
            c = couleur_default();
            modif_face_couleur ( f , c );
        }

        c->r = (unsigned char) ( 255.0f * distances[i] / max );
        c->v = (unsigned char) ( 255.0f * ( 1.0f - distances[i] / max ) );
        c->b = 0;
    }

    return;
}

void coloration_distance_axe ( maillage m , point p , vecteur v )
{
    int i;
    const int nbr_faces = nombre_de_faces ( m );
    float * distances = allouer_mem ( nbr_faces * sizeof(float) );
    float max = 0.0f; // Distance maximale

    // On parcourt le tableau des faces
    for ( i = 0 ; i < nbr_faces ; ++i )
    {
        face f = obtenir_face ( m , i );
        distances[i] = distance_axe_quali ( f , p , v );

        if ( distances[i] > max ) max = distances[i];
    }

    // On parcourt le tableau des faces
    for ( i = 0 ; i < nbr_faces ; ++i )
    {
        face f = obtenir_face ( m , i );
        couleur c = extraire_couleur ( f );

        if ( c == NULL )
        {
            c = couleur_default();
            modif_face_couleur ( f , c );
        }

        c->r = (unsigned char) ( 255.0f * distances[i] / max );
        c->v = (unsigned char) ( 255.0f * ( 1.0f - distances[i] / max ) );
        c->b = 0;
    }

    return;
}

void coloration_distance_plan ( maillage m , point p , vecteur v1 , vecteur v2 )
{
    int i;
    const int nbr_faces = nombre_de_faces ( m );
    float * distances = allouer_mem ( nbr_faces * sizeof(float) );
    float max = 0.0f; // Distance maximale

    // On parcourt le tableau des faces
    for ( i = 0 ; i < nbr_faces ; ++i )
    {
        face f = obtenir_face ( m , i );
        distances[i] = distance_plan_quali ( f , p , v1 , v2 );

        if ( distances[i] > max ) max = distances[i];
    }

    // On parcourt le tableau des faces
    for ( i = 0 ; i < nbr_faces ; ++i )
    {
        face f = obtenir_face ( m , i );
        couleur c = extraire_couleur ( f );

        if ( c == NULL )
        {
            c = couleur_default();
            modif_face_couleur ( f , c );
        }

        c->r = (unsigned char) ( 255.0f * distances[i] / max );
        c->v = (unsigned char) ( 255.0f * ( 1.0f - distances[i] / max ) );
        c->b = 0;
    }

    return;
}

void coloration_facteur_qualite ( maillage m )
{
    int i;
    const int nbr_faces = nombre_de_faces ( m );

    // On parcourt le tableau des faces
    for ( i = 0 ; i < nbr_faces ; ++i )
    {
        face f = obtenir_face ( m , i );
        float q = facteur_quali ( f );
        couleur c = extraire_couleur ( f );

        if ( c == NULL )
        {
            c = couleur_default();
            modif_face_couleur ( f , c );
        }

        c->r = (unsigned char) ( 255.0f * ( 1.0f - q ) );
        c->v = (unsigned char) ( 255.0f * q );
        c->b = 0;
    }

    return;
}

void coloration_courbure ( maillage m )
{
    int i;
    const int nbr_faces = nombre_de_faces ( m );

    // On parcourt le tableau des faces
    for ( i = 0 ; i < nbr_faces ; ++i )
    {
        float q = courbure_quali ( i , m ) / 3.0f;
        face f = obtenir_face ( m , i );
        couleur c = extraire_couleur ( f );

        if ( c == NULL )
        {
            c = couleur_default();
            modif_face_couleur ( f , c );
        }

        c->r = (unsigned char) ( 255.0f * q );
        c->v = (unsigned char) ( 255.0f * ( 1.0f - q ) );
        c->b = 0;
    }

    return;
}
