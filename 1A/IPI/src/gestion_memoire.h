/**
 * @file gestion_memoire.h
 * @brief Module de gestion de la mémoire et des tableaux
 * @author Simon Lasnier
 */

#ifndef MEMOIRE_H
#define MEMOIRE_H

#include <stddef.h> // Pour utiliser size_t
#include <stdlib.h> // Pour utiliser free

/**
 * @defgroup gestion_memoire Gestion mémoire
 * @brief Module de gestion de la mémoire et des tableaux
 * @author Simon Lasnier
 */

/*@{*/

/**
 * @brief Ajoute un élément à un tableau
 * @param t tableau auquel on veut ajouter l'élément
 * @param element élément que l'on veut ajouter, casté en void* automatiquement
 * @return l'indice de l'élément ajouté
 * @sa ajouter_element_aux
 */

#define ajouter_element(t, element) ajouter_element_aux(t, (void*)(element))

/**
 * @brief Remplace un élément d'un tableau par un élément donné
 * @param t tableau dont on veut remplacer un élément
 * @param indice indice de l'élément à remplacer dans le tableau
 * @param nouveau élément que l'on veut insérer, casté en void* automatiquement
 * @sa remplacer_element_aux
 */

#define remplacer_element(t, indice, nouveau) remplacer_element_aux(t, indice, (void*) (nouveau))

/**
 * @brief Accéde à un élément d'un tableau
 * @param t tableau dont on veut l'élément
 * @param indice indice de l'élément
 * @param type le type des éléments du tableau
 * @return l'élément correspondant à l'indice, casté en "type"
 * @sa acceder_element_aux
 */

#define acceder_element(t, indice, type)  (type) acceder_element_aux(t, indice)

/**
 * @brief recherche un élément dans un tableau
 * @param t le tableau dans lequel on cherche
 * @param element l'élément que l'on recherche, automatiquement casté en void*
 * @return l'indice de l'élément s'il a été trouvé, -1 sinon
 * @sa recherche_tableau_aux
 */

#define recherche_tableau(t, element) recherche_tableau_aux(t, (void*)(element))


/**
 * @struct tableau
 * @brief Structure pour gérer les tableaux de manière plus propre.
 */
typedef struct t_tableau* tableau;


/**
 * @brief Alloue de la mémoire (à la manière de malloc) avec gestion des erreurs
 * @param taille nombre d'octets à allouer
 * @pre la taille founie en paramètre n'est pas nulle
 * @post un pointeur vers la zone mémoire allouée est renvoyé
 * @return un pointeur vers la zone mémoire allouée
 * @test demander l'allocation de 0 octets de mémoire
 */

void* allouer_mem (size_t taille) ;

/**
 * Réalloue de la mémoire précédemment allouée (à la manière de
 * realloc) avec gestion des erreurs
 * @param pointeur pointeur vers la zone mémoire précédemment allouée ou NULL
 * @param taille nouvelle taille (en octets) de la zone mémoire a allouer
 * @pre la taille fournie en paramètre n'est pas nulle
 * @post un pointeur vers la zone mémoire agrandie, diminuée, ou créée (si le
 * pointeur fourni en paramètre vallait NULL) est renvoyé
 * @return un pointeur vers la zone mémoire :
 *      - agrandie ou diminuée si pointeur n'est pas NULL
 *      - créée si pointeur est NULL
 * @test réallouer de la mémoire qui n'a pas été allouée (NULL)
 * @test réallouer 0 octets de mémoire
 */

void* reallouer_mem (void *pointeur, size_t taille) ;

/**
 * Alloue et initialise à 0 (à la manière de calloc)
 * nb_elts * taille_elt octets de mémoire avec gestion des erreurs
 * @param nb_elts nombre d'éléments à allouer
 * @param taille_elt taille (en octets) des éléments à allouer
 * @pre nb_elts et taille_elt ne sont pas nuls
 * @post un pointeur vers la zone mémoire allouée est renvoyé
 * @return un pointeur vers la zone mémoire allouée
 * @test allouer la mémoire pour 0 éléments
 * @test allouer la mémoire pour des éléments de taille 0
 */

void* allouer_et_init_mem (size_t nb_elts, size_t taille_elt) ;

/**
 * Libère la mémoire précédemment allouée par allouer_mem,
 * reallouer_mem ou allouer_et_init_mem
 * @param pointeur pointeur vers la zone mémoire a libérer
 * @pre pointeur n'est pas NULL
 * @post la mémoire pointée par pointeur est libérée
 * @test libérer la mémoire d'un pointeur NULL
 */

#define	    liberer_mem	    free

/**
 * @brief Créer un nouveau tableau (vide)
 * @post le tableau créé est initialisé à 0 éléments
 * @return le nouveau tableau créé
 */

tableau new_tableau () ;

/**
 * @brief Ajoute un élément à un tableau
 * @param t tableau auquel on veut ajouter l'élément
 * @param element élément que l'on veut ajouter, en utilisant la macro
 * ajouter_element il n'y a pas besoin de faire le cast
 * @pre t est un tableau valide
 * @post l'élément a été ajouté au tableau
 * @return l'indice de l'élément ajouté
 * @test ajouter un élément à un tableau non valide
 * @sa ajouter_element
 */

int ajouter_element_aux (tableau t, void* element) ;

/**
 * @brief Remplace un élément d'un tableau par un élément donné
 * @param t tableau auquel on veut ajouter l'élément
 * @param indice indice de l'élément que l'on veut remplacer, en utilisant
 * la macro remplacer_element il n'y a pas besoin de faire le cast
 * @param nouveau l'élément à insérer à la place de l'élément d'indice donné
 * @pre t est un tableau valide
 * @post l'élément a été remplacé dans le tableau
 * @test remplacer un élément d'un tableau non valide
 * @sa remplacer_element
 */

void remplacer_element_aux (tableau t, int indice, void* nouveau) ;

/**
 * @brief Supprime un élément à un tableau à partir de son indice.
 * Attention si on supprime un élément au milieu du tableau, ça va décaler tous
 * les autres ! Donc tous les indices supérieurs à celui qui a été supprimé
 * devront être diminués de 1.
 * @param t tableau auquel on veut supprimer l'élément
 * @param indice indice de l'élément
 * @pre t doit être un tableau valide et indice doit être positif et plus
 * petit que la taille du tableau
 * @post l'élément a été supprimé du tableau t
 * @test supprimer un élément dont l'indice est très grand
 * @test supprimer un élément dont l'indice est négatif
 * @test supprimer un élément d'un tableau vide
 */

void supprimer_element (tableau t, int indice) ;

/**
 * @brief Accéde à un élément d'un tableau
 * @param t tableau dont on veut l'élément
 * @param indice indice de l'élément
 * @pre t est un tableau valide et indice n'est pas négatif ni trop grand
 * @post l'élément correspondant à l'indice est renvoyé, le tableau est
 * inchangé
 * @return l'élément correspondant à l'indice, en utilisant la macro
 * acceder_element il n'y a pas besoin de caster
 * @test accéder à l'élément -1 du tableau
 * @test accéder à un élément d'un tableau vide
 * @sa acceder_element
 */

void* acceder_element_aux (const tableau t, int indice) ;

/**
 * @brief récupère la taille d'un tableau
 * @param t tableau dont on veut récupérer la taille
 * @pre t est un tableau valide
 * @post la taille de tableau est renvoyée, ce dernier est inchangé
 * @return la taille du tableau
 * @test récupérer la taille d'un tableau vide
 */

int taille_tableau (const tableau t) ;

/**
 * @brief recherche un élément dans un tableau
 * @param t le tableau dans lequel on cherche
 * @param element l'élément que l'on recherche, en utilisant la macro
 * recherche_tableau il n'y a pas besoin de caster
 * @pre t est un tableau valide
 * @post renvoi l'indice de l'élément s'il a été trouvé ou -1 s'il n'a pas été
 * trouvé ; le tableau est inchangé
 * @return l'indice de l'élément s'il a été trouvé, -1 sinon
 * @test rechercher dans un tableau invalide (NULL)
 * @sa recherche_tableau
 */

int recherche_tableau_aux (const tableau t, void* element) ;

/**
 * @brief Supprime dans un tableau les éléments d'un autre tableau.
 * @param t1 tableau sur lequel on supprimera les elements
 * @param t2 Tableau contenant les éléments à supprimer
 * @pre t1 et t2 sont deux tableaux valides et leurs éléments doivent être de
 * même type
 * @post tous les éléments présents dans t1 et dans t2 ne sont plus dans t1
 */

tableau supprime_elements (tableau t1 , const tableau t2) ;

/**
 * Ajoute les éléments d'un tableau à un autre tableau.
 *
 * @param t1 : Tableau.
 * @param t2 : Tableau contenant les éléments à ajouter.
 */

void fusionne_tableaux ( tableau t1 , const tableau t2 ) ;

/**
 * Supprime les doublons d'un tableau.
 *
 * @param t : Tableau.
 * @return le nouveau tableau n'ayant pas les doublons
 */

tableau supprime_doublons ( tableau t ) ;

/**
 * @brief vide un tableau
 * @param t tableau à vider
 * @pre t est un tableau valide (non NULL)
 * @post le tableau est vidé le tableau est dans le même état qu'après un
 * appel à new_tableau)
 * @test vider un tableau qui n'est qu'un pointeur NULL
 */

void vider_tableau (tableau t) ;

/**
 * @brief supprime un tableau après l'avoir vidé
 * @param t tableau à supprimer
 * @pre aucune
 * @post le tableau est vidé (par un appel à vider_tableau) puis la mémoire
 * occupée par la structure t_tableau est libérée. Si le tableau est NULL,
 * la fonction ne fait rien mais ne déclenche pas d'erreur.
 */

void supprimer_tableau (tableau t) ;

/**
 * @brief trie un tableau d'entiers à l'aide de qsort
 * @param t tableau à trier
 * @pre aucune
 * @post le tableau est trié dans l'ordre croissant.
 */

void trier_tableau_entiers(tableau t) ;

/*@}*/

#endif

