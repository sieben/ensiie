/**
 * @file detection_correction.h
 * @brief Module de détection et de correction des erreurs sur les maillages.
 * @author Olivi Hermann
 * @author Vatin Charles
 */

#ifndef DETECTION_CORRECTION_H
#define DETECTION_CORRECTION_H

#include "maillages.h"

/**
 * @defgroup detect_correct Détection & Correction
 * @brief Détection et correction des erreurs sur les maillages.
 * @author Olivi Hermann
 * @author Vatin Charles
 * @author Lasnier Simon
 */

/*@{*/

/**
 * Supprime dans un maillage les éléments d'un tableau (sommets ou faces).
 *
 * @param m Maillage
 * @param T Tableau
 * @param type Des éléments du tableau (caractère 'f' pour face, 's' pour sommet)
 * @pre Maillage valide, type 'f' ou 's' correspondant au contenu du tableau
 * @post Les éléments du tableau ont été supprimés dans le maillage
 * @test Créer un tableau de sommets puis de faces et vérifier que le maillage a
 *       bien été vidé des sommets ou faces contenu(e)s dans le tableau.
 */

void suppression(maillage m, tableau T, char type);

/**
 * Recherche les sommets référencés deux fois.
 *
 * @param m Maillage
 * @pre Maillage valide
 * @post Indices des sommets référencé deux fois (une occurence par doublon)
 *       stockés dans un tableau
 * @return tableau contenant les indices des sommets référencés deux fois (une
 *         occurence par doublon).
 * @test Maillage avec doublon de sommet (On prend deux faces triangulaires, de
 *       manière à former un carré ABCD. Sauf que la face ABD utilise le sommet
 *       D et que la face BCD utilise un sommet D' superposé a D. La fonction
 *       doit normalement supprimer le sommet D' et le remplacer par D dans la
 *       face BCD).
 */

tableau recherche_doublons_sommet(maillage m);

/**
 * Supprime les sommets très proches. La proximité des sommets est évalué par
 * rapport à la moyenne de la distance entre deux sommets voisins sur tout le
 * maillage, de là une borne supérieure est calculée en fonction de l'écart-type
 * et d'un entier déterminé par l'utilisateur (précision) pour déterminer les
 * sommets qui doivent etre surprimés (ie ceux dont la distance avec un de ses
 * voisins est inférieure a la borne).
 *
 * @param m Maillage
 * @param p Entier indiquant la précision que l'on veut pour les sommets très
 *          proches
 * @pre Maillage valide
 * @post Maillage corrigé
 * @test Maillage de type bol (paroi très fine) avec une précision judicieusement
 *       choisie ; il faut vérifier que la forme du bol n'a pas été altérée.
 */

void supprime_sommetsproches(maillage m, int p);

/**
 * Supprime les sommets dupliqués. (Appel de la fonction supprime_sommetsproches
 * et des fonctions recherche_doublons_sommet et suppression.)
 *
 * @param m Maillage.
 * @pre Maillage valide
 * @post Maillage corrigé
 * @test vérifier qu'on sort bien de la fonction si l'utilisateur rentre une
 *       précision incorrecte (ie négative).
 * @test reprendre également les tests éffectués pour les doublons et sommets
 *       proches.
 */

void corr_somm_dups(maillage m);

/**
 * Recherche les sommets non référencés.
 *
 * @param m Maillage
 * @pre Maillage valide
 * @post Indices des sommets non référencés stockés dans un tableau
 * @return tableau contenant les indices des sommets non référencés
 * @test Maillage avec sommets non référencés (simplement vérifier que les
 *       sommets du tableau retrouné ne sont bien pas utilisés par les faces du
 *       maillage).
 */

tableau recherche_somm_nonref(maillage m);

/**
 * Supprime les sommets non réferencés. (Appel de la fonction
 * recherche_somm_nonref et de suppression).
 *
 * @param m Maillage
 * @pre Maillage valide
 * @post Maillage corrigé
 * @test Reprendre le test précédent et vérifier que les sommets ne sont plus
 *       dans la liste des sommets du maillage.
 */

void corr_somm_nonref(maillage m);


/**
 * Recherche les faces dupliquées.
 *
 * @param m Maillage
 * @pre Maillage valide
 * @post Indices des faces dupliquées (une occurence par doublon) stockés dans
 *       un tableau.
 * @return tableau contenant les indices des faces dupliquées (une occurence par
 *         doublon).
 * @test Maillage avec doublon de faces (on crée deux faces superposées (qui
 *       utilisent les 3 même sommets) et on vérifie que une des faces se trouve
 *       bien dans le tableau renvoyé).
 */

tableau recherche_doublons_face(maillage m);

/**
 * Supprime les faces dupliquées. (Appel de la fonction recherche_doublons_face
 * et de la fonction suppression).
 *
 * @param m Maillage
 * @pre Maillage valide
 * @post Maillage corrigé
 * @test Reprendre le test précédent (effectué dans la fonction de recherche) et
 *       vérifier que la face n'est plus dans le tableau de faces du maillage.
 */

void corr_faces_dups(maillage m);

/**
 * Recherche les faces invalides. (Les faces sont considérées invalides quand
 * elles utilisent des sommets qui n'existe pas dans la liste des sommets du
 * maillage, dans ce cas on supprime simplement la face).
 *
 * @param m Maillage
 * @pre Maillage valide
 * @post Indices des faces invalides stockés dans un tableau
 * @return tableau contenant les indices des faces invalides
 * @test maillage avec faces invalides (créer avec une face avec un de ses sommets).
 */

tableau recherche_faces_invalides(maillage m);

/**
 * Supprime les faces invalides. (Appel de la fonction recherche_faces_invalides
 * et de la fonction suppression.)
 *
 * @param m Maillage
 * @pre Maillage valide
 * @post Maillage corrigé
 * @return void
 * @test maillage avec faces invalides
 */

void corr_faces_invalides(maillage m);

/**
 * Fonction auxilliaire pour la correction des sommets non manifolds. Renvoie
 * un booléen qui détermine si les trois sommets passés en paramètre forment une
 * face du mailllage.
 *
 * @param m Maillage
 * @param s0 sommet
 * @param s1 sommet
 * @param s2 sommet
 * @pre Maillage valide
 * @post Maillage corrigé
 * @test Rentrer 3sommets qui forment réellement une face du maillage (la focntion doit renvoyer 1)
 * @test Rentrer 3sommets qui ne forment pas une face du maillage (la focntion doit renvoyer 0)
 */

int estuneface(maillage m,sommet s0,sommet s1,sommet s2);

/**
 * Détecte les sommets non manifolds.
 *
 * @param m Maillage
 * @pre Maillage valide
 * @post le nombre de sommets non manifold est renvoyé
 * @return le nombre de sommets non manifold
 * @test maillage avec sommets non manifold
 */

int recherche_somm_nonmanifold(maillage m);

/**
 * Détecte les faces manquantes (les "trous") et les corrige.
 *
 * Cette fonction ne corrige que les trous "simples", c'est à dire les
 * trous créés par des sommets non manifold et des trous de une ou deux
 * faces manquantes. Elle ne corrige pas les "grands trous", qui
 * nécéssiteraient beaucoup de faces pour être comblés.
 *
 * @param m Maillage
 * @pre Maillage valide
 * @post le maillage n'a plus de trous "simples", ils sont bouchés
 * @test maillage avec sommets non manifold ou avec des trous
 */

void corr_faces_manquantes (maillage m) ;

/**
 * Corrige les faces mal orientées.
 *
 * @param m Maillage
 * @pre Maillage valide
 * @post Maillage corrigé
 * @test maillage avec faces intérieures et extérieures.
 */

void corr_faces_malorientees(maillage m);

/**
 * Retourne le numéro (0, 1, ou 2) du float le plus petit parmis les 3 données en parametres.
 *
 * @param a float
 * @param b float
 * @param c float
 * @pre 3 flottants
 * @post un entier entre 0 et 2
 */

int minimum_3_float(float a, float b, float c);

/**
 * Calcul la distance entre un point et une droite (la droite est définie par 2 points).
 *
 * @param a point
 * @param droite1 point
 * @param droite2 point
 * @pre 3 points bien formés
 * @post la distance entre la point a et le projeté orthogonal de ce point sur la droite
 */

float distance_projete(point a, point droite1, point droite2);

/**
 * Supprime les faces dégénérées(en réorganisant le maillage).
 *
 * @param m Maillage
 * @pre Maillage valide
 * @post Maillage corrigé
 * @test maillage avec faces dégénérées et vérifier que la réorganisation des
 *       faces ne recré pas de faces dégénérées.
 */

void corr_faces_degenerees(maillage m);

/**
 * Applique toutes les corrections au maillage.
 *
 * @param m Maillage
 * @pre Maillage valide
 * @post Maillage corrigé
 * @test maillage comportant toutes les erreurs décrites
 */

void corr_maillage(maillage m);


/**
 * Corrige les faces s'intersectant mutuellement. (Toutes les fonctions qui suivent)
 *
 * @param m Maillage
 * @pre Maillage valide
 * @post Maillage corrigé
 * @test maillage avec faces s'intersectant mutuellement
*/

void corr_faces_intersect(maillage m);

/**
 * Fonction de correction des faces mal orientées 
 * 
 * @param m maillage que l'on désire corriger 
 * @pre Maillage valide
 * @post Maillage avec les faces bien orientées (les isolées) 
 * 
 * @test Maillage avec des faces malorientées
 * @test Maillage avec des faces bien orientées
 * */


void corr_faces_malorientees(maillage m);

/**
 * Fonction de test de l'orientation d'une face par rapport à ses voisines.
 * 
 * @param m Maillage
 * @param i Indice de la face étudiée au sein du maillage m.
 * 
 * @pre : Maillage bien formé et indice correct
 * @post : Est ce qu'une face est mal orientée par rapport à une de ses voisines
 * 
 * @return L'indice au sein du maillage m de la face mal orientée
 * */

int test_orientation ( maillage m , int i );

/**
 * Fonction de retournement d'une face
 * 
 * @param m Maillage
 * @param i Indice de la face que l'on désire retourner.
 * 
 * @pre La i-ème face est bien formée
 * @post La face est retournée
 * 
 * 
 * */

void flip_face ( maillage m , int i );

/*@}*/

#endif
