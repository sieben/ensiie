Pour compiler le programme, un module, ou un programme de tests, merci d'utiliser le Makefile.
Voici les cibles possibles :
	- make ou make all : compile le programme
	- make XXXX.o : compile le module "XXXX"
	- make test_XXXX : compile le fichier de test du module "XXXX"
	- make tests : compile tous les tests
	- make doc : construit la documentation avec Doxygen
	- make clean : nettoie le dossier (apres compilation)
	- make mrproper : nettoie, supprime l'executable et la doc

Les programmes de tests seront dans tests/test_[module]/


