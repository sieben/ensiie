/**
 * @file   vecteur.c
 * @brief  Implantation du module vecteur, contenant des outils vectoriels.
 * @author Rémy Leone & Teddy Michel
 */

#include "vecteur.h"
#include "gestion_memoire.h"

#include <math.h>
#include <stdlib.h>
#include <stdio.h>


#define EPSILON 0.001f

/**
 * @addtogroup vecteur
 */

/*@{*/

/**
 * @struct t_vecteur
 * Structure qui contient la structure de vecteur (même composantes qu'un
 * point).
 * Ses membres sont x, y, et z, trois flottants qui contiendront les
 * composantes du vecteur.
 */

typedef struct t_vecteur
{
    float x, y, z;
} t_vecteur;

/*@}*/


void afficher_vecteur(const vecteur v)
{
    printf("(%.2f, %.2f, %.2f)\n",
        extraire_composante(v, 0),
        extraire_composante(v, 1),
        extraire_composante(v, 2));
}

vecteur vecteur_nouveau ( float x , float y , float z )
{
    // Allocation de la mémoire
    vecteur v = allouer_mem ( sizeof ( t_vecteur ) );

    // Copie des composantes du vecteur
    v->x = x;
    v->y = y;
    v->z = z;

    return v;
}

vecteur vecteur_copie ( const vecteur v )
{
    if ( v == NULL ) return vecteur_nouveau ( 0.0f , 0.0f , 0.0f );
    return vecteur_nouveau ( v->x , v->y , v->z );
}

void vecteur_add ( const vecteur v1 , vecteur v2 )
{
    if ( v1 == NULL || v2 == NULL )
        return;

    v2->x += v1->x;
    v2->y += v1->y;
    v2->z += v1->z;
}

void vecteur_diff ( const vecteur v1 , vecteur v2 )
{
    if ( v1 == NULL || v2 == NULL )
        return;

    v2->x -= v1->x;
    v2->y -= v1->y;
    v2->z -= v1->z;
}

vecteur vecteur_diffN ( const vecteur v1 , const vecteur v2 )
{
    vecteur v = vecteur_copie(v2);
    vecteur_diff(v1, v);
    return v;
}

vecteur vecteur_multN ( const vecteur v1 , const float k )
{
    vecteur v = vecteur_copie(v1);
    vecteur_mult(v, k);
    return v;
}

void vecteur_mult ( vecteur v , float k )
{
    if (est_nul(v))
        return;

    v->x *= k;
    v->y *= k;
    v->z *= k;
}

vecteur vecteur_prod_vect ( const vecteur v1 , const vecteur v2 )
{
    if (est_nul(v1) || est_nul(v2))
        return vecteur_nul;

    // Création d'un nouveau vecteur
    return vecteur_nouveau ( v1->y * v2->z - v1->z * v2->y ,
            v1->z * v2->x - v1->x * v2->z ,
            v1->x * v2->y - v1->y * v2->x );
}

float vecteur_prod_scal ( const vecteur v1 , const vecteur v2 )
{
    if (est_nul(v1) || est_nul(v2))
        return 0.0f;

    // Calcul du produit scalaire
    return ( v1->x * v2->x + v1->y * v2->y + v1->z * v2->z );
}

bool vecteur_egaux ( const vecteur v1 , const vecteur v2 )
{
    if (est_nul(v1))
    {
        if (est_nul(v2))
            return true;
        else
            return false;
    }
    else
    {
        if (est_nul(v1))
            return false;
        else
            return ( fabsf ( v1->x - v2->x ) < EPSILON &&
                     fabsf ( v1->y - v2->y ) < EPSILON &&
                     fabsf ( v1->z - v2->z ) < EPSILON );
    }
}

float vecteur_norme ( const vecteur v )
{
    if (est_nul(v))
        return 0.0f;
    else
        return sqrt ( v->x * v->x + v->y * v->y + v->z * v->z );
}

bool est_nul ( const vecteur v )
{
    if (v == vecteur_nul)
        return true;
    else
        return ( v->x == 0 && v->y == 0 && v->z == 0 );
}

float extraire_composante ( vecteur v , int c )
{
    if (est_nul(v))
        return 0.0f;

    switch ( c )
    {
        default: fprintf ( stderr ,
            "Indice incorrect pour l'accès aux composantes d'un vecteur.\n" );
            return 0.0f; // Valeur incorrecte

        case 0 : return v->x;
        case 1 : return v->y;
        case 2 : return v->z;
    }
};

void vecteur_modifie ( vecteur v , float x , float y , float z )
{
    v->x = x;
    v->y = y;
    v->z = z;

    return;
}

void vecteur_normalise ( vecteur v )
{
    float n = vecteur_norme ( v );

    if ( n > 0.0f )
        vecteur_mult ( v , 1.0f / n );
}

float vecteur_angle ( vecteur v1 , vecteur v2 )
{
    float n1 = vecteur_norme ( v1 );
    float n2 = vecteur_norme ( v2 );

    if ( n1 > 0.0f && n2 > 0.0f )
    {
        float cosinus = vecteur_prod_scal ( v1 , v2 ) / ( n1 * n2 );

        if ( cosinus < -1.0f ) cosinus = -1.0f;
        else if ( cosinus > 1.0f ) cosinus = 1.0f;

        return acos ( cosinus );
    }

    return 0.0f;
}

float vecteur_distance ( const vecteur v1 , const vecteur v2 )
{
    if (est_nul(v1) || est_nul(v2))
        return 0.0f;

    vecteur v = vecteur_copie (v2);
    vecteur_diff(v1, v);
    float norme = vecteur_norme ( v );
    supprimer_vecteur(v);

    return norme;
}

float determinant_2D ( const vecteur v1 , const vecteur v2 )
{
    if (est_nul(v1) || est_nul(v2))
        return 0.0f;

    return ( v1->x * v2->y - v1->y * v2->x );
}

float determinant_3D ( const vecteur v1 , const vecteur v2 , const vecteur v3 )
{
    if (est_nul(v1) || est_nul(v2))
        return 0.0f;

    return ( v1->x * v2->y * v3->z
            + v2->x * v3->y * v1->z
            + v3->x * v1->y * v2->z
            - v1->x * v3->y * v2->z
            - v2->x * v1->y * v3->z
            - v3->x * v2->y * v1->z );
}
