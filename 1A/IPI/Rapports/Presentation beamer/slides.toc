\beamer@endinputifotherversion {3.07pt}
\select@language {french}
\beamer@sectionintoc {1}{Interfaces graphiques}{3}{1}{1}
\beamer@subsectionintoc {1}{1}{Interface en Ncurses}{4}{1}{1}
\beamer@subsectionintoc {1}{2}{Interface type smooth}{4}{1}{1}
\beamer@sectionintoc {2}{Subdivisions}{4}{1}{2}
\beamer@subsectionintoc {2}{1}{Subdivision plane}{5}{1}{2}
\beamer@subsectionintoc {2}{2}{Subdivision normale}{5}{1}{2}
\beamer@sectionintoc {3}{Qualit\IeC {\'e}}{5}{1}{3}
\beamer@sectionintoc {4}{Filtrage}{6}{1}{4}
\beamer@subsectionintoc {4}{1}{Filtrage uniforme}{7}{1}{4}
\beamer@subsectionintoc {4}{2}{Filtrage normal}{7}{1}{4}
\beamer@sectionintoc {5}{Correction des erreurs}{7}{1}{5}
