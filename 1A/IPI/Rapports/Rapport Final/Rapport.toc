\select@language {french}
\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Objectifs}{2}{subsection.1.1}
\contentsline {section}{\numberline {2}Planning}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Phase 1 : rendu le 5 mars 2010}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Phase 2 : rendu le 19 mars 2010}{2}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Phase 3 : rendu le 2 avril 2010}{3}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Phase 4 : rendu le 2 juin 2010}{3}{subsection.2.4}
\contentsline {section}{\numberline {3}Outils}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}Google Groupe}{3}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Environnement de d\IeC {\'e}veloppement}{4}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Subversion}{4}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Makefile}{5}{subsection.3.4}
\contentsline {section}{\numberline {4}Repartition des taches}{5}{section.4}
\contentsline {section}{\numberline {5}R\IeC {\'e}solution des probl\IeC {\`e}mes rencontr\IeC {\'e}s}{5}{section.5}
\contentsline {subsection}{\numberline {5.1}Richesses des objets rencontr\IeC {\'e}s}{5}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}R\IeC {\'e}utilisation d'un code source disponible}{5}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Adaptation du logiciel smooth}{6}{subsection.5.3}
