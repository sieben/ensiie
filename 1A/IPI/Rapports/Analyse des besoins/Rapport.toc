\select@language {french}
\contentsline {section}{\numberline {1}D\IeC {\'e}finitions}{2}{section.1}
\contentsline {section}{\numberline {2}Probl\IeC {\`e}mes rencontr\IeC {\'e}s}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Erreurs sur les sommets}{3}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Sommets non r\IeC {\'e}f\IeC {\'e}renc\IeC {\'e}s}{3}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Sommets dupliqu\IeC {\'e}s}{3}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}Sommets non manifolds}{4}{subsubsection.2.1.3}
\contentsline {subsection}{\numberline {2.2}Erreurs sur les faces}{4}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Faces invalides}{4}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Faces dupliqu\IeC {\'e}es}{4}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}Faces mal orient\IeC {\'e}es}{5}{subsubsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.4}Faces s'intersectant mutuellement}{5}{subsubsection.2.2.4}
\contentsline {subsubsection}{\numberline {2.2.5}Faces d'aire nulle ou d\IeC {\'e}g\IeC {\'e}n\IeC {\'e}r\IeC {\'e}es}{5}{subsubsection.2.2.5}
\contentsline {subsubsection}{\numberline {2.2.6}Faces manquantes}{6}{subsubsection.2.2.6}
\contentsline {subsection}{\numberline {2.3}Erreurs sur les normales}{6}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Normales non index\IeC {\'e}es}{6}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Normales incoh\IeC {\'e}rentes}{6}{subsubsection.2.3.2}
\contentsline {subsection}{\numberline {2.4}Erreurs sur les ar\IeC {\^e}tes}{7}{subsection.2.4}
\contentsline {subsubsection}{\numberline {2.4.1}Ar\IeC {\^e}tes non index\IeC {\'e}es}{7}{subsubsection.2.4.1}
\contentsline {subsubsection}{\numberline {2.4.2}Ar\IeC {\^e}tes non manifolds}{7}{subsubsection.2.4.2}
\contentsline {subsubsection}{\numberline {2.4.3}Ar\IeC {\^e}tes trop aigu\IeC {\"e}s}{7}{subsubsection.2.4.3}
\contentsline {section}{\numberline {3}Am\IeC {\'e}lioration de la qualit\IeC {\'e} des maillages }{7}{section.3}
\contentsline {subsection}{\numberline {3.1}Subdivision des maillages}{7}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Premi\IeC {\`e}re m\IeC {\'e}thode - Subdivision d'un triangle}{8}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Seconde m\IeC {\'e}thode - Subdivision avec utilisation des normales}{8}{subsubsection.3.1.2}
\contentsline {subsection}{\numberline {3.2}Filtrage}{12}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Qualit\IeC {\'e} d'un maillage}{12}{subsection.3.3}
\contentsline {section}{\numberline {4}Gestion des surfaces maill\IeC {\'e}es}{13}{section.4}
\contentsline {subsection}{\numberline {4.1}Gestion des *.obj}{13}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Gestion des vecteurs}{14}{subsection.4.2}
\contentsline {section}{\numberline {5}Conclusion}{14}{section.5}
