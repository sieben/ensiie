<?php

require_once ( 'include/main.php' );

// On n'a pas précisé l'identifiant de l'enseignant
if ( !isset ( $_GET['ens'] ) || !is_numeric ( $_GET['ens'] ) )
{
    header ( 'Location: liste_ens.php' );
    die();
}

// Seuls les administrateurs et l'enseignant concerné peuvent voir cette page
if ( ( !is_ens() && !is_admin() ) || ( !is_admin() && is_ens() && $_SESSION['uid'] != $_GET['ens'] ) )
{
    header ( 'Location: login.php' );
    die();
}


$titre = 'Informations sur un enseignant';

include_once ( 'include/header.php' );

$sql = 'SELECT nomens, prenomsens, login, courriel, 
               nbhrreelles, autresheures, reliquat, salariepublic
        FROM enseignant
        WHERE idens = ' . db_protect ( $_GET['ens'] ) . ';';

$req = db_query ( $db_link , $sql );

if ( pg_num_rows ( $req ) > 0 )
{
    echo '<h4>Informations générales : </h4>';

    $row = pg_fetch_assoc ( $req );

    echo "<ul>\n";
    echo '  <li><strong>Nom</strong>&nbsp: ' . $row['nomens'] . "</li>\n";
    echo '  <li><strong>Prénom</strong>&nbsp: ' . $row['prenomsens'] . "</li>\n";
    echo '  <li><strong>Login</strong>&nbsp: ' . $row['login'] . "</li>\n";
    echo '  <li><strong>Courriel</strong>&nbsp: <a href="mailto:' . $row['courriel'] . '">' . $row['courriel'] . "</a></li>\n";
    echo '  <li><strong>Nombre d\'heures réelles</strong>&nbsp: ' . $row['nbhrreelles'] . ' heure';
    if ( $row['nbhrreelles'] > 1 ) echo 's';
    echo "</li>\n";
    echo '  <li><strong>Autres heures</strong>&nbsp: ' . $row['autresheures'] . ' heure';
    if ( $row['autresheures'] > 1 ) echo 's';
    echo "</li>\n";
    echo '  <li><strong>Reliquat</strong>&nbsp: ' . $row['reliquat'] . ' heure';
    if ( $row['reliquat'] > 1 ) echo 's';
    echo "</li>\n";

    echo '  <li><strong>Salarié public</strong>&nbsp;: ';
    echo ( $row['salariepublic'] == 0 ? 'Non' : 'Oui' );
    echo "</li>\n";

    echo "</ul>\n";

    // Charges
    $charge = 'SELECT nomcharge, nbhrcharge
               FROM charge
               WHERE idens = ' . db_protect ( $_GET['ens'] ) . ';';

    $req = db_query ( $db_link , $charge );

    if ( pg_num_rows ( $req ) > 0 )
    {
        echo '<h4>Charges de l\'enseignant&nbsp;:' . "</h4>\n\n<ul>\n";

        while ( $row = pg_fetch_assoc ( $req ) )
        {
            echo '  <li><strong>' . $row['nomcharge'] . "</strong></li>\n";
        }

        echo "</ul>\n\n";
    }

    // Cours magistraux
    $sql = 'SELECT idensmt, intitule
            FROM choixcoursmagistraux
            NATURAL JOIN enseignement
            WHERE idens = ' . db_protect ( $_GET['ens'] ) . ';';

    $req = db_query ( $db_link , $sql );

    if ( pg_num_rows ( $req ) > 0 )
    {
        echo '<h4>Cours magistraux&nbsp;:' . "</h4>\n\n<ul>\n";

        while ( $row = pg_fetch_assoc ( $req ) )
        {
            echo '  <li><a href="result.php?ensmt=' . $row['idensmt'] . '">' . $row['intitule'] . "</a></li>\n";
        }

        echo "</ul>\n\n";
    }

    // TP
    $sql = 'SELECT idensmt, intitule
            FROM choixtp
            NATURAL JOIN enseignement
            WHERE idens = ' . db_protect ( $_GET['ens'] ) . ';';

    $req = db_query ( $db_link , $sql );

    if ( pg_num_rows ( $req ) > 0 )
    {
        echo '<h4>TP&nbsp;:' . "</h4>\n\n<ul>\n";

        while ( $row = pg_fetch_assoc ( $req ) )
        {
            echo '  <li><a href="result.php?ensmt=' . $row['idensmt'] . '">' . $row['intitule'] . "</a></li>\n";
        }

        echo "</ul>\n\n";
    }

    // TD
    $sql = 'SELECT idensmt, intitule
            FROM choixtd
            NATURAL JOIN enseignement
            WHERE idens = ' . db_protect ( $_GET['ens'] ) . ';';

    $req = db_query ( $db_link , $sql );

    if ( pg_num_rows ( $req ) > 0 )
    {
        echo '<h4>TD&nbsp;:' . "</h4>\n\n<ul>\n";

        while ( $row = pg_fetch_assoc ( $req ) )
        {
            echo '  <li><a href="result.php?ensmt=' . $row['idensmt'] . '">' . $row['intitule'] . "</a></li>\n";
        }

        echo "</ul>\n\n";
    }

    echo '<p><a href="result.php?ens=' . $_GET['ens'] . '">Résultats</a> - <a href="liste_ens.php">Retour à la liste des enseignants</a></p>';
}
else
{
    header ( 'Location: liste_ens.php' );
    die();
}

include_once ( 'include/footer.php' );

?>
