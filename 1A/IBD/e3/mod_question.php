<?php

require_once ( 'include/main.php' );

// L'utilisateur n'est pas connecté ou n'est pas administrateur
if ( !is_admin() )
{
    header ( 'Location: login.php' );
    die();
}

// On n'a pas précisé l'identifiant de la question
if ( !isset ( $_GET['id'] ) || !is_numeric ( $_GET['id'] ) )
{
    header ( 'Location: questionnaire.php' );
    die();
}

// On cherche la question dans la base
$sql = 'SELECT intitule, typequestion, orientation
        FROM questions
        WHERE idquestion = ' . db_protect ( $_GET['id'] ) . ';';

$req = db_query ( $db_link , $sql );

// La question n'existe pas
if ( pg_num_rows ( $req ) === 0 )
{
    header ( 'Location: questionnaire.php' );
    die();
}

$row = pg_fetch_assoc ( $req );


$files_css[] = 'form.css';

$titre = 'Modifier une question';

include_once ( 'include/header.php' );


// Traitement du formulaire
if ( isset ( $_POST['submit'] ) )
{
    if ( !isset ( $_POST['intitule'] ) || empty ( $_POST['intitule'] ) )
    {
        echo '<p class="erreur">Vous devez donner un intitulé à chaque question.</p>';
    }
    else
    {
        if ( $_POST['type'] == 'note' ||
             $_POST['type'] == 'texte' ||
             $_POST['type'] == 'nombre' ||
             $_POST['type'] == 'charge_tp' ||
             $_POST['type'] == 'charge_td' ||
             $_POST['type'] == 'filiere' )
        {
            $type_question = $_POST['type'];
        }
        else
        {
            $type_question = 'texte'; // Valeur par défaut
        }

        if ( isset ( $_POST['orientation'] ) && (
             $_POST['orientation'] == 0 || $_POST['orientation'] == 1 ) )
        {
            $orientation = $_POST['orientation'];
        }
        else $orientation = 0;

        $sql = "UPDATE questions
                SET intitule = '" . db_protect ( $_POST['intitule'] ) . "',
                    typequestion = '" . db_protect ( $type_question ) . "',
                    orientation = " . $orientation . "
                WHERE idquestion = " . db_protect ( $_GET['id'] ) . ';';

        $req = db_query ( $db_link , $sql );

        header ( 'Location: questionnaire.php' );
        die();
    }
}


// Affichage du formulaire
echo '<form action="mod_question.php?id=' . $_GET['id'] . '" method="post">';
echo "\n<fieldset>\n<legend>Modifier une question</legend>\n";

echo '<p class="form_line"><label for="form_row_intitule" class="form_label">Intitulé&nbsp;:</label> ';
echo '<input type="text" name="intitule" id="form_row_intitule" size="40" maxlength="150" value="';
echo ( isset ( $_POST['intitule'] ) ) ? $_POST['intitule'] : $row['intitule'];
echo '" />' . "</p>\n";

echo '<p class="form_line"><label for="form_row_orientation" class="form_label">Orientation&nbsp;:</label> ';
echo '<input type="text" name="orientation" id="form_row_orientation" size="10" maxlength="1" value="';
echo ( isset ( $_POST['orientation'] ) ) ? $_POST['orientation'] : $row['orientation'];
echo '" />' . "</p>\n";

echo '<p class="form_line"><label for="form_row_type" class="form_label">Type de question&nbsp;:</label> ';
echo '<select id="form_row_type" name="type">' . "\n";

// Type : texte
echo '  <option value="texte"';

if ( ( isset ( $_POST['type'] ) && $_POST['type'] == 'texte' ) ||
     $row['typequestion'] == 'texte' ) echo ' selected="selected"';

echo ">Texte</option>\n";

// Type : note
echo '  <option value="note"';

if ( ( isset ( $_POST['type'] ) && $_POST['type'] == 'note' ) ||
     $row['typequestion'] == 'note' ) echo ' selected="selected"';

echo ">Note</option>\n";

// Type : nombre
echo '  <option value="nombre"';

if ( ( isset ( $_POST['type'] ) && $_POST['type'] == 'nombre' ) ||
     $row['typequestion'] == 'nombre' ) echo ' selected="selected"';

echo ">Nombre</option>\n";

// Type : charge_tp
echo '  <option value="charge_tp"';

if ( ( isset ( $_POST['type'] ) && $_POST['type'] == 'charge_tp' ) ||
     $row['typequestion'] == 'charge_tp' ) echo ' selected="selected"';

echo ">Chargé de TP</option>\n";

// Type : charge_td
echo '  <option value="charge_td"';

if ( ( isset ( $_POST['type'] ) && $_POST['type'] == 'charge_td' ) ||
     $row['typequestion'] == 'charge_td' ) echo ' selected="selected"';

echo ">Chargé de TD</option>\n";

// Type : filiere
echo '  <option value="filiere"';

if ( ( isset ( $_POST['type'] ) && $_POST['type'] == 'filiere' ) ||
     $row['typequestion'] == 'filiere' ) echo ' selected="selected"';

echo ">Filière d'origine</option>\n";

echo "</select></p>\n";

echo "</fieldset>\n";
echo '<p class="form_submit"><input type="submit" name="submit" value="Valider" /><input type="button" class="form_back" value="Annuler" /></p>';
echo "</form>\n";

include_once ( 'include/footer.php' );

?>