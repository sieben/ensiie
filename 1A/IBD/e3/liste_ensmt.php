<?php

require_once ( 'include/main.php' );

// Seul l'administrateur peut voir cette page
if ( !is_admin() )
{
    header ( 'Location: index.php' );
    die();
}

$titre = 'Liste des enseignements';

include_once ( 'include/header.php' );


// Liste des enseignements
$sql = 'SELECT idensmt, intitule, code
        FROM enseignement
        ORDER BY code;';

$req = db_query ( $db_link , $sql );

if ( pg_num_rows ( $req ) > 0 )
{
    echo "<ul>\n";

    // Affichage de la liste des enseignements
    while ( $row = pg_fetch_assoc ( $req ) )
    {
        echo '  <li><a href="result.php?ensmt=' . $row['idensmt'] . '">';
        echo $row['intitule'] . ' (' . $row['code'] . ")</a></li>\n";
    }

    echo "</ul>\n";
}
else
{
    echo "<p>Il n'y a aucun enseignement pour le moment.</p>\n";
}

include_once ( 'include/footer.php' );

?>