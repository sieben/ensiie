<?php

require_once ( 'include/main.php' );

$titre = 'Questionnaire';

include_once ( 'include/header.php' );


// Recherche des questions et des sections
$sql = 'SELECT idquestion,
               intitule,
               ordrequestion,

               sections.idsection AS idsection,
               /* idsection */

               nom
        FROM sections

        LEFT OUTER JOIN questions
        ON questions.idsection = sections.idsection
        /* NATURAL JOIN sections */

        ORDER BY idsection, ordrequestion;';

$req = db_query ( $db_link , $sql );


if ( is_admin() )
{
    echo '<p><a href="new_section.php">Nouvelle section</a></p>' . "\n";
}

if ( pg_num_rows ( $req ) > 0 )
{
    $id_section = 0;

    // Affichage du questionnaire
    while ( $row = pg_fetch_assoc ( $req ) )
    {
        // Affichage d'une nouvelle section
        if ( $id_section != $row['idsection'] )
        {
            $id_section = $row['idsection'];

            echo '<h4>' . $row['nom'] . "</h4>\n";

            // Modification de la section
            if ( is_admin() )
            {
                echo '<p class="actions">';
                echo '<a href="mod_section.php?id=' . $id_section . '">Éditer</a>, ';
                echo '<a href="sup_section.php?id=' . $id_section . '">Supprimer</a>, ';

                if ( $row['idsection'] > 1 )
                {
                    echo '<a href="move_section.php?id=' . $id_section . '&amp;up">Monter</a>, ';
                }

                echo '<a href="move_section.php?id=' . $id_section . '&amp;down">Descendre</a>, ';
                echo '<a href="new_question.php?section=' . $id_section . '">Nouvelle question</a>';
                echo "</p>\n";
            }
        }

        if ( $row['idquestion'] != NULL )
        {
            // Affichage de la question
            echo '  <li>' . $row['intitule'];

            // Modification de la question
            if ( is_admin() )
            {
                echo ' <span class="actions">(';
                echo '<a href="mod_question.php?id=' . $row['idquestion'] . '">Éditer</a>, ';
                echo '<a href="sup_question.php?id=' . $row['idquestion'] . '">Supprimer</a>, ';

                if ( $row['ordrequestion'] > 1 )
                {
                    echo '<a href="move_question.php?id=' . $row['idquestion'] . '&amp;up">Monter</a>, ';
                }

                echo '<a href="move_question.php?id=' . $row['idquestion'] . '&amp;down">Descendre</a>';
                echo ')</span>';
            }

            echo "</li>\n";
        }
    }
}
// Aucune question ni section à afficher
else
{
    echo "<p>Il n'y a aucune question pour le moment.</p>\n";
}


include_once ( 'include/footer.php' );

?>
