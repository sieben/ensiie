<?php

require_once ( 'include/main.php' );

$titre = 'Accueil';

include_once ( 'include/header.php' );

echo "<p>Ce site est consacré à l'évaluation des enseignements que suivent les élèves de l'ENSIIE.</p>";

// Si on est connecté
if ( is_connected() )
{
    // Élève
    if ( is_eleve() )
    {
        echo '<p><a href="questionnaires_eleve.php">Liste des enseignements à évaluer</a></p>';
    }
    // Enseignant
    else
    {
        echo '<p><a href="questionnaires_ens.php">Liste des enseignements</a></p>';
    }
}

include_once ( 'include/footer.php' );

?>