<?php

require_once ( 'include/main.php' );

// Si l'utilisateur n'est pas connecté, ou que ce n'est pas un élève, on le redirige vers la page de connexion
if ( !is_eleve() )
{
    header ( 'Location: login.php' );
    die();
}


$titre = 'Liste des enseignements à évaluer';

include_once ( 'include/header.php' );


// Enseignements correspondant à la promo de l'élève
$sql = "SELECT idensmt, intitule, idUE
        FROM enseignement
        NATURAL JOIN OptionOuTc
        WHERE promo = '" . db_protect ( $_SESSION['promo'] ) . "';";

$req = db_query ( $db_link , $sql );


if ( pg_num_rows ( $req ) > 0 )
{
    // Liste des enseignements visibles
    $enseignements = array();

    while ( $row = pg_fetch_assoc ( $req ) )
    {
        $enseignements[$row['idensmt']] = array (
            'ue'       => $row['idUE'],
            'intitule' => $row['intitule'],
            'evalue'   => false );
    }

    // Conditionnelle ?
    $sql = "SELECT idUE1Manquee, idUE2Manquee
            FROM conditionnelle
            WHERE idEleve = '" . db_protect ( $_SESSION['uid'] ) . "';";

    $req2 = db_query ( $db_link , $sql );

    if ( pg_num_rows ( $req2 ) > 0 )
    {
        $row2 = pg_fetch_assoc ( $req2 );

        $ue1 = 0;
        $ue2 = 0;

        if ( !empty ( $row['idUE1Manquee'] ) )
        {
            $ue1 = $row['idUE1Manquee'];
        }

        if ( !empty ( $row['idUE2Manquee'] ) )
        {
            $ue2 = $row['idUE1Manquee'];
        }

        foreach ( $enseignements as $idensmt => $infos )
        {
            if ( $infos['ue'] == $ue1 || $infos['ue'] == $ue2 )
            {
                continue;
            }

            unset ( $enseignements[$idensmt] );
        }
    }
    else
    {
        // Redoublement ?
        $sql = "SELECT idUEReussie
                FROM redoublement
                WHERE idEleve = '" . db_protect ( $_SESSION['uid'] ) . "';";

        $req2 = db_query ( $db_link , $sql );

        if ( pg_num_rows ( $req2 ) > 0 )
        {
            while ( $row2 = pg_fetch_assoc ( $req2 ) )
            {
                unset ( $enseignements[$row2['idUEReussie']] );
            }
        }
    }


    // On cherche si l'élève a déjà évalué chaque enseignement
    $sql = 'SELECT idEnsmt
            FROM eleveensmt
            WHERE idEleve = ' . db_protect ( $_SESSION['uid'] ) . ';';

    $req2 = db_query ( $db_link , $sql );

    while ( $row2 = pg_fetch_assoc ( $req2 ) )
    {
        $enseignements[$row['idEnsmt']]['evalue'] = true;
    }


    // Affichage de la liste
    echo "<ul>\n";

    // Pour chaque enseignement
    foreach ( $enseignements as $idensmt => $infos )
    {
        echo '  <li>';

        if ( $infos['evalue'] )
        {
            echo $infos['intitule'] .' (évalué)';
        }
        else
        {
            echo '<a href="eleve_questionnaire.php?ensmt=' . $idensmt . '">' . $infos['intitule'] . '</a>';
        }

        echo "</li>\n";
    }

    echo "</ul>\n";
}
// Aucun enseignement
else
{
    echo "<p>Vous ne pouvez évaluer aucun enseignement.</p>\n";
}

include_once ( 'include/footer.php' );

?>