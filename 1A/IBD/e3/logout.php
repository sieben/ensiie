<?php

require_once ( 'include/main.php' );

// Suppression de toutes les variables de session
session_unset();

$_SESSION['infos'] = 'Vous n\'etes plus connecté.';

header ( 'Location: index.php' );

?>