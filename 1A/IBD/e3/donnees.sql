--
-- PostgreSQL database dump
--

-- Started on 2010-05-03 19:55:35 CEST

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- TOC entry 1918 (class 0 OID 0)
-- Dependencies: 1544
-- Name: charge_idcharge_seq; Type: SEQUENCE SET; Schema: public; Owner: sieben
--

SELECT pg_catalog.setval('charge_idcharge_seq', 30, false);


--
-- TOC entry 1919 (class 0 OID 0)
-- Dependencies: 1550
-- Name: eleve_ideleve_seq; Type: SEQUENCE SET; Schema: public; Owner: sieben
--

SELECT pg_catalog.setval('eleve_ideleve_seq', 1565, true);


--
-- TOC entry 1920 (class 0 OID 0)
-- Dependencies: 1553
-- Name: enseignant_idens_seq; Type: SEQUENCE SET; Schema: public; Owner: sieben
--

SELECT pg_catalog.setval('enseignant_idens_seq', 15, true);


--
-- TOC entry 1921 (class 0 OID 0)
-- Dependencies: 1555
-- Name: enseignement_idensmt_seq; Type: SEQUENCE SET; Schema: public; Owner: sieben
--

SELECT pg_catalog.setval('enseignement_idensmt_seq', 21, true);


--
-- TOC entry 1922 (class 0 OID 0)
-- Dependencies: 1557
-- Name: formation_idformation_seq; Type: SEQUENCE SET; Schema: public; Owner: sieben
--

SELECT pg_catalog.setval('formation_idformation_seq', 4, false);


--
-- TOC entry 1923 (class 0 OID 0)
-- Dependencies: 1559
-- Name: optionoutc_idoption_seq; Type: SEQUENCE SET; Schema: public; Owner: sieben
--

SELECT pg_catalog.setval('optionoutc_idoption_seq', 11, false);


--
-- TOC entry 1924 (class 0 OID 0)
-- Dependencies: 1562
-- Name: questions_idquestion_seq; Type: SEQUENCE SET; Schema: public; Owner: sieben
--

SELECT pg_catalog.setval('questions_idquestion_seq', 31, false);


--
-- TOC entry 1925 (class 0 OID 0)
-- Dependencies: 1566
-- Name: sections_idsection_seq; Type: SEQUENCE SET; Schema: public; Owner: sieben
--

SELECT pg_catalog.setval('sections_idsection_seq', 3, false);


--
-- TOC entry 1926 (class 0 OID 0)
-- Dependencies: 1568
-- Name: ue_idue_seq; Type: SEQUENCE SET; Schema: public; Owner: sieben
--

SELECT pg_catalog.setval('ue_idue_seq', 14, false);


--
-- TOC entry 1898 (class 0 OID 16387)
-- Dependencies: 1543
-- Data for Name: charge; Type: TABLE DATA; Schema: public; Owner: sieben
--

INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (10, 'Responsable IA et Informatique fondamentale', 0, 9999);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (11, 'Responsable Maths (2A)', 0, 9999);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (12, 'Responsable Organisation (2A)', 0, 9999);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (14, 'Responsable du tronc commun (3A)', 0, 9999);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (13, 'Responsable Formation humaine (2A)', 0, 9999);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (15, 'Responsable MLO', 0, 2);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (16, 'Responsable MOM', 0, 4);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (17, 'Responsable ISI', 0, 3);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (19, 'Responsable ECO', 0, 9999);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (18, 'Responsable IPI', 0, 13);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (20, 'Responsable IBD', 0, 15);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (21, 'Responsable MAN', 0, 10);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (22, 'Responsable MST', 0, 8);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (23, 'Responsable MPR', 0, 9);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (24, 'Responsable ECG', 0, 5);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (25, 'Responsable ECA', 0, 5);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (26, 'Responsable EGI', 0, 9999);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (27, 'Responsable IAP1', 0, 2);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (28, 'Responsable IAP2', 0, 12);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (29, 'Responsable IAP 3', 0, 11);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (30, 'Responsable CAN', 0, 14);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (0, 'Directeur des etudes', 0, 3);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (1, 'Responsable Math 1 (1A)', 0, 2);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (2, 'Responsable Math 2 (1A)', 0, 8);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (3, 'Responsable Algorithmique - Programmation (1A)', 0, 2);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (5, 'Responsable Application (1A)', 0, 13);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (4, 'Responsable Matériel & Systèmes (1A)', 0, 3);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (6, 'Responsable Formation humaine (1A)', 0, 14);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (7, 'Responsable Économie-Gestion (1A)', 0, 5);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (8, 'Responsable Génie logiciel (2A)', 0, 9999);
INSERT INTO charge (idcharge, nomcharge, nbhrcharge, idens) VALUES (9, 'Responsable Systèmes & Réseaux (2A)', 0, 9999);


--
-- TOC entry 1899 (class 0 OID 16395)
-- Dependencies: 1545
-- Data for Name: choixcoursmagistraux; Type: TABLE DATA; Schema: public; Owner: sieben
--

INSERT INTO choixcoursmagistraux (idensmt, idens, datechoix, nbhrcm) VALUES (5, 4, '2000-01-01', 0);
INSERT INTO choixcoursmagistraux (idensmt, idens, datechoix, nbhrcm) VALUES (6, 9, '2000-01-01', 0);
INSERT INTO choixcoursmagistraux (idensmt, idens, datechoix, nbhrcm) VALUES (3, 2, '2000-01-01', 0);
INSERT INTO choixcoursmagistraux (idensmt, idens, datechoix, nbhrcm) VALUES (7, 8, '2000-01-01', 0);
INSERT INTO choixcoursmagistraux (idensmt, idens, datechoix, nbhrcm) VALUES (8, 10, '2000-01-01', 0);
INSERT INTO choixcoursmagistraux (idensmt, idens, datechoix, nbhrcm) VALUES (9, 7, '2000-01-01', 0);
INSERT INTO choixcoursmagistraux (idensmt, idens, datechoix, nbhrcm) VALUES (10, 3, '2000-01-01', 0);
INSERT INTO choixcoursmagistraux (idensmt, idens, datechoix, nbhrcm) VALUES (11, 2, '2000-01-01', 0);
INSERT INTO choixcoursmagistraux (idensmt, idens, datechoix, nbhrcm) VALUES (12, 12, '2000-01-01', 0);
INSERT INTO choixcoursmagistraux (idensmt, idens, datechoix, nbhrcm) VALUES (13, 11, '2000-01-01', 0);
INSERT INTO choixcoursmagistraux (idensmt, idens, datechoix, nbhrcm) VALUES (14, 15, '2000-01-01', 0);
INSERT INTO choixcoursmagistraux (idensmt, idens, datechoix, nbhrcm) VALUES (15, 13, '2000-01-01', 0);
INSERT INTO choixcoursmagistraux (idensmt, idens, datechoix, nbhrcm) VALUES (16, 9999, '2000-01-01', 0);
INSERT INTO choixcoursmagistraux (idensmt, idens, datechoix, nbhrcm) VALUES (4, 9999, '2000-01-01', 0);
INSERT INTO choixcoursmagistraux (idensmt, idens, datechoix, nbhrcm) VALUES (17, 5, '2000-01-01', 0);
INSERT INTO choixcoursmagistraux (idensmt, idens, datechoix, nbhrcm) VALUES (18, 5, '2000-01-01', 0);
INSERT INTO choixcoursmagistraux (idensmt, idens, datechoix, nbhrcm) VALUES (19, 5, '2000-01-01', 0);
INSERT INTO choixcoursmagistraux (idensmt, idens, datechoix, nbhrcm) VALUES (20, 14, '2000-01-01', 0);


--
-- TOC entry 1900 (class 0 OID 16398)
-- Dependencies: 1546
-- Data for Name: choixtd; Type: TABLE DATA; Schema: public; Owner: sieben
--



--
-- TOC entry 1901 (class 0 OID 16401)
-- Dependencies: 1547
-- Data for Name: choixtp; Type: TABLE DATA; Schema: public; Owner: sieben
--



--
-- TOC entry 1902 (class 0 OID 16404)
-- Dependencies: 1548
-- Data for Name: conditionnelle; Type: TABLE DATA; Schema: public; Owner: sieben
--



--
-- TOC entry 1915 (class 0 OID 16533)
-- Dependencies: 1569
-- Data for Name: droits; Type: TABLE DATA; Schema: public; Owner: sieben
--

INSERT INTO droits (idens, idensmt) VALUES (1, 10);


--
-- TOC entry 1903 (class 0 OID 16407)
-- Dependencies: 1549
-- Data for Name: eleve; Type: TABLE DATA; Schema: public; Owner: sieben
--

INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1185, 'adnane.baddouh', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1186, 'adrian.vandier-ast', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1187, 'ahmed.bakhtaoui', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1188, 'alexandre.michelis', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1189, 'alexandre.montecucco', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1190, 'alexandre.pastorino', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1191, 'alioune.sow', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1192, 'anass.chakiralaoui', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1193, 'andreolivier.atebamandeng', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1194, 'anthony.dorme', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1195, 'anthony.luneau', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1196, 'antoine.menciere', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1197, 'antony.turpin', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1198, 'ariane.lemaire', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1199, 'artem.timoshkin', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1200, 'aude.giraud', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1201, 'aurelien.marteau', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1202, 'benjamin.herlin', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1203, 'benoit.bataille', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1204, 'benoit.waag', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1205, 'boris.maguet', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1206, 'bruno.maury', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1207, 'chaoran.zhong', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1208, 'charles.vatin', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1209, 'charly.florent', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1210, 'christophe.mongin', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1211, 'claire.schaefer', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1212, 'clement.amiot', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1213, 'clement.olivares', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1214, 'clement.prevost', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1215, 'corentin.deluce', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1216, 'cyril.jouve', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1217, 'david.aurat', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1218, 'edouard.pichonnier', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1219, 'elodie.fourny', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1220, 'emeric.roverch', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1221, 'fabien.fargere', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1222, 'florian.laroumagne', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1223, 'franck.garnier', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1224, 'francois.cheynier', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1225, 'frederic.guerard', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1226, 'frederic.maquin', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1227, 'gen.yang', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1228, 'gregoire.cotte', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1229, 'gregory.szwarc', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1230, 'guillaume.hauke', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1231, 'guillaume.sabot', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1232, 'haitem.korfed', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1233, 'hajar.tahri', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1234, 'hermann.olivi', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1235, 'herve.tamtotiam', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1236, 'hoangmichael.nguyen', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1237, 'imad.kourde', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1238, 'ines.chafei', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1239, 'ionut.iortoman', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1240, 'isabelle.tran-khoi', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1241, 'jean-francois.riquart', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1242, 'jerome.desboeufs', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1243, 'jocelyn.bretey', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1244, 'jonathan.bronner', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1245, 'joris.abalea', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1246, 'julien.dauphant', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1247, 'julien.michelet', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1248, 'justine.delovinfosse', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1249, 'kevin.landa', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1250, 'loic.arnoult', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1251, 'louis.renault', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1252, 'louis.tellier', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1253, 'marco.caradonna', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1254, 'marc.vanderwal', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1255, 'marien.ritzenthaler', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1256, 'maxime.ewoane', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1257, 'maxime.khoy', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1258, 'maxime.lenoir', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1259, 'melanie.dumon', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1260, 'mickael.coiffec-pennec', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1261, 'mohamedfadhel.azouzi', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1262, 'moussamamadou.diallo', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1263, 'nathalie.ngor', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1264, 'nicolas.braquart', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1265, 'nicolas.depierreux', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1266, 'nicolas.gasull', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1267, 'nicolas.godinho', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1268, 'nicolas.weber', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1269, 'pierre.boutry', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1270, 'pierre.gargam', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1271, 'pierre.mauvy', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1272, 'quang-van.dam', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1273, 'quentin.garcia', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1274, 'quentin.jeauffroy', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1275, 'radoine.abdelkaoui', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1276, 'remi.czternasty', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1277, 'remi.gerboud', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1278, 'remi.takase', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1279, 'robin.gosswiller', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1280, 'romain.allain', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1281, 'romain.francez', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1282, 'sara.iraqi', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1283, 'sebastien.brugulat', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1284, 'shunjia.chen', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1285, 'simon.becuwe', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1286, 'simon.lasnier', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1287, 'sothkhemarith.thou', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1288, 'stanislas.minaev', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1289, 'stanislas.morbieu', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1290, 'sybil.deboin', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1291, 'thibaud.dubuisson', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1292, 'thibaud.lagelouze', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1293, 'thibaut.lefebvre', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1294, 'thomas.comes', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1295, 'thomas.pitiot', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1296, 'tuan-anhpascal.nguyen', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1297, 'valentin.shamsnejad', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1298, 'valerian.broussard', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1299, 'vincent.grieu', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1300, 'xavier.raux', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1301, 'yacine.kerkouche', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1302, 'yann.arbaud', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1303, 'yasmine.benkiran', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1304, 'yasmine.harbit', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1305, 'yasmine.joundy', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1306, 'yassine.loudad', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1307, 'yves.lelievre', 'mdp', 2012, 1, 1, 1, 1, 2009);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1362, 'adrien.domurado', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1363, 'adrien.dong', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1364, 'agathe.coblence', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1365, 'alexandre.baroin', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1366, 'alexandre.chouraqui', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1367, 'alexandre.krol', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1368, 'alexandre.monzie', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1369, 'alexandre.notebaert', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1370, 'alice.gio', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1371, 'alice.mandin', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1372, 'anais.chesneau', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1373, 'anas.mgassy', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1374, 'annaelle.marc', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1375, 'antoine.artaud-macari', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1376, 'audric.castillan', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1377, 'bastien.caudan', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1378, 'bastien.chou', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1379, 'bastien.ricono', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1380, 'catherine.in', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1381, 'cedric.bouhier', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1382, 'charles-edouard.poisnel', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1383, 'charles.santi', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1384, 'christian.vieira', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1385, 'christophe.convert', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1386, 'codou.ndiaye', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1387, 'cyril.muller', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1388, 'damien.fouquet', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1389, 'damien.ricaud', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1390, 'delphine.leduc', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1391, 'dmitri.voitsekhovitch', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1392, 'dominique.padiou', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1393, 'dorine.hipp', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1394, 'elmehdi.guennoun', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1395, 'elsa.claudet', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1396, 'emmanuel.badet', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1397, 'evan.liomain', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1398, 'fabien.millou', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1399, 'fabrice.flavigne', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1400, 'fares.souissi', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1401, 'fatimazahra.mezouar', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1402, 'fatoumata.ngom', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1403, 'francois.merot', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1404, 'frederic.isacesco', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1405, 'frederic.wang', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1406, 'guido.manfredi', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1407, 'guillaume.delugre', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1408, 'halima.louati', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1409, 'hanaa.didi', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1410, 'heythem.gzara', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1411, 'hugues.cauvin', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1412, 'imene.djedeat', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1413, 'jean-baptiste.dijols', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1414, 'jeanchristophe.gatuingt', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1415, 'jean-damien.hatzenbuhler', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1416, 'jeremie.habiballah', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1417, 'jeremy.amode', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1418, 'jeremy.gilbert', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1419, 'jeremy.lamboley', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1420, 'jeremy.sule', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1421, 'johann.broudin', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1422, 'jonathan.nahon', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1423, 'jonathan.sanchez', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1424, 'julien.dutto', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1425, 'kevin.salabert', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1426, 'laure.mariucci', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1427, 'louis.volant', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1428, 'ludovic.balon', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1429, 'maher.omar', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1430, 'marie-christine.codarini', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1431, 'martial.lepauvre', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1432, 'mathieu.dammien', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1433, 'mathieu.landre', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1434, 'matthieu.mariapragassam', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1435, 'maxime.rousseau', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1436, 'miao.cai', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1437, 'mickael.chokron', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1438, 'momargaye.coundoul', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1439, 'nabil.mhenni', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1440, 'nathan.perianayagassamy', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1441, 'nicolas.ignace', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1442, 'nicolas.levain', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1443, 'nicolas.quirin', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1444, 'olivier.aujoulat', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1445, 'olivier.cros', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1446, 'olivier.lecoutre', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1447, 'olivier.petchy', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1448, 'olivier.roland', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1449, 'omar.harbi', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1450, 'orjouane.benharrath', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1451, 'paul.gregoire', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1452, 'paul.nguyen', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1453, 'philippe.barbe', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1454, 'philippe.valembois', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1455, 'pierre.cabeza', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1456, 'pierre.guilbault', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1457, 'pierre.trouve', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1458, 'pierre-yves.labastie-coeyrehourcq', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1459, 'quentin.farizon', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1460, 'rachid.ounit', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1461, 'raphael.guilmin', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1462, 'raphael.renard', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1463, 'rhizlane.elrhomri', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1464, 'richard.chong', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1465, 'roland.sadaka', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1466, 'romain.cotte', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1467, 'ronan.letoullec', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1468, 'sabine.trefond', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1469, 'sabrina.fahim', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1470, 'safouene.benmansour', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1471, 'samuel.coupe', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1472, 'sanjay.talreja', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1473, 'sebastien.drure', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1474, 'sebastien.juglar', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1475, 'sebastien.monchamps', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1476, 'sonia.kostenko', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1477, 'stephanie.paina', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1478, 'thibaud.rohmer', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1479, 'thibaut.theret', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1480, 'thomas.andrejak', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1481, 'thomas.fayolle', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1482, 'tiffany.havan', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1483, 'tom.delmas', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1484, 'toufik.djaider', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1485, 'victor.houlet', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1486, 'vincent.bonesteve', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1487, 'vincent.nimal', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1488, 'virgile.fritsch', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1489, 'virginie.quach', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1490, 'wajdi.ayed', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1491, 'walid.elferdi', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1492, 'willy.vedrine', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1493, 'wilson.jouet', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1494, 'xavier.coppin', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1495, 'yann.leenhardt', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1496, 'yiqi.zhu', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1497, 'youen.jarsale', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1498, 'youness.boukakiou', 'mdp', 2010, 1, 1, 1, 3, 2007);
INSERT INTO eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, idmajeure, grade, entree) VALUES (1499, 'yuge.huang', 'mdp', 2010, 1, 1, 1, 3, 2007);


--
-- TOC entry 1904 (class 0 OID 16415)
-- Dependencies: 1551
-- Data for Name: eleveensmt; Type: TABLE DATA; Schema: public; Owner: sieben
--

INSERT INTO eleveensmt (ideleve, idensmt, dateenvoi) VALUES (1277, 9, '2010-04-30');
INSERT INTO eleveensmt (ideleve, idensmt, dateenvoi) VALUES (1277, 3, '2010-04-30');
INSERT INTO eleveensmt (ideleve, idensmt, dateenvoi) VALUES (1200, 3, '2010-04-30');
INSERT INTO eleveensmt (ideleve, idensmt, dateenvoi) VALUES (1202, 3, '2010-04-30');
INSERT INTO eleveensmt (ideleve, idensmt, dateenvoi) VALUES (1194, 3, '2010-04-30');


--
-- TOC entry 1905 (class 0 OID 16418)
-- Dependencies: 1552
-- Data for Name: enseignant; Type: TABLE DATA; Schema: public; Owner: sieben
--

INSERT INTO enseignant (idens, nomens, prenomsens, nbhrreelles, courriel, reliquat, salariepublic, login, motdepasse, autresheures) VALUES (5, 'Godard', 'Patrick', 0, 'pat@expertcomptables.org', 0, 0, 'patrick', 'fuckfisc', 0);
INSERT INTO enseignant (idens, nomens, prenomsens, nbhrreelles, courriel, reliquat, salariepublic, login, motdepasse, autresheures) VALUES (3, 'Berthelol', 'Gerard', 12, 'gege@unixkiller.org', 0, 1, 'gege', 'elvis', 0);
INSERT INTO enseignant (idens, nomens, prenomsens, nbhrreelles, courriel, reliquat, salariepublic, login, motdepasse, autresheures) VALUES (7, 'Autin', 'Alain', 42, 'alain.autin@ancienscombattants.org', 0, 0, 'captainvietnam', 'vhdl', 0);
INSERT INTO enseignant (idens, nomens, prenomsens, nbhrreelles, courriel, reliquat, salariepublic, login, motdepasse, autresheures) VALUES (8, 'Ly Vath', 'Vathana', 42, 'lyvath@ensiie.fr', 0, 0, 'lyvath', 'mdp', 0);
INSERT INTO enseignant (idens, nomens, prenomsens, nbhrreelles, courriel, reliquat, salariepublic, login, motdepasse, autresheures) VALUES (9, 'Dossantos', 'Pierre', 0, 'dossantos@cea.fr', 0, 0, 'pierre', 'mdp', 0);
INSERT INTO enseignant (idens, nomens, prenomsens, nbhrreelles, courriel, reliquat, salariepublic, login, motdepasse, autresheures) VALUES (10, 'Torri', 'Robert', 0, 'robert@evry.fr', 0, 0, 'robert', 'mdp', 0);
INSERT INTO enseignant (idens, nomens, prenomsens, nbhrreelles, courriel, reliquat, salariepublic, login, motdepasse, autresheures) VALUES (11, 'Forest', 'Julien', 0, 'forest@ensiie.fr', 0, 0, 'cafe2clopes', 'cafe2clopes', 0);
INSERT INTO enseignant (idens, nomens, prenomsens, nbhrreelles, courriel, reliquat, salariepublic, login, motdepasse, autresheures) VALUES (12, 'Riolol', 'Robert', 0, 'riolol@ensiie.fr', 0, 0, 'riolol', 'lol', 0);
INSERT INTO enseignant (idens, nomens, prenomsens, nbhrreelles, courriel, reliquat, salariepublic, login, motdepasse, autresheures) VALUES (14, 'Viegnes', 'Cecile', 0, 'cecile@viegnes.org', 0, 0, 'cecile', 'mdp', 0);
INSERT INTO enseignant (idens, nomens, prenomsens, nbhrreelles, courriel, reliquat, salariepublic, login, motdepasse, autresheures) VALUES (15, 'Jouve', 'Mireille', 0, 'jouve@ensiie.fr', 0, 0, 'jouve', 'mdp', 0);
INSERT INTO enseignant (idens, nomens, prenomsens, nbhrreelles, courriel, reliquat, salariepublic, login, motdepasse, autresheures) VALUES (2, 'Dubois', 'Catherine', 0, 'dubois@ensiie.fr', 0, 0, 'dubois', 'mdp', 0);
INSERT INTO enseignant (idens, nomens, prenomsens, nbhrreelles, courriel, reliquat, salariepublic, login, motdepasse, autresheures) VALUES (1, 'Grau', 'Brigitte', 0, 'brigitte.grau@ensiie.fr', 0, 1, 'grau', 'mdp', 0);
INSERT INTO enseignant (idens, nomens, prenomsens, nbhrreelles, courriel, reliquat, salariepublic, login, motdepasse, autresheures) VALUES (4, 'Faye', 'Alain', 42, 'alain.faye@planeur.org', 0, 1, 'faye', 'mouette', 0);
INSERT INTO enseignant (idens, nomens, prenomsens, nbhrreelles, courriel, reliquat, salariepublic, login, motdepasse, autresheures) VALUES (13, 'Ligozat', 'Anne Laure', 0, 'annelaure@ligozat.fr', 0, 0, 'annelaure', '1001', 0);


--
-- TOC entry 1906 (class 0 OID 16426)
-- Dependencies: 1554
-- Data for Name: enseignement; Type: TABLE DATA; Schema: public; Owner: sieben
--

INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (54, 16, 6, 42, 'ICP2', 42, 'Controle de process', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (3, 1, 1, 42, 'MLO', 42, 'Logique', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (4, 1, 1, 42, 'MTG', 42, 'Théorie des graphes', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (6, 2, 1, 42, 'MPR', 42, 'Probabilités', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (55, 16, 6, 42, 'IPR2', 42, 'Projet robotique', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (7, 2, 1, 42, 'MST', 42, 'Statistiques', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (8, 2, 1, 42, 'MAN', 42, 'Analyse numérique', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (9, 4, 1, 42, 'IPM', 42, 'Assembleur et Projet microprocesseur', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (10, 4, 1, 42, 'ISI', 42, 'Systèmes informatiques', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (11, 3, 1, 42, 'IAP1', 42, 'Algorithmique 1', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (12, 3, 1, 42, 'IAP2', 42, 'Algorithmique 2', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (13, 3, 1, 42, 'IAP3', 42, 'Algorithmique 3', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (14, 5, 1, 42, 'IBD', 42, 'Base de données', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (15, 5, 1, 42, 'IPI', 42, 'Projet informatique', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (16, 7, 1, 42, 'ECO', 42, 'Economie', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (17, 7, 1, 42, 'ECG', 42, 'Gestion comptable et financière', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (18, 7, 1, 42, 'EGI', 42, 'Gestion des investissements', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (5, 1, 1, 42, 'MOM', 42, 'Optimisation', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (19, 7, 1, 42, 'ECA', 42, 'Analyse des couts', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (20, 6, 1, 42, 'CAN', 42, 'Anglais', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (21, 6, 1, 42, 'COM', 42, 'Marketing de carrière et communication', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (47, 15, 5, 42, 'IAM2', 42, 'Architecture des processeurs', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (22, 9, 2, 42, 'IPC2', 42, 'Programmation Concurrente', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (23, 9, 2, 42, 'IRE2', 42, 'Reseau et applications réparties', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (24, 9, 2, 42, 'ICO2', 42, 'Compilation', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (25, 8, 2, 42, 'ILO2', 42, 'Langages orientés objets', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (26, 8, 2, 42, 'SIA2', 42, 'Analyse et conception des systèmes d''informations', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (27, 8, 2, 42, 'ISF2', 42, 'Specification & Systèmes formels', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (28, 8, 2, 42, 'ITE2', 42, 'Test du logiciel', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (29, 10, 2, 42, 'MLC2', 42, 'Calculabilité', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (30, 10, 2, 42, 'MLSF2', 42, 'Langages et Systèmes formels', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (31, 10, 2, 42, 'IIA2', 42, 'Intelligence Artificielle', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (32, 10, 2, 42, 'IPG2', 42, 'Programmation logique', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (33, 10, 2, 42, 'MPM2', 42, 'Projet Math - Info', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (34, 11, 2, 42, 'MRO2', 42, 'Recherche Operationnelle', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (35, 11, 2, 42, 'MAD2', 42, 'Analyse de données', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (36, 11, 2, 42, 'MPM2', 42, 'Projet Math Info 1', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (37, 12, 2, 42, 'EDR2', 42, 'Droit civil et commercial', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (38, 12, 2, 42, 'EMG2', 42, 'Management général de l''entreprise', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (39, 12, 2, 42, 'EGP2', 42, 'Gestion de production', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (40, 12, 2, 42, 'EMP2', 42, 'Management de projet', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (41, 12, 2, 42, 'EGS2', 42, 'Gestion des stocks et budgétaire', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (42, 12, 2, 42, 'ECE2', 42, 'Création d''entreprise', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (43, 13, 2, 42, 'CAN2', 42, 'Anglais', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (44, 13, 2, 42, 'LV2', 42, 'Langue Vivante 2', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (45, 13, 2, 42, 'EPI2', 42, 'Epistemologie', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (46, 13, 2, 42, 'CFI2', 42, 'Confrontation et initiative', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (48, 15, 5, 42, 'ISE2', 42, 'Système d''exploitation', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (49, 15, 5, 42, 'LAN2', 42, 'Administration d''un lan', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (50, 15, 5, 42, 'IPR2', 42, 'Projet Système', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (51, 16, 6, 42, 'IRO2', 42, 'Robotique', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (52, 16, 6, 42, 'IRV2', 42, 'Realité virtuelle', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (53, 16, 6, 42, 'IVA2', 42, 'Vision artificielle', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (56, 17, 7, 42, 'EAS2', 42, 'Assurance ', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (57, 17, 7, 42, 'EMF2', 42, 'Introduction aux mathématique financières ', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (58, 17, 7, 42, 'ETM', 42, 'Theorie de la finance moderne', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (59, 17, 7, 42, 'EPR2', 42, 'Projet', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (60, 18, 4, 42, 'IASI2', 42, 'Architecture des SI - Architecture logicielle', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (61, 18, 4, 42, 'IBD2', 42, 'Base de données', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (62, 18, 4, 42, 'IPR2', 42, 'Projet', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (63, 14, 3, 42, 'IQL3', 42, 'Qualité du logiciel', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (64, 14, 3, 42, 'EJE3', 42, 'Jeu d''entreprise', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (65, 14, 3, 42, 'EMP3', 42, 'Management de projet ', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (66, 14, 3, 42, 'EDI3', 42, 'Droit de l''informatique', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (67, 19, 8, 42, 'IOSP3', 42, 'Outils sémantiques pour le parallélisme', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (68, 19, 8, 42, 'ILSP3', 42, 'Langages pour les systèmes parallèles', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (69, 19, 8, 42, 'ITR3', 42, 'Systèmes réactifs temps réel', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (70, 19, 8, 42, 'IPR3', 42, 'Projet de synthèse', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (71, 20, 9, 42, 'ITP3', 42, 'Techniques de preuves', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (72, 20, 9, 42, 'ISL3', 42, 'Sémantique des langages', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (73, 20, 9, 42, 'IAS3', 42, 'Analyse statique', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (74, 20, 9, 42, 'IPR3', 42, 'Projet ', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (75, 21, 10, 42, 'MRN3', 42, 'Reseaux de neurones', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (76, 21, 10, 42, 'MRG3', 42, 'Regression', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (77, 21, 10, 42, 'MST3', 42, 'Series temporelles', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (78, 21, 10, 42, 'MAD3', 42, 'Analyse des données et Data mining', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (79, 21, 10, 42, 'IPR3', 42, 'Projet', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (80, 22, 12, 42, 'IRQ3', 42, 'Routage et qualité de service dans l''internet', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (81, 22, 12, 42, 'ISR3', 42, 'Securité dans les reseaux', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (82, 22, 12, 42, 'IPR3', 42, 'Projet', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (83, 23, 13, 42, 'IASI3', 42, 'Architecture des SI - Architecture des logiciels', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (84, 23, 13, 42, 'IBD3', 42, 'Base de données ', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (85, 23, 13, 42, 'IPR3', 42, 'Projet ', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (86, 24, 14, 42, 'MRO3', 42, 'Recherche opérationnelle', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (87, 24, 14, 42, 'MMP3', 42, 'Methode polyédriques', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (88, 24, 14, 42, 'ICA3', 42, 'Compléxité des algorithmes', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (89, 24, 14, 42, 'MPS3', 42, 'Programmation semie-definie', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (90, 24, 14, 42, 'MAR3', 42, 'Applications de la recherche opérationnelle', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (91, 24, 14, 42, 'MEC3', 42, 'Etude de cas', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (92, 25, 15, 42, 'EGC3', 42, 'Gestion du changement ', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (93, 25, 15, 42, 'ECO3', 42, 'Gestion des connaissances', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (94, 25, 15, 42, 'ECI3', 42, 'L utilisateur en tant que client interne', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (95, 25, 15, 42, 'ECF3', 42, 'Le cycle fournisseur client', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (96, 26, 16, 42, 'EFS3', 42, 'Finance Stochastique', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (97, 26, 16, 42, 'EIF3', 42, 'Intruments financiers', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (98, 26, 16, 42, 'EDC3', 42, 'Derives de credits', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (99, 26, 16, 42, 'IPR3', 42, 'Projet', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (100, 27, 17, 42, 'IAA3', 42, 'Apprentissage artificiel', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (101, 27, 17, 42, 'ILF3', 42, 'Logique floue', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (102, 27, 17, 42, 'IAD3', 42, 'Intelligence artificielle distribuée', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (103, 27, 17, 42, 'ITAL3', 42, 'Traitement automatique de la langue ', '2000-01-01', '2000-01-01');
INSERT INTO enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) VALUES (104, 27, 17, 42, 'IPR3', 42, 'Projet', '2000-01-01', '2000-01-01');


--
-- TOC entry 1907 (class 0 OID 16434)
-- Dependencies: 1556
-- Data for Name: formation; Type: TABLE DATA; Schema: public; Owner: sieben
--

INSERT INTO formation (idformation, nomformation) VALUES (1, '1A');
INSERT INTO formation (idformation, nomformation) VALUES (2, '2A');
INSERT INTO formation (idformation, nomformation) VALUES (3, '3A');
INSERT INTO formation (idformation, nomformation) VALUES (4, 'FIP1');


--
-- TOC entry 1908 (class 0 OID 16442)
-- Dependencies: 1558
-- Data for Name: optionoutc; Type: TABLE DATA; Schema: public; Owner: sieben
--

INSERT INTO optionoutc (idoption, idformation, nomoption, nbgroupestd, promo) VALUES (4, 2, 'Systèmes d''information et Bases de données (2A)', 5, 2000);
INSERT INTO optionoutc (idoption, idformation, nomoption, nbgroupestd, promo) VALUES (5, 2, 'Système d''exploitation (2A)', 5, 2000);
INSERT INTO optionoutc (idoption, idformation, nomoption, nbgroupestd, promo) VALUES (6, 2, 'Robotique et réalité virtuelle (2A)', 5, 2000);
INSERT INTO optionoutc (idoption, idformation, nomoption, nbgroupestd, promo) VALUES (7, 2, 'Banque, Finance et Assurance (2A)', 5, 2000);
INSERT INTO optionoutc (idoption, idformation, nomoption, nbgroupestd, promo) VALUES (3, 3, 'Tronc commun (3A)', 5, 2000);
INSERT INTO optionoutc (idoption, idformation, nomoption, nbgroupestd, promo) VALUES (2, 2, 'Tronc commun (2A)', 5, 2000);
INSERT INTO optionoutc (idoption, idformation, nomoption, nbgroupestd, promo) VALUES (1, 1, 'Tronc commun (1A)', 5, 2000);
INSERT INTO optionoutc (idoption, idformation, nomoption, nbgroupestd, promo) VALUES (11, 1, 'Projet personnel', 5, 2000);
INSERT INTO optionoutc (idoption, idformation, nomoption, nbgroupestd, promo) VALUES (8, 3, 'Conception et validation d''applications réactives (3A)', 5, 2000);
INSERT INTO optionoutc (idoption, idformation, nomoption, nbgroupestd, promo) VALUES (9, 3, 'Programmation raisonnée (3A)', 5, 2000);
INSERT INTO optionoutc (idoption, idformation, nomoption, nbgroupestd, promo) VALUES (10, 3, 'Modelisation (3A)', 5, 2000);
INSERT INTO optionoutc (idoption, idformation, nomoption, nbgroupestd, promo) VALUES (13, 3, 'Système d''information avancée et bases de données (3A)', 5, 2000);
INSERT INTO optionoutc (idoption, idformation, nomoption, nbgroupestd, promo) VALUES (12, 3, 'Reseau - Securité :) (3A)', 5, 2000);
INSERT INTO optionoutc (idoption, idformation, nomoption, nbgroupestd, promo) VALUES (14, 3, 'Optimisation (3A)', 5, 2000);
INSERT INTO optionoutc (idoption, idformation, nomoption, nbgroupestd, promo) VALUES (15, 3, 'Nouvelles technologies et organisation des entreprises (3A)', 5, 2000);
INSERT INTO optionoutc (idoption, idformation, nomoption, nbgroupestd, promo) VALUES (16, 3, 'Marché Fist (3A)', 5, 2000);
INSERT INTO optionoutc (idoption, idformation, nomoption, nbgroupestd, promo) VALUES (17, 3, 'Intelligence artificielle (3A)', 5, 2000);


--
-- TOC entry 1909 (class 0 OID 16450)
-- Dependencies: 1560
-- Data for Name: questionnaire; Type: TABLE DATA; Schema: public; Owner: sieben
--

INSERT INTO questionnaire (idquestionnaire, idensmt, dateenvoi) VALUES (1, 9, '2010-04-30');
INSERT INTO questionnaire (idquestionnaire, idensmt, dateenvoi) VALUES (2, 3, '2010-04-30');
INSERT INTO questionnaire (idquestionnaire, idensmt, dateenvoi) VALUES (3, 3, '2010-04-30');
INSERT INTO questionnaire (idquestionnaire, idensmt, dateenvoi) VALUES (4, 3, '2010-04-30');
INSERT INTO questionnaire (idquestionnaire, idensmt, dateenvoi) VALUES (5, 3, '2010-04-30');


--
-- TOC entry 1910 (class 0 OID 16453)
-- Dependencies: 1561
-- Data for Name: questions; Type: TABLE DATA; Schema: public; Owner: sieben
--

INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (1, 'Globalement, vous vous estimez satisfait de cet enseignement', 'note', 1, 1);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (2, 'Les objectifs visés par l''enseignement ont été clairement expliqués', 'note', 1, 2);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (3, 'Le contenu de l''enseignement a correspondu à vos attentes', 'note', 1, 3);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (4, 'Le contenu de cet enseignement est clairement lié à votre future vie professionnelle', 'note', 1, 4);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (5, 'Des liens ont été établis entre l''enseignement et les pratiques professionnelles', 'note', 1, 5);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (6, 'Des liens ont été établis avec d''autres cours', 'note', 1, 6);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (7, 'Il n''y a pas de redites inutiles entre ce cours et  d''autres cours', 'note', 1, 7);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (8, 'Si il y a des redites, lesquelles', 'texte', 1, 8);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (9, 'Vous n''avez pas eu de difficultés dans la compréhension du cours', 'note', 1, 9);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (10, 'S''il y a des TD ou TP pour cet enseignement, ils sont bien articulés avec le cours', 'note', 1, 10);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (11, 'Vous n''avez pas eu de difficultés dans la compréhension des TD', 'note', 1, 11);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (12, 'Vous n''avez pas eu de difficultés dans la compréhension des TP', 'note', 1, 12);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (13, 'Si il a des parties du cours des TD/TP qui vous ont posés problème, lesquelles', 'texte', 1, 13);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (14, 'L''évaluation du cours réalisé est en accord avec l''enseignement dispensé', 'note', 1, 14);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (0, 'Filière d''origine :', 'texte', 0, 0);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (25, 'Avant de suivre cet enseignement, vous estimiez avoir une bonne connaissance de cette matière', 'note', 4, 26);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (26, 'Indiquez le pourcentage de cours suivis', 'nombre', 4, 27);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (27, 'Indiquez le pourcentage de TD-TP suivis', 'nombre', 4, 28);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (28, 'Evaluez l''intensité du travail personnel à fournir en dehors de l''ensignement', 'nombre', 4, 29);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (29, 'Sur quels point cet enseignement devrait-il être amélioré', 'texte', 4, 30);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (30, 'Comment cet enseignement devrait-il être amélioré', 'texte', 4, 31);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (31, 'Autres remarques', 'texte', 4, 32);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (15, 'Qualité pédagogique du chargé de cours', 'texte', 3, 16);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (16, 'Sur quels critères faites-vous cette évaluation', 'texte', 3, 17);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (17, 'L''enseignant de cours a suscité votre intérêt', 'note', 3, 18);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (18, 'Les exposés sont structurés', 'note', 3, 19);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (19, 'La forme de présentation (tableau, transparents, film, etc..) vous a bien convenu', 'note', 3, 20);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (20, 'Les documents fournis (notes, polycopiés, documentation en ligne) sont clairs, compréhensibles et adaptés', 'note', 3, 21);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (21, 'Des liens ont été établis entre théorie et pratique', 'note', 3, 22);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (22, 'L''enseignant est ouvert aux questions pédagogiques des élèves (compréhension du cours, etc...) ', 'note', 3, 23);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (23, 'Vous avez eu des réponses satisfaisantes aux questions', 'note', 3, 24);
INSERT INTO questions (idquestion, intitule, typequestion, idsection, ordrequestion) VALUES (24, 'Donner une note globale sur 20', 'note', 4, 25);


--
-- TOC entry 1911 (class 0 OID 16461)
-- Dependencies: 1563
-- Data for Name: redoublement; Type: TABLE DATA; Schema: public; Owner: sieben
--



--
-- TOC entry 1912 (class 0 OID 16464)
-- Dependencies: 1564
-- Data for Name: reponses; Type: TABLE DATA; Schema: public; Owner: sieben
--

INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (0, '3', 1);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (0, '''=''', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (1, '0', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (2, '0', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (3, '0', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (4, '0', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (5, '0', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (6, '0', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (7, '0', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (8, '', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (9, '0', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (10, '0', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (11, '0', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (12, '0', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (13, '', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (14, '0', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (15, '', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (16, '', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (17, '0', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (18, '0', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (19, '0', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (20, '0', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (21, '0', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (22, '0', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (23, '0', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (24, '0', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (25, '0', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (29, '', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (30, '', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (31, '', 2);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (0, '\\O/ Elvis \\O/', 3);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (1, '3', 3);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (2, '5', 3);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (3, '3', 3);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (4, '2', 3);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (5, '2', 3);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (6, '2', 3);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (7, '4', 3);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (0, '\\O/ Elvis is alive XDDD \\O/', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (1, '0', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (2, '0', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (3, '0', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (4, '0', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (5, '0', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (6, '0', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (7, '0', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (8, '', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (9, '0', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (10, '0', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (11, '0', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (12, '0', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (13, '', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (14, '0', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (15, '', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (16, '', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (17, '0', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (18, '0', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (19, '0', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (20, '0', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (21, '0', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (22, '0', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (23, '0', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (24, '0', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (25, '0', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (29, '', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (30, '', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (31, '', 4);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (0, '''=''', 5);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (1, '5', 5);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (2, '3', 5);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (3, '4', 5);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (4, '5', 5);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (5, '4', 5);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (6, '4', 5);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (7, '3', 5);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (8, '\\O/ Elvis \\O/', 5);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (9, '5', 5);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (10, '3', 5);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (11, '2', 5);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (12, '5', 5);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (13, '\\O/ Elvis \\O/\\O/ Elvis \\O/\\O/ Elvis \\O/\\O/ Elvis \\O/\\O/ Elvis \\O/\\O/ Elvis \\O/\\O/ Elvis \\O/\\O/ Elvis \\O/', 5);
INSERT INTO reponses (idquestion, valeur, idquestionnaire) VALUES (14, '4', 5);


--
-- TOC entry 1913 (class 0 OID 16467)
-- Dependencies: 1565
-- Data for Name: sections; Type: TABLE DATA; Schema: public; Owner: sieben
--

INSERT INTO sections (idsection, nom) VALUES (0, 'Renseignements élèves/stagiaires');
INSERT INTO sections (idsection, nom) VALUES (1, 'Appréciation de l''enseignement');
INSERT INTO sections (idsection, nom) VALUES (4, 'A propos de l''enseignement que vous avez suivi');
INSERT INTO sections (idsection, nom) VALUES (3, 'Interaction enseignant-élève');


--
-- TOC entry 1914 (class 0 OID 16475)
-- Dependencies: 1567
-- Data for Name: ue; Type: TABLE DATA; Schema: public; Owner: sieben
--

INSERT INTO ue (idue, nomue, idcharge) VALUES (13, 'Formation humaine (2A)', 13);
INSERT INTO ue (idue, nomue, idcharge) VALUES (14, 'Tronc Commun (3A)', 14);
INSERT INTO ue (idue, nomue, idcharge) VALUES (1, 'Math 1 (1A)', 1);
INSERT INTO ue (idue, nomue, idcharge) VALUES (2, 'Math 2 (1A)', 2);
INSERT INTO ue (idue, nomue, idcharge) VALUES (3, 'Algorithmique - Programmation (1A)', 3);
INSERT INTO ue (idue, nomue, idcharge) VALUES (4, 'Matériel & Systèmes (1A)', 4);
INSERT INTO ue (idue, nomue, idcharge) VALUES (5, 'Applications (1A)', 5);
INSERT INTO ue (idue, nomue, idcharge) VALUES (6, 'Formation humaine (1A)', 6);
INSERT INTO ue (idue, nomue, idcharge) VALUES (7, 'Economie-Gestion (1A)', 7);
INSERT INTO ue (idue, nomue, idcharge) VALUES (8, 'Génie Logiciel (2A)', 8);
INSERT INTO ue (idue, nomue, idcharge) VALUES (9, 'Système et réseaux (2A)', 9);
INSERT INTO ue (idue, nomue, idcharge) VALUES (10, 'IA et informatique fondamentale (2A)', 10);
INSERT INTO ue (idue, nomue, idcharge) VALUES (11, 'Maths (2A)', 11);
INSERT INTO ue (idue, nomue, idcharge) VALUES (12, 'Organisation (2A)', 12);
INSERT INTO ue (idue, nomue, idcharge) VALUES (19, 'Option - 3A - Conception et validation d''applications réactives', 19);
INSERT INTO ue (idue, nomue, idcharge) VALUES (18, 'Option - 2A - SIABD', 18);
INSERT INTO ue (idue, nomue, idcharge) VALUES (17, 'Option - 2A - Banque, Finance & Assurance', 17);
INSERT INTO ue (idue, nomue, idcharge) VALUES (16, 'Option - 2A - Robotique & Réalité virtuelle', 16);
INSERT INTO ue (idue, nomue, idcharge) VALUES (15, 'Option - 2A - Systèmes d''exploitation', 15);
INSERT INTO ue (idue, nomue, idcharge) VALUES (20, 'Option - 3A - Programmation raisonnée', 20);
INSERT INTO ue (idue, nomue, idcharge) VALUES (21, 'Option - 3A - Modélisation', 21);
INSERT INTO ue (idue, nomue, idcharge) VALUES (22, 'Option - 3A - Reseau, Securité', 22);
INSERT INTO ue (idue, nomue, idcharge) VALUES (23, 'Option - 3A - Systèmes d''information avancée et bases de données', 23);
INSERT INTO ue (idue, nomue, idcharge) VALUES (24, 'Option - 3A - Optimisation', 24);
INSERT INTO ue (idue, nomue, idcharge) VALUES (25, 'Option - 3A - Nouvelles technologies et organisation des entreprises', 25);
INSERT INTO ue (idue, nomue, idcharge) VALUES (26, 'Option - 3A - Marché fist', 26);
INSERT INTO ue (idue, nomue, idcharge) VALUES (27, 'Option - 3A - Intelligence artificielle', 27);


-- Completed on 2010-05-03 19:55:36 CEST

--
-- PostgreSQL database dump complete
--

