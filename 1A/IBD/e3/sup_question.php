<?php

require_once ( 'include/main.php' );

// Seul l'administrateur peut voir cette page
if ( !is_admin() )
{
    header ( 'Location: login.php' );
    die();
}

if ( !isset ( $_GET['id'] ) || !is_numeric ( $_GET['id'] ) )
{
    header ( 'Location: questionnaire.php' );
    die();
}

$files_css[] = 'form.css';

$titre = 'Supprimer une question';

include_once ( 'include/header.php' );

// On vérifie que la question existe
$sql = 'SELECT intitule, idsection
        FROM questions
        WHERE idquestion = ' . db_protect ( $_GET['id'] ) . ';';

$req = pg_query ( $db_link , $sql , __FILE__ , __LINE__ );

// La question n'existe pas
if ( pg_num_rows ( $req ) === 0 )
{
    header ( 'Location: questionnaire.php' );
    die();
}

$row = pg_fetch_assoc ( $req );


// Traitement du formulaire
if ( isset ( $_POST['submit'] ) )
{
    // On supprime toutes les réponses à cette question
    $sql = 'DELETE FROM reponses
            WHERE idquestion = ' . db_protect ( $_GET['id'] ) . ';';

    pg_query ( $db_link , $sql , __FILE__ , __LINE__ );

    // On supprime la question
    $sql = 'DELETE FROM questions
            WHERE idquestion = ' . db_protect ( $_GET['id'] ) . ';';

    pg_query ( $db_link , $sql , __FILE__ , __LINE__ );

    // On remonte les questions suivantes dans la section
    $sql = 'UPDATE questions
            SET idquestion = idquestion - 1
            WHERE idquestion > ' . db_protect ( $_GET['id'] ) . '
              AND idsection = ' . $row['idsection'] . ';';

    pg_query ( $db_link , $sql , __FILE__ , __LINE__ );

    // Message d'information à afficher sur la page suivante
    $_SESSION['infos'] = 'La question a été supprimée.';
    $_SESSION['infos_dialog'] = true;

    header ( 'Location: questionnaire.php' );
    die();
}

// Affichage de la page
echo "<p>Voulez-vous supprimer la question <quote>" . $row['intitule'] . "</quote> ?</p>";
echo '<form action="sup_question.php?id=' . $_GET['id'] . '" method="post">';
echo '<p class="form_submit"><input type="submit" name="submit" value="Oui" /> <input type="button" class="form_back" value="Non" /></p>';
echo "</form>";

include_once ( 'include/footer.php' );

?>
