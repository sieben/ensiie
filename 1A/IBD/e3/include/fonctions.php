<?php

/**
 * Connexion à la base de données
 **/

function db_connect ( $host , $port , $user , $pass , $base )
{
    return pg_connect ( 'host=' . $host . ' port=' . $port . ' dbname=' . $base . ' user=' . $user . ' password=' . $pass );
}


/**
 * Exécuter une requête SQL.
 **/

function db_query ( $link , $sql , $file = '' , $line = 0 )
{
    global $nbr_requetes;
    global $tps_requetes;

    $tps_start = microtime ( true );

    $res = pg_query ( $link , $sql ) or die ( 'Erreur SQL (' . $file . ' ligne ' . $line . ') : ' . pg_last_error() . '<br/>' . $sql );

    $tps_requetes += microtime ( true ) - $tps_start;
    $nbr_requetes++;

    return $res;
}


/**
 * Protège une chaine de caractère pour l'utiliser dans une requête.
 **/

function db_protect ( $texte )
{
    if ( get_magic_quotes_gpc () == 1 )
    {
        $texte = stripslashes ( $texte );
    }

    return pg_escape_string ( trim ( $texte ) );
}


/**
 * Indique si un enseignant a le droit de voir un enseignement.
 **/

function ensmt_enseignant ( $ensmt , $ens )
{
    global $db_link;

    // Cours magistraux
    $sql = 'SELECT COUNT(*) AS c
            FROM choixcoursmagistraux
            WHERE idensmt = ' . db_protect ( $ensmt ) . '
              AND idens = ' . db_protect ( $ens ) . ';';

    $req = db_query ( $db_link , $sql , __FILE__ , __LINE__ );

    $row = pg_fetch_assoc ( $req );

    if ( $row['c'] != 0 ) return true;

    // Chargé de TD
    $sql = 'SELECT COUNT(*) AS c
            FROM choixtd
            WHERE idensmt = ' . db_protect ( $ensmt ) . '
              AND idens = ' . db_protect ( $ens ) . ';';

    $req = db_query ( $db_link , $sql , __FILE__ , __LINE__ );

    $row = pg_fetch_assoc ( $req );

    if ( $row['c'] != 0 ) return true;

    // Chargé de TP
    $sql = 'SELECT COUNT(*) AS c
            FROM choixtp
            WHERE idensmt = ' . db_protect ( $ensmt ) . '
              AND idens = ' . db_protect ( $ens ) . ';';

    $req = db_query ( $db_link , $sql , __FILE__ , __LINE__ );

    $row = pg_fetch_assoc ( $req );

    if ( $row['c'] != 0 ) return true;

    // Table des droits
    $sql = 'SELECT COUNT(*) AS c
            FROM droits
            WHERE idensmt = ' . db_protect ( $ensmt ) . '
              AND idens = ' . db_protect ( $ens ) . ';';

    $req = db_query ( $db_link , $sql , __FILE__ , __LINE__ );

    $row = pg_fetch_assoc ( $req );

    return ( $row['c'] != 0 );
}


/**
 * Indique si un élève a le droit de voir un enseignement.
 * -1 : Déjà évalué
 *  0 : Non
 *  1 : Pas encore évalué
 **/

function ensmt_eleve ( $ensmt , $eleve )
{
    global $db_link;

    $sql = 'SELECT promo
            FROM eleve
            WHERE ideleve = ' . $eleve . ';';

    $req = db_query ( $db_link , $sql , __FILE__ , __LINE__ );

    if ( pg_num_rows ( $req ) == 0 )
    {
        return 0;
    }

    $row = pg_fetch_assoc ( $req );
    $promo = $row['promo'];

    $sql = 'SELECT idue
            FROM enseignement
            WHERE idensmt = ' . $ensmt . '
              AND ( idoption = ';

    // Enseignements correspondant à la promo de l'élève
    if ( $promo == 2012 )
    {
        $sql .= '1';
    }
    else if ( $promo == 2011 )
    {
        $sql .= '2';

        $sql_tmp = "SELECT idoption1
                    FROM eleve
                    WHERE ideleve = " . $eleve . ";";

        $req = db_query ( $db_link , $sql , __FILE__ , __LINE__ );

        $row = pg_fetch_assoc ( $req );

        if ( !empty ( $row['idoption1'] ) )
        {
             $sql .= ' OR idoption = ' . $row['idoption1'];
        }
    }
    else if ( $promo == 2010 )
    {
       $sql .= '3';

        $sql_tmp = 'SELECT idoption1, idoption2, idoption3
                    FROM eleve
                    WHERE ideleve = ' . $eleve . ';';

        $req = db_query ( $db_link , $sql_tmp , __FILE__ , __LINE__ );

        $row = pg_fetch_assoc ( $req );

        if ( !empty ( $row['idoption1'] ) )
        {
            $sql .= ' OR idoption = ' . $row['idoption1'];
        }

        if ( !empty ( $row['idoption2'] ) )
        {
            $sql .= ' OR idoption = ' . $row['idoption2'];
        }

        if ( !empty ( $row['idoption3'] ) )
        {
            $sql .= ' OR idoption = ' . $row['idoption3'];
        }
    }
    else
    {
        $sql .= '0';
    }

    $sql .= ');';

    $req = db_query ( $db_link , $sql , __FILE__ , __LINE__ );

    if ( pg_num_rows ( $req ) == 0 )
    {
        // Conditionnelle ?
        $sql = 'SELECT ens1.idensmt AS idensmt1
                       ens2.idensmt AS idensmt2
                FROM conditionnelle
                LEFT OUTER JOIN enseignement AS ens1 ON ens1.idue = idue1manquee
                LEFT OUTER JOIN enseignement AS ens2 ON ens2.idue = idue2manquee
                WHERE ideleve = ' . $eleve . ';';

        $req = db_query ( $db_link , $sql , __FILE__ , __LINE__ );

        while ( $row = pg_fetch_assoc ( $req ) )
        {
            if ( !empty ( $row['idensmt1'] ) )
            {
                if ( !empty ( $row['idensmt2'] ) )
                {
                    if ( $ensmt != $row['idensmt1'] &&
                         $ensmt != $row['idensmt2'] )
                    {
                        return 0;
                    }
                }
                else if ( $ensmt != $row['idensmt1'] )
                {
                    return 0;
                }
            }
        }

        return 0;
    }

    $row = pg_fetch_assoc ( $req );
    $idue = $row['idue'];

    // Redoublement ?
    $sql = 'SELECT iduereussie
            FROM redoublement
            WHERE ideleve = ' . $eleve . ';';

    $req = db_query ( $db_link , $sql , __FILE__ , __LINE__ );

    if ( pg_num_rows ( $req ) > 0 )
    {
        while ( $row = pg_fetch_assoc ( $req ) )
        {
            if ( $ensmt == $row['iduereussie'] )
            {
                return 0;
            }
        }
    }


    // On cherche si l'élève a déjà évalué cet enseignement
    $sql = 'SELECT idensmt
            FROM eleveensmt
            WHERE ideleve = ' . $eleve . '
              AND idensmt = ' . $ensmt . ';';

    $req = db_query ( $db_link , $sql , __FILE__ , __LINE__ );

    if ( pg_num_rows ( $req ) > 0 )
    {
        return -1;
    }

    return 1;
}


/**
 * Indique si l'utilisateur est le directeur des études.
 **/

function is_admin()
{
    return ( isset ( $_SESSION['uid'] ) && 
             isset ( $_SESSION['login'] ) && 
             isset ( $_SESSION['admin'] ) );
}


/**
 * Indique si l'utilisateur est un élève.
 **/

function is_eleve()
{
    return ( isset ( $_SESSION['uid'] ) && 
             isset ( $_SESSION['login'] ) && 
             isset ( $_SESSION['promo'] ) &&
             $_SESSION['promo'] != 0 );
}


/**
 * Indique si l'utilisateur est un enseignant.
 **/

function is_ens()
{
    return ( isset ( $_SESSION['uid'] ) && 
             isset ( $_SESSION['login'] ) && 
             isset ( $_SESSION['promo'] ) &&
             $_SESSION['promo'] == 0 );
}


/**
 * Indique si l'utilisateur est connecté.
 **/

function is_connected()
{
    return ( isset ( $_SESSION['uid'] ) && 
             isset ( $_SESSION['login'] ) );
}

?>
