<?php

error_reporting ( E_ALL );

ob_start ( 'ob_gzhandler' ); // Création du buffer avec compression GZip
session_start();             // Utilisation des sessions

require_once ( 'include/config.php' );
require_once ( 'include/fonctions.php' );

// Fuseau horaire
if ( function_exists ( 'date_default_timezone_set' ) )
{
    date_default_timezone_set ( 'Europe/Paris' );
}

// Connexion à la base de données
$db_link = db_connect ( $db_host , $db_port , $db_user , $db_pass , $db_base );

if ( $db_link === false )
{
    die ( 'Erreur lors de la connexion à la base de donnée.' );
}

// Variables communes à toutes les pages
$titre = '';
$nbr_requetes = 0;
$tps_requetes = 0;
$files_css = array();
$files_js = array();

?>
