<?php

/*
 * Liste des requêtes qui n'ont pas besoin de variables de sessions.
*/

// Affichage du questionnaire
$sql = 'SELECT idquestion, intitule, typequestion,
               ordrequestion, idsection, nom
        FROM questions
        NATURAL JOIN sections
        ORDER BY idsection, ordrequestion;';

// Liste des promos
$sql = 'SELECT DISTINCT(promo)
        FROM eleve
        ORDER BY promo;';

// Liste des élèves
$sql = 'SELECT ideleve, login, promo
        FROM eleve
        ORDER BY promo, login;';

// Liste des enseignements
$sql = 'SELECT idensmt, code
        FROM enseignement
        ORDER BY code;';

// Liste des enseignants
$sql = 'SELECT idens, nomens, prenomsens
        FROM enseignant
        ORDER BY nomens;';

// Recherche des questions et des sections
$sql = 'SELECT idquestion, intitule, ordrequestion,
               sections.idsection AS idsection, nom
        FROM sections
        LEFT OUTER JOIN questions
        ON questions.idsection = sections.idsection
        ORDER BY idsection, ordrequestion;';

?>
