<?php

class camembert
{
    private $dim;    // dim de position dans le tableau (Nombre d'élements déjà rentrés)
    private $tabVal; // tableau des valeurs
    private $tabNom; // tableau des noms
    private $tot;    // total des valeurs

    // camembert : constructeur / initialisation
    public function __construct()
    {
        $this->dim = 0;
        $this->tot = 0;

        $this->tabVal = array();
        $this->tabNom = array();
    } // fin camembert


    // add_tab : ajoute une donnee au tableau (valeur + libelle)
    public function add_tab ( $val, $nom )
    {
        if ($val >= 0)
        {
            $this->tabVal[$this->dim] = $val;
            $this->tabNom[$this->dim] = $nom;
            $this->tot = $this->tot + $val; // calcul du total
            $this->dim++; // MAJ de l'dim
        }
    } // fin add_tab


    // stat2png : genere une image camembert au format PNG
    public function stat2png()
    {
        $width       = 420; // largeur de l'image
        $height      = 220; // hauteur de l'image
        $centre_x    = 110; // position X du centre du camembert
        $centre_y    = 110; // position Y du centre du camembert
        $cam_largeur = 180; // largeur du camembert
        $cam_hauteur = 180; // hauteur du camembert

        // Déclaration et initialisation de l'image
        $img = ImageCreateTrueColor ( $width , $height )
               or die ( 'Probleme : Chargement de la lib GD impossible' );

        // Déclaration des couleurs
        $blanc = ImageColorAllocate ( $img , 255,255,255 );
        $noir  = ImageColorAllocate ( $img , 0,0,0 );

        $tab_couleurs[0] = array(100,100,100);
        $tab_couleurs[1] = array( 47,150,228);
        $tab_couleurs[2] = array(181, 55,228);
        $tab_couleurs[3] = array(189,224, 57);
        $tab_couleurs[4] = array(228, 99, 41);
        $tab_couleurs[5] = array(230,214, 46);

        // Couleur de fond de l'image (blanc)
        ImageFill ( $img , 0 , 0 , $blanc ); 

        $tot_angle = -90; // On commence à afficher les données à partir de midi

        // On peut commencer a afficher les morceaux de camemberts

        // On affiche les morceaux "clairs" du camembert
        // pour chaque morceau du camembert
        for ( $i = 0 ; $i < $this->dim ; $i++ )
        {
            // On calcule et mémorise l'angle
            $tab_angle[$i] = $tot_angle;
            $angle = ( $this->tabVal[$i] * 360 ) / $this->tot;
            $tot_angle += $angle;

            // On n'oublie pas de mémoriser le dernier angle
            $tab_angle[$this->dim] = -90;

            $color = ImageColorAllocate ( $img ,
                    $tab_couleurs[$i][0],
                    $tab_couleurs[$i][1],
                    $tab_couleurs[$i][2]
                ); // R G B

            if ( $tab_angle[$i] != $tot_angle )
            {
                // On affiche le morceau de camembert
                ImageFilledArc ( $img , $centre_x , $centre_y, $cam_largeur,
                        $cam_hauteur , $tab_angle[$i],
                        $tot_angle , $color , IMG_ARC_PIE );
            }

            //  On affiche la légende
            $x = 235;
            $y = 40 + $i * 16;

            // Petit carré de couleur
            ImageFilledRectangle ( $img , $x , $y , $x + 9 , $y + 9 , $color );

            // Texte
            ImageString ( $img , 2 , $x + 15 , $y - 2, $this->tabNom[$i] .
                ' (' . floor(($this->tabVal[$i] * 100)/$this->tot).'%)' ,
                $noir);
        }

        // Nombre de votants après la légende
        ImageString ( $img , 2 , $x + 15 , 150 , 
                      'Nombre de votants : ' . $this->tot , $noir );

        // Finalisation de l'image
        header ( 'Content-type: image/png' );
        ImagePNG ( $img); // Création de l'image
        ImageDestroy ( $img); // Nettoyage des ressources
    }
}

?>