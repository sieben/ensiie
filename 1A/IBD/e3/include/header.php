<?php

// Fichiers nécessaires à l'affichage d'une boite de dialogue
if ( isset ( $_SESSION['infos'] ) )
{
    $files_css[] = 'jquery-ui-1.8.custom.css';
    $files_js[] = 'jquery-ui-1.8.custom.min.js';
}

// Suppression des doublons
$files_js = array_unique ( $files_js );
$files_css = array_unique ( $files_css );

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/2002/REC-xhtml1-20020801/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">

<head>

<title>E3 - Espace d'Evaluation des Enseignements</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />

<meta name="DC.Title" content="E3 - Espace d'Evaluation des Enseignements" />
<meta name="DC.Format" content="text/html" />
<meta name="DC.Language" scheme="RFC3066" content="fr" />
<meta name="DC.Creator" content="Jean Cremese, Rémy Léone et Teddy Michel" />
<meta name="DC.Date" content="<?php echo date ( 'Y-m-d' ); ?>" />

<link rel="author" title="Auteur" href="Jean Cremese, Rémy Léone et Teddy Michel" />
<link rel="start" title="Accueil" href="index.php" />
<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/" />

<script type="text/javascript" src="javascript/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="javascript/fonctions.js"></script>
<?php

foreach ( $files_js as $file )
{
    echo '<script type="text/javascript" src="javascript/' . $file . '" /></script>' . "\n";
}

?>

<link rel="stylesheet" type="text/css" media="screen" href="css/styles.css" />
<?php

foreach ( $files_css as $file )
{
    echo '<link rel="stylesheet" type="text/css" media="screen" href="css/' . $file . '" />' . "\n";
}

?>

</head>

<body>

<div class="header-wrapper">

<div class="header pagewidth">
<h1><a href="index.php">E3</a></h1>
<h2>Espace d'Evaluation des Enseignements</h2>
</div>

</div>

<div class="nav-wrapper-outside">
<div class="nav-wrapper pagewidth">

<div class="nav">

<ul>
  <li><a href="index.php">Accueil</a></li>
  <li><a href="questionnaire.php">Questionnaire</a></li>
<?php

if ( is_connected() )
{
    if ( is_admin() )
    {
        echo '  <li><a href="administration.php">Administration</a></li>' . "\n";
    }

    echo '  <li><a href="logout.php">Déconnexion</a></li>' . "\n";
}
else
{
    echo '  <li><a href="login.php">Connexion</a></li>' . "\n";
}

?>
</ul>

</div>

</div>
</div>

<div class="pagewidth">
<div class="page-wrap">
<div class="content">

<?php

// Affichage d'une boite de dialogue (ou d'un message d'information)
if ( isset ( $_SESSION['infos'] ) && !empty ( $_SESSION['infos'] ) )
{
    if ( isset ( $_SESSION['infos_dialog'] ) )
    {
        echo '<script type="text/javascript">';
        echo '$(function() {$("#dialog").dialog({height: 140,modal: true});})';
        echo '</script>';
    }

    echo '<div id="';
    echo ( isset ( $_SESSION['infos_dialog'] ) ? 'dialog' : 'info' );
    echo '" class="';
    echo ( isset ( $_SESSION['infos_dialog'] ) ? 'dialog' : 'info' );
    echo '"><p>' . $_SESSION['infos'] . '</p></div>';

    unset ( $_SESSION['infos'] );
}

?>

<h3><?php echo $titre; ?></h3>
