<?php

require_once ( 'include/main.php' );

// On n'a pas précisé l'identifiant de l'élève
if ( !isset ( $_GET['eleve'] ) || !is_numeric ( $_GET['eleve'] ) )
{
    header ( 'Location: liste_eleves.php' );
    die();
}

// Seuls les administrateurs, les professeurs et l'élève concerné peuvent voir cette page
if ( ( !is_ens() && !is_admin() ) || ( is_eleve() && $_SESSION['uid'] != $_GET['eleve'] ) )
{
    header ( 'Location: login.php' );
    die();
}

$titre = 'Informations sur un élève';

include_once ( 'include/header.php' );

echo '<p><a href="liste_eleves.php">Retour à la liste des élèves</a></p>';


$sql = 'SELECT login, promo, groupe, sousgroupe, grade, entree,
               iduereussie, idue1manquee, idue2manquee,
               ue1.nomue AS intitule1,
               ue2.nomue AS intitule2,
               ue3.nomue AS intitule3
        FROM eleve
        LEFT OUTER JOIN redoublement
          ON redoublement.ideleve = eleve.ideleve
        LEFT OUTER JOIN conditionnelle
          ON conditionnelle.ideleve = eleve.ideleve
        LEFT OUTER JOIN ue AS ue1
          ON ue1.idue = redoublement.iduereussie
        LEFT OUTER JOIN ue AS ue2
          ON ue2.idue = conditionnelle.idue1manquee
        LEFT OUTER JOIN ue AS ue3
          ON ue3.idue = conditionnelle.idue2manquee
        WHERE eleve.ideleve = ' . db_protect ( $_GET['eleve'] ) . ';';

$req = db_query ( $db_link , $sql );

if ( pg_num_rows ( $req ) > 0 )
{
    $row = pg_fetch_assoc ( $req );

    // On indique si l'élève a redoublé ou est passé en conditionnelle :
    // pour cela, on teste si les valeurs idreussie et idue1manquee sont
    // vides ou non
    if ( !empty ( $row['iduereussie'] ) )
    {
        $redoub = 'Oui';
    }
    else
    {
        $redoub = 'Non';
    }

    if ( ( !empty ( $row['idue1manquee'] ) ) || !empty ( $row['idue2manqee'] ) )
    {
        $condi = 'Oui';
    }
    else
    {
        $condi = 'Non';
    }

    echo "<ul>\n";
    echo '  <li><strong>Login</strong>&nbsp: ' . $row['login'] . "</li>\n";
    echo '  <li><strong>Promotion</strong>&nbsp: ' . $row['promo'] . "</li>\n";
    echo '  <li><strong>Groupe</strong>&nbsp: ' . $row['groupe'] . '.' . $row['sousgroupe'] . "</li>\n";
    echo '  <li><strong>Grade</strong>&nbsp: ' . $row['grade'] . "</li>\n";
    echo '  <li><strong>Entrée</strong>&nbsp: ' . $row['entree'] . "</li>\n";
    echo '  <li><strong>Redoublement</strong>&nbsp: ' . $redoub;
    
    if ( $redoub == 'Oui' )
    {
        echo ' (' . $row['intitule1'] . ')';
    }
    
    echo "</li>\n";
    echo '  <li><strong>Conditionnelle</strong>&nbsp: ' . $condi;
    
    if ( $condi == 'Oui' )
    {
        echo ' (' . $row['intitule2'] . ', ' . $row['intitule3'] . ')';
    }
    
    echo "</li>\n";
    echo "</ul>\n";
}
else
{
    header ( 'Location: liste_eleves.php' );
    die();
}

include_once ( 'include/footer.php' );

?>
