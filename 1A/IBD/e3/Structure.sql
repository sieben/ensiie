--
-- PostgreSQL database dump
--

-- Started on 2010-05-03 17:13:41 CEST

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 1543 (class 1259 OID 16387)
-- Dependencies: 6
-- Name: charge; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE charge (
    idcharge integer NOT NULL,
    nomcharge text NOT NULL,
    nbhrcharge integer NOT NULL,
    idens integer NOT NULL
);


--
-- TOC entry 1903 (class 0 OID 0)
-- Dependencies: 1543
-- Name: TABLE charge; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE charge IS 'Table chargé de répertorier les différentes fonctions du personnel de l''école.';


--
-- TOC entry 1904 (class 0 OID 0)
-- Dependencies: 1543
-- Name: COLUMN charge.idens; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN charge.idens IS 'Identifiant d''un enseignant.';


--
-- TOC entry 1544 (class 1259 OID 16393)
-- Dependencies: 6 1543
-- Name: charge_idcharge_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE charge_idcharge_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 1905 (class 0 OID 0)
-- Dependencies: 1544
-- Name: charge_idcharge_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE charge_idcharge_seq OWNED BY charge.idcharge;


--
-- TOC entry 1545 (class 1259 OID 16395)
-- Dependencies: 6
-- Name: choixcoursmagistraux; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE choixcoursmagistraux (
    idensmt integer NOT NULL,
    idens integer NOT NULL,
    datechoix date NOT NULL,
    nbhrcm real NOT NULL
);


--
-- TOC entry 1906 (class 0 OID 0)
-- Dependencies: 1545
-- Name: TABLE choixcoursmagistraux; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE choixcoursmagistraux IS 'Donnée de l''énoncé';


--
-- TOC entry 1546 (class 1259 OID 16398)
-- Dependencies: 6
-- Name: choixtd; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE choixtd (
    idensmt integer NOT NULL,
    idens integer NOT NULL,
    nbgroupestd integer NOT NULL,
    datechoix date NOT NULL,
    nbhrpargroupe real NOT NULL
);


--
-- TOC entry 1907 (class 0 OID 0)
-- Dependencies: 1546
-- Name: TABLE choixtd; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE choixtd IS 'Donnée de l''énoncé';


--
-- TOC entry 1547 (class 1259 OID 16401)
-- Dependencies: 6
-- Name: choixtp; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE choixtp (
    idensmt integer NOT NULL,
    idens integer NOT NULL,
    nbgroupestp integer NOT NULL,
    datechoix date NOT NULL,
    nbhrpargroupe real NOT NULL
);


--
-- TOC entry 1908 (class 0 OID 0)
-- Dependencies: 1547
-- Name: TABLE choixtp; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE choixtp IS 'Donnée de l''énoncé';


--
-- TOC entry 1548 (class 1259 OID 16404)
-- Dependencies: 6
-- Name: conditionnelle; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE conditionnelle (
    ideleve integer NOT NULL,
    idue1manquee integer NOT NULL,
    idue2manquee integer NOT NULL
);


--
-- TOC entry 1909 (class 0 OID 0)
-- Dependencies: 1548
-- Name: TABLE conditionnelle; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE conditionnelle IS 'Si un élève est passé en conditionnelle (Au plus 2 ue non validées) alors il est dans cette table.';


--
-- TOC entry 1569 (class 1259 OID 16533)
-- Dependencies: 6
-- Name: droits; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE droits (
    idens integer NOT NULL,
    idensmt integer NOT NULL
);


--
-- TOC entry 1910 (class 0 OID 0)
-- Dependencies: 1569
-- Name: TABLE droits; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE droits IS 'Table contenant les droits d''accès aux résultats d''un enseignement';


--
-- TOC entry 1911 (class 0 OID 0)
-- Dependencies: 1569
-- Name: COLUMN droits.idens; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN droits.idens IS 'Identifiant d''un enseignant';


--
-- TOC entry 1912 (class 0 OID 0)
-- Dependencies: 1569
-- Name: COLUMN droits.idensmt; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN droits.idensmt IS 'Identifiant d''un enseignement.';


--
-- TOC entry 1549 (class 1259 OID 16407)
-- Dependencies: 6
-- Name: eleve; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE eleve (
    ideleve integer NOT NULL,
    login text NOT NULL,
    motdepasse text NOT NULL,
    promo integer NOT NULL,
    groupe integer NOT NULL,
    sousgroupe integer NOT NULL,
    idmajeure integer NOT NULL,
    grade integer NOT NULL,
    entree integer NOT NULL
);


--
-- TOC entry 1913 (class 0 OID 0)
-- Dependencies: 1549
-- Name: TABLE eleve; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE eleve IS 'Table qui contient les informations relatives aux élèves.';


--
-- TOC entry 1914 (class 0 OID 0)
-- Dependencies: 1549
-- Name: COLUMN eleve.promo; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN eleve.promo IS 'promo est incrémenté si l''élève redouble.';


--
-- TOC entry 1915 (class 0 OID 0)
-- Dependencies: 1549
-- Name: COLUMN eleve.idmajeure; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN eleve.idmajeure IS 'Identifiant de la majeure choisie par l''élève.';


--
-- TOC entry 1916 (class 0 OID 0)
-- Dependencies: 1549
-- Name: COLUMN eleve.grade; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN eleve.grade IS '1A, 2A, 3A';


--
-- TOC entry 1917 (class 0 OID 0)
-- Dependencies: 1549
-- Name: COLUMN eleve.entree; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN eleve.entree IS 'Année d''entrée à l''ENSIIE (Invariant)';


--
-- TOC entry 1550 (class 1259 OID 16413)
-- Dependencies: 6 1549
-- Name: eleve_ideleve_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE eleve_ideleve_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 1918 (class 0 OID 0)
-- Dependencies: 1550
-- Name: eleve_ideleve_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE eleve_ideleve_seq OWNED BY eleve.ideleve;


--
-- TOC entry 1551 (class 1259 OID 16415)
-- Dependencies: 6
-- Name: eleveensmt; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE eleveensmt (
    ideleve integer NOT NULL,
    idensmt integer NOT NULL,
    dateenvoi date NOT NULL
);


--
-- TOC entry 1552 (class 1259 OID 16418)
-- Dependencies: 6
-- Name: enseignant; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE enseignant (
    idens integer NOT NULL,
    nomens text NOT NULL,
    prenomsens text NOT NULL,
    nbhrreelles real NOT NULL,
    courriel text NOT NULL,
    reliquat real NOT NULL,
    salariepublic integer NOT NULL,
    login text NOT NULL,
    motdepasse text NOT NULL,
    autresheures real NOT NULL
);


--
-- TOC entry 1919 (class 0 OID 0)
-- Dependencies: 1552
-- Name: TABLE enseignant; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE enseignant IS 'Donnée de l''énoncé';


--
-- TOC entry 1553 (class 1259 OID 16424)
-- Dependencies: 1552 6
-- Name: enseignant_idens_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE enseignant_idens_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 1920 (class 0 OID 0)
-- Dependencies: 1553
-- Name: enseignant_idens_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE enseignant_idens_seq OWNED BY enseignant.idens;


--
-- TOC entry 1554 (class 1259 OID 16426)
-- Dependencies: 6
-- Name: enseignement; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE enseignement (
    idensmt integer NOT NULL,
    idue integer NOT NULL,
    idoption integer NOT NULL,
    nbhrcmtotal real NOT NULL,
    code text NOT NULL,
    nbhrtdtotal real NOT NULL,
    intitule text NOT NULL,
    datedebut date NOT NULL,
    datefin date NOT NULL
);


--
-- TOC entry 1921 (class 0 OID 0)
-- Dependencies: 1554
-- Name: TABLE enseignement; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE enseignement IS 'Donnée de l''énoncé';


--
-- TOC entry 1555 (class 1259 OID 16432)
-- Dependencies: 1554 6
-- Name: enseignement_idensmt_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE enseignement_idensmt_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 1922 (class 0 OID 0)
-- Dependencies: 1555
-- Name: enseignement_idensmt_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE enseignement_idensmt_seq OWNED BY enseignement.idensmt;


--
-- TOC entry 1556 (class 1259 OID 16434)
-- Dependencies: 6
-- Name: formation; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE formation (
    idformation integer NOT NULL,
    nomformation text NOT NULL
);


--
-- TOC entry 1923 (class 0 OID 0)
-- Dependencies: 1556
-- Name: TABLE formation; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE formation IS 'Donnée de l''énoncé';


--
-- TOC entry 1557 (class 1259 OID 16440)
-- Dependencies: 1556 6
-- Name: formation_idformation_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE formation_idformation_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 1924 (class 0 OID 0)
-- Dependencies: 1557
-- Name: formation_idformation_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE formation_idformation_seq OWNED BY formation.idformation;


--
-- TOC entry 1558 (class 1259 OID 16442)
-- Dependencies: 6
-- Name: optionoutc; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE optionoutc (
    idoption integer NOT NULL,
    idformation integer NOT NULL,
    nomoption text NOT NULL,
    nbgroupestd integer NOT NULL,
    promo integer NOT NULL
);


--
-- TOC entry 1925 (class 0 OID 0)
-- Dependencies: 1558
-- Name: TABLE optionoutc; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE optionoutc IS 'Donnée de l''énoncé';


--
-- TOC entry 1559 (class 1259 OID 16448)
-- Dependencies: 6 1558
-- Name: optionoutc_idoption_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE optionoutc_idoption_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 1926 (class 0 OID 0)
-- Dependencies: 1559
-- Name: optionoutc_idoption_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE optionoutc_idoption_seq OWNED BY optionoutc.idoption;


--
-- TOC entry 1560 (class 1259 OID 16450)
-- Dependencies: 6
-- Name: questionnaire; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE questionnaire (
    idquestionnaire integer NOT NULL,
    idensmt integer NOT NULL,
    dateenvoi date NOT NULL
);


--
-- TOC entry 1927 (class 0 OID 0)
-- Dependencies: 1560
-- Name: TABLE questionnaire; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE questionnaire IS 'Stockage d''un identifiant de questionnaire envoyé par un élève.';


--
-- TOC entry 1561 (class 1259 OID 16453)
-- Dependencies: 6
-- Name: questions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE questions (
    idquestion integer NOT NULL,
    intitule text NOT NULL,
    typequestion character varying(10) NOT NULL,
    idsection integer NOT NULL,
    ordrequestion integer NOT NULL
);


--
-- TOC entry 1928 (class 0 OID 0)
-- Dependencies: 1561
-- Name: TABLE questions; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE questions IS 'Contenu du questionnaire.';


--
-- TOC entry 1562 (class 1259 OID 16459)
-- Dependencies: 6 1561
-- Name: questions_idquestion_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE questions_idquestion_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 1929 (class 0 OID 0)
-- Dependencies: 1562
-- Name: questions_idquestion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE questions_idquestion_seq OWNED BY questions.idquestion;


--
-- TOC entry 1563 (class 1259 OID 16461)
-- Dependencies: 6
-- Name: redoublement; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE redoublement (
    ideleve integer NOT NULL,
    iduereussie integer NOT NULL
);


--
-- TOC entry 1930 (class 0 OID 0)
-- Dependencies: 1563
-- Name: TABLE redoublement; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE redoublement IS 'Si un élève redouble son année cela signifie qu''il est enregistré dans cette relation.
On ne stocke que les ue qu''il a d''ores et déjà réussies.';


--
-- TOC entry 1564 (class 1259 OID 16464)
-- Dependencies: 6
-- Name: reponses; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE reponses (
    idquestion integer NOT NULL,
    valeur text NOT NULL,
    idquestionnaire integer NOT NULL
);


--
-- TOC entry 1931 (class 0 OID 0)
-- Dependencies: 1564
-- Name: TABLE reponses; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE reponses IS 'Table contenant l''ensemble des réponses fournies par les élèves.';


--
-- TOC entry 1565 (class 1259 OID 16467)
-- Dependencies: 6
-- Name: sections; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE sections (
    idsection integer NOT NULL,
    nom text NOT NULL
);


--
-- TOC entry 1932 (class 0 OID 0)
-- Dependencies: 1565
-- Name: TABLE sections; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE sections IS 'Informations sur les différentes sections du questionnaire.';


--
-- TOC entry 1566 (class 1259 OID 16473)
-- Dependencies: 1565 6
-- Name: sections_idsection_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE sections_idsection_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 1933 (class 0 OID 0)
-- Dependencies: 1566
-- Name: sections_idsection_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE sections_idsection_seq OWNED BY sections.idsection;


--
-- TOC entry 1567 (class 1259 OID 16475)
-- Dependencies: 6
-- Name: ue; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE ue (
    idue integer NOT NULL,
    nomue text NOT NULL,
    idcharge integer NOT NULL
);


--
-- TOC entry 1934 (class 0 OID 0)
-- Dependencies: 1567
-- Name: TABLE ue; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE ue IS 'Donnée de l''énoncé';


--
-- TOC entry 1568 (class 1259 OID 16481)
-- Dependencies: 1567 6
-- Name: ue_idue_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE ue_idue_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- TOC entry 1935 (class 0 OID 0)
-- Dependencies: 1568
-- Name: ue_idue_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE ue_idue_seq OWNED BY ue.idue;


--
-- TOC entry 1847 (class 2604 OID 16483)
-- Dependencies: 1544 1543
-- Name: idcharge; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE charge ALTER COLUMN idcharge SET DEFAULT nextval('charge_idcharge_seq'::regclass);


--
-- TOC entry 1848 (class 2604 OID 16549)
-- Dependencies: 1550 1549
-- Name: ideleve; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE eleve ALTER COLUMN ideleve SET DEFAULT nextval('eleve_ideleve_seq'::regclass);


--
-- TOC entry 1849 (class 2604 OID 16485)
-- Dependencies: 1553 1552
-- Name: idens; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE enseignant ALTER COLUMN idens SET DEFAULT nextval('enseignant_idens_seq'::regclass);


--
-- TOC entry 1850 (class 2604 OID 16486)
-- Dependencies: 1555 1554
-- Name: idensmt; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE enseignement ALTER COLUMN idensmt SET DEFAULT nextval('enseignement_idensmt_seq'::regclass);


--
-- TOC entry 1851 (class 2604 OID 16487)
-- Dependencies: 1557 1556
-- Name: idformation; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE formation ALTER COLUMN idformation SET DEFAULT nextval('formation_idformation_seq'::regclass);


--
-- TOC entry 1852 (class 2604 OID 16488)
-- Dependencies: 1559 1558
-- Name: idoption; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE optionoutc ALTER COLUMN idoption SET DEFAULT nextval('optionoutc_idoption_seq'::regclass);


--
-- TOC entry 1853 (class 2604 OID 16489)
-- Dependencies: 1562 1561
-- Name: idquestion; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE questions ALTER COLUMN idquestion SET DEFAULT nextval('questions_idquestion_seq'::regclass);


--
-- TOC entry 1854 (class 2604 OID 16490)
-- Dependencies: 1566 1565
-- Name: idsection; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE sections ALTER COLUMN idsection SET DEFAULT nextval('sections_idsection_seq'::regclass);


--
-- TOC entry 1855 (class 2604 OID 16491)
-- Dependencies: 1568 1567
-- Name: idue; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ue ALTER COLUMN idue SET DEFAULT nextval('ue_idue_seq'::regclass);


--
-- TOC entry 1857 (class 2606 OID 16493)
-- Dependencies: 1543 1543
-- Name: charge_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY charge
    ADD CONSTRAINT charge_pkey PRIMARY KEY (idcharge);


--
-- TOC entry 1859 (class 2606 OID 16495)
-- Dependencies: 1545 1545 1545
-- Name: choixcoursmagistraux_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY choixcoursmagistraux
    ADD CONSTRAINT choixcoursmagistraux_pkey PRIMARY KEY (idensmt, idens);


--
-- TOC entry 1861 (class 2606 OID 16497)
-- Dependencies: 1546 1546 1546
-- Name: choixtd_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY choixtd
    ADD CONSTRAINT choixtd_pkey PRIMARY KEY (idensmt, idens);


--
-- TOC entry 1863 (class 2606 OID 16499)
-- Dependencies: 1547 1547 1547
-- Name: choixtp_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY choixtp
    ADD CONSTRAINT choixtp_pkey PRIMARY KEY (idensmt, idens);


--
-- TOC entry 1865 (class 2606 OID 16501)
-- Dependencies: 1548 1548
-- Name: conditionnelle_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY conditionnelle
    ADD CONSTRAINT conditionnelle_pkey PRIMARY KEY (ideleve);


--
-- TOC entry 1897 (class 2606 OID 16537)
-- Dependencies: 1569 1569 1569
-- Name: droits_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY droits
    ADD CONSTRAINT droits_pkey PRIMARY KEY (idens, idensmt);


--
-- TOC entry 1867 (class 2606 OID 16551)
-- Dependencies: 1549 1549
-- Name: eleve_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY eleve
    ADD CONSTRAINT eleve_pkey PRIMARY KEY (ideleve);


--
-- TOC entry 1869 (class 2606 OID 16505)
-- Dependencies: 1549 1549
-- Name: eleve_unique; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY eleve
    ADD CONSTRAINT eleve_unique UNIQUE (login);


--
-- TOC entry 1871 (class 2606 OID 16507)
-- Dependencies: 1551 1551 1551
-- Name: eleveensmt_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY eleveensmt
    ADD CONSTRAINT eleveensmt_pkey PRIMARY KEY (ideleve, idensmt);


--
-- TOC entry 1873 (class 2606 OID 16509)
-- Dependencies: 1552 1552
-- Name: enseignant_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY enseignant
    ADD CONSTRAINT enseignant_pkey PRIMARY KEY (idens);


--
-- TOC entry 1877 (class 2606 OID 16511)
-- Dependencies: 1554 1554
-- Name: enseignement_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY enseignement
    ADD CONSTRAINT enseignement_pkey PRIMARY KEY (idensmt);


--
-- TOC entry 1879 (class 2606 OID 16513)
-- Dependencies: 1556 1556
-- Name: formation_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY formation
    ADD CONSTRAINT formation_pkey PRIMARY KEY (idformation);


--
-- TOC entry 1881 (class 2606 OID 16515)
-- Dependencies: 1558 1558
-- Name: optionoutc_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY optionoutc
    ADD CONSTRAINT optionoutc_pkey PRIMARY KEY (idoption);


--
-- TOC entry 1875 (class 2606 OID 16517)
-- Dependencies: 1552 1552
-- Name: prof_unique; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY enseignant
    ADD CONSTRAINT prof_unique UNIQUE (login);


--
-- TOC entry 1885 (class 2606 OID 16519)
-- Dependencies: 1561 1561 1561
-- Name: question_unique; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY questions
    ADD CONSTRAINT question_unique UNIQUE (idsection, ordrequestion);


--
-- TOC entry 1883 (class 2606 OID 16521)
-- Dependencies: 1560 1560
-- Name: questionnaire_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY questionnaire
    ADD CONSTRAINT questionnaire_pkey PRIMARY KEY (idquestionnaire);


--
-- TOC entry 1887 (class 2606 OID 16523)
-- Dependencies: 1561 1561
-- Name: questions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY questions
    ADD CONSTRAINT questions_pkey PRIMARY KEY (idquestion);


--
-- TOC entry 1889 (class 2606 OID 16525)
-- Dependencies: 1563 1563 1563
-- Name: redoublement_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY redoublement
    ADD CONSTRAINT redoublement_pkey PRIMARY KEY (ideleve, iduereussie);


--
-- TOC entry 1891 (class 2606 OID 16527)
-- Dependencies: 1564 1564 1564
-- Name: reponses_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY reponses
    ADD CONSTRAINT reponses_pkey PRIMARY KEY (idquestion, idquestionnaire);


--
-- TOC entry 1893 (class 2606 OID 16529)
-- Dependencies: 1565 1565
-- Name: sections_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY sections
    ADD CONSTRAINT sections_pkey PRIMARY KEY (idsection);


--
-- TOC entry 1895 (class 2606 OID 16531)
-- Dependencies: 1567 1567
-- Name: ue_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY ue
    ADD CONSTRAINT ue_pkey PRIMARY KEY (idue);


--
-- TOC entry 1902 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2010-05-03 17:13:41 CEST

--
-- PostgreSQL database dump complete
--

