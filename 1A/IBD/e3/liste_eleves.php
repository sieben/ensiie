<?php

require_once ( 'include/main.php' );

// Seul l'administrateur peut voir cette page
if ( !is_admin() )
{
    header ( 'Location: login.php' );
    die();
}

$files_css[] = 'jquery-ui-1.8.custom.css';
$files_js[] = 'jquery-ui-1.8.custom.min.js';

$titre = 'Liste des élèves';

include_once ( 'include/header.php' );


// Liste des promos
$sql = 'SELECT DISTINCT(promo)
        FROM eleve
        ORDER BY promo;';

$req = db_query ( $db_link , $sql );

if ( pg_num_rows ( $req ) > 0 )
{
    $i = 0;

    // Affichage du script pour utiliser une barre d'onglets
?>

<script type="text/javascript">

$(function()
{
    $('#tabs').tabs();
});

</script>

<?php

    echo "<div id=\"tabs\">\n\n<ul>\n";

    // Liste des promos
    while ( $row = pg_fetch_assoc ( $req ) )
    {
        $i++;
        echo '  <li><a href="#tabs-' . $i . '">' . $row['promo'] . "</a></li>\n";
    }

    echo "</ul>\n\n";
}


// Liste des élèves
$sql = 'SELECT ideleve, login, promo
        FROM eleve
        ORDER BY promo, login;';

$req = db_query ( $db_link , $sql );

if ( pg_num_rows ( $req ) > 0 )
{
    $promo = -1;
    $i = 0;

    // Affichage de la liste des élèves
    while ( $row = pg_fetch_assoc ( $req ) )
    {
        // Affichage d'une autre promo
        if ( $promo != $row['promo'] )
        {
            $i++;

            if ( $promo != -1 ) echo "</ul>\n</div>\n\n";
            echo '<div id="tabs-' . $i . '">' . "\n<ul>\n";

            $promo = $row['promo'];
        }

        echo '  <li><a href="eleve.php?eleve=' . $row['ideleve'] . '">';
        echo $row['login'] . "</a></li>\n";
    }

    echo "</ul>\n</div>\n\n</div>\n\n";
}
// Aucun élève
else
{
    echo "<p>Il n'y a aucun élève pour le moment.</p>\n";
}

include_once ( 'include/footer.php' );

?>