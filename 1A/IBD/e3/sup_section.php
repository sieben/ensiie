<?php

require_once ( 'include/main.php' );

// Seul l'administrateur peut voit cette page
if ( !is_admin() )
{
    header ( 'Location: index.php' );
    die();
}

// Identifiant de la section
if ( !isset ( $_GET['id'] ) || !is_numeric ( $_GET['id'] ) )
{
    header ( 'Location: questionnaire.php' );
    die();
}


$files_css[] = 'form.css';

$titre = 'Supprimer une section';

include_once ( 'include/header.php' );

$sql = 'SELECT nom
        FROM sections
        WHERE idsection = ' . db_protect ( $_GET['id'] ) . ';';

$req = pg_query ( $db_link , $sql , __FILE__ , __LINE__ );

if ( pg_num_rows ( $req ) === 0 )
{
    header ( 'Location: questionnaire.php' );
    die();
}

// Traitement du formulaire
if ( isset ( $_POST['submit'] ) )
{
    // Suppression des réponses
    $sql = 'DELETE FROM reponses
            WHERE idquestion
            IN ( SELECT idquestion
                 FROM questions
                 WHERE idsection = ' . db_protect ( $_GET['id'] ) . ' );';

    pg_query ( $db_link , $sql , __FILE__ , __LINE__ );

    // Suppression des questions
    $sql = 'DELETE FROM questions
            WHERE idsection = ' . db_protect ( $_GET['id'] ) . ' );';

    pg_query ( $db_link , $sql , __FILE__ , __LINE__ );

    // Suppression de la section
    $sql = 'DELETE FROM sections
            WHERE idsection = ' . db_protect ( $_GET['id'] ) . ';';

    pg_query ( $db_link , $sql , __FILE__ , __LINE__ );

    // On remonte les sections suivantes
    $sql = 'UPDATE sections
            SET idsection = idsection - 1
            WHERE idsection > ' . db_protect ( $_GET['id'] ) . ';';

    pg_query ( $db_link , $sql , __FILE__ , __LINE__ );

    // Message d'information pour la page suivante
    $_SESSION['infos'] = 'La section et les questions qu\'elle contient ont été supprimée.';
    $_SESSION['infos_dialog'] = true;

    header ( 'Location: questionnaire.php' );
    die();
}

$row = pg_fetch_assoc ( $req );

// Affichage de la page
echo '<p>Voulez-vous supprimer la section <quote>' . $row['nom'] . "</quote> ?</p>\n";
echo '<form action="sup_section.php?id=' . $_GET['id'] . '" method="post">';
echo '<p class="form_submit"><input type="submit" name="submit" value="Oui" /> <input type="button" class="form_back" value="Non" /></p>';
echo "</form>\n";

include_once ( 'include/footer.php' );

?>
