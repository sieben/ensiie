<?php

require_once ( 'include/main.php' );

// L'utilisateur n'est pas connecté ou n'est pas administrateur
if ( !is_admin() )
{
    header ( 'Location: login.php' );
    die();
}

// On n'a pas précisé l'identifiant de la section
if ( !isset ( $_GET['id'] ) || !is_numeric ( $_GET['id'] ) )
{
    header ( 'Location: questionnaire.php' );
    die();
}

// On cherche la section dans la base
$sql = 'SELECT nom
        FROM sections
        WHERE idsection = ' . db_protect ( $_GET['id'] ) . ';';

$req = db_query ( $db_link , $sql );

// La section n'existe pas
if ( pg_num_rows ( $req ) === 0 )
{
    header ( 'Location: questionnaire.php' );
    die();
}

$row = pg_fetch_assoc ( $req );

$files_css[] = 'form.css';

$titre = 'Modifier une section';

include_once ( 'include/header.php' );


// Traitement du formulaire
if ( isset ( $_POST['submit'] ) )
{
    if ( !isset ( $_POST['nom'] ) || empty ( $_POST['nom'] ) )
    {
        echo '<p class="erreur">Vous devez donner un nom à chaque section.</p>';
    }
    else
    {
        $sql = "UPDATE sections
                SET nom = '" . db_protect ( $_POST['nom'] ) . "'
                WHERE idsection = " . db_protect ( $_GET['id'] ) . ';';

        $req = db_query ( $db_link , $sql );

        header ( 'Location: questionnaire.php' );
        die();
    }
}

// Affichage du formulaire
echo '<form action="mod_section.php?id=' . $_GET['id'] . '" method="post">';
echo "\n<fieldset>\n<legend>Modifier une section</legend>\n";

echo '<p class="form_line"><label for="form_row_intitule">Nom&nbsp;:</label> ';
echo '<input type="text" name="nom" id="form_row_nom" maxlength="150" size="40" value="';
echo ( isset ( $_POST['nom'] ) ) ? $_POST['nom'] : $row['nom'];
echo '" />' . "</p>\n";

echo "</fieldset>\n";
echo '<p class="form_submit"><input type="submit" name="submit" value="Valider" /><input type="button" class="form_back" value="Annuler" /></p>';
echo "\n</form>\n";

include_once ( 'include/footer.php' );

?>