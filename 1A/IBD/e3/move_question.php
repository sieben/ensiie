<?php

require_once ( 'include/main.php' );

// L'utilisateur n'est pas connecté ou n'est pas administrateur
if ( !is_admin() )
{
    header ( 'Location: login.php' );
    die();
}

// On n'a pas précisé l'identifiant de la question
if ( !isset ( $_GET['id'] ) || !is_numeric ( $_GET['id'] ) || ( !isset ( $_GET['down'] ) && !isset ( $_GET['up'] ) ) )
{
    header ( 'Location: index.php' );
    die();
}

$id = db_protect ( $_GET['id'] );

$sql = 'SELECT ordrequestion, idsection
        FROM questions
        WHERE idquestion= ' . $id . ';';

$req = db_query ( $db_link , $sql );

if ( pg_num_rows ( $req ) > 0 )
{
    $row = pg_fetch_assoc ( $req );

    // On déplace la question vers le haut
    if ( isset ( $_GET['up'] ) && $row['ordrequestion'] > 1 )
    {
        // On déplace la question précédente à la fin
        $sql = 'UPDATE questions
                SET ordrequestion = 99999
                WHERE ordrequestion = ' . ( $row['ordrequestion'] - 1 ) . '
                  AND idsection = ' . $row['idsection'] . ';';

        $req = db_query ( $db_link , $sql );

        if ( pg_affected_rows ( $req ) > 0 )
        {
            // On décale la question
            $sql = 'UPDATE questions
                    SET ordrequestion = ' . ( $row['ordrequestion'] - 1 ) . '
                    WHERE idquestion = ' . $id . ';';

            db_query ( $db_link , $sql );

            // On modifie l'ordre de la dernière question
            $sql = 'UPDATE questions
                    SET ordrequestion = ' . $row['ordrequestion'] . '
                    WHERE ordrequestion = 99999
                      AND idsection = ' . $row['idsection'] . ';';

            db_query ( $db_link , $sql );
        }
    }
    // On déplace la question vers le bas
    else
    {
        // On déplace la question suivante à la fin
        $sql = 'UPDATE questions
                SET ordrequestion = 99999
                WHERE ordrequestion = ' . ( $row['ordrequestion'] + 1 ) . '
                  AND idsection = ' . $row['idsection'] . ';';

        $req = db_query ( $db_link , $sql );

        if ( pg_affected_rows ( $req ) > 0 )
        {
            // On décale la question
            $sql = 'UPDATE questions
                    SET ordrequestion = ' . ( $row['ordrequestion'] + 1 ) . '
                    WHERE idquestion = ' . $id . ';';

            db_query ( $db_link , $sql );

            // On modifie l'ordre de la dernière question
            $sql = 'UPDATE questions
                    SET ordrequestion = ' . $row['ordrequestion'] . '
                    WHERE ordrequestion = 99999
                      AND idsection = ' . $row['idsection'] . ';';

            db_query ( $db_link , $sql );
        }
    }
}

header ( 'Location: questionnaire.php' );
die();

?>