<?php

require_once ( 'include/main.php' );

// L'utilisateur est déjà connecté
if ( is_connected() )
{
    header ( 'Location: index.php' );
    die();
}

$files_css[] = 'form.css';

$titre = 'Connexion';

include_once ( 'include/header.php' );

// Traitement du formulaire
if ( isset ( $_POST['submit'] )
  && isset ( $_POST['ulogin'] )
  && isset ( $_POST['password'] )
  && isset ( $_POST['type'] ) )
{
    if ( empty ( $_POST['ulogin'] ) )
    {
        echo '<p class="erreur">Vous devez donner un nom d\'utilisateur pour vous connecter.</p>';
    }
    else if ( empty ( $_POST['password'] ) )
    {
        echo '<p class="erreur">Vous devez donner un mot de passe pour vous connecter.</p>';
    }
    else if ( $_POST['type'] == 'prof' )
    {
        // Vérification du mot de passe
        $sql = "SELECT idens
                FROM enseignant
                WHERE login = '" . db_protect ( $_POST['ulogin'] ) . "'
                  AND motdepasse = '" . db_protect ( $_POST['password'] ) ."';";

        $req = db_query ( $db_link , $sql );

        if ( pg_num_rows ( $req ) === 0 )
        {
            echo '<p class="erreur">Le login ou le mot de passe est incorrect.</p>';
        }
        else
        {
            $row = pg_fetch_assoc ( $req );

            $_SESSION['login'] = $_POST['ulogin'];
            $_SESSION['promo'] = 0;
            $_SESSION['uid'] = $row['idens'];

            // On cherche si l'enseignant est le directeur des études
            $sql = 'SELECT idcharge
                    FROM charge
                    WHERE idens = ' . $row['idens'] . "
                      AND nomcharge = 'Directeur des etudes';";

            $req = db_query ( $db_link , $sql );

            if ( pg_num_rows ( $req ) > 0 )
            {
                $_SESSION['admin'] = true;
            }

            // Message d'information pour la page suivante
            $_SESSION['infos'] = 'Bienvenue ' . $_SESSION['login'] . ' !';

            header ( 'Location: index.php' );
            die();
        }
    }
    else if ( $_POST['type'] == 'eleve' )
    {
        // Vérification du mot de passe
        $sql = "SELECT ideleve, promo as pr, groupe as gr
                FROM eleve
                WHERE login = '" . db_protect ( $_POST['ulogin'] ) . "'
                  AND motdepasse = '" . db_protect ( $_POST['password'] ) . "';";

        $req = db_query ( $db_link , $sql );

        if ( pg_num_rows ( $req ) === 0 )
        {
            echo '<p class="erreur">Le login ou le mot de passe est incorrect.</p>';
        }
        else
        {
            $row = pg_fetch_assoc ( $req );

            $_SESSION['login'] = $_POST['ulogin'];
            $_SESSION['promo'] = $row['pr'];
            $_SESSION['uid'] = $row['ideleve'];
            $_SESSION['groupe'] = $row['gr'];

            // Message d'information pour la page suivante
            $_SESSION['infos'] = 'Bienvenue ' . $_SESSION['login'] . ' !';

            header ( 'Location: index.php' );
            die();
        }
    }
    else
    {
        echo '<p class="erreur">Vous devez indiquer si vous êtes un élève ou un professeur.</p>';
    }
}


// Formulaire de connexion
?>

<form action="login.php" method="post">

    <fieldset><legend>Connexion</legend>

        <p class="form_line">
            <input type="radio" name="type" id="form_type_prof" value="prof" />
            <label for="form_type_prof">Professeur</label>
            <input type="radio" name="type" id="form_type_eleve" 
                   value="eleve" checked="checked" />
            <label for="form_type_eleve">Élève</label>
        </p>

        <br/>

        <p class="form_line">
            <label for="form_login" class="form_label">
                Nom d'utilisateur&nbsp;:
            </label>
            <input type="text" name="ulogin" id="form_login"
                   maxlength="50" size="30" value="<?php
                   if ( isset ( $_POST['ulogin'] ) ) echo $_POST['ulogin'];
                   ?>" /></p>

        <p class="form_line"><label for="form_password" class="form_label">Mot de passe&nbsp;:</label>
            <input type="password" name="password" id="form_password" maxlength="50" size="30" value="" /></p>

    </fieldset>

    <p class="form_submit"><input type="submit" name="submit" id="form_submit" value="Connexion" /></p>

</form>

<?php

include_once ( 'include/footer.php' );

?>
