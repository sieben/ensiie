<?php

require_once ( 'include/main.php' );

// Si l'utilisateur n'est pas connecté, ou que ce n'est pas un enseignant, on le redirige vers la page de connexion
if ( !is_ens() )
{
    header ( 'Location: login.php' );
    die();
}


$titre = 'Liste des enseignements';

include_once ( 'include/header.php' );


// Enseignements visibles par l'enseignant
//...


include_once ( 'include/footer.php' );

?>