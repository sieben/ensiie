<?php

require_once ( 'include/main.php' );

// Seul l'administrateur peut voir cette page
if ( !is_admin() )
{
    header ( 'Location: index.php' );
    die();
}

$titre = 'Liste des enseignants';

include_once ( 'include/header.php' );


$sql = 'SELECT idens, nomens, prenomsens
        FROM enseignant
        ORDER BY nomens;';

$req = db_query ( $db_link , $sql );

if ( pg_num_rows ( $req ) > 0 )
{
    echo "<ul>\n";

    // Affichage de la liste des enseignants
    while ( $row = pg_fetch_assoc ( $req ) )
    {
        echo '  <li><a href="enseignant.php?ens=' . $row['idens'] . '">';
        echo $row['prenomsens'] . ' ' . $row['nomens']. "</a></li>\n";
    }

    echo "</ul>\n";
}
// Aucun enseignant
else
{
    echo "<p>Il n'y a aucun enseignant pour le moment.</p>\n";
}

include_once ( 'include/footer.php' );

?>