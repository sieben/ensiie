<?php

require_once ( 'include/main.php' );
require_once ( 'include/camembert.php');

$camembert = new camembert();

$type = ( isset ( $_GET['type'] ) ? $_GET['type'] : 'note' );

if ( $type == 'note' )
{
    $nbr_0 = ( isset ( $_GET['n0'] ) && is_numeric ( $_GET['n0'] ) ) ? $_GET['n0'] : 0;
    $nbr_1 = ( isset ( $_GET['n1'] ) && is_numeric ( $_GET['n1'] ) ) ? $_GET['n1'] : 0;
    $nbr_2 = ( isset ( $_GET['n2'] ) && is_numeric ( $_GET['n2'] ) ) ? $_GET['n2'] : 0;
    $nbr_3 = ( isset ( $_GET['n3'] ) && is_numeric ( $_GET['n3'] ) ) ? $_GET['n3'] : 0;
    $nbr_4 = ( isset ( $_GET['n4'] ) && is_numeric ( $_GET['n4'] ) ) ? $_GET['n4'] : 0;
    $nbr_5 = ( isset ( $_GET['n5'] ) && is_numeric ( $_GET['n5'] ) ) ? $_GET['n5'] : 0;

    // Pour éviter la division par 0
    if ( $nbr_0 + $nbr_1 + $nbr_2 + $nbr_3 + $nbr_4 + $nbr_5 == 0 )
    {
        $nbr_0 = 1;
    }

    // Ajout des sections
    $camembert->add_tab ( $nbr_0 , 'Ne se prononce pas' );
    $camembert->add_tab ( $nbr_1 , 'Tres mecontent' );
    $camembert->add_tab ( $nbr_2 , 'Mecontent' );
    $camembert->add_tab ( $nbr_3 , 'Neutre' );
    $camembert->add_tab ( $nbr_4 , 'Satisfait' );
    $camembert->add_tab ( $nbr_5 , 'Tres satisfait' );
}
else if ( $type == 'nbr' )
{
    $total = 0;

    // On gère au maximum 100 valeurs
    for ( $i = 0 ; $i < 99 ; $i++ )
    {
        if ( isset ( $_GET['n' . $i] ) && is_numeric ( $_GET['n' . $i] ) )
        {
            $camembert->add_tab ( $_GET['n' . $i] , $i );
            $total += $_GET['n' . $i];
        }
    }

    if ( $total == 0 )
    {
        $camembert->add_tab ( 0 , 1 );
    }
}

$camembert->stat2png();

?>