<?php

require_once ( 'include/main.php' );

// On vérifie que l'utilisateur est connecté et que ce n'est pas un élève
if ( !is_ens() && !is_admin() )
{
    header ( 'Location: login.php' );
    die();
}

// Identifiant de l'enseignement
if ( isset ( $_GET['ensmt'] ) && is_numeric ( $_GET['ensmt'] ) )
{
    // Informations de l'enseignement
    $sql = 'SELECT code, intitule, nomue
            FROM enseignement
            NATURAL JOIN ue
            WHERE idensmt = ' . db_protect ( $_GET['ensmt'] ) . ';';

    $req = db_query ( $db_link , $sql , __FILE__ , __LINE__ );

    // L'enseignement n'existe pas
    if ( pg_num_rows ( $req ) == 0 )
    {
        header ( 'Location: index.php' );
        die();
    }

    $row = pg_fetch_assoc ( $req );

    // On vérifie que l'enseignant a le droit d'afficher cet enseignement
    if ( !is_admin() && !ensmt_enseignant ( $_GET['ensmt'] , $_SESSION['uid'] ) )
    {
        header ( 'Location: index.php' );
        die();
    }

    $titre = $row['intitule'];
}
// Identifiant de l'enseignant
else if ( isset ( $_GET['ens'] ) && is_numeric ( $_GET['ens'] ) )
{
    $sql = 'SELECT nomens, prenomsens
            FROM enseignant
            WHERE idens = ' . db_protect ( $_GET['ens'] ) . ';';

    $req = db_query ( $db_link , $sql , __FILE__ , __LINE__ );

    // L'enseignant n'existe pas
    if ( pg_num_rows ( $req ) == 0 )
    {
        header ( 'Location: index.php' );
        die();
    }

    $row = pg_fetch_assoc ( $req );

    // On vérifie que l'enseignant a le droit d'afficher cette page
    if ( !is_admin() && $_GET['ens'] != $_SESSION['uid'] )
    {
        header ( 'Location: index.php' );
        die();
    }

    $titre = $row['prenomsens'] . ' ' . $row['nomens'];
}
else
{
    header ( 'Location: index.php' );
    die();
}


include_once ( 'include/header.php' );

echo "<ul>\n";

if ( isset ( $_GET['ensmt'] ) ) 
{
    echo '  <li>Intitulé&nbsp: ' . $row['intitule'] . "</li>\n";
    echo '  <li>Code&nbsp: ' . $row['code'] . "</li>\n";
    echo '  <li>UE&nbsp: ' . $row['nomue'] . "</li>\n";
}
else
{
    echo '  <li>Enseignant&nbsp;: <a href="enseignant.php?ens=' . $_GET['ens'] . '">' . $row['prenomsens'] . ' '. $row['nomens'] .  "</a></li>\n";
}

echo "</ul>\n";
echo '<p><a href="liste_ensmt.php">Retour à la liste des enseignements</a></p>';


// Recherche des réponses à chaque question
$sql = '( SELECT nom, idquestion, typequestion, intitule, valeur, orientation
        FROM reponses
        NATURAL JOIN questions
        NATURAL JOIN sections
        NATURAL JOIN questionnaire ';

if ( isset ( $_GET['ensmt'] ) ) 
{
    $sql .= 'WHERE idensmt = ' . db_protect ( $_GET['ensmt'] ) . ' ';
}
else
{
    $sql .= 'NATURAL JOIN choixcoursmagistraux
             WHERE idens = ' . db_protect ( $_GET['ens'] ) . ' ';
}

if ( !is_admin() ) $sql .= 'AND orientation = 0 ';

$sql .= "AND valeur <> ''
         ORDER BY idsection, idquestion ) ";

// On ajoute les questions spécifiques aux chargés de TD/TP
if ( !is_admin() )
{
    $sql .= 'UNION
             (SELECT nom,
                     idquestion,
                     typequestion,
                     intitule,
                     valeur,
                     orientation
              FROM reponses
              NATURAL JOIN questions
              NATURAL JOIN sections
              NATURAL JOIN questionnaire ';

    if ( isset ( $_GET['ensmt'] ) ) 
    {
        $sql .= 'WHERE idensmt = ' . db_protect ( $_GET['ensmt'] ) . ' ';
    }
    else
    {
        $sql .= 'NATURAL JOIN choixcoursmagistraux
             WHERE idens = ' . db_protect ( $_GET['ens'] ) . ' ';
    }

    $sql .= "AND valeur <> ''
             AND orientation = 1
             AND ( charge_td = " . $_SESSION['uid'] . "
                OR charge_tp = " . $_SESSION['uid'] . " )
             ORDER BY idsection, idquestion );";
}

$req = db_query ( $db_link , $sql , __FILE__ , __LINE__ );

$questions = array();

while ( $row = pg_fetch_assoc ( $req ) )
{
    $questions[$row['nom']][$row['idquestion']]['type'] = $row['typequestion'];
    $questions[$row['nom']][$row['idquestion']]['intitule'] = $row['intitule'];
    $questions[$row['nom']][$row['idquestion']]['orientation'] = $row['orientation'];

    // Nouvelle valeur
    $questions[$row['nom']][$row['idquestion']]['res'][] = $row['valeur'];
}

if ( empty ( $questions ) )
{
    echo "<p>Il n'y a aucune réponse pour cet enseignement.</p>\n";
}
else
{
    foreach ( $questions as $section => $reponses )
    {
        echo '<h4>' . $section . "</h4>\n";

        foreach ( $reponses as $question_id => $question_infos )
        {
            echo '<div class="question">';
            echo '<h4>' . $question_infos['intitule'] . '</h4>';

            $nbr_reponses = count ( $question_infos['res'] );
            echo '<ul><li>' . $nbr_reponses . ' réponse';
            if ( $nbr_reponses > 1 ) echo 's';
            echo '</li>';

            // Question de type note
            if ( $question_infos['type'] == 'note' )
            {
                $total = 0;
                $n = array ( 0 , 0 , 0 , 0 , 0 , 0 );

                // Pour calculer la moyenne
                foreach ( $question_infos['res'] as $result )
                {
                    if ( $question_infos['res'] != 0 )
                    {
                        $total += $result;
                        $n[$result]++;
                    }
                    else
                    {
                        $nbr_reponses--;
                    }
                }

                $moyenne = round ( $total / $nbr_reponses , 2 );

                echo '<li>Note moyenne&nbsp;: ' . $moyenne . ' / 5</li></ul>';

                // Affichage du camembert
                echo '<p><img src="camembert.php?type=note&amp;'
                                              . 'n0=' . $n[0] . '&amp;'
                                              . 'n1=' . $n[1] . '&amp;'
                                              . 'n2=' . $n[2] . '&amp;'
                                              . 'n3=' . $n[3] . '&amp;'
                                              . 'n4=' . $n[4] . '&amp;'
                                              . 'n5=' . $n[5] . '" /></p>';
            }

            // Question de type texte
            else if ( $question_infos['type'] == 'texte' )
            {
                echo '</ul>';

                if ( $nbr_reponses > 0 )
                {
                    echo '<p>Liste des réponses données&nbsp;:</p>';
                    echo "<ul>\n";

                    // Pour chaque réponse
                    foreach ( $question_infos['res'] as $result )
                    {
                        if ( !empty ( $result ) )
                        {
                            echo '  <li>' . $result . "</li>\n";
                        }
                    }

                    echo "</ul>\n";
                }
            }

            // Question de type nombre
            else if ( $question_infos['type'] == 'nombre' )
            {
                $total = 0;

                $n = array();

                // Pour calculer la moyenne
                foreach ( $question_infos['res'] as $result )
                {
                    $total += $result;

                    if ( isset ( $n[$result] ) ) $n[$result]++;
                    else $n[$result] = 1;
                }

                if ( $nbr_reponses > 0 )
                {
                    $moyenne = round ( $total / $nbr_reponses , 2 );
                }
                else
                {
                    $moyenne = 0;
                }

                echo '<li>Valeur moyenne&nbsp;: ' . $moyenne . '</li></ul>';

                // Affichage du camembert
                echo '<p><img src="camembert.php?type=nbr';

                foreach ( $n as $id => $nbr )
                {
                    echo '&amp;n' . $id . '=' . $nbr;
                }

                echo '" /></p>';
            }

            // Chargé de TP et de TD
            else if ( $question_infos['type'] == 'charge_tp' ||
                      $question_infos['type'] == 'charge_td' )
            {
                // On supprime les doublons de la liste des enseignants
                $question_infos['res'] = array_unique ( $question_infos['res'] , SORT_NUMERIC );

                echo '</ul>';

                if ( $nbr_reponses > 0 )
                {
                    echo '<p>Liste des réponses données&nbsp;:</p>';
                    echo "<ul>\n";

                    foreach ( $question_infos['res'] as $result )
                    {
                        $sql2 = 'SELECT nomens, prenomsens
                                 FROM enseignant
                                 WHERE idens = ' . db_protect ( $result ) . ';';

                        $req2 = db_query ( $db_link , $sql2 );

                        if ( pg_num_rows ( $req2 ) > 0 )
                        {
                            $row2 = pg_fetch_assoc ( $req2 );
                            echo '  <li><a href="enseignant.php?ens=' . $result . '">' . $row2['prenomsens'] . ' ' . $row2['nomens'] . "</a></li>\n";
                        }
                    }

                    echo "</ul>\n";
                }
            }

            // Filière d'origine
            else if ( $question_infos['type'] == 'filiere' )
            {
                // Nombre de réponses pour chaque filière
                $n = array();

                // Pour calculer la moyenne
                foreach ( $question_infos['res'] as $result )
                {
                    if ( isset ( $n[$result] ) ) $n[$result]++;
                    $n[$result] = 1;
                }

                // Affichage des résultats sous forme de liste
                if ( $nbr_reponses > 0 )
                {
                    echo "<ul>\n";

                    foreach ( $n as $filiere => $nombre )
                    {
                        echo '  <li>' . $filiere . '&nbsp;: ' . $nombre . "</li>\n";
                    }

                    echo "</ul>\n";
                }
            }
    
            echo "</p>\n</div>\n";
        }
    }

    echo '<p><a href="liste_ensmt.php">Retour à la liste des enseignements</a></p>';
}

include_once ( 'include/footer.php' );

?>