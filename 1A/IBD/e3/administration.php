<?php

require_once ( 'include/main.php' );

// Seul l'administrateur peut voir cette page
if ( !is_admin() )
{
    header ( 'Location: index.php' );
    die();
}

$titre = 'Administration';

include_once ( 'include/header.php' );

// Affichage de la page
?>

<ul>
  <li><a href="liste_ensmt.php">Liste des enseignements</a></li>
  <li><a href="liste_ens.php">Liste des enseignants</a></li>
  <li><a href="liste_eleves.php">Liste des élèves</a></li>
  <li><a href="questionnaire.php">Visualiser et modifier le questionnaire</a></li>
</ul>

<?php

include_once ( 'include/footer.php' );

?>