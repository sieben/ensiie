<?php

require_once ( 'include/main.php' );

// Si l'utilisateur n'est pas connecté, ou que ce n'est pas un enseignant,
// on le redirige vers la page de connexion
if ( !is_ens() )
{
    header ( 'Location: login.php' );
    die();
}


$titre = 'Liste des enseignements';

include_once ( 'include/header.php' );


if ( is_admin() )
{
    // Tous les enseignements sont visibles
    $sql = 'SELECT idensmt, intitule, code
            FROM enseignement;';

    $req = db_query ( $db_link , $sql );

    if ( pg_num_rows ( $req ) > 0 )
    {
        echo "<ul>\n";

        // Affichage de la liste des enseignements
        while ( $row = pg_fetch_assoc ( $req ) )
        {
            echo '  <li><a href="result.php?ensmt=' . $row['idensmt'] . '">';
            echo $row['intitule'] . ' (' . $row['code'] . ")</a></li>\n";
        }

        echo "</ul>\n";
    }
    else
    {
        echo "<p>Vous ne pouvez voir aucun enseignement.</p>\n";
    }
}
else
{
    $enseignements = array();

    // Cours magistraux
    $sql = 'SELECT idensmt, intitule, code
            FROM enseignement
            NATURAL JOIN choixcoursmagistraux
            WHERE idens = ' . db_protect ( $_SESSION['uid'] ) . ';';

    $req = db_query ( $db_link , $sql , __FILE__ , __LINE__ );

    if ( pg_num_rows ( $req ) > 0 )
    {
        while ( $row = pg_fetch_assoc ( $req ) )
        {
            $enseignements[$row['idensmt']] = array (
                'idensmt'  => $row['idensmt'],
                'intitule' => $row['intitule'],
                'code'     => $row['code'] );
        }
    }

    // TP
    $sql = 'SELECT idensmt, intitule, code
            FROM enseignement
            NATURAL JOIN choixtp
            WHERE idens = ' . db_protect ( $_SESSION['uid'] ) . ';';

    $req = db_query ( $db_link , $sql , __FILE__ , __LINE__ );

    if ( pg_num_rows ( $req ) > 0 )
    {
        while ( $row = pg_fetch_assoc ( $req ) )
        {
            if ( !isset ( $enseignements[$row['idensmt']] ) )
            {
                $enseignements[$row['idensmt']] = array (
                    'idensmt'  => $row['idensmt'],
                    'intitule' => $row['intitule'],
                    'code'     => $row['code'] );
            }
        }
    }

    // TD
    $sql = 'SELECT idensmt, intitule, code
            FROM enseignement
            NATURAL JOIN choixtd
            WHERE idens = ' . db_protect ( $_SESSION['uid'] ) . ';';

    $req = db_query ( $db_link , $sql , __FILE__ , __LINE__ );

    if ( pg_num_rows ( $req ) > 0 )
    {
        while ( $row = pg_fetch_assoc ( $req ) )
        {
            if ( !isset ( $enseignements[$row['idensmt']] ) )
            {
                $enseignements[$row['idensmt']] = array (
                    'idensmt'  => $row['idensmt'],
                    'intitule' => $row['intitule'],
                    'code'     => $row['code'] );
            }
        }
    }

    // Table des droits
    $sql = 'SELECT idensmt, intitule, code
            FROM droits
            NATURAL JOIN enseignement
            WHERE idens = ' . db_protect ( $_SESSION['uid'] ) . ';';

    $req = db_query ( $db_link , $sql , __FILE__ , __LINE__ );

    if ( pg_num_rows ( $req ) > 0 )
    {
        while ( $row = pg_fetch_assoc ( $req ) )
        {
            if ( !isset ( $enseignements[$row['idensmt']] ) )
            {
                $enseignements[$row['idensmt']] = array (
                    'idensmt'  => $row['idensmt'],
                    'intitule' => $row['intitule'],
                    'code'     => $row['code'] );
            }
        }
    }

    if ( sizeof ( $enseignements ) > 0 )
    {
        echo "<ul>\n";

        // Affichage de la liste des enseignements
        foreach ( $enseignements as $ens )
        {
            echo '  <li><a href="result.php?ensmt=' . $ens['idensmt'] . '">';
            echo $ens['intitule'] . ' (' . $ens['code'] . ")</a></li>\n";
        }

        echo "</ul>\n";
    }
    else
    {
        echo "<p>Vous ne pouvez voir aucun enseignement.</p>\n";
    }
}

include_once ( 'include/footer.php' );

?>