<?php

require_once ( 'include/main.php' );

// Si l'utilisateur n'est pas connecté, ou que ce n'est pas un élève,
// on le redirige vers la page de connexion
if ( !is_eleve() )
{
    header ( 'Location: login.php' );
    die();
}


// On n'a pas précisé l'identifiant de l'enseignement
if ( !isset ( $_GET['ensmt'] ) || !is_numeric ( $_GET['ensmt'] ) )
{
    header ( 'Location: index.php' );
    die();
}

// On vérifie que l'élève a le droit d'évaluer cet enseignement
if ( ensmt_eleve ( $_GET['ensmt'] , $_SESSION['uid'] ) != 1 )
{
    header ( 'Location: index.php' );
    die();
}


$files_css[] = 'form.css';
$titre = 'Évaluation d\'un enseignement';

include_once ( 'include/header.php' );


// Affichage du questionnaire
$sql = 'SELECT idquestion,
               intitule,
               typequestion,
               ordrequestion,
               idsection,
               nom
        FROM questions
        NATURAL JOIN sections
        ORDER BY idsection, ordrequestion;';

$req = db_query ( $db_link , $sql , __FILE__ , __LINE__ );


if ( pg_num_rows ( $req ) > 0 )
{
    // Traitement du formulaire
    if ( isset ( $_POST['submit'] ) )
    {
        // Identifiant de l'enregistrement
        $sql = 'SELECT MAX(idquestionnaire) AS max
                FROM questionnaire;';

        $req2 = db_query ( $db_link , $sql , __FILE__ , __LINE__ );

        if ( pg_num_rows ( $req2 ) > 0 )
        {
            $row = pg_fetch_assoc ( $req2 );
            $id_questionnaire = $row['max'] + 1;
        }
        else
        {
            $id_questionnaire = 1;
        }

        // Ajout des enregistrements à la base de données
        $sql = "INSERT INTO eleveensmt
                (ideleve, idensmt, dateenvoi)
                VALUES
                (" . $_SESSION['uid'] . ",
                 " . db_protect($_GET['ensmt']) . ",
                 '" . date ( 'Y-m-d' ) . "');";

        db_query ( $db_link , $sql , __FILE__ , __LINE__ );

        $sql = "INSERT INTO questionnaire
                (idquestionnaire, idensmt, dateenvoi)
                VALUES
                (" . $id_questionnaire . ",
                 " . db_protect ($_GET['ensmt']) .",
                 '" . date ( 'Y-m-d' ) . "');";

        db_query ( $db_link , $sql , __FILE__ , __LINE__ );
    }
    else
    {
        echo '<form action="eleve_questionnaire.php?ensmt=' . $_GET['ensmt'] . '" method="post">';
    }


    $id_section = 0;

    // Affichage du questionnaire
    while ( $row = pg_fetch_assoc ( $req ) )
    {
        // Affichage d'une nouvelle section
        if ( $id_section != $row['idsection'] )
        {
            if ( $id_section > 0 ) echo "</fieldset>\n\n";
            $id_section = $row['idsection'];

            echo "<fieldset>\n<legend>" . $row['nom'] . "</legend>\n\n";
        }

        // Question de type note
        if ( $row['typequestion'] == 'note' )
        {
            // Affichage de l'intitulé de la question
            echo '<p><label>' . $row['intitule'] . "</label></p>\n";

            echo "<p>\n";
            echo '  <input type="radio" name="q_' . $row['idquestion'] . '" id="form_row_' . $row['idquestion'] . '_0" value="0" checked="checked" /> ';
            echo '<label for="form_row_' . $row['idquestion'] . '_0">NSPP';
            echo "</label> \n";
            echo '  <input type="radio" name="q_' . $row['idquestion'] . '" id="form_row_' . $row['idquestion'] . '_1" value="1" /> ';
            echo '<label for="form_row_' . $row['idquestion'] . '_1">Tres mecontent';
            echo "</label> \n";
            echo '  <input type="radio" name="q_' . $row['idquestion'] . '" id="form_row_' . $row['idquestion'] . '_2" value="2" /> ';
            echo '<label for="form_row_' . $row['idquestion'] . '_2">Mécontent';
            echo "</label> \n";
            echo '  <input type="radio" name="q_' . $row['idquestion'] . '" id="form_row_' . $row['idquestion'] . '_3" value="3" /> ';
            echo '<label for="form_row_' . $row['idquestion'] . '_3">Neutre';
            echo "</label> \n";
            echo '  <input type="radio" name="q_' . $row['idquestion'] . '" id="form_row_' . $row['idquestion'] . '_4" value="4" /> ';
            echo '<label for="form_row_' . $row['idquestion'] . '_4">Satisfait';
            echo "</label> \n";
            echo '  <input type="radio" name="q_' . $row['idquestion'] . '" id="form_row_' . $row['idquestion'] . '_5" value="5" /> ';
            echo '<label for="form_row_' . $row['idquestion'] . '_5">Tres satisfait';
            echo "</label>\n</p>\n\n";

            if ( isset ( $_POST['q_' . $row['idquestion']] ) )
            {
                $sql = 'INSERT INTO reponses
                        (idquestionnaire,idquestion,valeur)
                        VALUES
                        ('. $id_questionnaire . ', '. $row['idquestion'] . ",
                         '".db_protect($_POST['q_'.$row['idquestion']] )."');";

                db_query ( $db_link , $sql , __FILE__ , __LINE__ );
            }
        }

        // Question de type nombre
        else if ( $row['typequestion'] == 'nombre' )
        {
            // Affichage de l'intitulé de la question
            echo '<p><label>' . $row['intitule'] . '</label> <input type="text" name="q_' . $row['idquestion'] . '" id="form_row_' . $row['idquestion'] . '" value="0" size="10" maxlength="9" /></p>' . "\n";

            if ( isset ( $_POST['q_' . $row['idquestion']] ) )
            {
                if ( is_numeric ( $_POST['q_' . $row['idquestion']] ) )
                {
		    $valeur = round ( $_POST['q_' . $row['idquestion']] );
		}
		else
		{
		    $valeur = 0;
                }

                $sql = 'INSERT INTO reponses
                        (idquestionnaire,idquestion,valeur)
                        VALUES
                        ('. $id_questionnaire . ', '. $row['idquestion'] . ",
                         '" . $valeur . "');";

                db_query ( $db_link , $sql );
            }
        }

        // Question de type charge_tp
        else if ( $row['typequestion'] == 'charge_tp' )
        {
            // Chargés de TP
            $sql2 = 'SELECT idens, nomens, prenomsens
                     FROM enseignant
                     NATURAL JOIN choixtp
                     NATURAL JOIN enseignement
                     LEFT OUTER JOIN optionoutc
                       ON optionoutc.idOption = enseignement.idOption
                     WHERE promo = ' . db_protect ( $_SESSION['promo'] ) . '
                       AND idensmt = ' . db_protect($_GET['ensmt']) . ';';

            $req2 = db_query ( $db_link , $sql2 );

            if ( pg_num_rows ( $req2 ) > 0 )
            {
                echo '<p class="form_line"><label for="form_row_' . $row['idquestion'] . '">' . $row['intitule'] . '</label> ';
                echo '<select name="q_' . $row['idquestion'] . '" id="form_row_' . $row['idquestion'] . '">' . "\n";
                echo '  <option value="0" selected="selected" disabled="disabled">Sélectionnez un enseignant</option>' . "\n";

                // Affichage de la liste des chargés de TP
                while ( $row2 = pg_fetch_assoc ( $req2 ) )
                {
                    echo '  <option value="' . $row2['idens'] . '">' . $row2['nomens'] .' ' . $row2['prenomsens'] . "</option>\n";
                }

                echo "</select>\n</p>\n\n";

                if ( isset ( $_POST['q_' . $row['idquestion']] ) )
                {
                    $sql = 'INSERT INTO reponses
                            (idquestionnaire,idquestion,valeur)
                            VALUES
                            ('. $id_questionnaire . ', '. $row['idquestion'] . ",
                             '".db_protect($_POST['q_'.$row['idquestion']] )."');";

                    db_query ( $db_link , $sql , __FILE__ , __LINE__ );

                    $charge_tp = $_POST['q_'.$row['idquestion']];
                }
            }
        }

        // Question de type charge_td
        else if ( $row['typequestion'] == 'charge_td' )
        {
            // Chargés de TD
            $sql2 = 'SELECT idens, nomens, prenomsens
                     FROM enseignant
                     NATURAL JOIN choixtd
                     NATURAL JOIN enseignement
                     LEFT OUTER JOIN optionoutc
                       ON optionoutc.idOption = enseignement.idOption
                     WHERE promo = ' . db_protect ( $_SESSION['promo'] ) . '
                       AND idensmt = ' . db_protect($_GET['ensmt']) . ';';

            $req2 = db_query ( $db_link , $sql2 , __FILE__ , __LINE__ );

            if ( pg_num_rows ( $req2 ) > 0 )
            {
                echo '<p class="form_line"><label for="form_row_' . $row['idquestion'] . '">' . $row['intitule'] . '</label> ';
                echo '<select name="q_' . $row['idquestion'] . '" id="form_row_' . $row['idquestion'] . '">' . "\n";
                echo '  <option value="0" selected="selected" disabled="disabled">Sélectionnez un enseignant</option>' . "\n";

                // Affichage de la liste des chargés de TD
                while ( $row2 = pg_fetch_assoc ( $req2 ) )
                {
                    echo '  <option value="' . $row2['idens'] . '">' . $row2['nomens'] .' ' . $row2['prenomsens'] . "</option>\n";
                }

                echo "</select>\n</p>\n\n";

                if ( isset ( $_POST['q_' . $row['idquestion']] ) )
                {
                    $sql = 'INSERT INTO reponses
                            (idquestionnaire,idquestion,valeur)
                            VALUES
                            ('. $id_questionnaire . ', '. $row['idquestion'] . ",
                             '".db_protect($_POST['q_'.$row['idquestion']] )."');";

                    db_query ( $db_link , $sql , __FILE__ , __LINE__ );

                    $charge_td = $_POST['q_'.$row['idquestion']];
                }
            }
        }

        // Question de type filiere
        else if ( $row['typequestion'] == 'filiere' )
        {
            echo '<p class="form_line"><label for="form_row_' . $row['idquestion'] . '">' . $row['intitule'] . '</label> ';
            echo '<select name="q_' . $row['idquestion'] . '" id="form_row_' . $row['idquestion'] . '">' . "\n";
            echo '  <option value="Non précisée" selected="selected" disabled="disabled">Sélectionnez une filière</option>' . "\n";

            // Liste des filières (non exaustive)
            $filieres = array ( 'PSI' , 'MP' , 'PC' , 'Fac' , 'IUT' , 'BTS' , 'Autre' );

            // Affichage de la liste des filières
            foreach ( $filieres as $nomf )
            {
                echo '  <option value="' . $nomf . '">' . $nomf . "</option>\n";
            }

            echo "</select>\n</p>\n\n";

            if ( isset ( $_POST['q_' . $row['idquestion']] ) )
            {
                $sql = 'INSERT INTO reponses
                        (idquestionnaire,idquestion,valeur)
                        VALUES
                        ('. $id_questionnaire . ', '. $row['idquestion'] . ",
                            '".db_protect($_POST['q_'.$row['idquestion']] )."');";

                db_query ( $db_link , $sql , __FILE__ , __LINE__ );
            }
        }

        // Question de type texte
        else /*if ( $row['typequestion'] == 'texte' )*/
        {
            // Affichage de l'intitulé de la question
            echo '<p><label for="form_row_' . $row['idquestion'] . '">' . $row['intitule'] . "</label></p>\n";

            echo '<textarea cols="80" rows="3" name="q_' . $row['idquestion'];
            echo '" id="form_row_' . $row['idquestion'] . '">';

            if ( isset ( $_POST['q_' . $row['idquestion']] ) )
            {
                echo $_POST['q_' . $row['idquestion']];

                $sql = 'INSERT INTO reponses
                        (idquestionnaire, idquestion, valeur)
                        VALUES
                        (' . $id_questionnaire . ', '. $row['idquestion'] . ",
                         '" . db_protect ( $_POST['q_'.$row['idquestion']] ) . "');";

                db_query ( $db_link , $sql , __FILE__ , __LINE__ );
            }

            echo "</textarea>\n\n";
        }
    }

    // Si l'élève a répondu, on le redirige
    if ( isset ( $_POST['submit'] ) )
    {
        if ( isset ( $charge_td ) )
        {
            $sql = 'UPDATE reponses
                    SET charge_td = ' . $charge_td . '
                    WHERE idquestionnaire = ' . $id_questionnaire . ';';

            db_query ( $db_link , $sql , __FILE__ , __LINE__ );
        }

        if ( isset ( $charge_tp ) )
        {
            $sql = 'UPDATE reponses
                    SET charge_tp = ' . $charge_tp . '
                    WHERE idquestionnaire = ' . $id_questionnaire . ';';

            db_query ( $db_link , $sql , __FILE__ , __LINE__ );
        }

        // Message d'information pour la page suivante
        $_SESSION['infos'] = 'Les réponses du questionnaire ont été envoyées.';

        header ( 'Location: index.php' );
        die();
    }

    echo "</fieldset>\n";
    echo '<p class="form_submit"><input type="submit" name="submit" value="Valider" /><input type="button" class="form_back" value="Annuler" /></p>';
    echo "</form>\n";
}
// Aucune question ni section  àafficher
else
{
    echo "<p>Il n'y a aucune question pour le moment.</p>\n";
}

include_once ( 'include/footer.php' );

?>