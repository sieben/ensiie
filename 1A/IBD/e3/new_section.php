<?php

require_once ( 'include/main.php' );

// Seul l'administrateur peut voir cette page
if ( !is_admin() )
{
    header ( 'Location: login.php' );
    die();
}

$files_css[] = 'form.css';

$titre = 'Nouvelle section';

include_once ( 'include/header.php' );


// Traitement du formulaire
if ( isset ( $_POST['submit'] ) )
{
    if ( !isset ( $_POST['nom'] ) || empty ( $_POST['nom'] ) )
    {
        echo '<p class="erreur">Vous devez donner un nom à cette section.</p>';
    }
    else
    {
        // Identifiant de la section
        $sql = 'SELECT MAX(idsection) AS max
                FROM sections;';

        $req = db_query ( $db_link , $sql );

        if ( pg_num_rows ( $req ) > 0 )
        {
            $row = pg_fetch_assoc ( $req );
            $id = $row['max'] + 1;
        }
        else
        {
            $id = 1;
        }

        // Création de la nouvelle section
        $sql = 'INSERT INTO sections
                (idsection, nom)
                VALUES
                (' . $id . ", '" . db_protect ( $_POST['nom'] ) . "');";

        db_query ( $db_link , $sql );

        header ( 'Location: questionnaire.php' );
        die();
    }
}


// Affichage du formulaire

echo '<form action="new_section.php" method="post">';
echo "<fieldset><legend>Nouvelle section</legend>\n";

echo '<p class="form_line"><label for="form_row_nom">Nom</label> <input type="text" name="nom" ';
echo 'id="form_row_nom" maxlength="150" size="30" value="';
if ( isset ( $_POST['nom'] ) ) echo $_POST['nom'];
echo '" />';

echo "</p>\n</fieldset>\n";
echo '<p class="form_submit"><input type="submit" name="submit" value="Valider" /><input type="button" class="form_back" value="Annuler" /></p>';
echo "</form>\n";

include_once ( 'include/footer.php' );

?>