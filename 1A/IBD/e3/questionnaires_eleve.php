<?php

require_once ( 'include/main.php' );

// Si l'utilisateur n'est pas connecté, ou que ce n'est pas un élève,
// on le redirige vers la page de connexion
if ( !is_eleve() )
{
    header ( 'Location: login.php' );
    die();
}


$titre = 'Liste des enseignements à évaluer';

include_once ( 'include/header.php' );

$sql = 'SELECT idensmt, intitule, code, idue
        FROM enseignement
        WHERE idoption = ';

// Enseignements correspondant à la promo de l'élève
if ( $_SESSION['promo'] == 2012 )
{
    $sql .= '1';
}
else if ( $_SESSION['promo'] == 2011 )
{
    $sql .= '2';

    $sql_tmp = "SELECT idoption1
                FROM eleve
                WHERE ideleve = " . $_SESSION['uid'] . ";";

    $req = db_query ( $db_link , $sql_tmp , __FILE__ , __LINE__ );

    $row = pg_fetch_assoc ( $req );

    if ( !empty ( $row['idoption1'] ) )
    {
        $sql .= ' OR idoption = ' . $row['idoption1'];
    }
}
else if ( $_SESSION['promo'] == 2010 )
{
    $sql .= '3';

    $sql_tmp = "SELECT idoption1, idoption2, idoption3
                FROM eleve
                WHERE ideleve = " . $_SESSION['uid'] . ";";

    $req = db_query ( $db_link , $sql_tmp );

    $row = pg_fetch_assoc ( $req );

    if ( !empty ( $row['idoption1'] ) )
    {
        $sql .= ' OR idoption = ' . $row['idoption1'];
    }

    if ( !empty ( $row['idoption2'] ) )
    {
        $sql .= ' OR idoption = ' . $row['idoption2'];
    }

    if ( !empty ( $row['idoption3'] ) )
    {
        $sql .= ' OR idoption = ' . $row['idoption3'];
    }
}
else
{
    header ( 'Location: index.php' );
    die();
}

$req = db_query ( $db_link , $sql );


if ( pg_num_rows ( $req ) > 0 )
{
    // Liste des enseignements visibles
    $enseignements = array();

    while ( $row = pg_fetch_assoc ( $req ) )
    {
        $enseignements[$row['idensmt']] = array (
            'ue'       => $row['idue'],
            'intitule' => $row['intitule'],
            'code'     => $row['code'],
            'evalue'   => false );
    }

    // Conditionnelle ?
    $sql = 'SELECT ens1.intitule AS intitule1,
                   ens1.code AS code1,
                   ens1.idue AS idue1,
                   ens1.idensmt AS idensmt1,
                   ens2.intitule AS intitule2,
                   ens2.code AS code2,
                   ens2.idue AS idue2,
                   ens2.idensmt AS idensmt2
            FROM conditionnelle
            LEFT JOIN ue AS ue1 ON ue1.idue = idue1manquee
            LEFT JOIN enseignement AS ens1 ON ens1.idue = ue1.idue
            LEFT JOIN ue AS ue2 ON ue2.idue = idue2manquee
            LEFT JOIN enseignement AS ens2 ON ens2.idue = ue2.idue
            WHERE ideleve = ' . db_protect ( $_SESSION['uid'] ) . ';';

    $req2 = db_query ( $db_link , $sql , __FILE__ , __LINE__ );

    while ( $row2 = pg_fetch_assoc ( $req2 ) )
    {
        if ( !empty ( $row2['idensmt2'] ) )
        {
            $enseignements[$row2['idensmt1']] = array (
                'ue'       => $row2['idue1'],
                'intitule' => $row2['intitule1'],
                'code'     => $row2['code1'],
                'evalue'   => false );
        }

        if ( !empty ( $row2['idensmt2'] ) )
        {
            $enseignements[$row2['idensmt2']] = array (
                'ue'       => $row2['idue2'],
                'intitule' => $row2['intitule2'],
                'code'     => $row2['code2'],
                'evalue'   => false );
        }
    }

    if ( pg_num_rows ( $req2 ) == 0 )
    {
        // Redoublement ?
        $sql = 'SELECT idensmt
                FROM redoublement
                LEFT JOIN ue ON idue = iduereussie
                NATURAL JOIN enseignement
                WHERE ideleve = ' . db_protect ( $_SESSION['uid'] ) . ';';

        $req2 = db_query ( $db_link , $sql );

        if ( pg_num_rows ( $req2 ) > 0 )
        {
            while ( $row2 = pg_fetch_assoc ( $req2 ) )
            {
                unset ( $enseignements[$row2['idensmt']] );
            }
        }
    }

    // On cherche si l'élève a déjà évalué chaque enseignement
    $sql = 'SELECT idensmt
            FROM eleveensmt
            WHERE ideleve = ' . db_protect ( $_SESSION['uid'] ) . ';';

    $req2 = db_query ( $db_link , $sql );

    while ( $row2 = pg_fetch_assoc ( $req2 ) )
    {
        $enseignements[$row2['idensmt']]['evalue'] = true;
    }


    // Affichage de la liste
    echo "<ul>\n";

    // Pour chaque enseignement
    foreach ( $enseignements as $idensmt => $infos )
    {
        echo '  <li>';

        if ( $infos['evalue'] )
        {
            echo $infos['intitule'] . ' (évalué)';
        }
        else
        {
            echo '<a href="eleve_questionnaire.php?ensmt=' . $idensmt . '">';
            echo $infos['intitule'] . ' (' . $infos['code'] . ')</a>';
        }

        echo "</li>\n";
    }

    echo "</ul>\n";
}
// Aucun enseignement
else
{
    echo "<p>Vous ne pouvez évaluer aucun enseignement.</p>\n";
}

include_once ( 'include/footer.php' );

?>