<?php

require_once ( 'include/main.php' );

// L'utilisateur n'est pas connecté ou n'est pas administrateur
if ( !is_admin() )
{
    header ( 'Location: login.php' );
    die();
}

// On n'a pas précisé l'identifiant de la section
if ( !isset ( $_GET['section'] ) || !is_numeric ( $_GET['section'] ) )
{
    header ( 'Location: questionnaire.php' );
    die();
}

// Recherche de la section
$sql = 'SELECT idsection
        FROM sections
        WHERE idsection = ' . db_protect ( $_GET['section'] ) . ';';

$req = db_query ( $db_link , $sql );

if ( pg_num_rows ( $req ) == 0 )
{
    header ( 'Location: questionnaire.php' );
    die();
}


$files_css[] = 'form.css';

$titre = 'Nouvelle question';

include_once ( 'include/header.php' );


// Traitement du formulaire
if ( isset ( $_POST['submit'] ) )
{
    if ( !isset ( $_POST['intitule'] ) || empty ( $_POST['intitule'] ) )
    {
        echo '<p class="erreur">Vous devez donner un nom à cette question.</p>';
    }
    else
    {
        // On cherche la position de la nouvelle question
        $sql = 'SELECT MAX(ordrequestion) AS max
                FROM questions
                WHERE idsection = ' . db_protect ( $_GET['section'] ) . ';';

        $req = db_query ( $db_link , $sql );

        // La section ne comporte aucune question
        if ( pg_num_rows ( $req ) === 0 )
        {
            $ordre = 1;
        }
        // La section comporte déjà des questions
        else
        {
            $row = pg_fetch_assoc ( $req );
            $ordre = $row['max'] + 1;
        }

        // On cherche l'identifiant de la nouvelle question
        $sql = 'SELECT MAX(idquestion) AS id
                FROM questions;';

        $req = db_query ( $db_link , $sql );

        if ( pg_num_rows ( $req ) === 0 )
        {
            $id_question = 1;
        }
        else
        {
            $row = pg_fetch_assoc ( $req );
            $id_question = $row['id'] + 1;
        }

        if ( $_POST['type'] == 'note' ||
             $_POST['type'] == 'texte' ||
             $_POST['type'] == 'nombre' ||
             $_POST['type'] == 'charge_tp' ||
             $_POST['type'] == 'charge_td' ||
             $_POST['type'] == 'filiere' )
        {
            $type_question = $_POST['type'];
        }
        else
        {
            $type_question = 'texte'; // Valeur par défaut
        }

        // Ajout de la question à la base
        $sql = 'INSERT INTO questions
                (idquestion, intitule, typequestion, idsection, ordrequestion)
                VALUES
                (' . $id_question . ",
                 '" . db_protect ( $_POST['intitule'] ) . "',
                 '" . db_protect ( $type_question ) . "',
                 " . db_protect ( $_GET['section'] ) . ',
                 ' . $ordre . ');';

        db_query ( $db_link , $sql );

        header ( 'Location: questionnaire.php' );
        die();
    }
}


// Formulaire

echo '<form action="new_question.php?section=' . $_GET['section'] . '" method="post">';
echo '<fieldset><legend>Nouvelle question</legend>';

echo '<p class="form_line"><label for="form_row_intitule" class="form_label">Intitulé</label> <input type="text" name="intitule" id="form_row_intitule" maxlength="42" size="30" value="' . ( isset ( $_POST['intitule'] ) ? $_POST['intitule'] : '' ) . '" /></p>';
echo '<p class="form_line"><label for="form_row_type" class="form_label">Type de question</label> <select name="type" id="form_row_type">';

// Types de question
echo '  <option value="note">Note</option>';
echo '  <option value="texte">Texte</option>';
echo '  <option value="nombre">Nombre</option>';
echo '  <option value="charge_tp">Chargé de TP</option>';
echo '  <option value="charge_td">Chargé de TD</option>';
echo '  <option value="filiere">Filière d\'origine</option>';

echo "</select></p>\n</fieldset>\n";
echo '<p class="form_submit"><input type="submit" name="submit" value="Valider" /><input type="button" class="form_back" value="Annuler" /></p>';
echo "</form>\n";

include_once ( 'include/footer.php' );

?>