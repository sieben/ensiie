--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: charge; Type: TABLE; Schema: public; Owner: teddy.michel; Tablespace: 
--

CREATE TABLE charge (
    idcharge integer NOT NULL,
    nomcharge text NOT NULL,
    nbhrcharge integer NOT NULL,
    idens integer NOT NULL
);


ALTER TABLE public.charge OWNER TO "teddy.michel";

--
-- Name: TABLE charge; Type: COMMENT; Schema: public; Owner: teddy.michel
--

COMMENT ON TABLE charge IS 'Table chargé de répertorier les différentes fonctions du personnel de l''école.';


--
-- Name: COLUMN charge.idens; Type: COMMENT; Schema: public; Owner: teddy.michel
--

COMMENT ON COLUMN charge.idens IS 'Identifiant d''un enseignant.';


--
-- Name: charge_idcharge_seq; Type: SEQUENCE; Schema: public; Owner: teddy.michel
--

CREATE SEQUENCE charge_idcharge_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.charge_idcharge_seq OWNER TO "teddy.michel";

--
-- Name: charge_idcharge_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: teddy.michel
--

ALTER SEQUENCE charge_idcharge_seq OWNED BY charge.idcharge;


--
-- Name: charge_idcharge_seq; Type: SEQUENCE SET; Schema: public; Owner: teddy.michel
--

SELECT pg_catalog.setval('charge_idcharge_seq', 30, false);


--
-- Name: choixcoursmagistraux; Type: TABLE; Schema: public; Owner: teddy.michel; Tablespace: 
--

CREATE TABLE choixcoursmagistraux (
    idensmt integer NOT NULL,
    idens integer NOT NULL,
    datechoix date NOT NULL,
    nbhrcm real NOT NULL
);


ALTER TABLE public.choixcoursmagistraux OWNER TO "teddy.michel";

--
-- Name: TABLE choixcoursmagistraux; Type: COMMENT; Schema: public; Owner: teddy.michel
--

COMMENT ON TABLE choixcoursmagistraux IS 'Donnée de l''énoncé';


--
-- Name: choixtd; Type: TABLE; Schema: public; Owner: teddy.michel; Tablespace: 
--

CREATE TABLE choixtd (
    idensmt integer NOT NULL,
    idens integer NOT NULL,
    nbgroupestd integer NOT NULL,
    datechoix date NOT NULL,
    nbhrpargroupe real NOT NULL
);


ALTER TABLE public.choixtd OWNER TO "teddy.michel";

--
-- Name: TABLE choixtd; Type: COMMENT; Schema: public; Owner: teddy.michel
--

COMMENT ON TABLE choixtd IS 'Donnée de l''énoncé';


--
-- Name: choixtp; Type: TABLE; Schema: public; Owner: teddy.michel; Tablespace: 
--

CREATE TABLE choixtp (
    idensmt integer NOT NULL,
    idens integer NOT NULL,
    nbgroupestp integer NOT NULL,
    datechoix date NOT NULL,
    nbhrpargroupe real NOT NULL
);


ALTER TABLE public.choixtp OWNER TO "teddy.michel";

--
-- Name: TABLE choixtp; Type: COMMENT; Schema: public; Owner: teddy.michel
--

COMMENT ON TABLE choixtp IS 'Donnée de l''énoncé';


--
-- Name: conditionnelle; Type: TABLE; Schema: public; Owner: teddy.michel; Tablespace: 
--

CREATE TABLE conditionnelle (
    ideleve integer NOT NULL,
    idue1manquee integer NOT NULL,
    idue2manquee integer NOT NULL
);


ALTER TABLE public.conditionnelle OWNER TO "teddy.michel";

--
-- Name: TABLE conditionnelle; Type: COMMENT; Schema: public; Owner: teddy.michel
--

COMMENT ON TABLE conditionnelle IS 'Si un élève est passé en conditionnelle (Au plus 2 ue non validées) alors il est dans cette table.';


--
-- Name: droits; Type: TABLE; Schema: public; Owner: teddy.michel; Tablespace: 
--

CREATE TABLE droits (
    idens integer NOT NULL,
    idensmt integer NOT NULL
);


ALTER TABLE public.droits OWNER TO "teddy.michel";

--
-- Name: TABLE droits; Type: COMMENT; Schema: public; Owner: teddy.michel
--

COMMENT ON TABLE droits IS 'Table contenant les droits d''accès aux résultats d''un enseignement';


--
-- Name: COLUMN droits.idens; Type: COMMENT; Schema: public; Owner: teddy.michel
--

COMMENT ON COLUMN droits.idens IS 'Identifiant d''un enseignant';


--
-- Name: COLUMN droits.idensmt; Type: COMMENT; Schema: public; Owner: teddy.michel
--

COMMENT ON COLUMN droits.idensmt IS 'Identifiant d''un enseignement.';


--
-- Name: eleve; Type: TABLE; Schema: public; Owner: teddy.michel; Tablespace: 
--

CREATE TABLE eleve (
    ideleve integer NOT NULL,
    login text NOT NULL,
    motdepasse text NOT NULL,
    promo integer NOT NULL,
    groupe integer NOT NULL,
    sousgroupe integer NOT NULL,
    grade integer NOT NULL,
    entree integer NOT NULL,
    idoption1 integer,
    idoption2 integer,
    idoption3 integer
);


ALTER TABLE public.eleve OWNER TO "teddy.michel";

--
-- Name: TABLE eleve; Type: COMMENT; Schema: public; Owner: teddy.michel
--

COMMENT ON TABLE eleve IS 'Table qui contient les informations relatives aux élèves.';


--
-- Name: eleve1a; Type: VIEW; Schema: public; Owner: teddy.michel
--

CREATE VIEW eleve1a AS
    SELECT eleve.login, eleve.entree, eleve.groupe, eleve.ideleve FROM eleve WHERE (eleve.grade = 1);


ALTER TABLE public.eleve1a OWNER TO "teddy.michel";

--
-- Name: VIEW eleve1a; Type: COMMENT; Schema: public; Owner: teddy.michel
--

COMMENT ON VIEW eleve1a IS 'Élèves de première année.';


--
-- Name: eleve2a; Type: VIEW; Schema: public; Owner: teddy.michel
--

CREATE VIEW eleve2a AS
    SELECT eleve.sousgroupe, eleve.login, eleve.entree, eleve.groupe, eleve.ideleve, eleve.idoption1 FROM eleve WHERE (eleve.grade = 2);


ALTER TABLE public.eleve2a OWNER TO "teddy.michel";

--
-- Name: VIEW eleve2a; Type: COMMENT; Schema: public; Owner: teddy.michel
--

COMMENT ON VIEW eleve2a IS 'Élèves de deuxième année.';


--
-- Name: eleve3a; Type: VIEW; Schema: public; Owner: teddy.michel
--

CREATE VIEW eleve3a AS
    SELECT eleve.sousgroupe, eleve.login, eleve.entree, eleve.groupe, eleve.ideleve, eleve.idoption1, eleve.idoption2, eleve.idoption3 FROM eleve WHERE (eleve.grade = 3);


ALTER TABLE public.eleve3a OWNER TO "teddy.michel";

--
-- Name: VIEW eleve3a; Type: COMMENT; Schema: public; Owner: teddy.michel
--

COMMENT ON VIEW eleve3a IS 'Élèves de troisième année.';


--
-- Name: eleve_ideleve_seq; Type: SEQUENCE; Schema: public; Owner: teddy.michel
--

CREATE SEQUENCE eleve_ideleve_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.eleve_ideleve_seq OWNER TO "teddy.michel";

--
-- Name: eleve_ideleve_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: teddy.michel
--

ALTER SEQUENCE eleve_ideleve_seq OWNED BY eleve.ideleve;


--
-- Name: eleve_ideleve_seq; Type: SEQUENCE SET; Schema: public; Owner: teddy.michel
--

SELECT pg_catalog.setval('eleve_ideleve_seq', 1565, true);


--
-- Name: eleveensmt; Type: TABLE; Schema: public; Owner: teddy.michel; Tablespace: 
--

CREATE TABLE eleveensmt (
    ideleve integer NOT NULL,
    idensmt integer NOT NULL,
    dateenvoi date NOT NULL
);


ALTER TABLE public.eleveensmt OWNER TO "teddy.michel";

--
-- Name: enseignant; Type: TABLE; Schema: public; Owner: teddy.michel; Tablespace: 
--

CREATE TABLE enseignant (
    idens integer NOT NULL,
    nomens text NOT NULL,
    prenomsens text NOT NULL,
    nbhrreelles real NOT NULL,
    courriel text NOT NULL,
    reliquat real NOT NULL,
    salariepublic integer NOT NULL,
    login text NOT NULL,
    motdepasse text NOT NULL,
    autresheures real NOT NULL
);


ALTER TABLE public.enseignant OWNER TO "teddy.michel";

--
-- Name: TABLE enseignant; Type: COMMENT; Schema: public; Owner: teddy.michel
--

COMMENT ON TABLE enseignant IS 'Donnée de l''énoncé';


--
-- Name: enseignant_idens_seq; Type: SEQUENCE; Schema: public; Owner: teddy.michel
--

CREATE SEQUENCE enseignant_idens_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.enseignant_idens_seq OWNER TO "teddy.michel";

--
-- Name: enseignant_idens_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: teddy.michel
--

ALTER SEQUENCE enseignant_idens_seq OWNED BY enseignant.idens;


--
-- Name: enseignant_idens_seq; Type: SEQUENCE SET; Schema: public; Owner: teddy.michel
--

SELECT pg_catalog.setval('enseignant_idens_seq', 17, true);


--
-- Name: enseignement; Type: TABLE; Schema: public; Owner: teddy.michel; Tablespace: 
--

CREATE TABLE enseignement (
    idensmt integer NOT NULL,
    idue integer NOT NULL,
    idoption integer NOT NULL,
    nbhrcmtotal real NOT NULL,
    code text NOT NULL,
    nbhrtdtotal real NOT NULL,
    intitule text NOT NULL,
    datedebut date NOT NULL,
    datefin date NOT NULL
);


ALTER TABLE public.enseignement OWNER TO "teddy.michel";

--
-- Name: TABLE enseignement; Type: COMMENT; Schema: public; Owner: teddy.michel
--

COMMENT ON TABLE enseignement IS 'Donnée de l''énoncé';


--
-- Name: enseignement_idensmt_seq; Type: SEQUENCE; Schema: public; Owner: teddy.michel
--

CREATE SEQUENCE enseignement_idensmt_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.enseignement_idensmt_seq OWNER TO "teddy.michel";

--
-- Name: enseignement_idensmt_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: teddy.michel
--

ALTER SEQUENCE enseignement_idensmt_seq OWNED BY enseignement.idensmt;


--
-- Name: enseignement_idensmt_seq; Type: SEQUENCE SET; Schema: public; Owner: teddy.michel
--

SELECT pg_catalog.setval('enseignement_idensmt_seq', 21, true);


--
-- Name: formation; Type: TABLE; Schema: public; Owner: teddy.michel; Tablespace: 
--

CREATE TABLE formation (
    idformation integer NOT NULL,
    nomformation text NOT NULL
);


ALTER TABLE public.formation OWNER TO "teddy.michel";

--
-- Name: TABLE formation; Type: COMMENT; Schema: public; Owner: teddy.michel
--

COMMENT ON TABLE formation IS 'Donnée de l''énoncé';


--
-- Name: formation_idformation_seq; Type: SEQUENCE; Schema: public; Owner: teddy.michel
--

CREATE SEQUENCE formation_idformation_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.formation_idformation_seq OWNER TO "teddy.michel";

--
-- Name: formation_idformation_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: teddy.michel
--

ALTER SEQUENCE formation_idformation_seq OWNED BY formation.idformation;


--
-- Name: formation_idformation_seq; Type: SEQUENCE SET; Schema: public; Owner: teddy.michel
--

SELECT pg_catalog.setval('formation_idformation_seq', 4, false);


--
-- Name: optionoutc; Type: TABLE; Schema: public; Owner: teddy.michel; Tablespace: 
--

CREATE TABLE optionoutc (
    idoption integer NOT NULL,
    idformation integer NOT NULL,
    nomoption text NOT NULL,
    nbgroupestd integer NOT NULL,
    promo integer NOT NULL
);


ALTER TABLE public.optionoutc OWNER TO "teddy.michel";

--
-- Name: TABLE optionoutc; Type: COMMENT; Schema: public; Owner: teddy.michel
--

COMMENT ON TABLE optionoutc IS 'Donnée de l''énoncé';


--
-- Name: optionoutc_idoption_seq; Type: SEQUENCE; Schema: public; Owner: teddy.michel
--

CREATE SEQUENCE optionoutc_idoption_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.optionoutc_idoption_seq OWNER TO "teddy.michel";

--
-- Name: optionoutc_idoption_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: teddy.michel
--

ALTER SEQUENCE optionoutc_idoption_seq OWNED BY optionoutc.idoption;


--
-- Name: optionoutc_idoption_seq; Type: SEQUENCE SET; Schema: public; Owner: teddy.michel
--

SELECT pg_catalog.setval('optionoutc_idoption_seq', 11, false);


--
-- Name: questionnaire; Type: TABLE; Schema: public; Owner: teddy.michel; Tablespace: 
--

CREATE TABLE questionnaire (
    idquestionnaire integer NOT NULL,
    idensmt integer NOT NULL,
    dateenvoi date NOT NULL
);


ALTER TABLE public.questionnaire OWNER TO "teddy.michel";

--
-- Name: TABLE questionnaire; Type: COMMENT; Schema: public; Owner: teddy.michel
--

COMMENT ON TABLE questionnaire IS 'Stockage d''un identifiant de questionnaire envoyé par un élève.';


--
-- Name: questions; Type: TABLE; Schema: public; Owner: teddy.michel; Tablespace: 
--

CREATE TABLE questions (
    idquestion integer NOT NULL,
    intitule text NOT NULL,
    typequestion character varying(10) NOT NULL,
    idsection integer NOT NULL,
    ordrequestion integer NOT NULL,
    orientation integer DEFAULT 0
);


ALTER TABLE public.questions OWNER TO "teddy.michel";

--
-- Name: TABLE questions; Type: COMMENT; Schema: public; Owner: teddy.michel
--

COMMENT ON TABLE questions IS 'Contenu du questionnaire.';


--
-- Name: questions_idquestion_seq; Type: SEQUENCE; Schema: public; Owner: teddy.michel
--

CREATE SEQUENCE questions_idquestion_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.questions_idquestion_seq OWNER TO "teddy.michel";

--
-- Name: questions_idquestion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: teddy.michel
--

ALTER SEQUENCE questions_idquestion_seq OWNED BY questions.idquestion;


--
-- Name: questions_idquestion_seq; Type: SEQUENCE SET; Schema: public; Owner: teddy.michel
--

SELECT pg_catalog.setval('questions_idquestion_seq', 31, false);


--
-- Name: redoublement; Type: TABLE; Schema: public; Owner: teddy.michel; Tablespace: 
--

CREATE TABLE redoublement (
    ideleve integer NOT NULL,
    iduereussie integer NOT NULL
);


ALTER TABLE public.redoublement OWNER TO "teddy.michel";

--
-- Name: TABLE redoublement; Type: COMMENT; Schema: public; Owner: teddy.michel
--

COMMENT ON TABLE redoublement IS 'Si un élève redouble son année cela signifie qu''il est enregistré dans cette relation.
On ne stocke que les ue qu''il a d''ores et déjà réussies.';


--
-- Name: reponses; Type: TABLE; Schema: public; Owner: teddy.michel; Tablespace: 
--

CREATE TABLE reponses (
    idquestion integer NOT NULL,
    valeur character varying(150) NOT NULL,
    idquestionnaire integer NOT NULL,
    charge_td integer,
    charge_tp integer
);


ALTER TABLE public.reponses OWNER TO "teddy.michel";

--
-- Name: TABLE reponses; Type: COMMENT; Schema: public; Owner: teddy.michel
--

COMMENT ON TABLE reponses IS 'Table contenant l''ensemble des réponses fournies par les élèves.';


--
-- Name: sections; Type: TABLE; Schema: public; Owner: teddy.michel; Tablespace: 
--

CREATE TABLE sections (
    idsection integer NOT NULL,
    nom text NOT NULL
);


ALTER TABLE public.sections OWNER TO "teddy.michel";

--
-- Name: TABLE sections; Type: COMMENT; Schema: public; Owner: teddy.michel
--

COMMENT ON TABLE sections IS 'Informations sur les différentes sections du questionnaire.';


--
-- Name: sections_idsection_seq; Type: SEQUENCE; Schema: public; Owner: teddy.michel
--

CREATE SEQUENCE sections_idsection_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.sections_idsection_seq OWNER TO "teddy.michel";

--
-- Name: sections_idsection_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: teddy.michel
--

ALTER SEQUENCE sections_idsection_seq OWNED BY sections.idsection;


--
-- Name: sections_idsection_seq; Type: SEQUENCE SET; Schema: public; Owner: teddy.michel
--

SELECT pg_catalog.setval('sections_idsection_seq', 3, false);


--
-- Name: ue; Type: TABLE; Schema: public; Owner: teddy.michel; Tablespace: 
--

CREATE TABLE ue (
    idue integer NOT NULL,
    nomue text NOT NULL,
    idcharge integer NOT NULL
);


ALTER TABLE public.ue OWNER TO "teddy.michel";

--
-- Name: TABLE ue; Type: COMMENT; Schema: public; Owner: teddy.michel
--

COMMENT ON TABLE ue IS 'Donnée de l''énoncé';


--
-- Name: ue_idue_seq; Type: SEQUENCE; Schema: public; Owner: teddy.michel
--

CREATE SEQUENCE ue_idue_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.ue_idue_seq OWNER TO "teddy.michel";

--
-- Name: ue_idue_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: teddy.michel
--

ALTER SEQUENCE ue_idue_seq OWNED BY ue.idue;


--
-- Name: ue_idue_seq; Type: SEQUENCE SET; Schema: public; Owner: teddy.michel
--

SELECT pg_catalog.setval('ue_idue_seq', 14, false);


--
-- Name: idcharge; Type: DEFAULT; Schema: public; Owner: teddy.michel
--

ALTER TABLE charge ALTER COLUMN idcharge SET DEFAULT nextval('charge_idcharge_seq'::regclass);


--
-- Name: ideleve; Type: DEFAULT; Schema: public; Owner: teddy.michel
--

ALTER TABLE eleve ALTER COLUMN ideleve SET DEFAULT nextval('eleve_ideleve_seq'::regclass);


--
-- Name: idens; Type: DEFAULT; Schema: public; Owner: teddy.michel
--

ALTER TABLE enseignant ALTER COLUMN idens SET DEFAULT nextval('enseignant_idens_seq'::regclass);


--
-- Name: idensmt; Type: DEFAULT; Schema: public; Owner: teddy.michel
--

ALTER TABLE enseignement ALTER COLUMN idensmt SET DEFAULT nextval('enseignement_idensmt_seq'::regclass);


--
-- Name: idformation; Type: DEFAULT; Schema: public; Owner: teddy.michel
--

ALTER TABLE formation ALTER COLUMN idformation SET DEFAULT nextval('formation_idformation_seq'::regclass);


--
-- Name: idoption; Type: DEFAULT; Schema: public; Owner: teddy.michel
--

ALTER TABLE optionoutc ALTER COLUMN idoption SET DEFAULT nextval('optionoutc_idoption_seq'::regclass);


--
-- Name: idquestion; Type: DEFAULT; Schema: public; Owner: teddy.michel
--

ALTER TABLE questions ALTER COLUMN idquestion SET DEFAULT nextval('questions_idquestion_seq'::regclass);


--
-- Name: idsection; Type: DEFAULT; Schema: public; Owner: teddy.michel
--

ALTER TABLE sections ALTER COLUMN idsection SET DEFAULT nextval('sections_idsection_seq'::regclass);


--
-- Name: idue; Type: DEFAULT; Schema: public; Owner: teddy.michel
--

ALTER TABLE ue ALTER COLUMN idue SET DEFAULT nextval('ue_idue_seq'::regclass);


--
-- Data for Name: charge; Type: TABLE DATA; Schema: public; Owner: teddy.michel
--

COPY charge (idcharge, nomcharge, nbhrcharge, idens) FROM stdin;
15	Responsable MLO	0	2
16	Responsable MOM	0	4
17	Responsable ISI	0	3
18	Responsable IPI	0	13
20	Responsable IBD	0	15
21	Responsable MAN	0	10
22	Responsable MST	0	8
23	Responsable MPR	0	9
24	Responsable ECG	0	5
25	Responsable ECA	0	5
27	Responsable IAP1	0	2
28	Responsable IAP2	0	12
29	Responsable IAP 3	0	11
30	Responsable CAN	0	14
1	Responsable Math 1 (1A)	0	2
2	Responsable Math 2 (1A)	0	8
3	Responsable Algorithmique - Programmation (1A)	0	2
5	Responsable Application (1A)	0	13
4	Responsable Matériel & Systèmes (1A)	0	3
6	Responsable Formation humaine (1A)	0	14
7	Responsable Économie-Gestion (1A)	0	5
0	Directeur des etudes	0	3
10	Responsable IA et Informatique fondamentale	0	1
11	Responsable Maths (2A)	0	1
12	Responsable Organisation (2A)	0	1
14	Responsable du tronc commun (3A)	0	1
13	Responsable Formation humaine (2A)	0	1
26	Responsable EGI	0	1
8	Responsable Génie logiciel (2A)	0	1
9	Responsable Systèmes & Réseaux (2A)	0	1
19	Responsable ECO	0	1
\.


--
-- Data for Name: choixcoursmagistraux; Type: TABLE DATA; Schema: public; Owner: teddy.michel
--

COPY choixcoursmagistraux (idensmt, idens, datechoix, nbhrcm) FROM stdin;
5	4	2000-01-01	0
6	9	2000-01-01	0
3	2	2000-01-01	0
7	8	2000-01-01	0
8	10	2000-01-01	0
9	7	2000-01-01	0
10	3	2000-01-01	0
11	2	2000-01-01	0
12	12	2000-01-01	0
13	11	2000-01-01	0
14	15	2000-01-01	0
15	13	2000-01-01	0
4	16	2009-08-08	4
17	5	2000-01-01	0
18	5	2000-01-01	0
19	5	2000-01-01	0
20	14	2000-01-01	0
\.


--
-- Data for Name: choixtd; Type: TABLE DATA; Schema: public; Owner: teddy.michel
--

COPY choixtd (idensmt, idens, nbgroupestd, datechoix, nbhrpargroupe) FROM stdin;
4	17	4	2009-06-05	4
14	1	2	2009-09-10	98
4	16	4	2009-05-05	4
\.


--
-- Data for Name: choixtp; Type: TABLE DATA; Schema: public; Owner: teddy.michel
--

COPY choixtp (idensmt, idens, nbgroupestp, datechoix, nbhrpargroupe) FROM stdin;
14	1	2	2009-09-10	5
\.


--
-- Data for Name: conditionnelle; Type: TABLE DATA; Schema: public; Owner: teddy.michel
--

COPY conditionnelle (ideleve, idue1manquee, idue2manquee) FROM stdin;
1567	1	2
\.


--
-- Data for Name: droits; Type: TABLE DATA; Schema: public; Owner: teddy.michel
--

COPY droits (idens, idensmt) FROM stdin;
2	14
7	9
7	14
\.


--
-- Data for Name: eleve; Type: TABLE DATA; Schema: public; Owner: teddy.michel
--

COPY eleve (ideleve, login, motdepasse, promo, groupe, sousgroupe, grade, entree, idoption1, idoption2, idoption3) FROM stdin;
1185	adnane.baddouh	mdp	2012	1	1	1	2009	\N	\N	\N
1186	adrian.vandier-ast	mdp	2012	1	1	1	2009	\N	\N	\N
1187	ahmed.bakhtaoui	mdp	2012	1	1	1	2009	\N	\N	\N
1188	alexandre.michelis	mdp	2012	1	1	1	2009	\N	\N	\N
1189	alexandre.montecucco	mdp	2012	1	1	1	2009	\N	\N	\N
1190	alexandre.pastorino	mdp	2012	1	1	1	2009	\N	\N	\N
1191	alioune.sow	mdp	2012	1	1	1	2009	\N	\N	\N
1192	anass.chakiralaoui	mdp	2012	1	1	1	2009	\N	\N	\N
1193	andreolivier.atebamandeng	mdp	2012	1	1	1	2009	\N	\N	\N
1194	anthony.dorme	mdp	2012	1	1	1	2009	\N	\N	\N
1195	anthony.luneau	mdp	2012	1	1	1	2009	\N	\N	\N
1196	antoine.menciere	mdp	2012	1	1	1	2009	\N	\N	\N
1197	antony.turpin	mdp	2012	1	1	1	2009	\N	\N	\N
1198	ariane.lemaire	mdp	2012	1	1	1	2009	\N	\N	\N
1199	artem.timoshkin	mdp	2012	1	1	1	2009	\N	\N	\N
1200	aude.giraud	mdp	2012	1	1	1	2009	\N	\N	\N
1201	aurelien.marteau	mdp	2012	1	1	1	2009	\N	\N	\N
1202	benjamin.herlin	mdp	2012	1	1	1	2009	\N	\N	\N
1203	benoit.bataille	mdp	2012	1	1	1	2009	\N	\N	\N
1204	benoit.waag	mdp	2012	1	1	1	2009	\N	\N	\N
1205	boris.maguet	mdp	2012	1	1	1	2009	\N	\N	\N
1206	bruno.maury	mdp	2012	1	1	1	2009	\N	\N	\N
1207	chaoran.zhong	mdp	2012	1	1	1	2009	\N	\N	\N
1208	charles.vatin	mdp	2012	1	1	1	2009	\N	\N	\N
1209	charly.florent	mdp	2012	1	1	1	2009	\N	\N	\N
1210	christophe.mongin	mdp	2012	1	1	1	2009	\N	\N	\N
1211	claire.schaefer	mdp	2012	1	1	1	2009	\N	\N	\N
1212	clement.amiot	mdp	2012	1	1	1	2009	\N	\N	\N
1213	clement.olivares	mdp	2012	1	1	1	2009	\N	\N	\N
1214	clement.prevost	mdp	2012	1	1	1	2009	\N	\N	\N
1215	corentin.deluce	mdp	2012	1	1	1	2009	\N	\N	\N
1216	cyril.jouve	mdp	2012	1	1	1	2009	\N	\N	\N
1217	david.aurat	mdp	2012	1	1	1	2009	\N	\N	\N
1218	edouard.pichonnier	mdp	2012	1	1	1	2009	\N	\N	\N
1219	elodie.fourny	mdp	2012	1	1	1	2009	\N	\N	\N
1220	emeric.roverch	mdp	2012	1	1	1	2009	\N	\N	\N
1221	fabien.fargere	mdp	2012	1	1	1	2009	\N	\N	\N
1222	florian.laroumagne	mdp	2012	1	1	1	2009	\N	\N	\N
1223	franck.garnier	mdp	2012	1	1	1	2009	\N	\N	\N
1224	francois.cheynier	mdp	2012	1	1	1	2009	\N	\N	\N
1225	frederic.guerard	mdp	2012	1	1	1	2009	\N	\N	\N
1226	frederic.maquin	mdp	2012	1	1	1	2009	\N	\N	\N
1227	gen.yang	mdp	2012	1	1	1	2009	\N	\N	\N
1228	gregoire.cotte	mdp	2012	1	1	1	2009	\N	\N	\N
1229	gregory.szwarc	mdp	2012	1	1	1	2009	\N	\N	\N
1230	guillaume.hauke	mdp	2012	1	1	1	2009	\N	\N	\N
1231	guillaume.sabot	mdp	2012	1	1	1	2009	\N	\N	\N
1232	haitem.korfed	mdp	2012	1	1	1	2009	\N	\N	\N
1233	hajar.tahri	mdp	2012	1	1	1	2009	\N	\N	\N
1234	hermann.olivi	mdp	2012	1	1	1	2009	\N	\N	\N
1235	herve.tamtotiam	mdp	2012	1	1	1	2009	\N	\N	\N
1236	hoangmichael.nguyen	mdp	2012	1	1	1	2009	\N	\N	\N
1237	imad.kourde	mdp	2012	1	1	1	2009	\N	\N	\N
1238	ines.chafei	mdp	2012	1	1	1	2009	\N	\N	\N
1239	ionut.iortoman	mdp	2012	1	1	1	2009	\N	\N	\N
1240	isabelle.tran-khoi	mdp	2012	1	1	1	2009	\N	\N	\N
1241	jean-francois.riquart	mdp	2012	1	1	1	2009	\N	\N	\N
1242	jerome.desboeufs	mdp	2012	1	1	1	2009	\N	\N	\N
1243	jocelyn.bretey	mdp	2012	1	1	1	2009	\N	\N	\N
1244	jonathan.bronner	mdp	2012	1	1	1	2009	\N	\N	\N
1245	joris.abalea	mdp	2012	1	1	1	2009	\N	\N	\N
1246	julien.dauphant	mdp	2012	1	1	1	2009	\N	\N	\N
1247	julien.michelet	mdp	2012	1	1	1	2009	\N	\N	\N
1248	justine.delovinfosse	mdp	2012	1	1	1	2009	\N	\N	\N
1249	kevin.landa	mdp	2012	1	1	1	2009	\N	\N	\N
1250	loic.arnoult	mdp	2012	1	1	1	2009	\N	\N	\N
1251	louis.renault	mdp	2012	1	1	1	2009	\N	\N	\N
1252	louis.tellier	mdp	2012	1	1	1	2009	\N	\N	\N
1253	marco.caradonna	mdp	2012	1	1	1	2009	\N	\N	\N
1254	marc.vanderwal	mdp	2012	1	1	1	2009	\N	\N	\N
1255	marien.ritzenthaler	mdp	2012	1	1	1	2009	\N	\N	\N
1256	maxime.ewoane	mdp	2012	1	1	1	2009	\N	\N	\N
1257	maxime.khoy	mdp	2012	1	1	1	2009	\N	\N	\N
1258	maxime.lenoir	mdp	2012	1	1	1	2009	\N	\N	\N
1259	melanie.dumon	mdp	2012	1	1	1	2009	\N	\N	\N
1260	mickael.coiffec-pennec	mdp	2012	1	1	1	2009	\N	\N	\N
1261	mohamedfadhel.azouzi	mdp	2012	1	1	1	2009	\N	\N	\N
1262	moussamamadou.diallo	mdp	2012	1	1	1	2009	\N	\N	\N
1263	nathalie.ngor	mdp	2012	1	1	1	2009	\N	\N	\N
1264	nicolas.braquart	mdp	2012	1	1	1	2009	\N	\N	\N
1265	nicolas.depierreux	mdp	2012	1	1	1	2009	\N	\N	\N
1266	nicolas.gasull	mdp	2012	1	1	1	2009	\N	\N	\N
1267	nicolas.godinho	mdp	2012	1	1	1	2009	\N	\N	\N
1268	nicolas.weber	mdp	2012	1	1	1	2009	\N	\N	\N
1269	pierre.boutry	mdp	2012	1	1	1	2009	\N	\N	\N
1270	pierre.gargam	mdp	2012	1	1	1	2009	\N	\N	\N
1271	pierre.mauvy	mdp	2012	1	1	1	2009	\N	\N	\N
1272	quang-van.dam	mdp	2012	1	1	1	2009	\N	\N	\N
1273	quentin.garcia	mdp	2012	1	1	1	2009	\N	\N	\N
1274	quentin.jeauffroy	mdp	2012	1	1	1	2009	\N	\N	\N
1275	radoine.abdelkaoui	mdp	2012	1	1	1	2009	\N	\N	\N
1276	remi.czternasty	mdp	2012	1	1	1	2009	\N	\N	\N
1277	remi.gerboud	mdp	2012	1	1	1	2009	\N	\N	\N
1278	remi.takase	mdp	2012	1	1	1	2009	\N	\N	\N
1279	robin.gosswiller	mdp	2012	1	1	1	2009	\N	\N	\N
1280	romain.allain	mdp	2012	1	1	1	2009	\N	\N	\N
1281	romain.francez	mdp	2012	1	1	1	2009	\N	\N	\N
1282	sara.iraqi	mdp	2012	1	1	1	2009	\N	\N	\N
1283	sebastien.brugulat	mdp	2012	1	1	1	2009	\N	\N	\N
1284	shunjia.chen	mdp	2012	1	1	1	2009	\N	\N	\N
1285	simon.becuwe	mdp	2012	1	1	1	2009	\N	\N	\N
1286	simon.lasnier	mdp	2012	1	1	1	2009	\N	\N	\N
1287	sothkhemarith.thou	mdp	2012	1	1	1	2009	\N	\N	\N
1288	stanislas.minaev	mdp	2012	1	1	1	2009	\N	\N	\N
1289	stanislas.morbieu	mdp	2012	1	1	1	2009	\N	\N	\N
1290	sybil.deboin	mdp	2012	1	1	1	2009	\N	\N	\N
1291	thibaud.dubuisson	mdp	2012	1	1	1	2009	\N	\N	\N
1292	thibaud.lagelouze	mdp	2012	1	1	1	2009	\N	\N	\N
1293	thibaut.lefebvre	mdp	2012	1	1	1	2009	\N	\N	\N
1294	thomas.comes	mdp	2012	1	1	1	2009	\N	\N	\N
1295	thomas.pitiot	mdp	2012	1	1	1	2009	\N	\N	\N
1296	tuan-anhpascal.nguyen	mdp	2012	1	1	1	2009	\N	\N	\N
1297	valentin.shamsnejad	mdp	2012	1	1	1	2009	\N	\N	\N
1298	valerian.broussard	mdp	2012	1	1	1	2009	\N	\N	\N
1299	vincent.grieu	mdp	2012	1	1	1	2009	\N	\N	\N
1300	xavier.raux	mdp	2012	1	1	1	2009	\N	\N	\N
1301	yacine.kerkouche	mdp	2012	1	1	1	2009	\N	\N	\N
1302	yann.arbaud	mdp	2012	1	1	1	2009	\N	\N	\N
1303	yasmine.benkiran	mdp	2012	1	1	1	2009	\N	\N	\N
1304	yasmine.harbit	mdp	2012	1	1	1	2009	\N	\N	\N
1305	yasmine.joundy	mdp	2012	1	1	1	2009	\N	\N	\N
1306	yassine.loudad	mdp	2012	1	1	1	2009	\N	\N	\N
1307	yves.lelievre	mdp	2012	1	1	1	2009	\N	\N	\N
1362	adrien.domurado	mdp	2010	1	1	3	2007	\N	\N	\N
1363	adrien.dong	mdp	2010	1	1	3	2007	\N	\N	\N
1364	agathe.coblence	mdp	2010	1	1	3	2007	\N	\N	\N
1365	alexandre.baroin	mdp	2010	1	1	3	2007	\N	\N	\N
1366	alexandre.chouraqui	mdp	2010	1	1	3	2007	\N	\N	\N
1367	alexandre.krol	mdp	2010	1	1	3	2007	\N	\N	\N
1368	alexandre.monzie	mdp	2010	1	1	3	2007	\N	\N	\N
1369	alexandre.notebaert	mdp	2010	1	1	3	2007	\N	\N	\N
1370	alice.gio	mdp	2010	1	1	3	2007	\N	\N	\N
1371	alice.mandin	mdp	2010	1	1	3	2007	\N	\N	\N
1372	anais.chesneau	mdp	2010	1	1	3	2007	\N	\N	\N
1373	anas.mgassy	mdp	2010	1	1	3	2007	\N	\N	\N
1374	annaelle.marc	mdp	2010	1	1	3	2007	\N	\N	\N
1375	antoine.artaud-macari	mdp	2010	1	1	3	2007	\N	\N	\N
1376	audric.castillan	mdp	2010	1	1	3	2007	\N	\N	\N
1377	bastien.caudan	mdp	2010	1	1	3	2007	\N	\N	\N
1378	bastien.chou	mdp	2010	1	1	3	2007	\N	\N	\N
1379	bastien.ricono	mdp	2010	1	1	3	2007	\N	\N	\N
1380	catherine.in	mdp	2010	1	1	3	2007	\N	\N	\N
1382	charles-edouard.poisnel	mdp	2010	1	1	3	2007	\N	\N	\N
1383	charles.santi	mdp	2010	1	1	3	2007	\N	\N	\N
1384	christian.vieira	mdp	2010	1	1	3	2007	\N	\N	\N
1385	christophe.convert	mdp	2010	1	1	3	2007	\N	\N	\N
1386	codou.ndiaye	mdp	2010	1	1	3	2007	\N	\N	\N
1387	cyril.muller	mdp	2010	1	1	3	2007	\N	\N	\N
1388	damien.fouquet	mdp	2010	1	1	3	2007	\N	\N	\N
1389	damien.ricaud	mdp	2010	1	1	3	2007	\N	\N	\N
1390	delphine.leduc	mdp	2010	1	1	3	2007	\N	\N	\N
1391	dmitri.voitsekhovitch	mdp	2010	1	1	3	2007	\N	\N	\N
1392	dominique.padiou	mdp	2010	1	1	3	2007	\N	\N	\N
1393	dorine.hipp	mdp	2010	1	1	3	2007	\N	\N	\N
1394	elmehdi.guennoun	mdp	2010	1	1	3	2007	\N	\N	\N
1395	elsa.claudet	mdp	2010	1	1	3	2007	\N	\N	\N
1396	emmanuel.badet	mdp	2010	1	1	3	2007	\N	\N	\N
1397	evan.liomain	mdp	2010	1	1	3	2007	\N	\N	\N
1398	fabien.millou	mdp	2010	1	1	3	2007	\N	\N	\N
1399	fabrice.flavigne	mdp	2010	1	1	3	2007	\N	\N	\N
1400	fares.souissi	mdp	2010	1	1	3	2007	\N	\N	\N
1401	fatimazahra.mezouar	mdp	2010	1	1	3	2007	\N	\N	\N
1402	fatoumata.ngom	mdp	2010	1	1	3	2007	\N	\N	\N
1403	francois.merot	mdp	2010	1	1	3	2007	\N	\N	\N
1404	frederic.isacesco	mdp	2010	1	1	3	2007	\N	\N	\N
1405	frederic.wang	mdp	2010	1	1	3	2007	\N	\N	\N
1406	guido.manfredi	mdp	2010	1	1	3	2007	\N	\N	\N
1407	guillaume.delugre	mdp	2010	1	1	3	2007	\N	\N	\N
1408	halima.louati	mdp	2010	1	1	3	2007	\N	\N	\N
1409	hanaa.didi	mdp	2010	1	1	3	2007	\N	\N	\N
1410	heythem.gzara	mdp	2010	1	1	3	2007	\N	\N	\N
1411	hugues.cauvin	mdp	2010	1	1	3	2007	\N	\N	\N
1412	imene.djedeat	mdp	2010	1	1	3	2007	\N	\N	\N
1413	jean-baptiste.dijols	mdp	2010	1	1	3	2007	\N	\N	\N
1414	jeanchristophe.gatuingt	mdp	2010	1	1	3	2007	\N	\N	\N
1415	jean-damien.hatzenbuhler	mdp	2010	1	1	3	2007	\N	\N	\N
1416	jeremie.habiballah	mdp	2010	1	1	3	2007	\N	\N	\N
1417	jeremy.amode	mdp	2010	1	1	3	2007	\N	\N	\N
1418	jeremy.gilbert	mdp	2010	1	1	3	2007	\N	\N	\N
1419	jeremy.lamboley	mdp	2010	1	1	3	2007	\N	\N	\N
1420	jeremy.sule	mdp	2010	1	1	3	2007	\N	\N	\N
1421	johann.broudin	mdp	2010	1	1	3	2007	\N	\N	\N
1422	jonathan.nahon	mdp	2010	1	1	3	2007	\N	\N	\N
1423	jonathan.sanchez	mdp	2010	1	1	3	2007	\N	\N	\N
1424	julien.dutto	mdp	2010	1	1	3	2007	\N	\N	\N
1425	kevin.salabert	mdp	2010	1	1	3	2007	\N	\N	\N
1426	laure.mariucci	mdp	2010	1	1	3	2007	\N	\N	\N
1427	louis.volant	mdp	2010	1	1	3	2007	\N	\N	\N
1428	ludovic.balon	mdp	2010	1	1	3	2007	\N	\N	\N
1429	maher.omar	mdp	2010	1	1	3	2007	\N	\N	\N
1430	marie-christine.codarini	mdp	2010	1	1	3	2007	\N	\N	\N
1431	martial.lepauvre	mdp	2010	1	1	3	2007	\N	\N	\N
1432	mathieu.dammien	mdp	2010	1	1	3	2007	\N	\N	\N
1433	mathieu.landre	mdp	2010	1	1	3	2007	\N	\N	\N
1434	matthieu.mariapragassam	mdp	2010	1	1	3	2007	\N	\N	\N
1435	maxime.rousseau	mdp	2010	1	1	3	2007	\N	\N	\N
1436	miao.cai	mdp	2010	1	1	3	2007	\N	\N	\N
1437	mickael.chokron	mdp	2010	1	1	3	2007	\N	\N	\N
1438	momargaye.coundoul	mdp	2010	1	1	3	2007	\N	\N	\N
1439	nabil.mhenni	mdp	2010	1	1	3	2007	\N	\N	\N
1440	nathan.perianayagassamy	mdp	2010	1	1	3	2007	\N	\N	\N
1441	nicolas.ignace	mdp	2010	1	1	3	2007	\N	\N	\N
1442	nicolas.levain	mdp	2010	1	1	3	2007	\N	\N	\N
1443	nicolas.quirin	mdp	2010	1	1	3	2007	\N	\N	\N
1444	olivier.aujoulat	mdp	2010	1	1	3	2007	\N	\N	\N
1445	olivier.cros	mdp	2010	1	1	3	2007	\N	\N	\N
1446	olivier.lecoutre	mdp	2010	1	1	3	2007	\N	\N	\N
1447	olivier.petchy	mdp	2010	1	1	3	2007	\N	\N	\N
1448	olivier.roland	mdp	2010	1	1	3	2007	\N	\N	\N
1449	omar.harbi	mdp	2010	1	1	3	2007	\N	\N	\N
1450	orjouane.benharrath	mdp	2010	1	1	3	2007	\N	\N	\N
1451	paul.gregoire	mdp	2010	1	1	3	2007	\N	\N	\N
1452	paul.nguyen	mdp	2010	1	1	3	2007	\N	\N	\N
1453	philippe.barbe	mdp	2010	1	1	3	2007	\N	\N	\N
1454	philippe.valembois	mdp	2010	1	1	3	2007	\N	\N	\N
1455	pierre.cabeza	mdp	2010	1	1	3	2007	\N	\N	\N
1456	pierre.guilbault	mdp	2010	1	1	3	2007	\N	\N	\N
1457	pierre.trouve	mdp	2010	1	1	3	2007	\N	\N	\N
1458	pierre-yves.labastie-coeyrehourcq	mdp	2010	1	1	3	2007	\N	\N	\N
1459	quentin.farizon	mdp	2010	1	1	3	2007	\N	\N	\N
1460	rachid.ounit	mdp	2010	1	1	3	2007	\N	\N	\N
1461	raphael.guilmin	mdp	2010	1	1	3	2007	\N	\N	\N
1462	raphael.renard	mdp	2010	1	1	3	2007	\N	\N	\N
1463	rhizlane.elrhomri	mdp	2010	1	1	3	2007	\N	\N	\N
1464	richard.chong	mdp	2010	1	1	3	2007	\N	\N	\N
1465	roland.sadaka	mdp	2010	1	1	3	2007	\N	\N	\N
1466	romain.cotte	mdp	2010	1	1	3	2007	\N	\N	\N
1467	ronan.letoullec	mdp	2010	1	1	3	2007	\N	\N	\N
1468	sabine.trefond	mdp	2010	1	1	3	2007	\N	\N	\N
1469	sabrina.fahim	mdp	2010	1	1	3	2007	\N	\N	\N
1470	safouene.benmansour	mdp	2010	1	1	3	2007	\N	\N	\N
1471	samuel.coupe	mdp	2010	1	1	3	2007	\N	\N	\N
1472	sanjay.talreja	mdp	2010	1	1	3	2007	\N	\N	\N
1473	sebastien.drure	mdp	2010	1	1	3	2007	\N	\N	\N
1474	sebastien.juglar	mdp	2010	1	1	3	2007	\N	\N	\N
1475	sebastien.monchamps	mdp	2010	1	1	3	2007	\N	\N	\N
1476	sonia.kostenko	mdp	2010	1	1	3	2007	\N	\N	\N
1477	stephanie.paina	mdp	2010	1	1	3	2007	\N	\N	\N
1478	thibaud.rohmer	mdp	2010	1	1	3	2007	\N	\N	\N
1479	thibaut.theret	mdp	2010	1	1	3	2007	\N	\N	\N
1480	thomas.andrejak	mdp	2010	1	1	3	2007	\N	\N	\N
1481	thomas.fayolle	mdp	2010	1	1	3	2007	\N	\N	\N
1483	tom.delmas	mdp	2010	1	1	3	2007	\N	\N	\N
1484	toufik.djaider	mdp	2010	1	1	3	2007	\N	\N	\N
1485	victor.houlet	mdp	2010	1	1	3	2007	\N	\N	\N
1486	vincent.bonesteve	mdp	2010	1	1	3	2007	\N	\N	\N
1487	vincent.nimal	mdp	2010	1	1	3	2007	\N	\N	\N
1488	virgile.fritsch	mdp	2010	1	1	3	2007	\N	\N	\N
1489	virginie.quach	mdp	2010	1	1	3	2007	\N	\N	\N
1490	wajdi.ayed	mdp	2010	1	1	3	2007	\N	\N	\N
1491	walid.elferdi	mdp	2010	1	1	3	2007	\N	\N	\N
1492	willy.vedrine	mdp	2010	1	1	3	2007	\N	\N	\N
1493	wilson.jouet	mdp	2010	1	1	3	2007	\N	\N	\N
1494	xavier.coppin	mdp	2010	1	1	3	2007	\N	\N	\N
1495	yann.leenhardt	mdp	2010	1	1	3	2007	\N	\N	\N
1496	yiqi.zhu	mdp	2010	1	1	3	2007	\N	\N	\N
1497	youen.jarsale	mdp	2010	1	1	3	2007	\N	\N	\N
1498	youness.boukakiou	mdp	2010	1	1	3	2007	\N	\N	\N
1499	yuge.huang	mdp	2010	1	1	3	2007	\N	\N	\N
1566	teddy.michel	mdp	2012	2	1	1	2009	\N	\N	\N
1567	florent.couffe	mdp	2011	2	1	2	2008	\N	\N	\N
1381	cedric.bouhier	mdp	2010	1	1	3	2007	15	16	17
1482	tiffany.havan	mdp	2010	1	1	3	2007	12	14	16
\.


--
-- Data for Name: eleveensmt; Type: TABLE DATA; Schema: public; Owner: teddy.michel
--

COPY eleveensmt (ideleve, idensmt, dateenvoi) FROM stdin;
1277	9	2010-04-30
1277	3	2010-04-30
1200	3	2010-04-30
1202	3	2010-04-30
1194	3	2010-04-30
1566	9	2010-05-03
1566	14	2010-05-03
1230	9	2010-05-03
1201	9	2010-05-03
1300	9	2010-05-03
1286	9	2010-05-03
1195	9	2010-05-03
1198	9	2010-05-03
1207	9	2010-05-03
1208	9	2010-05-03
1207	10	2010-05-03
1566	3	2010-05-03
1566	4	2010-05-03
1207	4	2010-05-03
1208	4	2010-05-03
1286	4	2010-05-03
\.


--
-- Data for Name: enseignant; Type: TABLE DATA; Schema: public; Owner: teddy.michel
--

COPY enseignant (idens, nomens, prenomsens, nbhrreelles, courriel, reliquat, salariepublic, login, motdepasse, autresheures) FROM stdin;
5	Godard	Patrick	0	pat@expertcomptables.org	0	0	patrick	fuckfisc	0
3	Berthelol	Gerard	12	gege@unixkiller.org	0	1	gege	elvis	0
7	Autin	Alain	42	alain.autin@ancienscombattants.org	0	0	captainvietnam	vhdl	0
8	Ly Vath	Vathana	42	lyvath@ensiie.fr	0	0	lyvath	mdp	0
9	Dossantos	Pierre	0	dossantos@cea.fr	0	0	pierre	mdp	0
10	Torri	Robert	0	robert@evry.fr	0	0	robert	mdp	0
11	Forest	Julien	0	forest@ensiie.fr	0	0	cafe2clopes	cafe2clopes	0
12	Riolol	Robert	0	riolol@ensiie.fr	0	0	riolol	lol	0
14	Viegnes	Cecile	0	cecile@viegnes.org	0	0	cecile	mdp	0
15	Jouve	Mireille	0	jouve@ensiie.fr	0	0	jouve	mdp	0
2	Dubois	Catherine	0	dubois@ensiie.fr	0	0	dubois	mdp	0
1	Grau	Brigitte	0	brigitte.grau@ensiie.fr	0	1	grau	mdp	0
4	Faye	Alain	42	alain.faye@planeur.org	0	1	faye	mouette	0
13	Ligozat	Anne Laure	0	annelaure@ligozat.fr	0	0	annelaure	1001	0
16	Costa	Madame	0		0	0	costa	mdp	0
17	Lambert	Mlle	0		0	0	lambert	mdp	0
\.


--
-- Data for Name: enseignement; Type: TABLE DATA; Schema: public; Owner: teddy.michel
--

COPY enseignement (idensmt, idue, idoption, nbhrcmtotal, code, nbhrtdtotal, intitule, datedebut, datefin) FROM stdin;
3	1	1	42	MLO	42	Logique	2000-01-01	2000-01-01
4	1	1	42	MTG	42	Théorie des graphes	2000-01-01	2000-01-01
6	2	1	42	MPR	42	Probabilités	2000-01-01	2000-01-01
7	2	1	42	MST	42	Statistiques	2000-01-01	2000-01-01
8	2	1	42	MAN	42	Analyse numérique	2000-01-01	2000-01-01
9	4	1	42	IPM	42	Assembleur et Projet microprocesseur	2000-01-01	2000-01-01
10	4	1	42	ISI	42	Systèmes informatiques	2000-01-01	2000-01-01
11	3	1	42	IAP1	42	Algorithmique 1	2000-01-01	2000-01-01
12	3	1	42	IAP2	42	Algorithmique 2	2000-01-01	2000-01-01
13	3	1	42	IAP3	42	Algorithmique 3	2000-01-01	2000-01-01
14	5	1	42	IBD	42	Base de données	2000-01-01	2000-01-01
15	5	1	42	IPI	42	Projet informatique	2000-01-01	2000-01-01
16	7	1	42	ECO	42	Economie	2000-01-01	2000-01-01
17	7	1	42	ECG	42	Gestion comptable et financière	2000-01-01	2000-01-01
18	7	1	42	EGI	42	Gestion des investissements	2000-01-01	2000-01-01
5	1	1	42	MOM	42	Optimisation	2000-01-01	2000-01-01
19	7	1	42	ECA	42	Analyse des couts	2000-01-01	2000-01-01
20	6	1	42	CAN	42	Anglais	2000-01-01	2000-01-01
21	6	1	42	COM	42	Marketing de carrière et communication	2000-01-01	2000-01-01
22	9	2	42	IPC2	42	Programmation Concurrente	2000-01-01	2000-01-01
23	9	2	42	IRE2	42	Reseau et applications réparties	2000-01-01	2000-01-01
24	9	2	42	ICO2	42	Compilation	2000-01-01	2000-01-01
25	8	2	42	ILO2	42	Langages orientés objets	2000-01-01	2000-01-01
26	8	2	42	SIA2	42	Analyse et conception des systèmes d'informations	2000-01-01	2000-01-01
27	8	2	42	ISF2	42	Specification & Systèmes formels	2000-01-01	2000-01-01
28	8	2	42	ITE2	42	Test du logiciel	2000-01-01	2000-01-01
29	10	2	42	MLC2	42	Calculabilité	2000-01-01	2000-01-01
30	10	2	42	MLSF2	42	Langages et Systèmes formels	2000-01-01	2000-01-01
31	10	2	42	IIA2	42	Intelligence Artificielle	2000-01-01	2000-01-01
32	10	2	42	IPG2	42	Programmation logique	2000-01-01	2000-01-01
33	10	2	42	MPM2	42	Projet Math - Info	2000-01-01	2000-01-01
34	11	2	42	MRO2	42	Recherche Operationnelle	2000-01-01	2000-01-01
35	11	2	42	MAD2	42	Analyse de données	2000-01-01	2000-01-01
36	11	2	42	MPM2	42	Projet Math Info 1	2000-01-01	2000-01-01
37	12	2	42	EDR2	42	Droit civil et commercial	2000-01-01	2000-01-01
38	12	2	42	EMG2	42	Management général de l'entreprise	2000-01-01	2000-01-01
39	12	2	42	EGP2	42	Gestion de production	2000-01-01	2000-01-01
40	12	2	42	EMP2	42	Management de projet	2000-01-01	2000-01-01
41	12	2	42	EGS2	42	Gestion des stocks et budgétaire	2000-01-01	2000-01-01
42	12	2	42	ECE2	42	Création d'entreprise	2000-01-01	2000-01-01
43	13	2	42	CAN2	42	Anglais	2000-01-01	2000-01-01
44	13	2	42	LV2	42	Langue Vivante 2	2000-01-01	2000-01-01
45	13	2	42	EPI2	42	Epistemologie	2000-01-01	2000-01-01
46	13	2	42	CFI2	42	Confrontation et initiative	2000-01-01	2000-01-01
48	15	5	42	ISE2	42	Système d'exploitation	2000-01-01	2000-01-01
49	15	5	42	LAN2	42	Administration d'un lan	2000-01-01	2000-01-01
50	15	5	42	IPR2	42	Projet Système	2000-01-01	2000-01-01
51	16	6	42	IRO2	42	Robotique	2000-01-01	2000-01-01
52	16	6	42	IRV2	42	Realité virtuelle	2000-01-01	2000-01-01
53	16	6	42	IVA2	42	Vision artificielle	2000-01-01	2000-01-01
56	17	7	42	EAS2	42	Assurance 	2000-01-01	2000-01-01
57	17	7	42	EMF2	42	Introduction aux mathématique financières 	2000-01-01	2000-01-01
58	17	7	42	ETM	42	Theorie de la finance moderne	2000-01-01	2000-01-01
59	17	7	42	EPR2	42	Projet	2000-01-01	2000-01-01
60	18	4	42	IASI2	42	Architecture des SI - Architecture logicielle	2000-01-01	2000-01-01
61	18	4	42	IBD2	42	Base de données	2000-01-01	2000-01-01
62	18	4	42	IPR2	42	Projet	2000-01-01	2000-01-01
63	14	3	42	IQL3	42	Qualité du logiciel	2000-01-01	2000-01-01
64	14	3	42	EJE3	42	Jeu d'entreprise	2000-01-01	2000-01-01
65	14	3	42	EMP3	42	Management de projet 	2000-01-01	2000-01-01
66	14	3	42	EDI3	42	Droit de l'informatique	2000-01-01	2000-01-01
67	19	8	42	IOSP3	42	Outils sémantiques pour le parallélisme	2000-01-01	2000-01-01
68	19	8	42	ILSP3	42	Langages pour les systèmes parallèles	2000-01-01	2000-01-01
69	19	8	42	ITR3	42	Systèmes réactifs temps réel	2000-01-01	2000-01-01
70	19	8	42	IPR3	42	Projet de synthèse	2000-01-01	2000-01-01
71	20	9	42	ITP3	42	Techniques de preuves	2000-01-01	2000-01-01
72	20	9	42	ISL3	42	Sémantique des langages	2000-01-01	2000-01-01
73	20	9	42	IAS3	42	Analyse statique	2000-01-01	2000-01-01
74	20	9	42	IPR3	42	Projet 	2000-01-01	2000-01-01
75	21	10	42	MRN3	42	Reseaux de neurones	2000-01-01	2000-01-01
76	21	10	42	MRG3	42	Regression	2000-01-01	2000-01-01
77	21	10	42	MST3	42	Series temporelles	2000-01-01	2000-01-01
78	21	10	42	MAD3	42	Analyse des données et Data mining	2000-01-01	2000-01-01
79	21	10	42	IPR3	42	Projet	2000-01-01	2000-01-01
80	22	12	42	IRQ3	42	Routage et qualité de service dans l'internet	2000-01-01	2000-01-01
81	22	12	42	ISR3	42	Securité dans les reseaux	2000-01-01	2000-01-01
82	22	12	42	IPR3	42	Projet	2000-01-01	2000-01-01
83	23	13	42	IASI3	42	Architecture des SI - Architecture des logiciels	2000-01-01	2000-01-01
84	23	13	42	IBD3	42	Base de données 	2000-01-01	2000-01-01
85	23	13	42	IPR3	42	Projet 	2000-01-01	2000-01-01
86	24	14	42	MRO3	42	Recherche opérationnelle	2000-01-01	2000-01-01
87	24	14	42	MMP3	42	Methode polyédriques	2000-01-01	2000-01-01
88	24	14	42	ICA3	42	Compléxité des algorithmes	2000-01-01	2000-01-01
89	24	14	42	MPS3	42	Programmation semie-definie	2000-01-01	2000-01-01
90	24	14	42	MAR3	42	Applications de la recherche opérationnelle	2000-01-01	2000-01-01
91	24	14	42	MEC3	42	Etude de cas	2000-01-01	2000-01-01
92	25	15	42	EGC3	42	Gestion du changement 	2000-01-01	2000-01-01
93	25	15	42	ECO3	42	Gestion des connaissances	2000-01-01	2000-01-01
94	25	15	42	ECI3	42	L utilisateur en tant que client interne	2000-01-01	2000-01-01
95	25	15	42	ECF3	42	Le cycle fournisseur client	2000-01-01	2000-01-01
96	26	16	42	EFS3	42	Finance Stochastique	2000-01-01	2000-01-01
97	26	16	42	EIF3	42	Intruments financiers	2000-01-01	2000-01-01
98	26	16	42	EDC3	42	Derives de credits	2000-01-01	2000-01-01
99	26	16	42	IPR3	42	Projet	2000-01-01	2000-01-01
100	27	17	42	IAA3	42	Apprentissage artificiel	2000-01-01	2000-01-01
101	27	17	42	ILF3	42	Logique floue	2000-01-01	2000-01-01
102	27	17	42	IAD3	42	Intelligence artificielle distribuée	2000-01-01	2000-01-01
103	27	17	42	ITAL3	42	Traitement automatique de la langue 	2000-01-01	2000-01-01
104	27	17	42	IPR3	42	Projet	2000-01-01	2000-01-01
\.


--
-- Data for Name: formation; Type: TABLE DATA; Schema: public; Owner: teddy.michel
--

COPY formation (idformation, nomformation) FROM stdin;
1	1A
2	2A
3	3A
4	FIP1
\.


--
-- Data for Name: optionoutc; Type: TABLE DATA; Schema: public; Owner: teddy.michel
--

COPY optionoutc (idoption, idformation, nomoption, nbgroupestd, promo) FROM stdin;
1	1	Tronc commun (1A)	5	2012
2	2	Tronc commun (2A)	5	2011
13	3	Système d'information avancée et bases de données (3A)	5	2000
11	1	Projet personnel	5	2011
12	3	Reseau - Securité :) (3A)	5	2000
6	2	Robotique et réalité virtuelle (2A)	5	2011
9	3	Option 2 (Réseaux, Sécurité,...)	5	2010
8	3	Option 1 (Modélisation)	5	2010
4	2	Systèmes d'information et Bases de données (2A)	5	2011
5	2	Système d'exploitation (2A)	5	2011
7	2	Banque, Finance et Assurance (2A)	5	2011
3	3	Tronc commun (3A)	5	2010
10	3	Option 3 (Marché Fist) 	5	2010
14	3	Optimisation (3A)	5	2000
15	3	Nouvelles technologies et organisation des entreprises (3A)	5	2000
16	3	Marché Fist (3A)	5	2000
17	3	Intelligence artificielle (3A)	5	2000
\.


--
-- Data for Name: questionnaire; Type: TABLE DATA; Schema: public; Owner: teddy.michel
--

COPY questionnaire (idquestionnaire, idensmt, dateenvoi) FROM stdin;
1	9	2010-04-30
2	3	2010-04-30
3	3	2010-04-30
4	3	2010-04-30
5	3	2010-04-30
6	9	2010-05-03
7	14	2010-05-03
8	9	2010-05-03
9	9	2010-05-03
10	9	2010-05-03
11	9	2010-05-03
12	9	2010-05-03
13	9	2010-05-03
14	9	2010-05-03
15	9	2010-05-03
16	10	2010-05-03
17	3	2010-05-03
18	4	2010-05-03
19	4	2010-05-03
20	4	2010-05-03
21	4	2010-05-03
\.


--
-- Data for Name: questions; Type: TABLE DATA; Schema: public; Owner: teddy.michel
--

COPY questions (idquestion, intitule, typequestion, idsection, ordrequestion, orientation) FROM stdin;
33	Chargé de TP :	charge_tp	2	3	0
25	Avant de suivre cet enseignement, vous estimiez avoir une bonne connaissance de cette matière	note	5	26	0
26	Indiquez le pourcentage de cours suivis	nombre	5	27	0
27	Indiquez le pourcentage de TD-TP suivis	nombre	5	28	0
28	Evaluez l'intensité du travail personnel à fournir en dehors de l'ensignement	nombre	5	29	0
29	Sur quels point cet enseignement devrait-il être amélioré	texte	5	30	0
30	Comment cet enseignement devrait-il être amélioré	texte	5	31	0
31	Autres remarques	texte	5	32	0
24	Donner une note globale sur 20	note	5	25	0
15	Qualité pédagogique du chargé de cours	texte	4	16	0
16	Sur quels critères faites-vous cette évaluation	texte	4	17	0
17	L'enseignant de cours a suscité votre intérêt	note	4	18	0
18	Les exposés sont structurés	note	4	19	0
19	La forme de présentation (tableau, transparents, film, etc..) vous a bien convenu	note	4	20	0
20	Les documents fournis (notes, polycopiés, documentation en ligne) sont clairs, compréhensibles et adaptés	note	4	21	0
21	Des liens ont été établis entre théorie et pratique	note	4	22	0
22	L'enseignant est ouvert aux questions pédagogiques des élèves (compréhension du cours, etc...) 	note	4	23	0
23	Vous avez eu des réponses satisfaisantes aux questions	note	4	24	0
1	Globalement, vous vous estimez satisfait de cet enseignement	note	3	1	0
2	Les objectifs visés par l'enseignement ont été clairement expliqués	note	3	2	0
3	Le contenu de l'enseignement a correspondu à vos attentes	note	3	3	0
4	Le contenu de cet enseignement est clairement lié à votre future vie professionnelle	note	3	4	0
5	Des liens ont été établis entre l'enseignement et les pratiques professionnelles	note	3	5	0
6	Des liens ont été établis avec d'autres cours	note	3	6	0
7	Il n'y a pas de redites inutiles entre ce cours et  d'autres cours	note	3	7	0
8	Si il y a des redites, lesquelles	texte	3	8	0
32	Filière d'origine :	filiere	2	1	0
34	Chargé de TD :	charge_td	2	2	0
9	Vous n'avez pas eu de difficultés dans la compréhension du cours	note	3	9	0
14	L'évaluation du cours réalisé est en accord avec l'enseignement dispensé	note	3	14	0
11	Vous n'avez pas eu de difficultés dans la compréhension des TD	note	3	11	1
13	Si il a des parties du cours des TD/TP qui vous ont posés problème, lesquelles	texte	3	13	1
12	Vous n'avez pas eu de difficultés dans la compréhension des TP	note	3	12	1
10	S'il y a des TD ou TP pour cet enseignement, ils sont bien articulés avec le cours	note	3	10	1
\.


--
-- Data for Name: redoublement; Type: TABLE DATA; Schema: public; Owner: teddy.michel
--

COPY redoublement (ideleve, iduereussie) FROM stdin;
1201	1
\.


--
-- Data for Name: reponses; Type: TABLE DATA; Schema: public; Owner: teddy.michel
--

COPY reponses (idquestion, valeur, idquestionnaire, charge_td, charge_tp) FROM stdin;
1	2	8	\N	\N
2	1	8	\N	\N
1	0	2	\N	\N
2	0	2	\N	\N
3	0	2	\N	\N
4	0	2	\N	\N
5	0	2	\N	\N
6	0	2	\N	\N
7	0	2	\N	\N
8		2	\N	\N
9	0	2	\N	\N
10	0	2	\N	\N
11	0	2	\N	\N
12	0	2	\N	\N
13		2	\N	\N
14	0	2	\N	\N
15		2	\N	\N
16		2	\N	\N
17	0	2	\N	\N
18	0	2	\N	\N
19	0	2	\N	\N
20	0	2	\N	\N
21	0	2	\N	\N
22	0	2	\N	\N
23	0	2	\N	\N
24	0	2	\N	\N
25	0	2	\N	\N
29		2	\N	\N
30		2	\N	\N
31		2	\N	\N
3	1	8	\N	\N
1	3	3	\N	\N
2	5	3	\N	\N
3	3	3	\N	\N
4	2	3	\N	\N
5	2	3	\N	\N
6	2	3	\N	\N
7	4	3	\N	\N
4	1	8	\N	\N
1	0	4	\N	\N
2	0	4	\N	\N
3	0	4	\N	\N
4	0	4	\N	\N
5	0	4	\N	\N
6	0	4	\N	\N
7	0	4	\N	\N
8		4	\N	\N
9	0	4	\N	\N
10	0	4	\N	\N
11	0	4	\N	\N
12	0	4	\N	\N
13		4	\N	\N
14	0	4	\N	\N
15		4	\N	\N
16		4	\N	\N
17	0	4	\N	\N
18	0	4	\N	\N
19	0	4	\N	\N
20	0	4	\N	\N
21	0	4	\N	\N
22	0	4	\N	\N
23	0	4	\N	\N
24	0	4	\N	\N
25	0	4	\N	\N
29		4	\N	\N
30		4	\N	\N
31		4	\N	\N
5	2	8	\N	\N
1	5	5	\N	\N
2	3	5	\N	\N
3	4	5	\N	\N
4	5	5	\N	\N
5	4	5	\N	\N
6	4	5	\N	\N
7	3	5	\N	\N
8	\\O/ Elvis \\O/	5	\N	\N
9	5	5	\N	\N
10	3	5	\N	\N
11	2	5	\N	\N
12	5	5	\N	\N
13	\\O/ Elvis \\O/\\O/ Elvis \\O/\\O/ Elvis \\O/\\O/ Elvis \\O/\\O/ Elvis \\O/\\O/ Elvis \\O/\\O/ Elvis \\O/\\O/ Elvis \\O/	5	\N	\N
14	4	5	\N	\N
6	3	8	\N	\N
1	1	6	\N	\N
2	1	6	\N	\N
3	2	6	\N	\N
4	3	6	\N	\N
5	5	6	\N	\N
6	3	6	\N	\N
7	2	6	\N	\N
8		6	\N	\N
9	5	6	\N	\N
10	2	6	\N	\N
11	4	6	\N	\N
12	2	6	\N	\N
13		6	\N	\N
14	2	6	\N	\N
15		6	\N	\N
16		6	\N	\N
17	0	6	\N	\N
18	0	6	\N	\N
19	0	6	\N	\N
20	0	6	\N	\N
21	0	6	\N	\N
22	0	6	\N	\N
23	0	6	\N	\N
24	0	6	\N	\N
25	0	6	\N	\N
26	0	6	\N	\N
27	0	6	\N	\N
28	0	6	\N	\N
29		6	\N	\N
30		6	\N	\N
31		6	\N	\N
7	2	8	\N	\N
8		8	\N	\N
9	2	8	\N	\N
10	4	8	\N	\N
11	4	8	\N	\N
12	3	8	\N	\N
13	Toutes	8	\N	\N
14	0	8	\N	\N
15	J'en sais rien	8	\N	\N
16	Aucuns	8	\N	\N
17	5	8	\N	\N
18	4	8	\N	\N
19	3	8	\N	\N
20	3	8	\N	\N
21	2	8	\N	\N
22	1	8	\N	\N
23	2	8	\N	\N
24	3	8	\N	\N
25	2	8	\N	\N
26	0	8	\N	\N
27	0	8	\N	\N
28	0	8	\N	\N
29		8	\N	\N
30	J'en sais rien	8	\N	\N
31	Bonjour, je suis Guillaume Hauke !!!!!!! ;)	8	\N	\N
1	1	9	\N	\N
2	4	9	\N	\N
3	2	9	\N	\N
4	3	9	\N	\N
5	5	9	\N	\N
6	0	9	\N	\N
7	2	9	\N	\N
8		9	\N	\N
9	4	9	\N	\N
10	2	9	\N	\N
11	2	9	\N	\N
12	3	9	\N	\N
13		9	\N	\N
14	2	9	\N	\N
15		9	\N	\N
16		9	\N	\N
17	2	9	\N	\N
18	2	9	\N	\N
19	5	9	\N	\N
20	4	9	\N	\N
21	5	9	\N	\N
22	4	9	\N	\N
23	3	9	\N	\N
24	2	9	\N	\N
25	4	9	\N	\N
26	0	9	\N	\N
27	20	9	\N	\N
28	0	9	\N	\N
29		9	\N	\N
30		9	\N	\N
31	My name is Aurelien Marteau.	9	\N	\N
1	5	10	\N	\N
2	5	10	\N	\N
3	5	10	\N	\N
4	5	10	\N	\N
5	5	10	\N	\N
6	5	10	\N	\N
7	5	10	\N	\N
8		10	\N	\N
9	5	10	\N	\N
10	5	10	\N	\N
11	5	10	\N	\N
12	5	10	\N	\N
13		10	\N	\N
14	5	10	\N	\N
15		10	\N	\N
16		10	\N	\N
17	5	10	\N	\N
18	5	10	\N	\N
19	5	10	\N	\N
20	5	10	\N	\N
21	5	10	\N	\N
22	5	10	\N	\N
23	5	10	\N	\N
24	5	10	\N	\N
25	5	10	\N	\N
26	100	10	\N	\N
27	100	10	\N	\N
28	80	10	\N	\N
29		10	\N	\N
30		10	\N	\N
31	Je m'appelle Xavier Raux.	10	\N	\N
1	2	11	\N	\N
2	2	11	\N	\N
3	2	11	\N	\N
4	2	11	\N	\N
5	2	11	\N	\N
6	2	11	\N	\N
7	2	11	\N	\N
8		11	\N	\N
9	2	11	\N	\N
10	2	11	\N	\N
11	2	11	\N	\N
12	2	11	\N	\N
13		11	\N	\N
14	2	11	\N	\N
15		11	\N	\N
16		11	\N	\N
17	2	11	\N	\N
18	2	11	\N	\N
19	2	11	\N	\N
20	2	11	\N	\N
21	2	11	\N	\N
22	2	11	\N	\N
23	0	11	\N	\N
24	2	11	\N	\N
25	2	11	\N	\N
26	0	11	\N	\N
27	75	11	\N	\N
28	0	11	\N	\N
29		11	\N	\N
30		11	\N	\N
31	Moi c'est Simon Lasnier !	11	\N	\N
1	4	12	\N	\N
2	4	12	\N	\N
3	4	12	\N	\N
4	4	12	\N	\N
5	4	12	\N	\N
6	4	12	\N	\N
7	0	12	\N	\N
8		12	\N	\N
9	0	12	\N	\N
10	4	12	\N	\N
11	0	12	\N	\N
12	4	12	\N	\N
13		12	\N	\N
14	0	12	\N	\N
15		12	\N	\N
16		12	\N	\N
17	4	12	\N	\N
18	0	12	\N	\N
19	2	12	\N	\N
20	3	12	\N	\N
21	1	12	\N	\N
22	2	12	\N	\N
23	5	12	\N	\N
24	1	12	\N	\N
25	1	12	\N	\N
26	0	12	\N	\N
27	0	12	\N	\N
28	0	12	\N	\N
29		12	\N	\N
30		12	\N	\N
31	Euh... Je sais plus qui je suis...	12	\N	\N
1	3	13	\N	\N
2	3	13	\N	\N
3	3	13	\N	\N
4	3	13	\N	\N
5	3	13	\N	\N
6	3	13	\N	\N
7	3	13	\N	\N
8		13	\N	\N
9	3	13	\N	\N
10	3	13	\N	\N
11	3	13	\N	\N
12	3	13	\N	\N
13		13	\N	\N
14	3	13	\N	\N
15		13	\N	\N
16		13	\N	\N
17	3	13	\N	\N
18	3	13	\N	\N
19	3	13	\N	\N
20	3	13	\N	\N
21	3	13	\N	\N
22	3	13	\N	\N
23	3	13	\N	\N
24	3	13	\N	\N
25	3	13	\N	\N
26	0	13	\N	\N
27	0	13	\N	\N
28	0	13	\N	\N
29		13	\N	\N
30		13	\N	\N
31	Ich heiSe Ariane Lemaire.	13	\N	\N
1	1	14	\N	\N
2	2	14	\N	\N
3	3	14	\N	\N
4	4	14	\N	\N
5	5	14	\N	\N
6	1	14	\N	\N
7	1	14	\N	\N
8		14	\N	\N
9	4	14	\N	\N
10	2	14	\N	\N
11	2	14	\N	\N
12	4	14	\N	\N
13		14	\N	\N
14	1	14	\N	\N
15		14	\N	\N
16		14	\N	\N
17	2	14	\N	\N
18	3	14	\N	\N
19	4	14	\N	\N
20	4	14	\N	\N
21	3	14	\N	\N
22	4	14	\N	\N
23	1	14	\N	\N
24	2	14	\N	\N
25	3	14	\N	\N
26	0	14	\N	\N
27	0	14	\N	\N
28	0	14	\N	\N
29		14	\N	\N
30		14	\N	\N
31	Trop trop la classse je trouve.	14	\N	\N
1	5	15	\N	\N
2	5	15	\N	\N
3	5	15	\N	\N
4	4	15	\N	\N
5	4	15	\N	\N
6	5	15	\N	\N
7	2	15	\N	\N
8		15	\N	\N
9	2	15	\N	\N
10	3	15	\N	\N
11	4	15	\N	\N
12	4	15	\N	\N
13		15	\N	\N
14	2	15	\N	\N
15		15	\N	\N
16		15	\N	\N
17	5	15	\N	\N
18	4	15	\N	\N
19	5	15	\N	\N
20	4	15	\N	\N
21	5	15	\N	\N
22	5	15	\N	\N
23	5	15	\N	\N
24	5	15	\N	\N
25	5	15	\N	\N
26	0	15	\N	\N
27	0	15	\N	\N
28	0	15	\N	\N
29		15	\N	\N
30		15	\N	\N
31	Et moi c'est Vatin ! Charles Vatin !	15	\N	\N
32	PSI	16	\N	\N
1	1	16	\N	\N
2	3	16	\N	\N
3	2	16	\N	\N
4	3	16	\N	\N
5	2	16	\N	\N
6	0	16	\N	\N
7	0	16	\N	\N
8		16	\N	\N
9	0	16	\N	\N
10	0	16	\N	\N
11	0	16	\N	\N
12	0	16	\N	\N
13		16	\N	\N
14	0	16	\N	\N
15		16	\N	\N
16		16	\N	\N
17	0	16	\N	\N
18	0	16	\N	\N
19	0	16	\N	\N
20	0	16	\N	\N
21	0	16	\N	\N
22	0	16	\N	\N
23	0	16	\N	\N
24	0	16	\N	\N
25	2	16	\N	\N
26	0	16	\N	\N
27	0	16	\N	\N
28	0	16	\N	\N
29		16	\N	\N
30		16	\N	\N
31	m	16	\N	\N
32	PSI	17	\N	\N
1	1	17	\N	\N
2	2	17	\N	\N
3	1	17	\N	\N
4	0	17	\N	\N
5	0	17	\N	\N
6	0	17	\N	\N
7	0	17	\N	\N
8		17	\N	\N
9	0	17	\N	\N
10	0	17	\N	\N
11	0	17	\N	\N
12	0	17	\N	\N
13		17	\N	\N
14	0	17	\N	\N
15		17	\N	\N
16		17	\N	\N
17	0	17	\N	\N
18	0	17	\N	\N
19	0	17	\N	\N
20	0	17	\N	\N
21	1	17	\N	\N
22	0	17	\N	\N
23	3	17	\N	\N
24	0	17	\N	\N
25	2	17	\N	\N
26	0	17	\N	\N
27	0	17	\N	\N
28	0	17	\N	\N
29		17	\N	\N
30	sdfsdf	17	\N	\N
31	sdfsdf	17	\N	\N
32	PSI	7	1	1
33	1	7	1	1
34	1	7	1	1
1	5	7	1	1
2	5	7	1	1
3	5	7	1	1
4	5	7	1	1
5	5	7	1	1
6	5	7	1	1
7	5	7	1	1
8		7	1	1
9	5	7	1	1
10	5	7	1	1
11	5	7	1	1
12	5	7	1	1
13		7	1	1
14	5	7	1	1
15		7	1	1
16		7	1	1
17	5	7	1	1
18	5	7	1	1
19	5	7	1	1
20	5	7	1	1
21	5	7	1	1
22	5	7	1	1
23	5	7	1	1
24	5	7	1	1
25	5	7	1	1
26	0	7	1	1
27	0	7	1	1
28	0	7	1	1
29		7	1	1
30		7	1	1
31		7	1	1
32	Fac	18	\N	\N
34	16	18	\N	\N
1	1	18	\N	\N
2	0	18	\N	\N
3	0	18	\N	\N
4	0	18	\N	\N
5	0	18	\N	\N
6	0	18	\N	\N
7	0	18	\N	\N
8		18	\N	\N
9	0	18	\N	\N
10	0	18	\N	\N
11	0	18	\N	\N
12	0	18	\N	\N
13		18	\N	\N
14	0	18	\N	\N
15		18	\N	\N
16		18	\N	\N
17	0	18	\N	\N
18	0	18	\N	\N
19	0	18	\N	\N
20	0	18	\N	\N
21	0	18	\N	\N
22	0	18	\N	\N
23	0	18	\N	\N
24	0	18	\N	\N
25	0	18	\N	\N
26	0	18	\N	\N
27	0	18	\N	\N
28	0	18	\N	\N
29		18	\N	\N
30		18	\N	\N
31		18	\N	\N
32	BTS	19	17	\N
34	16	19	17	\N
1	1	19	17	\N
2	1	19	17	\N
3	0	19	17	\N
4	0	19	17	\N
5	0	19	17	\N
6	0	19	17	\N
7	0	19	17	\N
8		19	17	\N
9	0	19	17	\N
10	1	19	17	\N
11	2	19	17	\N
12	3	19	17	\N
13	Oui	19	17	\N
14	4	19	17	\N
15		19	17	\N
16		19	17	\N
17	0	19	17	\N
18	0	19	17	\N
19	0	19	17	\N
20	0	19	17	\N
21	0	19	17	\N
22	0	19	17	\N
23	0	19	17	\N
24	0	19	17	\N
25	0	19	17	\N
26	0	19	17	\N
27	0	19	17	\N
28	0	19	17	\N
29		19	17	\N
30		19	17	\N
31		19	17	\N
32	IUT	20	\N	\N
34	16	20	\N	\N
1	5	20	\N	\N
2	0	20	\N	\N
3	0	20	\N	\N
4	1	20	\N	\N
5	0	20	\N	\N
6	0	20	\N	\N
7	0	20	\N	\N
8		20	\N	\N
9	0	20	\N	\N
10	3	20	\N	\N
11	3	20	\N	\N
12	2	20	\N	\N
13		20	\N	\N
14	0	20	\N	\N
15		20	\N	\N
16		20	\N	\N
17	0	20	\N	\N
18	0	20	\N	\N
19	0	20	\N	\N
20	0	20	\N	\N
21	0	20	\N	\N
22	0	20	\N	\N
23	0	20	\N	\N
24	0	20	\N	\N
25	0	20	\N	\N
26	0	20	\N	\N
27	0	20	\N	\N
28	0	20	\N	\N
29		20	\N	\N
30	Hihihi	20	\N	\N
31		20	\N	\N
32	Autre	21	16	\N
34	16	21	16	\N
1	0	21	16	\N
2	0	21	16	\N
3	0	21	16	\N
4	0	21	16	\N
5	0	21	16	\N
6	0	21	16	\N
7	0	21	16	\N
8		21	16	\N
9	0	21	16	\N
10	5	21	16	\N
11	5	21	16	\N
12	5	21	16	\N
13	Héhéhé	21	16	\N
14	0	21	16	\N
15		21	16	\N
16		21	16	\N
17	0	21	16	\N
18	0	21	16	\N
19	0	21	16	\N
20	0	21	16	\N
21	0	21	16	\N
22	0	21	16	\N
23	0	21	16	\N
24	0	21	16	\N
25	0	21	16	\N
26	0	21	16	\N
27	1	21	16	\N
28	0	21	16	\N
29		21	16	\N
30		21	16	\N
31		21	16	\N
\.


--
-- Data for Name: sections; Type: TABLE DATA; Schema: public; Owner: teddy.michel
--

COPY sections (idsection, nom) FROM stdin;
5	A propos de l'enseignement que vous avez suivi
4	Interaction enseignant-élève
3	Appréciation de l'enseignement
2	Renseignements élèves/stagiaires
\.


--
-- Data for Name: ue; Type: TABLE DATA; Schema: public; Owner: teddy.michel
--

COPY ue (idue, nomue, idcharge) FROM stdin;
13	Formation humaine (2A)	13
14	Tronc Commun (3A)	14
1	Math 1 (1A)	1
2	Math 2 (1A)	2
3	Algorithmique - Programmation (1A)	3
4	Matériel & Systèmes (1A)	4
5	Applications (1A)	5
6	Formation humaine (1A)	6
7	Economie-Gestion (1A)	7
8	Génie Logiciel (2A)	8
9	Système et réseaux (2A)	9
10	IA et informatique fondamentale (2A)	10
11	Maths (2A)	11
12	Organisation (2A)	12
19	Option - 3A - Conception et validation d'applications réactives	19
18	Option - 2A - SIABD	18
17	Option - 2A - Banque, Finance & Assurance	17
16	Option - 2A - Robotique & Réalité virtuelle	16
15	Option - 2A - Systèmes d'exploitation	15
20	Option - 3A - Programmation raisonnée	20
21	Option - 3A - Modélisation	21
22	Option - 3A - Reseau, Securité	22
23	Option - 3A - Systèmes d'information avancée et bases de données	23
24	Option - 3A - Optimisation	24
25	Option - 3A - Nouvelles technologies et organisation des entreprises	25
26	Option - 3A - Marché fist	26
27	Option - 3A - Intelligence artificielle	27
\.


--
-- Name: charge_pkey; Type: CONSTRAINT; Schema: public; Owner: teddy.michel; Tablespace: 
--

ALTER TABLE ONLY charge
    ADD CONSTRAINT charge_pkey PRIMARY KEY (idcharge);


--
-- Name: choixcoursmagistraux_pkey; Type: CONSTRAINT; Schema: public; Owner: teddy.michel; Tablespace: 
--

ALTER TABLE ONLY choixcoursmagistraux
    ADD CONSTRAINT choixcoursmagistraux_pkey PRIMARY KEY (idensmt, idens);


--
-- Name: choixtd_pkey; Type: CONSTRAINT; Schema: public; Owner: teddy.michel; Tablespace: 
--

ALTER TABLE ONLY choixtd
    ADD CONSTRAINT choixtd_pkey PRIMARY KEY (idensmt, idens);


--
-- Name: choixtp_pkey; Type: CONSTRAINT; Schema: public; Owner: teddy.michel; Tablespace: 
--

ALTER TABLE ONLY choixtp
    ADD CONSTRAINT choixtp_pkey PRIMARY KEY (idensmt, idens);


--
-- Name: conditionnelle_pkey; Type: CONSTRAINT; Schema: public; Owner: teddy.michel; Tablespace: 
--

ALTER TABLE ONLY conditionnelle
    ADD CONSTRAINT conditionnelle_pkey PRIMARY KEY (ideleve);


--
-- Name: droits_pkey; Type: CONSTRAINT; Schema: public; Owner: teddy.michel; Tablespace: 
--

ALTER TABLE ONLY droits
    ADD CONSTRAINT droits_pkey PRIMARY KEY (idens, idensmt);


--
-- Name: eleve_pkey; Type: CONSTRAINT; Schema: public; Owner: teddy.michel; Tablespace: 
--

ALTER TABLE ONLY eleve
    ADD CONSTRAINT eleve_pkey PRIMARY KEY (ideleve);


--
-- Name: eleve_unique; Type: CONSTRAINT; Schema: public; Owner: teddy.michel; Tablespace: 
--

ALTER TABLE ONLY eleve
    ADD CONSTRAINT eleve_unique UNIQUE (login);


--
-- Name: eleveensmt_pkey; Type: CONSTRAINT; Schema: public; Owner: teddy.michel; Tablespace: 
--

ALTER TABLE ONLY eleveensmt
    ADD CONSTRAINT eleveensmt_pkey PRIMARY KEY (ideleve, idensmt);


--
-- Name: enseignant_pkey; Type: CONSTRAINT; Schema: public; Owner: teddy.michel; Tablespace: 
--

ALTER TABLE ONLY enseignant
    ADD CONSTRAINT enseignant_pkey PRIMARY KEY (idens);


--
-- Name: enseignement_pkey; Type: CONSTRAINT; Schema: public; Owner: teddy.michel; Tablespace: 
--

ALTER TABLE ONLY enseignement
    ADD CONSTRAINT enseignement_pkey PRIMARY KEY (idensmt);


--
-- Name: formation_pkey; Type: CONSTRAINT; Schema: public; Owner: teddy.michel; Tablespace: 
--

ALTER TABLE ONLY formation
    ADD CONSTRAINT formation_pkey PRIMARY KEY (idformation);


--
-- Name: optionoutc_pkey; Type: CONSTRAINT; Schema: public; Owner: teddy.michel; Tablespace: 
--

ALTER TABLE ONLY optionoutc
    ADD CONSTRAINT optionoutc_pkey PRIMARY KEY (idoption);


--
-- Name: prof_unique; Type: CONSTRAINT; Schema: public; Owner: teddy.michel; Tablespace: 
--

ALTER TABLE ONLY enseignant
    ADD CONSTRAINT prof_unique UNIQUE (login);


--
-- Name: question_unique; Type: CONSTRAINT; Schema: public; Owner: teddy.michel; Tablespace: 
--

ALTER TABLE ONLY questions
    ADD CONSTRAINT question_unique UNIQUE (idsection, ordrequestion);


--
-- Name: questionnaire_pkey; Type: CONSTRAINT; Schema: public; Owner: teddy.michel; Tablespace: 
--

ALTER TABLE ONLY questionnaire
    ADD CONSTRAINT questionnaire_pkey PRIMARY KEY (idquestionnaire);


--
-- Name: questions_pkey; Type: CONSTRAINT; Schema: public; Owner: teddy.michel; Tablespace: 
--

ALTER TABLE ONLY questions
    ADD CONSTRAINT questions_pkey PRIMARY KEY (idquestion);


--
-- Name: redoublement_pkey; Type: CONSTRAINT; Schema: public; Owner: teddy.michel; Tablespace: 
--

ALTER TABLE ONLY redoublement
    ADD CONSTRAINT redoublement_pkey PRIMARY KEY (ideleve, iduereussie);


--
-- Name: reponses_pkey; Type: CONSTRAINT; Schema: public; Owner: teddy.michel; Tablespace: 
--

ALTER TABLE ONLY reponses
    ADD CONSTRAINT reponses_pkey PRIMARY KEY (idquestion, idquestionnaire);


--
-- Name: sections_pkey; Type: CONSTRAINT; Schema: public; Owner: teddy.michel; Tablespace: 
--

ALTER TABLE ONLY sections
    ADD CONSTRAINT sections_pkey PRIMARY KEY (idsection);


--
-- Name: ue_pkey; Type: CONSTRAINT; Schema: public; Owner: teddy.michel; Tablespace: 
--

ALTER TABLE ONLY ue
    ADD CONSTRAINT ue_pkey PRIMARY KEY (idue);


--
-- Name: charge_idens_fkey; Type: FK CONSTRAINT; Schema: public; Owner: teddy.michel
--

ALTER TABLE ONLY charge
    ADD CONSTRAINT charge_idens_fkey FOREIGN KEY (idens) REFERENCES enseignant(idens);


--
-- Name: choixcoursmagistraux_idens_fkey; Type: FK CONSTRAINT; Schema: public; Owner: teddy.michel
--

ALTER TABLE ONLY choixcoursmagistraux
    ADD CONSTRAINT choixcoursmagistraux_idens_fkey FOREIGN KEY (idens) REFERENCES enseignant(idens);


--
-- Name: choixcoursmagistraux_idensmt_fkey; Type: FK CONSTRAINT; Schema: public; Owner: teddy.michel
--

ALTER TABLE ONLY choixcoursmagistraux
    ADD CONSTRAINT choixcoursmagistraux_idensmt_fkey FOREIGN KEY (idensmt) REFERENCES enseignement(idensmt);


--
-- Name: choixtd_idens_fkey; Type: FK CONSTRAINT; Schema: public; Owner: teddy.michel
--

ALTER TABLE ONLY choixtd
    ADD CONSTRAINT choixtd_idens_fkey FOREIGN KEY (idens) REFERENCES enseignant(idens);


--
-- Name: choixtd_idensmt_fkey; Type: FK CONSTRAINT; Schema: public; Owner: teddy.michel
--

ALTER TABLE ONLY choixtd
    ADD CONSTRAINT choixtd_idensmt_fkey FOREIGN KEY (idensmt) REFERENCES enseignement(idensmt);


--
-- Name: choixtp_idens_fkey; Type: FK CONSTRAINT; Schema: public; Owner: teddy.michel
--

ALTER TABLE ONLY choixtp
    ADD CONSTRAINT choixtp_idens_fkey FOREIGN KEY (idens) REFERENCES enseignant(idens);


--
-- Name: choixtp_idensmt_fkey; Type: FK CONSTRAINT; Schema: public; Owner: teddy.michel
--

ALTER TABLE ONLY choixtp
    ADD CONSTRAINT choixtp_idensmt_fkey FOREIGN KEY (idensmt) REFERENCES enseignement(idensmt);


--
-- Name: conditionnelle_ideleve_fkey; Type: FK CONSTRAINT; Schema: public; Owner: teddy.michel
--

ALTER TABLE ONLY conditionnelle
    ADD CONSTRAINT conditionnelle_ideleve_fkey FOREIGN KEY (ideleve) REFERENCES eleve(ideleve);


--
-- Name: conditionnelle_idue1manquee_fkey; Type: FK CONSTRAINT; Schema: public; Owner: teddy.michel
--

ALTER TABLE ONLY conditionnelle
    ADD CONSTRAINT conditionnelle_idue1manquee_fkey FOREIGN KEY (idue1manquee) REFERENCES ue(idue);


--
-- Name: conditionnelle_idue2manquee_fkey; Type: FK CONSTRAINT; Schema: public; Owner: teddy.michel
--

ALTER TABLE ONLY conditionnelle
    ADD CONSTRAINT conditionnelle_idue2manquee_fkey FOREIGN KEY (idue2manquee) REFERENCES ue(idue);


--
-- Name: droits_idens_fkey; Type: FK CONSTRAINT; Schema: public; Owner: teddy.michel
--

ALTER TABLE ONLY droits
    ADD CONSTRAINT droits_idens_fkey FOREIGN KEY (idens) REFERENCES enseignant(idens);


--
-- Name: droits_idensmt_fkey; Type: FK CONSTRAINT; Schema: public; Owner: teddy.michel
--

ALTER TABLE ONLY droits
    ADD CONSTRAINT droits_idensmt_fkey FOREIGN KEY (idensmt) REFERENCES enseignement(idensmt);


--
-- Name: eleveensmt_ideleve_fkey; Type: FK CONSTRAINT; Schema: public; Owner: teddy.michel
--

ALTER TABLE ONLY eleveensmt
    ADD CONSTRAINT eleveensmt_ideleve_fkey FOREIGN KEY (ideleve) REFERENCES eleve(ideleve);


--
-- Name: eleveensmt_idensmt_fkey; Type: FK CONSTRAINT; Schema: public; Owner: teddy.michel
--

ALTER TABLE ONLY eleveensmt
    ADD CONSTRAINT eleveensmt_idensmt_fkey FOREIGN KEY (idensmt) REFERENCES enseignement(idensmt);


--
-- Name: questionnaire_idensmt_fkey; Type: FK CONSTRAINT; Schema: public; Owner: teddy.michel
--

ALTER TABLE ONLY questionnaire
    ADD CONSTRAINT questionnaire_idensmt_fkey FOREIGN KEY (idensmt) REFERENCES enseignement(idensmt);


--
-- Name: questions_idsection_fkey; Type: FK CONSTRAINT; Schema: public; Owner: teddy.michel
--

ALTER TABLE ONLY questions
    ADD CONSTRAINT questions_idsection_fkey FOREIGN KEY (idsection) REFERENCES sections(idsection);


--
-- Name: redoublement_ideleve_fkey; Type: FK CONSTRAINT; Schema: public; Owner: teddy.michel
--

ALTER TABLE ONLY redoublement
    ADD CONSTRAINT redoublement_ideleve_fkey FOREIGN KEY (ideleve) REFERENCES eleve(ideleve);


--
-- Name: redoublement_iduereussie_fkey; Type: FK CONSTRAINT; Schema: public; Owner: teddy.michel
--

ALTER TABLE ONLY redoublement
    ADD CONSTRAINT redoublement_iduereussie_fkey FOREIGN KEY (iduereussie) REFERENCES ue(idue);


--
-- Name: reponses_idquestion_fkey; Type: FK CONSTRAINT; Schema: public; Owner: teddy.michel
--

ALTER TABLE ONLY reponses
    ADD CONSTRAINT reponses_idquestion_fkey FOREIGN KEY (idquestion) REFERENCES questions(idquestion);


--
-- Name: public; Type: ACL; Schema: -; Owner: teddy.michel
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM "teddy.michel";
GRANT ALL ON SCHEMA public TO "teddy.michel";
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

