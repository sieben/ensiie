<?php

require_once ( 'include/main.php' );

// L'utilisateur n'est pas connecté ou n'est pas administrateur
if ( !is_admin() )
{
    header ( 'Location: login.php' );
    die();
}

// On n'a pas précisé l'identifiant de la section
if ( !isset ( $_GET['id'] ) || !is_numeric ( $_GET['id'] ) )
{
    header ( 'Location: index.php' );
    die();
}

$id = db_protect ( $_GET['id'] );

$sql = 'SELECT idsection
        FROM sections
        WHERE idsection = ' . $id . ';';

$req = db_query ( $db_link , $sql , __FILE__ , __LINE__ );

if ( pg_num_rows ( $req ) > 0 )
{
    $row = pg_fetch_assoc ( $req );

    // On déplace la section vers le haut
    if ( isset ( $_GET['up'] ) && $id > 1 )
    {
        // On déplace la section précédente à la fin
        $sql = 'UPDATE sections
                SET idsection = 99999
                WHERE idsection = ' . ( $id - 1 ) . ';';

        $req = db_query ( $db_link , $sql , __FILE__ , __LINE__ );

        if ( pg_affected_rows ( $req ) > 0 )
        {
	    // On décale les questions de la section précédente
	    $sql = 'UPDATE questions
	            SET idsection = 99999
	            WHERE idsection = ' . ( $id - 1 ) . ';';   

            db_query ( $db_link , $sql , __FILE__ , __LINE__ );

            // On décale la section
            $sql = 'UPDATE sections
                    SET idsection = ' . ( $id - 1 ) . '
                    WHERE idsection = ' . $id . ';';

            db_query ( $db_link , $sql , __FILE__ , __LINE__ );

            // On décale les questions de la section
            $sql = 'UPDATE questions
                    SET idsection = ' . ( $id - 1 ) . '
                    WHERE idsection = ' . $id . ';';

            db_query ( $db_link , $sql , __FILE__ , __LINE__ );

            // On modifie l'ordre de la dernière section
            $sql = 'UPDATE sections
                    SET idsection = ' . $id . '
                    WHERE idsection = 99999;';

            db_query ( $db_link , $sql );

            // On décale les questions de la dernière section
            $sql = 'UPDATE questions
                    SET idsection = ' . $id . '
                    WHERE idsection = 99999;';

            db_query ( $db_link , $sql );
        }
    }
    // On déplace la section vers le bas
    else
    {
        // On déplace la section suivante à la fin
        $sql = 'UPDATE sections
                SET idsection = 99999
                WHERE idsection = ' . ( $id + 1 ) . ';';

        $req = db_query ( $db_link , $sql );

        if ( pg_affected_rows ( $req ) > 0 )
        {
	    // On décale les questions de la section précédente
	    $sql = 'UPDATE questions
	            SET idsection = 99999
	            WHERE idsection = ' . ( $id + 1 ) . ';';   

            db_query ( $db_link , $sql );

            // On décale la section
            $sql = 'UPDATE sections
                    SET idsection = ' . ( $id + 1 ) . '
                    WHERE idsection = ' . $id . ';';

            db_query ( $db_link , $sql );

            // On décale les questions de la section
            $sql = 'UPDATE questions
                    SET idsection = ' . ( $id + 1 ) . '
                    WHERE idsection = ' . $id . ';';

            db_query ( $db_link , $sql );

            // On modifie l'ordre de la dernière section
            $sql = 'UPDATE sections
                    SET idsection = ' . $id . '
                    WHERE idsection = 99999;';

            db_query ( $db_link , $sql );

            // On décale les questions de la dernière section
            $sql = 'UPDATE questions
                    SET idsection = ' . $id . '
                    WHERE idsection = 99999;';

            db_query ( $db_link , $sql );
        }
    }
}

header ( 'Location: questionnaire.php' );
die();

?>
