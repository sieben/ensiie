(********************************************)
(* Auteur : Remy Leone                      *)
(* Groupe : 2.1                             *)
(* Charg� de TD : M. Gacogne                *)
(********************************************)

(* I - Proc�dures avec exceptions *)

exception Impossible
;;

(* Interface : indicatrice 
type int -> int list 
Argument : Un entier 
Precondition : Un entier positif 
Postcondition : Donne la liste de tous les carr�s 
parfaits strictement inferieur � n 
Tests: indicatrice (-5);;
indicatrice (0);;
indicatrice (42);;
*)

let indicatrice n =
  let rec sub_indicatrice k =
    if k*k < n 
    then (k*k)::(sub_indicatrice (k+1))
    else []
  in sub_indicatrice 1
;;

(* Interface : control
type : 'a list -> 'a list
Argument : Une liste
Precondition : Aucune
Postcondition : 
 Renvoie la liste donn�e en argument 
 si la liste a 4 �lements et sinon l�ve une exception.
Tests : control [];;
control [[1;2;3;4]];;
control [1;2;3;4];;
*)

let control list =
  if List.length list > 4 
  then raise Impossible 
  else list
;;

(* Interface : finder
type int -> int list -> int list
Argument : Entier , Liste d'entiers
Precondition : Aucune
Postcondition : Renvoie la possibilit� de d�composition 
	d'un nombre n avec la liste donn�e en argument
Tests : finder 42 [];;
finder 0 (indicatrice 42);;
finder (-42) (indicatrice 42);;
finder 0 [];;
*)

let rec finder s list =
  match list with
      [] -> if s = 0 
      then [] 
      else raise Impossible    
    |x::r -> try control (x::(finder (s-x) r)) with 
	  Impossible -> finder s r
;;

(* Interface : somme
type int list -> int
Argument : Une liste d'entiers
Precondition : Aucune
Postcondition : Somme des elements de la liste
Tests : somme [];;
somme [[]];;
somme [0;0;42;0;0];;
*)

let rec somme list =
  match list with
      [] -> 0
    |a::b -> a + somme b 
;;

(* Interface : solution 
type : int list -> int -> bool
Argument : Une liste d'entiers et un nombre entier
Precondition : Aucune
Postcondition : Verifie si la liste donn�e en argument 
a quatre elements et que leur somme est egale au nombre n
Tests : solution [] 0;;
solution [0] 0;;
solution [1;2;3;4] 10;;
*)

let solution list n =
  let test4 list = List.length list = 4 in
    ((somme list) = n)&&(test4 list)
;;

(* Toutes les solutions possibles *)

(* Interface : control2
type : 'a list list -> 'a list list
Argument : Liste de liste d'entiers
Precondition : Aucune
Postcondition : Purge toutes les listes 
	qui n'ont pas 4 �l�ments	
Tests : control2 [];;
control2 [[];[];[];[]];;
control2 [[1;2;3;4]];;
control2 [1;2;3;4];;
*)

let rec control2 list =
  match list with 
      [] -> []
    |a::b -> 
       if (List.length a) <> 4 
       then control2 b 
       else a::(control2 b)
;;

(* Interface : toutesolution
type : int -> int list -> int list list
Argument : Entier , Liste des carr�s possibles
Precondition : Aucune
Postcondition : 
Renvoie toutes les solutions possibles.
Tests : toutesolution 42 [];;
toutesolution 0 (indicatrice 42);;
toutesolution (-42) (indicatrice 42);;
toutesolution 0 [];;
toutesolution 185 (indicatrice 185);;
*)

let rec toutesolution s l =
  let f x list = x::list in match l with 
      [] -> if s = 0 then [[]] else []
    |x::r -> (toutesolution s r)@(List.map (f x) (toutesolution (s-x) r))
;;

(* Interface : all_lagrange 
type : int -> int list list
Argument : Entier positif
Precondition : Aucune 
Postcondition : 
Renvoie toutes les d�compositions possibles
Tests : all_lagrange 0;;
all_lagrange (-42);;
all_lagrange (42);;
all_lagrange (142);;
*)

let all_lagrange n =
	(toutesolution n (indicatrice n))
;;

(* Somme de deux cubes *)

(* Interface : indicatrice_cube
type : int -> int list
Argument : Entier positif
Precondition : Aucune
Postcondition : 
Donne la liste de tous les cube inf�rieurs � l'argument
Tests : indicatrice (-42);;
indicatrice 0;;
indicatrice_cube 42;;
*)

let indicatrice_cube n =
  let rec sub_indicatrice k =
    if k*k < n 
    then (k*k*k)::(sub_indicatrice (k+1))
    else []
  in sub_indicatrice 1;;

(*II - Parcours d'arbre *)

(* Interface : valide
type int list -> int -> bool
Argument : res, n
Precondition : Aucune
Postcondition : Reponds � la question :Est-il pertinent d'ajouter 
un nombre ou non � la liste solution ?
Tests : valide [] 0;;
valide [1;4;9;16] 30;;
*)

let valide res n = 
  match res with
      [] -> true
    |a::[] -> (n-a) >=  14
    |a::b::[] -> (n-a-b) >=  5
    |a::b::c::[] -> (n-a-b-c) >=  1
    |a::b::c::d::[] -> (n-a-b-c-d) = 0
    |_ -> false
;;

(* Interface : parcours
type : 
	int list -> int list list -> int list -> int -> int list
Arguments : 
	Liste d'entiers en cours de traitement, 
	Une pile de listes en attente,
	Une liste solution en construction,
	Le nombre � d�composer
Pr�condition : Aucune
Postcondition : 
	Renvoie la premi�re solution valide rencontr�e.
Tests : parcours [2] [] [] 2;;
parcours (indicatrice (-4)) [] [] 4;;
parcours (indicatrice 340) [] [] 340;;
*)

let rec parcours process pile res n =
  match process,pile,res with
      [],[],[]
	-> failwith "Impossible" (*FAIL*)
	  
    |_,_,res when (solution (res) (n)) 
	-> res (*Succes*)
       
    |[],hd_pile::tl_pile,hd_res::tl_res 
	-> parcours hd_pile tl_pile tl_res n (*Remont�e au p�re*)
       
    |hd_pro::tl_pro,pile,res
	-> 
       if valide (hd_pro::res) n 
       then parcours (indicatrice (hd_pro)) ((tl_pro)::(pile)) (hd_pro::res) n (*Descente au premier fils*)
       else parcours tl_pro pile res n (*Parcours horizontal*)
	 
    |_,_,_ -> failwith "Erreur conception"
;;

(* Interface : lagrange
type int -> int list
Argument : n
Precondition : Aucune
Postcondition : Renvoie la premi�re solution trouv�e
Tests : lagrange 399;;
*)

let lagrange n =
parcours (indicatrice n) [] [] n
;;

(* Interface : teal
type 'a list -> 'a list
Argument : list
Precondition : Aucune 
Postcondition : Renvoie la queue de la liste
ou la liste vide si la liste argument est vide 
Tests : teal [];;
teal [1;2;3];;
*)

let teal list = 
match list with
[] -> []
|x -> List.tl x
;;

(* Interface : parcours_integral
type : int list -> int list list -> int list -> int 
-> int list list
Argument : 
	Une liste en cours de traitement,
	Une liste de liste en attente,
	Une liste solution en cours de construction
	Un nombre n � d�composer	
Precondition : Aucune
Postcondition : 
Renvoie l'ensemble des solutions rencontr�es
Tests : parcours_integral [2] [] [] 2;;
parcours_integral (indicatrice (-4)) [] [] 4;;
parcours_integral (indicatrice 340) [] [] 340;;
*)

let rec parcours_integral process pile res n =
  match process,pile,res with
      [],[],[]
	-> [] (*FAIL*)
	  
    |pro,pile,res when (solution (res) (n)) 
	-> (res)::(parcours_integral (teal pro) (List.tl pile) (List.tl res) n)(*Succes*)
       
    |[],hd_pile::tl_pile,hd_res::tl_res 
	-> parcours_integral hd_pile tl_pile tl_res n (*Remont�e au p�re*)
       
    |hd_pro::tl_pro,pile,res
	-> 
       if valide (hd_pro::res) n 
       then parcours_integral (indicatrice (hd_pro)) ((tl_pro)::(pile)) (hd_pro::res) n (*Descente au premier fils*)
       else parcours_integral tl_pro pile res n (*Parcours horizontal*)
	 
    |_,_,_ -> failwith "Erreur conception"
;;

(* Interface : integral_lagrange 
type int -> int list list
Argument : n
Precondition : Aucune
Postcondition : Pour un nombre donn� renvoit l'ensemble des d�compostions
de ce nombre.
Tests : integral_lagrange (-45);;
integral_lagrange 42;;
integral_lagrange 399;;
integral_lagrange 99;;
*)

let integral_lagrange n =
	parcours_integral (indicatrice n) [] [] n
;;

integral_lagrange 1001;;

(* Interface : parcours_unlimited
type : int list -> int list list -> 
int list -> int -> int list list
Argument : 
	Une liste en cours de traitement,
	Une pile de listes � explorer,
	Une solution en cours de contruction
	Un nombre � d�composer
Precondition : Aucune
Postcondition : 
Renvoies l'ensemble des solutions possibles quelque
soit le nombre de termes dans la solution
Tests : parcours_unlimited [2] [] [] 2;;
parcours_unlimited (indicatrice (-4)) [] [] 4;;
parcours_unlimited (indicatrice 340) [] [] 340;;
*)

let rec parcours_unlimited process pile res n =
  match process,pile,res with
      [],[],[]
	-> [] (*FAIL*)
	  
    |pro,pile,res when ((somme res) = n) 
	-> (res)::(parcours_unlimited (teal pro) (List.tl pile) (teal res) n)(*Succes*)
       
    |[],hd_pile::tl_pile,hd_res::tl_res 
	-> parcours_unlimited hd_pile tl_pile tl_res n (*Remont�e au p�re*)
       
    |hd_pro::tl_pro,pile,res
	-> 
       if ( somme (hd_pro::res) ) <= n 
       then parcours_unlimited (indicatrice (hd_pro)) ((tl_pro)::(pile)) (hd_pro::res) n (*Descente au premier fils*)
       else parcours_unlimited tl_pro pile res n (*Parcours horizontal*)
	 
    |_,_,_ -> failwith "Erreur conception"
;;

(*Interface : lagrange_unlimited
type int -> int list list
argument : int
Precondition : Aucune
Postcondition : Renvoie l'ensemble des solutions pour un nombre donn�
Tests : lagrange_unlimited (-45);;
lagrange_unlimited 42;;
lagrange_unlimited 399;;
lagrange_unlimited 99;;
lagrange_unlimited 50;;
*)

let lagrange_unlimited n =
	parcours_unlimited (indicatrice n) [] [] n;;

lagrange_unlimited 1001;;