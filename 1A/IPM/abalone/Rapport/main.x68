﻿*------------------------------------------------------------------------------
*------------------------------------------------------------------------------
*
* INCLUDE
*
*------------------------------------------------------------------------------
*------------------------------------------------------------------------------

    include   "tablier.X68"
    include   "affichage.X68"
    include   "mouvement.X68"
    include   "fonctions.X68"
    include   "input.X68"
    include   "variables.X68"
    include   "verification.X68"

*------------------------------------------------------------------------------
*------------------------------------------------------------------------------
*
* MAIN
*
*------------------------------------------------------------------------------
*------------------------------------------------------------------------------

*------------------------------------------------------------------------------
* Aide-mémoire des conventions de l'abalone
*------------------------------------------------------------------------------

* D0 : Reservé pour les divers TRAP 
* D1 : Reservé pour les divers TRAP
* D2 : Memoire temporaire
* D3 : Direction du mouvement
* D4 : Première bille
* D5 : Seconde bille
* D6 : Troisième bille
* D7 : Oscille entre 'B' et 'N' (Blanc ou Noir)

* A0 : Adresse temporaire
* A1 : Reservé pour les divers TRAP d'affichage
* A2 : Adresse temporaire
* A3 : Adresse temporaire
* A4 : Adresse première bille
* A5 : ------- seconde bille
* A6 : ------- troisième bille
* A7 : Reservé 

    ORG    $1000

START:

*------------------------------------------------------------------------------
* Initialisation
*------------------------------------------------------------------------------

    MOVE.W    #'N',D7

nouveau_tour

    BSR       screen
    
    BSR       switch_player              * Attention à l'initialisation !

main_input

    BSR       print_ask_case
    BSR       input_first_ball

    BSR       print_ask_case
    BSR       input_second_ball

    BSR       print_ask_case
    BSR       input_third_ball

    
    BSR       print_direction
    BSR       print_ask_direction
    BSR       input_dir
    
    BSR       check_move                 * Sous-programme de verification la valeur de retour est dans D2
    
    CMPI      #1,D2
    BNE       main_input                 * Si c'est ok on continue sinon on redemande tout à l'utilisateur
    
    BSR       update_billes
    BSR       make_move
    
    BSR       flush_gouttiere            * Modification du score

    BSR       raz_check                  * Suppression des données temporaires
    BSR       flush_tableau              * Suppression des données temporaires

end_game

    BSR       check_victory
    CMP       'B',D2
    BEQ       print_white_win
    CMP       'N',D2
    BEQ       print_black_win
    
    BNE       nouveau_tour

    MOVE.B    #9,D0
    TRAP      #15
     
    END     START

*~Font name~Courier New~
*~Font size~10~
*~Tab type~1~
*~Tab size~8~
