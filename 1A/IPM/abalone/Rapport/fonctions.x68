﻿*------------------------------------------------------------------------------
*------------------------------------------------------------------------------
*
* FONCTIONS
*
*------------------------------------------------------------------------------
*------------------------------------------------------------------------------

*------------------------------------------------------------------------------
* Tour a tour des joueurs
*------------------------------------------------------------------------------

switch_player    

    EORI.l    #$C,D7                     *Valeur en héxa de 'B' XOR 'N'
    CMP       'B',D7
    BEQ       print_white_to_play
    BNE       print_black_to_play

    RTS

*------------------------------------------------------------------------------
* Mise à jour des adresses
*------------------------------------------------------------------------------

update_billes

    MOVE.L    #case_00,A2
    
    MOVE.L    D4,D2
    MULS.W    #$E,D2
    ADDA.l    D2,A2
    MOVE.L    A2,A4
    
    MOVE.L    #case_00,A2
    
    MOVE.L    D5,D2
    MULS.W    #$E,D2
    ADDA.l    D2,A2
    MOVE.L    A2,A5
    
    MOVE.L    #case_00,A2
    
    MOVE.L    D6,D2
    MULS.W    #$E,D2
    ADDA.l    D2,A2
    MOVE.L    A2,A6

    RTS
    
*------------------------------------------------------------------------------
* Mise à jour des scores (Une bille vient de se faire ejecter)
*------------------------------------------------------------------------------

flush_gouttiere

    LEA       case_00,A2
    CMP       #'B',(A2)
    BEQ       decrement_white
    CMP       #'N',(A2)
    BEQ       decrement_black
    
    RTS
    
decrement_white

    LEA       white_num,A2
    subi.b    #$1,(A2)

    LEA       case_00,A2
    MOVE.W    '_',(A2)
    
    RTS
    
decrement_black

    LEA       black_num,A2
    subi.b    #$1,(A2)
    
    LEA       case_00,A2
    MOVE.W    '_',(A2)
    
    RTS

*------------------------------------------------------------------------------
* Test de victoire
*------------------------------------------------------------------------------

check_victory

    LEA       white_num,A2
    CMP       #0,(A2)
    BEQ       white_win
    
    LEA       black_num,A2
    CMP       #0,(A2)
    BEQ       black_win
    
    RTS    

load_black_win
    
    MOVE.L    'N',D2
    RTS

load_white_win

    MOVE.L    'B',D2
    RTS
*---
* Test d'égalité dans le cas 1 contre 1
*--

check_draw

    LEA       white_num,A2
    CMP       #1,(A2)
    BNE       fail_check_draw

    CMP       #1,(A2)
    BNE       fail_check_draw
    BEQ       win_check_draw

win_check_draw

    MOVE.L    $1,D2
    RTS

fail_check_draw

    MOVE.L    $0,D2
    RTS
    
*------------------------------------------------------------------------------
* Remise à zero des compteurs pour la verification du mouvement 
*------------------------------------------------------------------------------

raz_check

raz_white_check

    LEA       check_white,A2
    MOVE.B    #$30,(A2)
    
raz_black_check

    LEA       check_black,A2
    MOVE.B    #$30,(A2)
    
    RTS
    
*------------------------------------------------------------------------------
* Remise à zero du tableau temporaire utilisé dans le deplacement 
*------------------------------------------------------------------------------

flush_tableau

    LEA       contenu_case,A2
    MOVE.B    $5,D2 

boucle_flush_tableau

    MOVE.L    #$0,(A2)+
    DBF       D2,boucle_flush_tableau
    
    RTS

*------------------------------------------------------------------------------
* Incrémente les compteurs de verification
*------------------------------------------------------------------------------

incr_white_check

    LEA       check_white,A2
    addq      #$1,(A2)
    RTS

incr_black_check

    LEA       check_black,A2
    addq      #$1,(A2)
    RTS

*-----------------------------------------------------------------------------
* Fonction remettant en ordre les trois billes dans les registres  D4, D5, D6
* (Autant le faire avant update_billes)
*-----------------------------------------------------------------------------

sort_billes

     CMP      D4,D5                      * on compare D4 et D5..
     BLT      sort_SUITE1                * si D5<D4..
     CMP      D5,D6                      * dans le cas contraire on compare D5 et D6..
     BLT      sort_NEXT1                 * si D6<D5..
     BRA      end_sort                   * dans le cas contraire si D5<D6 on a D4<D5<D6 on ne fait rien
     
sort_NEXT1

    CMP       D4,D6                      * on compare D4 et D6..
    BLT       sort_NEXT2                 * si D6<D4..
    MOVE.w    D5,D2                      * dans le cas contraire si D4<D6 on a D4<D6<D5 on met D5 dans D6 et D6 dans D4 et D5 grace au registre tampon D2
    MOVE.W    D6,D5    
    MOVE.W    D2,D6
    BRA       end_sort
    
sort_NEXT2

    MOVE.w    D4,D2                      * on a D6<D4<D5 on met D5 dans D6 et D6 dans D4 et D4 dans D5 grace au registre tampon D2
    MOVE.w    D6,D4
    MOVE.w    D5,D6
    MOVE.w    D2,D5
    BRA       end_sort
          
sort_SUITE1   
     CMP      D4,D6                      * on compare D4 et D6..
     BLT      sort_SUITE2                * si D6<D4..
     MOVE.W   D4,D2                      * dans le cas contraire D5<D4<D6 on met D4 dans D5 et D5 dans D4 grace au registre tampon D2
     MOVE.W   D5,D4
     MOVE.w   D2,D5
     BRA      end_sort


sort_SUITE2
     CMP      D5,D6                      * on compare D5 et D6..
     BLT      sort_SUITE3                * si D6<D5..
     MOVE.w   D4,D2                      * dans le cas contraire si D5<D6 on a D5<D6<D4 on met D4 dans D6 , D6 dan D5 et D5 dans D4 grace au registre tampon D2
     MOVE.w   D5,D4
     MOVE.w   D6,D5
     MOVE.w   D2,D6     
     BRA      end_sort
     
sort_SUITE3
     MOVE.W   D4,D2                      * on a D6<D5<D4 on met la valeur de D4 dans D6 et celle de D6 dans D4 grace au registre tampon D2
     MOVE.W   D6,D4
     MOVE.W   D2,D6
     BRA      end_sort
     
end_sort

    RTS
    
*------------------------------------------------------------------------------
* Fonctions indicatrices
*------------------------------------------------------------------------------

*------------------------------------------------------------------------------
* (etat_case) D2 -> (A0)
*------------------------------------------------------------------------------

etat_case                                * fonction qui nous informe sur l'état de la case de numero passé en paramètre

    MOVE.W    #case_00,A0                * on initialise nos adresses
    MULU.W    #7,D2                      * on met dans D2 la distance à parcourir depuis la case_00 (pour acceder à la ieme case il faut se decaler de 7*i)
    ADDA.w    D2,A0                      * A0 pointe maintenant sur la bonne case
    
    RTS

*~Font name~Courier New~
*~Font size~10~
*~Tab type~1~
*~Tab size~8~
