﻿*------------------------------------------------------------------------------
*------------------------------------------------------------------------------
*
* MOUVEMENT 
*
*------------------------------------------------------------------------------
*------------------------------------------------------------------------------


* L'algo consiste à déplacer vers l'avant à partir de la première case non vide
* Pour ce faire on va utiliser les trois registres d'adresses à notre disposition

* A2 et A3 seront les registres mobiles. Ce sont eux qui vont parcourir le tableau 
* et faire les échanges opportuns.
* Typiquement on a déplacer le contenu de la case pointée par A2 vers la case pointée par A3
* Puis on décrémente A2 et A3
* A0 sera le registre qui va servir de butée, en effet cet algo de déplacement doit bien
* s'arrêter quand on a déplacer toutes les billes et A0 pointra vers la première case non
* vide du tableau.
* A chaque étape on testera si A2 et A0 sont égaux, si tel est le cas cela signifie que l'on est 
* tombé sur un cas d'arrêt et à ce moment là l'algo de déplacement est terminé.

make_move

    MOVE.L    #contenu_case,A0
    MOVEQ.L   #6,D2 * C'est peut etre 5 ?
    
boucle_first_non_void

    CMP       #'_',(A0)
    BEQ       boucle_next_first_void
    DBF       D2,boucle_first_non_void
    
    MOVE.L    A0,A2
    
boucle_last_void

boucle_next_first_void

    ADDA      $4,A0
    DBF       D2,boucle_first_non_void

    
boucle_next_last_void



    RTS


*~Font name~Courier New~
*~Font size~10~
*~Tab type~1~
*~Tab size~8~
