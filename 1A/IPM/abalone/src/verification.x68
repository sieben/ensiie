*------------------------------------------------------------------------------
*------------------------------------------------------------------------------
*
* VERIFICATION
*
*------------------------------------------------------------------------------
*------------------------------------------------------------------------------

check_move

* Verification de l'appartenance des billes au joueur

    BSR       check_dir
    CMPI      #0,D2
    BNE       fail_check_move

* Verification que les billes soient bien accol�es
* Id�e : Pour tout mouvement il existe une premi�re bille.
* La trouver c'est faciliter grandement le travail de verification
* Si la direction est entre 1 et 3 ca veut dire que l'origine est la plus
* petite bille (La case la plus petite)
* Si la direction est entre 4 et 6 ca signifie que c'est la plus grande
* case.
* Remarque : 

    BSR       check_touch
    CMPI      #0,D2
    BNE       fail_check_move

    BSR       diff_billes
    CMPI      #0,D2
    BNE       fail_check_move

    BSR       check_sumito
    CMPI      #0,D2
    BNE       fail_check_move
    
fail_check_move

    RTS

*------------------------------------------------------------------------------
* Verification de la direction (Stock�e dans D3)
* ( 1->D2 = OK , 0->D2 = ERROR)
*------------------------------------------------------------------------------

check_dir     nop

    CMP       #1,D3
    BEQ       win_check_dir
    CMP       #2,D3
    BEQ       win_check_dir
    CMP       #3,D3
    BEQ       win_check_dir
    CMP       #4,D3
    BEQ       win_check_dir
    CMP       #5,D3
    BEQ       win_check_dir
    CMP       #6,D3
    BEQ       win_check_dir
    BNE       fail_check_dir

win_check_dir

    MOVEQ.L   #1,D2
    RTS

fail_check_dir

    MOVEQ.L   #0,D2
    RTS

*------------------------------------------------------------------------------
* Verification des nombres rentr�s
* ( 1 -> D2 = OK , 0->D2 = FAIL )
*------------------------------------------------------------------------------

check_num

    CMPI      #0,D4
    BLT       fail_check_num
    CMPI      #0,D5
    BLT       fail_check_num
    CMPI      #0,D6
    BLT       fail_check_num
    
    CMPI      #61,D4
    BGT       fail_check_num
    CMPI      #61,D5
    BLT       fail_check_num
    CMPI      #61,D6
    BLT       fail_check_num

win_check_num

    MOVEQ.L   #1,D2
    RTS

fail_check_num

    MOVEQ.L   #0,D2
    RTS

*------------------------------------------------------------------------------
* Verification joueur (Les billes jou�es appartiennent au joueur actif)
* (D2=1 => OK ; D2 = 0 => FAIL )
*------------------------------------------------------------------------------

same_player

    CMP       (A4),D7
    BNE       fail_verif_same_player
    
    CMP       (A5),D7
    BNE       fail_verif_same_player
    
    CMP       (A6),D7
    BNE       fail_verif_same_player

ok_verif_same_player

    MOVEQ.L   #1,D2
    RTS
    
fail_verif_same_player

    MOVEQ.L   #0,D2
    RTS

*------------------------------------------------------------------------------
* Verification que les billes sont diff�rentes
*------------------------------------------------------------------------------

diff_billes

    CMPI      #0,D4                      *on commence par comparer 0 � D4..        
    BEQ       diff_billes_SUITE1         *dans le cas d'egalit�..
    CMP       D4,D5                      *dans le cas contraire on compare D4 et D5..
    BEQ       diff_billes_NEXT1          *dans le cas d'egalit�..
    CMP       D4,D6                      *dans le cas contraire on compare D4 et D6    
    BEQ       diff_billes_4              *dans le cas d'egalit� D4=D6!=D5 (quatriemecas)..
    CMP       D5,D6                      *dans le cas contraire on compare D5 et D6..
    BEQ       diff_billes_4              *dans le cas d'egalit� D5=D6!=D4 (quatriemecas)..
    BRA       diff_billes_3              *dans le cas contraire D4!=D5!=D6 (troisiemecas))..

diff_billes_NEXT1

    CMP       D5,D6                      
    BEQ       diff_billes_2              *dans le cas d'egalit� D4=D5=D6!=0 (deuxiemecas)..
    BRA       diff_billes_4              *dans le cas contraire D4=D5!=D6 (quatriemecas)
    
diff_billes_SUITE1   

    CMPI      #0,D5        
    BEQ       diff_billes_SUITE2         *dans le cas d'egalit�..
    CMP       D5,D6                      *si D5!=0 et D4=0 on compare D5 et D6..
    BEQ       diff_billes_4              *dans le cas d'egalite D4!=D5=D6 (quatriemecas)..   
    CMP       D4,D6                      *dans le cas contraire on compare D4 et D6..
    BEQ       diff_billes_4              *dans le cas d'egalit� D4=D6!=D5 (quatrieme cas)..
    BRA       diff_billes_3              *dans la cas contraire D4!=D5!=D6 (troisieme cas)..
    
diff_billes_SUITE2  

    CMPI      #0,D6        
    BEQ       diff_billes_1              *dans le cas d'egalit� .. cad D4=D5=D6=0 (premiercas)..
    BRA       diff_billes_4              *si D6 !=0 , D4=D5=0..(qutrieme cas)..

diff_billes_1

    MOVEQ.L   #1,D2                      * D4=D5=D6=0
    RTS


diff_billes_2    

    MOVEQ.L   #2,D2                      * D4=D5=D6!=0
    RTS


diff_billes_3

    MOVEQ.L   #3,D2                      * D4!=D5, D5!=D6, D6!=D4
    RTS

diff_billes_4

    MOVEQ.L   #4,D2                      *cas ou D4,D5,D6 sont distincts
    RTS 

*------------------------------------------------------------------------------
* Verification du Sumito
*
* - Cette v�rification consiste � consulter les deux registres 
* check_white & check_black et v�rifier que celui qui a la main
* est bien celui qui a le plus de billes dans le tableau du mouvement
*
*------------------------------------------------------------------------------

check_sumito

* Exemples si l'utilisateur a choisit 

* N N N les cases voisine autorisent le mouvement si elles sont V ? ?
*                                                               B B ?    
*                                                               B V ?
* V N N les cases voisine autorisent le mouvement si elles sont V ? ?
*                                                               B V ?
* V V N les cases voisine autorisent le mouvement si elles sont V ? ?


    MOVE.L    u,D2
    MOVE.L    #contenu_case,A0
    MOVE.L    D3,D1
    
    MOVE.L    (A0)+,D0         
    CMP.L     D1,D0                
    BNE       next_check_sumito          * si la premiere case appartient au joueur 
                                         * (l'utilisateur a forcement choisit N N N 
                                         * sinon son choix n'est pas valide)
    MOVE.L    (A0)+,D0                   * on se place apres le choix du joueur
    MOVE.L    (A0)+,D0
    MOVE.L    (A0)+,D0
    CMP.L     D1,D0
    BEQ       fail_check_sumito          * Case suivante appartient au joueur (fail)
    CMP.L     D2,D0
    BEQ       win_check_sumito           * Case suivante vide (win)
    MOVE.L    (A0)+,D0                   * Consulte case suivante
    CMP.L     D1,D0               
    BEQ       fail_check_sumito          * Case appartient au joueur (fail)
    CMP.L     D2,D0
    BEQ       win_check_sumito           * Case vide (win)
    MOVE.L    (A0)+,D0                   * Consulte case suivante

    CMP.L     D1,D0            
    BEQ       fail_check_sumito          * Case appartient au joueur (fail)
    CMP.L     D2,D0
    BEQ       win_check_sumito           * Case vide (win)
    BRA       fail_check_sumito
    
next_check_sumito
   
    MOVE.L    (A0)+,D0                   * si la premiere case est vide on passe a la deuxieme 
    CMP.L     D1,D0            
    BNE       next2_check_sumito
    MOVE.L    (A0)+,D0                   * si la deuxieme case est blanche on se place apres le choix de 
                                         * l'utilisateur(l'utilisateur a forcement choisit V N N 
                                         * sinon son choix n'est pas valide)
    MOVE.L    (A0)+,D0
    CMP.L     D1,D0              
    BEQ       fail_check_sumito          * Case suivante appartient au joueur (fail)
    CMP.L     D2,D0               
    BEQ       win_check_sumito           * Case suivante vide (win)
    MOVE.L    (A0)+,D0                   * Consulte case suivante
    CMP.L     D2,D0
    BEQ       win_check_sumito           * Case suivante vide (win)
    BRA       fail_check_sumito          

next2_check_sumito    

    MOVE.L    (A0)+,D0                   * si les deux premieres cases sont vides
    MOVE.L    (A0)+,D0                   * on se placa apres le choix de l'utilisateur 
                                         * ( si les deux premieres cases sont 
                                         * vides l'utilisateur a forcement choisit V V N 
                                         * sinon son choix n'aurait pas ete valide)
    CMP.L     D2,D0
    BNE       fail_check_sumito          * Case non vide (fail)
    BRA       win_check_sumito           * Case vide (win)

win_check_sumito
    
    MOVE.L    #1,D2                      
    RTS

fail_check_sumito
    
    MOVE.L    #0,D2                      
    RTS

*------------------------------------------------------------------------------
* Verification touch
*------------------------------------------------------------------------------


check_touch

* Cas standard

* Il faut verifier que 
* voisin ( D4 , dir ) = D5

check_touch_first_ball

    CMP       #0,D4                      * Test premiere case vide
    BEQ       check_touch_second_ball

    MOVE.L    D4,D2
    BSR       etat_case                  * A0 pointe vers la case de D4
    MOVE.L    D3,D2                      * Pr�paration de la direction en vue d'aller chercher le voisin de D4
    MULU.W    #2,D2                      * Word : on multiplie par 2
    ADDA      D2,A0                      * On d�place A0 pour le faire pointer vers le bon num�ro de voisin
    CMP       (A0),D5                    * Le num�ro qui est point� par A0 doit correspondre � D5 si les billes sont accol�es 
    BNE       fail_check_touch

* voisin ( D5 , dir ) = D6

check_touch_second_ball

    MOVE.L    D5,D2
    BSR       etat_case    * Apr�s cette sous-fonction A0 pointe vers la case de D5
    MOVE.L    D3,D2    
    MULU.W    #2,D2
    ADDA      D2,A0
    CMP       (A0),D6
    BNE       fail_check_touch

* Cas accol�

* Il faut verifier que les numeros de case sont cons�cutifs
* Pour cela on incr�mente D4 (qui contient le numero de case)
* et on le compare � D5 (On fait de meme avec D5)
* Rq : on verifie que les billes sont bien sur une meme ligne
* Rq : on ne fait plus attention � la direction
* Rq : Le probl�me c'est que m�me si deux cases se suivent 
* d'un point de vue cardinal il est tout a fait possible
* qu'elle soit sur deux lignes distinctes. Ainsi il faut tester
* le fait que les trois billes sont sur la m�me ligne.

check_accole

    MOVE.L    D4,D2    * D4 + 1 = D5
    addi      #1,D2
    CMP       D2,D5
    BNE       fail_check_touch
    
    MOVE.L    D5,D2   *D5 + 1 = D6
    Addi      #1,D2
    CMP       D2,D6
    BNE       fail_check_touch
    
check_1_line * [1;5]

    CMPI      #5,D4
    BGE       check_2_line
    CMPI      #5,D5
    BGE       fail_check_touch
    CMPI      #6,D6
    BGE       fail_check_touch
    BLE       win_check_touch
    
    
check_2_line * [6;11]

    CMPI      #11,D4
    BGE       check_3_line
    CMPI      #11,D5
    BGE       fail_check_touch
    CMPI      #11,D6
    BGE       fail_check_touch
    BLE       win_check_touch
    

check_3_line * [12;18]

    CMPI      #18,D4
    BGE       check_4_line
    CMPI      #18,D5
    BGE       fail_check_touch
    CMPI      #18,D6
    BGE       fail_check_touch
    BLE       win_check_touch

check_4_line * [19;26]

    CMPI      #26,D4
    BGE       check_5_line
    CMPI      #26,D5
    BGE       fail_check_touch
    CMPI      #26,D6
    BGE       fail_check_touch
    BLE       win_check_touch

check_5_line * [27;35]

    CMPI      #35,D4
    BGE       check_6_line
    CMPI      #35,D5
    BGE       fail_check_touch
    CMPI      #35,D6
    BGE       fail_check_touch
    BLE       win_check_touch

check_6_line * [36;43]

    CMPI      #43,D4
    BGE       check_7_line
    CMPI      #43,D5
    BGE       fail_check_touch
    CMPI      #43,D6
    BGE       fail_check_touch
    BLE       win_check_touch
    
check_7_line * [44;50]

    CMPI      #50,D4
    BGE       check_8_line
    CMPI      #50,D5
    BGE       fail_check_touch
    CMPI      #50,D6
    BGE       fail_check_touch
    BLE       win_check_touch
    
check_8_line * [51;56]

    CMPI      #56,D4
    BGE       check_9_line
    CMPI      #56,D5
    BGE       fail_check_touch
    CMPI      #56,D6
    BGE       fail_check_touch
    BLE       win_check_touch
    
check_9_line * [57;61]
*Attention aux cas d'�galit� avec les bornes de l'intervalle

    CMPI      #61,D4
    BGT       fail_check_touch
    CMPI      #61,D5
    BGT       fail_check_touch
    CMPI      #61,D6
    BGE       fail_check_touch
    BLE       win_check_touch
    

win_check_touch

    MOVEQ.L   #1,D2
    RTS
    
fail_check_touch

    MOVEQ.L   #0,D2
    RTS

*------------------------------------------------------------------------------
* Check Choice
*------------------------------------------------------------------------------

check_choice

*fonction qui v�rifie si le choix de l'utilisateur est bon ou mauvais
*si l'utilisateur joue avec les boules blanches il a droit de choisir
* N N N
* V N N 
* V V N

    MOVE.L    D7,D3                      * On met l'identifiant du joueur stock� dans D7
    MOVE.L    #contenu_case,A1
    
    MOVE.L    (A1)+,D1           
    CMP.L     D3,D1
    BEQ       next_check_choice
    MOVE.L    (A1)+,D1                   * la premi�re case est vide
    CMP.L     D3,D1
    BEQ       continue_check_choice
    MOVE.L    (A1)+,D1                   * la premiere et la deuxieme case sont vides
    CMP.L     D3,D1
    BEQ       win_check_choice           * la premiere et la deuxieme case sont vides 
                                         * la derni�re appartient au joueur(bon choix)
    BRA       fail_check_choice          * les trois cases sont vides(mauvais choix)

continue_check_choice
 
    MOVE.L    (A1)+,D1                   * la premiere case est vide la deuxieme est blanche
    CMP.L     D3,D1
    BEQ       win_check_choice           * la premiere case est vide 
                                         * la deuxieme et la derni�re sont blanches (bon choix)
    BRA       fail_check_choice          * la premiere case est vide la deuxieme est blanche 
                                         * la derniere est vide(mauvais choix)

next_check_choice
    
    MOVE.L    (A1)+,D1                   
    CMP.L     D3,D1        
    BEQ       next2_check_choice
    BRA       fail_check_choice          * la premiere case est blanche 
                                         * la deuxieme est vide(mauvais choix)

next2_check_choice
    
    MOVE.L    (A1)+,D1                   * la premiere et la deuxieme case sont blanche 
    CMP.L     D3,D1
    BEQ       win_check_choice           * les deux premieres cases sont vides 
                                         * la derniere est blanche (bon choix)
    BRA       fail_check_choice          * les trois cases sont vides (mauvais choix)
    
win_check_choice
    
    MOVE.L    #1,D2                      
    RTS

fail_check_choice    
        
    MOVE.L    #0,D2                      
    RTS

*~Font name~Courier New~
*~Font size~10~
*~Tab type~1~
*~Tab size~8~
