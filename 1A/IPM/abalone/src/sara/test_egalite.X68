*-----------------------------------------------------------
* Program    :
* Written by :
* Date       :
* Description:
*-----------------------------------------------------------
	ORG	$000
START:				; first instruction of program

	
	MOVE.w VAR1,D4		*on met les variables dans les registres
	
	MOVE.w VAR2,D5
	
	MOVE.w VAR3,D6
	
	
		
	CMP #0,D4		*on commence par comparer 0 � D4..		
	BEQ SUITE1		*dans le cas d'egalit�..
	CMP D4,D5		*dans le cas contraire on compare D4 et D5..
	BEQ NEXT1		*dans le cas d'egalit�..
	CMP D4,D6		*dans le cas contraire on compare D4 et D6	
	BEQ QUATRIEMECAS  	*dans le cas d'egalit� D4=D6!=D5 (quatriemecas)..
	CMP D5,D6		*dans le cas contraire on compare D5 et D6..
	BEQ QUATRIEMECAS    	*dans le cas d'egalit� D5=D6!=D4 (quatriemecas)..
	BRA TROISIEMECAS	*dans le cas contraire D4!=D5!=D6 (troisiemecas))..




NEXT1   CMP D5,D6		*on compare D5 et D6..
	BEQ DEUXIEMECAS		*dans le cas d'egalit� D4=D5=D6!=0 (deuxiemecas)..
	BRA QUATRIEMECAS	*dans le cas contraire D4=D5!=D6 (quatriemecas)
	

	
	
SUITE1 	CMP #0,D5		*on compare 0 � D5..
	BEQ SUITE2              *dans le cas d'egalit�..
	CMP D5,D6		*si D5!=0 et D4=0 on compare D5 et D6..
	BEQ QUATRIEMECAS	*dans le cas d'egalite D4!=D5=D6 (quatriemecas)..   
	CMP D4,D6		*dans le cas contraire on compare D4 et D6..
	BEQ QUATRIEMECAS	*dans le cas d'egalit� D4=D6!=D5 (quatrieme cas)..
	BRA TROISIEMECAS	*dans la cas contraire D4!=D5!=D6 (troisieme cas)..
	
SUITE2  CMP #0,D6		*on compare 0 � D6..
	BEQ PREMIERCAS		*dans le cas d'egalit� .. cad D4=D5=D6=0 (premiercas)..
	BRA QUATRIEMECAS	*si D6 !=0 , D4=D5=0..(qutrieme cas)..



PREMIERCAS      move.w #1,D2	*cas ou D4,D5,D6 sont tous � 0
		BRA FIN


DEUXIEMECAS	move.w #2,D2	*cas ou D4,D5,D6 sont tous egaux mais non nuls
	        BRA FIN


TROISIEMECAS	move.w #3,D2	*cas ou D4,D5,D6 sont deux � deux distincts
	        BRA FIN



QUATRIEMECAS	move.w #4,D2	*cas ou D4,D5,D6 sont distincts

		BRA FIN 


FIN



* Variables and Strings

VAR1 DC.w 1

VAR2 DC.w 2

VAR3 DC.w 2

	END	START		; last line of source



*~Font name~Courier New~
*~Font size~10~
*~Tab type~1~
*~Tab size~8~
