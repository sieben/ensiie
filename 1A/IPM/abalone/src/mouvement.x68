*------------------------------------------------------------------------------
*------------------------------------------------------------------------------
*
* MOUVEMENT 
*
*------------------------------------------------------------------------------
*------------------------------------------------------------------------------


* L'algo consiste � d�placer vers l'avant � partir de la premi�re case non vide
* Pour ce faire on va utiliser les trois registres d'adresses � notre disposition

* A2 et A3 seront les registres mobiles. Ce sont eux qui vont parcourir le tableau 
* et faire les �changes opportuns.
* Typiquement on a d�placer le contenu de la case point�e par A2 vers la case point�e par A3
* Puis on d�cr�mente A2 et A3
* A0 sera le registre qui va servir de but�e, en effet cet algo de d�placement doit bien
* s'arr�ter quand on a d�placer toutes les billes et A0 pointra vers la premi�re case non
* vide du tableau.
* A chaque �tape on testera si A2 et A0 sont �gaux, si tel est le cas cela signifie que l'on est 
* tomb� sur un cas d'arr�t et � ce moment l� l'algo de d�placement est termin�.


*------------------------------------------------------------------------------
* Fonction d'�change de deux donn�es point�e par A2 et A3
*------------------------------------------------------------------------------

switch 

    MOVE.L    (A2),D2
    MOVE.L    (A3),(A2)
    MOVE.L    D2,(A3)
    
    RTS

*------------------------------------------------------------------------------
* Fonction de mise � jour du tablier
*------------------------------------------------------------------------------

make_move

    MOVE.L    #contenu_case,A0
    MOVEQ.L   #6,D2                      * C'est peut etre 5 ?
    
boucle_first_non_void

    CMP       #'_',(A0)
    BEQ       boucle_next_first_void
    DBF       D2,boucle_first_non_void
    
    MOVE.L    A0,A2
    
boucle_last_void

boucle_next_first_void

    ADDA      $4,A0
    DBF       D2,boucle_first_non_void

    
boucle_next_last_void



    RTS



*~Font name~Courier New~
*~Font size~10~
*~Tab type~1~
*~Tab size~8~
