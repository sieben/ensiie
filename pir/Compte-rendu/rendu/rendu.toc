\select@language {french}
\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}La qualit\IeC {\'e} de service (QoS)}{2}{subsection.1.1}
\contentsline {section}{\numberline {2}Mise en place de la qualit\IeC {\'e} de service sur des r\IeC {\'e}seaux Wi-fi}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Pr\IeC {\'e}sentation du Wifi}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}La norme 802.11}{3}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}DCF - \textit {Distributed coordination function}}{3}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}PCF - \textit {Point coordination function}}{4}{subsubsection.2.2.2}
\contentsline {subsection}{\numberline {2.3}La norme 802.11e}{4}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}HCF - \textit {Hybrid coordination function}}{4}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}EDCA - \textit {Enhanced distributed coordinated Access}}{4}{subsubsection.2.3.2}
\contentsline {subsubsection}{\numberline {2.3.3}HCCA - \textit {HCF controlled channel access}}{5}{subsubsection.2.3.3}
\contentsline {section}{\numberline {3}VoIP}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}Architecture de la VoIP}{5}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Les codecs audios utilis\IeC {\'e}s}{5}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}G711}{5}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}G723}{5}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}G729}{5}{subsubsection.3.2.3}
\contentsline {subsection}{\numberline {3.3}Securit\IeC {\'e} \& VoIP}{5}{subsection.3.3}
