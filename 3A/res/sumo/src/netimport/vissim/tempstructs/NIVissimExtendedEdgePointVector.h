/****************************************************************************/
/// @file    NIVissimExtendedEdgePointVector.h
/// @author  Daniel Krajzewicz
/// @date    Sept 2002
/// @version $Id: NIVissimExtendedEdgePointVector.h 9525 2011-01-04 21:22:52Z behrisch $
///
// -------------------
/****************************************************************************/
// SUMO, Simulation of Urban MObility; see http://sumo.sourceforge.net/
// Copyright (C) 2001-2011 DLR (http://www.dlr.de/) and contributors
/****************************************************************************/
//
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//
/****************************************************************************/
#ifndef NIVissimExtendedEdgePointVector_h
#define NIVissimExtendedEdgePointVector_h


// ===========================================================================
// included modules
// ===========================================================================
#ifdef _MSC_VER
#include <windows_config.h>
#else
#include <config.h>
#endif


#include <vector>
#include "NIVissimExtendedEdgePoint.h"

typedef std::vector<NIVissimExtendedEdgePoint*> NIVissimtendedEdgePointVector;


#endif

/****************************************************************************/

