/****************************************************************************/
/// @file    RONode.cpp
/// @author  Daniel Krajzewicz
/// @date    Sept 2002
/// @version $Id: RONode.cpp 11445 2011-10-31 23:24:01Z behrisch $
///
// A single router's node
/****************************************************************************/
// SUMO, Simulation of Urban MObility; see http://sumo.sourceforge.net/
// Copyright (C) 2001-2011 DLR (http://www.dlr.de/) and contributors
/****************************************************************************/
//
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//
/****************************************************************************/


// ===========================================================================
// included modules
// ===========================================================================
#ifdef _MSC_VER
#include <windows_config.h>
#else
#include <config.h>
#endif

#include <string>
#include "RONode.h"

#ifdef CHECK_MEMORY_LEAKS
#include <foreign/nvwa/debug_new.h>
#endif // CHECK_MEMORY_LEAKS


// ===========================================================================
// method definitions
// ===========================================================================
RONode::RONode(const std::string& id) throw()
    : Named(id) {}


RONode::~RONode() throw() {}


void
RONode::setPosition(const Position& p) throw() {
    myPosition = p;
}



/****************************************************************************/

