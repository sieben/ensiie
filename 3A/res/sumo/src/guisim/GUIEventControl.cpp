/****************************************************************************/
/// @file    GUIEventControl.cpp
/// @author  Daniel Krajzewicz
/// @date    Mon, 04 Feb 2008
/// @version $Id: GUIEventControl.cpp 11445 2011-10-31 23:24:01Z behrisch $
///
// Stores time-dependant events and executes them at the proper time (guisim)
/****************************************************************************/
// SUMO, Simulation of Urban MObility; see http://sumo.sourceforge.net/
// Copyright (C) 2001-2011 DLR (http://www.dlr.de/) and contributors
/****************************************************************************/
//
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//
/****************************************************************************/


// ===========================================================================
// included modules
// ===========================================================================
#ifdef _MSC_VER
#include <windows_config.h>
#else
#include <config.h>
#endif

#include <cassert>
#include <utils/foxtools/MFXMutex.h>
#include "GUIEventControl.h"

#ifdef CHECK_MEMORY_LEAKS
#include <foreign/nvwa/debug_new.h>
#endif // CHECK_MEMORY_LEAKS


// ===========================================================================
// member definitions
// ===========================================================================
GUIEventControl::GUIEventControl() throw() {}


GUIEventControl::~GUIEventControl() throw() {
}


SUMOTime
GUIEventControl::addEvent(Command* operation,
                          SUMOTime execTimeStep,
                          AdaptType type) throw() {
    myLock.lock();
    SUMOTime ret = MSEventControl::addEvent(operation, execTimeStep, type);
    myLock.unlock();
    return ret;
}


void
GUIEventControl::execute(SUMOTime execTime) throw(ProcessError) {
    myLock.lock();
    try {
        MSEventControl::execute(execTime);
    } catch (ProcessError&) {
        myLock.unlock();
        throw;
    }
    myLock.unlock();
}



/****************************************************************************/

