/****************************************************************************/
/// @file    GUIEdgeControlBuilder.cpp
/// @author  Daniel Krajzewicz
/// @date    Sept 2002
/// @version $Id: GUIEdgeControlBuilder.cpp 11445 2011-10-31 23:24:01Z behrisch $
///
// Derivation of NLEdgeControlBuilder which build gui-edges
/****************************************************************************/
// SUMO, Simulation of Urban MObility; see http://sumo.sourceforge.net/
// Copyright (C) 2001-2011 DLR (http://www.dlr.de/) and contributors
/****************************************************************************/
//
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//
/****************************************************************************/


// ===========================================================================
// included modules
// ===========================================================================
#ifdef _MSC_VER
#include <windows_config.h>
#else
#include <config.h>
#endif

#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include <guisim/GUIEdge.h>
#include <guisim/GUINet.h>
#include <guisim/GUILane.h>
#include <guisim/GUIInternalLane.h>
#include <microsim/MSJunction.h>
#include <netload/NLBuilder.h>
#include "GUIEdgeControlBuilder.h"
#include <gui/GUIGlobals.h>

#ifdef CHECK_MEMORY_LEAKS
#include <foreign/nvwa/debug_new.h>
#endif // CHECK_MEMORY_LEAKS


// ===========================================================================
// method definitions
// ===========================================================================
GUIEdgeControlBuilder::GUIEdgeControlBuilder() throw()
    : NLEdgeControlBuilder() {}


GUIEdgeControlBuilder::~GUIEdgeControlBuilder() throw() {}


MSEdge*
GUIEdgeControlBuilder::closeEdge() {
    MSEdge* ret = NLEdgeControlBuilder::closeEdge();
    static_cast<GUIEdge*>(ret)->initGeometry();
    return ret;
}


MSLane*
GUIEdgeControlBuilder::addLane(const std::string& id,
                               SUMOReal maxSpeed, SUMOReal length,
                               const PositionVector& shape,
                               SUMOReal width,
                               const SUMOVehicleClasses& allowed,
                               const SUMOVehicleClasses& disallowed) {
    MSLane* lane = 0;
    switch (myFunction) {
    case MSEdge::EDGEFUNCTION_INTERNAL:
        lane = new GUIInternalLane(id, maxSpeed, length, myActiveEdge,
                                   myCurrentNumericalLaneID++, shape, width, allowed, disallowed);
        break;
    case MSEdge::EDGEFUNCTION_NORMAL:
    case MSEdge::EDGEFUNCTION_CONNECTOR:
        lane = new GUILane(id, maxSpeed, length, myActiveEdge,
                           myCurrentNumericalLaneID++, shape, width, allowed, disallowed);
        break;
    default:
        throw InvalidArgument("A lane with an unknown type occured (" + toString(myFunction) + ")");
    }
    myLaneStorage->push_back(lane);
    return lane;
}



MSEdge*
GUIEdgeControlBuilder::buildEdge(const std::string& id, const std::string& streetName) throw() {
    return new GUIEdge(id, myCurrentNumericalEdgeID++, streetName);
}

/****************************************************************************/

