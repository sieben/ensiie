"""
@file    sumonet.py
@author  Daniel.Krajzewicz@dlr.de
@date    2008-03-27
@version $Id: sumonet.py 10691 2011-06-23 12:40:25Z behrisch $

Deprecated version of sumolib/net.py.

Copyright (C) 2008-2011 DLR (http://www.dlr.de/) and contributors
All rights reserved
"""

import os, sys
print >> sys.stderr, "Using this library is deprecated, please use sumolib.net instead!"
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from sumolib.net import *
