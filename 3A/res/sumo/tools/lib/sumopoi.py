#!/usr/bin/env python
"""
@file    sumopoi.py
@author  Daniel.Krajzewicz@dlr.de
@date    2010-02-18
@version $Id: sumopoi.py 10691 2011-06-23 12:40:25Z behrisch $

Deprecated version of sumolib/poi.py.

Copyright (C) 2010-2011 DLR (http://www.dlr.de/) and contributors
All rights reserved
"""

import os, sys
print >> sys.stderr, "Using this library is deprecated, please use sumolib.poi instead!"
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from sumolib.poi import *
