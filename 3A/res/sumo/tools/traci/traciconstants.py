# -*- coding: utf-8 -*-
"""
@file    traciconstants.py

This file exists for backward compatiblity only.

Copyright (C) 2009-2011 DLR (http://www.dlr.de/) and contributors
All rights reserved
"""
from constants import *

import sys
print >> sys.stderr, """traciconstants.py and traciControl.py are deprecated.
Please use the new Python API for TraCI."""
