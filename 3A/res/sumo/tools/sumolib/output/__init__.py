# -*- coding: utf-8 -*-
"""
@file    __init__.py
@author  Michael.Behrisch@dlr.de
@date    2011-06-23
@version $Id: __init__.py 10721 2011-06-25 06:35:18Z behrisch $

Python interface to SUMO especially for parsing output files.

Copyright (C) 2011-2011 DLR (http://www.dlr.de/) and contributors
All rights reserved
"""
import dump, inductionloop
