%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode
\documentclass[11pt, a4paper]{article}

\include{settings}

\begin{document}

\input{header}

\section{Introduction}

On se propose de simuler une solution possible afin de mettre en
place de la vidéosurveillance au sein d'un VMN (\textit{Vehicular Mesh
Network}).  En particulier, nous pouvons imaginer le problème d'un opérateur de
transports en commun, souhaitant équiper l'ensemble de ses bus de caméras
embarquées et d'un système de mise en relation avec un service central de
sécurité.  En cas d'incident, un flux vidéo peut être subrepticement activé
par le conducteur et envoyé à la centrale.

Plusieurs possibilités seraient envisageables.  Énormément de recherches ont
déjà été faites concernant les \textit{Vehicular Ad-Hoc Networks} (VANET).
Cependant, l'idée ici n'est pas d'utiliser une telle architecture distribuée,
mais plutôt un réseau type Wi-Fi en mode Infrastructure (fig. \ref{fig:infra}).

\begin{figure}[htp]
	\centering
	\includegraphics[clip=true,scale=0.72,trim=150 490 340 30]{gfx/infra.pdf}
	\caption{Idée de mise en œuvre du réseau de vidéosurveillance.  Les bus du
	réseau, symbolisés en bleu, sont reliés en permanence à un réseau Wi-Fi
	maillé, dont les antennes fixes sont installés sur les toits de bâtiments
	proches des lignes de bus.}
	\label{fig:infra}
\end{figure}

L'utilisation d'un réseau maillé 802.11s présente l'intérêt de minimiser les
coûts occasionnés par le câblage entre les multiples points d'accès.

Dans un premier temps, nous aborderons des généralités sur les réseaux sans fil
maillés, et sur la vidéo.  Ensuite, nous évoquerons les outils et les modèles
utilisés pour nos simulations.  Enfin, nous exposerons notre plan de tests.

\section{Généralités}

\subsection{Réseaux sans fil maillés}

Les réseaux Wi-Fi fonctionnent généralement en mode Infrastructure, c'est-à-dire
que des \emph{points d'accès} fournissent une couverture radio suffisante pour
que des \emph{clients} puissent s'y connecter dans un rayon limité.  Cependant,
de telles architectures ne permettent pas de couvrir une zone plus étendue,
comme un quartier, voire une ville entière.

À l'inverse, dans un réseau sans fil maillé, chaque \emph{point d'accès} sert de
\emph{relais} entre ses voisins et d'éventuels clients (fig. \ref{fig:mesh}).
Ceci permet de construire un réseau sans fil couvrant des zones beaucoup plus
étendues, et ce en éliminant quasiment tous les coûts de câblage réseau.
D'autres avantages incluent une robustesse accrue, une maintenance plus aisée,
et un service plus fiable \cite{citeulike:784761}.

\begin{figure}[htp]
	\centering
	\includegraphics[width=0.8\textwidth]{gfx/mesh.png}
	\caption{Illustration d'un réseau 802.11s}
	\label{fig:mesh}
\end{figure}

Cependant, la principale problématique concernant tout réseau sans fil maillé
utilisant MAC comme protocole de niveau 2 porte sur le passage à l'échelle.
Quel que soit l'algorithme de routage utilisé, le nombre de sauts entre deux
stations ne doit pas être supérieur à 4, sous peine d'une dégradation importante
de la bande passante.  \cite{citeulike:784761}

\subsection{Vidéo}

Les progrès récents en termes de codage vidéo, ainsi que le standard H.264,
rendent la transmission de vidéo en temps réel d'autant plus facile que les
applications sont nombreuses. \cite{citeulike:310671}

Les principales caractéristiques d'un flux vidéo est que le délai doit être
faible, et le flux doit tolérer la perte de quelques paquets sans conséquences
néfastes sur le reste du trafic.  Le codec utilisé joue également un rôle, en
termes de quantité de données par intervalle de temps à transporter sur le
réseau pour obtenir une qualité donnée, en termes de robustesse face à des
erreurs de transmission, mais aussi en termes de puissance de calcul nécessaire
pour l'encodage et le décodage, qui doit être fait à la volée.

\section{Outils de simulation}

La simulation d'un tel réseau en fonctionnement requiert l'utilisation
simultanée de deux outils de simulation, afin de simuler un trafic urbain d'une
part, et un trafic réseau d'autre part.

\subsection{Simulation de trafic urbain: SUMO}

L'outil SUMO a été choisi afin de simuler de manière réaliste un trafic urbain,
où certains véhicules sont des bus qui suivent un itinéraire prédéterminé.  Le
reste du trafic est généré à l'aide de modèles.

\subsection{Modélisation de trafic routier comprenant des bus}

Au sein de SUMO, les bus sont configurables\footnote{
	\url{http://sumo.sourceforge.net/doc/current/docs/Simulation/Public_Transport.html}
}, en décrivant le véhicule, les positions des arrêts, ainsi que leur durée.
Ainsi, on peut modéliser plusieurs lignes de bus, avec chacun plusieurs
véhicules.

\subsection{Simulation réseau: ns3}

L'outil de simulation réseau que nous avons retenu est NS3.  Celui-ci permet, en
particulier, de simuler un réseau 802.11s\footnote{
	\url{http://www.nsnam.org/doxygen-release/group__dot11s.html}
}, même s'il semblerait que l'implémentation soit imparfaite.  

\subsubsection{Modélisation d'un flux vidéo}

Un flux vidéo est un flux qui tolère la perte de paquets, mais ne supporte pas
de latence trop élevée.  Il est donc possible de le simuler à l'aide d'un flux
UDP, car les fonctions de retransmission et de fiabilité de TCP ne sont pas
nécessaires.

Un flux vidéo peut être à bitrate constant (CBR) ou variable (VBR).  Dans un
premier temps, nous utiliserons un modèle CBR.  Nous pourrons alors expérimenter
avec des débits variant entre 250~kbps et 2~Mbps.

\subsubsection{Abandon de iTetris \& création d'un jeu de trace}

Lors du rapport bibliographique précédent, il était question d'utiliser iTetris
afin de combiner \texttt{ns3} et \texttt{sumo}.
En raison d'un processus d'installation complexe
et peu documenté, nous avons préféré utiliser une approche consistant à
développer nous mêmes notre outil. Nous avons voulu utiliser
les traces produites par \texttt{sumo}
et compatibles au format de trace ns2 en les injectant dans
 une simulation \texttt{ns3} via un
\texttt{Ns2MobilityHelper}.

\section{Plan de tests}

\subsection{Problématique}

L'une des première question que nous voulons étudier est la stabilité du flux
vidéo entre le bus et le poste central en fonction du temps.
Il parait clair qu'une transmission de qualité suffisante ne pourra être
effective dans des cas de gigue très élevée. Ainsi nous avons voulu étudier
l'évolution du débit entre le bus et la station centrale en fonction du temps.

\subsection{Test avec un seul véhicule}

Dans un premier temps, nous validerons la faisabilité d'un envoi de flux vidéo à
travers un réseau 802.11s, depuis un équipement mobile en mouvement modélisant
le bus à au plus 13~m/s.  

\subsubsection{Création des traces de mouvement de véhicules via \textit{sumo}}

La création des fichiers de trace est décomposée en plusieurs étapes :
\begin{description}

\item[Création du réseau routier :] Un réseau routier dans \textit{sumo}
 est une collection de
nœuds reliés par des arêtes. Le réseau routier utilisé pour la simulation
est en forme de H avec deux
routes parallèles de 3,5 km de long, coupées à angle droit à mi-longueur par une
route qui les relie.

L'utilitaire \texttt{netconvert} a été utilisé afin de transformer ces
informations en un fichier exploitable par \texttt{sumo}.

\item[Création des véhicules :] Les fichiers d'exemples fournis avec le code
source de \texttt{sumo} ont étés utilisés afin de fournir rapidement une liste
de véhicules pouvant être utilisés. Il est à noter que tous les véhicules
utilisés dans la simulation sont définis individuellement dans un fichier XML.
Les véhicules ne sont pas générés via un quelconque processus automatique.

\item[Création d'un bus :] La description d'un des véhicules a été modifiée afin
de lui affecter les propriétés standard d'un bus telles que la longueur,
l'accélération, le poids, le freinage.

Une fois que le réseau routier et les véhicules ont étés définis, \textit{sumo}
a été lancé avec l'option \texttt{--netstate}, ce qui permet d'obtenir un
fichier XML listant la position de chaque noeud du système à chaque instant.

\item[Génération de fichiers TCL :] \textit{traceexporter} est l'utilitaire
	utilisé afin de transformer les résultats de la simulation de \textit{sumo} en
	un fichier exploitable par \textit{ns3}.

\item[Filtrage des résultats :] Afin de ne prendre que les informations
concernant le mouvement du bus nous devons effectuer un filtre sur la trace
obtenue lors de l'étape précédente. Nous utilisons une simple commande
\textit{grep} afin de ne sélectionner via l'identifiant du véhicule considéré
que les résultats pertinents.
\end{description}

\section{Résultats}

En l'état, les résultats ne sont pas exploitables, car les traces générées
montrent que le bus envoie un flux vidéo CBR, mais qu'il n'est pas reçu par la
station de destination, bien qu'il soit théoriquement dans la portée du réseau
maillé.  Ce résultat a été obtenu après avoir longuement lutté pour tenter de
faire fonctionner la simulation, ne serait-ce qu'en reprenant des exemples
simples, en essayant des versions différentes de ns-3 (3.7, 3.12.1 et 3.13),
voire en essayant des outils différents (comme ns-2).

Il ne nous est pas possible d'expliquer ce phénomène, autrement que par un
possible défaut dans l'implémentation de la couche de protocoles 802.11s au sein
de NS-3, ou bien par un détail d'implémentation qui nous a échappé.  En
particulier, nous avons apparemment mis au jour un bug dans le traçage pcap d'un
objet YansWifiPhyHelper.

%\subsection{Problèmes rencontrés}
%
%L'utilisation de ns-3 (version 3.12.1) fournissaient des
%traces au format pcap ne contenaient aucun paquet IP.
%
%Plus exactement, tcpdump -r foo.pcap ip, ne donnait
%aucun résultat, à part les beacons que les nœuds s'envoient
%périodiquement.  Même résultat avec l'exemple mesh.cc qui est fourni
%dans le code source de ns3, ou même encore avec le code en annexe d'une
%thèse de doctorat portant sur des simulations de réseaux 802.11s sous
%ns-3 (http://epubl.ltu.se/1653-0187/2010/041/LTU-PB-EX-10041-SE.pdf):
%pas un seul paquet IP capturé.  Ainsi, même en partant de code supposé
%juste, il semble que nous soyions tombés sur un bug de ns-3.  Bug qui,
%de plus, ne semble pas avoir été corrigé dans la version 3.13.
%
%Les traces XML générées par l'exemple mesh.cc ne montraient pas de
%trafic non plus, ce qui indique que le problème n'est pas issu du module
%d'écriture de fichiers pcap de ns-3.
%
%Ensuite, persuadés que nous aurions peut-être eus de meilleurs résultats
%sous ns-2, j'ai essayé, en vain, de le faire fonctionner tout en
%intégrant le patch 802.11s qui va bien.  Le patch en question, que
%j'avais trouvé, était incomplet, et était pour la plupart documenté en
%chinois, donc ce n'était pas non plus une option viable.
%
%Enfin, en revenant sous ns-3.7, qui était la version de ns-3 utilisée
%par l'auteur de la thèse, et en reprenant le code de la thèse, nous ne
%sommes pas non plus parvenus à obtenir des .pcap contenant des résultats
%significatifs (i.e. que des beacons, aucun paquet IP).

\section{Conclusion}

Bien que nous n'avons pas obtenu de résultats, cet essai de simulation a
néanmoins permis de nous interroger sur les problématiques liées à la mise en
place d'un réseau 802.11s global à une agglomération dans le cadre d'un projet
de vidéosurveillance dans les transports en commun.

En particulier, pour une agglomération comme Paris, la faisabilité d'un tel
projet est également lié à son coût de mise en œuvre.  Par exemple, si on estime
qu'on espace les antennes d'un réseau maillé d'environ 40 mètres en moyenne
(compte tenu des interférences pouvant peut-être être causées par d'autres
équipements fixes ou mobiles), et que le réseau de bus de Paris (intra-muros)
comporte 59 lignes de 9,6~km en moyenne, cela ferait plus de 14~000 antennes à
déployer, rien que pour longer les tronçons parcourus par les véhicules.  Sans
compter la maintenance des antennes, ainsi que l'espacement et la mise en œuvre
des centres de vidéosurveillance, et des moyens humains nécessaires.

De plus, bien que certains vantent les mérites de pareilles mesures, leur
efficacité est souvent remis en question, l'exemple de Londres étant par
ailleurs cité comme cas de référence.  Ceci dit, un réseau maillé comme celui
que l'on imagine a également d'autres utilisations, comme la diffusion de
publicités, ce qui permettrait au moins de rentabiliser l'investissement.  De
tels réseaux, s'ils sont possibles, sont effectivement polyvalents.

\nocite{*}

\selectlanguage{english}
\bibliographystyle{alpha}
\bibliography{main}

\end{document}

% vi:ts=2:sw=2:tw=80:
