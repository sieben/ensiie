%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode
\documentclass[11pt, a4paper]{article}

\include{settings}
\include{commands}

\begin{document}

\input{header}

\section{Introduction}

On se propose de simuler une solution possible afin de mettre en
place de la vidéosurveillance au sein d'un VMN (\textit{Vehicular Mesh
Network}).  En particulier, nous pouvons imaginer le problème d'un opérateur de
transports en commun, souhaitant équiper l'ensemble de ses bus de caméras
embarquées et d'un système de mise en relation avec un service central de
sécurité.  En cas d'incident, un flux vidéo peut être subrepticement activé
par le conducteur et envoyé à la centrale.

Plusieurs possibilités seraient envisageables. Énormément de recherches ont
déjà été faites concernant les \textit{Vehicular Ad-Hoc Networks} (VANET).
Cependant, l'idée ici n'est pas d'utiliser une telle architecture distribuée,
mais plutôt un réseau type Wi-Fi en mode Infrastructure (fig. \ref{fig:infra}).

\begin{figure}[htp]
	\centering
	\includegraphics[clip=true,scale=0.72,trim=150 490 340 30]{gfx/infra.pdf}
	\caption{Idée de mise en œuvre du réseau de vidéosurveillance.  Les bus du
	réseau, symbolisés en bleu, sont reliés en permanence à un réseau Wi-Fi
	maillé, dont les antennes fixes sont installés sur les toits de bâtiments
	proches des lignes de bus.}
	\label{fig:infra}
\end{figure}

L'utilisation d'un réseau maillé 802.11s présente l'intérêt de minimiser les
coûts occasionnés par le câblage entre les multiples points d'accès.

Dans un premier temps, nous aborderons des généralités sur les réseaux sans fil
maillés, et sur la vidéo.  Ensuite, nous évoquerons les outils et les modèles
utilisés pour nos simulations.  Enfin, nous exposerons notre plan de tests.

\section{Généralités}

\subsection{Réseaux sans fil maillés}

Les réseaux Wi-Fi fonctionnent généralement en mode Infrastructure, c'est-à-dire
que des \emph{points d'accès} fournissent une couverture radio suffisante pour
que des \emph{clients} puissent s'y connecter dans un rayon limité.  Cependant,
de telles architectures ne permettent pas de couvrir une zone plus étendue,
comme un quartier, voire une ville entière.

À l'inverse, dans un réseau sans fil maillé, chaque \emph{point d'accès} sert de
\emph{relais} entre ses voisins et d'éventuels clients (fig. \ref{fig:mesh}).
Ceci permet de construire un réseau sans fil couvrant des zones beaucoup plus
étendues, et ce en éliminant quasiment tous les coûts de câblage réseau.
D'autres avantages incluent une robustesse accrue, une maintenance plus aisée,
et un service plus fiable \cite{citeulike:784761}.

\begin{figure}[htp]
	\centering
	\includegraphics[width=0.8\textwidth]{gfx/mesh.png}
	\caption{Illustration d'un réseau 802.11s}
	\label{fig:mesh}
\end{figure}

Cependant, la principale problématique concernant tout réseau sans fil maillé
utilisant MAC comme protocole de niveau 2 porte sur le passage à l'échelle.
Quel que soit l'algorithme de routage utilisé, le nombre de sauts entre deux
stations ne doit pas être supérieur à 4, sous peine d'une dégradation importante
de la bande passante.  \cite{citeulike:784761}

\subsection{Vidéo}

Les progrès récents en termes de codage vidéo, ainsi que le standard H.264,
rendent la transmission de vidéo en temps réel d'autant plus facile que les
applications sont nombreuses. \cite{citeulike:310671}

Les principales caractéristiques d'un flux vidéo est que le délai doit être
faible, et le flux doit tolérer la perte de quelques paquets sans conséquences
néfastes sur le reste du trafic.  Le codec utilisé joue également un rôle, en
termes de quantité de données par intervalle de temps à transporter sur le
réseau pour obtenir une qualité donnée, en termes de robustesse face à des
erreurs de transmission, mais aussi en termes de puissance de calcul nécessaire
pour l'encodage et le décodage, qui doit être fait à la volée.

\section{Outils de simulation}

La simulation d'un tel réseau en fonctionnement requiert l'utilisation
simultanée de deux outils de simulation, afin de simuler un trafic urbain d'une
part, et un trafic réseau d'autre part.

\subsection{Simulation de trafic urbain: SUMO}

L'outil SUMO a été choisi afin de simuler de manière réaliste un trafic urbain,
où certains véhicules sont des bus qui suivent un itinéraire prédéterminé.  Le
reste du trafic est généré à l'aide de modèles.

\subsection{Modélisation de trafic routier comprenant des bus}

Au sein de SUMO, les bus sont configurables\footnote{
	\url{http://sumo.sourceforge.net/doc/current/docs/Simulation/Public_Transport.html}
}, en décrivant le véhicule, les positions des arrêts, ainsi que leur durée.
Ainsi, on peut modéliser plusieurs lignes de bus, avec chacun plusieurs
véhicules.

\subsection{Simulation réseau: NS3}

L'outil de simulation réseau que nous avons retenu est NS3.  Celui-ci permet, en
particulier, de simuler un réseau 802.11s\footnote{
	\url{http://www.nsnam.org/doxygen-release/group__dot11s.html}
}, même s'il semblerait que l'implémentation soit imparfaite.

\subsubsection{Modélisation d'un flux vidéo}

Un flux vidéo est un flux qui tolère la perte de paquets, mais ne supporte pas
de latence trop élevée.  Il est donc possible de le simuler à l'aide d'un flux
UDP, car les fonctions de retransmission et de fiabilité de TCP ne sont pas
nécessaires.

Un flux vidéo peut être à bitrate constant (CBR) ou variable (VBR).  Dans un
premier temps, nous utiliserons un modèle CBR.  Nous pourrons alors expérimenter
avec des débits variant entre 250~kbps et 2~Mbps.

\subsection{Mise en commun de NS3 et de SUMO}

Lors d'un précédent rapport, nous avions avancé la possibilité d'utiliser
iTetris pour effectuer notre simulation en combinant les traces de SUMO
au simulation de réseau via NS3. En raison de la complexité de l'installation
et de la documentation imparfaite de iTetris, nous avons renoncé à l'utiliser.

Nous avons préféré utiliser une approche plus directe en utilisant les traces
fournies par SUMO et les lire directement depuis NS3 via un Ns2MobilityHelper.

\subsection{Méthodologie}

\subsubsection{Création d'un réseau routier}

La première étape de la constitution d'un environnement de simulation est la
création d'un réseau routier. Il est crée en reliant des noeuds par des routes.

Ce fichier est ensuite passé à un utilitaire (\texttt{netconverter}) afin créer
les fichiers nécessaires pour que SUMO puisse modéliser le réseau routier
adapté.

\subsubsection{Création d'un ensemble de véhicule}

Nous utilisons le fichier donné en exemple dans sumo afin d'avoir un flux de
véhicule rapidement. Ce flux de véhicule

\subsubsection{Modification d'un véhicule pour le transformer en un bus}

Un bus ne comporte pas les mêmes propriétés vis à vis du mouvement que les
véhicules donnés dans l'ensemble de données précédents. Ainsi il est nécessaire
de modifier les paramètres du véhicules afin de lui donner des propriétés plus
proche de celle d'un bus (Freinage, poids, accélération, \ldots)

\subsubsection{Conversion des traces}

Nous transormons le gros jeu de données fournis par le résultat d'une simulation
via l'outil \texttt{ns2-trace-exporter} pour obtenir une trace du mouvement des
véhicules qui est compatible avec ns2 et ns3.

\subsubsection{Selection des traces pertinentes}

Nous utilisons l'outil en ligne de commande \texttt{grep} afin de selectionner
via l'identifiant du véhicule le bus pour n'obtenir que la trace de déplacement
le concernant. Cette trace est compatible avec ns3 et peut être utilisée
directement via un MobilityHelper.

\subsubsection{Importation dans ns3 des traces utilisées}

Nous utilisons la class MobilityHelper afin de lire et d'utiliser les traces
ns3 qui ont été fournies par les étapes précédentes. Nous obtenons ainsi des
informations sur le mouvement du bus qui sont directement exploitables.

\section{Plan de tests}

\subsection{Problématique étudiée}

Nous voulons étudier la stabilité du débit entre le bus et la station centrale en
fonction du temps. Il paraît clair qu'un débit très instable ne pourra faire
passer un flux vidéo de qualité acceptable.

\subsection{Plan du réseau routier}

Le réseau routier est ici en forme de H. Les deux routes parallèles qu'il
comporte font une distance de 3,5 km de long. Elles sont coupées à angle droit
par une route qui les relie sur 1 km.

\subsection{Test avec un seul véhicule}

Dans un premier temps, nous validerons la faisabilité d'un envoi de flux vidéo à
travers un réseau 802.11s, depuis un équipement mobile en mouvement à au plus
50~km/h.

\section{Résultats}

En l'état, les résultats ne sont pas exploitables, car les traces générées
montrent que le bus envoie un flux vidéo CBR, mais qu'il n'est pas reçu par la
station de destination, bien qu'il soit théoriquement dans la portée du réseau
maillé.  Ce résultat a été obtenu après avoir longuement lutté pour tenter de
faire fonctionner la simulation, ne serait-ce qu'en reprenant des exemples
simples, voire en essayant des outils différents (comme ns-2).

Il ne nous est pas possible d'expliquer ce phénomène, autrement que par un
possible défaut dans l'implémentation de la couche de protocoles 802.11s au sein
de NS-3, ou bien par un détail d'implémentation qui nous a échappé.


\nocite{*}

\selectlanguage{english}
\bibliographystyle{alpha}
\bibliography{main}

\end{document}

% vi:ts=2:sw=2:tw=80:
