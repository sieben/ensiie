# set number of nodes
set opt(nn) 884.0

# set activity file
set opt(af) $opt(config-path)
append opt(af) /ns2-act.tcl

# set mobility file
set opt(mf) $opt(config-path)
append opt(mf) /ns2-mob.tcl

# set start/stop time
set opt(start) 0.0
set opt(stop) 899.0

# set floor size
set opt(x) 4000
set opt(y) 1009
set opt(min-x) 0
set opt(min-y) -9
