/*
 * Simulation NS3
 */

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/applications-module.h"
#include "ns3/ns2-mobility-helper.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/mesh-helper.h"
#include "ns3/mobility-helper.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("Simulation");

/* Quelques constantes */

int
main (int argc, char *argv[])
{
	/* Paramètres de ligne de commande */
	uint32_t intervalle_x = 100;
	uint32_t intervalle_y = 100;

	std::string bus_trace_file;

	LogComponentEnable ("Ns2MobilityHelper", LOG_LEVEL_DEBUG);

	/* Parsing des options en ligne de commande */
	CommandLine cmd;
	cmd.AddValue ("tracefile", 
			"Trace NS2 à utiliser pour le mouvement du bus",
			bus_trace_file);
	cmd.AddValue ("intervalle-x",
			"Intervalle (axe X) entre 2 stations",
			intervalle_x);
	cmd.AddValue ("intervalle-y",
			"Intervalle (axe Y) entre 2 stations",
			intervalle_y);
	cmd.Parse (argc, argv);

	
	/* Création des nœuds */
	NodeContainer nodes;
	NodeContainer bus_nodes;


	/* ATTENTION: l'ordre est CRUCIAL */
	/* Création du bus */
	bus_nodes.Create(1);

	/* Création des antennes du mesh */
	nodes.Create(3500 / intervalle_x * 2);


	/* Configuration du canal Wi-Fi */
	YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();
	YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default ();
	wifiPhy.SetChannel (wifiChannel.Create ());

	/* Configuration du mesh helper et de la pile protocolaire */
	MeshHelper mesh = MeshHelper::Default ();
	mesh.SetStackInstaller ("ns3::Dot11sStack");
	mesh.SetSpreadInterfaceChannels (MeshHelper::ZERO_CHANNEL);
	mesh.SetMacType ("RandomStart", TimeValue (Seconds (0.1)));
	mesh.SetNumberOfInterfaces (1);
	
	NetDeviceContainer meshDevices = mesh.Install (wifiPhy, bus_nodes);
	meshDevices = mesh.Install (wifiPhy, nodes);
	
	/* Positionnement du réseau maillé en grille */
	MobilityHelper mobility;
	mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
			"MinX", DoubleValue (0.0),
			"MinY", DoubleValue (980 - intervalle_y),
			"DeltaX", DoubleValue (intervalle_x),
			"DeltaY", DoubleValue (intervalle_y),
			"GridWidth", UintegerValue (3500),
			"LayoutType", StringValue ("RowFirst"));
	mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
	mobility.Install (nodes);

	/* Positionnement du bus */
	Ns2MobilityHelper ns2 = Ns2MobilityHelper (bus_trace_file);

	bus_nodes.Create (1);

	/* Le bus est obligatoirement le nœud 0 d'après le fichier trace,
	 * ainsi que la façon dont est mise en place la simulation.
	 */
	ns2.Install ();


	/* Mise en place de la pile protocolaire Internet */
	InternetStackHelper inetStack;
	inetStack.Install (bus_nodes);
	inetStack.Install (nodes);
	Ipv4AddressHelper address;
	address.SetBase ("10.51.0.0", "255.255.0.0");
	Ipv4InterfaceContainer interfaces = address.Assign (meshDevices);


	/* Mise en place de l'appli de streaming vidéo */
	OnOffHelper onoff ("ns3::UdpSocketFactory", 
		Address(InetSocketAddress(Ipv4Address("10.51.0.8"), 51966)));
	onoff.SetAttribute("OnTime", RandomVariableValue (ConstantVariable (1)));
	onoff.SetAttribute("OffTime", RandomVariableValue (ConstantVariable (0)));
	ApplicationContainer app = onoff.Install (bus_nodes.Get (0));

	app.Start (Seconds (1.0));
	app.Stop (Seconds (300.0));

	/* Lancer la simulation */
	Simulator::Stop (Seconds (900));
	Simulator::Run ();
	Simulator::Destroy ();

	return 0;
}
