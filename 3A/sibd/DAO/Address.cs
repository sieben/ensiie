﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BASE
{
    public class Address
    {
        public int Id { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Street { get; set; }
        public string Postal_code { get; set; }
        public string Country { get; set; }
    }
}
