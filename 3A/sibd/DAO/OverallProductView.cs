﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BASE
{
    public class OverallProductView
    {
        public int C_id { get; set; }
        public string C_name { get; set; }
        public string C_description { get; set; }
        public int P_id { get; set; }
        public string P_name { get; set; }
        public decimal P_unit_price { get; set; }
        public int I_id { get; set; }
        public string I_name { get; set; }
        public decimal I_price { get; set; }
        public int I_quantity { get; set; }
    }
}
