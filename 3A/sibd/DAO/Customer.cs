﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BASE
{
    public class Customer
    {
        public int Id { get; set; }
        public string First_name { get; set; }
        public string Last_name { get; set; }
        public string Mail { get; set; }
        public string Phone { get; set; }
        public int Address_id { get; set; }
    }
}
