﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BASE
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Category_id { get; set; }
        public decimal Unit_price { get; set; }
    }
}
