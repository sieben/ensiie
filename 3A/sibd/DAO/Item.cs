﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BASE
{
    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Product_id { get; set; }
        public int Quantity { get; set; }
    }
}
