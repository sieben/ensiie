﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entity
{
    public class User
    {
        public int Id { get; set; }
        public string User_name { get; set; }
        public string Password { get; set; }
        public int Customer_id { get; set; }
        public string Is_admin { get; set; }
    }
}
