﻿<%@ Page Title="Gestion des Objets" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Items.aspx.cs" Inherits="pgmshopBackOffice.Products.Items" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<table>
        <tr>
            <td>
                <asp:Label runat="server" Text="Objets" ID="lbItem" Font-Bold="True" 
                    ForeColor="#0066CC" Font-Size="Medium"></asp:Label>
            </td>
        </tr>
        <tr>
             <td>
                <asp:Label runat="server" Text="Catégorie" ID="lbCatItem" Font-Bold="False" 
                    ForeColor="#0066CC" Font-Size="Small"></asp:Label>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlCatItem" 
                    ToolTip="Choisir le nom de la catégorie" 
                    onselectedindexchanged="ddlCatItem_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
             <td>
                <asp:Label runat="server" Text="Produit" ID="lbProdItem" Font-Bold="False" 
                    ForeColor="#0066CC" Font-Size="Small"></asp:Label>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlProdItem" 
                    ToolTip="Choisir le nom du produit" 
                    onselectedindexchanged="ddlProdItem_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                 <asp:Label runat="server" Text="Nom Objet" ID="lbItemName" Font-Bold="False" 
                    ForeColor="#0066CC" Font-Size="Small"></asp:Label>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlItemName" 
                    onselectedindexchanged="ddlItemName_SelectedIndexChanged">
                    <asp:ListItem>Nouvel Objet</asp:ListItem>
                </asp:DropDownList>
            </td>
            </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbItemName" ToolTip="Nouveau nom d'objet"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" Text="Prix" ID="lbItemPrice" Font-Bold="False" 
                    ForeColor="#0066CC" Font-Size="Small"></asp:Label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbItemPrice" ToolTip="Prix de l'objet" 
                    MaxLength="7"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" Text="Quantité" ID="lbItemQty" Font-Bold="False" 
                    ForeColor="#0066CC" Font-Size="Small"></asp:Label>
            </td>
             <td>
                <asp:TextBox runat="server" ID="tbItemQuantity"  
                    MaxLength="7"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button runat="server" ID="btItemAdd" Text="Ajouter/Mettre à jour" 
                    onclick="btItemAdd_Click" />
            </td>
            <td>
                <asp:Button runat="server" ID="btItemDel" Text="Supprimer" 
                    onclick="btItemDel_Click" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Label runat="server" Text="" ID="lbActionStatus" ForeColor="#FF3300" Font-Size="Large"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
