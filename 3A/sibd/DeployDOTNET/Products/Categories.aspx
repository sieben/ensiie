﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Categories.aspx.cs" Inherits="pgmshopBackOffice.Products.Categories" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<table>
        <tr>
            <td>
                <asp:Label runat="server" Text="Catégories" ID="lbCat" Font-Bold="True" 
                    ForeColor="#0066CC" Font-Size="Medium"></asp:Label>
            </td>
        </tr>
        <tr>
             <td>
                <asp:Label runat="server" Text="Nom" ID="lbCatName" Font-Bold="False" 
                    ForeColor="#0066CC" Font-Size="Small"></asp:Label>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlCatName" 
                    ToolTip="Choisir le nom de la catégorie" 
                    onselectedindexchanged="ddlCatName_SelectedIndexChanged">
                    <asp:ListItem>Nouvelle Catégorie</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbCatName" ToolTip="Nouveau nom de catégorie"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" Text="Description" ID="lbCatDescr" Font-Bold="False" 
                    ForeColor="#0066CC" Font-Size="Small"></asp:Label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbCatDescr" ToolTip="Description de la catégorie"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button runat="server" ID="btCatAdd" Text="Ajouter/Mettre à jour" 
                    onclick="btCatAdd_Click" />
            </td>
            <td>
                <asp:Button runat="server" ID="btCatDel" Text="Supprimer" 
                    onclick="btCatDel_Click" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Label runat="server" Text="" ID="lbActionStatus" ForeColor="#FF3300" Font-Size="Large"></asp:Label>
            </td>
        </tr>
        
    </table>
</asp:Content>
