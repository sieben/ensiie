SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS `pgmshop` ;
CREATE SCHEMA IF NOT EXISTS `pgmshop` DEFAULT CHARACTER SET utf8 ;
USE `pgmshop` ;

-- -----------------------------------------------------
-- Table `pgmshop`.`address`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pgmshop`.`address` ;

CREATE  TABLE IF NOT EXISTS `pgmshop`.`address` (
  `id` INT(10) NOT NULL AUTO_INCREMENT ,
  `city` VARCHAR(50) NOT NULL ,
  `state` VARCHAR(50) NOT NULL ,
  `street` VARCHAR(50) NOT NULL ,
  `postal_code` VARCHAR(50) NOT NULL ,
  `country` VARCHAR(50) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `pgmshop`.`category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pgmshop`.`category` ;

CREATE  TABLE IF NOT EXISTS `pgmshop`.`category` (
  `id` INT(10) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(50) NOT NULL ,
  `description` VARCHAR(200) NULL DEFAULT '' ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 33
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `pgmshop`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pgmshop`.`customer` ;

CREATE  TABLE IF NOT EXISTS `pgmshop`.`customer` (
  `id` INT(10) NOT NULL AUTO_INCREMENT ,
  `first_name` VARCHAR(50) NOT NULL ,
  `last_name` VARCHAR(50) NOT NULL ,
  `mail` VARCHAR(50) NOT NULL ,
  `phone` VARCHAR(50) NULL DEFAULT NULL ,
  `address_id` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_customer_address` (`address_id` ASC) ,
  CONSTRAINT `FK_customer_address`
    FOREIGN KEY (`address_id` )
    REFERENCES `pgmshop`.`address` (`id` )
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `pgmshop`.`product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pgmshop`.`product` ;

CREATE  TABLE IF NOT EXISTS `pgmshop`.`product` (
  `id` INT(10) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(50) NULL DEFAULT NULL ,
  `category_id` INT(11) NULL DEFAULT NULL ,
  `description` VARCHAR(200) NULL DEFAULT NULL ,
  `unit_price` DECIMAL(10,2) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_category` (`category_id` ASC) ,
  CONSTRAINT `FK_category`
    FOREIGN KEY (`category_id` )
    REFERENCES `pgmshop`.`category` (`id` )
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `pgmshop`.`item`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pgmshop`.`item` ;

CREATE  TABLE IF NOT EXISTS `pgmshop`.`item` (
  `id` INT(10) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(50) NOT NULL ,
  `price` DECIMAL(10,2) NULL DEFAULT NULL ,
  `product_id` INT(10) NOT NULL ,
  `quantity` INT(10) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_product` (`product_id` ASC) ,
  CONSTRAINT `FK_product`
    FOREIGN KEY (`product_id` )
    REFERENCES `pgmshop`.`product` (`id` )
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `pgmshop`.`order`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pgmshop`.`order` ;

CREATE  TABLE IF NOT EXISTS `pgmshop`.`order` (
  `id` INT(10) NOT NULL AUTO_INCREMENT ,
  `date` DATE NULL DEFAULT NULL ,
  `credit_card_type` ENUM('Visa','Master Card','CB','American Express') NOT NULL ,
  `credit_card_number` VARCHAR(16) NOT NULL ,
  `credit_card_expiry_date` VARCHAR(7) NOT NULL ,
  `address_id` INT(10) NOT NULL ,
  `customer_id` INT(10) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_customer` (`customer_id` ASC) ,
  INDEX `FK_order_address` (`address_id` ASC) ,
  CONSTRAINT `FK_customer`
    FOREIGN KEY (`customer_id` )
    REFERENCES `pgmshop`.`customer` (`id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `FK_order_address`
    FOREIGN KEY (`address_id` )
    REFERENCES `pgmshop`.`address` (`id` )
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `pgmshop`.`order_line`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pgmshop`.`order_line` ;

CREATE  TABLE IF NOT EXISTS `pgmshop`.`order_line` (
  `id` INT(10) NOT NULL AUTO_INCREMENT ,
  `quantity` INT(10) NOT NULL ,
  `item_id` INT(10) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_order_line_item` (`item_id` ASC) ,
  CONSTRAINT `FK_order_line_item`
    FOREIGN KEY (`item_id` )
    REFERENCES `pgmshop`.`item` (`id` )
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `pgmshop`.`order_order_line`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pgmshop`.`order_order_line` ;

CREATE  TABLE IF NOT EXISTS `pgmshop`.`order_order_line` (
  `orderline_id` INT(10) NOT NULL ,
  `order_id` INT(10) NOT NULL ,
  INDEX `FK_order_order_line` (`orderline_id` ASC) ,
  INDEX `FK_order` (`order_id` ASC) ,
  CONSTRAINT `FK_order`
    FOREIGN KEY (`order_id` )
    REFERENCES `pgmshop`.`order` (`id` ),
  CONSTRAINT `FK_order_order_line`
    FOREIGN KEY (`orderline_id` )
    REFERENCES `pgmshop`.`order_line` (`id` ))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `pgmshop`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pgmshop`.`user` ;

CREATE  TABLE IF NOT EXISTS `pgmshop`.`user` (
  `id` INT(10) NOT NULL AUTO_INCREMENT ,
  `customer_id` INT(10) NULL DEFAULT NULL ,
  `user_name` VARCHAR(50) NOT NULL ,
  `password` VARCHAR(50) NOT NULL ,
  `is_admin` ENUM('0','1') NOT NULL DEFAULT '0' ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `unique_user` (`user_name` ASC) ,
  INDEX `FK_user_customer` (`customer_id` ASC) ,
  CONSTRAINT `FK_user_customer`
    FOREIGN KEY (`customer_id` )
    REFERENCES `pgmshop`.`customer` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;



-- -----------------------------------------------------
-- Insertion de données pipo
-- -----------------------------------------------------
INSERT INTO category VALUES
	(1, 'keyboard', 'Claviers'),
	(2, 'mouse', 'Souris'),
	(3, 'storage', 'Stockage'),
	(4, 'cables', 'Câbles'),
	(5, 'memory', 'Mémoire vive');

INSERT INTO product VALUES
	(1, 'RedCom KBD-7000', 1, 'Doté d\'interrupteurs Cherry MX Black, ce clavier mécanique est une des références du marché.', 65),
	(2, 'RedCom KBD-9000', 1, 'Doté d\'interrupteurs Cherry MX Black, ce clavier mécanique est une des références du marché.', 85),
	(3, 'RedCom KBD-9500', 1, 'Doté d\'interrupteurs Cherry MX Black, ce clavier mécanique est une des références du marché.', 99),
	(4, 'Medialex M-2', 2, 'Prenez vos adversaires de vitesse avec cette souris laser 800 dpi 7 boutons !', 25),
	(5, 'Medialex M-4', 2, 'Prenez vos adversaires de vitesse avec cette souris laser 1600 dpi 7 boutons !', 55),
	(6, 'Medialex M-6', 2, 'Assurez-vous une victoire certaine avec cette souris laser 3200 dpi 9 boutons !', 70),
	(7, 'Eastern Digital 1 To', 3, 'Disque dur 1 To 7200 rpm avec 64 Mo de cache', 90),
	(8, 'Eastern Digital 1,5 To', 3, 'Disque dur 1,5 To 7200 rpm avec 64 Mo de cache', 120),
	(9, 'Eastern Digital 2 To', 3, 'Disque dur 2 To 7200 rpm avec 64 Mo de cache', 160),
	(10, 'Eastern Digital 3 To', 3, 'Disque dur 3 To 7200 rpm avec 64 Mo de cache', 200),
	(11, 'Eastern Digital 4 To', 3, 'Disque dur 4 To 7200 rpm avec 128 Mo de cache', 350),
	(12, 'Câble RJ-45 5 m plaqué or', 4, 'Oubliez tous vos problèmes de connectivité réseau avec ce câble haute qualité', 15),
	(13, 'Câble HDMI 1.4 - 3 m', 4, 'Oubliez tous vos problèmes d\'affichage avec ce câble haute qualité', 45),
	(14, 'Queenston kit 2 x 2 Go DDR3', 5, 'Mémoire DDR3/ECC de haute qualité cadencée à 1333 MHz', 30),
	(15, 'Queenston kit 2 x 4 Go DDR3', 5, 'Mémoire DDR3/ECC de haute qualité cadencée à 1333 MHz', 70),
	(16, 'Queenston kit 2 x 6 Go DDR3', 5, 'Mémoire DDR3/ECC de haute qualité cadencée à 1667 MHz', 150);

INSERT INTO item VALUES
	(1, 'RedCom KBD-7000', 65, 1, 5),
	(2, 'RedCom KBD-9000', 85, 2, 5),
	(3, 'RedCom KBD-9500', 99, 3, 5),
	(4, 'Medialex M-2', 25, 4, 5),
	(5, 'Medialex M-4', 55, 5, 5),
	(6, 'Medialex M-6', 70, 6, 5),
	(7, 'Eastern Digital 1 To', 90, 7, 5),
	(8, 'Eastern Digital 1,5 To', 120, 8, 5),
	(9, 'Eastern Digital 2 To', 160, 9, 5),
	(10, 'Eastern Digital 3 To', 200, 10, 5),
	(11, 'Eastern Digital 4 To', 350, 11, 5),
	(12, 'Câble RJ-45 5 m plaqué or', 15, 12, 5),
	(13, 'Câble HDMI 1.4 - 3 m', 45, 13, 5),
	(14, 'Queenston kit 2 x 2 Go DDR3', 30, 14, 5),
	(15, 'Queenston kit 2 x 4 Go DDR3', 70, 15, 5),
	(16, 'Queenston kit 2 x 6 Go DDR3', 150, 16, 5);

INSERT INTO address VALUES
	(1, 'PARIS', 'FR', '87 rue La Boétie', '75016', 'FR'),
	(2, 'MARTIGUES', 'FR', '38 Quai des Belges', '13500', 'FR'),
	(3, 'GIF-SUR-YVETTE', 'FR', '49 rue Saint Germain', '91190', 'FR'),
	(4, 'FONTENAY-SOUS-BOIS', 'FR', '44 Boulevard de Normandie', '94120', 'FR'),
	(5, 'LE PUY-EN-VELAY', 'FR', '60 rue Adolphe Wurtz', '43000', 'FR'),
	(6, 'LE BOUSCAT', 'FR', '87 boulevard Aristide Briand', '33110', 'FR'),
	(7, 'SAINTE-MARIE', 'FR', '76 rue Sébastopol', '97438', 'FR'),
	(8, 'VALENCIENNES', 'FR', '93 avenue de Provence', '59300', 'FR'),
	(9, 'CACHAN', 'FR', '8 Chemin Du Lavarin Sud', '94230', 'FR'),
	(10, 'MARSEILLE', 'FR', '56 rue Beauvau', '13001', 'FR'),
	(11, 'GUÉRET', 'FR', '50 Avenue des Tuileries', '23000', 'FR'),
	(12, 'STAINS', 'FR', '93 avenue Jules Ferry', '93240', 'FR'),
	(13, 'CERGY', 'FR', '35 boulevard Albin Durand', '95000', 'FR'),
	(14, 'ÉCHIROLLES', 'FR', '30 Avenue Millies Lacroix', '38130', 'FR'),
	(15, 'BOURGES', 'FR', '84 rue Petite Fusterie', '18000', 'FR'),
	(16, 'YERRES', 'FR', '89 Rue Bonnet', '91330', 'FR'),
	(17, 'NOISY-LE-SEC', 'FR', '39 Rue Roussy', '93130', 'FR'),
	(18, 'QUIMPER', 'FR', '37 rue de Penthièvre', '29000', 'FR'),
	(19, 'CHÂLONS-EN-CHAMPAGNE', 'FR', '21 boulevard Albin Durand', '51000', 'FR'),
	(20, 'AMIENS', 'FR', '61 rue de Geneve', '80000', 'FR'),
	(21, 'CRÉTEIL', 'FR', '73 boulevard Bryas', '94000', 'FR'),
	(22, 'SAINT-MALO', 'FR', '28 rue des Dunes', '35400', 'FR'),
	(23, 'MONTPELLIER', 'FR', '75 Avenue des Prés', '34000', 'FR'),
	(24, 'CLAMART', 'FR', '49 Rue de Strasbourg', '92140', 'FR'),
	(25, 'CHARTRES', 'FR', '91 place Maurice-Charretier', '28000', 'FR'),
	(26, 'CLAMART', 'FR', '41 Rue de Strasbourg', '92140', 'FR'),
	(27, 'NOUMÉA', 'FR', '8 Rue Roussy', '98800', 'FR'),
	(28, 'GAP', 'FR', '12 rue Saint Germain', '05000', 'FR'),
	(29, 'TOULOUSE', 'FR', '41 rue Pierre De Coubertin', '31000', 'FR'),
	(30, 'PARIS', 'FR', '15 Faubourg Saint Honoré', '75018', 'FR');

INSERT INTO customer VALUES
	(1, 'Babette', 'Boucher', 'BabetteBoucher@dodgit.com', '0142871963', 1),
	(2, 'Brigitte', 'Lacasse', 'BrigitteLacasse@spambob.com', '0449719856', 2),
	(3, 'Gauthier', 'Jodoin', 'GauthierJodoin@mailinator.com', '0122538160', 3),
	(4, 'Aloin', 'Gamelin', 'AloinGamelin@dodgit.com', '0170104326', 4),
	(5, 'Rive', 'Étoile', 'RiveEtoile@trashymail.com', '0406835122', 5),
	(6, 'Tabor', 'Lagueux', 'TaborLagueux@pookmail.com', '0506851256', 6),
	(7, 'Henry', 'Vaillancour', 'HenryVaillancour@mailinator.com', '0538041651', 7),
	(8, 'Vachel', 'Goudreau', 'VachelGoudreau@spambob.com', '0353165780', 8),
	(9, 'Luce', 'Paimboeuf', 'LucePaimboeuf@trashymail.com', '0175101142', 9),
	(10, 'Arienne', 'Leroy', 'ArienneLeroy@trashymail.com', '0427772005', 10),
	(11, 'Sébastien', 'Parrot', 'SebastienParrot@pookmail.com', '0579710104', 11),
	(12, 'Annette', 'Fouquet', 'AnnetteFouquet@dodgit.com', '0151745027', 12),
	(13, 'Martin', 'Laforge', 'MartinLaforge@dodgit.com', '0122027602', 13),
	(14, 'Burkett', 'Lefèbvre', 'BurkettLefebvre@mailinator.com', '0460713566', 14),
	(15, 'Sydney', 'Bouchard', 'SydneyBouchard@pookmail.com', '0209090679', 15),
	(16, 'Élisabeth', 'Chatigny', 'ElisabethChatigny@trashymail.com', '0159772027', 16),
	(17, 'Lorraine', 'Gagné', 'LorraineGagne@dodgit.com', '0128855001', 17),
	(18, 'Nouel', 'Chrétien', 'NouelChretien@trashymail.com', '0250579753', 18),
	(19, 'Cheney', 'Brodeur', 'CheneyBrodeur@spambob.com', '0316105738', 19),
	(20, 'Linette', 'Lambert', 'LinetteLambert@mailinator.com', '0350311915', 20),
	(21, 'Henriette', 'Salois', 'HenrietteSalois@mailinator.com', '0199850667', 21),
	(22, 'Montague', 'Truchon', 'MontagueTruchon@spambob.com', '0204812904', 22),
	(23, 'Ferragus', 'Chandonnet', 'FerragusChandonnet@spambob.com', '0483457175', 23),
	(24, 'Fortun', 'Vernadeau', 'FortunVernadeau@trashymail.com', '0105363263', 24),
	(25, 'Cendrillon', 'Noël', 'CendrillonNoel@trashymail.com', '0237767880', 25),
	(26, 'Isaac', 'Laforest', 'IsaacLaforest@dodgit.com', '0145860670', 26),
	(27, 'Archard', 'Loiselle', 'ArchardLoiselle@dodgit.com', '0362911751', 27),
	(28, 'Harriette', 'Doiron', 'HarrietteDoiron@mailinator.com', '0439689553', 28),
	(29, 'Talon', 'Deschênes', 'TalonDeschenes@pookmail.com', '0508283580', 29),
	(30, 'Albertine', 'Jobin', 'AlbertineJobin@mailinator.com', '0174889106', 30);

INSERT INTO `user` VALUES
	(1, 1, 'BabetteBoucher', PASSWORD('uogh8Ahcah'), '0'),
	(2, 2, 'BrigitteLacasse', PASSWORD('joos8hooPh'), '0'),
	(3, 3, 'GauthierJodoin', PASSWORD('Lohcahy5oh'), '0'),
	(4, 4, 'AloinGamelin', PASSWORD('jieb4Ohjah'), '0'),
	(5, 5, 'RiveEtoile', PASSWORD('eiLah3ur7cee'), '0'),
	(6, 6, 'TaborLagueux', PASSWORD('Laishi9ahn'), '0'),
	(7, 7, 'HenryVaillancour', PASSWORD('eeTuf4oih3'), '0'),
	(8, 8, 'VachelGoudreau', PASSWORD('aeNgeeT8'), '0'),
	(9, 9, 'LucePaimboeuf', PASSWORD('ahTooc8ph'), '0'),
	(10, 10, 'ArienneLeroy', PASSWORD('phohziP7'), '0'),
	(11, 11, 'SebastienParrot', PASSWORD('moogie1Loej'), '0'),
	(12, 12, 'AnnetteFouquet', PASSWORD('shuu1Xei'), '0'),
	(13, 13, 'MartinLaforge', PASSWORD('AChie8aes'), '0'),
	(14, 14, 'BurkettLefebvre', PASSWORD('eif3waiH'), '0'),
	(15, 15, 'SydneyBouchard', PASSWORD('sieM4oobai'), '0'),
	(16, 16, 'ElisabethChatigny', PASSWORD('InohnoaCoX3'), '0'),
	(17, 17, 'LorraineGagne', PASSWORD('hov8iePovae'), '0'),
	(18, 18, 'NouelChretien', PASSWORD('iez1oN6Y'), '0'),
	(19, 19, 'CheneyBrodeur', PASSWORD('fai5uaTai'), '0'),
	(20, 20, 'LinetteLambert', PASSWORD('Noaw0phah'), '0'),
	(21, 21, 'HenrietteSalois', PASSWORD('ohd3eeZe'), '0'),
	(22, 22, 'MontagueTruchon', PASSWORD('uD6oawieS'), '0'),
	(23, 23, 'FerragusChandonnet', PASSWORD('maok0ooT3ai'), '0'),
	(24, 24, 'FortunVernadeau', PASSWORD('eixoy8Mu'), '0'),
	(25, 25, 'CendrillonNoel', PASSWORD('reeWeaki2j'), '0'),
	(26, 26, 'IsaacLaforest', PASSWORD('kei8ohDoogh'), '0'),
	(27, 27, 'ArchardLoiselle', PASSWORD('Wei2Oozu6quoo'), '0'),
	(28, 28, 'HarrietteDoiron', PASSWORD('Alinix7ai'), '0'),
	(29, 29, 'TalonDeschenes', PASSWORD('Oz9eiphah'), '0'),
	(30, 30, 'AlbertineJobin', PASSWORD('aeHee3zu7m'), '0'),
	(31, NULL, 'admin', PASSWORD('admin'), '1');
	
	CREATE ALGORITHM=MERGE DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `overall_product_view` AS select `c`.`id` AS `c_id`,`c`.`name` AS `c_name`,`c`.`description` AS `c_description`,`p`.`id` AS `p_id`,`p`.`name` AS `p_name`,`p`.`unit_price` AS `p_unit_price`,`i`.`id` AS `i_id`,`i`.`name` AS `i_name`,`i`.`price` AS `i_price`, `i`.`quantity` AS `i_quantity` from ((`category` `c` join `product` `p` on((`c`.`id` = `p`.`category_id`))) join `item` `i` on((`p`.`id` = `i`.`product_id`)));

