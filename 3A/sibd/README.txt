PGMSHOP - A gamer e-commerce site
by Ionut Iortoman, Rémy Léone, Marc van der Wal

Cette application web est implémentée en utilisant une application J2EE pour
le font end et une application .net pour le back office.

BASE DE DONNÉES
===============

Il est nécessaire d'installer mysql ainsi que les bibliothèques JDBC nécessaires au fonctionnement de
notre application.

  mysql -u root -p < createUser.sql
  mysql -u root -p < scriptBDD.sql

FRONT OFFICE
============

L'installation se fait en plusieurs étapes :




BACK OFFICE
===========

Installation du back office (Partie .net)
-----------------------------------------

Déploiement: 
le paquet de déploiement (.zip) se trouve à BackOfficeSIABD\obj\Release\Package\pgmshopBackOffice.zip.
La connextion à la base de données est configurée à l'aide d'une chaine de connexion qui se trouve dans
le fichier app.config du projet DAL.
Le projet web est publié dans le dossier DeployDOTNET de la racine du dépot SVN.

Connexion à la partie back office
---------------------------------

- Lors du déploiement initial le mot de passe de connexion à l'interface back office est admin/admin
- Toute personne ayant dans la base de donnée son champ is_admin à 1
peut se connecter à la base de données


Remarque : Toutes les requêtes sont paramétrées

Description de l'application back office
------------------------------------------
Architecture par couches:
	Un projet Web (pgmshopBackOffice;

	3 bibliothèques de classes:
		DAL (Accès à la base de données)
		BLL (Classes métier)
		Entity (Les objets representant les données)
==		
L'accès à la base de données est réalisée à l'aide d'un dataset, afin de centraliser toutes les requêtes.

La gestion des produits est séparée en 3: Objet->Produit->Catégorie
Un CRUD a été réalisé pour chaque entité.
Une catégorie ne peut pas être supprimée si elle contient encore des Produits
Un produit ne peut pas être supprimé s'il contient encore des Objets
Un objet ne peut pas être supprime s'il a une quantité != 0

Une catégorie, un produit ou un objet ne peuvent pas éxister en double.

Des amélioration futures peuvent être envisagées
(par exemple, des statistiques sur les revenus de chaque commande,
la gestion des utilisateurs et la reinitialisation des mots de passe,
la gestion des promotions (qui était en bonus),
le suivi des colis, etc...)
