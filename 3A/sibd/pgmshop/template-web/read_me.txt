By downloading and/or using one of Icetemplates.com free website templates you
are legally bound by the following:

Users must at all times have Icetemplates.com text link on the front page of
their web site provided by Icetemplates.com. Please visit the Link Us 
page http://icetemplates.com/link-us.php. By violating linking rules you 
fully understand you are committing Copyright infringement.


Our website templates may be used for your own and/or your clients' websites, but
you may not sell/offer for free our templates in any sort of collection, such as
distributing to a third party via CD, diskette, or letting others to download
off your websites etc.


The templates are offered "as is" without warranty of any kind, either expressed or
implied. Icetemplates.com will not be liable for any damage or loss of data whatsoever
due to downloading or using a template. In no event shall Icetemplates.com be liable for
any damages including, but not limited to, direct, indirect, special, incidental or
consequential damages or other losses arising out of the use of or inability to use the
templates and/or information from Icetemplates.com.

Icetemplates.com team reserves the right to change or modify these terms with no prior notice.
