package exception;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ValidationExceptionTest {

    @BeforeClass
    public void setUp() {
        // code that will be invoked before this test starts
    }

    @Test
    public void aTest() {
        System.out.println("Test de Validation Exception");
    }

    @AfterClass
    public void cleanUp() {
        // code that will be invoked after this test ends
    }
}
