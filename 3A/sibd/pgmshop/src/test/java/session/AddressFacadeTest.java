package session;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AddressFacadeTest {

    @BeforeClass
    public void setUp() {
        // code that will be invoked before this test starts
    }

    @Test
    public void aTest() {
        System.out.println("Test de Address Facade");
    }

    @AfterClass
    public void cleanUp() {
        // code that will be invoked after this test ends
    }
}
