package fr.pgmshop.jsf;

import fr.pgmshop.entity.Address;
import fr.pgmshop.entity.catalog.Item;
import fr.pgmshop.entity.customer.Customer;
import fr.pgmshop.entity.order.CreditCard;
import fr.pgmshop.entity.order.Order;
import fr.pgmshop.stateful.CartItem;
import fr.pgmshop.stateful.ShoppingCartLocal;
import fr.pgmshop.stateless.catalog.CatalogLocal;
import fr.pgmshop.stateless.order.OrderLocal;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.EJB;

public class ShoppingCartController extends Controller {

    @EJB
    private ShoppingCartLocal shoppingCartBean;
    @EJB
    private CatalogLocal catalogBean;
    @EJB
    private OrderLocal orderBean;
    private final String cname = this.getClass().getName();
    private CreditCard creditCard = new CreditCard();
    private Customer customer;
    private Address deliveryAddress;
    private Order order;

    public String addItemToCart() {
        final String mname = "addItemToCart";
        logger.entering(cname, mname);

        String navigateTo = null;

        try {
            Item item = catalogBean.findItem(getParamId("itemId"));
            shoppingCartBean.addItem(item);
            navigateTo = "item.added";
        } catch (Exception e) {
            addMessage(cname, mname, e);
        }

        logger.exiting(cname, mname, navigateTo);
        return navigateTo;
    }

    public String removeItemFromCart() {
        final String mname = "removeItemFromCart";
        logger.entering(cname, mname);

        String navigateTo = null;

        try {
            Item item = catalogBean.findItem(getParamId("itemId"));
            shoppingCartBean.removeItem(item);
        } catch (Exception e) {
            addMessage(cname, mname, e);
        }

        logger.exiting(cname, mname, navigateTo);
        return navigateTo;
    }

    public String updateQuantity() {
        final String mname = "updateQuantity";
        logger.entering(cname, mname);

        String navigateTo = null;

        logger.exiting(cname, mname, navigateTo);
        return navigateTo;
    }

    public String checkout() {
        final String mname = "checkout";
        logger.entering(cname, mname);

        String navigateTo = "cart.checked.out";

        logger.exiting(cname, mname, navigateTo);
        return navigateTo;
    }

    public String confirmOrder() {
        final String mname = "confirmOrder";
        logger.entering(cname, mname);

        String navigateTo = null;

        try {
		/* On vérifie d'abord si tous les articles de la commande 
		 * ne sont pas en rupture de stock
		 */
	    for (CartItem ci : shoppingCartBean.getCartItems())
	    {
		    if (ci.getItem().getQty() < ci.getQuantity())
		    {
			    throw new Exception("L'article " 
				    + ci.getItem().getName() 
				    + " n'est pas suffisamment approvisionné."
				    + " "
				    + "Vous en avez pris "
				    + ci.getQuantity()
				    + " alors que nous en avons "
				    + ci.getItem().getQty()
				    + " en stock. "
				    + "Supprimez quelques exemplaires de votre "
				    + "panier.");
		    }
	    }
            order = orderBean.createOrder(customer, deliveryAddress, creditCard, shoppingCartBean.getCartItems());
            shoppingCartBean.empty();
            navigateTo = "order.confirmed";
        } catch (Exception e) {
            addMessage(cname, mname, e);
        }

        logger.exiting(cname, mname, navigateTo);
        return navigateTo;
    }

    public BigDecimal getTotal() {
        return shoppingCartBean.getTotal();
    }

    public List<CartItem> getCartItems() {
        return shoppingCartBean.getCartItems();
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
