package fr.pgmshop.stateless.order;

import fr.pgmshop.entity.Address;
import fr.pgmshop.entity.customer.Customer;
import fr.pgmshop.entity.order.CreditCard;
import fr.pgmshop.entity.order.Order;
import fr.pgmshop.stateful.CartItem;
import java.util.List;
import javax.ejb.Local;

@Local
public interface OrderLocal {

    Order createOrder(
            Customer customer,
            Address deliveryAddress,
            CreditCard creditCard,
            List<CartItem> cartItems);
}
