package fr.pgmshop.stateful;

import fr.pgmshop.entity.catalog.Item;
import java.math.BigDecimal;

public class CartItem {

    private Item item;
    private Integer quantity;

    public CartItem(Item item, Integer quantity) {
        this.item = item;
        this.quantity = quantity;
    }

    /*
     * L'utilisation des BigDecimal permet d'éviter les erreurs d'arrondis
     */
    public BigDecimal getSubTotal() {
        return item.getUnitCost().multiply(new BigDecimal(quantity));
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
