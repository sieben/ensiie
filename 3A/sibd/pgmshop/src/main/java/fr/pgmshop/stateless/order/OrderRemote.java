package fr.pgmshop.stateless.order;

import fr.pgmshop.entity.order.Order;
import java.util.List;
import javax.ejb.Remote;

@Remote
public interface OrderRemote {

    Order findOrder(Long orderId);

    void deleteOrder(Order order);

    List<Order> findOrders();
}
