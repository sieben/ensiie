package fr.pgmshop.stateless.user;

import fr.pgmshop.entity.customer.Customer;
import fr.pgmshop.entity.user.User;
import javax.ejb.Local;

@Local
public interface UserLocal {

    User authenticate(String login, String password);

    User createUser(User user, Customer customer);

    User findUser(Long userId);

    User updateUser(User user);
}
