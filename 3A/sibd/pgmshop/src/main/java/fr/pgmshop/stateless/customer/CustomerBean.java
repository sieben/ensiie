package fr.pgmshop.stateless.customer;

import fr.pgmshop.entity.Address;
import fr.pgmshop.entity.customer.Customer;
import fr.pgmshop.exception.ValidationException;
import fr.pgmshop.util.Constants;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

@SuppressWarnings(value = "unchecked")
@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
@Stateless(name = "CustomerSB")
public class CustomerBean implements CustomerRemote, CustomerLocal, Serializable {

    @PersistenceContext(unitName = "pgmshopPU")
    private EntityManager em;
    private final String cname = this.getClass().getName();
    private static final Logger logger = Logger.getLogger(Constants.LOGGER_STATELESS);

    @Override
    public Customer authenticate(final String login, final String password) {
        final String mname = "authenticate";
        logger.entering(cname, mname, new Object[]{login, password});

        // On s'assure de la validité des paramètres
        if (login == null || "".equals(login)) {
            throw new ValidationException("Invalid login");
        }

        Query query;
        Customer customer;

        // On recherche l'objet à partir de son login
        query = em.createQuery("SELECT c FROM Customer c WHERE c.login=:login");
        query.setParameter("login", login);
        customer = (Customer) query.getSingleResult();

        /*
         * Check if it's the right password if (customer != null) {
         * customer.matchPassword(password); }
         */

        logger.exiting(cname, mname, customer);
        return customer;
    }

    @Override
    public Customer createCustomer(final Customer customer, final Address homeAddress) {
        final String mname = "createCustomer";
        logger.entering(cname, mname, customer);

        // On s'assure de la validité des paramètres
        if (customer == null) {
            throw new ValidationException("Customer object is null");
        }

        customer.setHomeAddress(homeAddress);

        try {
            em.persist(customer);
        } catch (ConstraintViolationException e) {
            for (ConstraintViolation cv : e.getConstraintViolations()) {
                logger.log(java.util.logging.Level.WARNING, "{0}: {1}",
                        new Object[]{cv.getLeafBean(), cv.getMessage()});
            }
            throw new ValidationException("marche pas");
        }

        logger.exiting(cname, mname, customer);
        return customer;
    }

    @Override
    public Customer findCustomer(final Long customerId) {
        final String mname = "findCustomer";
        logger.entering(cname, mname, customerId);

        // On s'assure de la validité des paramètres
        if (customerId == null) {
            throw new ValidationException("Invalid id");
        }

        Customer customer;

        // On recherche l'objet à partir de son identifiant
        customer = em.find(Customer.class, customerId);

        logger.exiting(cname, mname, customer);
        return customer;
    }

    @Override
    public void deleteCustomer(final Customer customer) {
        final String mname = "deleteCustomer";
        logger.entering(cname, mname, customer);

        // On s'assure de la validité des paramètres
        if (customer == null) {
            throw new ValidationException("Customer object is null");
        }

        // On supprime l'objet de la base de données
        em.remove(em.merge(customer));

        logger.exiting(cname, mname);
    }

    @Override
    public Customer updateCustomer(final Customer customer, final Address homeAddress) {
        final String mname = "updateCustomer";
        logger.entering(cname, mname, customer);

        // On s'assure de la validité des paramètres
        if (customer == null) {
            throw new ValidationException("Customer object is null");
        }

        customer.setHomeAddress(homeAddress);

        // On modifie l'objet de la base de données
        em.merge(customer);

        logger.exiting(cname, mname, customer);
        return customer;
    }

    @Override
    public List<Customer> findCustomers() {
        final String mname = "findCustomers";
        logger.entering(cname, mname);

        Query query;
        List<Customer> customers;

        // On modifie l'objet de la base de données
        query = em.createQuery("SELECT c FROM Customer c");
        customers = query.getResultList();

        logger.exiting(cname, mname, customers.size());
        return customers;
    }
}
