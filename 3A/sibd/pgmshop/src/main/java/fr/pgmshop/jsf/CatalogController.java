package fr.pgmshop.jsf;

import fr.pgmshop.entity.catalog.Category;
import fr.pgmshop.entity.catalog.Item;
import fr.pgmshop.entity.catalog.Product;
import fr.pgmshop.stateless.catalog.CatalogLocal;
import java.util.List;
import javax.ejb.EJB;

public class CatalogController extends Controller {

    @EJB
    private CatalogLocal catalogBean;
    private final String cname = this.getClass().getName();
    private String keyword;
    private Category category;
    private Product product;
    private Item item;
    private List<Product> products;
    private List<Item> items;

    public String doFindProducts() {
        final String mname = "doFindProducts";
        logger.entering(cname, mname);
        String navigateTo = null;

        try {
            category = catalogBean.findCategory(getParamId("categoryId"));
            products = category.getProducts();
            navigateTo = "products.displayed";
        } catch (Exception e) {
            addMessage(cname, mname, e);
        }

        logger.exiting(cname, mname, navigateTo);
        return navigateTo;
    }

    public String doFindItems() {
        final String mname = "doFindItems";
        logger.entering(cname, mname);
        String navigateTo = null;

        try {
            product = catalogBean.findProduct(getParamId("productId"));
            items = product.getItems();
            navigateTo = "items.displayed";
        } catch (Exception e) {
            addMessage(cname, mname, e);
        }

        logger.exiting(cname, mname, navigateTo);
        return navigateTo;
    }

    public String doFindItem() {
        final String mname = "doFindItem";
        logger.entering(cname, mname);
        String navigateTo = null;

        try {
            item = catalogBean.findItem(getParamId("itemId"));
            navigateTo = "item.displayed";
        } catch (Exception e) {
            addMessage(cname, mname, e);
        }

        logger.exiting(cname, mname, navigateTo);
        return navigateTo;
    }

    public String doSearch(int start, int chunksize) {
        final String mname = "doSearch";
        logger.entering(cname, mname);
        String navigateTo = null;

        try {
            items = catalogBean.searchItems(keyword, start, chunksize);
            navigateTo = "items.found";
        } catch (Exception e) {
            addMessage(cname, mname, e);
        }

        logger.exiting(cname, mname, navigateTo);
        return navigateTo;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public List<Product> getProducts() {
        return products;
    }

    public List<Item> getItems() {
        return items;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
