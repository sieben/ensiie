package fr.pgmshop.util;

public interface Constants {

    public static final String PERSISTENCE_UNIT_NAME = "pgmshopPU";
    public static final String COMPANY_NAME = "pgmshop";
    public static final String COMPANY_STREET = "42, rue du headshot";
    public static final String COMPANY_CITY = "Evry";
    public static final String COMPANY_ZIPCODE = "91000";
    public static final String COMPANY_COUNTRY = "France";
    public static final String LOGGER_STATELESS = "stateless";
    public static final String LOGGER_STATEFUL = "stateful";
    public static final String LOGGER_ENTITY = "entity";
    public static final String LOGGER_SERVLET = "servlet";
    public static final String LOGGER_JSF = "jsf";
    public static final String LOGGER_CLIENT = "client";
    public static final String LOGGER_MDB = "mdb";
    public static final String LOGGER_UTIL = "util";
}
