package fr.pgmshop.stateless.customer;

import fr.pgmshop.entity.Address;
import fr.pgmshop.entity.customer.Customer;
import javax.ejb.Local;

@Local
public interface CustomerLocal {

    Customer authenticate(String login, String password);

    Customer createCustomer(Customer customer, Address homeAddress);

    Customer findCustomer(Long customerId);

    Customer updateCustomer(Customer customer, Address homeAddress);
}
