package fr.pgmshop.stateless.order;

import fr.pgmshop.entity.Address;
import fr.pgmshop.entity.customer.Customer;
import fr.pgmshop.entity.order.CreditCard;
import fr.pgmshop.entity.order.Order;
import fr.pgmshop.entity.order.OrderLine;
import fr.pgmshop.exception.ValidationException;
import fr.pgmshop.stateful.CartItem;
import fr.pgmshop.util.Constants;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.Connection;
import javax.jms.Topic;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@SuppressWarnings(value = "unchecked")
@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
@Stateless(name = "OrderSB")
public class OrderBean implements OrderRemote, OrderLocal {

    @PersistenceContext(unitName = "pgmshopPU")
    private EntityManager em;
    private Topic destinationOrder;
    private Connection connection;
    private final String cname = this.getClass().getName();
    private static final Logger logger = Logger.getLogger(Constants.LOGGER_STATELESS);

    @Override
    public Order createOrder(
            final Customer customer,
            final Address deliveryAddress,
            final CreditCard creditCard,
            final List<CartItem> cartItems) {
        final String mname = "createOrder";
        logger.entering(cname, mname, cartItems.size());

        // On s'assure de la validité des paramètres
        if (cartItems == null || cartItems.isEmpty()) {
            throw new ValidationException("Shopping cart is empty");
        }

        // Creation du bon de commande
        Order order = new Order(customer, em.merge(deliveryAddress), creditCard);

        // A partir du panier electronique on crée les lignes du bon de commande
        List<OrderLine> orderLines = new ArrayList<OrderLine>();

        for (CartItem cartItem : cartItems) {
            orderLines.add(new OrderLine(cartItem.getQuantity(), cartItem.getItem()));
        }
        order.setOrderLines(orderLines);

        // L'objet est persisté en base de données
        em.persist(order);

        logger.exiting(cname, mname, order);
        return order;
    }

    @Override
    public Order findOrder(final Long orderId) {
        final String mname = "findOrder";
        logger.entering(cname, mname, orderId);

        // On s'assure de la validité des paramètres
        if (orderId == null) {
            throw new ValidationException("Invalid id");
        }

        Order order;

        // On recherche l'objet à partir de son identifiant
        order = em.find(Order.class, orderId);

        logger.exiting(cname, mname, order);
        return order;
    }

    @Override
    public void deleteOrder(final Order order) {
        final String mname = "deleteOrder";
        logger.entering(cname, mname, order);

        // On s'assure de la validité des paramètres
        if (order == null) {
            throw new ValidationException("Order object is null");
        }

        // On supprime l'objet de la base de données
        em.remove(em.merge(order));

        logger.exiting(cname, mname);
    }

    @Override
    public List<Order> findOrders() {
        final String mname = "findOrders";
        logger.entering(cname, mname);

        Query query;
        List<Order> orders;

        // On modifie l'objet de la base de données
        query = em.createQuery("SELECT o FROM Order o");
        orders = query.getResultList();

        logger.exiting(cname, mname, orders.size());
        return orders;
    }
}
