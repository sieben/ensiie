package fr.pgmshop.entity.catalog;

import fr.pgmshop.exception.ValidationException;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "category")
public class Category implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(nullable = false, length = 30)
    private String name;
    @Column(nullable = false)
    private String description;
    @OneToMany(mappedBy = "category", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @OrderBy("name ASC")
    private List<Product> products;

    public Category() {
    }

    public Category(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @PrePersist
    @PreUpdate
    private void validateData() {
        if (name == null || "".equals(name)) {
            throw new ValidationException("Invalid name");
        }
        if (description == null || "".equals(description)) {
            throw new ValidationException("Invalid description");
        }
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Category category = (Category) o;

        if (!description.equals(category.description)) {
            return false;
        }
        if (!id.equals(category.id)) {
            return false;
        }
        if (!name.equals(category.name)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = id.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + description.hashCode();
        return result;
    }

    /*
     * Implémenation de la serialisation
     */
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Category");
        sb.append("{id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", products=").append(products == null ? 0 : products.size());
        sb.append('}');
        return sb.toString();
    }

    public Collection<Product> getProductCollection() {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
