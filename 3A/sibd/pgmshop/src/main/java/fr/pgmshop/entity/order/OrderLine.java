package fr.pgmshop.entity.order;

import fr.pgmshop.entity.catalog.Item;
import fr.pgmshop.exception.ValidationException;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Entity
@Table(name = "order_line")
public class OrderLine implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private Integer quantity;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "item_id", nullable = false)
    private Item item;

    public OrderLine() {
    }

    public OrderLine(Integer quantity, Item item) {
        this.quantity = quantity;
        this.item = item;
    }

    @PrePersist
    @PreUpdate
    private void validateData() {
        if (quantity == null || quantity < 0) {
            throw new ValidationException("Invalid quantity");
        }
    }

    public BigDecimal getSubTotal() {
        BigDecimal result = new BigDecimal(quantity.intValue());
        result = result.multiply(item.getUnitCost());
        return result;
    }

    public Long getId() {
        return id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrderLine orderLine = (OrderLine) o;

        if (!id.equals(orderLine.id)) {
            return false;
        }
        if (!item.equals(orderLine.item)) {
            return false;
        }
        if (!quantity.equals(orderLine.quantity)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = quantity.hashCode();
        result = 31 * result + item.hashCode();
        return result;
    }

    /* 
     * Implémenation de la serialisation
     */
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("OrderLine");
        sb.append("{id=").append(id);
        sb.append(", quantity=").append(quantity);
        sb.append(", item=").append(item);
        sb.append('}');
        return sb.toString();
    }
}
