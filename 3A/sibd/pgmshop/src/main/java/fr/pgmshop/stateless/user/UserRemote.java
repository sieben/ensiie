package fr.pgmshop.stateless.user;

import fr.pgmshop.entity.customer.Customer;
import fr.pgmshop.entity.user.User;
import java.util.List;
import javax.ejb.Remote;

@Remote
public interface UserRemote {

    User createUser(User user, Customer customer);

    User findUser(Long userId);

    void deleteUser(User user);

    User updateUser(User user);

    List<User> findUsers();
}
