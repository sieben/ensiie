package fr.pgmshop.stateless.catalog;

import fr.pgmshop.entity.catalog.Category;
import fr.pgmshop.entity.catalog.Item;
import fr.pgmshop.entity.catalog.Product;
import java.util.List;
import javax.ejb.Local;

@Local
public interface CatalogLocal {

    Category findCategory(Long categoryId);

    Product findProduct(Long productId);

    Item findItem(Long itemId);

    List<Item> searchItems(String keyword, int start, int chunkSize);
}
