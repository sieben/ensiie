package fr.pgmshop.entity.user;

import fr.pgmshop.entity.customer.Customer;
import fr.pgmshop.exception.ValidationException;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "user")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
    @NamedQuery(name = "User.findById", query = "SELECT u FROM User u WHERE u.id = :id"),
    @NamedQuery(name = "User.findByUserName", query = "SELECT u FROM User u WHERE u.userName = :userName"),
    @NamedQuery(name = "User.findByPassword", query = "SELECT u FROM User u WHERE u.password = :password"),
    @NamedQuery(name = "User.findByIsAdmin", query = "SELECT u FROM User u WHERE u.isAdmin = :isAdmin")})
public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "user_name")
    private String userName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "password")
    private String password;
    @Column(name = "is_admin")
    private String isAdmin;
    @JoinColumn(name = "customer_id", referencedColumnName = "id")
    @OneToOne
    private Customer customerId;

    public User() {
        this.isAdmin = "0";
    }

    public User(Integer id) {
        super();
        this.id = id;
    }

    public User(Integer id, String userName, String password, String isAdmin) {
        super();
        this.id = id;
        this.userName = userName;
        this.password = password;
        this.isAdmin = (isAdmin.equals("1") ? "1" : "0");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Given a password, this method then checks if it matches the user
     *
     * @param pwd
     * @throws ValidationException thrown if the password is empty or different
     * than the one store in database
     */
    public void matchPassword(String pwd) {
        if (pwd == null || "".equals(pwd)) {
            throw new ValidationException("Utilisateur ou mot de passe incorrect");
        }

        // The password entered by the customer is not the same stored in database
        if (!pwd.equals(password)) {
            throw new ValidationException("Utilisateur ou mot de passe incorrect");
        }
    }

    public String getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    public Customer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Customer customerId) {
        this.customerId = customerId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // Warning - ça va sans doute planter si les champs 
        // id ne sont pas définis 
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.pgmshop.entity.user.User[ id=" + id + " ]";
    }
}
