package fr.pgmshop.jsf;

import fr.pgmshop.util.Constants;
import static fr.pgmshop.util.ExceptionUtils.getRootCause;
import static fr.pgmshop.util.ExceptionUtils.isApplicationException;
import java.util.Map;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public abstract class Controller {

    protected static final Logger logger = Logger.getLogger(Constants.LOGGER_JSF);
    private final String cname = this.getClass().getName();

    protected void addMessage(String sourceClass, String sourceMethod, Throwable throwable) {
        Throwable cause = getRootCause(throwable);
        if (isApplicationException(cause)) {
            addWarningMessage(cause.getMessage());
        } else {
            addErrorMessage(throwable.getMessage());
            logger.throwing(sourceClass, sourceMethod, throwable);
        }
    }

    protected void addWarningMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, message, null));
    }

    protected void addErrorMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
    }

    protected String getParam(String param) {
        final String mname = "getParam";
        logger.entering(cname, mname);
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String, String> map = context.getExternalContext().getRequestParameterMap();
        String result = map.get(param);
        logger.exiting(cname, mname, result);
        return result;
    }

    protected Long getParamId(String param) {
        final String mname = "getParamId";
        logger.entering(cname, mname);
        Long result = Long.valueOf(getParam(param));
        logger.exiting(cname, mname, result);
        return result;
    }
}
