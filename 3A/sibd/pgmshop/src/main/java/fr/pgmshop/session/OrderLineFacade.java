package fr.pgmshop.session;

import fr.pgmshop.entity.order.OrderLine;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class OrderLineFacade extends AbstractFacade<OrderLine> {

    @PersistenceContext(unitName = "pgmshopPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OrderLineFacade() {
        super(OrderLine.class);
    }
}
