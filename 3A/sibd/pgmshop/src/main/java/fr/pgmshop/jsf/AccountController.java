package fr.pgmshop.jsf;

import fr.pgmshop.entity.Address;
import fr.pgmshop.entity.customer.Customer;
import fr.pgmshop.entity.user.User;
import fr.pgmshop.stateless.customer.CustomerLocal;
import fr.pgmshop.stateless.user.UserLocal;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class AccountController extends Controller {

    @EJB
    private UserLocal userBean;
    @EJB
    private CustomerLocal customerBean;
    private final String cname = this.getClass().getName();
    private String login;
    private String password;
    private String password2;
    private User user = new User();
    private Address homeAddress = new Address();
    private Customer customer = new Customer();

    public String doSignIn() {

        final String mname = "doSignIn";

        logger.entering(cname, mname);
        String navigateTo = null;

        try {
            user = userBean.authenticate(login, password);
	    customer = user.getCustomerId();
	    homeAddress = customer.getHomeAddress();
	    
            addWarningMessage("Authentification réussie !");
            navigateTo = "customer.signed.in";
        } catch (Exception e) {
            addMessage(cname, mname, e);
        }

        logger.exiting(cname, mname, navigateTo);
        return navigateTo;
    }

    public String doCreateNewAccount() {
        final String mname = "doCreateNewAccount";
        logger.entering(cname, mname);
        String navigateTo = null;

        // Id and password must be filled

        if ("".equals(user.getPassword()) || "".equals(password2)) {
            addWarningMessage("Id and passwords have to be filled");
            navigateTo = null;

        } else if (!user.getPassword().equals(password2)) {
            addWarningMessage("Both entered passwords have to be the same");
            navigateTo = null;

        } else {
            navigateTo = "create.a.new.account";
        }

        logger.exiting(cname, mname, navigateTo);
        return navigateTo;
    }

    public String doCreateUser() {
        final String mname = "doCreateUser";
        logger.entering(cname, mname);
        String navigateTo = null;

        try {
            // Creates the user
            customer = customerBean.createCustomer(customer, homeAddress);
            user = userBean.createUser(user, customer);
            navigateTo = "customer.created";
        } catch (Exception e) {
            addMessage(cname, mname, e);
        }

        logger.exiting(cname, mname, navigateTo);

        return navigateTo;

    }

    public String doUpdateAccount() {
        final String mname = "doUpdateAccount";
        logger.entering(cname, mname);
        String navigateTo = null;

        try {
            // Updates the user
            user = userBean.updateUser(user);
            navigateTo = "account.updated";
        } catch (Exception e) {
            addMessage(cname, mname, e);
        }

        logger.exiting(cname, mname, navigateTo);
        return navigateTo;
    }

    public String doSignOff() {
        final String mname = "doSignOff";
        logger.entering(cname, mname);
        String navigateTo = "main";
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) fc.getExternalContext().getSession(false);
        session.invalidate();
        logger.exiting(cname, mname, navigateTo);
        return navigateTo;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Address getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(Address homeAddress) {
        this.homeAddress = homeAddress;
    }
}
