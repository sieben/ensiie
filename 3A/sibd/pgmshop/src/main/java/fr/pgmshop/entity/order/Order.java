package fr.pgmshop.entity.order;

import fr.pgmshop.entity.Address;
import fr.pgmshop.entity.catalog.Category;
import fr.pgmshop.entity.customer.Customer;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "`order`")
@XmlRootElement
public class Order implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "`date`", updatable = false)
    @Temporal(TemporalType.DATE)
    private Date orderDate;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "order_order_line",
    joinColumns = {
        @JoinColumn(name = "order_id")},
    inverseJoinColumns = {
        @JoinColumn(name = "orderline_id")})
    private List<OrderLine> orderLines;
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", nullable = false)
    private Address deliveryAddress;
    @Embedded
    private CreditCard creditCard = new CreditCard();

    public Order() {
    }

    public Order(Customer customer, Address deliveryAddress, CreditCard creditCard) {
        this.customer = customer;
        this.deliveryAddress = deliveryAddress;
        this.creditCard = creditCard;
    }

    @PrePersist
    private void setDefaultData() {
        orderDate = new Date();
    }

    public BigDecimal getTotal() {
        if (orderLines == null || orderLines.isEmpty()) {
            return new BigDecimal(0);
        }

        BigDecimal total = new BigDecimal(0);

        // sum up the quantities
        for (OrderLine orderLine : orderLines) {
            total.add(orderLine.getSubTotal());
        }

        return total;
    }

    public Long getId() {
        return id;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<OrderLine> getOrderLines() {
        return orderLines;
    }

    public void setOrderLines(List<OrderLine> orderLines) {
        this.orderLines = orderLines;
    }

    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    public String getCreditCardNumber() {
        return creditCard.getCreditCardNumber();
    }

    public void setCreditCardNumber(String creditCardNumber) {
        creditCard.setCreditCardNumber(creditCardNumber);
    }

    public String getCreditCardType() {
        return creditCard.getCreditCardType();
    }

    public void setCreditCardType(String creditCardType) {
        creditCard.setCreditCardType(creditCardType);
    }

    public String getCreditCardExpiryDate() {
        return creditCard.getCreditCardExpDate();
    }

    public void setCreditCardExpiryDate(String creditCardExpiryDate) {
        creditCard.setCreditCardExpDate(creditCardExpiryDate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Order order = (Order) o;

        if (!id.equals(order.id)) {
            return false;
        }
        if (!orderDate.equals(order.orderDate)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = orderDate.hashCode();
        return result;
    }

    /*
     * Implémenation de la serialisation
     */
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Order");
        sb.append("{id=").append(id);
        sb.append(", orderDate=").append(orderDate);
        sb.append(", customer=").append(customer);
        sb.append(", orderLines=").append(orderLines == null ? 0 : orderLines.size());
        sb.append(", deliveryAddress=").append(deliveryAddress);
        sb.append(", creditCard=").append(creditCard);
        sb.append('}');
        return sb.toString();
    }

    public static Set<Category> getDistinctCategories(Order order) {
        Set<Category> catSet = new HashSet<Category>();
        for (OrderLine line : order.getOrderLines()) {
            catSet.add(line.getItem().getProduct().getCategory());
        }
        return catSet;
    }
}
