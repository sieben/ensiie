package fr.pgmshop.entity.catalog;

import fr.pgmshop.exception.ValidationException;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Entity
@Table(name = "item")
public class Item implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(nullable = false, length = 30)
    private String name;
    @Column(name = "quantity", nullable = false)
    private Integer qty;
    @Column(name = "price", nullable = false)
    private BigDecimal unitCost;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "product_id", nullable = false)
    private Product product;

    public Item() {
    }

    public Item(String name, BigDecimal unitCost) {
        this.name = name;
        this.unitCost = unitCost;
    }

    @PrePersist
    @PreUpdate
    private void validateData() {
        if (name == null || "".equals(name)) {
            throw new ValidationException("Invalid name");
        }
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public BigDecimal getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(BigDecimal unitCost) {
        this.unitCost = unitCost;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Item item = (Item) o;

        if (!id.equals(item.id)) {
            return false;
        }
        if (!name.equals(item.name)) {
            return false;
        }
        if (!unitCost.equals(item.unitCost)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = id.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + unitCost.hashCode();
        return result;
    }

    /*
     * Implémenation de la serialisation
     */
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Item");
        sb.append("{id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", unitCost=").append(unitCost);
        sb.append(", qty=").append(qty);
        sb.append(", product=").append(product);
        sb.append('}');
        return sb.toString();
    }

}
