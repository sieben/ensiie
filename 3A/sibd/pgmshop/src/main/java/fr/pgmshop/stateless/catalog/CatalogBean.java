package fr.pgmshop.stateless.catalog;

import fr.pgmshop.entity.catalog.Category;
import fr.pgmshop.entity.catalog.Item;
import fr.pgmshop.entity.catalog.Product;
import fr.pgmshop.exception.ValidationException;
import fr.pgmshop.util.Constants;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@SuppressWarnings(value = "unchecked")
@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
@Stateless(name = "CatalogSB")
public class CatalogBean implements CatalogRemote, CatalogLocal, Serializable {

    @PersistenceContext(unitName = "pgmshopPU")
    private EntityManager em;
    private final String cname = this.getClass().getName();
    private static final Logger logger = Logger.getLogger(Constants.LOGGER_STATELESS);

    @Override
    public Category createCategory(final Category category) {
        final String mname = "getNewCategory";
        logger.entering(cname, mname, category);

        // On s'assure de la validité des paramètres
        if (category == null) {
            throw new ValidationException("Category object is null");
        }

        // L'objet est persisté en base de données
        em.persist(category);

        logger.exiting(cname, mname, category);
        return category;
    }

    @Override
    public Category findCategory(final Long categoryId) {
        final String mname = "findCategory";
        logger.entering(cname, mname, categoryId);

        // On s'assure de la validité des paramètres
        if (categoryId == null) {
            throw new ValidationException("Invalid id");
        }

        Category category;

        // On recherche l'objet à partir de son identifiant
        category = em.find(Category.class, categoryId);

        logger.exiting(cname, mname, category);
        return category;
    }

    @Override
    public void deleteCategory(final Category category) {
        final String mname = "deleteCategory";
        logger.entering(cname, mname, category);

        // On s'assure de la validité des paramètres
        if (category == null) {
            throw new ValidationException("Category object is null");
        }

        // On supprime l'objet de la base de données
        em.remove(em.merge(category));

        logger.exiting(cname, mname);
    }

    @Override
    public Category updateCategory(final Category category) {
        final String mname = "updateCategory";
        logger.entering(cname, mname, category);

        // On s'assure de la validité des paramètres
        if (category == null) {
            throw new ValidationException("Category object is null");
        }

        // On modifie l'objet de la base de données
        em.merge(category);

        logger.exiting(cname, mname, category);
        return category;
    }

    @Override
    public List<Category> findCategories() {
        final String mname = "findCategories";
        logger.entering(cname, mname);

        Query query;
        List<Category> categories;

        // On modifie l'objet de la base de données
        query = em.createQuery("SELECT c FROM Category c ORDER BY c.name");
        categories = query.getResultList();

        logger.exiting(cname, mname, categories.size());
        return categories;
    }

    @Override
    public Product createProduct(final Product product, final Category category) {
        final String mname = "createProduct";
        logger.entering(cname, mname, product);

        // On s'assure de la validité des paramètres
        if (product == null) {
            throw new ValidationException("Product object is null");
        }
        if (category == null) {
            throw new ValidationException("Product must be attached to a category");
        }

        product.setCategory(category);

        // L'objet est persisté en base de données
        em.persist(product);

        logger.exiting(cname, mname, product);
        return product;
    }

    @Override
    public Product findProduct(final Long productId) {
        final String mname = "findProduct";
        logger.entering(cname, mname, productId);

        // On s'assure de la validité des paramètres
        if (productId == null) {
            throw new ValidationException("Invalid id");
        }

        Product product;

        // On recherche l'objet à partir de son identifiant
        product = em.find(Product.class, productId);

        logger.exiting(cname, mname, product);
        return product;
    }

    @Override
    public void deleteProduct(final Product product) {
        final String mname = "deleteProduct";
        logger.entering(cname, mname, product);

        // On s'assure de la validité des paramètres
        if (product == null) {
            throw new ValidationException("Product object is null");
        }

        // On supprime l'objet de la base de données
        em.remove(em.merge(product));

        logger.exiting(cname, mname);
    }

    @Override
    public Product updateProduct(final Product product, final Category category) {
        final String mname = "updateProduct";
        logger.entering(cname, mname, product);

        // On s'assure de la validité des paramètres
        if (product == null) {
            throw new ValidationException("Product object is null");
        }
        if (category == null) {
            throw new ValidationException("Product must be attached to a category");
        }

        product.setCategory(category);

        // On modifie l'objet de la base de données
        em.merge(product);

        logger.exiting(cname, mname, product);
        return product;
    }

    @Override
    public List<Product> findProducts() {
        final String mname = "findCategories";
        logger.entering(cname, mname);

        Query query;
        List<Product> products;

        // On modifie l'objet de la base de données
        query = em.createQuery("SELECT p FROM Product p ORDER BY p.name");
        products = query.getResultList();

        logger.exiting(cname, mname, products.size());
        return products;
    }

    @Override
    public Item createItem(final Item item, final Product product) {
        final String mname = "createItem";
        logger.entering(cname, mname, item);

        // On s'assure de la validité des paramètres
        if (item == null) {
            throw new ValidationException("Item object is null");
        }
        if (product == null) {
            throw new ValidationException("Item must be attached to a product");
        }

        item.setProduct(product);

        // L'objet est persisté en base de données
        em.persist(item);

        logger.exiting(cname, mname, item);
        return item;
    }

    @Override
    public Item findItem(final Long itemId) {
        final String mname = "findItem";
        logger.entering(cname, mname, itemId);

        // On s'assure de la validité des paramètres
        if (itemId == null) {
            throw new ValidationException("Invalid id");
        }

        Item item;

        // On recherche l'objet à partir de son identifiant
        item = em.find(Item.class, itemId);


        logger.exiting(cname, mname, item);
        return item;
    }

    public int getItemCount() {
        Query q = em.createQuery("select count(o) from Item as o");
        int count = ((Long) q.getSingleResult()).intValue();
        return count;
    }

    @Override
    public void deleteItem(final Item item) {
        final String mname = "deleteItem";
        logger.entering(cname, mname, item);

        // On s'assure de la validité des paramètres
        if (item == null) {
            throw new ValidationException("Item object is null");
        }

        // On supprime l'objet de la base de données
        em.remove(em.merge(item));

        logger.exiting(cname, mname);
    }

    @Override
    public Item updateItem(final Item item, final Product product) {
        final String mname = "updateItem";
        logger.entering(cname, mname, item);

        // On s'assure de la validité des paramètres
        if (item == null) {
            throw new ValidationException("Item object is null");
        }
        if (product == null) {
            throw new ValidationException("Item must be attached to a product");
        }

        item.setProduct(product);

        // On modifie l'objet de la base de données
        em.merge(item);

        logger.exiting(cname, mname, item);
        return item;
    }

    @Override
    public List<Item> findItems(int firstItem, int batchSize) {
        final String mname = "findCategories";
        logger.entering(cname, mname);

        Query query;
        List<Item> items;

        // On modifie l'objet de la base de données
        query = em.createQuery("SELECT i FROM Item i ORDER BY i.name");
        query.setMaxResults(batchSize);
        query.setFirstResult(firstItem);
        items = query.getResultList();

        logger.exiting(cname, mname, items.size());
        return items;
    }

    @Override
    public List<Item> searchItems(final String keyword, int start, int chunkSize) {
        final String mname = "searchItems";
        logger.entering(cname, mname, keyword);

        Query query;
        List<Item> items;

        // On modifie l'objet de la base de données
        query = em.createQuery("SELECT i FROM Item i WHERE UPPER(i.name) LIKE :keyword OR UPPER(i.product.name) LIKE :keyword ORDER BY i.product.category.name, i.product.name");
        query.setParameter("keyword", "%" + keyword.toUpperCase() + "%");
        query = query.setFirstResult(start);
        query = query.setMaxResults(chunkSize);
        items = query.getResultList();

        logger.exiting(cname, mname, items.size());
        return items;
    }
}
