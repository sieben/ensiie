package fr.pgmshop.stateless.catalog;

import fr.pgmshop.entity.catalog.Category;
import fr.pgmshop.entity.catalog.Item;
import fr.pgmshop.entity.catalog.Product;
import java.util.List;
import javax.ejb.Remote;

@Remote
public interface CatalogRemote {

    Category createCategory(Category category);

    Category findCategory(Long categoryId);

    void deleteCategory(Category category);

    Category updateCategory(Category category);

    List<Category> findCategories();

    Product createProduct(Product product, Category category);

    Product findProduct(Long productId);

    void deleteProduct(Product product);

    Product updateProduct(Product product, Category category);

    List<Product> findProducts();

    Item createItem(Item item, Product product);

    Item findItem(Long itemId);

    void deleteItem(Item item);

    Item updateItem(Item item, Product product);

    List<Item> findItems(int firstItem, int batchSize);
}
