package fr.pgmshop.entity.customer;

import fr.pgmshop.entity.Address;
import fr.pgmshop.exception.ValidationException;
import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "customer")
public class Customer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "first_name", nullable = false, length = 30)
    private String firstname;
    @Column(name = "last_name", nullable = false, length = 30)
    private String lastname;
    @Column(name = "phone", nullable = true, length = 50)
    private String telephone;
    @Column(name = "mail", nullable = false, length = 50)
    private String email;
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", nullable = true)
    private Address homeAddress;

    public Customer() {
    }

    public Customer(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    @PrePersist
    @PreUpdate
    private void validateData() {
        if (firstname == null || "".equals(firstname)) {
            throw new ValidationException("Invalid first name");
        }
        if (lastname == null || "".equals(lastname)) {
            throw new ValidationException("Invalid last name");
        }
    }

    public Integer getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(Address homeAddress) {
        this.homeAddress = homeAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Customer customer = (Customer) o;

        if (email != null ? !email.equals(customer.email) : customer.email != null) {
            return false;
        }
        if (!firstname.equals(customer.firstname)) {
            return false;
        }
        if (!id.equals(customer.id)) {
            return false;
        }
        if (!lastname.equals(customer.lastname)) {
            return false;
        }
        if (telephone != null ? !telephone.equals(customer.telephone) : customer.telephone != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        /*
         * result = id.hashCode();
         */
        result = 0;
        result = 31 * result + firstname.hashCode();
        result = 31 * result + lastname.hashCode();
        result = 31 * result + (telephone != null ? telephone.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        return result;
    }

    /*
     * Implémenation de la serialisation
     */
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Customer");
        sb.append("{id=").append(id);
        sb.append(", firstname='").append(firstname).append('\'');
        sb.append(", lastname='").append(lastname).append('\'');
        sb.append(", telephone='").append(telephone).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", homeAddress=").append(homeAddress);
        sb.append('}');
        return sb.toString();
    }

    public Customer(Integer id) {
        this.id = id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
