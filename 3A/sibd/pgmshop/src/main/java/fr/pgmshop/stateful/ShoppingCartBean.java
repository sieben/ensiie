package fr.pgmshop.stateful;

import fr.pgmshop.entity.catalog.Item;
import fr.pgmshop.util.Constants;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Stateful;

@Stateful(name = "ShoppingCartSB")
public class ShoppingCartBean implements ShoppingCartLocal, Serializable {

    private List<CartItem> cartItems;
    private final String cname = this.getClass().getName();
    private static final Logger logger = Logger.getLogger(Constants.LOGGER_STATEFUL);

    @PostConstruct
    public void initialize() {
        String mname = "initialize";
        logger.entering(cname, mname);
        cartItems = new ArrayList<CartItem>();
    }

    @PreDestroy
    public void clear() {
        String mname = "clear";
        logger.entering(cname, mname);
        cartItems = null;
    }

    @Override
    public void addItem(Item item) {

        String mname = "addItem";
        logger.entering(cname, mname, item);
        boolean itemFound = false;

        for (CartItem cartItem : cartItems) {

            // Si l'article existe déjà dans le panier, on modifie uniquement sa quantité

            if (cartItem.getItem().equals(item)) {
                cartItem.setQuantity(cartItem.getQuantity() + 1);
                itemFound = true;
            }
        }

        if (!itemFound) // Sinon on le rajoute dans le panier
        {
            cartItems.add(new CartItem(item, 1));
        }
        logger.exiting(cname, mname, cartItems.size());
    }

    @Override
    public void removeItem(Item item) {

        String mname = "removeItem";
        logger.entering(cname, mname, item);

        for (CartItem cartItem : cartItems) {
            if (cartItem.getItem().equals(item)) {
                cartItems.remove(cartItem);
                return;
            }
        }

        logger.exiting(cname, mname, cartItems.size());
    }

    @Override
    public BigDecimal getTotal() {

        String mname = "getTotal";
        logger.entering(cname, mname);

        if (cartItems == null || cartItems.isEmpty()) {
            return new BigDecimal(0);
        }

        BigDecimal total = new BigDecimal(0);

        // On additionne les quantités

        for (CartItem cartItem : cartItems) {
            total.add(cartItem.getSubTotal());
        }

        logger.exiting(cname, mname, total);
        return total;
    }

    @Override
    public void empty() {

        String mname = "empty";
        logger.entering(cname, mname);
        cartItems.clear();
    }

    @Override
    public List<CartItem> getCartItems() {
        return cartItems;
    }
}
