package fr.pgmshop.util;

import fr.pgmshop.exception.ValidationException;
import javax.ejb.EJBException;

public class ExceptionUtils {

    public static Throwable getRootCause(Throwable throwable) {

        Throwable cause;

        if (throwable instanceof EJBException) {
            cause = ((EJBException) throwable).getCausedByException();
        } else {
            cause = throwable.getCause();
        }

        if (cause != null) {
            throwable = cause;
            while ((throwable = throwable.getCause()) != null) {
                cause = throwable;
            }
        }
        return cause;
    }

    public static boolean isApplicationException(Throwable throwable) {
        return (throwable instanceof ValidationException);
    }
}
