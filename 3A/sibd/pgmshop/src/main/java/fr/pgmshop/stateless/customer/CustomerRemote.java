package fr.pgmshop.stateless.customer;

import fr.pgmshop.entity.Address;
import fr.pgmshop.entity.customer.Customer;
import java.util.List;
import javax.ejb.Remote;

@Remote
public interface CustomerRemote {

    Customer createCustomer(Customer customer, Address homeAddress);

    Customer findCustomer(Long customerId);

    void deleteCustomer(Customer customer);

    Customer updateCustomer(Customer customer, Address homeAddress);

    List<Customer> findCustomers();
}
