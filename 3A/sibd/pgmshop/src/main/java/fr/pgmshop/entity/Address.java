package fr.pgmshop.entity;

import fr.pgmshop.exception.ValidationException;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
// import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "address")
public class Address implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "city")
    private String city;
    @Size(min = 0, max = 50)
    @Column(name = "state")
    private String state;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "country")
    private String country;
    @Column(name = "street", nullable = false)
    private String street;
    @Column(name = "postal_code", nullable = false, length = 10)
    private String zipcode;

    public Address() {
    }

    public Address(String street, String city, String zipcode, String country) {
        this.street = street;
        this.city = city;
        this.zipcode = zipcode;
        this.country = country;
    }

    @PrePersist
    @PreUpdate
    private void validateData() {
        if (street == null || "".equals(street)) {
            throw new ValidationException("Invalid street");
        }
        if (city == null || "".equals(city)) {
            throw new ValidationException("Invalid city");
        }
        if (zipcode == null || "".equals(zipcode)) {
            throw new ValidationException("Invalid zip code");
        }
        if (country == null || "".equals(country)) {
            throw new ValidationException("Invalid country");
        }
    }

    public Integer getId() {
        return id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Address address = (Address) o;

        if (!city.equals(address.city)) {
            return false;
        }
        if (!country.equals(address.country)) {
            return false;
        }
        if (!id.equals(address.id)) {
            return false;
        }
        if (!street.equals(address.street)) {
            return false;
        }
        if (!zipcode.equals(address.zipcode)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = 0;
        result = 31 * result + street.hashCode();
        result = 31 * result + city.hashCode();
        result = 31 * result + zipcode.hashCode();
        result = 31 * result + country.hashCode();
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Address");
        sb.append("{id=").append(id);
        sb.append(", street='").append(street).append('\'');
        sb.append(", city='").append(city).append('\'');
        sb.append(", zipcode='").append(zipcode).append('\'');
        sb.append(", country='").append(country).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public Address(Integer id) {
        this.id = id;
    }

    public Address(Integer id, String city, String state, String country) {
        this.id = id;
        this.city = city;
        this.state = state;
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
