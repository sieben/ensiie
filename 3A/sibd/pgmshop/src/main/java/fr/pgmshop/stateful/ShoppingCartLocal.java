package fr.pgmshop.stateful;

import fr.pgmshop.entity.catalog.Item;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;

@Local
public interface ShoppingCartLocal {

    void addItem(Item item);

    void removeItem(Item item);

    BigDecimal getTotal();

    void empty();

    List<CartItem> getCartItems();
}
