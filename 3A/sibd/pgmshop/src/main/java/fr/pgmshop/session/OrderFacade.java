package fr.pgmshop.session;

import fr.pgmshop.entity.order.Order;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class OrderFacade extends AbstractFacade<Order> {

    @PersistenceContext(unitName = "pgmshopPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OrderFacade() {
        super(Order.class);
    }
}
