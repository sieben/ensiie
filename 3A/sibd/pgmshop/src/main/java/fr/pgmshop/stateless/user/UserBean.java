package fr.pgmshop.stateless.user;

import fr.pgmshop.entity.customer.Customer;
import fr.pgmshop.entity.user.User;
import fr.pgmshop.exception.ValidationException;
import fr.pgmshop.util.Constants;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

@SuppressWarnings(value = "unchecked")
@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
@Stateless(name = "UserSB")
public class UserBean implements UserRemote, UserLocal {

    @PersistenceContext(unitName = "pgmshopPU")
    private EntityManager em;
    private final String cname = this.getClass().getName();
    private static final Logger logger = Logger.getLogger(Constants.LOGGER_STATELESS);

    @Override
    public User authenticate(final String login, final String password) {
        final String mname = "authenticate";
        logger.entering(cname, mname, new Object[]{login, password});

        // On s'assure de la validité des paramètres
        if (login == null || "".equals(login)) {
            throw new ValidationException("Invalid login");
        }

        Query query;
        User user;

        try {
            // On recherche l'objet à partir de son login
            query = em.createQuery("SELECT u FROM User u WHERE u.userName=:userName");
            query.setParameter("userName", login);
            user = (User) query.getSingleResult();

            // Check if it's the right password
            if (user != null) {
                user.matchPassword(password);
            }
        } catch (NoResultException e) {
            throw new ValidationException("Utilisateur ou mot de passe incorrects");
        } catch (Exception e) {
            throw new ValidationException(e.toString());
        }

        logger.exiting(cname, mname, user);
        return user;
    }

    @Override
    public User createUser(final User user, final Customer customer) {
        final String mname = "createUser";
        logger.entering(cname, mname, user);

        // On s'assure de la validité des paramètres
        if (user == null) {
            throw new ValidationException("User object is null");
        }

        user.setCustomerId(customer);
        try {
            em.persist(user);
        } catch (ConstraintViolationException e) {
            for (ConstraintViolation cv : e.getConstraintViolations()) {
                logger.log(java.util.logging.Level.WARNING, "{0}: {1}", new Object[]{cv.getLeafBean(), cv.getMessage()});
            }
            throw new ValidationException("marche pas");
        }

        logger.exiting(cname, mname, user);
        return user;
    }

    @Override
    public User findUser(final Long userId) {
        final String mname = "findUser";
        logger.entering(cname, mname, userId);

        // On s'assure de la validité des paramètres
        if (userId == null) {
            throw new ValidationException("Invalid id");
        }

        User user;

        // On recherche l'objet à partir de son identifiant
        user = em.find(User.class, userId);

        logger.exiting(cname, mname, user);
        return user;
    }

    @Override
    public void deleteUser(final User user) {
        final String mname = "deleteUser";
        logger.entering(cname, mname, user);

        // On s'assure de la validité des paramètres
        if (user == null) {
            throw new ValidationException("User object is null");
        }

        // On supprime l'objet de la base de données
        em.remove(em.merge(user));

        logger.exiting(cname, mname);
    }

    @Override
    public User updateUser(final User user) {
        final String mname = "updateUser";
        logger.entering(cname, mname, user);

        // On s'assure de la validité des paramètres
        if (user == null) {
            throw new ValidationException("User object is null");
        }

        // On modifie l'objet de la base de données
        em.merge(user);

        logger.exiting(cname, mname, user);
        return user;
    }

    @Override
    public List<User> findUsers() {
        final String mname = "findUsers";
        logger.entering(cname, mname);

        Query query;
        List<User> users;

        // On modifie l'objet de la base de données
        query = em.createQuery("SELECT u FROM user u");
        users = query.getResultList();

        logger.exiting(cname, mname, users.size());
        return users;
    }
}
