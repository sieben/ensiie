﻿using System;

namespace DAL
{
    public class QueryParameter
    {
        public string Key { get; set; }
        public object Value { get; set; }
    }
}