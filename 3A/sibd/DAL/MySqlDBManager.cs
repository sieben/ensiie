﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Globalization;

using MySql.Data.MySqlClient;

namespace DAL
{
    /// <summary>
    /// Summary description for DBAccess
    /// </summary>
    public static class MySqlDBManager
    {
        /// <summary>
        /// Defines the number of times the DBManager will try to get a valid connection from the connection pool
        /// before throwing an exception. This is useful when network has been lost and all (or some) connections in the pool
        /// are broken (in this case, the connection.Open() method returns a broken connection from the pool ...).
        /// </summary>
        private const int MAX_ATTEMPTS_TO_GET_NEW_CONNECTION = 10;

        /// <summary>
        /// Executes a SQL statement against the connection and returns the number of rows affected.
        /// </summary>
        /// <param name="query">The SQL query to execute (UPDATE, INSERT, DELETE)</param>
        /// <param name="parameters">The optional parameters that come with the parameterized query</param>
        /// <returns>The number of rows affected or -1 if not set.</returns>
        public static int ExecuteNonQuery(string query, QueryParameter[] parameters)
        {
            int rowsAffected = -1;

            using (MySqlConnection connection = ConnectionFactory.CreateConnection())
            {
                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    command.CommandType = CommandType.Text;
                    if ((parameters != null) && (parameters.Length > 0))
                    {
                        foreach (QueryParameter param in parameters)
                        {
                            MySqlParameter parameter = new MySqlParameter(param.Key, param.Value);
                            command.Parameters.Add(parameter);
                        }
                    }

                    OpenConnection(connection);
                    rowsAffected = command.ExecuteNonQuery();
                    CloseConnection(connection);

                    return rowsAffected;
                }
            }
        }

        /// <summary>
        /// Executes a SELECT SQL statement against the connection and returns the fetched data
        /// </summary>
        /// <param name="query">The SELECT query to execute</param>
        /// <param name="parameters">The optional parameters that come with the parameterized query</param>
        /// <returns>The requested data by the SELECT statement</returns>
        public static DataTable ExecuteDatatable(string query, QueryParameter[] parameters)
        {
            using (MySqlConnection connection = ConnectionFactory.CreateConnection())
            {
                using (MySqlDataAdapter adapter = new MySqlDataAdapter(query, connection))
                {
                    if ((parameters != null) && (parameters.Length > 0))
                    {
                        foreach (QueryParameter param in parameters)
                        {
                            MySqlParameter parameter = new MySqlParameter(param.Key, param.Value);
                            adapter.SelectCommand.Parameters.Add(parameter);
                        }
                    }

                    DataTable table = GetNewDataTable();

                    OpenConnection(connection);
                    adapter.Fill(table);
                    CloseConnection(connection);

                    return table;
                }
            }
        }

        #region Helpers

        public static string FormatString(string str)
        {
            return str.Replace("'", "''");
        }

        /// <summary>
        /// Open the Connection when the state is not already open.
        /// </summary>
        private static void OpenConnection(MySqlConnection connection)
        {
            // We should put the broken connection out of the connection pool
            // if a network problem has happened
            int attemptNumber = 0;
            while ((connection.State == ConnectionState.Broken) && (attemptNumber < MAX_ATTEMPTS_TO_GET_NEW_CONNECTION))
            {
                connection.Dispose();
                connection = ConnectionFactory.CreateConnection();

                ++attemptNumber;
            }

            // Change the state of the connection to 'Open' if necessary
            if (connection.State != ConnectionState.Open)
                connection.Open();
        }


        /// <summary>
        /// Close the Connection when the state is open.
        /// </summary>
        private static void CloseConnection(MySqlConnection connection)
        {
            if ((connection != null) && (connection.State != ConnectionState.Closed))
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Gets a new well initialized instance of DataSet
        /// </summary>
        public static DataSet GetNewDataSet()
        {
            DataSet ds = new DataSet();

            // Data should not be dependant upon culture
            ds.Locale = CultureInfo.InvariantCulture;

            // DataSet should not be case sensitive (as the database) to avoid problems while merging (duplicated columns, and so on ...)
            ds.CaseSensitive = false;

            // DataSet should always be serialized with BinaryFormatter (for .NET Remoting)
            ds.RemotingFormat = SerializationFormat.Binary;

            return ds;
        }

        /// <summary>
        /// Gets a new well initialized instance of DataTable
        /// </summary>
        public static DataTable GetNewDataTable()
        {
            return InitializeDataTable(null);
        }

        /// <summary>
        /// Initializes an instance of DataTable that will be returned by the DBManager
        /// </summary>
        /// <returns></returns>
        public static DataTable InitializeDataTable(DataTable tableToInitialize)
        {
            if (tableToInitialize == null)
                tableToInitialize = new DataTable();

            // Data should not be dependant upon culture
            tableToInitialize.Locale = CultureInfo.InvariantCulture;

            // DataTable should not be case sensitive (as the database) to avoid problems while merging (duplicated columns, and so on ...)
            tableToInitialize.CaseSensitive = false;

            // DataTable should always be serialized with BinaryFormatter (for .NET Remoting)
            // Check if the table is not attached to a dataset cause their Remoting Format cannot be different
            if (tableToInitialize.DataSet == null)
                tableToInitialize.RemotingFormat = SerializationFormat.Binary;

            return tableToInitialize;
        }

        #endregion
    }
}