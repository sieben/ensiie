﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace DAL
{
    public static class AddStuff
    {
        public static bool AddCategory(string strCatName, string strCatDescr)
        {
            string query = "INSERT INTO category (name, description) VALUES (@CatName, @Description)";
            QueryParameter[] parameters = new QueryParameter[] {
                    new QueryParameter { Key = "@CatName", Value = strCatName },
                    new QueryParameter { Key = "@Description", Value = strCatDescr }
            };
            return (MySqlDBManager.ExecuteNonQuery(query,parameters) != -1);
        }

        public static bool AddProduct(string strProdName, string strPrice, string strCat)
        {
            decimal prodPrice = decimal.Parse(strPrice);

            string preQuery = "SELECT id FROM category WHERE name = @CatName;";
            QueryParameter[] preParameters = new QueryParameter[] {
                    new QueryParameter { Key = "@CatName", Value = strCat }};
            DataTable dt = MySqlDBManager.ExecuteDatatable(preQuery, preParameters);
            int catID = (int) dt.Rows[0]["id"];

            string query = "INSERT INTO product (name, category_id, unit_price) VALUES (@ProdName, @CatID, @Price)";
            QueryParameter[] parameters = new QueryParameter[] {
                    new QueryParameter { Key = "@ProdName", Value = strProdName },
                    new QueryParameter { Key = "@CatID", Value = catID },
                    new QueryParameter { Key = "@Price", Value = prodPrice }
            };
            return (MySqlDBManager.ExecuteNonQuery(query, parameters) != -1);
        }
        public static bool AddItem(string strItName, string strItPrice, string strProdName)
        {
            decimal itPrice = decimal.Parse(strItPrice);

            string preQuery = "SELECT id FROM product WHERE name = @ProdName;";
            QueryParameter[] preParameters = new QueryParameter[] {
                    new QueryParameter { Key = "@ProdName", Value = strProdName }};
            DataTable dt = MySqlDBManager.ExecuteDatatable(preQuery, preParameters);
            int prodID = (int)dt.Rows[0]["id"];

            string query = "INSERT INTO product (name, price, product_id) VALUES (@ProdName, @Price, @ProdID)";
            QueryParameter[] parameters = new QueryParameter[] {
                    new QueryParameter { Key = "@ProdName", Value = strProdName },
                    new QueryParameter { Key = "@ProdID", Value = prodID },
                    new QueryParameter { Key = "@Price", Value = itPrice }
            };
            return (MySqlDBManager.ExecuteNonQuery(query, parameters) != -1);
        }
    }
}