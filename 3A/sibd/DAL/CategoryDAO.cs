﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace DAL
{
    public class CategoryDAO
    {
        /// <summary>
        /// Gets the category list.
        /// </summary>
        /// <returns></returns>
        public static List<Entity.Category> GetCategoryList()
        {
            DataSet1TableAdapters.categoryTableAdapter Adapter = new DataSet1TableAdapters.categoryTableAdapter();
            DataSet1.categoryDataTable table = Adapter.GetCategories();
            IList<Entity.Category> list = new List<Entity.Category>();
            foreach (DataRow row in table.Rows)
            {
                Entity.Category cat = new Entity.Category();
                cat.Id = (int)row["id"];
                cat.Name = row["name"] as string;
                cat.Description = row["description"] as string;
                
                list.Add(cat);
            }
            return (List<Entity.Category>)list;
        }
        /// <summary>
        /// Gets the category name list.
        /// </summary>
        /// <returns></returns>
        public static List<string> GetCatNameList()
        {
            IList<string> nameList = new List<string>();
            List<Entity.Category> list = GetCategoryList();
            for (int i = 0; i < list.Count; i++)
            {
                nameList.Add(list[i].Name);
            }
            return (List<string>)nameList;
        }
        /// <summary>
        /// Gets the description of the category by its name.
        /// </summary>
        /// <param name="CatName">Name of the cat.</param>
        /// <returns></returns>
        public static string GetCatDescByName(string CatName)
        {
            DataSet1TableAdapters.categoryTableAdapter Adapter = new DataSet1TableAdapters.categoryTableAdapter();
            DataRow row = Adapter.GetDescriptionByName(CatName).Rows[0];
            return (row["description"] as string);
        }

        /// <summary>
        /// Gets the id of the category id by its name.
        /// </summary>
        /// <param name="CatName">Name of the category.</param>
        /// <returns></returns>
        public static int GetCatIdByName(string CatName)
        {
            DataSet1TableAdapters.categoryTableAdapter Adapter = new DataSet1TableAdapters.categoryTableAdapter();
            DataRow row = Adapter.GetCatIdByName(CatName).Rows[0];
            return ((int)row["id"]);
        }


        /// <summary>
        /// Updates the category.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="description">The description.</param>
        /// <param name="originalName">The original name.</param>
        /// <returns></returns>
        public static string UpdateCategory (string name, string description, string originalName)
        {
            int i = 0;
            DataSet1TableAdapters.categoryTableAdapter Adapter = new DataSet1TableAdapters.categoryTableAdapter();
            i = Adapter.UpdateCategory(name, description,originalName);
            if (i == 0)
                return ("Rien a changé");
            else return ("Mise à jour appliquée");
        }
        /// <summary>
        /// Inserts the category.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="description">The description.</param>
        /// <returns></returns>
        public static string InsertCategory(string name, string description)
        {
            int i = 0;
            DataSet1TableAdapters.categoryTableAdapter Adapter = new DataSet1TableAdapters.categoryTableAdapter();
            i = Adapter.InsertCategory(name, description);
            if (i == 0)
                return ("Echec de l'ajout");
            else return ("1 Catégorie a été rajoutée");
        }
        /// <summary>
        /// Deletes the category.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static string DeleteCategory(string name)
        {
            int i = 0;
            DataSet1TableAdapters.categoryTableAdapter Adapter = new DataSet1TableAdapters.categoryTableAdapter();
            i = Adapter.DeleteCategory(name);
            if (i == 0)
                return ("Echec de la suppression");
            else return ("1 Catégorie a été supprimée");
        }
    }
}
