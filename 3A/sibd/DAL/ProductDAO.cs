﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace DAL
{
    public static class ProductDAO
    {
        /// <summary>
        /// Gets the product list.
        /// </summary>
        /// <returns></returns>
        public static List<Entity.Product> GetProductList()
        {
            //string req = "SELECT * FROM product";
            DataSet1TableAdapters.productTableAdapter Adaptor = new DataSet1TableAdapters.productTableAdapter();
            DataSet1.productDataTable table = Adaptor.GetProducts();
            //DataTable table = MySqlDBManager.ExecuteDatatable(req, null);
            IList<Entity.Product> list = new List<Entity.Product>();
            foreach (DataRow row in table.Rows)
            {
                Entity.Product prod = new Entity.Product();
                prod.Id = (int)row["id"];
                prod.Name = row["name"] as string;
                prod.Unit_price = (decimal)row["unit_price"];
                prod.Category_id = (int)row["category_id"];
                
                list.Add(prod);
            }
            return (List<Entity.Product>)list;
        }
        /// <summary>
        /// Gets the product name list.
        /// </summary>
        /// <returns></returns>
        public static List<string> GetProdNameList()
        {
            IList<string> nameList = new List<string>();
            List<Entity.Product> list = GetProductList();
            for (int i = 0; i < list.Count; i++)
            {
                nameList.Add(list[i].Name);
            }
            return (List<string>)nameList;
        }
        /// <summary>
        /// Gets the product name list by category name.
        /// </summary>
        /// <param name="catName">Name of the cat.</param>
        /// <returns></returns>
        public static List<string> GetProdNameByCatNameList(string catName)
        {
            DataSet1TableAdapters.productTableAdapter Adaptor = new DataSet1TableAdapters.productTableAdapter();
            DataSet1.productDataTable table = Adaptor.GetProdNameByCatName(catName);
            IList<string> nameList = new List<string>();
            foreach (DataRow row in table)
            {
                nameList.Add(row["name"] as string);
            }
            return (List<string>)nameList;
        }
        /// <summary>
        /// Gets the price of the product by its name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static decimal GetProdPriceByName(string name)
        {
            decimal price = 0;
            IList<Entity.Product> list = GetProductList();
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Name.Equals(name))
                     price = list[i].Unit_price;
            }
            return price;
        }
        /// <summary>
        /// Updates the product.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="catName">Name of the category.</param>
        /// <param name="originalName">The original name.</param>
        /// <param name="price">The price.</param>
        /// <returns>A status string or error.</returns>
        public static string UpdateProduct(string name, string catName, string originalName, decimal price)
        {
            int count = 0;
            int catId = CategoryDAO.GetCatIdByName(catName);
            DataSet1TableAdapters.productTableAdapter Adapter = new DataSet1TableAdapters.productTableAdapter();
            count = Adapter.UpdateProduct(name, catId, price, originalName);
            if (count == 0 || count == -1)
                return ("Rien a changé");
            else return ("Mise à jour appliquée");
        }
        /// <summary>
        /// Inserts the product.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="catName">Name of the category.</param>
        /// <param name="price">The price.</param>
        /// <returns></returns>
        public static string InsertProduct(string name, string catName, decimal price)
        {
            int count = 0;
            int catId=CategoryDAO.GetCatIdByName(catName);
            DataSet1TableAdapters.productTableAdapter Adapter = new DataSet1TableAdapters.productTableAdapter();
            count = Adapter.InsertProduct(name, catId, price);
            if (count == 0)
                return ("Echec de l'ajout");
            else return ("1 Produit a été rajouté");
        }
        /// <summary>
        /// Gets the name of the prod id by.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static int GetProdIdByName(string name)
        {
            int id = 0;
            IList<Entity.Product> list = GetProductList();
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Name.Equals(name))
                    id = list[i].Id;
            }
            return id;
        }
        /// <summary>
        /// Deletes the product.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static string DeleteProduct(string name)
        {
            int i = 0;
            DataSet1TableAdapters.productTableAdapter Adapter = new DataSet1TableAdapters.productTableAdapter();
            i = Adapter.DeleteProduct(name);
            if (i == 0)
                return ("Echec de la suppression");
            else return ("1 Produit a été supprimée");
        }

    }
}