﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace DAL
{
    public class ItemDAO
    {
        /// <summary>
        /// Gets the item list.
        /// </summary>
        /// <returns></returns>
        public static List<Entity.Item> GetItemList()
        {
            DataSet1TableAdapters.itemTableAdapter Adaptor = new DataSet1TableAdapters.itemTableAdapter();
            DataSet1.itemDataTable table = Adaptor.GetItems();
            IList<Entity.Item> list = new List<Entity.Item>();
            foreach (DataRow row in table.Rows)
            {
                Entity.Item item = new Entity.Item();
                item.Id = (int)row["id"];
                item.Name = row["name"] as string;
                item.Price = (decimal)row["price"];
                item.Product_id = (int)row["product_id"];
                item.Quantity = (int)row["quantity"];

                list.Add(item);
            }
            return (List<Entity.Item>)list;
        }
        /// <summary>
        /// Gets the item name list.
        /// </summary>
        /// <returns></returns>
        public static List<string> GetItemNameList()
        {
            IList<string> nameList = new List<string>();
            List<Entity.Item> list = GetItemList();
            for (int i = 0; i < list.Count; i++)
            {
                nameList.Add(list[i].Name);
            }
            return (List<string>)nameList;
        }
        /// <summary>
        /// Gets the item name by prod name list.
        /// </summary>
        /// <param name="ProdName">Name of the product.</param>
        /// <returns></returns>
        public static List<string> GetItemNameByProdNameList(string ProdName)
        {
            DataSet1TableAdapters.itemTableAdapter Adaptor = new DataSet1TableAdapters.itemTableAdapter();
            DataSet1.itemDataTable table = Adaptor.GetItemDataByProductName(ProdName);
            IList<string> nameList = new List<string>();
            foreach (DataRow row in table)
            {
                nameList.Add(row["name"] as string);
            }
            return (List<string>)nameList;
        }
        /// <summary>
        /// Gets the price of the item by its name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static decimal GetItemPriceByName(string name)
        {
            decimal price = 0;
            IList<Entity.Item> list = GetItemList();
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Name.Equals(name))
                    price = list[i].Price;
            }
            return price;
        }
        /// <summary>
        /// Gets the name of the item quantity by.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static int GetItemQuantityByName(string name)
        {
            int quantity = 0;
            IList<Entity.Item> list = GetItemList();
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Name.Equals(name))
                    quantity = list[i].Quantity;
            }
            return quantity;
        }
        /// <summary>
        /// Updates the item.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="prodName">Name of the product.</param>
        /// <param name="originalName">The original name.</param>
        /// <param name="price">The price.</param>
        /// <param name="qty">The quantity.</param>
        /// <returns></returns>
        public static string UpdateItem(string name, string prodName, string originalName, decimal price, int qty)
        {
            int count = 0;
            int prodId = ProductDAO.GetProdIdByName(prodName);
            DataSet1TableAdapters.itemTableAdapter Adapter = new DataSet1TableAdapters.itemTableAdapter();
            count = Adapter.UpdateItem(name, price, prodId, qty, originalName);
            if (count == 0 || count == -1)
                return ("Rien a changé");
            else return ("Mise à jour appliquée");
        }
        /// <summary>
        /// Inserts the item.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="prodName">Name of the product.</param>
        /// <param name="price">The price.</param>
        /// <param name="quantity">The quantity.</param>
        /// <returns></returns>
        public static string InsertItem(string name, string prodName, decimal price, int quantity)
        {
            int count = 0;
            int prodId = ProductDAO.GetProdIdByName(prodName);
            DataSet1TableAdapters.itemTableAdapter Adapter = new DataSet1TableAdapters.itemTableAdapter();
            count = Adapter.InsertItem(name, price, prodId, quantity);
            if (count == 0)
                return ("Echec de l'ajout");
            else return ("1 Objet a été rajouté");
        }
        /// <summary>
        /// Deletes the item.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static string DeleteItem(string name)
        {
            int i = 0;
            DataSet1TableAdapters.itemTableAdapter Adapter = new DataSet1TableAdapters.itemTableAdapter();
            i = Adapter.DeleteItem(name);
            if (i == 0)
                return ("Echec de la suppression");
            else return ("1 Objet a été supprimée");
        }


    }
}
