﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace DAL
{
    public class OverallProductViewDAO
    {
        /// <summary>
        /// Overalls the product view.
        /// </summary>
        /// <returns></returns>
        public static List<Entity.OverallProductView> OverallProductView()
        {
            DataSet1TableAdapters.overall_product_viewTableAdapter Adapter = new DataSet1TableAdapters.overall_product_viewTableAdapter();
            DataSet1.overall_product_viewDataTable table = Adapter.GetOverallProductView();
            IList<Entity.OverallProductView> list = new List<Entity.OverallProductView>();
            foreach (DataRow row in table.Rows)
            {
                Entity.OverallProductView opv = new Entity.OverallProductView();
                opv.C_id = (int)row["c_id"];
                opv.C_name = row["c_name"] as string;
                opv.C_description = row["c_description"] as string;
                opv.P_id = (int)row["p_id"];
                opv.P_name = row["p_name"] as string;
                opv.P_unit_price = (decimal)row["p_unit_price"];
                opv.I_id = (int)row["i_id"];
                opv.I_name = row["i_name"] as string;
                opv.I_price = (decimal)row["i_price"];
                opv.I_quantity = (int)row["i_quantity"];

                list.Add(opv);
            }
            return (List<Entity.OverallProductView>)list;
        }
    }
}