﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;


namespace DAL
{
    public static class Authenticator
    {
        public static bool Authentify(string strUser, string strPasswd)
        {
            bool test = false;
            string query = "SELECT is_admin FROM user WHERE user_name=@UserName and password=password(@Password)";
            QueryParameter[] parameters = new QueryParameter[] {
                    new QueryParameter { Key = "@UserName", Value = strUser },
                    new QueryParameter { Key = "@Password", Value = strPasswd }
            };
            DataTable table = MySqlDBManager.ExecuteDatatable(query, parameters);
                if (table.Rows.Count == 1)
                {
                    if (table.Rows[0]["is_admin"].ToString().Equals("1"))
                    {
                        test = true;
                    }
                }
            
            return test;
            
        }
    }
}