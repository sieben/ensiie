﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace DAL
{
    public class UserDAO
    {
        /// <summary>
        /// Gets the user list.
        /// </summary>
        /// <returns></returns>
        public static List<Entity.User> GetUserList()
        {
            DataSet1TableAdapters.userTableAdapter Adapter = new DataSet1TableAdapters.userTableAdapter();
            DataSet1.userDataTable table = Adapter.GetUsers();
            IList<Entity.User> list = new List<Entity.User>();
            foreach (DataRow row in table.Rows)
            {
                Entity.User u = new Entity.User();
                u.Id = (int)row["id"];
                u.Is_admin = row["is_admin"] as string;
                u.Customer_id = (int)row["customer_id"];
                u.User_name = row["user_name"] as string;
                u.Password = row["password"] as string;
                
                list.Add(u);
            }
            return (List<Entity.User>)list;
        }
        /// <summary>
        /// Checks if the iser is an admin.
        /// </summary>
        /// <param name="UserName">Name of the user.</param>
        /// <param name="Password">The password.</param>
        /// <returns></returns>
        public static List<string> GetIsAdmin(string UserName, string Password)
        {
            DataSet1TableAdapters.userTableAdapter Adapter = new DataSet1TableAdapters.userTableAdapter();
            DataSet1.userDataTable table = Adapter.GetIsAdminByUserAndPassword(UserName, Password);
            IList<string> list = new List<string>();
            foreach (DataRow row in table.Rows)
            {
                string st = row["is_admin"] as string;
                list.Add(st);
            }
            return (List<string>)list;
        }
    }
}
