﻿using System.Configuration;

using MySql.Data.MySqlClient;

namespace DAL
{
    /// <summary>
    /// Provides access to the connection string and delivers new SQL connections
    /// </summary>
    public static class ConnectionFactory
    {
        private static string s_ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["MySQLConnection"].ConnectionString;

        /// <summary>
        /// Returns a free connection from the Connection Pool
        /// (ADO .NET manages automatically a connection pool for each different connection string)
        /// </summary>
        public static MySqlConnection CreateConnection()
        {
            return new MySqlConnection(s_ConnectionString);
        }

        /// <summary>
        /// Gets the connection string
        /// </summary>
        public static string ConnectionString
        {
            get { return s_ConnectionString; }
        }
    }
}