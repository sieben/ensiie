-- Script de création d'utilisateur

CREATE USER 'pgmshop'@'localhost' IDENTIFIED BY 'pgmshop';
GRANT SELECT, INSERT, UPDATE, DELETE, LOCK TABLES ON `pgmshop`.* TO 'pgmshop'@'localhost';
