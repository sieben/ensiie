﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace pgmshopBackOffice.Products
{
    public partial class Categories : System.Web.UI.Page
    {

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ddlCatName.AppendDataBoundItems = true;
            if (!IsPostBack)
            {
                ddlCatName.DataSource = BLL.CategoryBLL.LoadCatNames();
                ddlCatName.DataBind();
            }       
            ddlCatName.AutoPostBack = true;
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlCatName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ddlCatName_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbActionStatus.Text = null;

            if (!(ddlCatName.Text.Equals("Nouvelle Catégorie")))
            {
                tbCatName.Text = ddlCatName.Text;
                tbCatDescr.Text = BLL.CategoryBLL.LoadCatDescrByName(ddlCatName.Text);
            }
            else
            {
                tbCatName.Text = "";
                tbCatDescr.Text = "";
            }
        }

        /// <summary>
        /// Handles the Click event of the btCatAdd control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btCatAdd_Click(object sender, EventArgs e)
        {
            if (!(ddlCatName.Text.Equals("Nouvelle Catégorie")))
                lbActionStatus.Text = BLL.CategoryBLL.UpdateCategory(tbCatName.Text, tbCatDescr.Text, ddlCatName.Text);
            else
                lbActionStatus.Text = BLL.CategoryBLL.InsertCategory(tbCatName.Text, tbCatDescr.Text);
        }

        /// <summary>
        /// Handles the Click event of the btCatDel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btCatDel_Click(object sender, EventArgs e)
        {
            lbActionStatus.Text = BLL.CategoryBLL.DeleteCategory(tbCatName.Text);
        }
    }
}