﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace pgmshopBackOffice.Products
{
    public partial class Products : System.Web.UI.Page
    {
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlCatProd.DataSource = BLL.CategoryBLL.LoadCatNames();
                ddlCatProd.DataBind();
                ddlProdName.AppendDataBoundItems = true;
                ddlProdName.DataSource = BLL.ProductBLL.LoadProdNamesByCat(ddlCatProd.Text);
                ddlProdName.DataBind();
            }
            ddlCatProd.AutoPostBack = true;
            ddlProdName.AutoPostBack = true;
        }


        /// <summary>
        /// Handles the Click event of the btProdAdd control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btProdAdd_Click(object sender, EventArgs e)
        {
            if (!(ddlProdName.Text.Equals("Nouveau Produit")))
                lbActionStatus.Text = BLL.ProductBLL.UpdateProduct(tbProdName.Text, ddlCatProd.Text,tbProductPrice.Text, ddlProdName.Text);
            else
                lbActionStatus.Text = BLL.ProductBLL.InsertProduct(tbProdName.Text, ddlCatProd.Text, tbProductPrice.Text);
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlCatProd control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ddlCatProd_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbActionStatus.Text = null;
            ddlProdName.Items.Clear();
            ddlProdName.Items.Add("Nouveau Produit");
            ddlProdName.AppendDataBoundItems = true;
            ddlProdName.DataSource = BLL.ProductBLL.LoadProdNamesByCat(ddlCatProd.Text);
            ddlProdName.DataBind();
            if (!ddlProdName.Text.Equals("Nouveau Produit"))
            {
                tbProdName.Text = ddlProdName.Text;
                tbProductPrice.Text = BLL.ProductBLL.LoadProdPriceByName(ddlProdName.Text).ToString();
            }
            else
            {
                tbProdName.Text = "";
                tbProductPrice.Text = "";
            }
            
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlProdName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ddlProdName_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbActionStatus.Text = null;
            if (!ddlProdName.Text.Equals("Nouveau Produit"))
            {
                tbProdName.Text = ddlProdName.Text;
                tbProductPrice.Text = BLL.ProductBLL.LoadProdPriceByName(ddlProdName.Text).ToString();
            }
            else
            {
                tbProdName.Text = "";
                tbProductPrice.Text = "";
            }
        }

        /// <summary>
        /// Handles the Click event of the btProdDel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btProdDel_Click(object sender, EventArgs e)
        {
            lbActionStatus.Text = BLL.ProductBLL.DeleteProduct(ddlProdName.Text);
        }

        
    }
}