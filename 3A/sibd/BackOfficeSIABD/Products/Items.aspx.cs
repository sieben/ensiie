﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace pgmshopBackOffice.Products
{
    public partial class Items : System.Web.UI.Page
    {
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlCatItem.DataSource = BLL.CategoryBLL.LoadCatNames();
                ddlCatItem.DataBind();
                ddlProdItem.DataSource = BLL.ProductBLL.LoadProdNamesByCat(ddlCatItem.Text);
                ddlProdItem.DataBind();

            }
            ddlCatItem.AutoPostBack = true;
            ddlProdItem.AutoPostBack = true;
            ddlItemName.AutoPostBack = true;
        }
        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlCatItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public void ddlCatItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlProdItem.Items.Clear();
            ddlProdItem.DataSource = BLL.ProductBLL.LoadProdNamesByCat(ddlCatItem.Text);
            ddlProdItem.DataBind();
            lbActionStatus.Text = null;
            tbItemName.Text = "";
            tbItemPrice.Text = "";
            tbItemQuantity.Text = "";
        }
        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlProdItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public void ddlProdItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlItemName.Items.Clear();
            ddlItemName.Items.Add("Nouvel Objet");
            ddlItemName.AppendDataBoundItems = true;
            ddlItemName.DataSource = BLL.ItemBLL.LoadItemNamesByProduct(ddlProdItem.Text);
            ddlItemName.DataBind();
            lbActionStatus.Text = null;
            if (!ddlItemName.Text.Equals("Nouvel Objet"))
            {
                tbItemName.Text = ddlItemName.Text;
                tbItemPrice.Text = BLL.ItemBLL.LoadItemPriceByName(ddlItemName.Text).ToString();
                tbItemQuantity.Text = BLL.ItemBLL.LoadItemQuantityByName(ddlItemName.Text).ToString();
            }
            else
            {
                tbItemName.Text = "";
                tbItemPrice.Text = "";
                tbItemQuantity.Text = "";
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ddlItemName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ddlItemName_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbActionStatus.Text = null;
            if (!ddlItemName.Text.Equals("Nouvel Objet"))
            {
                tbItemName.Text = ddlItemName.Text;
                tbItemPrice.Text = BLL.ItemBLL.LoadItemPriceByName(ddlItemName.Text).ToString();
                tbItemQuantity.Text = BLL.ItemBLL.LoadItemQuantityByName(ddlItemName.Text).ToString();
            }
            else
            {
                tbItemName.Text = "";
                tbItemPrice.Text = "";
                tbItemQuantity.Text = "";
            }
        }

        /// <summary>
        /// Handles the Click event of the btItemAdd control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btItemAdd_Click(object sender, EventArgs e)
        {
            if (!(ddlItemName.Text.Equals("Nouvel Objet")))
                lbActionStatus.Text = BLL.ItemBLL.UpdateItem(tbItemName.Text, ddlProdItem.Text, tbItemPrice.Text, tbItemQuantity.Text, ddlItemName.Text);
            else
                lbActionStatus.Text = BLL.ItemBLL.InsertItem(tbItemName.Text, ddlProdItem.Text, tbItemPrice.Text, tbItemQuantity.Text);
        }

        /// <summary>
        /// Handles the Click event of the btItemDel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btItemDel_Click(object sender, EventArgs e)
        {
            lbActionStatus.Text = BLL.ItemBLL.DeleteItem(ddlItemName.Text);
        }
    }
}