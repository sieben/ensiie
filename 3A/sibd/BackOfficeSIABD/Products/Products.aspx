﻿<%@ Page Title="Gestion des Produits" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Products.aspx.cs" Inherits="pgmshopBackOffice.Products.Products" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table>
        <tr>
            <td>
                <asp:Label runat="server" Text="Produits" ID="lbProduct" Font-Bold="True" 
                    ForeColor="#0066CC" Font-Size="Medium"></asp:Label>
            </td>
        </tr>
        <tr>
             <td>
                <asp:Label runat="server" Text="Catégorie" ID="lbCatProd" Font-Bold="False" 
                    ForeColor="#0066CC" Font-Size="Small"></asp:Label>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlCatProd" 
                    ToolTip="Choisir le nom de la catégorie" 
                    onselectedindexchanged="ddlCatProd_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                 <asp:Label runat="server" Text="Nom Produit" ID="lbProdName" Font-Bold="False" 
                    ForeColor="#0066CC" Font-Size="Small"></asp:Label>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlProdName" 
                    onselectedindexchanged="ddlProdName_SelectedIndexChanged">
                    <asp:ListItem>Nouveau Produit</asp:ListItem>
                </asp:DropDownList>
            </td>
            </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbProdName" ToolTip="Nouveau nom de produit"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" Text="Prix" ID="lbProductPrice" Font-Bold="False" 
                    ForeColor="#0066CC" Font-Size="Small"></asp:Label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbProductPrice" ToolTip="Prix du produit" 
                    MaxLength="7"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button runat="server" ID="btProdAdd" Text="Ajouter/Mettre à jour" 
                    onclick="btProdAdd_Click" />
            </td>
            <td>
                <asp:Button runat="server" ID="btProdDel" Text="Supprimer" 
                    onclick="btProdDel_Click" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Label runat="server" Text="" ID="lbActionStatus" ForeColor="#FF3300" Font-Size="Large"></asp:Label>
            </td>
        </tr>
        
    </table>
</asp:Content>
