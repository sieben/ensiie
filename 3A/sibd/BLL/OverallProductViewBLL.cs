﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using DAL.DataSet1TableAdapters;
using Entity;

namespace BLL
{
    public class OverallProductViewBLL
    {
        public static List<Entity.OverallProductView> LoadProdList()
        {
            return DAL.OverallProductViewDAO.OverallProductView();
        }
    }
}
