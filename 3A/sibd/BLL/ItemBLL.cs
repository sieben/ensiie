﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace BLL
{
    public class ItemBLL
    {
        /// <summary>
        /// Loads the item names.
        /// </summary>
        /// <returns></returns>
        public static List<string> LoadItemNames()
        {
            return DAL.ItemDAO.GetItemNameList();
        }
        /// <summary>
        /// Loads the item names by product.
        /// </summary>
        /// <param name="productName">Name of the product.</param>
        /// <returns></returns>
        public static List<string> LoadItemNamesByProduct(string productName)
        {
            return DAL.ItemDAO.GetItemNameByProdNameList(productName);
        }
        /// <summary>
        /// Loads the price of the item by its name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static decimal LoadItemPriceByName(string name)
        {
            return DAL.ItemDAO.GetItemPriceByName(name);
        }
        /// <summary>
        /// Loads the quantity of the item by its name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static int LoadItemQuantityByName(string name)
        {
            return DAL.ItemDAO.GetItemQuantityByName(name);
        }

        /// <summary>
        /// Updates the product.
        /// </summary>
        /// <param name="name">The name of the item.</param>
        /// <param name="prodName">Name of the product.</param>
        /// <param name="strPrice">The price string.</param>
        /// <param name="strQty">The quantity string.</param>
        /// <param name="originalName">The original name.</param>
        /// <returns></returns>
         public static string UpdateItem(string name, string prodName, string strPrice, string strQty, string originalName)
         {
            NumberStyles style = NumberStyles.AllowDecimalPoint;
            decimal decPrice;
            int qty;
            try
            {
                qty = int.Parse(strQty);
            }
            catch(FormatException)
            {
               return "La quantité n'a pas le bon format!";
            }
            try
            {
                decPrice = decimal.Parse(strPrice, style);
            }
            catch(FormatException)
            {
                return "Le prix n'a pas le bon format";
            }
            decimal test=decPrice*100;
            if (!((int)test == test)) //verifier si 2 décimales ou moins après la virgule
                return "Le prix n'a pas le bon format!";
            else
            {
                List<string> list = DAL.ItemDAO.GetItemNameList();
                foreach (string str in list)
                {
                    if (str.Equals(name) && !str.Equals(originalName))
                    {
                        return ("Ce nom existe déjà!");
                    }
                }
                return DAL.ItemDAO.UpdateItem(name, prodName,originalName, decPrice,qty);
            }
        }
         /// <summary>
         /// Inserts the item.
         /// </summary>
         /// <param name="name">The name.</param>
         /// <param name="prodName">Name of the product.</param>
         /// <param name="strPrice">The price string.</param>
         /// <param name="strQty">The qty string.</param>
         /// <returns></returns>
         public static string InsertItem(string name, string prodName, string strPrice, string strQty)
         {
             NumberStyles style = NumberStyles.AllowDecimalPoint;
             decimal decPrice;
             int qty;
             try
             {
                 qty = int.Parse(strQty);
             }
             catch (FormatException)
             {
                 return "La quantité n'a pas le bon format!";
             }
             try
             {
                 decPrice = decimal.Parse(strPrice, style);
             }
             catch (FormatException)
             {
                 return "Le prix n'a pas le bon format";
             }
             decimal test = decPrice * 100;
             if (!((int)test == test)) //verifier si 2 décimales ou moins après la virgule
                 return "Le prix n'a pas le bon format!";
             else
             {
                 List<string> list = DAL.ItemDAO.GetItemNameList();
                 foreach (string str in list)
                 {
                     if (str.Equals(name))
                     {
                         return ("Cet objet existe déjà!");
                     }
                 }
                 return DAL.ItemDAO.InsertItem(name, prodName, decPrice,qty);
             }

         }
         public static string DeleteItem(string name)
         {
             List<Entity.Item> list = DAL.ItemDAO.GetItemList();
             foreach (Entity.Item i in list)
             {
                 if (i.Quantity !=0)
                     return ("Suppression impossible, il y a encore des objets en stock!");
             }
             return DAL.ItemDAO.DeleteItem(name);
         }
    }
}
