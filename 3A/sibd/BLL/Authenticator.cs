﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;

namespace BLL
{
    public class Authenticator
    {
        public static bool Login(string strUser, string strPasswd)
        {
            bool test = false;
            List<string> list = DAL.UserDAO.GetIsAdmin(strUser, strPasswd);
            if (list.Count == 1)
            {
                if (list[0].Equals("1"))
                    test = true;
            }
            return test;
        }
    }
}
