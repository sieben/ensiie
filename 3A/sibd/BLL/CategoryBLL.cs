﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entity;
using DAL;

namespace BLL
{
    public class CategoryBLL
    {
        /// <summary>
        /// Loads the category names.
        /// </summary>
        /// <returns></returns>
        public static List<string> LoadCatNames()
        {
            return DAL.CategoryDAO.GetCatNameList();
        }
        /// <summary>
        /// Loads the name of the category description by name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static string LoadCatDescrByName(string name)
        {
            return DAL.CategoryDAO.GetCatDescByName(name);
        }
        /// <summary>
        /// Updates the category.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="description">The description.</param>
        /// <param name="originalName">The original name.</param>
        /// <returns></returns>
        public static string UpdateCategory(string name, string description, string originalName)
        {
            List<string> list = CategoryDAO.GetCatNameList();
            foreach (string str in list)
            {
                if (str.Equals(name) && !str.Equals(originalName))
                {
                    return ("Ce nom existe déjà!");
                }
            }
            return CategoryDAO.UpdateCategory(name, description, originalName);

        }
        /// <summary>
        /// Inserts the category.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="description">The description.</param>
        /// <returns></returns>
        public static string InsertCategory(string name, string description)
        {
            List<string> list = CategoryDAO.GetCatNameList();
            foreach (string str in list)
            {
                if(str.Equals(name))
                {
                    return ("Cette catégorie existe déjà!");
                }
            }
            return CategoryDAO.InsertCategory (name, description);
        }
        /// <summary>
        /// Deletes the category.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static string DeleteCategory(string name)
        {
            int id = CategoryDAO.GetCatIdByName(name);
            List<Entity.Product> list = ProductDAO.GetProductList();
            foreach (Product p in list)
            {
                if (p.Category_id == id)
                    return ("Suppression impossible, il y a encore des produits dans la catégorie!");
            }
            return CategoryDAO.DeleteCategory(name);
        }
    }
}
