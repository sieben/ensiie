﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace BLL
{
    public class ProductBLL
    {
        /// <summary>
        /// Loads the product names.
        /// </summary>
        /// <returns>The list of product names.</returns>
        public static List<string> LoadProdNames()
        {
            return DAL.ProductDAO.GetProdNameList();
        }

        /// <summary>
        /// Loads the product names by category.
        /// </summary>
        /// <param name="catName">Name of the category.</param>
        /// <returns>A list of product names.</returns>
        public static List<string> LoadProdNamesByCat(string catName)
        {
            return DAL.ProductDAO.GetProdNameByCatNameList(catName);
        }

        /// <summary>
        /// Loads the price of the product by its name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static decimal LoadProdPriceByName(string name)
        {
            return DAL.ProductDAO.GetProdPriceByName(name);
        }
        
        /// <summary>
        /// Updates the product.
        /// </summary>
        /// <param name="name">The new name.</param>
        /// <param name="catName">Name of the category.</param>
        /// <param name="strPrice">The price as string.</param>
        /// <param name="originalName">The original name.</param>
        /// <returns></returns>
        public static string UpdateProduct(string name, string catName, string strPrice, string originalName)
        {
            NumberStyles style = NumberStyles.AllowDecimalPoint;
            decimal decPrice;
            try
            {
                decPrice = decimal.Parse(strPrice, style);
            }
            catch(FormatException)
            {
                return "Le prix n'a pas le bon format";
            }
            decimal test=decPrice*100;
            if (!((int)test == test)) //verifier si 2 décimales ou moins après la virgule
                return "Le prix n'a pas le bon format!";
            else
            {
                List<string> list = DAL.ProductDAO.GetProdNameList();
                foreach (string str in list)
                {
                    if (str.Equals(name) && !str.Equals(originalName))
                    {
                        return ("Ce nom existe déjà!");
                    }
                }
                return DAL.ProductDAO.UpdateProduct(name, catName, originalName, decPrice);
            }
        }
        /// <summary>
        /// Inserts the product.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="catName">Name of the cat.</param>
        /// <param name="strPrice">The price as a string.</param>
        /// <returns></returns>
        public static string InsertProduct(string name, string catName, string strPrice)
        {
            NumberStyles style = NumberStyles.AllowDecimalPoint;
            decimal decPrice;
            try
            {
                decPrice = decimal.Parse(strPrice, style);
            }
            catch (FormatException)
            {
                return "Le prix n'a pas le bon format";
            }
            decimal test=decPrice*100;
            if (!((int)test == test)) //verifier si 2 décimales ou moins après la virgule
                return "Le prix n'a pas le bon format!";
            else
            {
                List<string> list = DAL.ProductDAO.GetProdNameList();
                foreach (string str in list)
                {
                    if (str.Equals(name))
                    {
                        return ("Ce produit existe déjà!");
                    }
                }
                return DAL.ProductDAO.InsertProduct(name, catName, decPrice);
            }
        }
        /// <summary>
        /// Loads the id of the product by its name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static int LoadProdIdByName(string name)
        {
            return DAL.ProductDAO.GetProdIdByName(name);
        }
        /// <summary>
        /// Deletes the product.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static string DeleteProduct(string name)
        {
            int id = DAL.ProductDAO.GetProdIdByName(name);
            List<Entity.Item> list = DAL.ItemDAO.GetItemList();
            foreach (Entity.Item i in list)
            {
                if (i.Product_id == id)
                    return ("Suppression impossible, il y a encore des objets dans la catégorie!");
            }
            return DAL.ProductDAO.DeleteProduct(name);
        }
    }
}
